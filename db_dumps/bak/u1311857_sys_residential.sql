-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 20, 2021 at 10:06 PM
-- Server version: 10.3.31-MariaDB-cll-lve
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u1311857_sys_residential`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `cart_add_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_cart_type` VARCHAR(30), IN `p_item_id` INT(11))  BEGIN

  

  declare t_success smallint; 

  

  declare t_exist	varchar(1) default 'N';

  declare t_cart_id int;

  

  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then

      

    -- cari dulu item tersebut sudah ada atau belum

    select  'Y', cart_id

    into    t_exist, t_cart_id

    from    cart

    where   is_active = 'Y'

            and user_id = p_user_id

            and item_id = p_item_id;

    

    

    if ifnull(t_exist,'N') = 'N' then

      -- insert baru

      insert into cart

      (last_update_date, last_updated_by, creation_date, created_by,

      user_id, cart_type, item_id, qty)

      values

      (now(), p_user_id, now(), p_user_id,

      p_user_id, p_cart_type, p_item_id, 1);

    else

      -- update quantity

      update  cart

      set     qty = qty + 1

      where   cart_id = t_cart_id;

    end if;



                        

    set o_status = 1;

    set o_message = 'Barang berhasil ditambahkan ke keranjang';



  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `cart_del_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_cart_id` INT UNSIGNED)  BEGIN

  

  declare t_success smallint;

  declare t_valid varchar(1);



  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end; 

  

  if access_token_fc(p_access_token, p_user_id) = 'Y' then

    

    update  cart

    set     is_active = 'N',

            last_update_date = now(),

            last_updated_by = p_user_id

    where   cart_id = p_cart_id

            and is_active = 'Y';

    

    select row_count() into t_success;

    

    if t_success <= 0 then

      set o_status = -1;

      select get_message_fc('NOT_FOUND') into o_message;

    else   

      set o_status = 1;

      select get_message_fc('DELETE_SUCCESS') into o_message;

    end if;  



  else

    

    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

    

  end if;

END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `cart_qty_upd_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_cart_id` INT(10) UNSIGNED, IN `p_qty` INT)  BEGIN

  

  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then

      

    -- update quantity

    update  cart

    set     qty = p_qty

    where   cart_id = p_cart_id;



                        

    set o_status = 1;

    set o_message = '';



  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `cc_ticket_approval_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_ticket_id` INT(10) UNSIGNED, IN `p_mtc_approval` VARCHAR(15))  BEGIN

  

  declare t_success smallint; 

  


  declare t_kavling_id int;

  declare t_mtc_cost double(15,2);

  declare t_id_trans int;

  

  declare exit handler for sqlexception

  begin

    ROLLBACK;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then



    -- cari data

    select  kavling_id, mtc_cost

    into    t_kavling_id, t_mtc_cost

    from    cc_ticket

    where   ticket_id = p_ticket_id;



    START TRANSACTION;    

    

    if p_mtc_approval = 'approved' then

      

      update  cc_ticket

      set     status = 'unpaid',

              status_date = now(),

              status_by = p_user_id

      where   ticket_id = p_ticket_id;

      

      insert into transaksi_retail

      (last_update_date, last_updated_by, creation_date, created_by,

       type_trans, date_issued, kavling_id, user_id, nominal)

      values

      (now(), p_user_id, now(), p_user_id,

       3, now(), t_kavling_id, p_user_id, t_mtc_cost);

       

      

      set t_id_trans = last_insert_id();

       

      insert into transaksi_retail_detail

      (last_update_date, last_updated_by, creation_date, created_by,

       id_trans, item_id, item_qty, item_hpp, item_price, subtotal)

      values

      (now(), p_user_id, now(), p_user_id,

       t_id_trans, p_ticket_id, 1, 0, t_mtc_cost, t_mtc_cost);

    

    else 

    

      update  cc_ticket

      set     status = p_mtc_approval,

              status_date = now(),

              status_by = p_user_id

      where   ticket_id = p_ticket_id;

    

    end if;

    

    

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



    COMMIT;

  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `cc_ticket_closed_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_ticket_id` INT(10) UNSIGNED)  BEGIN
  
  declare t_success smallint; 
  
  declare exit handler for sqlexception
  begin
    set o_status = -1;
    select get_message_fc('TROUBLE') into o_message;
  end;  

  if access_token_fc(p_access_token, p_user_id) = 'Y' then

    update  cc_ticket
    set     status = 'closed',
            status_date = now(),
            status_by = p_user_id,
            finish_date = now(),
            is_closed = 'Y'
    where   ticket_id = p_ticket_id;
    
    set o_status = 1;
    select get_message_fc('SAVE_SUCCESS') into o_message;

  else

    set o_status = -1;
    select get_message_fc('ACCESS_VIOLATION') into o_message;
  
  end if;
  
END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `cc_ticket_history_attach_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_history_id` INT(10) UNSIGNED, IN `p_ticket_id` INT(10) UNSIGNED, IN `p_seqno` TINYINT(4), IN `p_attach_file` VARCHAR(100))  BEGIN

  

  declare t_success smallint; 

  

  

  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then



    insert into cc_ticket_history_attach

    (last_update_date, last_updated_by, creation_date, created_by,

     history_id, ticket_id, seqno, attach_file)

    values

    (now(), p_user_id, now(), p_user_id,

     p_history_id, p_ticket_id, p_seqno, p_attach_file);

        

    

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `cc_ticket_new_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), OUT `o_ticket_id` INT(10) UNSIGNED, OUT `o_history_id` INT(10) UNSIGNED, IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_kavling_id` INT(11) UNSIGNED, IN `p_kompleks_id` INT(10) UNSIGNED, IN `p_subject` VARCHAR(200), IN `p_ticket_type` VARCHAR(30), IN `p_description` VARCHAR(3000))  BEGIN

  

  declare t_success smallint; 

  

  declare t_code varchar(5);

  declare t_ticket_num varchar(30);

  

  declare exit handler for sqlexception

  begin

    ROLLBACK;

    set o_ticket_id = 0;

    set o_history_id = 0;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then



    START TRANSACTION;

    

    set t_code = 'MS';

      

    -- generate no order

    select  concat(t_code,date_format(now(),'%y%m'),lpad(ifnull(max(substr(ticket_num,-4)),0) + 1,4,'0'))

    into    t_ticket_num

    from    cc_ticket

    where   substr(ticket_num,1,6) = concat(t_code,date_format(now(),'%y%m'));

  

    if ifnull(t_ticket_num,'') = '' then

      set t_ticket_num = concat(t_code,date_format(now(),'%y%m'),'0001');

    end if;    



    insert into cc_ticket

    (last_update_date, last_updated_by, creation_date, created_by,

     ticket_num, ticket_date, request_by, kavling_id, kompleks_id, 

     subject, ticket_type, description, status, status_date, status_by)

    values

    (now(), p_user_id, now(), p_user_id,

     t_ticket_num, now(), p_user_id, p_kavling_id, p_kompleks_id, 

     p_subject, p_ticket_type, trim(both '\n' from p_description), 'submit', now(), p_user_id);

    

    set o_ticket_id = last_insert_id();

    

    

    

    insert into cc_ticket_history

    (last_update_date, last_updated_by, creation_date, created_by,

     ticket_id, sent_by, send_date, description)

    values

    (now(), p_user_id, now(), p_user_id,

     o_ticket_id, p_user_id, now(), trim(both '\n' from p_description));

     

    set o_history_id = last_insert_id();

    

    

    set o_status = 1;

    select get_message_fc('TICKET_NEW_SUCCESS') into o_message;



    COMMIT;

  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `cc_ticket_reply_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), OUT `o_history_id` INT, IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_ticket_id` INT(10) UNSIGNED, IN `p_description` VARCHAR(3000), IN `p_is_mtc` ENUM('Y','N'), IN `p_mtc_cost` DOUBLE(15,2), IN `p_is_confirmation` ENUM('Y','N'), IN `p_reply_type` VARCHAR(10))  BEGIN
  
  declare t_success smallint; 
  
  declare t_push_id int;
  declare t_status varchar(30);
  declare t_ticket_num varchar(20);
  declare t_fcm_token varchar(300);
  declare t_user_id int;
  
  declare t_ticket_type varchar(20);
  
  declare exit handler for sqlexception
  begin
    ROLLBACK;
    set o_history_id = 0;
    set o_status = -1;
    select get_message_fc('TROUBLE') into o_message;
  end;  

  if access_token_fc(p_access_token, p_user_id) = 'Y' then

    -- cari ticket_type
    select  ticket_type
    into    t_ticket_type
    from    cc_ticket
    where   ticket_id = p_ticket_id;

    START TRANSACTION;
    
    if p_reply_type = 'user' then
      set t_status = 'user_response';
    else
      set t_status = 'admin_response';
    end if;
    
    insert into cc_ticket_history
    (last_update_date, last_updated_by, creation_date, created_by,
     ticket_id, sent_by, send_date, description, is_mtc, mtc_cost)
    values
    (now(), p_user_id, now(), p_user_id,
     p_ticket_id, p_user_id, now(), trim(both '\n' from p_description), p_is_mtc, p_mtc_cost);
     
    set o_history_id = last_insert_id();
    

    update  cc_ticket
    set     status = t_status,
            status_date = now(),
            status_by = p_user_id,
            is_confirmation = if(p_reply_type = 'user', 'N', p_is_confirmation)
    where   ticket_id = p_ticket_id;    
    
    
    if p_is_mtc = 'Y' then
    
      update  cc_ticket
      set     is_mtc = p_is_mtc,
              mtc_detail = trim(both '\n' from p_description),
              mtc_cost = p_mtc_cost,
              status = 'request_approval',
              status_date = now(),
              status_by = p_user_id
      where   ticket_id = p_ticket_id;
      
    end if;
    
    if p_reply_type <> 'user' then
    
      -- update last_admin_respon
      update  cc_ticket
      set     last_admin_respon = now()
      where   ticket_id = p_ticket_id;
      
      -- cari data + fcm_token
      select  ut.user_id, ct.ticket_num, ut.fcm_token
      into    t_user_id, t_ticket_num, t_fcm_token
      from    cc_ticket ct
              join user_token ut
                on ct.request_by = ut.user_id
      where   ct.ticket_id = p_ticket_id;
      

      -- INBOX
      insert into inbox
      (last_update_date, last_updated_by, creation_date, created_by,
       inbox_date, user_id, msg_subject, msg_body, msg_type, source_id)
      values
      (now(), p_user_id, now(), p_user_id,
       now(), t_user_id, concat('Tiket #',t_ticket_num), trim(both '\n' from p_description), t_ticket_type, p_ticket_id);
      
      
      update  user_counter
      set     inbox_count = inbox_count + 1
      where   user_id = t_user_id;
    
    else
    
      select  ut.fcm_token
      into    t_fcm_token
      from    cc_ticket ct
              join cc_ticket_history cth
                on ct.ticket_id = cth.ticket_id
                   and ct.request_by <> cth.sent_by
              join user_token ut
                on cth.sent_by = ut.user_id
      where   ct.ticket_id = p_ticket_id
      order by cth.history_id desc
      limit 1;
    
    end if;
    
    insert into push_notifications
    (push_type, fcm_token)
    values
    (t_ticket_type, t_fcm_token);
  
    set t_push_id = last_insert_id();
    
    
    insert into push_notification_parameters
    (push_id, key_label, key_value)
    values
    (t_push_id, 'push_type', t_ticket_type),
    (t_push_id, 'ticket_id', p_ticket_id),
    (t_push_id, 'message', trim(both '\n' from p_description));   
    
    
    set o_status = 1;
    select get_message_fc('SAVE_SUCCESS') into o_message;

    COMMIT;
  else

    set o_status = -1;
    select get_message_fc('ACCESS_VIOLATION') into o_message;
  
  end if;
  
END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `cc_ticket_work_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_ticket_id` INT(10) UNSIGNED, IN `p_start_date` DATETIME, IN `p_vendor_name` VARCHAR(100))  BEGIN
  
  declare t_success smallint; 
  
  
  declare exit handler for sqlexception
  begin
    set o_status = -1;
    select get_message_fc('TROUBLE') into o_message;
  end;  

  if access_token_fc(p_access_token, p_user_id) = 'Y' then
      
    update  cc_ticket
    set     status = 'on_progress',
            status_date = now(),
            status_by = p_user_id,
            start_date = p_start_date,
            vendor_name = p_vendor_name            
    where   ticket_id = p_ticket_id;
          
    
    set o_status = 1;
    select get_message_fc('SAVE_SUCCESS') into o_message;

  else

    set o_status = -1;
    select get_message_fc('ACCESS_VIOLATION') into o_message;
  
  end if;
  
END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `change_password_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_old_password` VARCHAR(50), IN `p_new_password` VARCHAR(50))  BEGIN

  declare t_success smallint;
  
  declare exit handler for sqlexception
  begin
    set o_status = -1;
    select get_message_fc('TROUBLE') into o_message;
  end;
  
  if access_token_fc(p_access_token, p_user_id) = 'Y' then
    
    update  user_login_data
    set     user_password =  md5(p_new_password)
    where   user_id = p_user_id
            and user_password = md5(p_old_password);
  
    select row_count() into t_success;
    
    if t_success <= 0 then
      set o_status = -1;
      select get_message_fc('CHANGE_PASSWORD_FAILED') into o_message;  
    else
      set o_status = 1;
      select get_message_fc('CHANGE_PASSWORD_SUCCESS') into o_message;
    end if;
  else
  
    set o_status = -1;
    select get_message_fc('ACCESS_VIOLATION') into o_message;
  
  end if;
    
END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `inbox_del_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_inbox_id` INT UNSIGNED)  BEGIN   



  declare t_success smallint;



  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end; 

  

  if access_token_fc(p_access_token, p_user_id) = 'Y' then

        

    update  inbox

    set     is_active = 'N',

            last_update_date = now(),

            last_updated_by = p_user_id

    where   inbox_id = p_inbox_id;

    

    select row_count() into t_success;

    

    if t_success <= 0 then

      set o_status = -1;

      select get_message_fc('NOT_FOUND') into o_message;

    else   

      set o_status = 1;

      select get_message_fc('DELETE_SUCCESS') into o_message;

    end if;  

    

  else

    

    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

    

  end if;

     

END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `inbox_open_pc` (IN `p_user_id` INT)  BEGIN   



  update  user_counter

  set     inbox_count = 0

  where   user_id = p_user_id;

     

END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `inbox_read_pc` (IN `p_inbox_id` INT)  BEGIN   



  update  inbox

  set     is_read = 'Y'

  where   inbox_id = p_inbox_id;

     

END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `midtrans_response_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_id_payment` INT(10) UNSIGNED, IN `p_status_id` INT(10), IN `p_status_code` INT(11), IN `p_transaction_id` VARCHAR(500), IN `p_transaction_status` VARCHAR(50), IN `p_transaction_time` DATETIME, IN `p_midtrans_payment_type` VARCHAR(50), IN `p_bank_name` VARCHAR(50), IN `p_va_number` VARCHAR(50), IN `p_bill_key` VARCHAR(50), IN `p_biller_code` VARCHAR(50), IN `p_payment_code` VARCHAR(50))  BEGIN

  

  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then

       

    update  transaksi_payment

    set     status_id = p_status_id,

            status_code = p_status_code,

            transaction_id = p_transaction_id,

            transaction_status = p_transaction_status,

            transaction_time = p_transaction_time,

            midtrans_payment_type = p_midtrans_payment_type,

            bank_name = p_bank_name,

            va_number = p_va_number,

            bill_key = p_bill_key,

            biller_code = p_biller_code,

            payment_code = p_payment_code

    where   id_payment = p_id_payment;

    

    

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `news_del_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_news_id` INT UNSIGNED)  BEGIN

  

  declare t_success smallint;

  declare t_valid varchar(1);



  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end; 

  

  if access_token_fc(p_access_token, p_user_id) = 'Y' then

    

    update  news

    set     is_active = 'N',

            last_update_date = now(),

            last_updated_by = p_user_id

    where   news_id = p_news_id

            and is_active = 'Y';

    

    select row_count() into t_success;

    

    if t_success <= 0 then

      set o_status = -1;

      select get_message_fc('NOT_FOUND') into o_message;

    else   

      set o_status = 1;

      select get_message_fc('DELETE_SUCCESS') into o_message;

    end if;  



  else

    

    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

    

  end if;

END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `news_save_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), OUT `o_news_id` INT(10) UNSIGNED, IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_news_id` INT UNSIGNED, IN `p_news_type` VARCHAR(30), IN `p_subject` VARCHAR(200), IN `p_description` VARCHAR(5000), IN `p_start_date` DATE, IN `p_end_date` DATE, IN `p_file_name` VARCHAR(200), IN `p_is_banner` VARCHAR(1))  BEGIN
  
  declare t_success smallint; 
  
  declare t_level	smallint(6);
  
  declare exit handler for sqlexception
  begin
    set o_news_id = p_news_id;
    set o_status = -1;
    select get_message_fc('TROUBLE') into o_message;
  end;  

  if access_token_fc(p_access_token, p_user_id) = 'Y' then
      
    if p_news_id > 0 then
    
      update  news
      set     news_type = p_news_type,
              subject = p_subject,
              description = p_description,
              start_date = p_start_date,
              end_date = p_end_date,
              file_name = if(length(p_file_name) > 0, p_file_name, file_name),
              is_banner = p_is_banner,
              last_update_date = now(),
              last_updated_by = p_user_id
      where   news_id = p_news_id
              and is_active = 'Y';
                    
      select row_count() into t_success;
    
      if t_success <= 0 then
        set o_news_id = p_news_id;
        set o_status = -1;
        select get_message_fc('NOT_FOUND') into o_message;
      else      
        set o_news_id = p_news_id;
        set o_status = 1;
        select get_message_fc('SAVE_SUCCESS') into o_message;
      end if;                       
                      
    else
    
      insert into news
      (last_update_date, last_updated_by, creation_date, created_by,
      news_type, subject, description, start_date, end_date, file_name, is_banner)
      values
      (now(), p_user_id, now(), p_user_id,
      p_news_type, p_subject, p_description, p_start_date, p_end_date, p_file_name, p_is_banner);
      
      set o_news_id = last_insert_id();
                        
      set o_status = 1;
      select get_message_fc('SAVE_SUCCESS') into o_message;

    end if;

  else

    set o_status = -1;
    select get_message_fc('ACCESS_VIOLATION') into o_message;
  
  end if;
  
END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `payment_paid_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_id_payment` INT(10) UNSIGNED)  BEGIN

  declare t_user_id int;

  declare t_payment_id varchar(45);

  declare t_total_bayar double;

  

  declare t_ticket_id int;

  declare t_id_trans	bigint(10);

  declare t_type_trans	int(10);



  declare t_last_flag tinyint default 0;

  

  declare detail_cur cursor for

    select  tpd.id_trans, tpd.type_trans

    from    transaksi_payment_detail tpd

    where   tpd.is_active = 'Y'

            and tpd.id_payment = p_id_payment;

 

  declare continue handler for sqlstate '02000' set t_last_flag = 1;    



  

  declare exit handler for sqlexception

  begin

    ROLLBACK;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then

       

    START TRANSACTION;    

    

    -- cari user_id

    select  user_id, payment_id, total + biaya_transaksi

    into    t_user_id, t_payment_id, t_total_bayar

    from    transaksi_payment

    where   id_payment = p_id_payment;

    

       

    update  transaksi_payment

    set     status_id = 2, -- paid / settlement / capture

            date_paid = now()

    where   id_payment = p_id_payment;

    

    

    -- INBOX

    insert into inbox

    (last_update_date, last_updated_by, creation_date, created_by,

     inbox_date, user_id, msg_subject, msg_body, 

     msg_type, source_id)

    values

    (now(), p_user_id, now(), p_user_id,

     now(), t_user_id, 'Pembayaran Berhasil', concat('Pembayaran untuk No Transaksi ',t_payment_id,' Berhasil'), 

     'payment_paid', p_id_payment);



    

    update  user_counter

    set     inbox_count = inbox_count + 1

    where   user_id = t_user_id;

    

    

    set t_last_flag = 0;

    open detail_cur;

    repeat

      fetch detail_cur into t_id_trans, t_type_trans;   



      if t_last_flag <> 1 then

      

        if t_type_trans = 1 then -- ipl

        

          update  transaksi_ipl

          set     status_id = 2, -- paid / settlement / capture

                  date_paid = now()

          where   id_trans = t_id_trans;

                

--         else if t_type_trans = 2 then -- retail

      

        elseif t_type_trans = 3 then -- handyman



          update  transaksi_retail

          set     status_id = 2, -- paid / settlement / capture

                  date_paid = now()

          where   id_trans = t_id_trans;

          

          -- cari ticket_id

          select  item_id

          into    t_ticket_id

          from    transaksi_retail_detail

          where   id_trans = t_id_trans;

          

          update  cc_ticket

          set     status = 'paid',

                  status_date = now(),

                  status_by = p_user_id,

                  is_confirmation = 'Y'

          where   ticket_id = t_ticket_id;



        end if;

                

      end if;

      

    until t_last_flag = 1 end repeat;

    close detail_cur;  

    

        

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



    COMMIT;

  else

  

    ROLLBACK;

    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `push_notification_sent_pc` (IN `p_push_id` INT UNSIGNED)  BEGIN



  update  push_notifications

  set     is_sent = 'Y',

          send_date = now(),

          is_active = 'N'

  where   push_id = p_push_id;     



END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `transaksi_payment_detail_save_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), OUT `o_id_payment_detail` INT(10) UNSIGNED, IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_id_payment` BIGINT(10), IN `p_id_trans` BIGINT(10), IN `p_type_trans` INT(10), IN `p_total` DOUBLE)  BEGIN

  

  declare t_exist varchar(1) default 'N';

  declare t_payment_id varchar(45);  

  

  declare exit handler for sqlexception

  begin

    ROLLBACK;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then

       

    START TRANSACTION;       

       

    -- cek exist atau tidak

    select  'Y'

    into    t_exist

    from    transaksi_payment_detail

    where   is_active = 'N'

            and type_trans = p_type_trans

            and id_trans = p_id_trans;

           

       

    if ifnull(t_exist, 'N') = 'N' then

      -- cari payment_id

      select  payment_id

      into    t_payment_id

      from    transaksi_payment

      where   id_payment = p_id_payment;

        

      insert into transaksi_payment_detail

      (last_update_date, last_updated_by, creation_date, created_by,    

      id_payment, id_trans, type_trans, total)

      values

      (now(), p_user_id, now(), p_user_id,

      p_id_payment, p_id_trans, p_type_trans, p_total);

      

      set o_id_payment_detail = last_insert_id();

      

      if (p_type_trans = 1) then -- IPL

      

        update  transaksi_ipl

        set     payment_id = t_payment_id,

                status_id = 1 -- pending

        where   id_trans = p_id_trans;

        

      elseif (p_type_trans in (2, 3)) then -- Handyman



        update  transaksi_retail

        set     payment_id = t_payment_id,

                status_id = 1 -- pending

        where   id_trans = p_id_trans;



      end if;

      

      set o_status = 1;

      select get_message_fc('SAVE_SUCCESS') into o_message;

      

    end if;

    

    COMMIT;



  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `transaksi_payment_save_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), OUT `o_id_payment` INT(10) UNSIGNED, IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_unique_code` VARCHAR(50), IN `p_payment_id` VARCHAR(45), IN `p_total` DOUBLE, IN `p_payment_type` INT(11), IN `p_biaya_transaksi` DOUBLE)  BEGIN



  declare t_id_payment int;

  declare t_payment_id varchar(45);

  declare t_total_bayar double;

  

  

  declare exit handler for sqlexception

  begin

    ROLLBACK;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then

  

    START TRANSACTION;

  

    -- cari unique_code sudah ada atau belum

    select  id_payment, payment_id, total + biaya_transaksi

    into    t_id_payment, t_payment_id, t_total_bayar

    from    transaksi_payment

    where   unique_code = p_unique_code;

  

    if ifnull(t_id_payment,0) > 0 then

    

      update  transaksi_payment

      set     total = p_total, 

              payment_type = p_payment_type, 

              biaya_transaksi = p_biaya_transaksi,

              last_update_date = now(),

              last_updated_by = p_user_id

      where   id_payment = t_id_payment;

    

      set o_id_payment = t_id_payment;

      

    else

       

      insert into transaksi_payment

      (last_update_date, last_updated_by, creation_date, created_by,    

      unique_code, date_issued, user_id, payment_id, total, payment_type, biaya_transaksi)

      values

      (now(), p_user_id, now(), p_user_id,

      p_unique_code, now(), p_user_id, p_payment_id, p_total, p_payment_type, p_biaya_transaksi);

    

      set o_id_payment = last_insert_id();

      set t_payment_id = p_payment_id;

    

    end if;

    

    -- INBOX

    insert into inbox

    (last_update_date, last_updated_by, creation_date, created_by,

     inbox_date, user_id, msg_subject, msg_body, 

     msg_type, source_id)

    values

    (now(), p_user_id, now(), p_user_id,

     now(), p_user_id, 'Belum Terbayar', concat('Mohon lakukan pembayaran untuk No Transaksi ',t_payment_id), 

     'payment_unpaid', o_id_payment);

    



    update  user_counter

    set     inbox_count = inbox_count + 1

    where   user_id = p_user_id;

    

    

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



    COMMIT;

  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `transaksi_retail_detail_save_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_id_trans` BIGINT(10), IN `p_item_id` BIGINT(10), IN `p_item_qty` DOUBLE, IN `p_item_price` DOUBLE)  BEGIN



  declare t_item_hpp double;

  

  declare exit handler for sqlexception

  begin

    ROLLBACK;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then

  

    -- cari hpp

    select  item_hpp

    into    t_item_hpp

    from    master_item

    where   item_id = p_item_id;

  

    START TRANSACTION;

     

    insert into transaksi_retail_detail

    (last_update_date, last_updated_by, creation_date, created_by,    

    id_trans, item_id, item_qty, item_hpp, item_price, subtotal)

    values

    (now(), p_user_id, now(), p_user_id,

    p_id_trans, p_item_id, p_item_qty, ifnull(t_item_hpp,0), p_item_price, p_item_qty * p_item_price);

  

    

    update  cart

    set     is_active = 'N',

            last_update_date = now(),

            last_updated_by = p_user_id

    where   is_active = 'Y'

            and user_id = p_user_id

            and item_id = p_item_id;

    

    

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



    COMMIT;

  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `transaksi_retail_save_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), OUT `o_id_trans` INT(10) UNSIGNED, IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_type_trans` INT(10), IN `p_kavling_id` BIGINT(10), IN `p_nominal` DOUBLE)  BEGIN

  

  declare exit handler for sqlexception

  begin

    ROLLBACK;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then

  

    START TRANSACTION;

     

    insert into transaksi_retail

    (last_update_date, last_updated_by, creation_date, created_by,    

    type_trans, date_issued, kavling_id, user_id, nominal)

    values

    (now(), p_user_id, now(), p_user_id,

    p_type_trans, now(), p_kavling_id, p_user_id, p_nominal);

  

    set o_id_trans = last_insert_id();

    

    

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



    COMMIT;

  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `update_user_token_pc` (IN `p_access_token` VARCHAR(50), IN `p_user_id` INT UNSIGNED, IN `p_token_type` VARCHAR(20), IN `p_token` VARCHAR(300))  BEGIN   

  if access_token_fc(p_access_token, p_user_id) = 'Y' then

    

    if p_token_type = 'FCM' then

    

      update  user_token

      set     fcm_token = p_token

      where   user_id = p_user_id;

    

    end if;

    

  end if;

END$$

CREATE DEFINER=`u1311857`@`localhost` PROCEDURE `user_login_pc` (OUT `o_status` TINYINT, OUT `o_message` VARCHAR(200), OUT `o_access_token` VARCHAR(50), OUT `o_user_id` INT UNSIGNED, OUT `o_user_name` VARCHAR(30), OUT `o_user_full_name` VARCHAR(20), OUT `o_user_id_type` TINYINT, OUT `o_kavling_id` INT UNSIGNED, OUT `o_kompleks_id` INT UNSIGNED, OUT `o_kompleks_name` VARCHAR(200), OUT `o_kompleks_address` VARCHAR(200), OUT `o_blok_name` VARCHAR(200), OUT `o_house_no` VARCHAR(200), IN `p_user_name` VARCHAR(50), IN `p_user_password` VARCHAR(100), IN `p_group_type` INT)  BEGIN

  declare t_user_id int unsigned default 0;
  declare t_user_name varchar(30);
  declare t_user_full_name varchar(50);
  declare t_user_id_type tinyint;
  declare t_kavling_id int;
  declare t_kompleks_id int;
  declare t_kompleks_name varchar(200);
  declare t_kompleks_address varchar(200);
  declare t_blok_name varchar(200);
  declare t_house_no varchar(200);
  declare t_access_token varchar(50);

  
  declare exit handler for sqlexception
  begin
    set o_status = -1;
    select get_message_fc('TROUBLE') into o_message;
  end;
  
  begin
    select  uld.user_id, uld.user_name, uld.user_full_name, uld.user_id_type,
            uk.kavling_id, mkav.kompleks_id, mkom.kompleks_name, mkom.kompleks_address, 
            mb.blok_name, mkav.house_no, ut.access_token
    into    t_user_id, t_user_name, t_user_full_name, t_user_id_type, 
            t_kavling_id, t_kompleks_id, t_kompleks_name, t_kompleks_address, 
            t_blok_name, t_house_no, t_access_token
    from    user_login_data uld
            join user_token ut
              on uld.user_id = ut.user_id
            join master_group mg
              on uld.group_id = mg.group_id               
            left join user_kavling uk
              on uld.user_id = uk.user_id
                 and uk.is_active = 'Y'
            left join master_kavling mkav
              on uk.kavling_id = mkav.kavling_id
            left join master_blok mb
              on mkav.blok_id = mb.blok_id
            left join master_kompleks mkom
              on mkav.kompleks_id = mkom.kompleks_id
    where   upper(trim(uld.user_name)) = upper(trim(p_user_name))
            and uld.user_password = md5(p_user_password)
            and mg.group_type = p_group_type
            and uld.is_active = 'Y'
    limit 1;
  end;
  
  if ifnull(t_user_id, 0) <= 0 then
    
    set o_status = -1;
    select get_message_fc('LOGIN_FAILED') into o_message;
    set o_user_id = 0;
    set o_access_token = null;
  
  else    

    set o_status = 1;
    set o_message = null;
    set o_access_token = t_access_token;
    set o_user_id = t_user_id;
    set o_user_name = t_user_name;
    set o_user_full_name = t_user_full_name;
    set o_user_id_type = t_user_id_type;
    set o_kavling_id = t_kavling_id;
    set o_kompleks_id = t_kompleks_id;
    set o_kompleks_name = t_kompleks_name;
    set o_kompleks_address = t_kompleks_address;
    set o_blok_name = t_blok_name;
    set o_house_no = t_house_no;

  end if;

END$$

--
-- Functions
--
CREATE DEFINER=`u1311857`@`localhost` FUNCTION `access_token_fc` (`p_access_token` VARCHAR(50), `p_user_id` INT UNSIGNED) RETURNS VARCHAR(1) CHARSET latin1 BEGIN

  

  declare t_val varchar(1) default 'N';



  begin

    select  'Y'

    into    t_val

    from    user_login_data uld

            join user_token ut

              on uld.user_id = ut.user_id

    where   uld.user_id = p_user_id

            and ut.access_token = p_access_token

            and uld.is_active = 'Y';

  end;

  

  set t_val = ifnull(t_val,'N');

  

	RETURN t_val;

END$$

CREATE DEFINER=`u1311857`@`localhost` FUNCTION `get_attach_file` (`p_history_id` INT, `p_seqno` TINYINT) RETURNS VARCHAR(200) CHARSET latin1 BEGIN

  

  declare t_val varchar(200);



  begin

    select  attach_file

    into    t_val

    from    cc_ticket_history_attach

    where   is_active = 'Y'

            and history_id = p_history_id

            and seqno = p_seqno;

  end;

  

	RETURN t_val;

END$$

CREATE DEFINER=`u1311857`@`localhost` FUNCTION `get_message_fc` (`p_code` VARCHAR(50)) RETURNS VARCHAR(200) CHARSET latin1 BEGIN

  

  declare t_message varchar(200);



  begin

    select  message

    into    t_message

    from    messages

    where   language_code = 'in'

            and code = p_code;

  end;

  

	RETURN t_message;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `cart_type` enum('shop','drugs') DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `creation_date` datetime DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_id`, `user_id`, `cart_type`, `item_id`, `qty`, `is_active`, `creation_date`, `created_by`, `last_update_date`, `last_updated_by`) VALUES
(3, 2, 'drugs', 5, 1, 'Y', '2021-08-24 12:30:54', 2, '2021-08-24 12:30:54', 2),
(4, 2, 'shop', 4, 4, 'N', '2021-08-24 12:30:58', 2, '2021-08-24 12:31:32', 2),
(5, 2, 'shop', 1, 1, 'N', '2021-08-24 12:31:02', 2, '2021-08-24 12:31:32', 2),
(6, 2, 'shop', 3, 1, 'N', '2021-08-24 12:35:51', 2, '2021-08-24 12:35:55', 2),
(7, 2, 'shop', 3, 6, 'N', '2021-08-24 13:49:22', 2, '2021-09-06 23:51:42', 2),
(8, 2, 'shop', 4, 7, 'Y', '2021-08-24 13:49:33', 2, '2021-08-24 13:49:33', 2),
(9, 2, 'drugs', 2, 1, 'Y', '2021-08-25 18:27:59', 2, '2021-08-25 18:27:59', 2),
(10, 2, 'drugs', 20, 1, 'Y', '2021-09-15 22:08:18', 2, '2021-09-15 22:08:18', 2),
(11, 2, 'drugs', 18, 1, 'Y', '2021-09-20 16:57:49', 2, '2021-09-20 16:57:49', 2),
(12, 2, 'shop', 13, 1, 'Y', '2021-09-20 16:58:38', 2, '2021-09-20 16:58:38', 2),
(13, 2, 'shop', 12, 1, 'N', '2021-09-20 16:58:57', 2, '2021-09-20 16:59:29', 2);

-- --------------------------------------------------------

--
-- Table structure for table `cc_category`
--

CREATE TABLE `cc_category` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(100) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_category`
--

INSERT INTO `cc_category` (`category_id`, `category_name`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 'Customer Care', 'Y', NULL, NULL, NULL, NULL),
(2, 'Perbaikan Rumah', 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cc_ticket`
--

CREATE TABLE `cc_ticket` (
  `ticket_id` int(10) UNSIGNED NOT NULL,
  `ticket_num` varchar(20) DEFAULT NULL,
  `ticket_date` datetime NOT NULL,
  `request_by` int(10) UNSIGNED NOT NULL,
  `kavling_id` int(11) UNSIGNED DEFAULT NULL,
  `kompleks_id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(200) DEFAULT NULL,
  `ticket_type` enum('cs','repair') DEFAULT NULL,
  `description` varchar(3000) DEFAULT NULL,
  `is_mtc` enum('Y','N') DEFAULT 'N',
  `mtc_detail` varchar(3000) DEFAULT NULL,
  `mtc_cost` double(15,2) DEFAULT NULL,
  `mtc_attach_file` varchar(20) DEFAULT NULL,
  `mtc_approval` varchar(15) DEFAULT NULL,
  `mtc_approval_date` datetime DEFAULT NULL,
  `status` enum('submit','closed','admin_response','user_response','request_approval','approved','rejected','on_progress','unpaid','paid') DEFAULT NULL,
  `status_date` datetime DEFAULT NULL,
  `status_by` int(11) DEFAULT NULL,
  `last_admin_respon` datetime DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `vendor_name` varchar(100) DEFAULT NULL,
  `finish_date` datetime DEFAULT NULL,
  `is_confirmation` enum('Y','N') DEFAULT 'N',
  `is_closed` enum('Y','N') DEFAULT 'N',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_ticket`
--

INSERT INTO `cc_ticket` (`ticket_id`, `ticket_num`, `ticket_date`, `request_by`, `kavling_id`, `kompleks_id`, `subject`, `ticket_type`, `description`, `is_mtc`, `mtc_detail`, `mtc_cost`, `mtc_attach_file`, `mtc_approval`, `mtc_approval_date`, `status`, `status_date`, `status_by`, `last_admin_respon`, `start_date`, `vendor_name`, `finish_date`, `is_confirmation`, `is_closed`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 'MS21070001', '2021-07-23 12:08:11', 2, 2, 1, 'Rumah Bocor', 'repair', 'Kamar Depan bocor saat hujan lebat kemarin. Air merembes di dinding\n\nMohon untuk kirim tukang untuk pengecekan.\n\nTerima Kasih.', 'Y', 'Selamat Sore, \r\n\r\nDari hasil pengecekan dilapangan, ada biaya perbaikan sebesar Rp 320.000 dengan rincian sebagai berikut: \r\n- Aquaproof: 200.000\r\n- Serat Fiber: 20.000\r\n- Jasa: 100.000\r\n\r\nTerima kasih', 320000.00, NULL, NULL, NULL, 'closed', '2021-08-18 00:00:00', 1, NULL, NULL, NULL, NULL, 'N', 'Y', 'N', 2, '2021-07-23 12:08:11', 1, '2021-08-18 20:58:40'),
(2, 'MS21070002', '2021-07-23 13:53:43', 2, 2, 1, 'Sampah 3 hari tidak diambil', 'cs', 'Sampah Rumah saya sudah 3 hari tidak diambil, bau busuk. Mohon diperhatikan mengenai petugas kebersihan.', 'N', 'aaaaaaaa bbbbb', 500.00, NULL, NULL, NULL, 'request_approval', '2021-08-24 00:00:00', 1, NULL, NULL, NULL, NULL, 'N', 'N', 'Y', 2, '2021-07-23 13:53:43', 1, '2021-08-24 07:57:13'),
(3, 'MS21070003', '2021-07-23 16:46:47', 2, 2, 1, 'Saluran PDAM Mati', 'cs', 'Saluran PDAM dari kemaren malam mati, sampai sekarang masih belum nyala. mohon dibantu cek.\n\nTq', 'N', NULL, NULL, NULL, NULL, NULL, 'submit', NULL, 2, NULL, NULL, NULL, NULL, 'N', 'N', 'Y', 2, '2021-07-23 16:46:47', 2, '2021-07-23 16:46:47'),
(4, 'MS21070004', '2021-07-23 16:49:39', 2, 2, 1, 'Jalan Berlubang', 'cs', 'Jalan di depan gang 3 berlubang, membahayakan warga', 'N', 'perbaikan', 150000.00, NULL, NULL, NULL, 'request_approval', '2021-08-13 00:00:00', 1, NULL, NULL, NULL, NULL, 'N', 'N', 'Y', 2, '2021-07-23 16:49:39', 1, '2021-08-13 23:39:09'),
(5, 'MS21080001', '2021-08-23 00:33:47', 2, 2, 1, 'Perbaikan Taman', 'cs', 'Taman mints ditata ulang', 'N', NULL, NULL, NULL, NULL, NULL, 'user_response', '2021-08-23 00:38:26', 2, NULL, NULL, NULL, NULL, 'N', 'N', 'Y', 2, '2021-08-23 00:33:47', 2, '2021-08-23 00:33:47'),
(6, 'MS21080002', '2021-08-24 00:44:45', 2, 2, 1, 'Jendela tidak bisa ditutup', 'cs', 'jendela kamar tidak bisa ditutup', 'N', NULL, NULL, NULL, NULL, NULL, 'admin_response', '2021-09-02 22:10:30', 1, NULL, NULL, NULL, NULL, 'N', 'N', 'Y', 2, '2021-08-24 00:44:45', 2, '2021-08-24 00:44:45'),
(7, 'MS21080003', '2021-08-24 22:02:18', 2, 2, 1, 'Genteng bocor', 'cs', 'genteng bocor', 'Y', 'ada biaya perbaikan', 500000.00, NULL, NULL, NULL, 'request_approval', '2021-09-03 20:00:49', 1, NULL, NULL, NULL, NULL, 'N', 'N', 'Y', 2, '2021-08-24 22:02:18', 2, '2021-08-24 22:02:18'),
(8, 'MS21080004', '2021-08-24 22:05:30', 2, 2, 1, 'Genteng bocor bagian samping', 'repair', 'Genteng bocor bagian samping', 'N', NULL, NULL, NULL, NULL, NULL, 'user_response', '2021-09-19 09:18:09', 2, NULL, NULL, NULL, NULL, 'N', 'N', 'Y', 2, '2021-08-24 22:05:30', 2, '2021-08-24 22:05:30'),
(9, 'MS21080005', '2021-08-24 22:25:19', 2, 2, 1, 'Cat Garasi', 'repair', 'ngecat garasi seluruh nya', 'N', NULL, NULL, NULL, NULL, NULL, 'submit', '2021-08-24 22:25:19', 2, NULL, NULL, NULL, NULL, 'N', 'N', 'Y', 2, '2021-08-24 22:25:19', 2, '2021-08-24 22:25:19'),
(10, 'MS21080006', '2021-08-24 22:29:17', 2, 2, 1, 'xxxx', 'repair', 'xxxx', 'N', NULL, NULL, NULL, NULL, NULL, 'submit', '2021-08-24 22:29:17', 2, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 2, '2021-08-24 22:29:17', 2, '2021-08-24 22:29:17'),
(11, 'MS21080007', '2021-08-24 22:30:00', 2, 2, 1, 'bbbbb', 'cs', 'bbbb', 'N', NULL, NULL, NULL, NULL, NULL, 'submit', '2021-08-24 22:30:00', 2, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 2, '2021-08-24 22:30:00', 2, '2021-08-24 22:30:00'),
(12, 'MS21080008', '2021-08-31 23:12:45', 2, 2, 1, 'Jam Dinding saya kurang bagus', 'cs', 'Bonus hadiah jam dari Perumahan kurang bagus. ', 'N', NULL, NULL, NULL, NULL, NULL, 'user_response', '2021-09-16 19:04:08', 2, NULL, NULL, NULL, NULL, 'N', 'N', 'Y', 2, '2021-08-31 23:12:45', 2, '2021-08-31 23:12:45'),
(13, 'MS21090001', '2021-09-11 01:12:20', 2, 2, 1, 'tes', 'repair', 'tesss', 'Y', 'ada biaya tukang', 75000.00, NULL, NULL, NULL, 'closed', '2021-09-11 02:41:05', 2, '2021-09-11 01:15:10', '2021-09-16 00:00:00', 'Rudi', '2021-09-11 02:41:05', 'Y', 'Y', 'Y', 2, '2021-09-11 01:12:20', 2, '2021-09-11 01:12:20'),
(14, 'MS21090002', '2021-09-16 12:15:57', 2, 2, 1, 'kapan fasum dibuka ?', 'cs', 'kapan fasum dibuka lagi?', 'N', NULL, NULL, NULL, NULL, NULL, 'user_response', '2021-09-17 21:15:37', 2, NULL, NULL, NULL, NULL, 'N', 'N', 'Y', 2, '2021-09-16 12:15:57', 2, '2021-09-16 12:15:57');

-- --------------------------------------------------------

--
-- Table structure for table `cc_ticket_history`
--

CREATE TABLE `cc_ticket_history` (
  `history_id` int(10) UNSIGNED NOT NULL,
  `ticket_id` int(10) UNSIGNED NOT NULL,
  `sent_by` int(11) DEFAULT NULL,
  `send_date` datetime DEFAULT NULL,
  `description` varchar(3000) DEFAULT NULL,
  `is_mtc` enum('Y','N') DEFAULT 'N',
  `mtc_cost` double(15,2) DEFAULT 0.00,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_ticket_history`
--

INSERT INTO `cc_ticket_history` (`history_id`, `ticket_id`, `sent_by`, `send_date`, `description`, `is_mtc`, `mtc_cost`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 1, 2, '2021-07-23 12:08:11', 'Kamar Depan bocor saat hujan lebat kemarin. Air merembes di dinding\n\nMohon untuk kirim tukang untuk pengecekan.\n\nTerima Kasih.', 'N', 0.00, 'Y', 2, '2021-07-23 12:08:11', 2, '2021-07-23 12:08:11'),
(2, 1, 1, '2021-07-23 12:41:28', 'Selamat Pagi, \r\n\r\nTim kami akan datang ke rumah Anda pada Sabtu, 23 Juli 2021 pukul 15.00 - 16.00 \r\n\r\n\r\nTerima kasih,\r\nAndini', 'N', NULL, 'Y', 1, '2021-07-23 12:41:28', 1, '2021-07-23 12:41:28'),
(3, 1, 1, '2021-07-23 12:45:13', 'Selamat Sore, \r\n\r\nDari hasil pengecekan dilapangan, ada biaya perbaikan sebesar Rp 320.000 dengan rincian sebagai berikut: \r\n- Aquaproof: 200.000\r\n- Serat Fiber: 20.000\r\n- Jasa: 100.000\r\n\r\nTerima kasih', 'Y', 320000.00, 'Y', 1, '2021-07-23 12:45:13', 1, '2021-08-13 23:41:32'),
(4, 2, 2, '2021-07-23 13:53:43', 'SAMPAH RUMAH SAYA SUDAH 3 HARI TIDAK DIAMBIL, BAU BUSUK. MOHON DIPERHATIKAN MENGENAI PETUGAS KEBERSIHAN.', 'N', 200.00, 'Y', 2, '2021-07-23 13:53:43', 1, '2021-08-13 22:29:50'),
(5, 3, 2, '2021-07-23 16:46:47', 'Saluran PDAM dari kemaren malam mati, sampai sekarang masih belum nyala. mohon dibantu cek.\n\nTq', 'N', 0.00, 'Y', 2, '2021-07-23 16:46:47', 2, '2021-07-23 16:46:47'),
(6, 4, 2, '2021-07-23 16:49:39', 'Jalan di depan gang 3 berlubang, membahayakan warga', 'N', 0.00, 'Y', 2, '2021-07-23 16:49:39', 2, '2021-07-23 16:49:39'),
(7, 2, 1, '2021-08-13 00:00:00', 'DARI ADMIN OMONG OK', 'Y', 3.00, 'Y', 1, '2021-08-13 22:30:04', 1, '2021-08-13 23:36:55'),
(8, 4, 1, '2021-08-13 00:00:00', 'perbaikan', 'Y', 150000.00, 'Y', 1, '2021-08-13 23:19:28', 1, '2021-08-13 23:39:09'),
(10, 2, 1, '2021-08-19 00:00:00', 'aaaaaaaa bbbbb', 'Y', 500.00, 'Y', 1, '2021-08-19 22:09:20', 1, '2021-08-24 07:57:13'),
(11, 5, 2, '2021-08-23 00:33:47', 'Taman mints ditata ulang', 'N', 0.00, 'Y', 2, '2021-08-23 00:33:47', 2, '2021-08-23 00:33:47'),
(12, 5, 2, '2021-08-23 00:38:26', 'pakai rumput gajah', 'N', 0.00, 'Y', 2, '2021-08-23 00:38:26', 2, '2021-08-23 00:38:26'),
(13, 6, 2, '2021-08-24 00:44:45', 'jendela kamar tidak bisa ditutup', 'N', 0.00, 'Y', 2, '2021-08-24 00:44:45', 2, '2021-08-24 00:44:45'),
(14, 6, 2, '2021-08-24 00:46:16', 'sekalian ganti kusen', 'N', 0.00, 'Y', 2, '2021-08-24 00:46:16', 2, '2021-08-24 00:46:16'),
(15, 7, 2, '2021-08-24 22:02:18', 'genteng bocor', 'N', 0.00, 'Y', 2, '2021-08-24 22:02:18', 2, '2021-08-24 22:02:18'),
(16, 8, 2, '2021-08-24 22:05:30', 'Genteng bocor bagian samping', 'N', 0.00, 'Y', 2, '2021-08-24 22:05:30', 2, '2021-08-24 22:05:30'),
(17, 8, 2, '2021-08-24 22:11:57', 'samping kiri', 'N', 0.00, 'Y', 2, '2021-08-24 22:11:57', 2, '2021-08-24 22:11:57'),
(18, 9, 2, '2021-08-24 22:25:19', 'ngecat garasi seluruh nya', 'N', 0.00, 'Y', 2, '2021-08-24 22:25:19', 2, '2021-08-24 22:25:19'),
(19, 10, 2, '2021-08-24 22:29:17', 'xxxx', 'N', 0.00, 'Y', 2, '2021-08-24 22:29:17', 2, '2021-08-24 22:29:17'),
(20, 11, 2, '2021-08-24 22:30:00', 'bbbb', 'N', 0.00, 'Y', 2, '2021-08-24 22:30:00', 2, '2021-08-24 22:30:00'),
(21, 12, 2, '2021-08-31 23:12:45', 'Bonus hadiah jam dari Perumahan kurang bagus. ', 'N', 0.00, 'Y', 2, '2021-08-31 23:12:45', 2, '2021-08-31 23:12:45'),
(22, 12, 1, '2021-09-01 00:46:38', 'beli baru', 'N', 0.00, 'Y', 1, '2021-09-01 00:46:38', 1, '2021-09-01 00:46:38'),
(23, 6, 1, '2021-09-02 22:03:13', 'lanjut', 'N', 0.00, 'Y', 1, '2021-09-02 22:03:13', 1, '2021-09-02 22:03:13'),
(24, 6, 1, '2021-09-02 22:10:30', 'tim kami akan melakukan pengecekan ke lapangan', 'N', 0.00, 'Y', 1, '2021-09-02 22:10:30', 1, '2021-09-02 22:10:30'),
(25, 7, 1, '2021-09-03 20:00:49', 'ada biaya perbaikan', 'Y', 500000.00, 'Y', 1, '2021-09-03 20:00:49', 1, '2021-09-03 20:00:49'),
(26, 13, 2, '2021-09-11 01:12:20', 'tesss', 'N', 0.00, 'Y', 2, '2021-09-11 01:12:20', 2, '2021-09-11 01:12:20'),
(27, 13, 1, '2021-09-11 01:12:48', 'ya kenapa ?', 'N', 0.00, 'Y', 1, '2021-09-11 01:12:48', 1, '2021-09-11 01:12:48'),
(28, 13, 2, '2021-09-11 01:14:08', 'genteng bocor', 'N', 0.00, 'Y', 2, '2021-09-11 01:14:08', 2, '2021-09-11 01:14:08'),
(29, 13, 1, '2021-09-11 01:15:10', 'ada biaya tukang', 'Y', 75000.00, 'Y', 1, '2021-09-11 01:15:10', 1, '2021-09-11 01:15:10'),
(30, 12, 2, '2021-09-15 20:23:33', 'okee siap', 'N', 0.00, 'Y', 2, '2021-09-15 20:23:33', 2, '2021-09-15 20:23:33'),
(31, 12, 2, '2021-09-15 20:26:35', 'baru harga berapa ya?', 'N', 0.00, 'Y', 2, '2021-09-15 20:26:35', 2, '2021-09-15 20:26:35'),
(32, 12, 2, '2021-09-15 20:30:00', 'halooo', 'N', 0.00, 'Y', 2, '2021-09-15 20:30:00', 2, '2021-09-15 20:30:00'),
(33, 12, 2, '2021-09-16 12:09:32', 'tes tess', 'N', 0.00, 'Y', 2, '2021-09-16 12:09:32', 2, '2021-09-16 12:09:32'),
(34, 14, 2, '2021-09-16 12:15:57', 'kapan fasum dibuka lagi?', 'N', 0.00, 'Y', 2, '2021-09-16 12:15:57', 2, '2021-09-16 12:15:57'),
(35, 12, 2, '2021-09-16 12:19:10', 'bagaimana?', 'N', 0.00, 'Y', 2, '2021-09-16 12:19:10', 2, '2021-09-16 12:19:10'),
(36, 12, 2, '2021-09-16 12:20:58', 'hmmm', 'N', 0.00, 'Y', 2, '2021-09-16 12:20:58', 2, '2021-09-16 12:20:58'),
(37, 12, 2, '2021-09-16 12:21:13', 'helooooo', 'N', 0.00, 'Y', 2, '2021-09-16 12:21:13', 2, '2021-09-16 12:21:13'),
(38, 12, 2, '2021-09-16 12:21:19', 'haiii', 'N', 0.00, 'Y', 2, '2021-09-16 12:21:19', 2, '2021-09-16 12:21:19'),
(40, 12, 2, '2021-09-16 19:04:08', 'yaaa', 'N', 0.00, 'Y', 2, '2021-09-16 19:04:08', 2, '2021-09-16 19:04:08'),
(41, 14, 2, '2021-09-16 22:27:43', 'boleh buat BBQ party? ', 'N', 0.00, 'Y', 2, '2021-09-16 22:27:43', 2, '2021-09-16 22:27:43'),
(42, 14, 2, '2021-09-17 20:25:43', '', 'N', 0.00, 'Y', 2, '2021-09-17 20:25:43', 2, '2021-09-17 20:25:43'),
(43, 14, 2, '2021-09-17 21:09:55', NULL, 'N', 0.00, 'Y', 2, '2021-09-17 21:09:55', 2, '2021-09-17 21:09:55'),
(44, 14, 2, '2021-09-17 21:15:37', NULL, 'N', 0.00, 'Y', 2, '2021-09-17 21:15:37', 2, '2021-09-17 21:15:37'),
(45, 8, 2, '2021-09-19 09:18:09', 'Gelap', 'N', 0.00, 'Y', 2, '2021-09-19 09:18:09', 2, '2021-09-19 09:18:09');

-- --------------------------------------------------------

--
-- Table structure for table `cc_ticket_history_attach`
--

CREATE TABLE `cc_ticket_history_attach` (
  `attach_id` int(10) UNSIGNED NOT NULL,
  `history_id` int(10) UNSIGNED DEFAULT NULL,
  `ticket_id` int(10) UNSIGNED DEFAULT NULL,
  `seqno` tinyint(4) DEFAULT NULL,
  `attach_file` varchar(100) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cc_ticket_history_attach`
--

INSERT INTO `cc_ticket_history_attach` (`attach_id`, `history_id`, `ticket_id`, `seqno`, `attach_file`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 1, 1, 1, 'd6c79c12e2b2a6456d847e4f82c0a69c.jpg', 'Y', 2, '2021-07-23 12:08:11', 2, '2021-07-23 12:08:11'),
(3, 1, 1, 3, '5ff6805378fea5b2a43d79771cd71d84.jpg', 'Y', 2, '2021-07-23 12:08:11', 2, '2021-07-23 12:08:11'),
(4, 1, 1, 4, 'ca64f3aa520e9e526bfd2464de26c853.jpg', 'Y', 2, '2021-07-23 12:08:11', 2, '2021-07-23 12:08:11'),
(9, 10, 2, 1, 'D8E7A1D3D084B88E635B642753007ED5.jpg', 'N', 1, '2021-08-19 23:01:57', NULL, NULL),
(10, 10, 2, 3, '594243A656FF9E181678F7D342248762.jpg', 'Y', 1, '2021-08-19 23:01:57', 1, '2021-08-24 07:57:13'),
(11, 12, 5, 1, 'bf0082b28fa68a93461968502cd495fd.jpg', 'Y', 2, '2021-08-23 00:38:26', 2, '2021-08-23 00:38:26'),
(12, 14, 6, 1, '797ee27c6ff37acd8e1085ebe59972cf.jpg', 'Y', 2, '2021-08-24 00:46:16', 2, '2021-08-24 00:46:16'),
(13, 17, 8, 1, '8a45e6e6c7f2c2b5cbdf71907fcc600c.jpg', 'Y', 2, '2021-08-24 22:11:57', 2, '2021-08-24 22:11:57'),
(14, 20, 11, 1, 'ddf9d099a505a9c9aebe09384cdb129b.jpg', 'Y', 2, '2021-08-24 22:30:00', 2, '2021-08-24 22:30:00'),
(15, 21, 12, 1, '433915edca160bc13d5fda660843373e.jpg', 'Y', 2, '2021-08-31 23:12:45', 2, '2021-08-31 23:12:45'),
(16, 21, 12, 2, '4d44754f40dfae8a13951512f097fb09.jpg', 'Y', 2, '2021-08-31 23:12:45', 2, '2021-08-31 23:12:45'),
(17, 22, 12, 1, '7a8f441c0faf9be1c33cb5fb822784fc.jpg', 'Y', 1, '2021-09-01 00:46:38', 1, '2021-09-01 00:46:38'),
(18, 23, 6, 1, 'c04feb5de989de8c71e6545e8fce2f7e.jpg', 'Y', 1, '2021-09-02 22:03:13', 1, '2021-09-02 22:03:13'),
(19, 42, 14, 1, '5f85f8ff33ee079fb7fae55c8d56c7fa.jpg', 'Y', 2, '2021-09-17 20:25:43', 2, '2021-09-17 20:25:43'),
(20, 43, 14, 1, 'd3a135c5ef99973f3892457744f28b74.jpg', 'Y', 2, '2021-09-17 21:09:55', 2, '2021-09-17 21:09:55'),
(21, 44, 14, 1, 'fa85a5b48e89605992a4523a919d6408.jpg', 'Y', 2, '2021-09-17 21:15:37', 2, '2021-09-17 21:15:37'),
(22, 45, 8, 1, '0b37e80f9d1ec7733c7fd860b6b9d7a7.jpg', 'Y', 2, '2021-09-19 09:18:09', 2, '2021-09-19 09:18:09');

-- --------------------------------------------------------

--
-- Table structure for table `inbox`
--

CREATE TABLE `inbox` (
  `inbox_id` int(10) UNSIGNED NOT NULL,
  `inbox_date` datetime DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `msg_subject` varchar(200) DEFAULT NULL,
  `msg_body` varchar(2000) DEFAULT NULL,
  `msg_type` varchar(20) DEFAULT NULL,
  `source_id` int(10) UNSIGNED DEFAULT NULL,
  `is_read` enum('Y','N') DEFAULT 'N',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inbox`
--

INSERT INTO `inbox` (`inbox_id`, `inbox_date`, `user_id`, `msg_subject`, `msg_body`, `msg_type`, `source_id`, `is_read`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(2, '2021-08-24 12:36:06', 2, 'Belum Terbayar', 'Mohon lakukan pembayaran untuk No Transaksi 20210824123606208', 'payment_unpaid', 3, 'Y', 'Y', 2, '2021-08-24 12:36:06', 2, '2021-08-24 12:36:06'),
(3, '2021-08-24 13:50:10', 2, 'Belum Terbayar', 'Mohon lakukan pembayaran untuk No Transaksi 20210824135010342', 'payment_unpaid', 4, 'Y', 'Y', 2, '2021-08-24 13:50:10', 2, '2021-08-24 13:50:10'),
(4, '2021-09-01 00:46:38', 2, 'Tiket #MS21080008', 'beli baru', 'repair', 12, 'N', 'Y', 1, '2021-09-01 00:46:38', 1, '2021-09-01 00:46:38'),
(5, '2021-09-02 22:10:30', 2, 'Tiket #MS21080002', 'tim kami akan melakukan pengecekan ke lapangan', 'repair', 6, 'Y', 'Y', 1, '2021-09-02 22:10:30', 1, '2021-09-02 22:10:30'),
(6, '2021-09-03 20:00:49', 2, 'Tiket #MS21080003', 'ada biaya perbaikan', 'repair', 7, 'N', 'Y', 1, '2021-09-03 20:00:49', 1, '2021-09-03 20:00:49'),
(7, '2021-09-11 01:12:48', 2, 'Tiket #MS21090001', 'ya kenapa ?', 'repair', 13, 'Y', 'Y', 1, '2021-09-11 01:12:48', 1, '2021-09-11 01:12:48'),
(8, '2021-09-11 01:15:10', 2, 'Tiket #MS21090001', 'ada biaya tukang', 'repair', 13, 'Y', 'Y', 1, '2021-09-11 01:15:10', 1, '2021-09-11 01:15:10'),
(9, '2021-09-11 01:31:25', 2, 'Belum Terbayar', 'Mohon lakukan pembayaran untuk No Transaksi 20210911013125760', 'payment_unpaid', 5, 'Y', 'Y', 2, '2021-09-11 01:31:25', 2, '2021-09-11 01:31:25'),
(10, '2021-09-11 01:31:42', 2, 'Pembayaran Berhasil', 'Pembayaran untuk No Transaksi 20210911013125760 Berhasil', 'payment_paid', 5, 'N', 'Y', 2, '2021-09-11 01:31:42', 2, '2021-09-11 01:31:42'),
(11, '2021-09-13 21:24:11', 2, 'Belum Terbayar', 'Mohon lakukan pembayaran untuk No Transaksi 20210913212411856', 'payment_unpaid', 6, 'Y', 'Y', 2, '2021-09-13 21:24:11', 2, '2021-09-13 21:24:11');

-- --------------------------------------------------------

--
-- Table structure for table `item_category`
--

CREATE TABLE `item_category` (
  `item_category_id` int(10) UNSIGNED NOT NULL,
  `item_category_name` varchar(45) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_category`
--

INSERT INTO `item_category` (`item_category_id`, `item_category_name`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 'Retail', 'Y', 0, NULL, 0, NULL),
(2, 'Drugs', 'Y', 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_bank`
--

CREATE TABLE `master_bank` (
  `bank_id` bigint(10) UNSIGNED NOT NULL,
  `bank_name` varchar(100) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` smallint(6) DEFAULT 0,
  `creation_datetime` datetime DEFAULT NULL,
  `last_update_by` smallint(6) DEFAULT 0,
  `last_updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_bank`
--

INSERT INTO `master_bank` (`bank_id`, `bank_name`, `is_active`, `created_by`, `creation_datetime`, `last_update_by`, `last_updated_date`) VALUES
(1, 'Bank Mandiri', 'Y', 1, NULL, 0, NULL),
(2, 'Bank Permata', 'Y', 1, NULL, 0, NULL),
(3, 'Bank BNI', 'Y', 1, NULL, 0, NULL),
(4, 'Bank BRI', 'Y', 1, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_blok`
--

CREATE TABLE `master_blok` (
  `blok_id` bigint(10) NOT NULL,
  `kompleks_id` bigint(10) DEFAULT 0,
  `blok_name` varchar(45) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_blok`
--

INSERT INTO `master_blok` (`blok_id`, `kompleks_id`, `blok_name`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 1, 'APEL', 'Y', 1, '2021-07-07 01:02:58', 1, '2021-08-24 07:17:28'),
(2, 1, 'jeruk', 'N', 1, '2021-07-07 01:02:58', 1, '2021-07-07 01:04:10'),
(3, 2, 'KAV 1', 'Y', 1, '2021-08-03 23:00:22', 1, '2021-08-03 23:00:37'),
(4, 2, 'KAV 2', 'N', 1, '2021-08-03 23:00:22', 1, '2021-08-03 23:00:37'),
(5, 1, 'PISANG', 'Y', 1, '2021-08-24 07:17:28', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_group`
--

CREATE TABLE `master_group` (
  `group_id` tinyint(3) UNSIGNED NOT NULL,
  `group_name` varchar(50) NOT NULL DEFAULT '',
  `group_description` varchar(50) NOT NULL DEFAULT '',
  `group_type` tinyint(3) DEFAULT 0,
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_group`
--

INSERT INTO `master_group` (`group_id`, `group_name`, `group_description`, `group_type`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 'ADMIN', 'GROUP ADMIN AAAA', 2, 'Y', NULL, NULL, 1, '2021-08-01 23:45:44'),
(2, 'LAND LORD', '', 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'RESIDENT', '', 0, 'Y', 1, '2021-07-20 23:54:54', NULL, NULL),
(7, 'AAA', 'AAAA', 0, 'Y', 1, '2021-08-01 23:48:49', NULL, NULL),
(11, 'ASDF', 'ASD', 0, 'Y', 1, '2021-08-01 23:53:16', NULL, NULL),
(12, 'GROUP ADMIN', '123', 2, 'N', 1, '2021-08-20 22:22:59', 1, '2021-08-20 22:23:05');

-- --------------------------------------------------------

--
-- Table structure for table `master_item`
--

CREATE TABLE `master_item` (
  `item_id` bigint(10) UNSIGNED NOT NULL,
  `item_name` varchar(100) DEFAULT '',
  `unit_id` smallint(6) DEFAULT 0,
  `item_hpp` double DEFAULT 0,
  `item_price` double DEFAULT 0,
  `qty` double DEFAULT 0,
  `item_jasa` enum('Y','N') DEFAULT 'N',
  `item_category_id` int(11) DEFAULT 0,
  `image_file` varchar(100) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_item`
--

INSERT INTO `master_item` (`item_id`, `item_name`, `unit_id`, `item_hpp`, `item_price`, `qty`, `item_jasa`, `item_category_id`, `image_file`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 'TABUNG GAS 12 KG', 1, 10000, 20000, 10, 'N', 1, '88A0A68D282AB3FBC27D246A786D6F3F.jpg', 'Y', 1, '2021-08-24 10:52:22', 1, '2021-08-24 11:36:21'),
(2, 'AQUA GALON 19 LITER', 1, 20000, 25000, 10, 'N', 1, '72F281695D43BBEF3FB143CACFDA6E94.jpg', 'Y', 1, '2021-08-24 10:55:19', 1, '2021-08-24 11:36:01'),
(3, 'INDOMIE GORENG', 1, 2200, 3000, 20, 'N', 1, '83CBF6580B70FFD7920E7B1390E67E4A.jpg', 'Y', 1, '2021-08-24 10:55:48', 1, '2021-08-24 11:36:11'),
(4, 'TABUNG GAS 3 KG', 1, 15000, 20000, 20, 'N', 1, '3B816CB9C78F1EB35182133505CA6D04.jpg', 'Y', 1, '2021-08-24 10:56:06', 1, '2021-08-24 11:36:32'),
(5, 'AQUA BOTOL 1,5 LITER', 1, 2000, 5000, 30, 'N', 1, '2D3B873C2408BA3225DB676AE13EA7FA.jpg', 'Y', 1, '2021-08-24 10:56:29', 1, '2021-09-01 22:03:29'),
(6, 'PANADOL BIRU KAPLET', 1, 13000, 20000, 10, 'N', 2, 'F7A36B40224ADBDBC3B8AFB874DD4176.jpg', 'Y', 1, '2021-08-24 11:14:19', 1, '2021-08-24 11:37:00'),
(7, 'TOLAK ANGIN BIRU KOTAK', 1, 25000, 30000, 10, 'N', 2, '3D1C7F81EF5C721CF40D545D2175EAC7.jpg', 'Y', 1, '2021-08-24 11:14:46', 1, '2021-08-24 11:37:28'),
(8, 'PANADOL HIJAU KAPLET', 1, 15000, 20000, 20, 'N', 2, '00C9B6A81867C4FAFA32BF61EE9A1FF0.jpg', 'Y', 1, '2021-08-24 11:15:09', 1, '2021-08-24 11:37:10'),
(9, 'REDOXON', 1, 45000, 50000, 20, 'N', 2, '6595A1750E3682654B5D30F7075E25F3.jpg', 'Y', 1, '2021-08-24 11:15:29', 1, '2021-08-24 11:37:19'),
(10, 'HANSAPLAST ISI 10', 1, 5000, 10000, 10, 'N', 2, '35F17BC098620AA8B253BB7E9E23A9DC.jpg', 'Y', 1, '2021-08-24 11:15:47', 1, '2021-08-24 11:36:49'),
(11, 'BENG BENG', 1, 100, 1000, 1, 'N', 1, 'C41501CCA7FB8CEC202F92BC1F1DA0FA.jpg', 'Y', 1, '2021-09-01 21:44:40', 0, NULL),
(12, 'CHITATO', 1, 5000, 10000, 100, 'N', 1, '8113508AA999D80C50F715626746E468.jpg', 'Y', 1, '2021-09-01 21:52:13', 1, '2021-09-14 09:02:33'),
(13, 'HANDUK', 1, 5000, 20000, 100, 'N', 1, '8C52895512AF68A99E337B85F9B7BA72.jpg', 'Y', 1, '2021-09-01 21:52:34', 1, '2021-09-14 09:03:48'),
(14, 'TISSUE PASEO', 1, 2000, 20000, 50, 'N', 1, '326C182FC22A923795A0C7168558EFDA.jpg', 'Y', 1, '2021-09-01 21:52:56', 1, '2021-09-14 09:04:20'),
(15, 'PULPEN', 3, 2000, 10000, 50, 'N', 1, 'D9DFF7645BD43899E02E8C2CCAD99C29.jpg', 'Y', 1, '2021-09-01 21:53:16', 1, '2021-09-14 09:04:03'),
(16, 'ANTANGIN', 3, 10000, 20000, 5, 'N', 2, '249B6B01B616DF9A30D9859BB1C620C3.jpg', 'Y', 1, '2021-09-01 21:53:45', 1, '2021-09-14 09:04:44'),
(17, 'VICKS VAPORUB', 1, 2000, 3000, 15, 'N', 2, 'ED680C4F3917BA68BD32B1C76FD57BFD.jpg', 'Y', 1, '2021-09-01 21:54:03', 0, NULL),
(18, 'KOYO HANSAPLAST', 3, 15000, 20000, 10, 'N', 2, '979F12133D7B13BFAE893B1AE4F8F740.jpg', 'Y', 1, '2021-09-01 21:54:27', 1, '2021-09-14 09:05:02'),
(19, 'MINYAK KAYU PUTIH 60ML', 1, 20000, 30000, 15, 'N', 2, 'B6E9E46A3091F053E7A81EF9CFA52706.jpg', 'Y', 1, '2021-09-01 21:54:46', 0, NULL),
(20, 'VICKS INHALER', 1, 32000, 50000, 10, 'N', 2, 'A301E932871875AFE6FAAFAC655DC773.jpg', 'Y', 1, '2021-09-01 21:55:32', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_kavling`
--

CREATE TABLE `master_kavling` (
  `kavling_id` bigint(10) NOT NULL,
  `kompleks_id` bigint(10) DEFAULT 0,
  `blok_id` bigint(10) DEFAULT 0,
  `house_no` varchar(45) DEFAULT '',
  `biaya_ipl` double DEFAULT 0,
  `durasi_pembayaran` int(10) DEFAULT 0,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) UNSIGNED DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_kavling`
--

INSERT INTO `master_kavling` (`kavling_id`, `kompleks_id`, `blok_id`, `house_no`, `biaya_ipl`, `durasi_pembayaran`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 1, 1, 'F0100', 250000, 2, 'Y', 1, '2021-07-07 23:26:38', 1, '2021-08-03 23:40:43'),
(2, 1, 1, '20', 20000, 3, 'Y', 1, '2021-07-07 23:36:52', 0, NULL),
(3, 2, 3, '55', 1500, 10, 'Y', 1, '2021-08-03 23:48:49', 0, NULL),
(4, 2, 3, 'A-1', 5000, 20, 'Y', 1, '2021-08-24 07:23:51', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_kompleks`
--

CREATE TABLE `master_kompleks` (
  `kompleks_id` bigint(10) UNSIGNED NOT NULL,
  `kompleks_name` varchar(50) DEFAULT '',
  `kompleks_address` varchar(200) DEFAULT '',
  `biaya_ipl` double DEFAULT 0,
  `durasi_pembayaran` int(10) DEFAULT 0,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) UNSIGNED DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_kompleks`
--

INSERT INTO `master_kompleks` (`kompleks_id`, `kompleks_name`, `kompleks_address`, `biaya_ipl`, `durasi_pembayaran`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 'SYAILENDRA', 'PURWOSARI 123', 20000, 3, 'Y', 1, '2021-07-07 01:02:58', 1, '2021-08-24 07:17:28'),
(2, 'PERMATA', 'AAAAAAAAAASSSSSS', 5000, 20, 'Y', 1, '2021-08-03 23:00:22', 1, '2021-08-03 23:00:37');

-- --------------------------------------------------------

--
-- Table structure for table `master_module`
--

CREATE TABLE `master_module` (
  `module_id` smallint(6) NOT NULL,
  `module_name` varchar(100) DEFAULT '',
  `module_features` tinyint(3) UNSIGNED DEFAULT 0,
  `module_active` tinyint(3) UNSIGNED DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_module`
--

INSERT INTO `master_module` (`module_id`, `module_name`, `module_features`, `module_active`) VALUES
(1, 'MANAJEMEN SISTEM', 1, 1),
(2, 'TAMBAH / HAPUS GROUP USER', 2, 1),
(3, 'TAMBAH / HAPUS USER', 2, 1),
(4, 'PENGATURAN AKSES MODUL', 1, 1),
(5, 'BACKUP / RESTORE DATABASE', 1, 0),
(6, 'PENGATURAN SISTEM APLIKASI', 1, 1),
(21, 'TAMBAH / HAPUS KOMPLEKS', 2, 1),
(22, 'TAMBAH / HAPUS KAVLING', 2, 1),
(23, 'TAMBAH / HAPUS ITEM RETAIL', 2, 1),
(24, 'TAMBAH / HAPUS SATUAN', 2, 1),
(25, 'TAMBAH / HAPUS PEMILIK KAVLING', 1, 1),
(26, 'TAMBAH / HAPUS ITEM DRUGS', 2, 1),
(27, 'TAMBAH / HAPUS NEWS', 2, 1),
(41, 'RINGKASAN IPL', 1, 1),
(42, 'TAMBAH / HAPUS TRANSAKSI RETAIL', 2, 1),
(43, 'PENYESUAIAN STOK RETAIL', 1, 1),
(44, 'PENERIMAAN BARANG RETAIL', 2, 1),
(45, 'TAMBAH / HAPUS TRANSAKSI DRUGS', 2, 1),
(46, 'PENYESUAIAN STOK DRUGS', 1, 1),
(47, 'PENERIMAAN BARANG DRUGS', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_module_access`
--

CREATE TABLE `master_module_access` (
  `id` bigint(20) NOT NULL,
  `group_id` tinyint(3) UNSIGNED NOT NULL,
  `module_id` smallint(6) NOT NULL,
  `user_access_option` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_module_access`
--

INSERT INTO `master_module_access` (`id`, `group_id`, `module_id`, `user_access_option`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 7),
(3, 1, 3, 7),
(4, 1, 4, 1),
(5, 1, 5, 1),
(6, 1, 6, 1),
(7, 1, 21, 7),
(8, 1, 22, 7),
(9, 1, 23, 7),
(10, 1, 24, 7),
(11, 1, 25, 1),
(12, 1, 41, 1),
(13, 1, 42, 7),
(14, 1, 43, 1),
(15, 1, 44, 7),
(16, 7, 1, 1),
(17, 7, 2, 7),
(18, 7, 3, 7),
(19, 7, 4, 1),
(20, 7, 6, 1),
(21, 7, 21, 7),
(22, 7, 22, 7),
(23, 7, 23, 7),
(24, 7, 24, 7),
(25, 7, 25, 1),
(26, 7, 41, 1),
(27, 7, 42, 7),
(28, 7, 43, 1),
(29, 7, 44, 7),
(30, 1, 26, 7),
(31, 1, 45, 7),
(32, 1, 46, 1),
(33, 1, 47, 7),
(34, 1, 27, 7);

-- --------------------------------------------------------

--
-- Table structure for table `master_payment_mode`
--

CREATE TABLE `master_payment_mode` (
  `payment_type` int(10) UNSIGNED NOT NULL,
  `seqno` tinyint(4) DEFAULT NULL,
  `payment_type_name` varchar(100) DEFAULT '',
  `payment_code` varchar(30) DEFAULT NULL,
  `bank` varchar(20) DEFAULT NULL,
  `cstore` varchar(20) DEFAULT NULL,
  `logo` varchar(30) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_payment_mode`
--

INSERT INTO `master_payment_mode` (`payment_type`, `seqno`, `payment_type_name`, `payment_code`, `bank`, `cstore`, `logo`, `is_active`) VALUES
(1, 1, 'BRI Virtual Account', 'bank_transfer', 'bri', NULL, 'bri', 'Y'),
(2, 4, 'Gopay', 'gopay', NULL, NULL, 'gopay', 'Y'),
(3, 5, 'ShopeePay', 'shopeepay', NULL, NULL, 'shopeepay', 'Y'),
(4, 6, 'Indomaret', 'cstore', NULL, 'indomaret', 'indomaret', 'Y'),
(5, 7, 'Alfa Group', 'cstore', NULL, 'alfamart', 'alfamart', 'Y'),
(6, 2, 'BNI Virtual Account', 'bank_transfer', 'bni', NULL, 'bni', 'Y'),
(7, 3, 'Mandiri Virtual Account', 'echannel', '', NULL, 'mandiri', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `master_unit`
--

CREATE TABLE `master_unit` (
  `unit_id` bigint(10) UNSIGNED NOT NULL,
  `unit_name` varchar(100) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_unit`
--

INSERT INTO `master_unit` (`unit_id`, `unit_name`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 'PCS', 'Y', 1, '2021-07-09 23:32:08', 0, NULL),
(2, 'KG', 'Y', 1, '2021-07-09 23:32:15', 1, '2021-07-09 23:40:01'),
(3, 'BOXA', 'Y', 1, '2021-07-09 23:40:15', 1, '2021-08-24 07:36:47'),
(4, 'JAM', 'Y', 1, '2021-07-28 12:05:20', 0, NULL),
(5, 'HARI AAAA', 'Y', 1, '2021-08-05 00:17:17', 1, '2021-08-05 00:17:23');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `language_code` varchar(10) NOT NULL,
  `code` varchar(50) NOT NULL,
  `message` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`language_code`, `code`, `message`) VALUES
('in', 'ACCESS_VIOLATION', 'Anda tidak memiliki hak akses'),
('in', 'CHANGE_PASSWORD_FAILED', 'Password Lama salah, mohon periksa kembali'),
('in', 'CHANGE_PASSWORD_SUCCESS', 'Password berhasil diganti'),
('in', 'DELETE_SUCCESS', 'Data berhasil dihapus'),
('in', 'LOGIN_FAILED', 'Username / Password salah, mohon ulangi kembali'),
('in', 'NOT_FOUND', 'Data tidak ditemukan'),
('in', 'SAVE_SUCCESS', 'Data berhasil disimpan'),
('in', 'TICKET_NEW_SUCCESS', 'Tiket Anda sudah kami terima, dan akan kami tindak lanjuti. Terima Kasih.'),
('in', 'TROUBLE', 'Terjadi gangguan pada sistem, hubungi tim support kami'),
('in', 'USER_DUPLICATE', 'Username sudah digunakan');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `news_id` int(10) UNSIGNED NOT NULL,
  `news_type` enum('buletin','lifestyle') DEFAULT NULL,
  `subject` varchar(200) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `file_name` varchar(200) DEFAULT NULL,
  `is_banner` enum('Y','N') DEFAULT 'N',
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `creation_date` datetime DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `news_type`, `subject`, `description`, `start_date`, `end_date`, `file_name`, `is_banner`, `is_active`, `creation_date`, `created_by`, `last_update_date`, `last_updated_by`) VALUES
(6, 'buletin', 'Opening Soon 1', 'Nantikan pembukaan cluster baru dari Meara Santosa', '2021-09-01', '2021-09-30', 'opening.jpg', 'N', 'Y', NULL, NULL, '2021-09-03 16:21:22', 1),
(7, 'buletin', 'Opening Soon 2', 'Opening Soon', '2021-09-01', '2021-09-15', 'opening.jpg', 'Y', 'N', NULL, NULL, '2021-09-09 22:03:07', 1),
(8, 'buletin', 'Opening Soon 3', NULL, NULL, NULL, 'opening.jpg', 'N', 'N', NULL, NULL, '2021-09-03 16:06:34', 1),
(9, 'buletin', 'tes dari winApp', 'test dari winApp, bisa muncul di android', NULL, NULL, '13BE4A513405C503880885FE0DB1D687.jpg', 'Y', 'Y', '2021-08-31 23:08:16', 1, '2021-08-31 23:08:51', 1),
(10, 'buletin', 'Wallpaper unik untuk Rumah Anda', 'Halo guys tersedia model wallpaper 3D batu bata putih', '2021-09-09', '2021-10-09', '1fdee76e86a27d08f31ffdae429f8d4e.jpg', 'Y', 'Y', '2021-09-09 22:02:54', 1, '2021-09-09 22:02:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `penerimaan_detail`
--

CREATE TABLE `penerimaan_detail` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `penerimaan_id` bigint(10) DEFAULT 0,
  `item_id` bigint(10) DEFAULT 0,
  `item_qty` double DEFAULT 0,
  `item_hpp` double DEFAULT 0,
  `item_subtotal` double DEFAULT 0,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penerimaan_detail`
--

INSERT INTO `penerimaan_detail` (`id`, `penerimaan_id`, `item_id`, `item_qty`, `item_hpp`, `item_subtotal`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 1, 2, 5, 5000, 25000, 'Y', 1, '2021-07-27 23:15:57', 1, '2021-07-27 23:23:27'),
(2, 1, 1, 10, 20000, 200000, 'Y', 1, '2021-07-27 23:15:57', 1, '2021-07-27 23:23:27'),
(3, 2, 2, 10, 2000, 20000, 'Y', 1, '2021-07-28 13:25:24', 1, '2021-08-24 07:51:27'),
(4, 3, 2, 5, 1327.84, 6639.2, 'Y', 1, '2021-08-10 22:55:42', 0, NULL),
(5, 4, 2, 100, 1328, 132800, 'Y', 1, '2021-08-20 23:00:17', 1, '2021-08-24 07:50:39'),
(6, 4, 5, 10, 0, 0, 'Y', 1, '2021-08-24 07:50:39', 0, NULL),
(7, 2, 1, 2, 100, 200, 'Y', 1, '2021-08-24 07:51:27', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `penerimaan_header`
--

CREATE TABLE `penerimaan_header` (
  `penerimaan_id` bigint(10) UNSIGNED NOT NULL,
  `type_trans` int(10) DEFAULT 1,
  `penerimaan_datetime` datetime DEFAULT NULL,
  `penerimaan_total` double DEFAULT 0,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penerimaan_header`
--

INSERT INTO `penerimaan_header` (`penerimaan_id`, `type_trans`, `penerimaan_datetime`, `penerimaan_total`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 2, '2021-07-04 00:00:00', 225000, 'N', 1, '2021-07-27 23:15:57', 1, '2021-08-10 22:56:06'),
(2, 2, '2021-07-28 00:00:00', 20200, 'Y', 1, '2021-07-28 13:25:21', 1, '2021-08-24 07:51:27'),
(3, 2, '2021-08-10 00:00:00', 6639.2, 'Y', 1, '2021-08-10 22:54:38', 1, '2021-08-10 22:55:42'),
(4, 4, '2021-08-20 00:00:00', 132800, 'Y', 1, '2021-08-20 23:00:17', 1, '2021-08-24 07:50:39');

-- --------------------------------------------------------

--
-- Table structure for table `penyesuaian_stok`
--

CREATE TABLE `penyesuaian_stok` (
  `ID` bigint(10) UNSIGNED NOT NULL,
  `item_id` bigint(10) DEFAULT 0,
  `qty_awal` double DEFAULT 0,
  `qty_baru` double DEFAULT 0,
  `keterangan` varchar(200) DEFAULT '',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penyesuaian_stok`
--

INSERT INTO `penyesuaian_stok` (`ID`, `item_id`, `qty_awal`, `qty_baru`, `keterangan`, `created_by`, `creation_date`) VALUES
(1, 2, 15, 20, 'AAA', 1, '2021-07-27 23:13:53'),
(2, 1, 28, 30, 'AAAA', 1, '2021-08-08 14:41:09'),
(3, 1, 10, 50, 'ASAD', 1, '2021-08-20 22:43:19'),
(4, 1, 28, 30, 'TAMBAH 2', 1, '2021-08-24 07:46:43'),
(5, 2, 131, 140, 'BELI BARU', 1, '2021-08-24 07:47:01');

-- --------------------------------------------------------

--
-- Table structure for table `push_notifications`
--

CREATE TABLE `push_notifications` (
  `push_id` int(10) UNSIGNED NOT NULL,
  `push_type` varchar(30) DEFAULT NULL,
  `fcm_token` varchar(300) DEFAULT NULL,
  `is_sent` enum('Y','N') DEFAULT 'N',
  `send_date` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_notifications`
--

INSERT INTO `push_notifications` (`push_id`, `push_type`, `fcm_token`, `is_sent`, `send_date`, `is_active`) VALUES
(1, 'admin_reply', NULL, 'N', NULL, 'Y'),
(2, 'admin_reply', NULL, 'N', NULL, 'Y'),
(3, 'admin_reply', NULL, 'N', NULL, 'Y'),
(4, 'admin_reply', NULL, 'N', NULL, 'Y'),
(5, 'admin_reply', NULL, 'N', NULL, 'Y'),
(6, 'admin_reply', NULL, 'N', NULL, 'Y'),
(7, 'admin_reply', NULL, 'N', NULL, 'Y'),
(8, 'admin_reply', NULL, 'N', NULL, 'Y'),
(9, 'admin_reply', 'eAUKSA3OQ9u5uCXV0JQb-j:APA91bEnxDM-gudEzoP4hZDfiPDy079r8ptZWRMHQK9Ch6qB_-g314N8IBgw-0VskyspyN8IROP_H0-b22YDDC-ojqZUV28uvHWZ35kTipCyyz9d0lg64utaw27SETotzgBICPTKnK2f', 'N', NULL, 'Y'),
(10, 'admin_reply', 'eAUKSA3OQ9u5uCXV0JQb-j:APA91bEnxDM-gudEzoP4hZDfiPDy079r8ptZWRMHQK9Ch6qB_-g314N8IBgw-0VskyspyN8IROP_H0-b22YDDC-ojqZUV28uvHWZ35kTipCyyz9d0lg64utaw27SETotzgBICPTKnK2f', 'N', NULL, 'Y'),
(11, 'admin_reply', 'eAUKSA3OQ9u5uCXV0JQb-j:APA91bEnxDM-gudEzoP4hZDfiPDy079r8ptZWRMHQK9Ch6qB_-g314N8IBgw-0VskyspyN8IROP_H0-b22YDDC-ojqZUV28uvHWZ35kTipCyyz9d0lg64utaw27SETotzgBICPTKnK2f', 'N', NULL, 'Y'),
(12, 'admin_reply', 'eAUKSA3OQ9u5uCXV0JQb-j:APA91bEnxDM-gudEzoP4hZDfiPDy079r8ptZWRMHQK9Ch6qB_-g314N8IBgw-0VskyspyN8IROP_H0-b22YDDC-ojqZUV28uvHWZ35kTipCyyz9d0lg64utaw27SETotzgBICPTKnK2f', 'N', NULL, 'Y'),
(13, 'admin_reply', 'eAUKSA3OQ9u5uCXV0JQb-j:APA91bEnxDM-gudEzoP4hZDfiPDy079r8ptZWRMHQK9Ch6qB_-g314N8IBgw-0VskyspyN8IROP_H0-b22YDDC-ojqZUV28uvHWZ35kTipCyyz9d0lg64utaw27SETotzgBICPTKnK2f', 'N', NULL, 'Y'),
(14, 'cs', 'fd_CUEyBTTmORyFNRfRLit:APA91bFvMIVnBerhvZN4mL-7RcpqDMQl5e0CgiVpLD4F7u4tioytNu2iVUYngOi-e6Yov_AFJSN3B5VnSZEc5ayqebO3abLhS-rJpCORmq-vZMnpvPGPzFGecyaDUnDxLhDnpKJeHNI7', 'N', NULL, 'Y'),
(15, 'cs', NULL, 'N', NULL, 'Y'),
(16, 'cs', NULL, 'N', NULL, 'Y'),
(17, 'cs', NULL, 'N', NULL, 'Y'),
(18, 'cs', NULL, 'N', NULL, 'Y'),
(19, 'repair', NULL, 'N', NULL, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `push_notification_parameters`
--

CREATE TABLE `push_notification_parameters` (
  `push_id` int(10) UNSIGNED NOT NULL,
  `key_label` varchar(100) NOT NULL,
  `key_value` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_notification_parameters`
--

INSERT INTO `push_notification_parameters` (`push_id`, `key_label`, `key_value`) VALUES
(1, 'message', 'Betul Pak, berikut rincian anya'),
(1, 'ticket_id', '3'),
(1, 'title', 'Reply #MS21070001'),
(2, 'message', 'Baik pak, tim kami akan mengecek besok pukul 10.00'),
(2, 'ticket_id', '4'),
(2, 'title', 'Reply #MS21070002'),
(3, 'message', 'Setelah ada pengecekan, ada biaya jasa untuk pembersihan selokan'),
(3, 'ticket_id', '4'),
(3, 'title', 'Reply #MS21070002'),
(4, 'message', 'Terima kasih, kami akan mengirim teknisi kesana'),
(4, 'ticket_id', '6'),
(4, 'title', 'Reply #MS21070004'),
(5, 'message', 'Biaya Perbaikan sebesar Rp 250.000'),
(5, 'ticket_id', '6'),
(5, 'title', 'Reply #MS21070004'),
(6, 'message', 'Ada perbaikan'),
(6, 'ticket_id', '5'),
(6, 'title', 'Reply #MS21070003'),
(7, 'message', 'Selamat Pagi, Tim kami akan datang ke rumah Anda pada Sabtu, 23 Juli 2021 pukul 15.00 - 16.00 Terima kasih'),
(7, 'ticket_id', '1'),
(7, 'title', 'Reply #MS21070001'),
(8, 'message', 'Selamat Sore, Dari hasil pengecekan dilapangan, ada biaya perbaikan sebesar Rp 320.000 dengan rincian sebagai berikut: Aquaproof: Rp 200.000 Serat Fiber: Rp. 20.000 Jasa: 100.000'),
(8, 'ticket_id', '1'),
(8, 'title', 'Reply #MS21070001'),
(9, 'message', 'beli baru'),
(9, 'ticket_id', '12'),
(9, 'title', 'Reply #MS21080008'),
(10, 'message', 'tim kami akan melakukan pengecekan ke lapangan'),
(10, 'ticket_id', '6'),
(10, 'title', 'Reply #MS21080002'),
(11, 'message', 'ada biaya perbaikan'),
(11, 'ticket_id', '7'),
(11, 'title', 'Reply #MS21080003'),
(12, 'message', 'ya kenapa ?'),
(12, 'ticket_id', '13'),
(12, 'title', 'Reply #MS21090001'),
(13, 'message', 'ada biaya tukang'),
(13, 'ticket_id', '13'),
(13, 'title', 'Reply #MS21090001'),
(14, 'message', 'yaaa'),
(14, 'push_type', 'cs'),
(14, 'ticket_id', '12'),
(15, 'message', 'boleh buat BBQ party? '),
(15, 'push_type', 'cs'),
(15, 'ticket_id', '14'),
(16, 'message', ''),
(16, 'push_type', 'cs'),
(16, 'ticket_id', '14'),
(17, 'message', NULL),
(17, 'push_type', 'cs'),
(17, 'ticket_id', '14'),
(18, 'message', NULL),
(18, 'push_type', 'cs'),
(18, 'ticket_id', '14'),
(19, 'message', 'Gelap'),
(19, 'push_type', 'repair'),
(19, 'ticket_id', '8');

-- --------------------------------------------------------

--
-- Table structure for table `sys_config`
--

CREATE TABLE `sys_config` (
  `ID` tinyint(3) UNSIGNED NOT NULL,
  `company_name` varchar(50) DEFAULT '',
  `company_address` varchar(100) DEFAULT '',
  `company_phone` varchar(20) DEFAULT '',
  `company_email` varchar(50) DEFAULT '',
  `default_printer` varchar(200) DEFAULT '',
  `auto_backup_flag` tinyint(3) UNSIGNED DEFAULT 0,
  `auto_backup_dir` varchar(200) DEFAULT '',
  `print_preview` tinyint(3) UNSIGNED DEFAULT 0,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sys_config`
--

INSERT INTO `sys_config` (`ID`, `company_name`, `company_address`, `company_phone`, `company_email`, `default_printer`, `auto_backup_flag`, `auto_backup_dir`, `print_preview`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 'PT. MENARA SANTOSA', 'JL. RONGGOWARSITO NO.73, KEPRABON', '(0271) 643100', 'menara@menarasantosa.com', 'PrimoPDF', 0, '', 0, 'Y', 0, NULL, 1, '2021-08-24 07:17:01');

-- --------------------------------------------------------

--
-- Table structure for table `sys_config_transaksi`
--

CREATE TABLE `sys_config_transaksi` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_type` int(11) DEFAULT 0,
  `biaya_transaksi` double DEFAULT 0,
  `biaya_transaksi_percent` double DEFAULT 0,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sys_config_transaksi`
--

INSERT INTO `sys_config_transaksi` (`id`, `payment_type`, `biaya_transaksi`, `biaya_transaksi_percent`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 1, 4000, 0, 'Y', 1, '2021-07-20 23:19:58', 1, '2021-08-24 07:17:01'),
(2, 2, 0, 2, 'Y', 1, '2021-07-20 23:28:35', 1, '2021-08-24 07:17:01'),
(3, 3, 0, 1.5, 'Y', 1, '2021-07-20 23:28:35', 1, '2021-08-24 07:17:01'),
(4, 4, 0, 0, 'Y', 1, '2021-07-20 23:28:35', 1, '2021-08-24 07:17:01'),
(5, 5, 5000, 0, 'Y', 1, '2021-07-20 23:28:35', 1, '2021-08-24 07:17:01'),
(6, 6, 4000, 0, 'Y', 0, NULL, 1, '2021-08-24 07:17:01'),
(7, 7, 4000, 0, 'Y', 0, NULL, 1, '2021-08-24 07:17:01');

-- --------------------------------------------------------

--
-- Table structure for table `tblusertocheckedrecords`
--

CREATE TABLE `tblusertocheckedrecords` (
  `recID` int(11) NOT NULL,
  `userID` int(11) NOT NULL DEFAULT 0,
  `query` text CHARACTER SET utf8 NOT NULL,
  `uniqueKey` varchar(30) NOT NULL DEFAULT '',
  `createdDate` datetime NOT NULL DEFAULT current_timestamp(),
  `type` int(11) NOT NULL DEFAULT 0,
  `payment_type` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblusertocheckedrecords`
--

INSERT INTO `tblusertocheckedrecords` (`recID`, `userID`, `query`, `uniqueKey`, `createdDate`, `type`, `payment_type`) VALUES
(1, 1, '', 'p6g9iq2jbeil18oin3dmd41emn1270', '2021-07-21 10:55:55', 1, 1),
(4, 1, '', 'b9a2f9db70b1ef098ba14654a18d2', '2021-09-14 17:26:39', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblusertocheckedrecordsdetail`
--

CREATE TABLE `tblusertocheckedrecordsdetail` (
  `id` int(11) NOT NULL,
  `recID` int(11) NOT NULL DEFAULT 0,
  `recordID` int(11) NOT NULL DEFAULT 0,
  `trans_type` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_handyman`
--

CREATE TABLE `transaksi_handyman` (
  `id_trans` bigint(10) UNSIGNED NOT NULL,
  `type_trans` int(11) DEFAULT 3,
  `date_issued` datetime DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  `kavling_id` bigint(10) DEFAULT 0,
  `user_id` bigint(10) DEFAULT 0,
  `payment_id` varchar(45) DEFAULT NULL,
  `nominal` double DEFAULT 0,
  `status_id` int(10) DEFAULT 0,
  `date_paid` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_ipl`
--

CREATE TABLE `transaksi_ipl` (
  `id_trans` bigint(10) UNSIGNED NOT NULL,
  `type_trans` int(10) DEFAULT 0,
  `date_issued` datetime DEFAULT NULL,
  `kavling_id` bigint(10) DEFAULT 0,
  `user_id` bigint(10) DEFAULT 0,
  `payment_id` varchar(45) DEFAULT NULL,
  `nominal` double DEFAULT 0,
  `status_id` int(10) DEFAULT 0,
  `date_paid` datetime DEFAULT NULL,
  `start_ipl` datetime DEFAULT NULL,
  `end_ipl` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_ipl`
--

INSERT INTO `transaksi_ipl` (`id_trans`, `type_trans`, `date_issued`, `kavling_id`, `user_id`, `payment_id`, `nominal`, `status_id`, `date_paid`, `start_ipl`, `end_ipl`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 1, '2021-08-08 00:00:00', 2, 2, NULL, 60000, 0, NULL, '2021-08-01 00:00:00', '2021-10-01 00:00:00', 'N', 1, '2021-08-08 11:34:15', 1, '2021-08-20 22:23:48'),
(2, 1, '2021-08-20 00:00:00', 2, 2, NULL, 60000, 0, NULL, '2021-08-01 00:00:00', '2021-10-01 00:00:00', 'Y', 1, '2021-08-20 22:23:53', 0, NULL),
(3, 1, '2021-08-24 00:00:00', 4, 4, NULL, 100000, 0, NULL, '2021-08-01 00:00:00', '2023-03-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_ipl_detail`
--

CREATE TABLE `transaksi_ipl_detail` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `id_trans` bigint(10) DEFAULT 0,
  `nominal` double DEFAULT 0,
  `periode_pembayaran` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_ipl_detail`
--

INSERT INTO `transaksi_ipl_detail` (`id`, `id_trans`, `nominal`, `periode_pembayaran`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 1, 20000, '2021-08-01 00:00:00', 'Y', 1, '2021-08-08 11:34:15', 0, NULL),
(2, 1, 20000, '2021-09-01 00:00:00', 'Y', 1, '2021-08-08 11:34:15', 0, NULL),
(3, 1, 20000, '2021-10-01 00:00:00', 'Y', 1, '2021-08-08 11:34:15', 0, NULL),
(4, 2, 20000, '2021-08-01 00:00:00', 'Y', 1, '2021-08-20 22:23:53', 0, NULL),
(5, 2, 20000, '2021-09-01 00:00:00', 'Y', 1, '2021-08-20 22:23:53', 0, NULL),
(6, 2, 20000, '2021-10-01 00:00:00', 'Y', 1, '2021-08-20 22:23:53', 0, NULL),
(7, 3, 5000, '2021-08-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(8, 3, 5000, '2021-09-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(9, 3, 5000, '2021-10-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(10, 3, 5000, '2021-11-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(11, 3, 5000, '2021-12-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(12, 3, 5000, '2022-01-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(13, 3, 5000, '2022-02-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(14, 3, 5000, '2022-03-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(15, 3, 5000, '2022-04-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(16, 3, 5000, '2022-05-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(17, 3, 5000, '2022-06-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(18, 3, 5000, '2022-07-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(19, 3, 5000, '2022-08-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(20, 3, 5000, '2022-09-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(21, 3, 5000, '2022-10-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(22, 3, 5000, '2022-11-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(23, 3, 5000, '2022-12-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(24, 3, 5000, '2023-01-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(25, 3, 5000, '2023-02-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL),
(26, 3, 5000, '2023-03-01 00:00:00', 'Y', 1, '2021-08-24 07:39:55', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_payment`
--

CREATE TABLE `transaksi_payment` (
  `id_payment` bigint(10) UNSIGNED NOT NULL,
  `unique_code` varchar(50) DEFAULT NULL,
  `date_issued` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `payment_id` varchar(45) DEFAULT '',
  `total` double DEFAULT 0,
  `payment_type` int(11) DEFAULT 0,
  `biaya_transaksi` double DEFAULT 0,
  `status_id` int(10) DEFAULT 0,
  `date_paid` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL,
  `status_code` int(11) DEFAULT 0,
  `transaction_id` varchar(500) DEFAULT NULL,
  `transaction_status` varchar(50) DEFAULT NULL,
  `transaction_time` datetime DEFAULT '1900-01-01 00:00:00',
  `midtrans_payment_type` varchar(50) DEFAULT NULL,
  `bank_name` varchar(50) DEFAULT NULL,
  `va_number` varchar(50) DEFAULT NULL,
  `bill_key` varchar(50) DEFAULT NULL,
  `biller_code` varchar(50) DEFAULT NULL,
  `payment_code` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_payment`
--

INSERT INTO `transaksi_payment` (`id_payment`, `unique_code`, `date_issued`, `user_id`, `payment_id`, `total`, `payment_type`, `biaya_transaksi`, `status_id`, `date_paid`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`, `status_code`, `transaction_id`, `transaction_status`, `transaction_time`, `midtrans_payment_type`, `bank_name`, `va_number`, `bill_key`, `biller_code`, `payment_code`) VALUES
(1, NULL, '2021-07-21 18:41:36', NULL, '1446156635', 65000, 1, 4000, 1, '2021-07-21 18:41:36', 'Y', 1, '2021-07-21 18:41:36', 0, NULL, 201, 'b67e7e72-d97b-4d39-ba90-e7e691630a25', 'pending', '2021-07-21 18:41:34', 'bank_transfer', 'Bni', '9883400086116934', '', '', ''),
(3, '58531109-4c30-4940-8fba-063a7402e817', '2021-08-24 12:36:06', 2, '20210824123606208', 80000, 6, 4000, 1, NULL, 'Y', 2, '2021-08-24 12:36:06', 2, '2021-08-24 12:36:06', 201, 'da71944b-f605-4f9a-b1b6-8b7cdf11a103', 'pending', '2021-08-24 12:36:06', 'bank_transfer', 'bni', '9881500770429130', '', '', ''),
(4, '42775af7-7172-400f-95d7-1a2782940f33', '2021-08-24 13:50:10', 2, '20210824135010342', 89000, 6, 4000, 1, NULL, 'Y', 2, '2021-08-24 13:50:10', 2, '2021-08-24 13:50:10', 201, 'c5a3a36e-f4e6-46be-8e42-10245ac2e62f', 'pending', '2021-08-24 13:50:10', 'bank_transfer', 'bni', '9881500772624790', '', '', ''),
(5, '9b7a33fd-56b3-4b71-bd1a-5b0f94df9050', '2021-09-11 01:31:25', 2, '20210911013125760', 75000, 2, 1500, 2, '2021-09-11 01:31:42', 'Y', 2, '2021-09-11 01:31:25', 2, '2021-09-11 01:31:25', 201, '5fba52a0-6d8c-414b-8733-7db039a63094', 'pending', '2021-09-11 01:31:26', 'gopay', '', '', '', '', ''),
(6, 'bd3e2a95-9d86-43b8-9ffb-b9e8cbc4c929', '2021-09-13 21:24:11', 2, '20210913212411856', 120000, 7, 4000, 1, NULL, 'Y', 2, '2021-09-13 21:24:11', 2, '2021-09-13 21:24:11', 201, '4fe56c71-19a1-4bfd-a613-1b85067e1859', 'pending', '2021-09-13 21:24:12', 'echannel', '', '', '508311482350', '70012', '');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_payment_detail`
--

CREATE TABLE `transaksi_payment_detail` (
  `id_payment_detail` bigint(10) UNSIGNED NOT NULL,
  `id_payment` bigint(10) DEFAULT 0,
  `id_trans` bigint(10) DEFAULT 0,
  `type_trans` int(10) DEFAULT 0,
  `total` double DEFAULT 0,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_payment_detail`
--

INSERT INTO `transaksi_payment_detail` (`id_payment_detail`, `id_payment`, `id_trans`, `type_trans`, `total`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 1, 1, 2, 65000, 'Y', 1, '2021-07-21 18:41:36', 0, NULL),
(2, 3, 2, 2, 80000, 'Y', 2, '2021-08-24 12:36:06', 2, '2021-08-24 12:36:06'),
(3, 4, 9, 2, 89000, 'Y', 2, '2021-08-24 13:50:10', 2, '2021-08-24 13:50:10'),
(4, 5, 12, 3, 75000, 'Y', 2, '2021-09-11 01:31:25', 2, '2021-09-11 01:31:25'),
(5, 6, 11, 2, 120000, 'Y', 2, '2021-09-13 21:24:11', 2, '2021-09-13 21:24:11');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_retail`
--

CREATE TABLE `transaksi_retail` (
  `id_trans` bigint(10) UNSIGNED NOT NULL,
  `type_trans` int(10) DEFAULT 0,
  `date_issued` datetime DEFAULT NULL,
  `date_expired` datetime DEFAULT NULL,
  `kavling_id` bigint(10) DEFAULT 0,
  `user_id` bigint(10) DEFAULT 0,
  `payment_id` varchar(45) DEFAULT NULL,
  `nominal` double DEFAULT 0,
  `status_id` int(10) DEFAULT 0,
  `date_paid` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_retail`
--

INSERT INTO `transaksi_retail` (`id_trans`, `type_trans`, `date_issued`, `date_expired`, `kavling_id`, `user_id`, `payment_id`, `nominal`, `status_id`, `date_paid`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 2, '2021-07-20 10:34:28', NULL, 2, 1, '1446156635', 65000, 1, '2021-07-21 18:41:34', 'Y', 1, '2021-07-19 23:55:41', 1, '2021-07-20 10:34:41'),
(2, 2, '2021-07-29 00:00:00', NULL, 2, 2, '20210824123606208', 80000, 1, NULL, 'Y', 1, '2021-07-27 23:11:21', 1, '2021-08-09 23:44:05'),
(3, 2, '2021-08-08 00:00:00', NULL, 2, 1, NULL, 140000, 0, NULL, 'N', 1, '2021-08-08 13:59:37', 1, '2021-08-08 14:00:29'),
(4, 2, '2021-08-09 00:00:00', NULL, 2, 5, NULL, 200000, 0, NULL, 'Y', 1, '2021-08-09 00:24:01', 1, '2021-08-18 13:00:33'),
(5, 2, '2021-08-18 00:00:00', '2021-08-18 00:44:05', 2, 1, NULL, 500000, 0, NULL, 'N', 1, '2021-08-18 00:44:05', 0, '2021-08-18 22:31:31'),
(6, 2, '2021-08-18 00:00:00', '2021-08-19 00:55:36', 2, 5, NULL, 200000, 0, NULL, 'Y', 1, '2021-08-18 12:55:36', 0, NULL),
(7, 2, '2021-08-18 00:00:00', '2021-08-19 01:08:03', 2, 5, NULL, 240000, 0, NULL, 'Y', 1, '2021-08-18 13:08:03', 1, '2021-08-24 07:44:25'),
(8, 4, '2021-08-20 00:00:00', '2021-08-21 11:16:33', 2, 1, '1409202109573379', 20000, 0, NULL, 'Y', 1, '2021-08-20 23:16:33', 1, '2021-09-14 16:57:33'),
(9, 2, '2021-08-24 12:31:32', NULL, 2, 2, '20210824135010342', 89000, 1, NULL, 'Y', 2, '2021-08-24 12:31:32', 2, '2021-08-24 12:31:32'),
(10, 2, '2021-08-24 12:35:55', NULL, 2, 2, NULL, 20000, 0, NULL, 'Y', 2, '2021-08-24 12:35:55', 2, '2021-08-24 12:35:55'),
(11, 2, '2021-09-06 23:51:42', NULL, 2, 2, '20210913212411856', 120000, 1, NULL, 'Y', 2, '2021-09-06 23:51:42', 2, '2021-09-06 23:51:42'),
(12, 3, '2021-09-11 01:15:26', NULL, 2, 2, '20210911013125760', 75000, 2, '2021-09-11 01:31:42', 'Y', 2, '2021-09-11 01:15:26', 2, '2021-09-11 01:15:26');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_retail_detail`
--

CREATE TABLE `transaksi_retail_detail` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `id_trans` bigint(10) DEFAULT 0,
  `item_id` bigint(10) DEFAULT 0,
  `item_qty` double DEFAULT 0,
  `item_hpp` double DEFAULT 0,
  `item_price` double DEFAULT 0,
  `subtotal` double DEFAULT 0,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_retail_detail`
--

INSERT INTO `transaksi_retail_detail` (`id`, `id_trans`, `item_id`, `item_qty`, `item_hpp`, `item_price`, `subtotal`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 0, 1, 3, 0, 20000, 60000, 'Y', 1, '2021-07-19 23:55:41', 1, '2021-08-24 07:40:47'),
(2, 0, 2, 5, 0, 5000, 25000, 'Y', 1, '2021-07-20 10:25:26', 1, '2021-08-24 07:40:47'),
(3, 2, 2, 10, 0, 8000, 80000, 'Y', 1, '2021-07-27 23:11:21', 1, '2021-08-09 23:44:05'),
(4, 2, 1, 8, 0, 20000, 160000, 'N', 1, '2021-07-27 23:11:21', 1, '2021-08-08 14:01:41'),
(5, 3, 1, 7, 0, 20000, 140000, 'Y', 1, '2021-08-08 13:59:37', 1, '2021-08-08 14:00:02'),
(6, 4, 3, 10, 10000, 20000, 200000, 'Y', 1, '2021-08-09 00:24:01', 1, '2021-08-18 13:00:33'),
(7, 5, 4, 10, 0, 50000, 500000, 'Y', 1, '2021-08-18 00:44:05', 0, NULL),
(8, 6, 1, 10, 0, 20000, 200000, 'Y', 1, '2021-08-18 12:55:36', 0, NULL),
(9, 7, 3, 11, 10000, 20000, 220000, 'Y', 1, '2021-08-18 13:08:03', 1, '2021-08-24 07:44:25'),
(10, 8, 2, 4, 1328, 5000, 20000, 'Y', 1, '2021-08-20 23:16:33', 1, '2021-08-24 07:44:49'),
(11, 0, 1, 20, 0, 20000, 400000, 'Y', 1, '2021-08-24 07:43:38', 0, NULL),
(12, 7, 1, 1, 0, 20000, 20000, 'Y', 1, '2021-08-24 07:44:25', 0, NULL),
(13, 9, 1, 1, 0, 75000, 75000, 'Y', 2, '2021-08-24 12:31:32', 2, '2021-08-24 12:31:32'),
(14, 9, 4, 4, 0, 3500, 14000, 'Y', 2, '2021-08-24 12:31:32', 2, '2021-08-24 12:31:32'),
(15, 10, 3, 1, 10000, 20000, 20000, 'Y', 2, '2021-08-24 12:35:55', 2, '2021-08-24 12:35:55'),
(16, 11, 3, 6, 10000, 20000, 120000, 'Y', 2, '2021-09-06 23:51:42', 2, '2021-09-06 23:51:42'),
(17, 12, 13, 1, 0, 75000, 75000, 'Y', 2, '2021-09-11 01:15:26', 2, '2021-09-11 01:15:26');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_type`
--

CREATE TABLE `transaksi_type` (
  `type_trans` int(10) UNSIGNED NOT NULL,
  `type_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_type`
--

INSERT INTO `transaksi_type` (`type_trans`, `type_name`) VALUES
(1, 'TRANSAKSI IPL'),
(2, 'TRANSAKSI RETAIL'),
(3, 'TRANSAKSI SERVICE'),
(4, 'TRANSAKSI APOTIK');

-- --------------------------------------------------------

--
-- Table structure for table `user_counter`
--

CREATE TABLE `user_counter` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `cart_count` smallint(5) UNSIGNED DEFAULT 0,
  `inbox_count` smallint(5) UNSIGNED DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_counter`
--

INSERT INTO `user_counter` (`user_id`, `cart_count`, `inbox_count`) VALUES
(1, 0, 0),
(2, 0, 0),
(4, 0, 0),
(6, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_kavling`
--

CREATE TABLE `user_kavling` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `user_id` bigint(10) DEFAULT NULL,
  `kavling_id` bigint(10) DEFAULT NULL,
  `start_ipl` datetime DEFAULT NULL,
  `remarks` varchar(200) DEFAULT '',
  `user_qr_code` varchar(100) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_kavling`
--

INSERT INTO `user_kavling` (`id`, `user_id`, `kavling_id`, `start_ipl`, `remarks`, `user_qr_code`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 1, 1, '2021-08-01 15:45:55', 'AAAAA', 'menarasantosa', 'N', 1, '2021-07-09 15:46:43', 1, '2021-07-01 16:14:16'),
(2, 1, 2, '2021-08-01 00:00:00', 'SSSSSSSSDDDD', 'menarasantosa', 'Y', 1, '2021-07-09 16:16:48', 1, '2021-08-20 23:41:57'),
(3, 2, 2, '2021-07-01 00:00:00', 'DDDDDDD', 'menarasantosa', 'Y', 1, '2021-07-20 23:57:32', 1, '2021-08-20 23:41:57'),
(4, 5, 2, '2021-08-01 00:00:00', 'DFFFFF', 'menarasantosa', 'N', 1, '2021-08-04 21:02:44', 1, '2021-08-04 21:10:53'),
(5, 5, 2, '2021-08-01 00:00:00', '', 'menarasantosa', 'N', 1, '2021-08-04 22:14:25', 1, '2021-08-04 22:16:37'),
(6, 5, 2, '2021-08-01 00:00:00', '', 'menarasantosa', 'N', 1, '2021-08-04 22:16:43', 1, '2021-08-20 23:41:56'),
(7, 6, 1, '2021-08-01 00:00:00', '', 'menarasantosa', 'Y', 1, '2021-08-20 23:51:22', 0, NULL),
(8, 4, 4, '2021-08-01 00:00:00', '', 'menarasantosa', 'N', 1, '2021-08-24 07:26:26', 1, '2021-08-24 07:31:21'),
(9, 4, 4, '2021-08-01 00:00:00', 'AAAA', 'menarasantosa', 'N', 1, '2021-08-24 07:26:33', 1, '2021-08-24 07:31:18'),
(10, 4, 4, '2021-08-01 00:00:00', 'AAAA', 'menarasantosa', 'N', 1, '2021-08-24 07:26:42', 0, NULL),
(11, 4, 4, '2021-08-01 00:00:00', '', 'menarasantosa', 'N', 1, '2021-08-24 07:32:24', 1, '2021-08-24 07:34:52'),
(12, 4, 4, '2021-08-01 00:00:00', 'AAA', 'menarasantosa', 'N', 1, '2021-08-24 07:32:43', 1, '2021-08-24 07:35:04'),
(13, 4, 4, '2021-08-01 00:00:00', 'AAA', 'menarasantosa', 'N', 1, '2021-08-24 07:32:49', 1, '2021-08-24 07:35:08'),
(14, 4, 4, '2021-08-01 00:00:00', 'AAAA', 'menarasantosa', 'Y', 1, '2021-08-24 07:35:16', 1, '2021-08-24 07:35:29');

-- --------------------------------------------------------

--
-- Table structure for table `user_login_data`
--

CREATE TABLE `user_login_data` (
  `user_id` bigint(10) UNSIGNED NOT NULL,
  `user_name` varchar(15) DEFAULT '',
  `user_password` varchar(50) DEFAULT '',
  `group_id` tinyint(3) UNSIGNED DEFAULT 0,
  `last_login` datetime DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `user_full_name` varchar(100) DEFAULT '',
  `user_id_type` tinyint(3) DEFAULT 0,
  `user_id_no` varchar(100) DEFAULT '',
  `user_phone_1` varchar(45) DEFAULT '',
  `user_phone_2` varchar(45) DEFAULT '',
  `user_email_address` varchar(200) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_login_data`
--

INSERT INTO `user_login_data` (`user_id`, `user_name`, `user_password`, `group_id`, `last_login`, `last_logout`, `user_full_name`, `user_id_type`, `user_id_no`, `user_phone_1`, `user_phone_2`, `user_email_address`, `is_active`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`) VALUES
(1, 'ADMIN', '202cb962ac59075b964b07152d234b70', 1, '2021-09-14 09:15:50', '2021-08-31 23:09:08', 'ADMIN1234567', 1, '123456', '081558775652', '0111', 'ARISTON@ALPHASOFT.COM', 'Y', NULL, NULL, 1, '2021-08-24 00:29:35'),
(2, 'RESIDENT', '202cb962ac59075b964b07152d234b70', 3, NULL, NULL, 'Resident', 1, '', '0123', '', '', 'Y', 1, '2021-07-20 23:55:19', NULL, NULL),
(4, 'LANDLORD', '202cb962ac59075b964b07152d234b70', 2, NULL, NULL, 'AAAA', 1, '', '000', '', '', 'Y', 1, '2021-08-04 20:52:17', NULL, NULL),
(5, 'ANDRI', '202cb962ac59075b964b07152d234b70', 3, NULL, NULL, 'AAA', 1, '', '000', '', '', 'N', 1, '2021-08-04 20:56:58', 1, '2021-08-20 23:48:44'),
(6, 'ANDRI', '202cb962ac59075b964b07152d234b70', 3, NULL, NULL, 'ANDRI ANDRI', 1, '', '0123', '', '', 'Y', 1, '2021-08-20 23:49:12', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_token`
--

CREATE TABLE `user_token` (
  `user_id` bigint(10) UNSIGNED NOT NULL DEFAULT 0,
  `access_token` varchar(300) DEFAULT NULL,
  `fcm_token` varchar(300) DEFAULT NULL,
  `gcm_token` varchar(300) DEFAULT NULL,
  `ios_token` varchar(300) DEFAULT NULL,
  `web_token` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_token`
--

INSERT INTO `user_token` (`user_id`, `access_token`, `fcm_token`, `gcm_token`, `ios_token`, `web_token`) VALUES
(1, 'OQP0UY8ZUX', '1', '1', '1', '1'),
(2, 'NRVX4QOF8D', 'dDt3FtmaTpqiAgZWZCFrEX:APA91bGCS2e-6mbsRKxJYwrO0j2T370hcfKKLiFhrFHbWOAKRnw5ig039d7UFsfcWwaRs-IGaz6WUMemK4Bpa1h1NcuBRR8dSk3HxPI107FlICTOASOznJ-Ir0yrkkp0i6pElQtPmdCd', '2', '2', '2'),
(4, 'NRVX4QOF8R', NULL, NULL, NULL, NULL),
(5, 'DUL0G6MLST', NULL, NULL, NULL, NULL),
(6, '10DE7A67E2263E847076A16084AD956D', 'eAUKSA3OQ9u5uCXV0JQb-j:APA91bEnxDM-gudEzoP4hZDfiPDy079r8ptZWRMHQK9Ch6qB_-g314N8IBgw-0VskyspyN8IROP_H0-b22YDDC-ojqZUV28uvHWZ35kTipCyyz9d0lg64utaw27SETotzgBICPTKnK2f', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `cc_category`
--
ALTER TABLE `cc_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `cc_ticket`
--
ALTER TABLE `cc_ticket`
  ADD PRIMARY KEY (`ticket_id`);

--
-- Indexes for table `cc_ticket_history`
--
ALTER TABLE `cc_ticket_history`
  ADD PRIMARY KEY (`history_id`);

--
-- Indexes for table `cc_ticket_history_attach`
--
ALTER TABLE `cc_ticket_history_attach`
  ADD PRIMARY KEY (`attach_id`);

--
-- Indexes for table `inbox`
--
ALTER TABLE `inbox`
  ADD PRIMARY KEY (`inbox_id`);

--
-- Indexes for table `item_category`
--
ALTER TABLE `item_category`
  ADD PRIMARY KEY (`item_category_id`);

--
-- Indexes for table `master_bank`
--
ALTER TABLE `master_bank`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `master_blok`
--
ALTER TABLE `master_blok`
  ADD PRIMARY KEY (`blok_id`),
  ADD UNIQUE KEY `blok_name_UNIQUE` (`blok_name`),
  ADD KEY `secondary` (`kompleks_id`);

--
-- Indexes for table `master_group`
--
ALTER TABLE `master_group`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `master_item`
--
ALTER TABLE `master_item`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `master_kavling`
--
ALTER TABLE `master_kavling`
  ADD PRIMARY KEY (`kavling_id`),
  ADD KEY `secondary` (`kompleks_id`);

--
-- Indexes for table `master_kompleks`
--
ALTER TABLE `master_kompleks`
  ADD PRIMARY KEY (`kompleks_id`);

--
-- Indexes for table `master_module`
--
ALTER TABLE `master_module`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `master_module_access`
--
ALTER TABLE `master_module_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_payment_mode`
--
ALTER TABLE `master_payment_mode`
  ADD PRIMARY KEY (`payment_type`);

--
-- Indexes for table `master_unit`
--
ALTER TABLE `master_unit`
  ADD PRIMARY KEY (`unit_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`language_code`,`code`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `penerimaan_detail`
--
ALTER TABLE `penerimaan_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penerimaan_header`
--
ALTER TABLE `penerimaan_header`
  ADD PRIMARY KEY (`penerimaan_id`);

--
-- Indexes for table `penyesuaian_stok`
--
ALTER TABLE `penyesuaian_stok`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `push_notifications`
--
ALTER TABLE `push_notifications`
  ADD PRIMARY KEY (`push_id`);

--
-- Indexes for table `push_notification_parameters`
--
ALTER TABLE `push_notification_parameters`
  ADD PRIMARY KEY (`push_id`,`key_label`);

--
-- Indexes for table `sys_config`
--
ALTER TABLE `sys_config`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sys_config_transaksi`
--
ALTER TABLE `sys_config_transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblusertocheckedrecords`
--
ALTER TABLE `tblusertocheckedrecords`
  ADD PRIMARY KEY (`recID`);

--
-- Indexes for table `tblusertocheckedrecordsdetail`
--
ALTER TABLE `tblusertocheckedrecordsdetail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_handyman`
--
ALTER TABLE `transaksi_handyman`
  ADD PRIMARY KEY (`id_trans`);

--
-- Indexes for table `transaksi_ipl`
--
ALTER TABLE `transaksi_ipl`
  ADD PRIMARY KEY (`id_trans`);

--
-- Indexes for table `transaksi_ipl_detail`
--
ALTER TABLE `transaksi_ipl_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_payment`
--
ALTER TABLE `transaksi_payment`
  ADD PRIMARY KEY (`id_payment`);

--
-- Indexes for table `transaksi_payment_detail`
--
ALTER TABLE `transaksi_payment_detail`
  ADD PRIMARY KEY (`id_payment_detail`);

--
-- Indexes for table `transaksi_retail`
--
ALTER TABLE `transaksi_retail`
  ADD PRIMARY KEY (`id_trans`);

--
-- Indexes for table `transaksi_retail_detail`
--
ALTER TABLE `transaksi_retail_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_type`
--
ALTER TABLE `transaksi_type`
  ADD PRIMARY KEY (`type_trans`);

--
-- Indexes for table `user_counter`
--
ALTER TABLE `user_counter`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_kavling`
--
ALTER TABLE `user_kavling`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_login_data`
--
ALTER TABLE `user_login_data`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `SECONDARY` (`user_id`);

--
-- Indexes for table `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `cc_category`
--
ALTER TABLE `cc_category`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cc_ticket`
--
ALTER TABLE `cc_ticket`
  MODIFY `ticket_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `cc_ticket_history`
--
ALTER TABLE `cc_ticket_history`
  MODIFY `history_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `cc_ticket_history_attach`
--
ALTER TABLE `cc_ticket_history_attach`
  MODIFY `attach_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `inbox`
--
ALTER TABLE `inbox`
  MODIFY `inbox_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `item_category`
--
ALTER TABLE `item_category`
  MODIFY `item_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `master_bank`
--
ALTER TABLE `master_bank`
  MODIFY `bank_id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `master_blok`
--
ALTER TABLE `master_blok`
  MODIFY `blok_id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `master_group`
--
ALTER TABLE `master_group`
  MODIFY `group_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `master_item`
--
ALTER TABLE `master_item`
  MODIFY `item_id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `master_kavling`
--
ALTER TABLE `master_kavling`
  MODIFY `kavling_id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `master_kompleks`
--
ALTER TABLE `master_kompleks`
  MODIFY `kompleks_id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `master_module_access`
--
ALTER TABLE `master_module_access`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `master_payment_mode`
--
ALTER TABLE `master_payment_mode`
  MODIFY `payment_type` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `master_unit`
--
ALTER TABLE `master_unit`
  MODIFY `unit_id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `penerimaan_detail`
--
ALTER TABLE `penerimaan_detail`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `penerimaan_header`
--
ALTER TABLE `penerimaan_header`
  MODIFY `penerimaan_id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `penyesuaian_stok`
--
ALTER TABLE `penyesuaian_stok`
  MODIFY `ID` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `push_notifications`
--
ALTER TABLE `push_notifications`
  MODIFY `push_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `sys_config`
--
ALTER TABLE `sys_config`
  MODIFY `ID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sys_config_transaksi`
--
ALTER TABLE `sys_config_transaksi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tblusertocheckedrecords`
--
ALTER TABLE `tblusertocheckedrecords`
  MODIFY `recID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tblusertocheckedrecordsdetail`
--
ALTER TABLE `tblusertocheckedrecordsdetail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transaksi_handyman`
--
ALTER TABLE `transaksi_handyman`
  MODIFY `id_trans` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaksi_ipl`
--
ALTER TABLE `transaksi_ipl`
  MODIFY `id_trans` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transaksi_ipl_detail`
--
ALTER TABLE `transaksi_ipl_detail`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `transaksi_payment`
--
ALTER TABLE `transaksi_payment`
  MODIFY `id_payment` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `transaksi_payment_detail`
--
ALTER TABLE `transaksi_payment_detail`
  MODIFY `id_payment_detail` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `transaksi_retail`
--
ALTER TABLE `transaksi_retail`
  MODIFY `id_trans` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `transaksi_retail_detail`
--
ALTER TABLE `transaksi_retail_detail`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `transaksi_type`
--
ALTER TABLE `transaksi_type`
  MODIFY `type_trans` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_kavling`
--
ALTER TABLE `user_kavling`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `user_login_data`
--
ALTER TABLE `user_login_data`
  MODIFY `user_id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
