CREATE DATABASE  IF NOT EXISTS `menara_santosa` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `menara_santosa`;
-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: menara_santosa
-- ------------------------------------------------------
-- Server version	5.7.35-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cc_category`
--

DROP TABLE IF EXISTS `cc_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cc_category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cc_category`
--

LOCK TABLES `cc_category` WRITE;
/*!40000 ALTER TABLE `cc_category` DISABLE KEYS */;
INSERT INTO `cc_category` VALUES (1,'Customer Care','Y',NULL,NULL,NULL,NULL),(2,'Perbaikan Rumah','Y',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `cc_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cc_ticket`
--

DROP TABLE IF EXISTS `cc_ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cc_ticket` (
  `ticket_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_num` varchar(20) DEFAULT NULL,
  `ticket_date` datetime NOT NULL,
  `request_by` int(10) unsigned NOT NULL,
  `kavling_id` int(11) unsigned DEFAULT NULL,
  `kompleks_id` int(10) unsigned NOT NULL,
  `subject` varchar(200) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `description` varchar(3000) DEFAULT NULL,
  `is_mtc` enum('Y','N') DEFAULT 'N',
  `mtc_detail` varchar(3000) DEFAULT NULL,
  `mtc_cost` double(15,2) DEFAULT NULL,
  `mtc_attach_file` varchar(20) DEFAULT NULL,
  `mtc_approval` varchar(15) DEFAULT NULL,
  `mtc_approval_date` datetime DEFAULT NULL,
  `status` enum('submit','closed','admin_response','user_response','request_approval','approved','rejected') DEFAULT NULL,
  `status_date` datetime DEFAULT NULL,
  `status_by` int(11) DEFAULT NULL,
  `is_closed` enum('Y','N') DEFAULT 'N',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ticket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cc_ticket`
--

LOCK TABLES `cc_ticket` WRITE;
/*!40000 ALTER TABLE `cc_ticket` DISABLE KEYS */;
INSERT INTO `cc_ticket` VALUES (1,'MS21070001','2021-07-23 12:08:11',2,2,1,'Rumah Bocor',2,'Kamar Depan bocor saat hujan lebat kemarin. Air merembes di dinding\n\nMohon untuk kirim tukang untuk pengecekan.\n\nTerima Kasih.','Y','Selamat Sore, \r\n\r\nDari hasil pengecekan dilapangan, ada biaya perbaikan sebesar Rp 320.000 dengan rincian sebagai berikut: \r\n- Aquaproof: 200.000\r\n- Serat Fiber: 20.000\r\n- Jasa: 100.000\r\n\r\nTerima kasih',320000.00,NULL,NULL,NULL,'request_approval','2021-07-23 12:45:13',1,'N','Y',2,'2021-07-23 12:08:11',2,'2021-07-23 12:08:11'),(2,'MS21070002','2021-07-23 13:53:43',2,2,1,'Sampah 3 hari tidak diambil',1,'Sampah Rumah saya sudah 3 hari tidak diambil, bau busuk. Mohon diperhatikan mengenai petugas kebersihan.','N',NULL,NULL,NULL,NULL,NULL,'submit',NULL,2,'N','Y',2,'2021-07-23 13:53:43',2,'2021-07-23 13:53:43'),(3,'MS21070003','2021-07-23 16:46:47',2,2,1,'Saluran PDAM Mati',1,'Saluran PDAM dari kemaren malam mati, sampai sekarang masih belum nyala. mohon dibantu cek.\n\nTq','N',NULL,NULL,NULL,NULL,NULL,'submit',NULL,2,'N','Y',2,'2021-07-23 16:46:47',2,'2021-07-23 16:46:47'),(4,'MS21070004','2021-07-23 16:49:39',2,2,1,'Jalan Berlubang',1,'Jalan di depan gang 3 berlubang, membahayakan warga','N',NULL,NULL,NULL,NULL,NULL,'submit',NULL,2,'N','Y',2,'2021-07-23 16:49:39',2,'2021-07-23 16:49:39');
/*!40000 ALTER TABLE `cc_ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cc_ticket_history`
--

DROP TABLE IF EXISTS `cc_ticket_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cc_ticket_history` (
  `history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_id` int(10) unsigned NOT NULL,
  `sent_by` int(11) DEFAULT NULL,
  `send_date` datetime DEFAULT NULL,
  `description` varchar(3000) DEFAULT NULL,
  `is_mtc` enum('Y','N') DEFAULT 'N',
  `mtc_cost` double(15,2) DEFAULT '0.00',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`history_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cc_ticket_history`
--

LOCK TABLES `cc_ticket_history` WRITE;
/*!40000 ALTER TABLE `cc_ticket_history` DISABLE KEYS */;
INSERT INTO `cc_ticket_history` VALUES (1,1,2,'2021-07-23 12:08:11','Kamar Depan bocor saat hujan lebat kemarin. Air merembes di dinding\n\nMohon untuk kirim tukang untuk pengecekan.\n\nTerima Kasih.','N',0.00,'Y',2,'2021-07-23 12:08:11',2,'2021-07-23 12:08:11'),(2,1,1,'2021-07-23 12:41:28','Selamat Pagi, \r\n\r\nTim kami akan datang ke rumah Anda pada Sabtu, 23 Juli 2021 pukul 15.00 - 16.00 \r\n\r\n\r\nTerima kasih,\r\nAndini','N',NULL,'Y',1,'2021-07-23 12:41:28',1,'2021-07-23 12:41:28'),(3,1,1,'2021-07-23 12:45:13','Selamat Sore, \r\n\r\nDari hasil pengecekan dilapangan, ada biaya perbaikan sebesar Rp 320.000 dengan rincian sebagai berikut: \r\n- Aquaproof: 200.000\r\n- Serat Fiber: 20.000\r\n- Jasa: 100.000\r\n\r\nTerima kasih','Y',320000.00,'Y',1,'2021-07-23 12:45:13',1,'2021-07-23 12:45:13'),(4,2,2,'2021-07-23 13:53:43','Sampah Rumah saya sudah 3 hari tidak diambil, bau busuk. Mohon diperhatikan mengenai petugas kebersihan.','N',0.00,'Y',2,'2021-07-23 13:53:43',2,'2021-07-23 13:53:43'),(5,3,2,'2021-07-23 16:46:47','Saluran PDAM dari kemaren malam mati, sampai sekarang masih belum nyala. mohon dibantu cek.\n\nTq','N',0.00,'Y',2,'2021-07-23 16:46:47',2,'2021-07-23 16:46:47'),(6,4,2,'2021-07-23 16:49:39','Jalan di depan gang 3 berlubang, membahayakan warga','N',0.00,'Y',2,'2021-07-23 16:49:39',2,'2021-07-23 16:49:39');
/*!40000 ALTER TABLE `cc_ticket_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cc_ticket_history_attach`
--

DROP TABLE IF EXISTS `cc_ticket_history_attach`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cc_ticket_history_attach` (
  `attach_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `history_id` int(10) unsigned DEFAULT NULL,
  `ticket_id` int(10) unsigned DEFAULT NULL,
  `seqno` tinyint(4) DEFAULT NULL,
  `attach_file` varchar(100) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`attach_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cc_ticket_history_attach`
--

LOCK TABLES `cc_ticket_history_attach` WRITE;
/*!40000 ALTER TABLE `cc_ticket_history_attach` DISABLE KEYS */;
INSERT INTO `cc_ticket_history_attach` VALUES (1,1,1,1,'d6c79c12e2b2a6456d847e4f82c0a69c.jpg','Y',2,'2021-07-23 12:08:11',2,'2021-07-23 12:08:11'),(3,1,1,3,'5ff6805378fea5b2a43d79771cd71d84.jpg','Y',2,'2021-07-23 12:08:11',2,'2021-07-23 12:08:11'),(4,1,1,4,'ca64f3aa520e9e526bfd2464de26c853.jpg','Y',2,'2021-07-23 12:08:11',2,'2021-07-23 12:08:11');
/*!40000 ALTER TABLE `cc_ticket_history_attach` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_bank`
--

DROP TABLE IF EXISTS `master_bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_bank` (
  `bank_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(100) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` smallint(6) DEFAULT '0',
  `creation_datetime` datetime DEFAULT NULL,
  `last_update_by` smallint(6) DEFAULT '0',
  `last_updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_bank`
--

LOCK TABLES `master_bank` WRITE;
/*!40000 ALTER TABLE `master_bank` DISABLE KEYS */;
INSERT INTO `master_bank` VALUES (1,'Bank Mandiri','Y',1,NULL,0,NULL),(2,'Bank Permata','Y',1,NULL,0,NULL),(3,'Bank BNI','Y',1,NULL,0,NULL),(4,'Bank BRI','Y',1,NULL,0,NULL);
/*!40000 ALTER TABLE `master_bank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_blok`
--

DROP TABLE IF EXISTS `master_blok`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_blok` (
  `blok_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `kompleks_id` bigint(10) DEFAULT '0',
  `blok_name` varchar(45) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`blok_id`),
  UNIQUE KEY `blok_name_UNIQUE` (`blok_name`),
  KEY `secondary` (`kompleks_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_blok`
--

LOCK TABLES `master_blok` WRITE;
/*!40000 ALTER TABLE `master_blok` DISABLE KEYS */;
INSERT INTO `master_blok` VALUES (1,1,'apel','Y',1,'2021-07-07 01:02:58',1,'2021-07-07 01:04:10'),(2,1,'jeruk','N',1,'2021-07-07 01:02:58',1,'2021-07-07 01:04:10'),(3,2,'KAV 1','Y',1,'2021-08-03 23:00:22',1,'2021-08-03 23:00:37'),(4,2,'KAV 2','N',1,'2021-08-03 23:00:22',1,'2021-08-03 23:00:37');
/*!40000 ALTER TABLE `master_blok` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_group`
--

DROP TABLE IF EXISTS `master_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_group` (
  `group_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL DEFAULT '',
  `group_description` varchar(50) NOT NULL DEFAULT '',
  `group_type` tinyint(3) DEFAULT '0',
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_group`
--

LOCK TABLES `master_group` WRITE;
/*!40000 ALTER TABLE `master_group` DISABLE KEYS */;
INSERT INTO `master_group` VALUES (1,'ADMIN','GROUP ADMIN AAAA',2,'Y',NULL,NULL,1,'2021-08-01 23:45:44'),(2,'LAND LORD','',1,'Y',NULL,NULL,NULL,NULL),(3,'RESIDENT','',0,'Y',1,'2021-07-20 23:54:54',NULL,NULL),(7,'AAA','AAAA',0,'Y',1,'2021-08-01 23:48:49',NULL,NULL),(11,'ASDF','ASD',0,'Y',1,'2021-08-01 23:53:16',NULL,NULL);
/*!40000 ALTER TABLE `master_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_item`
--

DROP TABLE IF EXISTS `master_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_item` (
  `item_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_name` varchar(100) DEFAULT '',
  `unit_id` smallint(6) DEFAULT '0',
  `item_hpp` double DEFAULT '0',
  `item_price` double DEFAULT '0',
  `qty` double DEFAULT '0',
  `item_jasa` enum('Y','N') DEFAULT 'N',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_item`
--

LOCK TABLES `master_item` WRITE;
/*!40000 ALTER TABLE `master_item` DISABLE KEYS */;
INSERT INTO `master_item` VALUES (1,'TABUNG GAS 15KG',1,0,20000,28,'N','Y',1,'2021-07-09 23:45:37',0,NULL),(2,'TABUNG GAS 10KG',2,1327.84,5000,35,'N','Y',1,'2021-07-20 10:25:14',0,NULL),(3,'JASA CAT TEMBOK',4,10000,20000,0,'Y','Y',1,'2021-07-28 12:08:07',1,'2021-08-04 23:54:24'),(4,'JASA BERSIH BERSIH',5,0,50000,24,'N','Y',1,'2021-08-05 00:17:49',1,'2021-08-05 00:17:56');
/*!40000 ALTER TABLE `master_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_kavling`
--

DROP TABLE IF EXISTS `master_kavling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_kavling` (
  `kavling_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `kompleks_id` bigint(10) DEFAULT '0',
  `blok_id` bigint(10) DEFAULT '0',
  `house_no` varchar(45) DEFAULT '',
  `biaya_ipl` double DEFAULT '0',
  `durasi_pembayaran` int(10) DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`kavling_id`),
  KEY `secondary` (`kompleks_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_kavling`
--

LOCK TABLES `master_kavling` WRITE;
/*!40000 ALTER TABLE `master_kavling` DISABLE KEYS */;
INSERT INTO `master_kavling` VALUES (1,1,1,'F0100',250000,2,'Y',1,'2021-07-07 23:26:38',1,'2021-08-03 23:40:43'),(2,1,1,'20',20000,3,'Y',1,'2021-07-07 23:36:52',0,NULL),(3,2,3,'55',1500,10,'Y',1,'2021-08-03 23:48:49',0,NULL);
/*!40000 ALTER TABLE `master_kavling` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_kompleks`
--

DROP TABLE IF EXISTS `master_kompleks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_kompleks` (
  `kompleks_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `kompleks_name` varchar(50) DEFAULT '',
  `kompleks_address` varchar(200) DEFAULT '',
  `biaya_ipl` double DEFAULT '0',
  `durasi_pembayaran` int(10) DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`kompleks_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_kompleks`
--

LOCK TABLES `master_kompleks` WRITE;
/*!40000 ALTER TABLE `master_kompleks` DISABLE KEYS */;
INSERT INTO `master_kompleks` VALUES (1,'SYAILENDRA','PURWOSARI',20000,3,'Y',1,'2021-07-07 01:02:58',1,'2021-07-07 01:04:10'),(2,'PERMATA','AAAAAAAAAASSSSSS',5000,20,'Y',1,'2021-08-03 23:00:22',1,'2021-08-03 23:00:37');
/*!40000 ALTER TABLE `master_kompleks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_module`
--

DROP TABLE IF EXISTS `master_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_module` (
  `module_id` smallint(6) NOT NULL,
  `module_name` varchar(100) DEFAULT '',
  `module_features` tinyint(3) unsigned DEFAULT '0',
  `module_active` tinyint(3) unsigned DEFAULT '1',
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_module`
--

LOCK TABLES `master_module` WRITE;
/*!40000 ALTER TABLE `master_module` DISABLE KEYS */;
INSERT INTO `master_module` VALUES (1,'MANAJEMEN SISTEM',1,1),(2,'TAMBAH / HAPUS GROUP USER',2,1),(3,'TAMBAH / HAPUS USER',2,1),(4,'PENGATURAN AKSES MODUL',1,1),(5,'BACKUP / RESTORE DATABASE',1,0),(6,'PENGATURAN SISTEM APLIKASI',1,1),(21,'TAMBAH / HAPUS KOMPLEKS',2,1),(22,'TAMBAH / HAPUS KAVLING',2,1),(23,'TAMBAH / HAPUS ITEM',2,1),(24,'TAMBAH / HAPUS SATUAN',2,1),(25,'TAMBAH / HAPUS PEMILIK KAVLING',1,1),(41,'RINGKASAN IPL',1,1),(42,'TAMBAH / HAPUS TRANSAKSI RETAIL',2,1),(43,'PENYESUAIAN STOK',1,1),(44,'PENERIMAAN BARANG',2,1);
/*!40000 ALTER TABLE `master_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_module_access`
--

DROP TABLE IF EXISTS `master_module_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_module_access` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` tinyint(3) unsigned NOT NULL,
  `module_id` smallint(6) NOT NULL,
  `user_access_option` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_module_access`
--

LOCK TABLES `master_module_access` WRITE;
/*!40000 ALTER TABLE `master_module_access` DISABLE KEYS */;
INSERT INTO `master_module_access` VALUES (1,1,1,1),(2,1,2,6),(3,1,3,6),(4,1,4,1),(5,1,5,1),(6,1,6,1),(7,1,21,7),(8,1,22,7),(9,1,23,7),(10,1,24,7),(11,1,25,1),(12,1,41,1),(13,1,42,7),(14,1,43,1),(15,1,44,7),(16,7,1,1),(17,7,2,7),(18,7,3,7),(19,7,4,1),(20,7,6,1),(21,7,21,7),(22,7,22,7),(23,7,23,7),(24,7,24,7),(25,7,25,1),(26,7,41,1),(27,7,42,7),(28,7,43,1),(29,7,44,7);
/*!40000 ALTER TABLE `master_module_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_payment_mode`
--

DROP TABLE IF EXISTS `master_payment_mode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_payment_mode` (
  `payment_type` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_type_name` varchar(100) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  PRIMARY KEY (`payment_type`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_payment_mode`
--

LOCK TABLES `master_payment_mode` WRITE;
/*!40000 ALTER TABLE `master_payment_mode` DISABLE KEYS */;
INSERT INTO `master_payment_mode` VALUES (1,'Bank Transfer','Y'),(2,'Gopay','Y'),(3,'ShopeePay','Y'),(4,'Indomaret','Y'),(5,'Alfa Group','Y');
/*!40000 ALTER TABLE `master_payment_mode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_unit`
--

DROP TABLE IF EXISTS `master_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_unit` (
  `unit_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(100) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_unit`
--

LOCK TABLES `master_unit` WRITE;
/*!40000 ALTER TABLE `master_unit` DISABLE KEYS */;
INSERT INTO `master_unit` VALUES (1,'PCS','Y',1,'2021-07-09 23:32:08',0,NULL),(2,'KG','Y',1,'2021-07-09 23:32:15',1,'2021-07-09 23:40:01'),(3,'BOX','Y',1,'2021-07-09 23:40:15',0,NULL),(4,'JAM','Y',1,'2021-07-28 12:05:20',0,NULL),(5,'HARI AAAA','Y',1,'2021-08-05 00:17:17',1,'2021-08-05 00:17:23');
/*!40000 ALTER TABLE `master_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `messages` (
  `language_code` varchar(10) NOT NULL,
  `code` varchar(50) NOT NULL,
  `message` varchar(200) NOT NULL,
  PRIMARY KEY (`language_code`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES ('in','ACCESS_VIOLATION','Anda tidak memiliki hak akses'),('in','CHANGE_PASSWORD_FAILED','Password Lama salah, mohon periksa kembali'),('in','CHANGE_PASSWORD_SUCCESS','Password berhasil diganti'),('in','DELETE_SUCCESS','Data berhasil dihapus'),('in','LOGIN_FAILED','Username / Password salah, mohon ulangi kembali'),('in','NOT_FOUND','Data tidak ditemukan'),('in','SAVE_SUCCESS','Data berhasil disimpan'),('in','TROUBLE','Terjadi gangguan pada sistem, hubungi tim support kami'),('in','USER_DUPLICATE','Username sudah digunakan');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `penerimaan_detail`
--

DROP TABLE IF EXISTS `penerimaan_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `penerimaan_detail` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `penerimaan_id` bigint(10) DEFAULT '0',
  `item_id` bigint(10) DEFAULT '0',
  `item_qty` double DEFAULT '0',
  `item_hpp` double DEFAULT '0',
  `item_subtotal` double DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `penerimaan_detail`
--

LOCK TABLES `penerimaan_detail` WRITE;
/*!40000 ALTER TABLE `penerimaan_detail` DISABLE KEYS */;
INSERT INTO `penerimaan_detail` VALUES (1,1,2,5,5000,25000,'Y',1,'2021-07-27 23:15:57',1,'2021-07-27 23:23:27'),(2,1,1,10,20000,200000,'Y',1,'2021-07-27 23:15:57',1,'2021-07-27 23:23:27'),(3,2,2,10,2000,20000,'Y',1,'2021-07-28 13:25:24',1,'2021-07-28 13:34:54');
/*!40000 ALTER TABLE `penerimaan_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `penerimaan_header`
--

DROP TABLE IF EXISTS `penerimaan_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `penerimaan_header` (
  `penerimaan_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `penerimaan_datetime` datetime DEFAULT NULL,
  `penerimaan_total` double DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`penerimaan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `penerimaan_header`
--

LOCK TABLES `penerimaan_header` WRITE;
/*!40000 ALTER TABLE `penerimaan_header` DISABLE KEYS */;
INSERT INTO `penerimaan_header` VALUES (1,'2021-07-27 23:15:45',225000,'Y',1,'2021-07-27 23:15:57',1,'2021-07-27 23:23:27'),(2,'2021-07-28 13:25:11',20000,'Y',1,'2021-07-28 13:25:21',1,'2021-07-28 13:34:52');
/*!40000 ALTER TABLE `penerimaan_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `penyesuaian_stok`
--

DROP TABLE IF EXISTS `penyesuaian_stok`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `penyesuaian_stok` (
  `ID` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` bigint(10) DEFAULT '0',
  `qty_awal` double DEFAULT '0',
  `qty_baru` double DEFAULT '0',
  `keterangan` varchar(200) DEFAULT '',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `penyesuaian_stok`
--

LOCK TABLES `penyesuaian_stok` WRITE;
/*!40000 ALTER TABLE `penyesuaian_stok` DISABLE KEYS */;
INSERT INTO `penyesuaian_stok` VALUES (1,2,15,20,'AAA',1,'2021-07-27 23:13:53');
/*!40000 ALTER TABLE `penyesuaian_stok` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `push_notification_parameters`
--

DROP TABLE IF EXISTS `push_notification_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `push_notification_parameters` (
  `push_id` int(10) unsigned NOT NULL,
  `key_label` varchar(100) NOT NULL,
  `key_value` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`push_id`,`key_label`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `push_notification_parameters`
--

LOCK TABLES `push_notification_parameters` WRITE;
/*!40000 ALTER TABLE `push_notification_parameters` DISABLE KEYS */;
INSERT INTO `push_notification_parameters` VALUES (1,'message','Betul Pak, berikut rincian anya'),(1,'ticket_id','3'),(1,'title','Reply #MS21070001'),(2,'message','Baik pak, tim kami akan mengecek besok pukul 10.00'),(2,'ticket_id','4'),(2,'title','Reply #MS21070002'),(3,'message','Setelah ada pengecekan, ada biaya jasa untuk pembersihan selokan'),(3,'ticket_id','4'),(3,'title','Reply #MS21070002'),(4,'message','Terima kasih, kami akan mengirim teknisi kesana'),(4,'ticket_id','6'),(4,'title','Reply #MS21070004'),(5,'message','Biaya Perbaikan sebesar Rp 250.000'),(5,'ticket_id','6'),(5,'title','Reply #MS21070004'),(6,'message','Ada perbaikan'),(6,'ticket_id','5'),(6,'title','Reply #MS21070003'),(7,'message','Selamat Pagi, Tim kami akan datang ke rumah Anda pada Sabtu, 23 Juli 2021 pukul 15.00 - 16.00 Terima kasih'),(7,'ticket_id','1'),(7,'title','Reply #MS21070001'),(8,'message','Selamat Sore, Dari hasil pengecekan dilapangan, ada biaya perbaikan sebesar Rp 320.000 dengan rincian sebagai berikut: Aquaproof: Rp 200.000 Serat Fiber: Rp. 20.000 Jasa: 100.000'),(8,'ticket_id','1'),(8,'title','Reply #MS21070001');
/*!40000 ALTER TABLE `push_notification_parameters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `push_notifications`
--

DROP TABLE IF EXISTS `push_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `push_notifications` (
  `push_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `push_type` varchar(30) DEFAULT NULL,
  `fcm_token` varchar(300) DEFAULT NULL,
  `is_sent` enum('Y','N') DEFAULT 'N',
  `send_date` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  PRIMARY KEY (`push_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `push_notifications`
--

LOCK TABLES `push_notifications` WRITE;
/*!40000 ALTER TABLE `push_notifications` DISABLE KEYS */;
INSERT INTO `push_notifications` VALUES (1,'admin_reply',NULL,'N',NULL,'Y'),(2,'admin_reply',NULL,'N',NULL,'Y'),(3,'admin_reply',NULL,'N',NULL,'Y'),(4,'admin_reply',NULL,'N',NULL,'Y'),(5,'admin_reply',NULL,'N',NULL,'Y'),(6,'admin_reply',NULL,'N',NULL,'Y'),(7,'admin_reply',NULL,'N',NULL,'Y'),(8,'admin_reply',NULL,'N',NULL,'Y');
/*!40000 ALTER TABLE `push_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_config`
--

DROP TABLE IF EXISTS `sys_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_config` (
  `ID` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(50) DEFAULT '',
  `company_address` varchar(100) DEFAULT '',
  `company_phone` varchar(20) DEFAULT '',
  `company_email` varchar(50) DEFAULT '',
  `default_printer` varchar(200) DEFAULT '',
  `auto_backup_flag` tinyint(3) unsigned DEFAULT '0',
  `auto_backup_dir` varchar(200) DEFAULT '',
  `print_preview` tinyint(3) unsigned DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_config`
--

LOCK TABLES `sys_config` WRITE;
/*!40000 ALTER TABLE `sys_config` DISABLE KEYS */;
INSERT INTO `sys_config` VALUES (1,'PT. MENARA SANTOSA','JL. RONGGOWARSITO NO.73, KEPRABON','(0271) 643100','aaaa','PrimoPDF',0,'',0,'Y',0,NULL,1,'2021-08-03 22:01:18');
/*!40000 ALTER TABLE `sys_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_config_transaksi`
--

DROP TABLE IF EXISTS `sys_config_transaksi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_config_transaksi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_type` int(11) DEFAULT '0',
  `biaya_transaksi` double DEFAULT '0',
  `biaya_transaksi_percent` double DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_config_transaksi`
--

LOCK TABLES `sys_config_transaksi` WRITE;
/*!40000 ALTER TABLE `sys_config_transaksi` DISABLE KEYS */;
INSERT INTO `sys_config_transaksi` VALUES (1,1,4000,0,'Y',1,'2021-07-20 23:19:58',1,'2021-08-03 22:01:18'),(2,2,100,2,'Y',1,'2021-07-20 23:28:35',1,'2021-08-03 22:01:18'),(3,3,100,1.5,'Y',1,'2021-07-20 23:28:35',1,'2021-08-03 22:01:18'),(4,4,100,0,'Y',1,'2021-07-20 23:28:35',1,'2021-08-03 22:01:18'),(5,5,5000,0,'Y',1,'2021-07-20 23:28:35',1,'2021-08-03 22:01:18');
/*!40000 ALTER TABLE `sys_config_transaksi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblusertocheckedrecords`
--

DROP TABLE IF EXISTS `tblusertocheckedrecords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblusertocheckedrecords` (
  `recID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL DEFAULT '0',
  `query` text CHARACTER SET utf8 NOT NULL,
  `uniqueKey` varchar(30) NOT NULL DEFAULT '',
  `createdDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` int(11) NOT NULL DEFAULT '0',
  `payment_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`recID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblusertocheckedrecords`
--

LOCK TABLES `tblusertocheckedrecords` WRITE;
/*!40000 ALTER TABLE `tblusertocheckedrecords` DISABLE KEYS */;
INSERT INTO `tblusertocheckedrecords` VALUES (1,1,'','p6g9iq2jbeil18oin3dmd41emn1270','2021-07-21 10:55:55',1,1);
/*!40000 ALTER TABLE `tblusertocheckedrecords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblusertocheckedrecordsdetail`
--

DROP TABLE IF EXISTS `tblusertocheckedrecordsdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblusertocheckedrecordsdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recID` int(11) NOT NULL DEFAULT '0',
  `recordID` int(11) NOT NULL DEFAULT '0',
  `trans_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblusertocheckedrecordsdetail`
--

LOCK TABLES `tblusertocheckedrecordsdetail` WRITE;
/*!40000 ALTER TABLE `tblusertocheckedrecordsdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblusertocheckedrecordsdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_handyman`
--

DROP TABLE IF EXISTS `transaksi_handyman`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksi_handyman` (
  `id_trans` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_issued` datetime DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  `kavling_id` bigint(10) DEFAULT '0',
  `user_id` bigint(10) DEFAULT '0',
  `payment_id` varchar(45) DEFAULT NULL,
  `nominal` double DEFAULT '0',
  `status_id` int(10) DEFAULT '0',
  `date_paid` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_trans`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_handyman`
--

LOCK TABLES `transaksi_handyman` WRITE;
/*!40000 ALTER TABLE `transaksi_handyman` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaksi_handyman` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_ipl`
--

DROP TABLE IF EXISTS `transaksi_ipl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksi_ipl` (
  `id_trans` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_trans` int(10) DEFAULT '0',
  `date_issued` datetime DEFAULT NULL,
  `kavling_id` bigint(10) DEFAULT '0',
  `user_id` bigint(10) DEFAULT '0',
  `payment_id` varchar(45) DEFAULT NULL,
  `nominal` double DEFAULT '0',
  `status_id` int(10) DEFAULT '0',
  `date_paid` datetime DEFAULT NULL,
  `start_ipl` datetime DEFAULT NULL,
  `end_ipl` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_trans`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_ipl`
--

LOCK TABLES `transaksi_ipl` WRITE;
/*!40000 ALTER TABLE `transaksi_ipl` DISABLE KEYS */;
INSERT INTO `transaksi_ipl` VALUES (1,1,'2021-07-15 00:23:05',2,1,'2015949151',60000,0,'2021-07-21 18:16:24','2021-07-09 00:00:00','2021-09-09 00:00:00','Y',1,'2021-07-15 00:23:05',0,NULL);
/*!40000 ALTER TABLE `transaksi_ipl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_ipl_detail`
--

DROP TABLE IF EXISTS `transaksi_ipl_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksi_ipl_detail` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_trans` bigint(10) DEFAULT '0',
  `nominal` double DEFAULT '0',
  `periode_pembayaran` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_ipl_detail`
--

LOCK TABLES `transaksi_ipl_detail` WRITE;
/*!40000 ALTER TABLE `transaksi_ipl_detail` DISABLE KEYS */;
INSERT INTO `transaksi_ipl_detail` VALUES (1,1,20000,'2021-07-09 00:00:00','Y',1,'2021-07-15 00:23:05',0,NULL),(2,1,20000,'2021-08-09 00:00:00','Y',1,'2021-07-15 00:23:05',0,NULL),(3,1,20000,'2021-09-09 00:00:00','Y',1,'2021-07-15 00:23:05',0,NULL);
/*!40000 ALTER TABLE `transaksi_ipl_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_payment`
--

DROP TABLE IF EXISTS `transaksi_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksi_payment` (
  `id_payment` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_issued` datetime DEFAULT NULL,
  `payment_id` varchar(45) DEFAULT '',
  `total` double DEFAULT '0',
  `payment_type` int(11) DEFAULT '0',
  `biaya_transaksi` double DEFAULT '0',
  `status_id` int(10) DEFAULT '0',
  `date_paid` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  `status_code` int(11) NOT NULL DEFAULT '0',
  `transaction_id` varchar(500) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transaction_status` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transaction_time` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `midtrans_payment_type` varchar(50) NOT NULL DEFAULT '',
  `bank_name` varchar(50) NOT NULL DEFAULT '',
  `va_number` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `bill_key` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `biller_code` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `payment_code` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`id_payment`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_payment`
--

LOCK TABLES `transaksi_payment` WRITE;
/*!40000 ALTER TABLE `transaksi_payment` DISABLE KEYS */;
INSERT INTO `transaksi_payment` VALUES (1,'2021-07-21 18:41:36','1446156635',65000,1,4000,1,'2021-07-21 18:41:36','Y',1,'2021-07-21 18:41:36',0,NULL,201,'b67e7e72-d97b-4d39-ba90-e7e691630a25','pending','2021-07-21 18:41:34','bank_transfer','Bni','9883400086116934','','','');
/*!40000 ALTER TABLE `transaksi_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_payment_detail`
--

DROP TABLE IF EXISTS `transaksi_payment_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksi_payment_detail` (
  `id_payment_detail` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_payment` bigint(10) DEFAULT '0',
  `id_trans` bigint(10) DEFAULT '0',
  `type_trans` int(10) DEFAULT '0',
  `total` double DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_payment_detail`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_payment_detail`
--

LOCK TABLES `transaksi_payment_detail` WRITE;
/*!40000 ALTER TABLE `transaksi_payment_detail` DISABLE KEYS */;
INSERT INTO `transaksi_payment_detail` VALUES (1,1,1,2,65000,'Y',1,'2021-07-21 18:41:36',0,NULL);
/*!40000 ALTER TABLE `transaksi_payment_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_retail`
--

DROP TABLE IF EXISTS `transaksi_retail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksi_retail` (
  `id_trans` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_trans` int(10) DEFAULT '0',
  `date_issued` datetime DEFAULT NULL,
  `kavling_id` bigint(10) DEFAULT '0',
  `user_id` bigint(10) DEFAULT '0',
  `payment_id` varchar(45) DEFAULT NULL,
  `nominal` double DEFAULT '0',
  `status_id` int(10) DEFAULT '0',
  `date_paid` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_trans`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_retail`
--

LOCK TABLES `transaksi_retail` WRITE;
/*!40000 ALTER TABLE `transaksi_retail` DISABLE KEYS */;
INSERT INTO `transaksi_retail` VALUES (1,2,'2021-07-20 10:34:28',2,1,'1446156635',65000,1,'2021-07-21 18:41:34','Y',1,'2021-07-19 23:55:41',1,'2021-07-20 10:34:41'),(2,2,'2021-07-27 23:11:34',2,2,NULL,185000,0,NULL,'N',1,'2021-07-27 23:11:21',1,'2021-07-27 23:11:41');
/*!40000 ALTER TABLE `transaksi_retail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_retail_detail`
--

DROP TABLE IF EXISTS `transaksi_retail_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksi_retail_detail` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_trans` bigint(10) DEFAULT '0',
  `item_id` bigint(10) DEFAULT '0',
  `item_qty` double DEFAULT '0',
  `item_hpp` double DEFAULT '0',
  `item_price` double DEFAULT '0',
  `subtotal` double DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_retail_detail`
--

LOCK TABLES `transaksi_retail_detail` WRITE;
/*!40000 ALTER TABLE `transaksi_retail_detail` DISABLE KEYS */;
INSERT INTO `transaksi_retail_detail` VALUES (1,1,1,2,0,20000,40000,'Y',1,'2021-07-19 23:55:41',1,'2021-07-20 10:34:41'),(2,1,2,5,0,5000,25000,'Y',1,'2021-07-20 10:25:26',1,'2021-07-20 10:34:41'),(3,2,2,5,0,5000,25000,'Y',1,'2021-07-27 23:11:21',0,NULL),(4,2,1,8,0,20000,160000,'Y',1,'2021-07-27 23:11:21',0,NULL);
/*!40000 ALTER TABLE `transaksi_retail_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_kavling`
--

DROP TABLE IF EXISTS `user_kavling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_kavling` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) DEFAULT NULL,
  `kavling_id` bigint(10) DEFAULT NULL,
  `start_ipl` datetime DEFAULT NULL,
  `remarks` varchar(200) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_kavling`
--

LOCK TABLES `user_kavling` WRITE;
/*!40000 ALTER TABLE `user_kavling` DISABLE KEYS */;
INSERT INTO `user_kavling` VALUES (1,1,1,'2021-08-01 15:45:55','AAAAA','N',1,'2021-07-09 15:46:43',1,'2021-07-01 16:14:16'),(2,1,2,'2021-08-01 00:00:00','SSSSSSSSDDDD','Y',1,'2021-07-09 16:16:48',1,'2021-08-04 22:16:43'),(3,2,2,'2021-07-01 00:00:00','DDDDDDD','Y',1,'2021-07-20 23:57:32',1,'2021-08-04 22:16:43'),(4,5,2,'2021-08-01 00:00:00','DFFFFF','N',1,'2021-08-04 21:02:44',1,'2021-08-04 21:10:53'),(5,5,2,'2021-08-01 00:00:00','','N',1,'2021-08-04 22:14:25',1,'2021-08-04 22:16:37'),(6,5,2,'2021-08-01 00:00:00','','Y',1,'2021-08-04 22:16:43',0,NULL);
/*!40000 ALTER TABLE `user_kavling` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_login_data`
--

DROP TABLE IF EXISTS `user_login_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_login_data` (
  `user_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(15) DEFAULT '',
  `user_password` varchar(15) DEFAULT '',
  `group_id` tinyint(3) unsigned DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `user_full_name` varchar(100) DEFAULT '',
  `user_id_type` tinyint(3) DEFAULT '0',
  `user_id_no` varchar(100) DEFAULT '',
  `user_phone_1` varchar(45) DEFAULT '',
  `user_phone_2` varchar(45) DEFAULT '',
  `user_email_address` varchar(200) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `SECONDARY` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_login_data`
--

LOCK TABLES `user_login_data` WRITE;
/*!40000 ALTER TABLE `user_login_data` DISABLE KEYS */;
INSERT INTO `user_login_data` VALUES (1,'ADMIN','admin',1,'2021-08-05 00:15:52','2021-08-05 00:18:14','ADMIN1234567',1,'123456','081558775652','0111','ARISTON@ALPHASOFT.COM','Y',NULL,NULL,1,'2021-08-02 23:34:31'),(2,'RESIDENT','123',3,NULL,NULL,'Resident',1,'','0123','','','Y',1,'2021-07-20 23:55:19',NULL,NULL),(4,'LANDLORD','123',2,NULL,NULL,'AAAA',1,'','000','','','Y',1,'2021-08-04 20:52:17',NULL,NULL),(5,'ANDRI','admin',3,NULL,NULL,'AAA',1,'','000','','','Y',1,'2021-08-04 20:56:58',NULL,NULL);
/*!40000 ALTER TABLE `user_login_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_token`
--

DROP TABLE IF EXISTS `user_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_token` (
  `user_id` bigint(10) unsigned NOT NULL DEFAULT '0',
  `access_token` varchar(300) DEFAULT NULL,
  `fcm_token` varchar(300) DEFAULT NULL,
  `gcm_token` varchar(300) DEFAULT NULL,
  `ios_token` varchar(300) DEFAULT NULL,
  `web_token` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_token`
--

LOCK TABLES `user_token` WRITE;
/*!40000 ALTER TABLE `user_token` DISABLE KEYS */;
INSERT INTO `user_token` VALUES (1,'OQP0UY8ZUX','1','1','1','1'),(2,'NRVX4QOF8D','2','2','2','2'),(4,'NRVX4QOF8R',NULL,NULL,NULL,NULL),(5,'DUL0G6MLST',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user_token` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-05  0:28:17
