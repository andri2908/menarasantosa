-- MySQL dump 10.13  Distrib 5.6.44, for Win64 (x86_64)
--
-- Host: localhost    Database: menara_santosa
-- ------------------------------------------------------
-- Server version	5.6.44-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `menara_santosa`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `menara_santosa` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `menara_santosa`;

--
-- Table structure for table `cc_category`
--

DROP TABLE IF EXISTS `cc_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cc_category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cc_category`
--

LOCK TABLES `cc_category` WRITE;
/*!40000 ALTER TABLE `cc_category` DISABLE KEYS */;
INSERT INTO `cc_category` VALUES (1,'Customer Care','Y',NULL,NULL,NULL,NULL),(2,'Perbaikan Rumah','Y',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `cc_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cc_ticket`
--

DROP TABLE IF EXISTS `cc_ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cc_ticket` (
  `ticket_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_num` varchar(20) DEFAULT NULL,
  `ticket_date` datetime NOT NULL,
  `request_by` int(10) unsigned NOT NULL,
  `kavling_id` int(11) unsigned DEFAULT NULL,
  `kompleks_id` int(10) unsigned NOT NULL,
  `subject` varchar(200) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `description` varchar(3000) DEFAULT NULL,
  `is_mtc` enum('Y','N') DEFAULT 'N',
  `mtc_detail` varchar(3000) DEFAULT NULL,
  `mtc_cost` double(15,2) DEFAULT NULL,
  `mtc_attach_file` varchar(20) DEFAULT NULL,
  `mtc_approval` varchar(15) DEFAULT NULL,
  `mtc_approval_date` datetime DEFAULT NULL,
  `status` enum('submit','closed','admin_response','user_response','request_approval','approved','rejected') DEFAULT NULL,
  `status_date` datetime DEFAULT NULL,
  `status_by` int(11) DEFAULT NULL,
  `is_closed` enum('Y','N') DEFAULT 'N',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ticket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cc_ticket`
--

LOCK TABLES `cc_ticket` WRITE;
/*!40000 ALTER TABLE `cc_ticket` DISABLE KEYS */;
INSERT INTO `cc_ticket` VALUES (1,'MS21070001','2021-07-23 12:08:11',2,2,1,'Rumah Bocor',2,'Kamar Depan bocor saat hujan lebat kemarin. Air merembes di dinding\n\nMohon untuk kirim tukang untuk pengecekan.\n\nTerima Kasih.','Y','Selamat Sore, \r\n\r\nDari hasil pengecekan dilapangan, ada biaya perbaikan sebesar Rp 320.000 dengan rincian sebagai berikut: \r\n- Aquaproof: 200.000\r\n- Serat Fiber: 20.000\r\n- Jasa: 100.000\r\n\r\nTerima kasih',320000.00,NULL,NULL,NULL,'request_approval','2021-07-23 12:45:13',1,'N','Y',2,'2021-07-23 12:08:11',2,'2021-07-23 12:08:11'),(2,'MS21070002','2021-07-23 13:53:43',2,2,1,'Sampah 3 hari tidak diambil',1,'Sampah Rumah saya sudah 3 hari tidak diambil, bau busuk. Mohon diperhatikan mengenai petugas kebersihan.','N',NULL,NULL,NULL,NULL,NULL,'submit',NULL,2,'N','Y',2,'2021-07-23 13:53:43',2,'2021-07-23 13:53:43'),(3,'MS21070003','2021-07-23 16:46:47',2,2,1,'Saluran PDAM Mati',1,'Saluran PDAM dari kemaren malam mati, sampai sekarang masih belum nyala. mohon dibantu cek.\n\nTq','N',NULL,NULL,NULL,NULL,NULL,'submit',NULL,2,'N','Y',2,'2021-07-23 16:46:47',2,'2021-07-23 16:46:47'),(4,'MS21070004','2021-07-23 16:49:39',2,2,1,'Jalan Berlubang',1,'Jalan di depan gang 3 berlubang, membahayakan warga','N',NULL,NULL,NULL,NULL,NULL,'submit',NULL,2,'N','Y',2,'2021-07-23 16:49:39',2,'2021-07-23 16:49:39');
/*!40000 ALTER TABLE `cc_ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cc_ticket_history`
--

DROP TABLE IF EXISTS `cc_ticket_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cc_ticket_history` (
  `history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_id` int(10) unsigned NOT NULL,
  `sent_by` int(11) DEFAULT NULL,
  `send_date` datetime DEFAULT NULL,
  `description` varchar(3000) DEFAULT NULL,
  `is_mtc` enum('Y','N') DEFAULT 'N',
  `mtc_cost` double(15,2) DEFAULT '0.00',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`history_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cc_ticket_history`
--

LOCK TABLES `cc_ticket_history` WRITE;
/*!40000 ALTER TABLE `cc_ticket_history` DISABLE KEYS */;
INSERT INTO `cc_ticket_history` VALUES (1,1,2,'2021-07-23 12:08:11','Kamar Depan bocor saat hujan lebat kemarin. Air merembes di dinding\n\nMohon untuk kirim tukang untuk pengecekan.\n\nTerima Kasih.','N',0.00,'Y',2,'2021-07-23 12:08:11',2,'2021-07-23 12:08:11'),(2,1,1,'2021-07-23 12:41:28','Selamat Pagi, \r\n\r\nTim kami akan datang ke rumah Anda pada Sabtu, 23 Juli 2021 pukul 15.00 - 16.00 \r\n\r\n\r\nTerima kasih,\r\nAndini','N',NULL,'Y',1,'2021-07-23 12:41:28',1,'2021-07-23 12:41:28'),(3,1,1,'2021-07-23 12:45:13','Selamat Sore, \r\n\r\nDari hasil pengecekan dilapangan, ada biaya perbaikan sebesar Rp 320.000 dengan rincian sebagai berikut: \r\n- Aquaproof: 200.000\r\n- Serat Fiber: 20.000\r\n- Jasa: 100.000\r\n\r\nTerima kasih','Y',320000.00,'Y',1,'2021-07-23 12:45:13',1,'2021-07-23 12:45:13'),(4,2,2,'2021-07-23 13:53:43','Sampah Rumah saya sudah 3 hari tidak diambil, bau busuk. Mohon diperhatikan mengenai petugas kebersihan.','N',0.00,'Y',2,'2021-07-23 13:53:43',2,'2021-07-23 13:53:43'),(5,3,2,'2021-07-23 16:46:47','Saluran PDAM dari kemaren malam mati, sampai sekarang masih belum nyala. mohon dibantu cek.\n\nTq','N',0.00,'Y',2,'2021-07-23 16:46:47',2,'2021-07-23 16:46:47'),(6,4,2,'2021-07-23 16:49:39','Jalan di depan gang 3 berlubang, membahayakan warga','N',0.00,'Y',2,'2021-07-23 16:49:39',2,'2021-07-23 16:49:39');
/*!40000 ALTER TABLE `cc_ticket_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cc_ticket_history_attach`
--

DROP TABLE IF EXISTS `cc_ticket_history_attach`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cc_ticket_history_attach` (
  `attach_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `history_id` int(10) unsigned DEFAULT NULL,
  `ticket_id` int(10) unsigned DEFAULT NULL,
  `seqno` tinyint(4) DEFAULT NULL,
  `attach_file` varchar(100) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`attach_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cc_ticket_history_attach`
--

LOCK TABLES `cc_ticket_history_attach` WRITE;
/*!40000 ALTER TABLE `cc_ticket_history_attach` DISABLE KEYS */;
INSERT INTO `cc_ticket_history_attach` VALUES (1,1,1,1,'d6c79c12e2b2a6456d847e4f82c0a69c.jpg','Y',2,'2021-07-23 12:08:11',2,'2021-07-23 12:08:11'),(3,1,1,3,'5ff6805378fea5b2a43d79771cd71d84.jpg','Y',2,'2021-07-23 12:08:11',2,'2021-07-23 12:08:11'),(4,1,1,4,'ca64f3aa520e9e526bfd2464de26c853.jpg','Y',2,'2021-07-23 12:08:11',2,'2021-07-23 12:08:11');
/*!40000 ALTER TABLE `cc_ticket_history_attach` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_blok`
--

--
-- Table structure for table `push_notification_parameters`
--

DROP TABLE IF EXISTS `push_notification_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `push_notification_parameters` (
  `push_id` int(10) unsigned NOT NULL,
  `key_label` varchar(100) NOT NULL,
  `key_value` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`push_id`,`key_label`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `push_notification_parameters`
--

LOCK TABLES `push_notification_parameters` WRITE;
/*!40000 ALTER TABLE `push_notification_parameters` DISABLE KEYS */;
INSERT INTO `push_notification_parameters` VALUES (1,'message','Betul Pak, berikut rincian anya'),(1,'ticket_id','3'),(1,'title','Reply #MS21070001'),(2,'message','Baik pak, tim kami akan mengecek besok pukul 10.00'),(2,'ticket_id','4'),(2,'title','Reply #MS21070002'),(3,'message','Setelah ada pengecekan, ada biaya jasa untuk pembersihan selokan'),(3,'ticket_id','4'),(3,'title','Reply #MS21070002'),(4,'message','Terima kasih, kami akan mengirim teknisi kesana'),(4,'ticket_id','6'),(4,'title','Reply #MS21070004'),(5,'message','Biaya Perbaikan sebesar Rp 250.000'),(5,'ticket_id','6'),(5,'title','Reply #MS21070004'),(6,'message','Ada perbaikan'),(6,'ticket_id','5'),(6,'title','Reply #MS21070003'),(7,'message','Selamat Pagi, Tim kami akan datang ke rumah Anda pada Sabtu, 23 Juli 2021 pukul 15.00 - 16.00 Terima kasih'),(7,'ticket_id','1'),(7,'title','Reply #MS21070001'),(8,'message','Selamat Sore, Dari hasil pengecekan dilapangan, ada biaya perbaikan sebesar Rp 320.000 dengan rincian sebagai berikut: Aquaproof: Rp 200.000 Serat Fiber: Rp. 20.000 Jasa: 100.000'),(8,'ticket_id','1'),(8,'title','Reply #MS21070001');
/*!40000 ALTER TABLE `push_notification_parameters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `push_notifications`
--

DROP TABLE IF EXISTS `push_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `push_notifications` (
  `push_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `push_type` varchar(30) DEFAULT NULL,
  `fcm_token` varchar(300) DEFAULT NULL,
  `is_sent` enum('Y','N') DEFAULT 'N',
  `send_date` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  PRIMARY KEY (`push_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `push_notifications`
--

LOCK TABLES `push_notifications` WRITE;
/*!40000 ALTER TABLE `push_notifications` DISABLE KEYS */;
INSERT INTO `push_notifications` VALUES (1,'admin_reply',NULL,'N',NULL,'Y'),(2,'admin_reply',NULL,'N',NULL,'Y'),(3,'admin_reply',NULL,'N',NULL,'Y'),(4,'admin_reply',NULL,'N',NULL,'Y'),(5,'admin_reply',NULL,'N',NULL,'Y'),(6,'admin_reply',NULL,'N',NULL,'Y'),(7,'admin_reply',NULL,'N',NULL,'Y'),(8,'admin_reply',NULL,'N',NULL,'Y');
/*!40000 ALTER TABLE `push_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_handyman`
--

DROP TABLE IF EXISTS `transaksi_handyman`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaksi_handyman` (
  `id_trans` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_issued` datetime DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  `kavling_id` bigint(10) DEFAULT '0',
  `user_id` bigint(10) DEFAULT '0',
  `payment_id` varchar(45) DEFAULT NULL,
  `nominal` double DEFAULT '0',
  `status_id` int(10) DEFAULT '0',
  `date_paid` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_trans`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_handyman`
--

LOCK TABLES `transaksi_handyman` WRITE;
/*!40000 ALTER TABLE `transaksi_handyman` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaksi_handyman` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'menara_santosa'
--
/*!50003 DROP FUNCTION IF EXISTS `access_token_fc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `access_token_fc`(

  p_access_token  varchar(50),

  p_user_id       int unsigned

) RETURNS varchar(1) CHARSET latin1
BEGIN

  

  declare t_val varchar(1) default 'N';



  begin

    select  'Y'

    into    t_val

    from    user_login_data uld

            join user_token ut

              on uld.user_id = ut.user_id

    where   uld.user_id = p_user_id

            and ut.access_token = p_access_token

            and uld.is_active = 'Y';

  end;

  

  set t_val = ifnull(t_val,'N');

  

	RETURN t_val;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_attach_file` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_attach_file`(

  p_history_id    int,

  p_seqno         tinyint

) RETURNS varchar(200) CHARSET latin1
BEGIN

  

  declare t_val varchar(200);



  begin

    select  attach_file

    into    t_val

    from    cc_ticket_history_attach

    where   is_active = 'Y'

            and history_id = p_history_id

            and seqno = p_seqno;

  end;

  

	RETURN t_val;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_message_fc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_message_fc`(

  p_code	        varchar(50)

) RETURNS varchar(200) CHARSET latin1
BEGIN

  

  declare t_message varchar(200);



  begin

    select  message

    into    t_message

    from    messages

    where   language_code = 'in'

            and code = p_code;

  end;

  

	RETURN t_message;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cc_category_del_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `cc_category_del_pc`(

  out o_status         tinyint,

  out o_message        varchar(200),

  in  p_access_token   varchar(50),

  in  p_user_id        int unsigned,

  in  p_category_id      int unsigned

)
BEGIN

  

  declare t_success smallint;



  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end; 

  

  if access_token_fc(p_access_token, p_user_id) = 'Y' then

        

    update  cc_category

    set     is_active = 'N',

            last_update_date = now(),

            last_updated_by = p_user_id

    where   category_id = p_category_id

            and is_active = 'Y';

    

    select row_count() into t_success;

    

    if t_success <= 0 then

      set o_status = -1;

      select get_message_fc('NOT_FOUND') into o_message;

    else   

      set o_status = 1;

      select get_message_fc('DELETE_SUCCESS') into o_message;

    end if;  

    

  else

    

    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

    

  end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cc_category_save_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `cc_category_save_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  out o_category_id     int(10) unsigned,

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_category_id	    int(10) unsigned,

  in  p_category_name	  varchar(100)

)
BEGIN

  

  declare t_success smallint; 

  

  declare exit handler for sqlexception

  begin

    set o_category_id = p_category_id;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then



    if p_category_id > 0 then

    

      update  cc_category

      set     category_name = p_category_name,

              last_update_date = now(),

              last_updated_by = p_user_id

      where   category_id = p_category_id

              and is_active = 'Y';

                    

      select row_count() into t_success;

    

      if t_success <= 0 then

        set o_category_id = p_category_id;

        set o_status = -1;

        select get_message_fc('NOT_FOUND') into o_message;

      else               

        set o_category_id = p_category_id;

        set o_status = 1;

        select get_message_fc('SAVE_SUCCESS') into o_message;

      end if;                       

                      

    else

    

      insert into cc_category

      (last_update_date, last_updated_by, creation_date, created_by, category_name)

      values

      (now(), p_user_id, now(), p_user_id, p_category_name);

      

      set o_category_id = last_insert_id();

      

      set o_status = 1;

      select get_message_fc('SAVE_SUCCESS') into o_message;



    end if;

    

  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cc_ticket_approval_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `cc_ticket_approval_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_ticket_id	int(10) unsigned,

  in  p_mtc_approval	varchar(15)

)
BEGIN

  

  declare t_success smallint; 

  

  declare t_kavling_id int;

  declare t_mtc_cost double(15,2);

  

  declare exit handler for sqlexception

  begin

    ROLLBACK;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then



    -- cari data

    select  kavling_id, mtc_cost

    into    t_kavling_id, t_mtc_cost

    from    cc_ticket

    where   ticket_id = p_ticket_id;



    START TRANSACTION;



    update  cc_ticket

    set     status = p_mtc_approval,

            status_date = now(),

            status_by = p_user_id

    where   ticket_id = p_ticket_id;

    

    

    if p_mtc_approval = 'approved' then

      

      insert into transaksi_handyman

      (last_update_date, last_updated_by, creation_date, created_by,

       date_issued, ticket_id, kavling_id, user_id, nominal)

      values

      (now(), p_user_id, now(), p_user_id,

       now(), p_ticket_id, t_kavling_id, p_user_id, t_mtc_cost);

      

    end if;

    

    

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



    COMMIT;

  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cc_ticket_closed_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `cc_ticket_closed_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_ticket_id	int(10) unsigned

)
BEGIN

  

  declare t_success smallint; 

  

  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then



    update  cc_ticket

    set     status = 'closed',

            status_date = now(),

            status_by = p_user_id

    where   ticket_id = p_ticket_id;

    

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cc_ticket_history_attach_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `cc_ticket_history_attach_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_history_id	int(10) unsigned,

  in  p_ticket_id	int(10) unsigned,

  in  p_seqno	tinyint(4),

  in  p_attach_file	varchar(100)

)
BEGIN

  

  declare t_success smallint; 

  

  

  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then



    insert into cc_ticket_history_attach

    (last_update_date, last_updated_by, creation_date, created_by,

     history_id, ticket_id, seqno, attach_file)

    values

    (now(), p_user_id, now(), p_user_id,

     p_history_id, p_ticket_id, p_seqno, p_attach_file);

        

    

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cc_ticket_new_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `cc_ticket_new_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  out o_ticket_id        int(10) unsigned,

  out o_history_id        int(10) unsigned,

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_kavling_id	int(11) unsigned,

  in  p_kompleks_id	int(10) unsigned,

  in  p_subject	varchar(200),

  in  p_category_id	int(11),

  in  p_description	varchar(3000)

)
BEGIN

  

  declare t_success smallint; 

  

  declare t_code varchar(5);

  declare t_ticket_num varchar(30);

  

  declare exit handler for sqlexception

  begin

    ROLLBACK;

    set o_ticket_id = 0;

    set o_history_id = 0;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then



    START TRANSACTION;

    

    set t_code = 'MS';

      

    -- generate no order

    select  concat(t_code,date_format(now(),'%y%m'),lpad(ifnull(max(substr(ticket_num,-4)),0) + 1,4,'0'))

    into    t_ticket_num

    from    cc_ticket

    where   substr(ticket_num,1,6) = concat(t_code,date_format(now(),'%y%m'));

  

    if ifnull(t_ticket_num,'') = '' then

      set t_ticket_num = concat(t_code,date_format(now(),'%y%m'),'0001');

    end if;    



    insert into cc_ticket

    (last_update_date, last_updated_by, creation_date, created_by,

     ticket_num, ticket_date, request_by, kavling_id, kompleks_id, 

     subject, category_id, description, status, status_date, status_by)

    values

    (now(), p_user_id, now(), p_user_id,

     t_ticket_num, now(), p_user_id, p_kavling_id, p_kompleks_id, 

     p_subject, p_category_id, trim(both '\n' from p_description), 'submit', now(), p_user_id);

    

    set o_ticket_id = last_insert_id();

    

    

    

    insert into cc_ticket_history

    (last_update_date, last_updated_by, creation_date, created_by,

     ticket_id, sent_by, send_date, description)

    values

    (now(), p_user_id, now(), p_user_id,

     o_ticket_id, p_user_id, now(), trim(both '\n' from p_description));

     

    set o_history_id = last_insert_id();

    

    

    set o_status = 1;

    select get_message_fc('TICKET_NEW_SUCCESS') into o_message;



    COMMIT;

  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cc_ticket_reply_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `cc_ticket_reply_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  out o_history_id      int,

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_ticket_id	int(10) unsigned,

  in  p_description	varchar(3000),

  in  p_is_mtc	enum('Y','N'),

  in  p_mtc_cost	double(15,2),

  in  p_reply_type varchar(10)

)
BEGIN

  

  declare t_success smallint; 

  

  declare t_push_id int;

  declare t_status varchar(30);

  declare t_ticket_num varchar(20);

  declare t_fcm_token varchar(300);

  

  declare exit handler for sqlexception

  begin

    ROLLBACK;

    set o_history_id = 0;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then



    START TRANSACTION;

    

    if p_reply_type = 'user' then

      set t_status = 'user_response';

    else

      set t_status = 'admin_response';

    end if;

    

    insert into cc_ticket_history

    (last_update_date, last_updated_by, creation_date, created_by,

     ticket_id, sent_by, send_date, description, is_mtc, mtc_cost)

    values

    (now(), p_user_id, now(), p_user_id,

     p_ticket_id, p_user_id, now(), trim(both '\n' from p_description), p_is_mtc, p_mtc_cost);

     

    set o_history_id = last_insert_id();

    



    update  cc_ticket

    set     status = t_status,

            status_date = now(),

            status_by = p_user_id

    where   ticket_id = p_ticket_id;    

    

    

    if p_is_mtc = 'Y' then

    

      update  cc_ticket

      set     is_mtc = p_is_mtc,

              mtc_detail = trim(both '\n' from p_description),

              mtc_cost = p_mtc_cost,

              status = 'request_approval',

              status_date = now(),

              status_by = p_user_id

      where   ticket_id = p_ticket_id;

      

    end if;

    

    if p_reply_type <> 'user' then

      

      -- cari data + fcm_token

      select  ct.ticket_num, ut.fcm_token

      into    t_ticket_num, t_fcm_token

      from    cc_ticket ct

              join user_token ut

                on ct.request_by = ut.user_id

      where   ct.ticket_id = p_ticket_id;

      

      insert into push_notifications

      (push_type, fcm_token)

      values

      ('admin_reply', t_fcm_token);

    

      set t_push_id = last_insert_id();

      

      

      insert into push_notification_parameters

      (push_id, key_label, key_value)

      values

      (t_push_id, 'ticket_id', p_ticket_id),

      (t_push_id, 'title', concat('Reply #',t_ticket_num)),

      (t_push_id, 'message', trim(both '\n' from p_description));      

    end if;

    

    

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



    COMMIT;

  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `push_notification_sent_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `push_notification_sent_pc`(

  in p_push_id int unsigned 

)
BEGIN



  update  push_notifications

  set     is_sent = 'Y',

          send_date = now(),

          is_active = 'N'

  where   push_id = p_push_id;     



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_user_token_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_token_pc`(

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_token_type      varchar(20),

  in  p_token           varchar(300)

)
BEGIN   

  if access_token_fc(p_access_token, p_user_id) = 'Y' then

    

    if p_token_type = 'FCM' then

    

      update  user_token

      set     fcm_token = p_token

      where   user_id = p_user_id;

    

    end if;

    

  end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_login_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `user_login_pc`(

  out o_status              tinyint,

  out o_message             varchar(200),

  out o_access_token        varchar(50),

  out o_user_id             int unsigned,

  out o_user_name           varchar(30),

  out o_user_full_name      varchar(20),

  out o_user_id_type        tinyint,

  out o_kavling_id          int unsigned,

  out o_kompleks_id         int unsigned,  

  out o_kompleks_name       varchar(200),

  out o_kompleks_address    varchar(200),  

  out o_blok_name           varchar(200),

  out o_house_no            varchar(200),

  in  p_user_name           varchar(50),

  in  p_user_password       varchar(100)

)
BEGIN



  declare t_user_id int unsigned default 0;

  declare t_user_name varchar(30);

  declare t_user_full_name varchar(50);

  declare t_user_id_type tinyint;

  declare t_kavling_id int;

  declare t_kompleks_id int;

  declare t_kompleks_name varchar(200);

  declare t_kompleks_address varchar(200);

  declare t_blok_name varchar(200);

  declare t_house_no varchar(200);

  declare t_access_token varchar(50);



  

  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;

  

  begin

    select  uld.user_id, uld.user_name, uld.user_full_name, uld.user_id_type,

            uk.kavling_id, mkav.kompleks_id, mkom.kompleks_name, mkom.kompleks_address, 

            mb.blok_name, mkav.house_no, ut.access_token

    into    t_user_id, t_user_name, t_user_full_name, t_user_id_type, 

            t_kavling_id, t_kompleks_id, t_kompleks_name, t_kompleks_address, 

            t_blok_name, t_house_no, t_access_token

    from    user_login_data uld

            join user_token ut

              on uld.user_id = ut.user_id

            left join user_kavling uk

              on uld.user_id = uk.user_id

                 and uk.is_active = 'Y'

            left join master_kavling mkav

              on uk.kavling_id = mkav.kavling_id

            left join master_blok mb

              on mkav.blok_id = mb.blok_id

            left join master_kompleks mkom

              on mkav.kompleks_id = mkom.kompleks_id

    where   upper(trim(uld.user_name)) = upper(trim(p_user_name))

            and uld.user_password = p_user_password

            and uld.is_active = 'Y'

    limit 1;

  end;

  

  if ifnull(t_user_id, 0) <= 0 then

    

    set o_status = -1;

    select get_message_fc('LOGIN_FAILED') into o_message;

    set o_user_id = 0;

    set o_access_token = null;

  

  else    



    set o_status = 1;

    set o_message = null;

    set o_access_token = t_access_token;

    set o_user_id = t_user_id;

    set o_user_name = t_user_name;

    set o_user_full_name = t_user_full_name;

    set o_user_id_type = t_user_id_type;

    set o_kavling_id = t_kavling_id;

    set o_kompleks_id = t_kompleks_id;

    set o_kompleks_name = t_kompleks_name;

    set o_kompleks_address = t_kompleks_address;

    set o_blok_name = t_blok_name;

    set o_house_no = t_house_no;



  end if;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-27 14:03:07
