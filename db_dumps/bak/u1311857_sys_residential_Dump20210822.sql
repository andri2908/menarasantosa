CREATE DATABASE  IF NOT EXISTS `u1311857_sys_residential` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `u1311857_sys_residential`;
-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: u1311857_sys_residential
-- ------------------------------------------------------
-- Server version	5.7.35-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cart` (
  `cart_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `cart_type` enum('shop','drugs') DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `creation_date` datetime DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`cart_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cc_ticket`
--

DROP TABLE IF EXISTS `cc_ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cc_ticket` (
  `ticket_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_num` varchar(20) DEFAULT NULL,
  `ticket_date` datetime NOT NULL,
  `request_by` int(10) unsigned NOT NULL,
  `kavling_id` int(11) unsigned DEFAULT NULL,
  `kompleks_id` int(10) unsigned NOT NULL,
  `subject` varchar(200) DEFAULT NULL,
  `ticket_type` enum('cs','repair') DEFAULT NULL,
  `description` varchar(3000) DEFAULT NULL,
  `is_mtc` enum('Y','N') DEFAULT 'N',
  `mtc_detail` varchar(3000) DEFAULT NULL,
  `mtc_cost` double(15,2) DEFAULT NULL,
  `mtc_attach_file` varchar(20) DEFAULT NULL,
  `mtc_approval` varchar(15) DEFAULT NULL,
  `mtc_approval_date` datetime DEFAULT NULL,
  `status` enum('submit','closed','admin_response','user_response','request_approval','approved','rejected') DEFAULT NULL,
  `status_date` datetime DEFAULT NULL,
  `status_by` int(11) DEFAULT NULL,
  `is_confirmation` enum('Y','N') DEFAULT 'N',
  `is_closed` enum('Y','N') DEFAULT 'N',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ticket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cc_ticket`
--

LOCK TABLES `cc_ticket` WRITE;
/*!40000 ALTER TABLE `cc_ticket` DISABLE KEYS */;
INSERT INTO `cc_ticket` VALUES (1,'MS21070001','2021-07-23 12:08:11',2,2,1,'Rumah Bocor','repair','Kamar Depan bocor saat hujan lebat kemarin. Air merembes di dinding\n\nMohon untuk kirim tukang untuk pengecekan.\n\nTerima Kasih.','Y','Selamat Sore, \r\n\r\nDari hasil pengecekan dilapangan, ada biaya perbaikan sebesar Rp 320.000 dengan rincian sebagai berikut: \r\n- Aquaproof: 200.000\r\n- Serat Fiber: 20.000\r\n- Jasa: 100.000\r\n\r\nTerima kasih',320000.00,NULL,NULL,NULL,'closed','2021-08-18 00:00:00',1,'N','Y','N',2,'2021-07-23 12:08:11',1,'2021-08-18 20:58:40'),(2,'MS21070002','2021-07-23 13:53:43',2,2,1,'Sampah 3 hari tidak diambil','cs','Sampah Rumah saya sudah 3 hari tidak diambil, bau busuk. Mohon diperhatikan mengenai petugas kebersihan.','N','',0.00,NULL,NULL,NULL,'admin_response','2021-08-20 00:00:00',1,'N','N','Y',2,'2021-07-23 13:53:43',1,'2021-08-20 23:17:15'),(3,'MS21070003','2021-07-23 16:46:47',2,2,1,'Saluran PDAM Mati','cs','Saluran PDAM dari kemaren malam mati, sampai sekarang masih belum nyala. mohon dibantu cek.\n\nTq','N',NULL,NULL,NULL,NULL,NULL,'submit',NULL,2,'N','N','Y',2,'2021-07-23 16:46:47',2,'2021-07-23 16:46:47'),(4,'MS21070004','2021-07-23 16:49:39',2,2,1,'Jalan Berlubang','cs','Jalan di depan gang 3 berlubang, membahayakan warga','N','perbaikan',150000.00,NULL,NULL,NULL,'request_approval','2021-08-13 00:00:00',1,'N','N','Y',2,'2021-07-23 16:49:39',1,'2021-08-13 23:39:09');
/*!40000 ALTER TABLE `cc_ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cc_ticket_history`
--

DROP TABLE IF EXISTS `cc_ticket_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cc_ticket_history` (
  `history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_id` int(10) unsigned NOT NULL,
  `sent_by` int(11) DEFAULT NULL,
  `send_date` datetime DEFAULT NULL,
  `description` varchar(3000) DEFAULT NULL,
  `is_mtc` enum('Y','N') DEFAULT 'N',
  `mtc_cost` double(15,2) DEFAULT '0.00',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`history_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cc_ticket_history`
--

LOCK TABLES `cc_ticket_history` WRITE;
/*!40000 ALTER TABLE `cc_ticket_history` DISABLE KEYS */;
INSERT INTO `cc_ticket_history` VALUES (1,1,2,'2021-07-23 12:08:11','Kamar Depan bocor saat hujan lebat kemarin. Air merembes di dinding\n\nMohon untuk kirim tukang untuk pengecekan.\n\nTerima Kasih.','N',0.00,'Y',2,'2021-07-23 12:08:11',2,'2021-07-23 12:08:11'),(2,1,1,'2021-07-23 12:41:28','Selamat Pagi, \r\n\r\nTim kami akan datang ke rumah Anda pada Sabtu, 23 Juli 2021 pukul 15.00 - 16.00 \r\n\r\n\r\nTerima kasih,\r\nAndini','N',NULL,'Y',1,'2021-07-23 12:41:28',1,'2021-07-23 12:41:28'),(3,1,1,'2021-07-23 12:45:13','Selamat Sore, \r\n\r\nDari hasil pengecekan dilapangan, ada biaya perbaikan sebesar Rp 320.000 dengan rincian sebagai berikut: \r\n- Aquaproof: 200.000\r\n- Serat Fiber: 20.000\r\n- Jasa: 100.000\r\n\r\nTerima kasih','Y',320000.00,'Y',1,'2021-07-23 12:45:13',1,'2021-08-13 23:41:32'),(4,2,2,'2021-07-23 13:53:43','SAMPAH RUMAH SAYA SUDAH 3 HARI TIDAK DIAMBIL, BAU BUSUK. MOHON DIPERHATIKAN MENGENAI PETUGAS KEBERSIHAN.','N',200.00,'Y',2,'2021-07-23 13:53:43',1,'2021-08-13 22:29:50'),(5,3,2,'2021-07-23 16:46:47','Saluran PDAM dari kemaren malam mati, sampai sekarang masih belum nyala. mohon dibantu cek.\n\nTq','N',0.00,'Y',2,'2021-07-23 16:46:47',2,'2021-07-23 16:46:47'),(6,4,2,'2021-07-23 16:49:39','Jalan di depan gang 3 berlubang, membahayakan warga','N',0.00,'Y',2,'2021-07-23 16:49:39',2,'2021-07-23 16:49:39'),(7,2,1,'2021-08-13 00:00:00','DARI ADMIN OMONG OK','Y',3.00,'Y',1,'2021-08-13 22:30:04',1,'2021-08-13 23:36:55'),(8,4,1,'2021-08-13 00:00:00','perbaikan','Y',150000.00,'Y',1,'2021-08-13 23:19:28',1,'2021-08-13 23:39:09'),(10,2,1,'2021-08-19 00:00:00','aaaaaaaa','N',500.00,'Y',1,'2021-08-19 22:09:20',1,'2021-08-20 23:17:15');
/*!40000 ALTER TABLE `cc_ticket_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cc_ticket_history_attach`
--

DROP TABLE IF EXISTS `cc_ticket_history_attach`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cc_ticket_history_attach` (
  `attach_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `history_id` int(10) unsigned DEFAULT NULL,
  `ticket_id` int(10) unsigned DEFAULT NULL,
  `seqno` tinyint(4) DEFAULT NULL,
  `attach_file` varchar(100) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`attach_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cc_ticket_history_attach`
--

LOCK TABLES `cc_ticket_history_attach` WRITE;
/*!40000 ALTER TABLE `cc_ticket_history_attach` DISABLE KEYS */;
INSERT INTO `cc_ticket_history_attach` VALUES (1,1,1,1,'d6c79c12e2b2a6456d847e4f82c0a69c.jpg','Y',2,'2021-07-23 12:08:11',2,'2021-07-23 12:08:11'),(3,1,1,3,'5ff6805378fea5b2a43d79771cd71d84.jpg','Y',2,'2021-07-23 12:08:11',2,'2021-07-23 12:08:11'),(4,1,1,4,'ca64f3aa520e9e526bfd2464de26c853.jpg','Y',2,'2021-07-23 12:08:11',2,'2021-07-23 12:08:11'),(9,10,2,1,'D8E7A1D3D084B88E635B642753007ED5.jpg','N',1,'2021-08-19 23:01:57',NULL,NULL),(10,10,2,3,'594243A656FF9E181678F7D342248762.jpg','Y',1,'2021-08-19 23:01:57',1,'2021-08-20 23:17:15');
/*!40000 ALTER TABLE `cc_ticket_history_attach` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_category`
--

DROP TABLE IF EXISTS `item_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_category` (
  `item_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_category_name` varchar(45) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`item_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_category`
--

LOCK TABLES `item_category` WRITE;
/*!40000 ALTER TABLE `item_category` DISABLE KEYS */;
INSERT INTO `item_category` VALUES (1,'Retail','Y',0,NULL,0,NULL),(2,'Drugs','Y',0,NULL,0,NULL);
/*!40000 ALTER TABLE `item_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_bank`
--

DROP TABLE IF EXISTS `master_bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_bank` (
  `bank_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(100) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` smallint(6) DEFAULT '0',
  `creation_datetime` datetime DEFAULT NULL,
  `last_update_by` smallint(6) DEFAULT '0',
  `last_updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_bank`
--

LOCK TABLES `master_bank` WRITE;
/*!40000 ALTER TABLE `master_bank` DISABLE KEYS */;
INSERT INTO `master_bank` VALUES (1,'Bank Mandiri','Y',1,NULL,0,NULL),(2,'Bank Permata','Y',1,NULL,0,NULL),(3,'Bank BNI','Y',1,NULL,0,NULL),(4,'Bank BRI','Y',1,NULL,0,NULL);
/*!40000 ALTER TABLE `master_bank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_blok`
--

DROP TABLE IF EXISTS `master_blok`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_blok` (
  `blok_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `kompleks_id` bigint(10) DEFAULT '0',
  `blok_name` varchar(45) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`blok_id`),
  UNIQUE KEY `blok_name_UNIQUE` (`blok_name`),
  KEY `secondary` (`kompleks_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_blok`
--

LOCK TABLES `master_blok` WRITE;
/*!40000 ALTER TABLE `master_blok` DISABLE KEYS */;
INSERT INTO `master_blok` VALUES (1,1,'apel','Y',1,'2021-07-07 01:02:58',1,'2021-07-07 01:04:10'),(2,1,'jeruk','N',1,'2021-07-07 01:02:58',1,'2021-07-07 01:04:10'),(3,2,'KAV 1','Y',1,'2021-08-03 23:00:22',1,'2021-08-03 23:00:37'),(4,2,'KAV 2','N',1,'2021-08-03 23:00:22',1,'2021-08-03 23:00:37');
/*!40000 ALTER TABLE `master_blok` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_group`
--

DROP TABLE IF EXISTS `master_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_group` (
  `group_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL DEFAULT '',
  `group_description` varchar(50) NOT NULL DEFAULT '',
  `group_type` tinyint(3) DEFAULT '0',
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_group`
--

LOCK TABLES `master_group` WRITE;
/*!40000 ALTER TABLE `master_group` DISABLE KEYS */;
INSERT INTO `master_group` VALUES (1,'ADMIN','GROUP ADMIN AAAA',2,'Y',NULL,NULL,1,'2021-08-01 23:45:44'),(2,'LAND LORD','',1,'Y',NULL,NULL,NULL,NULL),(3,'RESIDENT','',0,'Y',1,'2021-07-20 23:54:54',NULL,NULL),(7,'AAA','AAAA',0,'Y',1,'2021-08-01 23:48:49',NULL,NULL),(11,'ASDF','ASD',0,'Y',1,'2021-08-01 23:53:16',NULL,NULL),(12,'GROUP ADMIN','123',2,'N',1,'2021-08-20 22:22:59',1,'2021-08-20 22:23:05');
/*!40000 ALTER TABLE `master_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_item`
--

DROP TABLE IF EXISTS `master_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_item` (
  `item_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_name` varchar(100) DEFAULT '',
  `unit_id` smallint(6) DEFAULT '0',
  `item_hpp` double DEFAULT '0',
  `item_price` double DEFAULT '0',
  `qty` double DEFAULT '0',
  `item_jasa` enum('Y','N') DEFAULT 'N',
  `item_category_id` int(11) DEFAULT '0',
  `image_file` varchar(100) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_item`
--

LOCK TABLES `master_item` WRITE;
/*!40000 ALTER TABLE `master_item` DISABLE KEYS */;
INSERT INTO `master_item` VALUES (1,'TABUNG GAS 15KG',1,0,20000,50,'N',1,'546418BDADB97313D9675A90A627BC31.jpg','Y',1,'2021-07-09 23:45:37',1,'2021-08-18 00:14:33'),(2,'OBAT PANADOL',2,1328,5000,130,'N',2,'CTY9UXZ93SYJR7B.jpg','Y',1,'2021-07-20 10:25:14',1,'2021-08-20 23:14:38'),(3,'JASA CAT TEMBOK',4,10000,20000,0,'Y',1,'','Y',1,'2021-07-28 12:08:07',1,'2021-08-04 23:54:24'),(4,'JASA BERSIH BERSIH',5,0,50000,64,'N',1,'','Y',1,'2021-08-05 00:17:49',1,'2021-08-05 00:17:56');
/*!40000 ALTER TABLE `master_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_kavling`
--

DROP TABLE IF EXISTS `master_kavling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_kavling` (
  `kavling_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `kompleks_id` bigint(10) DEFAULT '0',
  `blok_id` bigint(10) DEFAULT '0',
  `house_no` varchar(45) DEFAULT '',
  `biaya_ipl` double DEFAULT '0',
  `durasi_pembayaran` int(10) DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`kavling_id`),
  KEY `secondary` (`kompleks_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_kavling`
--

LOCK TABLES `master_kavling` WRITE;
/*!40000 ALTER TABLE `master_kavling` DISABLE KEYS */;
INSERT INTO `master_kavling` VALUES (1,1,1,'F0100',250000,2,'Y',1,'2021-07-07 23:26:38',1,'2021-08-03 23:40:43'),(2,1,1,'20',20000,3,'Y',1,'2021-07-07 23:36:52',0,NULL),(3,2,3,'55',1500,10,'Y',1,'2021-08-03 23:48:49',0,NULL);
/*!40000 ALTER TABLE `master_kavling` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_kompleks`
--

DROP TABLE IF EXISTS `master_kompleks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_kompleks` (
  `kompleks_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `kompleks_name` varchar(50) DEFAULT '',
  `kompleks_address` varchar(200) DEFAULT '',
  `biaya_ipl` double DEFAULT '0',
  `durasi_pembayaran` int(10) DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`kompleks_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_kompleks`
--

LOCK TABLES `master_kompleks` WRITE;
/*!40000 ALTER TABLE `master_kompleks` DISABLE KEYS */;
INSERT INTO `master_kompleks` VALUES (1,'SYAILENDRA','PURWOSARI',20000,3,'Y',1,'2021-07-07 01:02:58',1,'2021-07-07 01:04:10'),(2,'PERMATA','AAAAAAAAAASSSSSS',5000,20,'Y',1,'2021-08-03 23:00:22',1,'2021-08-03 23:00:37');
/*!40000 ALTER TABLE `master_kompleks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_module`
--

DROP TABLE IF EXISTS `master_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_module` (
  `module_id` smallint(6) NOT NULL,
  `module_name` varchar(100) DEFAULT '',
  `module_features` tinyint(3) unsigned DEFAULT '0',
  `module_active` tinyint(3) unsigned DEFAULT '1',
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_module`
--

LOCK TABLES `master_module` WRITE;
/*!40000 ALTER TABLE `master_module` DISABLE KEYS */;
INSERT INTO `master_module` VALUES (1,'MANAJEMEN SISTEM',1,1),(2,'TAMBAH / HAPUS GROUP USER',2,1),(3,'TAMBAH / HAPUS USER',2,1),(4,'PENGATURAN AKSES MODUL',1,1),(5,'BACKUP / RESTORE DATABASE',1,0),(6,'PENGATURAN SISTEM APLIKASI',1,1),(21,'TAMBAH / HAPUS KOMPLEKS',2,1),(22,'TAMBAH / HAPUS KAVLING',2,1),(23,'TAMBAH / HAPUS ITEM RETAIL',2,1),(24,'TAMBAH / HAPUS SATUAN',2,1),(25,'TAMBAH / HAPUS PEMILIK KAVLING',1,1),(26,'TAMBAH / HAPUS ITEM DRUGS',2,1),(41,'RINGKASAN IPL',1,1),(42,'TAMBAH / HAPUS TRANSAKSI RETAIL',2,1),(43,'PENYESUAIAN STOK RETAIL',1,1),(44,'PENERIMAAN BARANG RETAIL',2,1),(45,'TAMBAH / HAPUS TRANSAKSI DRUGS',2,1),(46,'PENYESUAIAN STOK DRUGS',1,1),(47,'PENERIMAAN BARANG DRUGS',2,1);
/*!40000 ALTER TABLE `master_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_module_access`
--

DROP TABLE IF EXISTS `master_module_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_module_access` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` tinyint(3) unsigned NOT NULL,
  `module_id` smallint(6) NOT NULL,
  `user_access_option` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_module_access`
--

LOCK TABLES `master_module_access` WRITE;
/*!40000 ALTER TABLE `master_module_access` DISABLE KEYS */;
INSERT INTO `master_module_access` VALUES (1,1,1,1),(2,1,2,7),(3,1,3,7),(4,1,4,1),(5,1,5,1),(6,1,6,1),(7,1,21,7),(8,1,22,7),(9,1,23,7),(10,1,24,7),(11,1,25,1),(12,1,41,1),(13,1,42,7),(14,1,43,1),(15,1,44,7),(16,7,1,1),(17,7,2,7),(18,7,3,7),(19,7,4,1),(20,7,6,1),(21,7,21,7),(22,7,22,7),(23,7,23,7),(24,7,24,7),(25,7,25,1),(26,7,41,1),(27,7,42,7),(28,7,43,1),(29,7,44,7),(30,1,26,7),(31,1,45,7),(32,1,46,1),(33,1,47,7);
/*!40000 ALTER TABLE `master_module_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_payment_mode`
--

DROP TABLE IF EXISTS `master_payment_mode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_payment_mode` (
  `payment_type` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `seqno` tinyint(4) DEFAULT NULL,
  `payment_type_name` varchar(100) DEFAULT '',
  `payment_code` varchar(30) DEFAULT NULL,
  `bank` varchar(20) DEFAULT NULL,
  `cstore` varchar(20) DEFAULT NULL,
  `logo` varchar(30) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  PRIMARY KEY (`payment_type`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_payment_mode`
--

LOCK TABLES `master_payment_mode` WRITE;
/*!40000 ALTER TABLE `master_payment_mode` DISABLE KEYS */;
INSERT INTO `master_payment_mode` VALUES (1,1,'BRI Virtual Account','bank_transfer','bri',NULL,'bri','Y'),(2,4,'Gopay','gopay',NULL,NULL,'gopay','Y'),(3,5,'ShopeePay','shopeepay',NULL,NULL,'shopeepay','Y'),(4,6,'Indomaret','cstore',NULL,'indomaret','indomaret','Y'),(5,7,'Alfa Group','cstore',NULL,'alfamart','alfamart','Y'),(6,2,'BNI Virtual Account','bank_transfer','bni',NULL,'bni','Y'),(7,3,'Mandiri Virtual Account','echannel','',NULL,'mandiri','Y');
/*!40000 ALTER TABLE `master_payment_mode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_unit`
--

DROP TABLE IF EXISTS `master_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_unit` (
  `unit_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(100) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_unit`
--

LOCK TABLES `master_unit` WRITE;
/*!40000 ALTER TABLE `master_unit` DISABLE KEYS */;
INSERT INTO `master_unit` VALUES (1,'PCS','Y',1,'2021-07-09 23:32:08',0,NULL),(2,'KG','Y',1,'2021-07-09 23:32:15',1,'2021-07-09 23:40:01'),(3,'BOX','Y',1,'2021-07-09 23:40:15',0,NULL),(4,'JAM','Y',1,'2021-07-28 12:05:20',0,NULL),(5,'HARI AAAA','Y',1,'2021-08-05 00:17:17',1,'2021-08-05 00:17:23');
/*!40000 ALTER TABLE `master_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `messages` (
  `language_code` varchar(10) NOT NULL,
  `code` varchar(50) NOT NULL,
  `message` varchar(200) NOT NULL,
  PRIMARY KEY (`language_code`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES ('in','ACCESS_VIOLATION','Anda tidak memiliki hak akses'),('in','CHANGE_PASSWORD_FAILED','Password Lama salah, mohon periksa kembali'),('in','CHANGE_PASSWORD_SUCCESS','Password berhasil diganti'),('in','DELETE_SUCCESS','Data berhasil dihapus'),('in','LOGIN_FAILED','Username / Password salah, mohon ulangi kembali'),('in','NOT_FOUND','Data tidak ditemukan'),('in','SAVE_SUCCESS','Data berhasil disimpan'),('in','TROUBLE','Terjadi gangguan pada sistem, hubungi tim support kami'),('in','USER_DUPLICATE','Username sudah digunakan');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `news` (
  `news_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `news_type` enum('buletin','lifestyle') DEFAULT NULL,
  `subject` varchar(200) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `file_name` varchar(200) DEFAULT NULL,
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `creation_date` datetime DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `penerimaan_detail`
--

DROP TABLE IF EXISTS `penerimaan_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `penerimaan_detail` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `penerimaan_id` bigint(10) DEFAULT '0',
  `item_id` bigint(10) DEFAULT '0',
  `item_qty` double DEFAULT '0',
  `item_hpp` double DEFAULT '0',
  `item_subtotal` double DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `penerimaan_detail`
--

LOCK TABLES `penerimaan_detail` WRITE;
/*!40000 ALTER TABLE `penerimaan_detail` DISABLE KEYS */;
INSERT INTO `penerimaan_detail` VALUES (1,1,2,5,5000,25000,'Y',1,'2021-07-27 23:15:57',1,'2021-07-27 23:23:27'),(2,1,1,10,20000,200000,'Y',1,'2021-07-27 23:15:57',1,'2021-07-27 23:23:27'),(3,2,2,10,2000,20000,'Y',1,'2021-07-28 13:25:24',1,'2021-08-11 21:49:32'),(4,3,2,5,1327.84,6639.2,'Y',1,'2021-08-10 22:55:42',0,NULL),(5,4,2,100,1328,132800,'Y',1,'2021-08-20 23:00:17',1,'2021-08-20 23:00:24');
/*!40000 ALTER TABLE `penerimaan_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `penerimaan_header`
--

DROP TABLE IF EXISTS `penerimaan_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `penerimaan_header` (
  `penerimaan_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_trans` int(10) DEFAULT '1',
  `penerimaan_datetime` datetime DEFAULT NULL,
  `penerimaan_total` double DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`penerimaan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `penerimaan_header`
--

LOCK TABLES `penerimaan_header` WRITE;
/*!40000 ALTER TABLE `penerimaan_header` DISABLE KEYS */;
INSERT INTO `penerimaan_header` VALUES (1,2,'2021-07-04 00:00:00',225000,'N',1,'2021-07-27 23:15:57',1,'2021-08-10 22:56:06'),(2,2,'2021-07-28 00:00:00',20000,'Y',1,'2021-07-28 13:25:21',1,'2021-08-11 21:49:32'),(3,2,'2021-08-10 00:00:00',6639.2,'Y',1,'2021-08-10 22:54:38',1,'2021-08-10 22:55:42'),(4,4,'2021-08-20 00:00:00',132800,'Y',1,'2021-08-20 23:00:17',1,'2021-08-20 23:00:24');
/*!40000 ALTER TABLE `penerimaan_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `penyesuaian_stok`
--

DROP TABLE IF EXISTS `penyesuaian_stok`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `penyesuaian_stok` (
  `ID` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` bigint(10) DEFAULT '0',
  `qty_awal` double DEFAULT '0',
  `qty_baru` double DEFAULT '0',
  `keterangan` varchar(200) DEFAULT '',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `penyesuaian_stok`
--

LOCK TABLES `penyesuaian_stok` WRITE;
/*!40000 ALTER TABLE `penyesuaian_stok` DISABLE KEYS */;
INSERT INTO `penyesuaian_stok` VALUES (1,2,15,20,'AAA',1,'2021-07-27 23:13:53'),(2,1,28,30,'AAAA',1,'2021-08-08 14:41:09'),(3,1,10,50,'ASAD',1,'2021-08-20 22:43:19');
/*!40000 ALTER TABLE `penyesuaian_stok` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `push_notification_parameters`
--

DROP TABLE IF EXISTS `push_notification_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `push_notification_parameters` (
  `push_id` int(10) unsigned NOT NULL,
  `key_label` varchar(100) NOT NULL,
  `key_value` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`push_id`,`key_label`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `push_notification_parameters`
--

LOCK TABLES `push_notification_parameters` WRITE;
/*!40000 ALTER TABLE `push_notification_parameters` DISABLE KEYS */;
INSERT INTO `push_notification_parameters` VALUES (1,'message','Betul Pak, berikut rincian anya'),(1,'ticket_id','3'),(1,'title','Reply #MS21070001'),(2,'message','Baik pak, tim kami akan mengecek besok pukul 10.00'),(2,'ticket_id','4'),(2,'title','Reply #MS21070002'),(3,'message','Setelah ada pengecekan, ada biaya jasa untuk pembersihan selokan'),(3,'ticket_id','4'),(3,'title','Reply #MS21070002'),(4,'message','Terima kasih, kami akan mengirim teknisi kesana'),(4,'ticket_id','6'),(4,'title','Reply #MS21070004'),(5,'message','Biaya Perbaikan sebesar Rp 250.000'),(5,'ticket_id','6'),(5,'title','Reply #MS21070004'),(6,'message','Ada perbaikan'),(6,'ticket_id','5'),(6,'title','Reply #MS21070003'),(7,'message','Selamat Pagi, Tim kami akan datang ke rumah Anda pada Sabtu, 23 Juli 2021 pukul 15.00 - 16.00 Terima kasih'),(7,'ticket_id','1'),(7,'title','Reply #MS21070001'),(8,'message','Selamat Sore, Dari hasil pengecekan dilapangan, ada biaya perbaikan sebesar Rp 320.000 dengan rincian sebagai berikut: Aquaproof: Rp 200.000 Serat Fiber: Rp. 20.000 Jasa: 100.000'),(8,'ticket_id','1'),(8,'title','Reply #MS21070001');
/*!40000 ALTER TABLE `push_notification_parameters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `push_notifications`
--

DROP TABLE IF EXISTS `push_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `push_notifications` (
  `push_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `push_type` varchar(30) DEFAULT NULL,
  `fcm_token` varchar(300) DEFAULT NULL,
  `is_sent` enum('Y','N') DEFAULT 'N',
  `send_date` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  PRIMARY KEY (`push_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `push_notifications`
--

LOCK TABLES `push_notifications` WRITE;
/*!40000 ALTER TABLE `push_notifications` DISABLE KEYS */;
INSERT INTO `push_notifications` VALUES (1,'admin_reply',NULL,'N',NULL,'Y'),(2,'admin_reply',NULL,'N',NULL,'Y'),(3,'admin_reply',NULL,'N',NULL,'Y'),(4,'admin_reply',NULL,'N',NULL,'Y'),(5,'admin_reply',NULL,'N',NULL,'Y'),(6,'admin_reply',NULL,'N',NULL,'Y'),(7,'admin_reply',NULL,'N',NULL,'Y'),(8,'admin_reply',NULL,'N',NULL,'Y');
/*!40000 ALTER TABLE `push_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_config`
--

DROP TABLE IF EXISTS `sys_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_config` (
  `ID` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(50) DEFAULT '',
  `company_address` varchar(100) DEFAULT '',
  `company_phone` varchar(20) DEFAULT '',
  `company_email` varchar(50) DEFAULT '',
  `default_printer` varchar(200) DEFAULT '',
  `auto_backup_flag` tinyint(3) unsigned DEFAULT '0',
  `auto_backup_dir` varchar(200) DEFAULT '',
  `print_preview` tinyint(3) unsigned DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_config`
--

LOCK TABLES `sys_config` WRITE;
/*!40000 ALTER TABLE `sys_config` DISABLE KEYS */;
INSERT INTO `sys_config` VALUES (1,'PT. MENARA SANTOSA','JL. RONGGOWARSITO NO.73, KEPRABON','(0271) 643100','menara@menarasantosa.com','PrimoPDF',0,'',0,'Y',0,NULL,1,'2021-08-20 23:02:12');
/*!40000 ALTER TABLE `sys_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_config_transaksi`
--

DROP TABLE IF EXISTS `sys_config_transaksi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_config_transaksi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_type` int(11) DEFAULT '0',
  `biaya_transaksi` double DEFAULT '0',
  `biaya_transaksi_percent` double DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_config_transaksi`
--

LOCK TABLES `sys_config_transaksi` WRITE;
/*!40000 ALTER TABLE `sys_config_transaksi` DISABLE KEYS */;
INSERT INTO `sys_config_transaksi` VALUES (1,1,4000,0,'Y',1,'2021-07-20 23:19:58',1,'2021-08-20 23:02:12'),(2,2,0,2,'Y',1,'2021-07-20 23:28:35',1,'2021-08-20 23:02:12'),(3,3,0,1.5,'Y',1,'2021-07-20 23:28:35',1,'2021-08-20 23:02:12'),(4,4,0,0,'Y',1,'2021-07-20 23:28:35',1,'2021-08-20 23:02:12'),(5,5,5000,0,'Y',1,'2021-07-20 23:28:35',1,'2021-08-20 23:02:12'),(6,6,4000,0,'Y',0,NULL,1,'2021-08-20 23:02:12'),(7,7,4000,0,'Y',0,NULL,1,'2021-08-20 23:02:12');
/*!40000 ALTER TABLE `sys_config_transaksi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblusertocheckedrecords`
--

DROP TABLE IF EXISTS `tblusertocheckedrecords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblusertocheckedrecords` (
  `recID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL DEFAULT '0',
  `query` text CHARACTER SET utf8 NOT NULL,
  `uniqueKey` varchar(30) NOT NULL DEFAULT '',
  `createdDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` int(11) NOT NULL DEFAULT '0',
  `payment_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`recID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblusertocheckedrecords`
--

LOCK TABLES `tblusertocheckedrecords` WRITE;
/*!40000 ALTER TABLE `tblusertocheckedrecords` DISABLE KEYS */;
INSERT INTO `tblusertocheckedrecords` VALUES (1,1,'','p6g9iq2jbeil18oin3dmd41emn1270','2021-07-21 10:55:55',1,1);
/*!40000 ALTER TABLE `tblusertocheckedrecords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblusertocheckedrecordsdetail`
--

DROP TABLE IF EXISTS `tblusertocheckedrecordsdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblusertocheckedrecordsdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recID` int(11) NOT NULL DEFAULT '0',
  `recordID` int(11) NOT NULL DEFAULT '0',
  `trans_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblusertocheckedrecordsdetail`
--

LOCK TABLES `tblusertocheckedrecordsdetail` WRITE;
/*!40000 ALTER TABLE `tblusertocheckedrecordsdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblusertocheckedrecordsdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_ipl`
--

DROP TABLE IF EXISTS `transaksi_ipl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksi_ipl` (
  `id_trans` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_trans` int(10) DEFAULT '0',
  `date_issued` datetime DEFAULT NULL,
  `kavling_id` bigint(10) DEFAULT '0',
  `user_id` bigint(10) DEFAULT '0',
  `payment_id` varchar(45) DEFAULT NULL,
  `nominal` double DEFAULT '0',
  `status_id` int(10) DEFAULT '0',
  `date_paid` datetime DEFAULT NULL,
  `start_ipl` datetime DEFAULT NULL,
  `end_ipl` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_trans`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_ipl`
--

LOCK TABLES `transaksi_ipl` WRITE;
/*!40000 ALTER TABLE `transaksi_ipl` DISABLE KEYS */;
INSERT INTO `transaksi_ipl` VALUES (1,1,'2021-08-08 00:00:00',2,5,NULL,60000,0,NULL,'2021-08-01 00:00:00','2021-10-01 00:00:00','N',1,'2021-08-08 11:34:15',1,'2021-08-20 22:23:48'),(2,1,'2021-08-20 00:00:00',2,5,NULL,60000,0,NULL,'2021-08-01 00:00:00','2021-10-01 00:00:00','Y',1,'2021-08-20 22:23:53',0,NULL);
/*!40000 ALTER TABLE `transaksi_ipl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_ipl_detail`
--

DROP TABLE IF EXISTS `transaksi_ipl_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksi_ipl_detail` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_trans` bigint(10) DEFAULT '0',
  `nominal` double DEFAULT '0',
  `periode_pembayaran` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_ipl_detail`
--

LOCK TABLES `transaksi_ipl_detail` WRITE;
/*!40000 ALTER TABLE `transaksi_ipl_detail` DISABLE KEYS */;
INSERT INTO `transaksi_ipl_detail` VALUES (1,1,20000,'2021-08-01 00:00:00','Y',1,'2021-08-08 11:34:15',0,NULL),(2,1,20000,'2021-09-01 00:00:00','Y',1,'2021-08-08 11:34:15',0,NULL),(3,1,20000,'2021-10-01 00:00:00','Y',1,'2021-08-08 11:34:15',0,NULL),(4,2,20000,'2021-08-01 00:00:00','Y',1,'2021-08-20 22:23:53',0,NULL),(5,2,20000,'2021-09-01 00:00:00','Y',1,'2021-08-20 22:23:53',0,NULL),(6,2,20000,'2021-10-01 00:00:00','Y',1,'2021-08-20 22:23:53',0,NULL);
/*!40000 ALTER TABLE `transaksi_ipl_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_payment`
--

DROP TABLE IF EXISTS `transaksi_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksi_payment` (
  `id_payment` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `unique_code` varchar(50) DEFAULT NULL,
  `date_issued` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `payment_id` varchar(45) DEFAULT '',
  `total` double DEFAULT '0',
  `payment_type` int(11) DEFAULT '0',
  `biaya_transaksi` double DEFAULT '0',
  `status_id` int(10) DEFAULT '0',
  `date_paid` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  `status_code` int(11) DEFAULT '0',
  `transaction_id` varchar(500) DEFAULT NULL,
  `transaction_status` varchar(50) DEFAULT NULL,
  `transaction_time` datetime DEFAULT '1900-01-01 00:00:00',
  `midtrans_payment_type` varchar(50) DEFAULT NULL,
  `bank_name` varchar(50) DEFAULT NULL,
  `va_number` varchar(50) DEFAULT NULL,
  `bill_key` varchar(50) DEFAULT NULL,
  `biller_code` varchar(50) DEFAULT NULL,
  `payment_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_payment`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_payment`
--

LOCK TABLES `transaksi_payment` WRITE;
/*!40000 ALTER TABLE `transaksi_payment` DISABLE KEYS */;
INSERT INTO `transaksi_payment` VALUES (1,NULL,'2021-07-21 18:41:36',NULL,'1446156635',65000,1,4000,1,'2021-07-21 18:41:36','Y',1,'2021-07-21 18:41:36',0,NULL,201,'b67e7e72-d97b-4d39-ba90-e7e691630a25','pending','2021-07-21 18:41:34','bank_transfer','Bni','9883400086116934','','','');
/*!40000 ALTER TABLE `transaksi_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_payment_detail`
--

DROP TABLE IF EXISTS `transaksi_payment_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksi_payment_detail` (
  `id_payment_detail` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_payment` bigint(10) DEFAULT '0',
  `id_trans` bigint(10) DEFAULT '0',
  `type_trans` int(10) DEFAULT '0',
  `total` double DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_payment_detail`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_payment_detail`
--

LOCK TABLES `transaksi_payment_detail` WRITE;
/*!40000 ALTER TABLE `transaksi_payment_detail` DISABLE KEYS */;
INSERT INTO `transaksi_payment_detail` VALUES (1,1,1,2,65000,'Y',1,'2021-07-21 18:41:36',0,NULL);
/*!40000 ALTER TABLE `transaksi_payment_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_retail`
--

DROP TABLE IF EXISTS `transaksi_retail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksi_retail` (
  `id_trans` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_trans` int(10) DEFAULT '0',
  `date_issued` datetime DEFAULT NULL,
  `date_expired` datetime DEFAULT NULL,
  `kavling_id` bigint(10) DEFAULT '0',
  `user_id` bigint(10) DEFAULT '0',
  `payment_id` varchar(45) DEFAULT NULL,
  `nominal` double DEFAULT '0',
  `status_id` int(10) DEFAULT '0',
  `date_paid` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_trans`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_retail`
--

LOCK TABLES `transaksi_retail` WRITE;
/*!40000 ALTER TABLE `transaksi_retail` DISABLE KEYS */;
INSERT INTO `transaksi_retail` VALUES (1,2,'2021-07-20 10:34:28',NULL,2,1,'1446156635',65000,1,'2021-07-21 18:41:34','Y',1,'2021-07-19 23:55:41',1,'2021-07-20 10:34:41'),(2,2,'2021-07-29 00:00:00',NULL,2,2,NULL,80000,0,NULL,'Y',1,'2021-07-27 23:11:21',1,'2021-08-09 23:44:05'),(3,2,'2021-08-08 00:00:00',NULL,2,1,NULL,140000,0,NULL,'N',1,'2021-08-08 13:59:37',1,'2021-08-08 14:00:29'),(4,2,'2021-08-09 00:00:00',NULL,2,5,NULL,200000,0,NULL,'Y',1,'2021-08-09 00:24:01',1,'2021-08-18 13:00:33'),(5,2,'2021-08-18 00:00:00','2021-08-18 00:44:05',2,1,NULL,500000,0,NULL,'N',1,'2021-08-18 00:44:05',0,'2021-08-18 22:31:31'),(6,2,'2021-08-18 00:00:00','2021-08-19 00:55:36',2,5,NULL,200000,0,NULL,'Y',1,'2021-08-18 12:55:36',0,NULL),(7,2,'2021-08-18 00:00:00','2021-08-19 01:08:03',2,5,NULL,200000,0,NULL,'Y',1,'2021-08-18 13:08:03',0,NULL),(8,4,'2021-08-20 00:00:00','2021-08-21 11:16:33',2,1,NULL,25000,0,NULL,'Y',1,'2021-08-20 23:16:33',0,NULL);
/*!40000 ALTER TABLE `transaksi_retail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_retail_detail`
--

DROP TABLE IF EXISTS `transaksi_retail_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksi_retail_detail` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_trans` bigint(10) DEFAULT '0',
  `item_id` bigint(10) DEFAULT '0',
  `item_qty` double DEFAULT '0',
  `item_hpp` double DEFAULT '0',
  `item_price` double DEFAULT '0',
  `subtotal` double DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_retail_detail`
--

LOCK TABLES `transaksi_retail_detail` WRITE;
/*!40000 ALTER TABLE `transaksi_retail_detail` DISABLE KEYS */;
INSERT INTO `transaksi_retail_detail` VALUES (1,1,1,2,0,20000,40000,'Y',1,'2021-07-19 23:55:41',1,'2021-07-20 10:34:41'),(2,1,2,5,0,5000,25000,'Y',1,'2021-07-20 10:25:26',1,'2021-07-20 10:34:41'),(3,2,2,10,0,8000,80000,'Y',1,'2021-07-27 23:11:21',1,'2021-08-09 23:44:05'),(4,2,1,8,0,20000,160000,'N',1,'2021-07-27 23:11:21',1,'2021-08-08 14:01:41'),(5,3,1,7,0,20000,140000,'Y',1,'2021-08-08 13:59:37',1,'2021-08-08 14:00:02'),(6,4,3,10,10000,20000,200000,'Y',1,'2021-08-09 00:24:01',1,'2021-08-18 13:00:33'),(7,5,4,10,0,50000,500000,'Y',1,'2021-08-18 00:44:05',0,NULL),(8,6,1,10,0,20000,200000,'Y',1,'2021-08-18 12:55:36',0,NULL),(9,7,3,10,10000,20000,200000,'Y',1,'2021-08-18 13:08:03',0,NULL),(10,8,2,5,1328,5000,25000,'Y',1,'2021-08-20 23:16:33',0,NULL);
/*!40000 ALTER TABLE `transaksi_retail_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_type`
--

DROP TABLE IF EXISTS `transaksi_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksi_type` (
  `type_trans` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`type_trans`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_type`
--

LOCK TABLES `transaksi_type` WRITE;
/*!40000 ALTER TABLE `transaksi_type` DISABLE KEYS */;
INSERT INTO `transaksi_type` VALUES (1,'TRANSAKSI IPL'),(2,'TRANSAKSI RETAIL'),(3,'TRANSAKSI SERVICE'),(4,'TRANSAKSI APOTIK');
/*!40000 ALTER TABLE `transaksi_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_kavling`
--

DROP TABLE IF EXISTS `user_kavling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_kavling` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) DEFAULT NULL,
  `kavling_id` bigint(10) DEFAULT NULL,
  `start_ipl` datetime DEFAULT NULL,
  `remarks` varchar(200) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_kavling`
--

LOCK TABLES `user_kavling` WRITE;
/*!40000 ALTER TABLE `user_kavling` DISABLE KEYS */;
INSERT INTO `user_kavling` VALUES (1,1,1,'2021-08-01 15:45:55','AAAAA','N',1,'2021-07-09 15:46:43',1,'2021-07-01 16:14:16'),(2,1,2,'2021-08-01 00:00:00','SSSSSSSSDDDD','Y',1,'2021-07-09 16:16:48',1,'2021-08-20 23:41:57'),(3,2,2,'2021-07-01 00:00:00','DDDDDDD','Y',1,'2021-07-20 23:57:32',1,'2021-08-20 23:41:57'),(4,5,2,'2021-08-01 00:00:00','DFFFFF','N',1,'2021-08-04 21:02:44',1,'2021-08-04 21:10:53'),(5,5,2,'2021-08-01 00:00:00','','N',1,'2021-08-04 22:14:25',1,'2021-08-04 22:16:37'),(6,5,2,'2021-08-01 00:00:00','','N',1,'2021-08-04 22:16:43',1,'2021-08-20 23:41:56'),(7,6,1,'2021-08-01 00:00:00','','Y',1,'2021-08-20 23:51:22',0,NULL);
/*!40000 ALTER TABLE `user_kavling` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_login_data`
--

DROP TABLE IF EXISTS `user_login_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_login_data` (
  `user_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(15) DEFAULT '',
  `user_password` varchar(15) DEFAULT '',
  `group_id` tinyint(3) unsigned DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `user_full_name` varchar(100) DEFAULT '',
  `user_id_type` tinyint(3) DEFAULT '0',
  `user_id_no` varchar(100) DEFAULT '',
  `user_phone_1` varchar(45) DEFAULT '',
  `user_phone_2` varchar(45) DEFAULT '',
  `user_email_address` varchar(200) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `SECONDARY` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_login_data`
--

LOCK TABLES `user_login_data` WRITE;
/*!40000 ALTER TABLE `user_login_data` DISABLE KEYS */;
INSERT INTO `user_login_data` VALUES (1,'ADMIN','admin',1,'2021-08-20 23:48:08','2021-08-20 23:51:25','ADMIN1234567',1,'123456','081558775652','0111','ARISTON@ALPHASOFT.COM','Y',NULL,NULL,1,'2021-08-02 23:34:31'),(2,'RESIDENT','123',3,NULL,NULL,'Resident',1,'','0123','','','Y',1,'2021-07-20 23:55:19',NULL,NULL),(4,'LANDLORD','123',2,NULL,NULL,'AAAA',1,'','000','','','Y',1,'2021-08-04 20:52:17',NULL,NULL),(5,'ANDRI','',3,NULL,NULL,'AAA',1,'','000','','','N',1,'2021-08-04 20:56:58',1,'2021-08-20 23:48:44'),(6,'ANDRI','andri123',3,NULL,NULL,'ANDRI ANDRI',1,'','0123','','','Y',1,'2021-08-20 23:49:12',NULL,NULL);
/*!40000 ALTER TABLE `user_login_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_token`
--

DROP TABLE IF EXISTS `user_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_token` (
  `user_id` bigint(10) unsigned NOT NULL DEFAULT '0',
  `access_token` varchar(300) DEFAULT NULL,
  `fcm_token` varchar(300) DEFAULT NULL,
  `gcm_token` varchar(300) DEFAULT NULL,
  `ios_token` varchar(300) DEFAULT NULL,
  `web_token` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_token`
--

LOCK TABLES `user_token` WRITE;
/*!40000 ALTER TABLE `user_token` DISABLE KEYS */;
INSERT INTO `user_token` VALUES (1,'OQP0UY8ZUX','1','1','1','1'),(2,'NRVX4QOF8D','2','2','2','2'),(4,'NRVX4QOF8R',NULL,NULL,NULL,NULL),(5,'DUL0G6MLST',NULL,NULL,NULL,NULL),(6,'10DE7A67E2263E847076A16084AD956D',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'u1311857_sys_residential'
--

--
-- Dumping routines for database 'u1311857_sys_residential'
--
/*!50003 DROP FUNCTION IF EXISTS `access_token_fc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE FUNCTION `access_token_fc`(

  p_access_token  varchar(50),

  p_user_id       int unsigned

) RETURNS varchar(1) CHARSET latin1
BEGIN

  

  declare t_val varchar(1) default 'N';



  begin

    select  'Y'

    into    t_val

    from    user_login_data uld

            join user_token ut

              on uld.user_id = ut.user_id

    where   uld.user_id = p_user_id

            and ut.access_token = p_access_token

            and uld.is_active = 'Y';

  end;

  

  set t_val = ifnull(t_val,'N');

  

	RETURN t_val;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_attach_file` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE FUNCTION `get_attach_file`(

  p_history_id    int,

  p_seqno         tinyint

) RETURNS varchar(200) CHARSET latin1
BEGIN

  

  declare t_val varchar(200);



  begin

    select  attach_file

    into    t_val

    from    cc_ticket_history_attach

    where   is_active = 'Y'

            and history_id = p_history_id

            and seqno = p_seqno;

  end;

  

	RETURN t_val;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_message_fc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE FUNCTION `get_message_fc`(

  p_code	        varchar(50)

) RETURNS varchar(200) CHARSET latin1
BEGIN

  

  declare t_message varchar(200);



  begin

    select  message

    into    t_message

    from    messages

    where   language_code = 'in'

            and code = p_code;

  end;

  

	RETURN t_message;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cart_add_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `cart_add_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_cart_type	varchar(30),

  in  p_item_id	int(11)

)
BEGIN

  

  declare t_success smallint; 

  

  declare t_exist	varchar(1) default 'N';

  declare t_cart_id int;

  

  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then

      

    -- cari dulu item tersebut sudah ada atau belum

    select  'Y', cart_id

    into    t_exist, t_cart_id

    from    cart

    where   is_active = 'Y'

            and user_id = p_user_id

            and item_id = p_item_id;

    

    

    if ifnull(t_exist,'N') = 'N' then

      -- insert baru

      insert into cart

      (last_update_date, last_updated_by, creation_date, created_by,

      user_id, cart_type, item_id, qty)

      values

      (now(), p_user_id, now(), p_user_id,

      p_user_id, p_cart_type, p_item_id, 1);

    else

      -- update quantity

      update  cart

      set     qty = qty + 1

      where   cart_id = t_cart_id;

    end if;



                        

    set o_status = 1;

    set o_message = 'Barang berhasil ditambahkan ke keranjang';



  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cart_del_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `cart_del_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_cart_id         int unsigned

)
BEGIN

  

  declare t_success smallint;

  declare t_valid varchar(1);



  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end; 

  

  if access_token_fc(p_access_token, p_user_id) = 'Y' then

    

    update  cart

    set     is_active = 'N',

            last_update_date = now(),

            last_updated_by = p_user_id

    where   cart_id = p_cart_id

            and is_active = 'Y';

    

    select row_count() into t_success;

    

    if t_success <= 0 then

      set o_status = -1;

      select get_message_fc('NOT_FOUND') into o_message;

    else   

      set o_status = 1;

      select get_message_fc('DELETE_SUCCESS') into o_message;

    end if;  



  else

    

    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

    

  end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cart_qty_upd_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `cart_qty_upd_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_cart_id	        int(10) unsigned,

  in  p_qty             int

)
BEGIN

  

  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then

      

    -- update quantity

    update  cart

    set     qty = p_qty

    where   cart_id = p_cart_id;



                        

    set o_status = 1;

    set o_message = '';



  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cc_ticket_approval_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `cc_ticket_approval_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_ticket_id	int(10) unsigned,

  in  p_mtc_approval	varchar(15)

)
BEGIN

  

  declare t_success smallint; 

  

  declare t_kavling_id int;

  declare t_mtc_cost double(15,2);

  declare t_id_trans int;

  

  declare exit handler for sqlexception

  begin

    ROLLBACK;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then



    -- cari data

    select  kavling_id, mtc_cost

    into    t_kavling_id, t_mtc_cost

    from    cc_ticket

    where   ticket_id = p_ticket_id;



    START TRANSACTION;    

    

    if p_mtc_approval = 'approved' then

      

      update  cc_ticket

      set     status = 'unpaid',

              status_date = now(),

              status_by = p_user_id

      where   ticket_id = p_ticket_id;

      

      insert into transaksi_retail

      (last_update_date, last_updated_by, creation_date, created_by,

       type_trans, date_issued, kavling_id, user_id, nominal)

      values

      (now(), p_user_id, now(), p_user_id,

       3, now(), t_kavling_id, p_user_id, t_mtc_cost);

       

      

      set t_id_trans = last_insert_id();

       

      insert into transaksi_retail_detail

      (last_update_date, last_updated_by, creation_date, created_by,

       id_trans, item_id, item_qty, item_hpp, item_price, subtotal)

      values

      (now(), p_user_id, now(), p_user_id,

       t_id_trans, p_ticket_id, 1, 0, t_mtc_cost, t_mtc_cost);

    

    else 

    

      update  cc_ticket

      set     status = p_mtc_approval,

              status_date = now(),

              status_by = p_user_id

      where   ticket_id = p_ticket_id;

    

    end if;

    

    

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



    COMMIT;

  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cc_ticket_closed_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `cc_ticket_closed_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_ticket_id	int(10) unsigned

)
BEGIN

  

  declare t_success smallint; 

  

  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then



    update  cc_ticket

    set     status = 'closed',

            status_date = now(),

            status_by = p_user_id

    where   ticket_id = p_ticket_id;

    

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cc_ticket_history_attach_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `cc_ticket_history_attach_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_history_id	int(10) unsigned,

  in  p_ticket_id	int(10) unsigned,

  in  p_seqno	tinyint(4),

  in  p_attach_file	varchar(100)

)
BEGIN

  

  declare t_success smallint; 

  

  

  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then



    insert into cc_ticket_history_attach

    (last_update_date, last_updated_by, creation_date, created_by,

     history_id, ticket_id, seqno, attach_file)

    values

    (now(), p_user_id, now(), p_user_id,

     p_history_id, p_ticket_id, p_seqno, p_attach_file);

        

    

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cc_ticket_new_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `cc_ticket_new_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  out o_ticket_id        int(10) unsigned,

  out o_history_id        int(10) unsigned,

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_kavling_id	int(11) unsigned,

  in  p_kompleks_id	int(10) unsigned,

  in  p_subject	varchar(200),

  in  p_ticket_type varchar(30),

  in  p_description	varchar(3000)

)
BEGIN

  

  declare t_success smallint; 

  

  declare t_code varchar(5);

  declare t_ticket_num varchar(30);

  

  declare exit handler for sqlexception

  begin

    ROLLBACK;

    set o_ticket_id = 0;

    set o_history_id = 0;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then



    START TRANSACTION;

    

    set t_code = 'MS';

      

    -- generate no order

    select  concat(t_code,date_format(now(),'%y%m'),lpad(ifnull(max(substr(ticket_num,-4)),0) + 1,4,'0'))

    into    t_ticket_num

    from    cc_ticket

    where   substr(ticket_num,1,6) = concat(t_code,date_format(now(),'%y%m'));

  

    if ifnull(t_ticket_num,'') = '' then

      set t_ticket_num = concat(t_code,date_format(now(),'%y%m'),'0001');

    end if;    



    insert into cc_ticket

    (last_update_date, last_updated_by, creation_date, created_by,

     ticket_num, ticket_date, request_by, kavling_id, kompleks_id, 

     subject, ticket_type, description, status, status_date, status_by)

    values

    (now(), p_user_id, now(), p_user_id,

     t_ticket_num, now(), p_user_id, p_kavling_id, p_kompleks_id, 

     p_subject, p_ticket_type, trim(both '\n' from p_description), 'submit', now(), p_user_id);

    

    set o_ticket_id = last_insert_id();

    

    

    

    insert into cc_ticket_history

    (last_update_date, last_updated_by, creation_date, created_by,

     ticket_id, sent_by, send_date, description)

    values

    (now(), p_user_id, now(), p_user_id,

     o_ticket_id, p_user_id, now(), trim(both '\n' from p_description));

     

    set o_history_id = last_insert_id();

    

    

    set o_status = 1;

    select get_message_fc('TICKET_NEW_SUCCESS') into o_message;



    COMMIT;

  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cc_ticket_reply_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `cc_ticket_reply_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  out o_history_id      int,

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_ticket_id	int(10) unsigned,

  in  p_description	varchar(3000),

  in  p_is_mtc	enum('Y','N'),

  in  p_mtc_cost	double(15,2),

  in  p_is_confirmation enum('Y','N'),

  in  p_reply_type varchar(10)

)
BEGIN

  

  declare t_success smallint; 

  

  declare t_push_id int;

  declare t_status varchar(30);

  declare t_ticket_num varchar(20);

  declare t_fcm_token varchar(300);

  declare t_user_id int;

  

  declare exit handler for sqlexception

  begin

    ROLLBACK;

    set o_history_id = 0;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then



    START TRANSACTION;

    

    if p_reply_type = 'user' then

      set t_status = 'user_response';

    else

      set t_status = 'admin_response';

    end if;

    

    insert into cc_ticket_history

    (last_update_date, last_updated_by, creation_date, created_by,

     ticket_id, sent_by, send_date, description, is_mtc, mtc_cost)

    values

    (now(), p_user_id, now(), p_user_id,

     p_ticket_id, p_user_id, now(), trim(both '\n' from p_description), p_is_mtc, p_mtc_cost);

     

    set o_history_id = last_insert_id();

    



    update  cc_ticket

    set     status = t_status,

            status_date = now(),

            status_by = p_user_id,

            is_confirmation = if(p_reply_type = 'user', 'N', p_is_confirmation)

    where   ticket_id = p_ticket_id;    

    

    

    if p_is_mtc = 'Y' then

    

      update  cc_ticket

      set     is_mtc = p_is_mtc,

              mtc_detail = trim(both '\n' from p_description),

              mtc_cost = p_mtc_cost,

              status = 'request_approval',

              status_date = now(),

              status_by = p_user_id

      where   ticket_id = p_ticket_id;

      

    end if;

    

    if p_reply_type <> 'user' then

      

      -- cari data + fcm_token

      select  ut.user_id, ct.ticket_num, ut.fcm_token

      into    t_user_id, t_ticket_num, t_fcm_token

      from    cc_ticket ct

              join user_token ut

                on ct.request_by = ut.user_id

      where   ct.ticket_id = p_ticket_id;

      

      insert into push_notifications

      (push_type, fcm_token)

      values

      ('admin_reply', t_fcm_token);

    

      set t_push_id = last_insert_id();

      

      

      insert into push_notification_parameters

      (push_id, key_label, key_value)

      values

      (t_push_id, 'ticket_id', p_ticket_id),

      (t_push_id, 'title', concat('Reply #',t_ticket_num)),

      (t_push_id, 'message', trim(both '\n' from p_description));   

      



      -- INBOX

      insert into inbox

      (last_update_date, last_updated_by, creation_date, created_by,

       inbox_date, user_id, msg_subject, msg_body, msg_type, source_id)

      values

      (now(), p_user_id, now(), p_user_id,

       now(), t_user_id, concat('Tiket #',t_ticket_num), trim(both '\n' from p_description), 'handyman', p_ticket_id);

      

      

      update  user_counter

      set     inbox_count = inbox_count + 1

      where   user_id = t_user_id;

      

    end if;

    

    

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



    COMMIT;

  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `inbox_del_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `inbox_del_pc`(

  out o_status         tinyint,

  out o_message        varchar(200),

  in  p_access_token   varchar(50),

  in  p_user_id        int unsigned,

  in  p_inbox_id       int unsigned

)
BEGIN   



  declare t_success smallint;



  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end; 

  

  if access_token_fc(p_access_token, p_user_id) = 'Y' then

        

    update  inbox

    set     is_active = 'N',

            last_update_date = now(),

            last_updated_by = p_user_id

    where   inbox_id = p_inbox_id;

    

    select row_count() into t_success;

    

    if t_success <= 0 then

      set o_status = -1;

      select get_message_fc('NOT_FOUND') into o_message;

    else   

      set o_status = 1;

      select get_message_fc('DELETE_SUCCESS') into o_message;

    end if;  

    

  else

    

    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

    

  end if;

     

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `inbox_open_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `inbox_open_pc`(

  in  p_user_id int

)
BEGIN   



  update  user_counter

  set     inbox_count = 0

  where   user_id = p_user_id;

     

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `inbox_read_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `inbox_read_pc`(

  in  p_inbox_id int

)
BEGIN   



  update  inbox

  set     is_read = 'Y'

  where   inbox_id = p_inbox_id;

     

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `midtrans_response_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `midtrans_response_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_id_payment      int(10) unsigned,

  in  p_status_id	int(10),

  in  p_status_code	int(11),

  in  p_transaction_id	varchar(500),

  in  p_transaction_status	varchar(50),

  in  p_transaction_time	datetime,

  in  p_midtrans_payment_type	varchar(50),

  in  p_bank_name	varchar(50),

  in  p_va_number	varchar(50),

  in  p_bill_key	varchar(50),

  in  p_biller_code	varchar(50),

  in  p_payment_code	varchar(50)  

)
BEGIN

  

  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then

       

    update  transaksi_payment

    set     status_id = p_status_id,

            status_code = p_status_code,

            transaction_id = p_transaction_id,

            transaction_status = p_transaction_status,

            transaction_time = p_transaction_time,

            midtrans_payment_type = p_midtrans_payment_type,

            bank_name = p_bank_name,

            va_number = p_va_number,

            bill_key = p_bill_key,

            biller_code = p_biller_code,

            payment_code = p_payment_code

    where   id_payment = p_id_payment;

    

    

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `news_del_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `news_del_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_news_id         int unsigned

)
BEGIN

  

  declare t_success smallint;

  declare t_valid varchar(1);



  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end; 

  

  if access_token_fc(p_access_token, p_user_id) = 'Y' then

    

    update  news

    set     is_active = 'N',

            last_update_date = now(),

            last_updated_by = p_user_id

    where   news_id = p_news_id

            and is_active = 'Y';

    

    select row_count() into t_success;

    

    if t_success <= 0 then

      set o_status = -1;

      select get_message_fc('NOT_FOUND') into o_message;

    else   

      set o_status = 1;

      select get_message_fc('DELETE_SUCCESS') into o_message;

    end if;  



  else

    

    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

    

  end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `news_save_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `news_save_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  out o_news_id     int(10) unsigned,

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_news_id         int unsigned,

  in  p_news_type	varchar(30),

  in  p_subject	varchar(200),

  in  p_description	varchar(5000),

  in  p_start_date	date,

  in  p_end_date	date,

  in  p_file_name	varchar(200)

)
BEGIN

  

  declare t_success smallint; 

  

  declare t_level	smallint(6);

  

  declare exit handler for sqlexception

  begin

    set o_news_id = p_news_id;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then

      

    if p_news_id > 0 then

    

      update  news

      set     news_type = p_news_type,

              subject = p_subject,

              description = p_description,

              start_date = p_start_date,

              end_date = p_end_date,

              file_name = p_file_name,

              last_update_date = now(),

              last_updated_by = p_user_id

      where   news_id = p_news_id

              and is_active = 'Y';

                    

      select row_count() into t_success;

    

      if t_success <= 0 then

        set o_news_id = p_news_id;

        set o_status = -1;

        select get_message_fc('NOT_FOUND') into o_message;

      else      

        set o_news_id = p_news_id;

        set o_status = 1;

        select get_message_fc('SAVE_SUCCESS') into o_message;

      end if;                       

                      

    else

    

      insert into news

      (last_update_date, last_updated_by, creation_date, created_by,

      news_type, subject, description, start_date, end_date, file_name)

      values

      (now(), p_user_id, now(), p_user_id,

      p_news_type, p_subject, p_description, p_start_date, p_end_date, p_file_name);

      

      set o_news_id = last_insert_id();

                        

      set o_status = 1;

      select get_message_fc('SAVE_SUCCESS') into o_message;



    end if;



  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `payment_paid_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `payment_paid_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_id_payment      int(10) unsigned

)
BEGIN

  declare t_user_id int;

  declare t_payment_id varchar(45);

  declare t_total_bayar double;

  

  declare t_ticket_id int;

  declare t_id_trans	bigint(10);

  declare t_type_trans	int(10);



  declare t_last_flag tinyint default 0;

  

  declare detail_cur cursor for

    select  tpd.id_trans, tpd.type_trans

    from    transaksi_payment_detail tpd

    where   tpd.is_active = 'Y'

            and tpd.id_payment = p_id_payment;

 

  declare continue handler for sqlstate '02000' set t_last_flag = 1;    



  

  declare exit handler for sqlexception

  begin

    ROLLBACK;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then

       

    START TRANSACTION;    

    

    -- cari user_id

    select  user_id, payment_id, total + biaya_transaksi

    into    t_user_id, t_payment_id, t_total_bayar

    from    transaksi_payment

    where   id_payment = p_id_payment;

    

       

    update  transaksi_payment

    set     status_id = 2, -- paid / settlement / capture

            date_paid = now()

    where   id_payment = p_id_payment;

    

    

    -- INBOX

    insert into inbox

    (last_update_date, last_updated_by, creation_date, created_by,

     inbox_date, user_id, msg_subject, msg_body, 

     msg_type, source_id)

    values

    (now(), p_user_id, now(), p_user_id,

     now(), t_user_id, 'Pembayaran Berhasil', concat('Pembayaran untuk No Transaksi ',t_payment_id,' Berhasil'), 

     'payment_paid', p_id_payment);



    

    update  user_counter

    set     inbox_count = inbox_count + 1

    where   user_id = t_user_id;

    

    

    set t_last_flag = 0;

    open detail_cur;

    repeat

      fetch detail_cur into t_id_trans, t_type_trans;   



      if t_last_flag <> 1 then

      

        if t_type_trans = 1 then -- ipl

        

          update  transaksi_ipl

          set     status_id = 2, -- paid / settlement / capture

                  date_paid = now()

          where   id_trans = t_id_trans;

                

--         else if t_type_trans = 2 then -- retail

      

        elseif t_type_trans = 3 then -- handyman



          update  transaksi_retail

          set     status_id = 2, -- paid / settlement / capture

                  date_paid = now()

          where   id_trans = t_id_trans;

          

          -- cari ticket_id

          select  item_id

          into    t_ticket_id

          from    transaksi_retail_detail

          where   id_trans = t_id_trans;

          

          update  cc_ticket

          set     status = 'paid',

                  status_date = now(),

                  status_by = p_user_id,

                  is_confirmation = 'Y'

          where   ticket_id = t_ticket_id;



        end if;

                

      end if;

      

    until t_last_flag = 1 end repeat;

    close detail_cur;  

    

        

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



    COMMIT;

  else

  

    ROLLBACK;

    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `push_notification_sent_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `push_notification_sent_pc`(

  in p_push_id int unsigned 

)
BEGIN



  update  push_notifications

  set     is_sent = 'Y',

          send_date = now(),

          is_active = 'N'

  where   push_id = p_push_id;     



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `transaksi_payment_detail_save_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `transaksi_payment_detail_save_pc`(

  out o_status              tinyint,

  out o_message             varchar(200),

  out o_id_payment_detail   int(10) unsigned,

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_id_payment	    bigint(10),

  in  p_id_trans	      bigint(10),

  in  p_type_trans	    int(10),

  in  p_total	          double

)
BEGIN

  

  declare t_exist varchar(1) default 'N';

  declare t_payment_id varchar(45);  

  

  declare exit handler for sqlexception

  begin

    ROLLBACK;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then

       

    START TRANSACTION;       

       

    -- cek exist atau tidak

    select  'Y'

    into    t_exist

    from    transaksi_payment_detail

    where   is_active = 'N'

            and type_trans = p_type_trans

            and id_trans = p_id_trans;

           

       

    if ifnull(t_exist, 'N') = 'N' then

      -- cari payment_id

      select  payment_id

      into    t_payment_id

      from    transaksi_payment

      where   id_payment = p_id_payment;

        

      insert into transaksi_payment_detail

      (last_update_date, last_updated_by, creation_date, created_by,    

      id_payment, id_trans, type_trans, total)

      values

      (now(), p_user_id, now(), p_user_id,

      p_id_payment, p_id_trans, p_type_trans, p_total);

      

      set o_id_payment_detail = last_insert_id();

      

      if (p_type_trans = 1) then -- IPL

      

        update  transaksi_ipl

        set     payment_id = t_payment_id,

                status_id = 1 -- pending

        where   id_trans = p_id_trans;

        

      elseif (p_type_trans in (2, 3)) then -- Handyman



        update  transaksi_retail

        set     payment_id = t_payment_id,

                status_id = 1 -- pending

        where   id_trans = p_id_trans;



      end if;

      

      set o_status = 1;

      select get_message_fc('SAVE_SUCCESS') into o_message;

      

    end if;

    

    COMMIT;



  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `transaksi_payment_save_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `transaksi_payment_save_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  out o_id_payment      int(10) unsigned,

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_unique_code	    varchar(50),

  in  p_payment_id      varchar(45),

  in  p_total	          double,

  in  p_payment_type	  int(11),

  in  p_biaya_transaksi	double

)
BEGIN



  declare t_id_payment int;

  declare t_payment_id varchar(45);

  declare t_total_bayar double;

  

  

  declare exit handler for sqlexception

  begin

    ROLLBACK;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then

  

    START TRANSACTION;

  

    -- cari unique_code sudah ada atau belum

    select  id_payment, payment_id, total + biaya_transaksi

    into    t_id_payment, t_payment_id, t_total_bayar

    from    transaksi_payment

    where   unique_code = p_unique_code;

  

    if ifnull(t_id_payment,0) > 0 then

    

      update  transaksi_payment

      set     total = p_total, 

              payment_type = p_payment_type, 

              biaya_transaksi = p_biaya_transaksi,

              last_update_date = now(),

              last_updated_by = p_user_id

      where   id_payment = t_id_payment;

    

      set o_id_payment = t_id_payment;

      

    else

       

      insert into transaksi_payment

      (last_update_date, last_updated_by, creation_date, created_by,    

      unique_code, date_issued, user_id, payment_id, total, payment_type, biaya_transaksi)

      values

      (now(), p_user_id, now(), p_user_id,

      p_unique_code, now(), p_user_id, p_payment_id, p_total, p_payment_type, p_biaya_transaksi);

    

      set o_id_payment = last_insert_id();

      set t_payment_id = p_payment_id;

    

    end if;

    

    -- INBOX

    insert into inbox

    (last_update_date, last_updated_by, creation_date, created_by,

     inbox_date, user_id, msg_subject, msg_body, 

     msg_type, source_id)

    values

    (now(), p_user_id, now(), p_user_id,

     now(), p_user_id, 'Belum Terbayar', concat('Mohon lakukan pembayaran untuk No Transaksi ',t_payment_id), 

     'payment_unpaid', o_id_payment);

    



    update  user_counter

    set     inbox_count = inbox_count + 1

    where   user_id = p_user_id;

    

    

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



    COMMIT;

  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `transaksi_retail_detail_save_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `transaksi_retail_detail_save_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_id_trans	bigint(10),

  in  p_item_id	bigint(10),

  in  p_item_qty	double,

  in  p_item_price	double

)
BEGIN



  declare t_item_hpp double;

  

  declare exit handler for sqlexception

  begin

    ROLLBACK;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then

  

    -- cari hpp

    select  item_hpp

    into    t_item_hpp

    from    master_item

    where   item_id = p_item_id;

  

    START TRANSACTION;

     

    insert into transaksi_retail_detail

    (last_update_date, last_updated_by, creation_date, created_by,    

    id_trans, item_id, item_qty, item_hpp, item_price, subtotal)

    values

    (now(), p_user_id, now(), p_user_id,

    p_id_trans, p_item_id, p_item_qty, ifnull(t_item_hpp,0), p_item_price, p_item_qty * p_item_price);

  

    

    update  cart

    set     is_active = 'N',

            last_update_date = now(),

            last_updated_by = p_user_id

    where   is_active = 'Y'

            and user_id = p_user_id

            and item_id = p_item_id;

    

    

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



    COMMIT;

  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `transaksi_retail_save_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `transaksi_retail_save_pc`(

  out o_status          tinyint,

  out o_message         varchar(200),

  out o_id_trans      int(10) unsigned,

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_type_trans	int(10),

  in  p_kavling_id	bigint(10),

  in  p_nominal double

)
BEGIN

  

  declare exit handler for sqlexception

  begin

    ROLLBACK;

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;  



  if access_token_fc(p_access_token, p_user_id) = 'Y' then

  

    START TRANSACTION;

     

    insert into transaksi_retail

    (last_update_date, last_updated_by, creation_date, created_by,    

    type_trans, date_issued, kavling_id, user_id, nominal)

    values

    (now(), p_user_id, now(), p_user_id,

    p_type_trans, now(), p_kavling_id, p_user_id, p_nominal);

  

    set o_id_trans = last_insert_id();

    

    

    set o_status = 1;

    select get_message_fc('SAVE_SUCCESS') into o_message;



    COMMIT;

  else



    set o_status = -1;

    select get_message_fc('ACCESS_VIOLATION') into o_message;

  

  end if;

  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_user_token_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `update_user_token_pc`(

  in  p_access_token    varchar(50),

  in  p_user_id         int unsigned,

  in  p_token_type      varchar(20),

  in  p_token           varchar(300)

)
BEGIN   

  if access_token_fc(p_access_token, p_user_id) = 'Y' then

    

    if p_token_type = 'FCM' then

    

      update  user_token

      set     fcm_token = p_token

      where   user_id = p_user_id;

    

    end if;

    

  end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_login_pc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `user_login_pc`(

  out o_status              tinyint,

  out o_message             varchar(200),

  out o_access_token        varchar(50),

  out o_user_id             int unsigned,

  out o_user_name           varchar(30),

  out o_user_full_name      varchar(20),

  out o_user_id_type        tinyint,

  out o_kavling_id          int unsigned,

  out o_kompleks_id         int unsigned,  

  out o_kompleks_name       varchar(200),

  out o_kompleks_address    varchar(200),  

  out o_blok_name           varchar(200),

  out o_house_no            varchar(200),

  in  p_user_name           varchar(50),

  in  p_user_password       varchar(100)

)
BEGIN



  declare t_user_id int unsigned default 0;

  declare t_user_name varchar(30);

  declare t_user_full_name varchar(50);

  declare t_user_id_type tinyint;

  declare t_kavling_id int;

  declare t_kompleks_id int;

  declare t_kompleks_name varchar(200);

  declare t_kompleks_address varchar(200);

  declare t_blok_name varchar(200);

  declare t_house_no varchar(200);

  declare t_access_token varchar(50);



  

  declare exit handler for sqlexception

  begin

    set o_status = -1;

    select get_message_fc('TROUBLE') into o_message;

  end;

  

  begin

    select  uld.user_id, uld.user_name, uld.user_full_name, uld.user_id_type,

            uk.kavling_id, mkav.kompleks_id, mkom.kompleks_name, mkom.kompleks_address, 

            mb.blok_name, mkav.house_no, ut.access_token

    into    t_user_id, t_user_name, t_user_full_name, t_user_id_type, 

            t_kavling_id, t_kompleks_id, t_kompleks_name, t_kompleks_address, 

            t_blok_name, t_house_no, t_access_token

    from    user_login_data uld

            join user_token ut

              on uld.user_id = ut.user_id

            left join user_kavling uk

              on uld.user_id = uk.user_id

                 and uk.is_active = 'Y'

            left join master_kavling mkav

              on uk.kavling_id = mkav.kavling_id

            left join master_blok mb

              on mkav.blok_id = mb.blok_id

            left join master_kompleks mkom

              on mkav.kompleks_id = mkom.kompleks_id

    where   upper(trim(uld.user_name)) = upper(trim(p_user_name))

            and uld.user_password = p_user_password

            and uld.is_active = 'Y'

    limit 1;

  end;

  

  if ifnull(t_user_id, 0) <= 0 then

    

    set o_status = -1;

    select get_message_fc('LOGIN_FAILED') into o_message;

    set o_user_id = 0;

    set o_access_token = null;

  

  else    



    set o_status = 1;

    set o_message = null;

    set o_access_token = t_access_token;

    set o_user_id = t_user_id;

    set o_user_name = t_user_name;

    set o_user_full_name = t_user_full_name;

    set o_user_id_type = t_user_id_type;

    set o_kavling_id = t_kavling_id;

    set o_kompleks_id = t_kompleks_id;

    set o_kompleks_name = t_kompleks_name;

    set o_kompleks_address = t_kompleks_address;

    set o_blok_name = t_blok_name;

    set o_house_no = t_house_no;



  end if;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-22 23:37:45
