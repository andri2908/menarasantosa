CREATE DATABASE  IF NOT EXISTS `menara_santosa` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `menara_santosa`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: menara_santosa
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.19-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `master_blok`
--

DROP TABLE IF EXISTS `master_blok`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_blok` (
  `blok_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `kompleks_id` bigint(10) DEFAULT 0,
  `blok_name` varchar(45) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`blok_id`),
  UNIQUE KEY `blok_name_UNIQUE` (`blok_name`),
  KEY `secondary` (`kompleks_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_blok`
--

LOCK TABLES `master_blok` WRITE;
/*!40000 ALTER TABLE `master_blok` DISABLE KEYS */;
INSERT INTO `master_blok` VALUES (1,1,'apel','Y',1,'2021-07-07 01:02:58',1,'2021-07-07 01:04:10'),(2,1,'jeruk','N',1,'2021-07-07 01:02:58',1,'2021-07-07 01:04:10');
/*!40000 ALTER TABLE `master_blok` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_group`
--

DROP TABLE IF EXISTS `master_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_group` (
  `group_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL DEFAULT '',
  `group_description` varchar(50) NOT NULL DEFAULT '',
  `group_type` tinyint(3) DEFAULT 0,
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_group`
--

LOCK TABLES `master_group` WRITE;
/*!40000 ALTER TABLE `master_group` DISABLE KEYS */;
INSERT INTO `master_group` VALUES (1,'ADMIN','GROUP ADMIN',2,'Y',NULL,NULL,1,'2021-07-02 15:22:41'),(2,'LAND LORD','',1,'Y',NULL,NULL,NULL,NULL),(3,'RESIDENT','',0,'Y',1,'2021-07-20 23:54:54',NULL,NULL);
/*!40000 ALTER TABLE `master_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_item`
--

DROP TABLE IF EXISTS `master_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_item` (
  `item_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_name` varchar(100) DEFAULT '',
  `unit_id` smallint(6) DEFAULT 0,
  `item_price` double DEFAULT 0,
  `qty` double DEFAULT 0,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_item`
--

LOCK TABLES `master_item` WRITE;
/*!40000 ALTER TABLE `master_item` DISABLE KEYS */;
INSERT INTO `master_item` VALUES (1,'TABUNG GAS 15KG',1,20000,18,'Y',1,'2021-07-09 23:45:37',0,NULL),(2,'TABUNG GAS 10KG',2,5000,15,'Y',1,'2021-07-20 10:25:14',0,NULL);
/*!40000 ALTER TABLE `master_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_kavling`
--

DROP TABLE IF EXISTS `master_kavling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_kavling` (
  `kavling_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `kompleks_id` bigint(10) DEFAULT 0,
  `blok_id` bigint(10) DEFAULT 0,
  `house_no` varchar(45) DEFAULT '',
  `biaya_ipl` double DEFAULT 0,
  `durasi_pembayaran` int(10) DEFAULT 0,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`kavling_id`),
  KEY `secondary` (`kompleks_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_kavling`
--

LOCK TABLES `master_kavling` WRITE;
/*!40000 ALTER TABLE `master_kavling` DISABLE KEYS */;
INSERT INTO `master_kavling` VALUES (1,1,1,'F01',250000,2,'Y',1,'2021-07-07 23:26:38',1,'2021-07-07 23:36:36'),(2,1,1,'20',20000,3,'Y',1,'2021-07-07 23:36:52',0,NULL);
/*!40000 ALTER TABLE `master_kavling` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_kompleks`
--

DROP TABLE IF EXISTS `master_kompleks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_kompleks` (
  `kompleks_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `kompleks_name` varchar(50) DEFAULT '',
  `kompleks_address` varchar(200) DEFAULT '',
  `biaya_ipl` double DEFAULT 0,
  `durasi_pembayaran` int(10) DEFAULT 0,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`kompleks_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_kompleks`
--

LOCK TABLES `master_kompleks` WRITE;
/*!40000 ALTER TABLE `master_kompleks` DISABLE KEYS */;
INSERT INTO `master_kompleks` VALUES (1,'SYAILENDRA','PURWOSARI',20000,3,'Y',1,'2021-07-07 01:02:58',1,'2021-07-07 01:04:10');
/*!40000 ALTER TABLE `master_kompleks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_module`
--

DROP TABLE IF EXISTS `master_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_module` (
  `module_id` smallint(6) NOT NULL,
  `module_name` varchar(100) DEFAULT '',
  `module_features` tinyint(3) unsigned DEFAULT 0,
  `module_active` tinyint(3) unsigned DEFAULT 1,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_module`
--

LOCK TABLES `master_module` WRITE;
/*!40000 ALTER TABLE `master_module` DISABLE KEYS */;
INSERT INTO `master_module` VALUES (1,'MANAJEMEN SISTEM',1,1),(2,'TAMBAH / HAPUS GROUP USER',2,1),(3,'TAMBAH / HAPUS USER',2,1),(4,'PENGATURAN AKSES MODUL',1,1),(5,'BACKUP / RESTORE DATABASE',1,1),(6,'PENGATURAN SISTEM APLIKASI',1,1),(21,'TAMBAH / HAPUS KOMPLEKS',2,1),(22,'TAMBAH / HAPUS KAVLING',2,1),(23,'TAMBAH / HAPUS ITEM',2,1),(24,'TAMBAH / HAPUS SATUAN',2,1),(25,'TAMBAH / HAPUS PEMILIK KAVLING',1,1),(41,'RINGKASAN IPL',1,1),(42,'TAMBAH / HAPUS TRANSAKSI RETAIL',2,1);
/*!40000 ALTER TABLE `master_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_module_access`
--

DROP TABLE IF EXISTS `master_module_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_module_access` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` tinyint(3) unsigned NOT NULL,
  `module_id` smallint(6) NOT NULL,
  `user_access_option` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_module_access`
--

LOCK TABLES `master_module_access` WRITE;
/*!40000 ALTER TABLE `master_module_access` DISABLE KEYS */;
INSERT INTO `master_module_access` VALUES (1,1,1,1),(2,1,2,7),(3,1,3,7),(4,1,4,1),(5,1,5,1),(6,1,6,1),(7,1,21,7),(8,1,22,7),(9,1,23,7),(10,1,24,7),(11,1,25,1),(12,1,41,1),(13,1,42,7);
/*!40000 ALTER TABLE `master_module_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_payment_mode`
--

DROP TABLE IF EXISTS `master_payment_mode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_payment_mode` (
  `payment_type` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_type_name` varchar(100) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  PRIMARY KEY (`payment_type`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_payment_mode`
--

LOCK TABLES `master_payment_mode` WRITE;
/*!40000 ALTER TABLE `master_payment_mode` DISABLE KEYS */;
INSERT INTO `master_payment_mode` VALUES (1,'Bank Transfer','Y'),(2,'Gopay','Y'),(3,'ShopeePay','Y'),(4,'Indomaret','Y'),(5,'Alfa Group','Y');
/*!40000 ALTER TABLE `master_payment_mode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_unit`
--

DROP TABLE IF EXISTS `master_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_unit` (
  `unit_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(100) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_unit`
--

LOCK TABLES `master_unit` WRITE;
/*!40000 ALTER TABLE `master_unit` DISABLE KEYS */;
INSERT INTO `master_unit` VALUES (1,'PCS','Y',1,'2021-07-09 23:32:08',0,NULL),(2,'KG','Y',1,'2021-07-09 23:32:15',1,'2021-07-09 23:40:01'),(3,'BOX','Y',1,'2021-07-09 23:40:15',0,NULL);
/*!40000 ALTER TABLE `master_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `language_code` varchar(10) NOT NULL,
  `code` varchar(50) NOT NULL,
  `message` varchar(200) NOT NULL,
  PRIMARY KEY (`language_code`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES ('in','ACCESS_VIOLATION','Anda tidak memiliki hak akses'),('in','CHANGE_PASSWORD_FAILED','Password Lama salah, mohon periksa kembali'),('in','CHANGE_PASSWORD_SUCCESS','Password berhasil diganti'),('in','DELETE_SUCCESS','Data berhasil dihapus'),('in','LOGIN_FAILED','Username / Password salah, mohon ulangi kembali'),('in','NOT_FOUND','Data tidak ditemukan'),('in','SAVE_SUCCESS','Data berhasil disimpan'),('in','TROUBLE','Terjadi gangguan pada sistem, hubungi tim support kami'),('in','USER_DUPLICATE','Username sudah digunakan');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_config`
--

DROP TABLE IF EXISTS `sys_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_config` (
  `ID` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(50) DEFAULT '',
  `company_address` varchar(100) DEFAULT '',
  `company_phone` varchar(20) DEFAULT '',
  `company_email` varchar(50) DEFAULT '',
  `default_printer` varchar(200) DEFAULT '',
  `auto_backup_flag` tinyint(3) unsigned DEFAULT 0,
  `auto_backup_dir` varchar(200) DEFAULT '',
  `print_preview` tinyint(3) unsigned DEFAULT 0,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_config`
--

LOCK TABLES `sys_config` WRITE;
/*!40000 ALTER TABLE `sys_config` DISABLE KEYS */;
INSERT INTO `sys_config` VALUES (1,'PT. MENARA SANTOSA','JL. RONGGOWARSITO NO.73, KEPRABON','(0271) 643100','','PrimoPDF',0,'',0,'Y',0,NULL,1,'2021-07-20 23:28:35');
/*!40000 ALTER TABLE `sys_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_config_transaksi`
--

DROP TABLE IF EXISTS `sys_config_transaksi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_config_transaksi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_type` int(11) DEFAULT 0,
  `biaya_transaksi` double DEFAULT 0,
  `biaya_transaksi_percent` double DEFAULT 0,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_config_transaksi`
--

LOCK TABLES `sys_config_transaksi` WRITE;
/*!40000 ALTER TABLE `sys_config_transaksi` DISABLE KEYS */;
INSERT INTO `sys_config_transaksi` VALUES (1,1,4000,0,'Y',1,'2021-07-20 23:19:58',1,'2021-07-20 23:28:35'),(2,2,0,2,'Y',1,'2021-07-20 23:28:35',0,NULL),(3,3,0,1.5,'Y',1,'2021-07-20 23:28:35',0,NULL),(4,4,0,0,'Y',1,'2021-07-20 23:28:35',0,NULL),(5,5,5000,0,'Y',1,'2021-07-20 23:28:35',0,NULL);
/*!40000 ALTER TABLE `sys_config_transaksi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblusertocheckedrecords`
--

DROP TABLE IF EXISTS `tblusertocheckedrecords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblusertocheckedrecords` (
  `recID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL DEFAULT 0,
  `query` text CHARACTER SET utf8 NOT NULL DEFAULT '',
  `uniqueKey` varchar(30) NOT NULL DEFAULT '',
  `createdDate` datetime NOT NULL DEFAULT current_timestamp(),
  `type` int(11) NOT NULL DEFAULT 0,
  `payment_type` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`recID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblusertocheckedrecords`
--

LOCK TABLES `tblusertocheckedrecords` WRITE;
/*!40000 ALTER TABLE `tblusertocheckedrecords` DISABLE KEYS */;
INSERT INTO `tblusertocheckedrecords` VALUES (1,1,'','p6g9iq2jbeil18oin3dmd41emn1270','2021-07-21 10:55:55',1,1);
/*!40000 ALTER TABLE `tblusertocheckedrecords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblusertocheckedrecordsdetail`
--

DROP TABLE IF EXISTS `tblusertocheckedrecordsdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblusertocheckedrecordsdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recID` int(11) NOT NULL DEFAULT 0,
  `recordID` int(11) NOT NULL DEFAULT 0,
  `trans_type` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblusertocheckedrecordsdetail`
--

LOCK TABLES `tblusertocheckedrecordsdetail` WRITE;
/*!40000 ALTER TABLE `tblusertocheckedrecordsdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblusertocheckedrecordsdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_ipl`
--

DROP TABLE IF EXISTS `transaksi_ipl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaksi_ipl` (
  `id_trans` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_trans` int(10) DEFAULT 0,
  `date_issued` datetime DEFAULT NULL,
  `kavling_id` bigint(10) DEFAULT 0,
  `user_id` bigint(10) DEFAULT 0,
  `payment_id` varchar(45) DEFAULT NULL,
  `nominal` double DEFAULT 0,
  `status_id` int(10) DEFAULT 0,
  `date_paid` datetime DEFAULT NULL,
  `start_ipl` datetime DEFAULT NULL,
  `end_ipl` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_trans`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_ipl`
--

LOCK TABLES `transaksi_ipl` WRITE;
/*!40000 ALTER TABLE `transaksi_ipl` DISABLE KEYS */;
INSERT INTO `transaksi_ipl` VALUES (1,1,'2021-07-15 00:23:05',2,1,'2015949151',60000,0,'2021-07-21 18:16:24','2021-07-09 00:00:00','2021-09-09 00:00:00','Y',1,'2021-07-15 00:23:05',0,NULL);
/*!40000 ALTER TABLE `transaksi_ipl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_ipl_detail`
--

DROP TABLE IF EXISTS `transaksi_ipl_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaksi_ipl_detail` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_trans` bigint(10) DEFAULT 0,
  `nominal` double DEFAULT 0,
  `periode_pembayaran` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_ipl_detail`
--

LOCK TABLES `transaksi_ipl_detail` WRITE;
/*!40000 ALTER TABLE `transaksi_ipl_detail` DISABLE KEYS */;
INSERT INTO `transaksi_ipl_detail` VALUES (1,1,20000,'2021-07-09 00:00:00','Y',1,'2021-07-15 00:23:05',0,NULL),(2,1,20000,'2021-08-09 00:00:00','Y',1,'2021-07-15 00:23:05',0,NULL),(3,1,20000,'2021-09-09 00:00:00','Y',1,'2021-07-15 00:23:05',0,NULL);
/*!40000 ALTER TABLE `transaksi_ipl_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_payment`
--

DROP TABLE IF EXISTS `transaksi_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaksi_payment` (
  `id_payment` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_issued` datetime DEFAULT NULL,
  `payment_id` varchar(45) DEFAULT '',
  `total` double DEFAULT 0,
  `payment_type` int(11) DEFAULT 0,
  `biaya_transaksi` double DEFAULT 0,
  `status_id` int(10) DEFAULT 0,
  `date_paid` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL,
  `transaksi_paymentcol` varchar(45) DEFAULT NULL,
  `status_code` int(11) NOT NULL DEFAULT 0,
  `transaction_id` varchar(500) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transaction_status` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transaction_time` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `midtrans_payment_type` varchar(50) NOT NULL DEFAULT '',
  `bank_name` varchar(50) NOT NULL DEFAULT '',
  `va_number` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `bill_key` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `biller_code` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `payment_code` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`id_payment`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_payment`
--

LOCK TABLES `transaksi_payment` WRITE;
/*!40000 ALTER TABLE `transaksi_payment` DISABLE KEYS */;
INSERT INTO `transaksi_payment` VALUES (1,'2021-07-21 18:41:36','1446156635',65000,1,4000,1,'2021-07-21 18:41:36','Y',1,'2021-07-21 18:41:36',0,NULL,NULL,201,'b67e7e72-d97b-4d39-ba90-e7e691630a25','pending','2021-07-21 18:41:34','bank_transfer','Bni','9883400086116934','','','');
/*!40000 ALTER TABLE `transaksi_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_payment_detail`
--

DROP TABLE IF EXISTS `transaksi_payment_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaksi_payment_detail` (
  `id_payment_detail` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_payment` bigint(10) DEFAULT 0,
  `id_trans` bigint(10) DEFAULT 0,
  `type_trans` int(10) DEFAULT 0,
  `total` double DEFAULT 0,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_payment_detail`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_payment_detail`
--

LOCK TABLES `transaksi_payment_detail` WRITE;
/*!40000 ALTER TABLE `transaksi_payment_detail` DISABLE KEYS */;
INSERT INTO `transaksi_payment_detail` VALUES (1,1,1,2,65000,'Y',1,'2021-07-21 18:41:36',0,NULL);
/*!40000 ALTER TABLE `transaksi_payment_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_retail`
--

DROP TABLE IF EXISTS `transaksi_retail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaksi_retail` (
  `id_trans` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_trans` int(10) DEFAULT 0,
  `date_issued` datetime DEFAULT NULL,
  `kavling_id` bigint(10) DEFAULT 0,
  `user_id` bigint(10) DEFAULT 0,
  `payment_id` varchar(45) DEFAULT NULL,
  `nominal` double DEFAULT 0,
  `status_id` int(10) DEFAULT 0,
  `date_paid` datetime DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_trans`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_retail`
--

LOCK TABLES `transaksi_retail` WRITE;
/*!40000 ALTER TABLE `transaksi_retail` DISABLE KEYS */;
INSERT INTO `transaksi_retail` VALUES (1,2,'2021-07-20 10:34:28',2,1,'1446156635',65000,1,'2021-07-21 18:41:34','Y',1,'2021-07-19 23:55:41',1,'2021-07-20 10:34:41');
/*!40000 ALTER TABLE `transaksi_retail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_retail_detail`
--

DROP TABLE IF EXISTS `transaksi_retail_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaksi_retail_detail` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_trans` bigint(10) DEFAULT 0,
  `item_id` bigint(10) DEFAULT 0,
  `item_qty` double DEFAULT 0,
  `item_price` double DEFAULT 0,
  `subtotal` double DEFAULT 0,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_retail_detail`
--

LOCK TABLES `transaksi_retail_detail` WRITE;
/*!40000 ALTER TABLE `transaksi_retail_detail` DISABLE KEYS */;
INSERT INTO `transaksi_retail_detail` VALUES (1,1,1,2,20000,40000,'Y',1,'2021-07-19 23:55:41',1,'2021-07-20 10:34:41'),(2,1,2,5,5000,25000,'Y',1,'2021-07-20 10:25:26',1,'2021-07-20 10:34:41');
/*!40000 ALTER TABLE `transaksi_retail_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_kavling`
--

DROP TABLE IF EXISTS `user_kavling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_kavling` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) DEFAULT NULL,
  `kavling_id` bigint(10) DEFAULT NULL,
  `start_ipl` datetime DEFAULT NULL,
  `remarks` varchar(200) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT 0,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT 0,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_kavling`
--

LOCK TABLES `user_kavling` WRITE;
/*!40000 ALTER TABLE `user_kavling` DISABLE KEYS */;
INSERT INTO `user_kavling` VALUES (1,1,1,'2021-08-01 15:45:55','AAAAA','N',1,'2021-07-09 15:46:43',1,'2021-07-01 16:14:16'),(2,1,2,'2021-07-09 16:16:15','','Y',1,'2021-07-09 16:16:48',1,'2021-07-20 23:57:32'),(3,2,2,'2021-07-20 23:57:29','','Y',1,'2021-07-20 23:57:32',0,NULL);
/*!40000 ALTER TABLE `user_kavling` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_login_data`
--

DROP TABLE IF EXISTS `user_login_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_login_data` (
  `user_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(15) DEFAULT '',
  `user_password` varchar(15) DEFAULT '',
  `group_id` tinyint(3) unsigned DEFAULT 0,
  `last_login` datetime DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `user_full_name` varchar(100) DEFAULT '',
  `user_id_type` tinyint(3) DEFAULT 0,
  `user_id_no` varchar(100) DEFAULT '',
  `user_phone_1` varchar(45) DEFAULT '',
  `user_phone_2` varchar(45) DEFAULT '',
  `user_email_address` varchar(200) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `SECONDARY` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_login_data`
--

LOCK TABLES `user_login_data` WRITE;
/*!40000 ALTER TABLE `user_login_data` DISABLE KEYS */;
INSERT INTO `user_login_data` VALUES (1,'ADMIN','admin',1,'2021-07-20 23:56:34','2021-07-20 23:57:47','ADMIN 123',1,'123456','081558775652','0111','ariston@alphasoft.com','Y',NULL,NULL,1,'2021-07-19 21:23:12'),(2,'RESIDENT','123',3,NULL,NULL,'RESIDENT',1,'','0123','','','Y',1,'2021-07-20 23:55:19',NULL,NULL);
/*!40000 ALTER TABLE `user_login_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_token`
--

DROP TABLE IF EXISTS `user_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_token` (
  `user_id` bigint(10) unsigned NOT NULL DEFAULT 0,
  `access_token` varchar(300) DEFAULT NULL,
  `fcm_token` varchar(300) DEFAULT NULL,
  `gcm_token` varchar(300) DEFAULT NULL,
  `ios_token` varchar(300) DEFAULT NULL,
  `web_token` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_token`
--

LOCK TABLES `user_token` WRITE;
/*!40000 ALTER TABLE `user_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_token` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-21 19:09:51
