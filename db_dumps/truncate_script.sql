TRUNCATE cart;
TRUNCATE cc_ticket;
TRUNCATE cc_ticket_history;
TRUNCATE cc_ticket_history_attach;
TRUNCATE master_bank;
TRUNCATE master_blok;
TRUNCATE master_group;
TRUNCATE master_item;
TRUNCATE master_kavling;
TRUNCATE master_kompleks;
TRUNCATE master_module_access;
TRUNCATE master_unit;
TRUNCATE messages;
TRUNCATE news;
TRUNCATE penerimaan_detail;
TRUNCATE penerimaan_header;
TRUNCATE penyesuaian_stok;
TRUNCATE push_notification_parameters;
TRUNCATE push_notifications;
TRUNCATE tblusertocheckedrecords;
TRUNCATE tblusertocheckedrecordsdetail;
TRUNCATE transaksi_ipl;
TRUNCATE transaksi_ipl_detail;
TRUNCATE transaksi_payment;
TRUNCATE transaksi_payment_detail;
TRUNCATE transaksi_retail;
TRUNCATE transaksi_retail_detail;
TRUNCATE transaksi_type;
TRUNCATE user_kavling;
TRUNCATE user_login_data;
TRUNCATE user_token;

INSERT INTO `master_group` VALUES (1,'ADMIN','GROUP ADMIN',2,'Y',NULL,NULL,1,'2021-08-01 23:45:44');

INSERT INTO `master_module_access` VALUES (1,1,1,1),(2,1,2,7),(3,1,3,7),(4,1,4,1),(5,1,5,1),(6,1,6,1),(7,1,21,7),(8,1,22,7),(9,1,23,7),(10,1,24,7),(11,1,25,1),(12,1,41,1),(13,1,42,7),(14,1,43,1),(15,1,44,7),(16,7,1,1),(17,7,2,7),(18,7,3,7),(19,7,4,1),(20,7,6,1),(21,7,21,7),(22,7,22,7),(23,7,23,7),(24,7,24,7),(25,7,25,1),(26,7,41,1),(27,7,42,7),(28,7,43,1),(29,7,44,7),(30,1,26,7),(31,1,45,7),(32,1,46,1),(33,1,47,7);

INSERT INTO `user_login_data` VALUES (1,'ADMIN','admin',1,'2021-08-23 18:04:37','2021-08-23 18:04:51','ADMIN1234567',1,'123456','081558775652','0111','ADMIN@ALPHASOFT.COM','Y',NULL,NULL,1,'2021-08-02 23:34:31');

INSERT INTO `user_token` VALUES (1,'10DE7A67E2263E847076A16084AD956D',NULL,NULL,NULL,NULL);
