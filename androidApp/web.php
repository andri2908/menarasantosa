<?php
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*=======================  MidTrans ================================*/
/*$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->post("/coba", "MidtransController@getSnapToken");
    $router->post("/coba/charge", "MidtransController@getSnapToken");
});
*/

/*=======================  CONFIGURATION ================================*/
$router->post('/sendpush_cronjobs', 'ConfigurationController@sendpush_cronjobs');
$router->post('/fcm_token_send', 'ConfigurationController@fcm_token_send');
$router->post('/fcm_topics_send', 'ConfigurationController@fcm_topics_send');


/*=======================  GLOBAL ================================*/
$router->post('/auto_login', 'GlobalController@auto_login');
$router->post('/user_login', 'GlobalController@user_login');
$router->post('/update_token', 'GlobalController@update_token');
$router->post('/change_password', 'GlobalController@change_password');
$router->post('/midtrans_charge', 'GlobalController@midtrans_charge');
$router->post('/midtrans_notification', 'GlobalController@midtrans_notification');


/*=======================  MAIN ================================*/
$router->post('/server_date', 'MainController@server_date');

$router->post('/user_kavling_list', 'MainController@user_kavling_list');
$router->post('/user_kavling_data', 'MainController@user_kavling_data');



$router->post('/cc_list', 'MainController@cc_list');
$router->post('/cc_data', 'MainController@cc_data');
$router->post('/cc_new', 'MainController@cc_new');
$router->post('/cc_reply', 'MainController@cc_reply');
$router->post('/cc_approval', 'MainController@cc_approval');
$router->post('/cc_work', 'MainController@cc_work');
$router->post('/cc_close', 'MainController@cc_close');
$router->post('/cc_confirmation', 'MainController@cc_confirmation');
$router->post('/cc_history_list', 'MainController@cc_history_list');
$router->post('/cc_history_attach_list', 'MainController@cc_history_attach_list');

$router->post('/cc_admin_list', 'MainController@cc_admin_list');


$router->post('/ipl_list', 'MainController@ipl_list');
$router->post('/ipl_data', 'MainController@ipl_data');

$router->post('/tagihan_list', 'MainController@tagihan_list');
$router->post('/tagihan_checkout', 'MainController@tagihan_checkout');
$router->post('/tagihan_paid', 'MainController@tagihan_paid');

$router->post('/payment_list', 'MainController@payment_list');
$router->post('/payment_data', 'MainController@payment_data');
$router->post('/payment_detail', 'MainController@payment_detail');
$router->post('/payment_view_detail', 'MainController@payment_view_detail');


$router->post('/inbox_list', 'MainController@inbox_list');
$router->post('/inbox_del', 'MainController@inbox_del');
$router->post('/inbox_open', 'MainController@inbox_open');
$router->post('/inbox_read', 'MainController@inbox_read');


$router->post('/news_list', 'MainController@news_list');
$router->post('/news_data', 'MainController@news_data');
$router->post('/news_save', 'MainController@news_save');
$router->post('/news_del', 'MainController@news_del');


$router->post('/item_list', 'MainController@item_list');
$router->post('/cart_list', 'MainController@cart_list');
$router->post('/cart_add', 'MainController@cart_add');
$router->post('/cart_qty', 'MainController@cart_qty');
$router->post('/cart_del', 'MainController@cart_del');
$router->post('/cart_checkout', 'MainController@cart_checkout');


$router->post('/product_list', 'MainController@product_list');
$router->post('/product_data', 'MainController@product_data');
$router->post('/product_save', 'MainController@product_save');
$router->post('/product_del', 'MainController@product_del');


$router->post('/retail_undeliver_list', 'MainController@retail_undeliver_list');
$router->post('/retail_undeliver_data', 'MainController@retail_undeliver_data');
$router->post('/retail_undeliver_detail_list', 'MainController@retail_undeliver_detail_list');
$router->post('/retail_undeliver_status', 'MainController@retail_undeliver_status');



/*=======================  LOV ================================*/
$router->post('/payment_method_lov', 'LovController@payment_method_lov');
$router->post('/user_kavling_lov', 'LovController@user_kavling_lov');
$router->post('/ipl_year_lov', 'LovController@ipl_year_lov');
$router->post('/unit_lov', 'LovController@unit_lov');



