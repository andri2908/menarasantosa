<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Validator;
use PDO;
use PDF;

date_default_timezone_set('Asia/Jakarta');

class ConfigurationController extends Controller
{


    // Send Push Notification Cronjobs
    public function sendpush_cronjobs(Request $request) {

        $query = "  select  pn.push_id, pn.push_type
                    from    push_notifications pn
                    where   pn.is_active = 'Y'
                            and pn.is_sent = 'N'";


        $result = \DB::select(\DB::raw($query));  


        if (!empty($result)) {
            foreach ($result as $line) {

                $request->merge(['p_push_id' => $line->push_id]);

                // if ($line->push_type == 'job_to_driver')
                //     $this->sendpush_job_to_driver($request);
                if ($line->push_type == 'blast')
                    $this->sendpush_notification_blast($request);
                else $this->sendpush_notification($request);

            }
        }
    }


    // Send Push Notification (Spesific FCM Token)
    public function sendpush_notification(Request $request){

        $p_push_id   = $request->get('p_push_id');
        

        $query = "  select  pn.server_key, pn.fcm_token
                    from    push_notifications pn
                    where   pn.is_active = 'Y'
                            and pn.is_sent = 'N'
                            and pn.push_id = ".$p_push_id;


        $result = \DB::select(\DB::raw($query));  


        if (!empty($result)) {

            $token = "";
            $server_key = "";
            foreach ($result as $line) {
                $token = $line->fcm_token;
                $server_key = $line->server_key;
            }

            $query_param = "select  pnp.key_label, pnp.key_value
                            from    push_notification_parameters pnp
                            where   pnp.push_id = ".$p_push_id;


            $result_param = \DB::select(\DB::raw($query_param));  

            // namanya associate array ([xxx => yyy])
            $extraNotificationData = array();
            if (!empty($result_param)) {

                foreach ($result_param as $line_param) {
                    $extraNotificationData += [$line_param->key_label => $line_param->key_value];
                }                

            }

            $fcmNotification = [
                //'registration_ids' => $tokenList, //multple token array
                'to'            => "$token", //single token
                'data'          => $extraNotificationData
            ];

            $url = 'https://fcm.googleapis.com/fcm/send';

            $headers = array(
                 'Authorization: key='.$server_key,
                 'Content-Type: application/json'
            );

            // $headers = array(
            //      'Authorization: key=AAAAx_FXXZU:APA91bHDo7SD5CTKS_t5DQAx69AyrlbFUjy4h2GPChxpgVWc29QchDs0elihKNf0J6LsVO2ngcPkWiXu4fQYKtfr3D0d-BwA2MdiVpBEXtGNSR34bO0Hvy_lKC7ejwy60BoINDcFrmxw',
            //      'Content-Type: application/json'
            // );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
            $result_fcm = curl_exec($ch);
            curl_close($ch);


            if (!$result_fcm === false) {
                $pdo = \DB::connection()->getPdo();
                // calling stored procedure command
                $sql = "CALL push_notification_sent_pc(:p_push_id)";

                // prepare for execution of the stored procedure
                $stmt = $pdo->prepare($sql);

                $stmt->bindValue(':p_push_id', $p_push_id, PDO::PARAM_INT);


                // execute the stored procedure
                $stmt->execute();
                $stmt->closeCursor();
            }
        }    
    }



    // Send Push Notification (blast)
    public function sendpush_notification_blast(Request $request){

        $p_push_id   = $request->get('p_push_id');
        

        $query = "  select  pn.fcm_token
                    from    push_notifications pn
                    where   pn.is_active = 'Y'
                            and pn.is_sent = 'N'
                            and pn.push_id = ".$p_push_id;


        $result = \DB::select(\DB::raw($query));  


        if (!empty($result)) {

            foreach ($result as $line) {
                $token = $line->fcm_token;
            }

            $query_param = "select  pnp.key_label, pnp.key_value
                            from    push_notification_parameters pnp
                            where   pnp.push_id = ".$p_push_id;


            $result_param = \DB::select(\DB::raw($query_param));  

            // namanya associate array ([xxx => yyy])
            $extraNotificationData = array();
            if (!empty($result_param)) {

                foreach ($result_param as $line_param) {
                    $extraNotificationData += [$line_param->key_label => $line_param->key_value];
                }                

            }

            $fcmNotification = [
                //'registration_ids' => $tokenList, //multple token array
                'to'            => "/topics/".$token, //single token
                'data'          => $extraNotificationData
            ];

            $url = 'https://fcm.googleapis.com/fcm/send';


            $headers = array(
                 'Authorization: key=AAAAx_FXXZU:APA91bHDo7SD5CTKS_t5DQAx69AyrlbFUjy4h2GPChxpgVWc29QchDs0elihKNf0J6LsVO2ngcPkWiXu4fQYKtfr3D0d-BwA2MdiVpBEXtGNSR34bO0Hvy_lKC7ejwy60BoINDcFrmxw',
                 'Content-Type: application/json'
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
            $result_fcm = curl_exec($ch);
            curl_close($ch);


            if (!$result_fcm === false) {
                $pdo = \DB::connection()->getPdo();
                // calling stored procedure command
                $sql = "CALL push_notification_sent_pc(:p_push_id)";

                // prepare for execution of the stored procedure
                $stmt = $pdo->prepare($sql);

                $stmt->bindValue(':p_push_id', $p_push_id, PDO::PARAM_INT);


                // execute the stored procedure
                $stmt->execute();
                $stmt->closeCursor();
            }
        }    
    }

    // Send Push Notification (Spesific FCM Token)
    public function fcm_token_send(Request $request){

        $p_token   = $request->get('p_token');
        $p_data    = json_decode($request->get('p_data'),true);

        $key = 'AAAAx_FXXZU:APA91bHDo7SD5CTKS_t5DQAx69AyrlbFUjy4h2GPChxpgVWc29QchDs0elihKNf0J6LsVO2ngcPkWiXu4fQYKtfr3D0d-BwA2MdiVpBEXtGNSR34bO0Hvy_lKC7ejwy60BoINDcFrmxw';


        $extraNotificationData = array();
        $extraNotificationData = array_values($p_data);

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'            => $p_token, //single token
            'data'          => $extraNotificationData[0]
        ];

        $url = 'https://fcm.googleapis.com/fcm/send';

        $headers = array(
             'Authorization: key='.$key,
             'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result_fcm = curl_exec($ch);
        curl_close($ch);


        if (!$result_fcm === false) {
            return response()->json([[
                'o_status'         => 1,
                'o_message'        => "Pesan terkirim",
            ]], 200);
        } else {
            return response()->json([[
                'o_status'         => -1,
                'o_message'        => "Pesan tidak terkirim",
            ]], 200);
        }       

    }



    // Send Push Notification (by Topics)
    public function fcm_topics_send(Request $request){

        $p_topics       = $request->get('p_topics');
        $p_data         = json_decode($request->get('p_data'));

        $key = 'AAAAx_FXXZU:APA91bHDo7SD5CTKS_t5DQAx69AyrlbFUjy4h2GPChxpgVWc29QchDs0elihKNf0J6LsVO2ngcPkWiXu4fQYKtfr3D0d-BwA2MdiVpBEXtGNSR34bO0Hvy_lKC7ejwy60BoINDcFrmxw';



        $extraNotificationData = array();
        $extraNotificationData = array_values($p_data);

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'            => "/topics/".$p_topics, //topics
            'data'          => $extraNotificationData[0]
        ];


        $url = 'https://fcm.googleapis.com/fcm/send';

        $headers = array(
             'Authorization: key='.$key,
             'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result_fcm = curl_exec($ch);
        curl_close($ch);


        if (!$result_fcm === false) {
            return response()->json([[
                'o_status'         => 1,
                'o_message'        => "Pesan terkirim",
            ]], 200);
        } else {
            return response()->json([[
                'o_status'         => -1,
                'o_message'        => "Pesan tidak terkirim",
            ]], 200);
        }       

    }


}
