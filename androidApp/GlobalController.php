<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Validator;
use PDO;
use PDF;
use Intervention\image\Facades\Image;

// Configurations
use App\Http\Controllers\Midtrans\Config;
use App\Http\Controllers\Midtrans\CoreApi;



date_default_timezone_set('Asia/Jakarta');

class GlobalController extends Controller
{

    // auto login
    public function auto_login(Request $request){
        
        $validation = Validator::make($request->all(),[ 
            'p_user_id'         => 'required',
            'p_access_token'    => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_user_id        = $request->get('p_user_id');
            $p_access_token   = $request->get('p_access_token');

            $query = "  
                select  uld.user_id, uld.user_name, uld.user_full_name, uld.user_id_type,
                        uk.kavling_id, mkav.kompleks_id, mkom.kompleks_name, mkom.kompleks_address, 
                        mb.blok_name, mkav.house_no, ut.access_token
                from    user_login_data uld
                        join user_token ut
                          on uld.user_id = ut.user_id
                        left join user_kavling uk
                          on uld.user_id = uk.user_id
                             and uk.is_active = 'Y'
                        left join master_kavling mkav
                          on uk.kavling_id = mkav.kavling_id
                        left join master_blok mb
                          on mkav.blok_id = mb.blok_id
                        left join master_kompleks mkom
                          on mkav.kompleks_id = mkom.kompleks_id
                where   uld.user_id = ".$p_user_id."
                        and ut.access_token = '".$p_access_token."'
                limit 1";


            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


    // untuk user login
    public function user_login(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_user_name'      => 'required',
            'p_user_password'   => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_user_name       = $request->get('p_user_name');
            $p_user_password   = $request->get('p_user_password');
            $p_group_type      = $request->get('p_group_type');

            $pdo = \DB::connection()->getPdo();
            // calling stored procedure command
            $sql = "CALL user_login_pc(@o_status, @o_message, @o_access_token, @o_user_id, @o_user_name, @o_user_full_name, @o_user_id_type, @o_kavling_id, @o_kompleks_id, @o_kompleks_name, @o_kompleks_address, @o_blok_name, @o_house_no, :p_user_name, :p_user_password, :p_group_type)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindValue(':p_user_name', $p_user_name, PDO::PARAM_STR);
            $stmt->bindValue(':p_user_password', $p_user_password, PDO::PARAM_STR);
            $stmt->bindValue(':p_group_type', $p_group_type, PDO::PARAM_STR);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor();

            // execute the second query to get output
            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message, @o_access_token as access_token, @o_user_id as user_id, @o_user_name as user_name, @o_user_full_name as user_full_name, @o_user_id_type as user_id_type, @o_kavling_id as kavling_id, @o_kompleks_id as kompleks_id, @o_kompleks_name as kompleks_name, @o_kompleks_address as kompleks_address, @o_blok_name as blok_name, @o_house_no as house_no")->fetch(PDO::FETCH_ASSOC);   

            \DB::commit();
            $result [] = $row; // convert object ke array (biar output seragam semua)

            return response()->json($result, 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }    


    // update_token
    public function update_token(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
            'p_token_type'          => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_token_type           = $request->get('p_token_type');
            $p_token                = $request->get('p_token');

            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL update_user_token_pc(:p_access_token, :p_user_id, :p_token_type, :p_token)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindValue(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindValue(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindValue(':p_token_type', $p_token_type, PDO::PARAM_STR);
            $stmt->bindValue(':p_token', $p_token, PDO::PARAM_STR);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor();

            \DB::commit();

            return response()->json([[
                'o_status'  => 1,
                'o_message' => '',
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }  

    // change password
    public function change_password(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
            'p_old_password'        => 'required',
            'p_new_password'        => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_old_password         = $request->get('p_old_password');
            $p_new_password         = $request->get('p_new_password');

            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL change_password_pc(@o_status, @o_message, :p_access_token, :p_user_id, :p_old_password, :p_new_password)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindValue(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindValue(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindValue(':p_old_password', $p_old_password, PDO::PARAM_STR);
            $stmt->bindValue(':p_new_password', $p_new_password, PDO::PARAM_STR);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor();

            // execute the second query to get output
            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message")->fetch(PDO::FETCH_ASSOC);   

            \DB::commit();
            $result [] = $row; // convert object ke array (biar output seragam semua)

            return response()->json($result, 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }  
 

/*=============================  M I D T R A N S ===================================*/

    // midtrans_charge
    public function midtrans_charge(Request $request){

        try {

            $p_payment_type         = $request->get('p_payment_type');
            $p_transaction_details  = json_decode($request->get('p_transaction_details'));
            $p_bank_transfer        = json_decode($request->get('p_bank_transfer'));
            $p_bca_klikbca  = json_decode($request->get('p_bca_klikbca'));
            $p_cstore  = json_decode($request->get('p_cstore'));
            $p_customer_details  = json_decode($request->get('p_customer_details'));
            $p_gopay  = json_decode($request->get('p_gopay'));
            $p_item_details  = json_decode($request->get('p_item_details'));
            $p_shopeepay  = json_decode($request->get('p_shopeepay'));



            // BANK TRANSFER DENGAN VIRTUAL ACCOUNT (VA)
            if ($p_payment_type == "bank_transfer") {
                $order_id = "";
                $gross_amount = 0;
                // transaction_details
                if (is_array($p_transaction_details) || is_object($p_transaction_details))
                {
                    foreach ($p_transaction_details as $transaction_details) {
                        $order_id = $transaction_details->order_id;
                        $gross_amount = $transaction_details->gross_amount;
                    }
                }

                $bank = "";
                $va_number = "";
                // bank_transfer
                if (is_array($p_bank_transfer) || is_object($p_bank_transfer))
                {
                    foreach ($p_bank_transfer as $bank_transfer) {
                        $bank = $bank_transfer->bank;
                        $va_number = $bank_transfer->va_number;
                    }
                }


                $transaction = array(
                    "payment_type" => $p_payment_type,
                    "transaction_details" => [
                        "order_id" => $order_id,
                        "gross_amount" => $gross_amount,
                    ],
                    "bank_transfer" => [
                        "bank" => $bank,
                        "va_number" => $va_number,
                    ]
                );
            }

/*            $transaction = array(
                "payment_type" => "bank_transfer",
                "transaction_details" => [
                    "gross_amount" => 150000,
                    "order_id" => date('Y-m-dHis')
                ],
                // "customer_details" => [
                //     "email" => "chiki@bumbum.com",
                //     "first_name" => "Azhar",
                //     "last_name" => "Ogi",
                //     "phone" => "+628948484848"
                // ],
                // "item_details" => array([
                //     "id" => "1388998298204",
                //     "price" => 5000,
                //     "quantity" => 1,
                //     "name" => "Panci Miako"
                // ], [
                //     "id" => "1388998298202",
                //     "price" => 5000,
                //     "quantity" => 1,
                //     "name" => "Ayam Geprek"
                // ]),
                "bank_transfer" => [
                    "bank" => "bca",
                    "va_number" => "1400000272363",
                ]
            );
*/
            $charge = CoreApi::charge($transaction);
            if (!$charge) {
                return response()->json([[
                    'o_status'  => -1,
                    'o_message' => 'Error',                    
                ]], 200);
            }

            return response()->json([[
                'o_status'  => 1,
                'o_message' => 'Transaksi Berhasil dibuat',
                'o_result'  => $charge,
            ]], 200);
        } catch (\Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage()
            ]], 200);
        }
    } 

}
