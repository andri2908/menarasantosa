<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use PDO;
use Intervention\image\Facades\Image;

date_default_timezone_set('Asia/Jakarta');

class LovController extends Controller
{
    

    // payment_method_lov
    public function payment_method_lov(Request $request){
        
        try {
            $p_key   = !empty($request->get('p_key')) ? $request->get('p_key') : 0;

            $query = "  select  99 as seqno, 'N' as flag,  
                                mpm.payment_type, mpm.seqno, mpm.payment_type_name, 
                                ifnull(sct.biaya_transaksi,0) as biaya_transaksi, ifnull(sct.biaya_transaksi_percent,0) as biaya_transaksi_percent,
                                mpm.payment_code as midtrans_payment_type, mpm.bank as midtrans_bank, mpm.cstore as midtrans_cstore,
                                mpm.logo
                        from    master_payment_mode mpm
                                left join sys_config_transaksi sct
                                  on mpm.payment_type = sct.payment_type
                        where   mpm.is_active = 'Y'
                                and mpm.payment_type <> ".$p_key."
                        union
                        select  1 as seqno, 'Y' as flag,
                                mpm.payment_type, mpm.seqno, mpm.payment_type_name, 
                                ifnull(sct.biaya_transaksi,0) as biaya_transaksi, ifnull(sct.biaya_transaksi_percent,0) as biaya_transaksi_percent,
                                mpm.payment_code as midtrans_payment_type, mpm.bank as midtrans_bank, mpm.cstore as midtrans_cstore,
                                mpm.logo
                        from    master_payment_mode mpm
                                left join sys_config_transaksi sct
                                  on mpm.payment_type = sct.payment_type
                        where   mpm.is_active = 'Y'
                                and mpm.payment_type = ".$p_key."
                        order by 1, 4";

            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }
    

    // user_kavling_lov
    public function user_kavling_lov(Request $request){
        
        try {
            $p_user_id  = $request->get('p_user_id');
            $p_key      = !empty($request->get('p_key')) ? $request->get('p_key') : 0;

            $query = "  select  99 as seqno, 'N' as flag,  
                                uk.kavling_id as id_int, 1 as seqno, '' as id_string, 
                                concat(mb.blok_name, ' ', mkav.house_no, ', ', mkom.kompleks_name) as description, '' as detail
                        from    user_kavling uk
                                join master_kavling mkav
                                  on uk.kavling_id = mkav.kavling_id
                                join master_kompleks mkom
                                  on mkav.kompleks_id = mkom.kompleks_id
                                join master_blok mb
                                  on mkav.blok_id = mb.blok_id
                        where   uk.is_active = 'Y'
                                and uk.user_id = ".$p_user_id."
                                and uk.kavling_id <> ".$p_key."
                        union
                        select  1 as seqno, 'Y' as flag,
                                uk.kavling_id as id_int, 1 as seqno, '' as id_string, 
                                concat(mb.blok_name, ' ', mkav.house_no, ', ', mkom.kompleks_name) as description, '' as detail
                        from    user_kavling uk
                                join master_kavling mkav
                                  on uk.kavling_id = mkav.kavling_id
                                join master_kompleks mkom
                                  on mkav.kompleks_id = mkom.kompleks_id
                                join master_blok mb
                                  on mkav.blok_id = mb.blok_id
                        where   uk.is_active = 'Y'
                                and uk.kavling_id = ".$p_key."
                        order by 1, 4, 6";

            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


    // ipl_year_lov
    public function ipl_year_lov(Request $request){
        
        try {
            $p_kavling_id   = $request->get('p_kavling_id');
            $p_key          = !empty($request->get('p_key')) ? $request->get('p_key') : 0;

            $query = "  select  distinct 99 as seqno, 'N' as flag,  
                                0 as id_int, 1 as seqno, date_format(tid.periode_pembayaran,'%Y') as id_string, 
                                date_format(tid.periode_pembayaran,'%Y') as description, '' as detail
                        from    transaksi_ipl ti
                                join transaksi_ipl_detail tid
                                  on ti.id_trans = tid.id_trans
                                     and tid.is_active = 'Y'
                        where   ti.is_active = 'Y'
                                and ti.kavling_id = ".$p_kavling_id."
                                and date_format(tid.periode_pembayaran,'%Y') <> '".$p_key."'
                        union
                        select  1 as seqno, 'Y' as flag,
                                0 as id_int, 1 as seqno, date_format(tid.periode_pembayaran,'%Y') as id_string, 
                                date_format(tid.periode_pembayaran,'%Y') as description, '' as detail
                        from    transaksi_ipl ti
                                join transaksi_ipl_detail tid
                                  on ti.id_trans = tid.id_trans
                                     and tid.is_active = 'Y'
                        where   ti.is_active = 'Y'
                                and date_format(tid.periode_pembayaran,'%Y') = '".$p_key."'
                        order by 1, 4, 6";

            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


    // unit_lov
    public function unit_lov(Request $request){
        
        try {
            $p_key  = !empty($request->get('p_key')) ? $request->get('p_key') : 0;

            $query = "  select  distinct 99 as seqno, 'N' as flag ,  
                                mu.unit_id as id_int, '' as id_string, 
                                mu.unit_name as description, '' as detail
                        from    master_unit mu
                        where   mu.is_active = 'Y'
                                and mu.unit_id <> ".$p_key."
                        union
                        select  1 as seqno, 'Y' as flag,
                                mu.unit_id as id_int, '' as id_string, 
                                mu.unit_name as description, '' as detail
                        from    master_unit mu
                        where   mu.is_active = 'Y'
                                and mu.unit_id = ".$p_key."
                        order by 1, 3, 6";

            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }
}
