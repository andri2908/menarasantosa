<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use PDO;

// Configurations
use App\Http\Controllers\Midtrans\Config;
use App\Http\Controllers\Midtrans\CoreApi;


date_default_timezone_set('Asia/Jakarta');

class MainController extends Controller
{

    // date server
    public function server_date(Request $request){
        
        try {
            $query = "  select  now() as server_date"; 


            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


    // user_kavling_list
    public function user_kavling_list(Request $request){
        
        $validation = Validator::make($request->all(),[ 
            'p_user_id'   => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_user_id  = $request->get('p_user_id');


            $query = "  
                select  uk.kavling_id, mkav.kompleks_id, mkom.kompleks_name, 
                        concat(mb.blok_name, ' ', mkav.house_no, ', ', mkom.kompleks_name) as address_detail,
                        mkav.status as status, mkom.img_kompleks as kompleks_photo
                from    user_kavling uk
                        left join master_kavling mkav
                          on uk.kavling_id = mkav.kavling_id
                        left join master_blok mb
                          on mkav.blok_id = mb.blok_id
                        left join master_kompleks mkom
                          on mkav.kompleks_id = mkom.kompleks_id
                where   uk.user_id = ".$p_user_id;



            $result = \DB::select(\DB::raw($query));

            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


    // user_kavling_data
    public function user_kavling_data(Request $request){
        
        $validation = Validator::make($request->all(),[ 
            'p_kavling_id'   => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_kavling_id  = $request->get('p_kavling_id');


            $query = "  
                select  uk.user_qr_code
                from    user_kavling uk                        
                where   uk.kavling_id = ".$p_kavling_id;



            $result = \DB::select(\DB::raw($query));

            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }

/*====================== CUSTOMER CARE ===============================*/

    // cc_list
    public function cc_list(Request $request){
        
        try {
            $p_user_id           = !empty($request->get('p_user_id')) ? $request->get('p_user_id') : 0; 
            $p_ticket_type       = $request->get('p_ticket_type'); 
            $p_is_closed         = !empty($request->get('p_is_closed')) ? $request->get('p_is_closed') : 'N'; 
            $p_last_id           = !empty($request->get('p_last_id')) ? $request->get('p_last_id') : '99999999999'; 
            $p_filter_start_date = !empty($request->get('p_filter_start_date')) ? $request->get('p_filter_start_date') : NULL;
            $p_filter_end_date   = !empty($request->get('p_filter_end_date')) ? $request->get('p_filter_end_date') : NULL;

            if ($p_last_id == '0')
                $p_last_id = '9999999999';


            $date_start_str = "";
            if ($p_filter_start_date != NULL){
                $date_start_str = " and date(ct.ticket_date) >= date_format('".$p_filter_start_date."','%Y-%m-%d')";
            }

            $date_end_str = "";
            if ($p_filter_end_date != NULL){
                $date_end_str = " and date(ct.ticket_date) <= date_format('".$p_filter_end_date."','%Y-%m-%d')";
            }


            $query = "  
                select  ct.ticket_id, ct.ticket_num, ct.ticket_date, ct.subject, ct.description, ct.status, ct.status_date,                		
                        case
                        when ct.status = 'submit' then
                            'Tiket Dibuat'
                        when ct.status = 'user_response' then
                            concat('Balasan ', uld.user_full_name)
                        when ct.status = 'admin_response' then
                            concat('Balasan ', uld.user_full_name)
                        when ct.status = 'request_approval' then
                            'Menunggu Persetujuan'
                        when ct.status = 'approved' then
                            'Setuju'
                        when ct.status = 'rejected' then
                            'Tidak Setuju'
                        when ct.status = 'closed' then
                            'Selesai'
                        when ct.status = 'unpaid' then
                            'Menunggu Pembayaran'
                        when ct.status = 'paid' then
                            'Pembayaran Lunas'
                        when ct.status = 'on_progress' then
                            'Dalam Pengerjaan'
                        end status_display, ct.is_mtc, ifnull(ct.mtc_cost,0) as mtc_cost,
                        case
                        when ct.is_confirmation = 'Y' and ct.is_closed = 'N' then
                        	'Y'
                        else
                        	'N'
                        end is_confirmation,
                        ct.last_admin_respon, ct.start_date, ct.vendor_name, ct.finish_date,
                        concat(date_format(ct.ticket_date,'%y%m%d'),lpad(ct.ticket_id,5,'0')) as concat_data
                from    cc_ticket ct
                		join user_login_data uld
                		  on ct.status_by = uld.user_id
                where   ct.is_active = 'Y'
                        and ct.request_by = ".$p_user_id."
                        and ct.ticket_type = '".$p_ticket_type."'
                        and ct.is_closed = '".$p_is_closed."'
                        and concat(date_format(ct.ticket_date,'%y%m%d'),lpad(ct.ticket_id,5,'0')) < '".$p_last_id."'".
                        $date_start_str.
                        $date_end_str."
                order by concat(date_format(ct.ticket_date,'%y%m%d'),lpad(ct.ticket_id,5,'0')) desc
                limit 30";


            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


    // cc_data
    public function cc_data(Request $request){
        
        $validation = Validator::make($request->all(),[ 
            'p_ticket_id'   => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_ticket_id  = $request->get('p_ticket_id');


            $query = "  
                select  ct.ticket_id, ct.ticket_type, ct.ticket_num, ct.ticket_date, ct.request_by, ct.kavling_id, mkav.house_no,
                        ct.kompleks_id, mkom.kompleks_name, mkom.kompleks_address, 
                        ct.subject, ct.description,
                        ct.is_mtc, ct.mtc_detail, ifnull(ct.mtc_cost, 0) as mtc_cost, ct.mtc_attach_file, ct.mtc_approval, ct.mtc_approval_date,
                        ct.is_confirmation, ct.is_closed, ct.status
                from    cc_ticket ct
                        join master_kavling mkav
                          on ct.kavling_id = mkav.kavling_id
                        join master_kompleks mkom
                          on ct.kompleks_id = mkom.kompleks_id
                where   ct.is_active = 'Y'
                        and ct.ticket_id = ".$p_ticket_id;



            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }



    // cc_new
    public function cc_new(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'    => 'required',
            'p_user_id'         => 'required',
            'p_kavling_id'      => 'required',
            'p_kompleks_id'     => 'required',
            'p_subject'         => 'required',
            'p_description'     => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token       = $request->get('p_access_token');
            $p_user_id            = $request->get('p_user_id');
            $p_kavling_id         = $request->get('p_kavling_id');
            $p_kompleks_id        = $request->get('p_kompleks_id');
            $p_subject            = $request->get('p_subject');
            $p_ticket_type        = $request->get('p_ticket_type');
            $p_description        = $request->get('p_description');
            $p_attach             = json_decode($request->get('p_attach'));

            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL cc_ticket_new_pc(@o_status, @o_message, @o_ticket_id, @o_history_id, :p_access_token, :p_user_id, :p_kavling_id, :p_kompleks_id, :p_subject, :p_ticket_type, :p_description)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_kavling_id', $p_kavling_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_kompleks_id', $p_kompleks_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_subject', $p_subject, PDO::PARAM_STR);
            $stmt->bindParam(':p_ticket_type', $p_ticket_type, PDO::PARAM_STR);
            $stmt->bindParam(':p_description', $p_description, PDO::PARAM_STR);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
		

            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message, @o_ticket_id as o_ticket_id, @o_history_id as o_history_id")->fetch(PDO::FETCH_ASSOC);   
        
            $o_status = $row['o_status'];
            $o_message = $row['o_message'];
            $o_ticket_id = $row['o_ticket_id'];
            $o_history_id = $row['o_history_id'];


            if ($o_status == "1") {

                // save lampiran
                if (is_array($p_attach) || is_object($p_attach))
                {
                    foreach ($p_attach as $attach) {
                        try {
                            $base64                 = base64_decode($attach->raw_photo);
                            $p_seqno                = $attach->seqno;
                            $imageName              = md5($o_history_id.'_'.$p_seqno.$this->now->format('dmY_His')).'.jpg';
                            $originalPath           = '../public/sys_contents/img_cs/';



                            // insert atau update
                            if (strlen($attach->raw_photo) > 0)
                                file_put_contents($originalPath.$imageName, $base64);

                               
                            // calling stored procedure command
                            $sql = "CALL cc_ticket_history_attach_pc(@o_status, @o_message, :p_access_token, :p_user_id, :p_history_id, :p_ticket_id, :p_seqno, :p_attach_file)";

                            // prepare for execution of the stored procedure
                            $stmt = $pdo->prepare($sql);

                            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
                            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
                            $stmt->bindParam(':p_history_id', $o_history_id, PDO::PARAM_INT);
                            $stmt->bindParam(':p_ticket_id', $o_ticket_id, PDO::PARAM_INT);
                            $stmt->bindParam(':p_seqno', $p_seqno, PDO::PARAM_INT);
                            $stmt->bindParam(':p_attach_file', $imageName, PDO::PARAM_STR);

                            // execute the stored procedure
                            $stmt->execute();
                            $stmt->closeCursor();  


                        } catch (Exception $e) {
                            $flag = false;
                        }
                    }            
                } 
            }




            \DB::commit();
            return response()->json([[
                'o_status'         => $o_status,
                'o_message'        => $o_message,
                'o_ticket_id'      => $o_ticket_id,
                'o_history_id'     => $o_history_id,
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }       



    // cc_reply
    public function cc_reply(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'    => 'required',
            'p_user_id'         => 'required',
            'p_ticket_id'       => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token       = $request->get('p_access_token');
            $p_user_id            = $request->get('p_user_id');
            $p_ticket_id          = $request->get('p_ticket_id');
            $p_description        = $request->get('p_description');
            $p_is_mtc             = !empty($request->get('p_is_mtc')) ? $request->get('p_is_mtc') : 'N';
            $p_mtc_cost           = !empty($request->get('p_mtc_cost')) ? $request->get('p_mtc_cost') : 0;
            $p_is_confirmation    = !empty($request->get('p_is_confirmation')) ? $request->get('p_is_confirmation') : 'N';
            $p_reply_type         = $request->get('p_reply_type');
            $p_attach             = json_decode($request->get('p_attach'));

            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL cc_ticket_reply_pc(@o_status, @o_message, @o_history_id, @o_date, :p_access_token, :p_user_id, :p_ticket_id, :p_description, :p_is_mtc, :p_mtc_cost, :p_is_confirmation, :p_reply_type)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_ticket_id', $p_ticket_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_description', $p_description, PDO::PARAM_STR);
            $stmt->bindParam(':p_is_mtc', $p_is_mtc, PDO::PARAM_STR);
            $stmt->bindParam(':p_mtc_cost', $p_mtc_cost, PDO::PARAM_STR);
            $stmt->bindParam(':p_is_confirmation', $p_is_confirmation, PDO::PARAM_STR);
            $stmt->bindParam(':p_reply_type', $p_reply_type, PDO::PARAM_STR);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
        

            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message, @o_history_id as o_history_id, @o_date as o_date")->fetch(PDO::FETCH_ASSOC);   
        
            $o_status = $row['o_status'];
            $o_message = $row['o_message'];
            $o_history_id = $row['o_history_id'];
            $o_date = $row['o_date'];


            $imageName = "";
            if ($o_status == "1") {

                // save lampiran
                if (is_array($p_attach) || is_object($p_attach))
                {
                    foreach ($p_attach as $attach) {
                        try {

                            $base64                 = base64_decode($attach->raw_photo);
                            $p_seqno                = $attach->seqno;
                            $imageName              = md5($o_history_id.'_'.$p_seqno.$this->now->format('dmY_His')).'.jpg';
                            $originalPath           = '../public/sys_contents/img_cs/';



                            // insert atau update
                            if (strlen($attach->raw_photo) > 0)
                                file_put_contents($originalPath.$imageName, $base64);

                               
                            // calling stored procedure command
                            $sql = "CALL cc_ticket_history_attach_pc(@o_status, @o_message, :p_access_token, :p_user_id, :p_history_id, :p_ticket_id, :p_seqno, :p_attach_file)";

                            // prepare for execution of the stored procedure
                            $stmt = $pdo->prepare($sql);

                            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
                            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
                            $stmt->bindParam(':p_history_id', $o_history_id, PDO::PARAM_INT);
                            $stmt->bindParam(':p_ticket_id', $p_ticket_id, PDO::PARAM_INT);
                            $stmt->bindParam(':p_seqno', $p_seqno, PDO::PARAM_INT);
                            $stmt->bindParam(':p_attach_file', $imageName, PDO::PARAM_STR);

                            // execute the stored procedure
                            $stmt->execute();
                            $stmt->closeCursor();  

                        } catch (Exception $e) {
                            $flag = false;
                        }
                    }            
                } 
            }




            \DB::commit();
            return response()->json([[
                'o_status'         => $o_status,
                'o_message'        => $o_message,
                'o_history_id'     => $o_history_id,
                'o_date'           => $o_date,
                'o_attach_file'    => $imageName,                
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }       



    // cc_approval
    public function cc_approval(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'    => 'required',
            'p_user_id'         => 'required',
            'p_ticket_id'      => 'required',
            'p_mtc_approval'     => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token       = $request->get('p_access_token');
            $p_user_id            = $request->get('p_user_id');
            $p_ticket_id          = $request->get('p_ticket_id');
            $p_mtc_approval       = $request->get('p_mtc_approval');

            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL cc_ticket_approval_pc(@o_status, @o_message, :p_access_token, :p_user_id, :p_ticket_id, :p_mtc_approval)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_ticket_id', $p_ticket_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_mtc_approval', $p_mtc_approval, PDO::PARAM_STR);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
        

            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message")->fetch(PDO::FETCH_ASSOC);   
        
            $o_status = $row['o_status'];
            $o_message = $row['o_message'];


            \DB::commit();
            return response()->json([[
                'o_status'         => $o_status,
                'o_message'        => $o_message,
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }  


    // cc_work
    public function cc_work(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'    => 'required',
            'p_user_id'         => 'required',
            'p_ticket_id'       => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token       = $request->get('p_access_token');
            $p_user_id            = $request->get('p_user_id');
            $p_ticket_id          = $request->get('p_ticket_id');
            $p_start_date          = $request->get('p_start_date');
            $p_vendor_name          = $request->get('p_vendor_name');

            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL cc_ticket_work_pc(@o_status, @o_message, :p_access_token, :p_user_id, :p_ticket_id, :p_start_date, :p_vendor_name)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_ticket_id', $p_ticket_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_start_date', $p_start_date, PDO::PARAM_STR);
            $stmt->bindParam(':p_vendor_name', $p_vendor_name, PDO::PARAM_STR);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
        

            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message")->fetch(PDO::FETCH_ASSOC);   
        
            $o_status = $row['o_status'];
            $o_message = $row['o_message'];


            \DB::commit();
            return response()->json([[
                'o_status'         => $o_status,
                'o_message'        => $o_message,
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }  


    // cc_close
    public function cc_close(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'    => 'required',
            'p_user_id'         => 'required',
            'p_ticket_id'       => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token       = $request->get('p_access_token');
            $p_user_id            = $request->get('p_user_id');
            $p_ticket_id          = $request->get('p_ticket_id');

            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL cc_ticket_closed_pc(@o_status, @o_message, :p_access_token, :p_user_id, :p_ticket_id)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_ticket_id', $p_ticket_id, PDO::PARAM_INT);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
        

            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message")->fetch(PDO::FETCH_ASSOC);   
        
            $o_status = $row['o_status'];
            $o_message = $row['o_message'];


            \DB::commit();
            return response()->json([[
                'o_status'         => $o_status,
                'o_message'        => $o_message,
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }  


    // cc_confirmation
    public function cc_confirmation(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'    => 'required',
            'p_user_id'         => 'required',
            'p_ticket_id'       => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token       = $request->get('p_access_token');
            $p_user_id            = $request->get('p_user_id');
            $p_ticket_id          = $request->get('p_ticket_id');

            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL cc_confirmation_pc(@o_status, @o_message, :p_access_token, :p_user_id, :p_ticket_id)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_ticket_id', $p_ticket_id, PDO::PARAM_INT);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
        

            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message")->fetch(PDO::FETCH_ASSOC);   
        
            $o_status = $row['o_status'];
            $o_message = $row['o_message'];


            \DB::commit();
            return response()->json([[
                'o_status'         => $o_status,
                'o_message'        => $o_message,
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }  

    // cc_history_list
    public function cc_history_list(Request $request){
        
        $validation = Validator::make($request->all(),[ 
            'p_ticket_id'   => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_ticket_id  = $request->get('p_ticket_id');


            $query = "  
                select  ccth.history_id, ct.request_by, ccth.sent_by, 
                        case
                        when ct.request_by = ccth.sent_by then
                            'user'
                        else
                            'admin'
                        end sender_type,
                        uld.user_full_name as send_name, ccth.send_date, ccth.description, 
                        get_attach_file(ccth.history_id,1) attach_file1, get_attach_file(ccth.history_id,2) attach_file2, 
                        get_attach_file(ccth.history_id,3) attach_file3, get_attach_file(ccth.history_id,4) attach_file4,
                        case
                        when length(get_attach_file(ccth.history_id,1) > 0 ) or length(get_attach_file(ccth.history_id,2) > 0 )
                             or length(get_attach_file(ccth.history_id,3) > 0 ) or length(get_attach_file(ccth.history_id,4) > 0 ) then
                            'Y'
                        else
                            'N'
                        end is_attach,
                        ccth.is_mtc, ccth.mtc_cost
                from    cc_ticket_history ccth
                        join cc_ticket ct
                          on ccth.ticket_id = ct.ticket_id
                        join user_login_data uld
                          on ccth.sent_by = uld.user_id
                where   ccth.is_active = 'Y'
                        and ccth.ticket_id = ".$p_ticket_id."
                order by ccth.send_date";



            $result = \DB::select(\DB::raw($query));

            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


    // cc_history_attach_list
    public function cc_history_attach_list(Request $request){
        
        $validation = Validator::make($request->all(),[ 
            'p_history_id'   => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_history_id  = $request->get('p_history_id');


            $query = "  
                select  cctha.attach_id, cctha.seqno, cctha.attach_file
                from    cc_ticket_history_attach cctha
                where   cctha.is_active = 'Y'
                        and cctha.history_id = ".$p_history_id."
                order by cctha.seqno";



            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }    


    // cc_admin_list
    public function cc_admin_list(Request $request){
        
        try {
            $p_ticket_type       = $request->get('p_ticket_type'); 
            $p_type              = $request->get('p_type'); 
            $p_last_id           = !empty($request->get('p_last_id')) ? $request->get('p_last_id') : '99999999999'; 

            if ($p_last_id == '0')
                $p_last_id = '9999999999';


            $type_str = "";
            if ($p_type == 'outstanding') {
                $type_str = " and (ct.status in ('submit','user_response','on_progress')
                                   or (ct.status = 'paid' and ifnull(ct.vendor_name,'') = '')
                                  )";
            } elseif ($p_type == 'replied') {
                $type_str = " and (ct.status in ('admin_response')
                                   or (ct.status = 'paid' and length(ct.vendor_name) > 0)
                                  )";
            }


            $query = "  
                select  ct.ticket_id, ct.ticket_num, ct.ticket_date, ct.subject, ct.description, ct.status, ct.status_date,                     
                        case
                        when ct.status = 'submit' then
                            'Tiket Dibuat'
                        when ct.status = 'user_response' then
                            concat('Balasan ', uld.user_full_name)
                        when ct.status = 'admin_response' then
                            concat('Balasan ', uld.user_full_name)
                        when ct.status = 'request_approval' then
                            'Menunggu Persetujuan'
                        when ct.status = 'approved' then
                            'Setuju'
                        when ct.status = 'rejected' then
                            'Tidak Setuju'
                        when ct.status = 'closed' then
                            'Selesai'
                        when ct.status = 'unpaid' then
                            'Menunggu Pembayaran'
                        when ct.status = 'paid' then
                            'Pembayaran Lunas'
                        when ct.status = 'on_progress' then
                            'Dalam Pengerjaan'
                        end status_display, ct.is_mtc, ifnull(ct.mtc_cost,0) as mtc_cost,
                        case
                        when ct.is_confirmation = 'Y' and ct.is_closed = 'N' then
                            'Y'
                        else
                            'N'
                        end is_confirmation,
                        ct.last_admin_respon, ct.start_date, ct.vendor_name, ct.finish_date,
                        concat(date_format(ct.ticket_date,'%y%m%d'),lpad(ct.ticket_id,5,'0')) as concat_data
                from    cc_ticket ct
                        join user_login_data uld
                          on ct.status_by = uld.user_id
                where   ct.is_active = 'Y'
                        and ct.is_closed = 'N'
                        and ct.ticket_type = '".$p_ticket_type."'
                        and concat(date_format(ct.ticket_date,'%y%m%d'),lpad(ct.ticket_id,5,'0')) < '".$p_last_id."'".
                        $type_str."
                order by concat(date_format(ct.ticket_date,'%y%m%d'),lpad(ct.ticket_id,5,'0')) desc
                limit 30";


            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }    


/*=================================== I P L ========================================*/

    // ipl_list
    public function ipl_list(Request $request){
        
        $validation = Validator::make($request->all(),[ 
            'p_user_id'   => 'required',
            'p_kavling_id'   => 'required',
            'p_tahun'   => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_user_id      = $request->get('p_user_id');
            $p_kavling_id   = $request->get('p_kavling_id');
            $p_tahun        = $request->get('p_tahun');


            $query = "  
                select  p.period, p.bulan, ifnull(t.nominal, 0) as nominal, t.date_paid, t.is_lunas, 
                        case
                        when p.period < (select date_format(start_ipl,'%Y%m') from user_kavling where user_id = ".$p_user_id." and kavling_id = ".$p_kavling_id." limit 1) then
                          'X'
                        when t.is_lunas = 'Y' then
                          'Y'
                        when t.is_lunas = 'N' and p.period <= date_format(now(),'%Y%m') then
                          'N'
                        else
                          'Z'
                        end status
                from
                (
                  select  concat('".$p_tahun."','01') as period, '01' as bulan
                  union
                  select  concat('".$p_tahun."','02') as period, '02' as bulan
                  union
                  select  concat('".$p_tahun."','03') as period, '03' as bulan
                  union
                  select  concat('".$p_tahun."','04') as period, '04' as bulan
                  union
                  select  concat('".$p_tahun."','05') as period, '05' as bulan
                  union
                  select  concat('".$p_tahun."','06') as period, '06' as bulan
                  union
                  select  concat('".$p_tahun."','07') as period, '07' as bulan
                  union
                  select  concat('".$p_tahun."','08') as period, '08' as bulan
                  union
                  select  concat('".$p_tahun."','09') as period, '09' as bulan
                  union
                  select  concat('".$p_tahun."','10') as period, '10' as bulan
                  union
                  select  concat('".$p_tahun."','11') as period, '11' as bulan
                  union
                  select  concat('".$p_tahun."','12') as period, '12' as bulan
                ) p
                left join 
                (
                  select  date_format(tid.periode_pembayaran,'%Y%m') as period, tid.nominal,
                          date_format(tid.periode_pembayaran,'%m') as bulan, date_paid, 
                          case
                          when ti.status_id <= 1 then
                            'N'
                          when ti.status_id = 2 then
                            'Y'
                          end is_lunas
                  from    transaksi_ipl ti
                          join transaksi_ipl_detail tid
                            on ti.id_trans = tid.id_trans
                              and tid.is_active = 'Y'
                  where   ti.is_active = 'Y'
                          and ti.kavling_id = ".$p_kavling_id."
                          and date_format(tid.periode_pembayaran,'%Y') = '".$p_tahun."'
                ) t
                  on p.period = t.period
                order by p.period";



            $result = \DB::select(\DB::raw($query));

            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


    // ipl_data
    public function ipl_data(Request $request){
        
        $validation = Validator::make($request->all(),[ 
            'p_kavling_id'   => 'required',
            'p_tahun'   => 'required',
            'p_bulan'   => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_kavling_id  = $request->get('p_kavling_id');
            $p_tahun  = $request->get('p_tahun');
            $p_bulan  = $request->get('p_bulan');


            $query = "  
                select  tid.periode_pembayaran, ti.payment_id, tid.nominal, ti.date_paid, mpm.payment_type_name,
                        case
                        when tp.status_id = 1 then
                            'Belum Terbayar'
                        when tp.status_id = 2 then
                            'Sudah Terbayar'
                        end status
                from    transaksi_ipl_detail tid
                        join transaksi_ipl ti
                          on tid.id_trans = ti.id_trans
                             and ti.kavling_id = ".$p_kavling_id."
                        left join transaksi_payment_detail tpd
                          on tpd.is_active = 'Y'
                             and ti.type_trans = tpd.type_trans
                             and ti.id_trans = tpd.id_trans
                        left join transaksi_payment tp
                          on tpd.id_payment = tp.id_payment
                             and tp.status_id <= 2
                        left join master_payment_mode mpm
                          on tp.payment_type = mpm.payment_type
                where   tid.is_active = 'Y'
                        and date_format(tid.periode_pembayaran,'%Y') = '".$p_tahun."'
                        and date_format(tid.periode_pembayaran,'%m') = '".$p_bulan."'
                limit 1";



            $result = \DB::select(\DB::raw($query));

            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


/*=================================== T A G I H A N ========================================*/

    // tagihan_list
    public function tagihan_list(Request $request){
        
        $validation = Validator::make($request->all(),[ 
            'p_user_id'   => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_user_id          = $request->get('p_user_id');
            $p_filter_tagihan   = $request->get('p_filter_tagihan');

            $filter_ipl_str = "";
            $filter_retail_str = "";
            $filter_repair_str = "";
            if ($p_filter_tagihan == "ipl") {
                $filter_ipl_str = " and ti.type_trans = 1";
                $filter_retail_str = " and tr.type_trans = 1";
                $filter_repair_str = " and tr.type_trans = 1";
            }


            $query = "  
                select  1 as seqno, tr.type_trans, 
                        'handyman' as type_trans_name, tr.id_trans, ct.subject as description, 
                        concat(mb.blok_name, ' ', mkav.house_no, ', ', mkom.kompleks_name) as detail,
                        tr.nominal as total
				from    transaksi_retail tr
                        join master_kavling mkav
                          on tr.kavling_id = mkav.kavling_id
                        join master_kompleks mkom
                          on mkav.kompleks_id = mkom.kompleks_id
                        join master_blok mb
                          on mkav.blok_id = mb.blok_id
                        join transaksi_retail_detail rtd
                          on rtd.is_active = 'Y'
                             and tr.id_trans = rtd.id_trans
				        join cc_ticket ct
				          on rtd.item_id = ct.ticket_id
				where   tr.is_active = 'Y'
                        and tr.status_id = 0
                        and tr.type_trans = 3
				        and tr.user_id = ".$p_user_id.
                        $filter_repair_str."
                union
                select  2 as seqno, ti.type_trans, 'ipl' as type_trans_name, ti.id_trans, 
				        case
				        when date(ti.start_ipl) = date(ti.end_ipl) then
				          concat('Biaya IPL ', date_format(ti.start_ipl,'%b-%y'))
				        else
				          concat('Biaya IPL ', date_format(ti.start_ipl,'%b-%y'), ' s/d ', date_format(ti.end_ipl,'%b-%y'), ' (',date_format(end_ipl,'%Y%m') - date_format(start_ipl,'%Y%m') + 1,' bulan)')
				        end description,
                        concat(mb.blok_name, ' ', mkav.house_no, ', ', mkom.kompleks_name) as detail,
				        ti.nominal as total
				from    transaksi_ipl ti
                        join master_kavling mkav
                          on ti.kavling_id = mkav.kavling_id
                        join master_kompleks mkom
                          on mkav.kompleks_id = mkom.kompleks_id
                        join master_blok mb
                          on mkav.blok_id = mb.blok_id
				where   ti.is_active = 'Y'
                        and ti.status_id = 0
				        and ti.user_id = ".$p_user_id.
                        $filter_ipl_str."
                union
                select  3 as seqno, tr.type_trans, 
                        case
                        when tr.type_trans = 2 then 
                            'shop'
                        when tr.type_trans = 4 then 
                            'drugs'
                        end type_trans_name, tr.id_trans, 
                        case
                        when tr.type_trans = 2 then 
                            'Transaksi Minimarket'
                        when tr.type_trans = 4 then 
                            'Transaksi Apotek'
                        end description, 
                        concat(mb.blok_name, ' ', mkav.house_no, ', ', mkom.kompleks_name) as detail,
                        tr.nominal as total
                from    transaksi_retail tr
                        join master_kavling mkav
                          on tr.kavling_id = mkav.kavling_id
                        join master_kompleks mkom
                          on mkav.kompleks_id = mkom.kompleks_id
                        join master_blok mb
                          on mkav.blok_id = mb.blok_id
                        join transaksi_retail_detail rtd
                          on rtd.is_active = 'Y'
                             and tr.id_trans = rtd.id_trans
                where   tr.is_active = 'Y'
                        and tr.status_id = 0
                        and tr.type_trans in (2,4)
                        and tr.date_expired >= now()
                        and tr.user_id = ".$p_user_id.
                        $filter_retail_str."
				order by 1, 3";



            $result = \DB::select(\DB::raw($query));

            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }

    // tagihan_checkout
    public function tagihan_checkout(Request $request){

        \DB::beginTransaction();

        try {

            $d = new \DateTime();
            $payment_id = $d->format("YmdHisv");

            $p_access_token     = $request->get('p_access_token');
            $p_user_id          = $request->get('p_user_id');
            $p_unique_code      = $request->get('p_unique_code');
            $p_total            = $request->get('p_total');
            $p_biaya_transaksi  = $request->get('p_biaya_transaksi');
            $p_lines            = json_decode($request->get('p_lines'));



            $p_payment_type         = $request->get('p_payment_type');
            $p_payment_type_code    = $request->get('p_payment_type_code');
            $p_transaction_details  = json_decode($request->get('p_transaction_details'));
            $p_customer_details     = json_decode($request->get('p_customer_details'));
            $p_item_details         = json_decode($request->get('p_item_details'));
            $p_bank_transfer        = json_decode($request->get('p_bank_transfer'));
            $p_cstore               = json_decode($request->get('p_cstore'));


            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL transaksi_payment_save_pc(@o_status, @o_message, @o_id_payment, :p_access_token, :p_user_id, :p_unique_code, :p_payment_id, :p_total, :p_payment_type, :p_biaya_transaksi)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_unique_code', $p_unique_code, PDO::PARAM_STR);
            $stmt->bindParam(':p_payment_id', $payment_id, PDO::PARAM_STR);
            $stmt->bindParam(':p_total', $p_total, PDO::PARAM_STR);
            $stmt->bindParam(':p_payment_type', $p_payment_type, PDO::PARAM_INT);
            $stmt->bindParam(':p_biaya_transaksi', $p_biaya_transaksi, PDO::PARAM_STR);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
        

            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message, @o_id_payment as o_id_payment")->fetch(PDO::FETCH_ASSOC);   
        
            $o_status = $row['o_status'];
            $o_message = $row['o_message'];
            $o_id_payment = $row['o_id_payment'];


            // return midtrans
            $status_code = "";
            $status_message = "";
            $transaction_id = "";
            $order_id = "";
            $gross_amount = "";
            $payment_type = "";
            $transaction_time = "";
            $transaction_status = "";
            $fraud_status = "";
            $currency = "";
            $va_number = "";
            $bill_key = "";
            $biller_code = "";
            $url_deeplink = "";
            $merchant_id = "";
            $payment_code = "";


            if ($o_status == "1") {

                // save lampiran
                if (is_array($p_lines) || is_object($p_lines))
                {
                    foreach ($p_lines as $lines) {
                        try {

                            // calling stored procedure command
                            $sql = "CALL transaksi_payment_detail_save_pc(@o_status, @o_message, @o_id_payment_detail, :p_access_token, :p_user_id, :p_id_payment, :p_id_trans, :p_type_trans, :p_total)";

                            // prepare for execution of the stored procedure
                            $stmt = $pdo->prepare($sql);

                            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
                            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
                            $stmt->bindParam(':p_id_payment', $o_id_payment, PDO::PARAM_INT);
                            $stmt->bindParam(':p_id_trans', $lines->id_trans, PDO::PARAM_INT);
                            $stmt->bindParam(':p_type_trans', $lines->type_trans, PDO::PARAM_INT);
                            $stmt->bindParam(':p_total', $lines->total, PDO::PARAM_STR);

                            // execute the stored procedure
                            $stmt->execute();
                            $stmt->closeCursor();  

                        } catch (Exception $e) {
                        }

                    }

                }

            }



            $bank = "";
            $cstore = "";

            $order_id = "";
            $gross_amount = 0;
            // transaction_details
            if (is_array($p_transaction_details) || is_object($p_transaction_details))
            {
                foreach ($p_transaction_details as $transaction_details) {
                    $order_id = $payment_id;
                    $gross_amount = $transaction_details->gross_amount;
                }
            }

            if ($p_payment_type_code == "bank_transfer") {
                // BANK TRANSFER DENGAN VIRTUAL ACCOUNT (BCA, PERMATA, BNI, BRI)

                // bank_transfer
                if (is_array($p_bank_transfer) || is_object($p_bank_transfer))
                {
                    foreach ($p_bank_transfer as $bank_transfer) {
                        $bank = $bank_transfer->bank;
                    }
                }


                $transaction = array(
                    "payment_type" => $p_payment_type_code,
                    "transaction_details" => [
                        "order_id" => $order_id,
                        "gross_amount" => $gross_amount,
                    ],
                    "bank_transfer" => [
                        "bank" => $bank,
                    ]
                );
            } elseif ($p_payment_type_code == "echannel") {
                // BANK TRANSFER DENGAN VIRTUAL ACCOUNT (MANDIRI)      

                $transaction = array(
                    "payment_type" => $p_payment_type_code,
                    "transaction_details" => [
                        "order_id" => $order_id,
                        "gross_amount" => $gross_amount,
                    ],
                    "echannel" => [
                        "bill_info1" => "bayar untuk",
                        "bill_info2" => "debt",
                    ]
                );

            } elseif ($p_payment_type_code == "gopay") {

                $transaction = array(
                    "payment_type" => $p_payment_type_code,
                    "transaction_details" => [
                        "order_id" => $order_id,
                        "gross_amount" => $gross_amount,
                    ],
                    "gopay" => [
                        "enable_callback" => true,
                        "callback_url" => "app://menarasantosa/ewallet?total=".$gross_amount."&payment_type=GoPay&payment_id=".$payment_id."&id_payment=".$o_id_payment
                    ]
                );

                        // "callback_url" => "app://menarasantosa/gopay"

            } elseif ($p_payment_type_code == "shopeepay") {

                $transaction = array(
                    "payment_type" => $p_payment_type_code,
                    "transaction_details" => [
                        "order_id" => $order_id,
                        "gross_amount" => $gross_amount,
                    ],
                    "shopeepay" => [
                        "callback_url" => "app://menarasantosa/ewallet?total=".$gross_amount."&payment_type=ShopeePay&payment_id=".$payment_id."&id_payment=".$o_id_payment
                    ]
                );

            } elseif ($p_payment_type_code == "cstore") {

                // bank_transfer
                if (is_array($p_cstore) || is_object($p_cstore))
                {
                    foreach ($p_cstore as $cstores) {
                        $cstore = $cstores->store;
                    }
                }

                $transaction = array(
                    "payment_type" => $p_payment_type_code,
                    "transaction_details" => [
                        "order_id" => $order_id,
                        "gross_amount" => $gross_amount,
                    ],
                    "cstore" => [
                        "store" => $cstore,
                    ]
                );

            }


            $charge = CoreApi::charge($transaction);
            if (!$charge) {
                return response()->json([[
                    'o_status'  => -1,
                    'o_message' => 'Error',                    
                ]], 200);
            } 




            if ($p_payment_type_code == "bank_transfer") {

                if ($bank == "permata") {

                    $status_code = $charge->status_code;
                    $status_message = $charge->status_message;
                    $transaction_id = $charge->transaction_id;
                    $order_id = $charge->order_id;
                    $gross_amount = $charge->gross_amount;
                    $payment_type = $charge->payment_type;
                    $transaction_time = $charge->transaction_time;
                    $transaction_status = $charge->transaction_status;
                    $fraud_status = $charge->fraud_status;
                    $currency = $charge->currency;
                    $va_number = $charge->permata_va_number;
                    $merchant_id = $charge->merchant_id;

                } if ($bank == "bca" || $bank == "bni" || $bank == "bri") {

                    $status_code = $charge->status_code;
                    $status_message = $charge->status_message;
                    $transaction_id = $charge->transaction_id;
                    $order_id = $charge->order_id;
                    $gross_amount = $charge->gross_amount;
                    $payment_type = $charge->payment_type;
                    $transaction_time = $charge->transaction_time;
                    $transaction_status = $charge->transaction_status;
                    $fraud_status = $charge->fraud_status;
                    $currency = $charge->currency;
                    $merchant_id = $charge->merchant_id;

                    foreach ($charge->va_numbers as $va) {
                        $va_number = $va->va_number;
                    }

                }

            } elseif ($p_payment_type_code == "echannel") {

                $status_code = $charge->status_code;
                $status_message = $charge->status_message;
                $transaction_id = $charge->transaction_id;
                $order_id = $charge->order_id;
                $gross_amount = $charge->gross_amount;
                $payment_type = $charge->payment_type;
                $transaction_time = $charge->transaction_time;
                $transaction_status = $charge->transaction_status;
                $fraud_status = $charge->fraud_status;
                $bill_key = $charge->bill_key;
                $biller_code = $charge->biller_code;
                $currency = $charge->currency;

            } elseif ($p_payment_type_code == "gopay") {

                $status_code = $charge->status_code;
                $status_message = $charge->status_message;
                $transaction_id = $charge->transaction_id;
                $order_id = $charge->order_id;
                $gross_amount = $charge->gross_amount;
                $payment_type = $charge->payment_type;
                $transaction_time = $charge->transaction_time;
                $transaction_status = $charge->transaction_status;
                $currency = $charge->currency;

                foreach ($charge->actions as $action) {

                    if ($action->name == "deeplink-redirect") {
                        $url_deeplink = $action->url;
                    }
                }

            } elseif ($p_payment_type_code == "shopeepay") {

                $status_code = $charge->status_code;
                $status_message = $charge->status_message;
                $transaction_id = $charge->transaction_id;
                $order_id = $charge->order_id;
                $gross_amount = $charge->gross_amount;
                $payment_type = $charge->payment_type;
                $transaction_time = $charge->transaction_time;
                $transaction_status = $charge->transaction_status;
                $fraud_status = $charge->fraud_status;
                $currency = $charge->currency;
                $merchant_id = $charge->merchant_id;

                foreach ($charge->actions as $action) {

                    if ($action->name == "deeplink-redirect") {
                        $url_deeplink = $action->url;
                    }
                }

            } elseif ($p_payment_type_code == "cstore") {

                $status_code = $charge->status_code;
                $status_message = $charge->status_message;
                $transaction_id = $charge->transaction_id;
                $order_id = $charge->order_id;
                $gross_amount = $charge->gross_amount;
                $payment_type = $charge->payment_type;
                $transaction_time = $charge->transaction_time;
                $transaction_status = $charge->transaction_status;
                $payment_code = $charge->payment_code;

            }




            // untuk update table
            try {

                $status_id = 1; // PENDING

                // calling stored procedure command
                $sql = "CALL midtrans_response_pc(@o_status, @o_message, :p_access_token, :p_user_id, :p_id_payment, :p_status_id, :p_status_code, :p_transaction_id, :p_transaction_status, :p_transaction_time, :p_midtrans_payment_type, :p_bank_name, :p_va_number, :p_bill_key, :p_biller_code, :p_payment_code)";

                // prepare for execution of the stored procedure
                $stmt = $pdo->prepare($sql);

                $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
                $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
                $stmt->bindParam(':p_id_payment', $o_id_payment, PDO::PARAM_INT);
                $stmt->bindParam(':p_status_id', $status_id, PDO::PARAM_INT); 
                $stmt->bindParam(':p_status_code', $status_code, PDO::PARAM_STR);
                $stmt->bindParam(':p_transaction_id', $transaction_id, PDO::PARAM_STR);
                $stmt->bindParam(':p_transaction_status', $transaction_status, PDO::PARAM_STR);
                $stmt->bindParam(':p_transaction_time', $transaction_time, PDO::PARAM_STR);
                $stmt->bindParam(':p_midtrans_payment_type', $payment_type, PDO::PARAM_STR);
                $stmt->bindParam(':p_bank_name', $bank, PDO::PARAM_STR);
                $stmt->bindParam(':p_va_number', $va_number, PDO::PARAM_STR);
                $stmt->bindParam(':p_bill_key', $bill_key, PDO::PARAM_STR);
                $stmt->bindParam(':p_biller_code', $biller_code, PDO::PARAM_STR);
                $stmt->bindParam(':p_payment_code', $payment_code, PDO::PARAM_STR);

                // execute the stored procedure
                $stmt->execute();
                $stmt->closeCursor();  

            } catch (Exception $e) {
            }

            \DB::commit();      

            return response()->json([[
                'o_status'      => 1,
                'o_message'     => 'Transaksi Berhasil dibuat',
                'o_payment_id'  => $payment_id,
                'charge'        => $charge,
                'status_code'           => $status_code,
                'status_message'        => $status_message,
                'transaction_id'        => $transaction_id,
                'order_id'              => $order_id,
                'gross_amount'          => $gross_amount,
                'payment_type'          => $payment_type,
                'transaction_time'      => $transaction_time,
                'transaction_status'    => $transaction_status,
                'fraud_status'          => $fraud_status,
                'permata_va_number'     => $va_number,
                'currency'              => $currency,
                'va_number'             => $va_number,
                'bill_key'              => $bill_key,
                'biller_code'           => $biller_code,
                'url_deeplink'          => $url_deeplink,
                'merchant_id'           => $merchant_id,
                'payment_code'          => $payment_code,
            ]], 200);
        } catch (\Exception $e) {
            \DB::rollback();

            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    } 


    // tagihan_paid
    public function tagihan_paid(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'    => 'required',
            'p_user_id'         => 'required',
            'p_id_payment'      => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token       = $request->get('p_access_token');
            $p_user_id            = $request->get('p_user_id');
            $p_id_payment         = $request->get('p_id_payment');

            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL payment_paid_pc(@o_status, @o_message, :p_access_token, :p_user_id, :p_id_payment)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_id_payment', $p_id_payment, PDO::PARAM_INT);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
        

            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message")->fetch(PDO::FETCH_ASSOC);   
        
            $o_status = $row['o_status'];
            $o_message = $row['o_message'];


            \DB::commit();
            return response()->json([[
                'o_status'         => $o_status,
                'o_message'        => $o_message,
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }  



/*====================== PAYMENT ===============================*/

    // payment_list
    public function payment_list(Request $request){
        
        try {
            $p_user_id           = !empty($request->get('p_user_id')) ? $request->get('p_user_id') : 0; 
            $p_status_id         = $request->get('p_status_id');
            $p_filter_start_date = !empty($request->get('p_filter_start_date')) ? $request->get('p_filter_start_date') : NULL;
            $p_filter_end_date   = !empty($request->get('p_filter_end_date')) ? $request->get('p_filter_end_date') : NULL;
            $p_last_id           = !empty($request->get('p_last_id')) ? $request->get('p_last_id') : '99999999999'; 


            if ($p_last_id == '0')
                $p_last_id = '9999999999';


            $status_str = "";
            if ($p_status_id != NULL) {
                $status_str = " and tp.status_id = ".$p_status_id.
                                " and date_add(tp.transaction_time,interval 1 day) >= now()";

            }

            $date_start_str = "";
            if ($p_filter_start_date != NULL){
                $date_start_str = " and date(tp.date_issued) >= date_format('".$p_filter_start_date."','%Y-%m-%d')";
            }

            $date_end_str = "";
            if ($p_filter_end_date != NULL){
                $date_end_str = " and date(tp.date_issued) <= date_format('".$p_filter_end_date."','%Y-%m-%d')";
            }


            $query = "  
                select  tp.id_payment, tp.date_issued, tp.payment_id, tp.payment_type, mpm.payment_type_name,
                        tp.date_paid, tp.total + tp.biaya_transaksi as total_tagihan,
                        concat(date_format(tp.date_issued,'%y%m%d'),lpad(tp.id_payment,8,'0')) as concat_data
                from    transaksi_payment tp
                        join master_payment_mode mpm
                          on tp.payment_type = mpm.payment_type
                where   tp.is_active = 'Y'
                        and concat(date_format(tp.date_issued,'%y%m%d'),lpad(tp.id_payment,8,'0')) < '".$p_last_id."'
                        and tp.user_id = ".$p_user_id.
                        $status_str.
                        $date_start_str.
                        $date_end_str."                        
                order by concat(date_format(tp.date_issued,'%y%m%d'),lpad(tp.id_payment,8,'0'))
                limit 30";



            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


    // payment_data
    public function payment_data(Request $request){
        
        try {
            $p_id_payment  = $request->get('p_id_payment'); 


            $query = "  
                select  tp.id_payment, tp.date_issued, tp.payment_id, tp.total, tp.biaya_transaksi, 
                        tp.total + tp.biaya_transaksi as total_tagihan, tp.status_id, tp.date_paid,
                        case
                        when tp.status_id = 1 and date_add(tp.transaction_time,interval 1 day) >= now() then
                            'BELUM LUNAS' 
                        when tp.status_id = 1 and date_add(tp.transaction_time,interval 1 day) < now() then
                            'GAGAL BAYAR' 
                        when tp.status_id = 2 then
                            'PEMBAYARAN LUNAS'
                        else
                            'GAGAL'
                        end status,
                        case
                        when tp.status_id = 1 and date_add(tp.transaction_time,interval 1 day) >= now() then
                            concat('Lakukan pembayaran sebelum ', date_format(date_add(tp.transaction_time,interval 1 day),'%d-%b-%Y %H:%i'))
                        when tp.status_id = 1 and date_add(tp.transaction_time,interval 1 day) < now() then
                            'Waktu pembayaran sudah melewati batas waktu' 
                        when tp.status_id = 2 then
                            'Terima kasih atas pembayaran yang sudah dilakukan'
                        else
                            '-'
                        end status_note,
                        tp.midtrans_payment_type, tp.payment_type, mpm.payment_type_name, tp.va_number, tp.bill_key
                from    transaksi_payment tp
                        join master_payment_mode mpm
                          on tp.payment_type = mpm.payment_type
                where   tp.id_payment = ".$p_id_payment;


            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


    // payment_detail
    public function payment_detail(Request $request){
        
        try {
            $p_id_payment  = $request->get('p_id_payment'); 


            $query = "  
                select  tpd.id_payment_detail, tpd.id_trans, tpd.type_trans, 
                        case
                        when tpd.type_trans = 1 then
                          case
                            when date(ti.start_ipl) = date(ti.end_ipl) then
                              concat('Biaya IPL ', date_format(ti.start_ipl,'%b-%y'))
                            else
                              concat('Biaya IPL ', date_format(ti.start_ipl,'%b-%y'), ' s/d ', date_format(ti.end_ipl,'%b-%y'), ' (',date_format(end_ipl,'%Y%m') - date_format(start_ipl,'%Y%m') + 1,' bulan)')
                            end 
                        when tpd.type_trans = 2 then
                          'Transaksi Minimarket'
                        when tpd.type_trans = 3 then
                          ct.subject
                        when tpd.type_trans = 4 then
                          'Transaksi Apotek'
                        end description,
                        tpd.total
                from    transaksi_payment_detail tpd
                        left join transaksi_ipl ti
                          on tpd.id_trans = ti.id_trans
                             and tpd.type_trans = 1
                        left join transaksi_retail tr
                          on tpd.id_trans = tr.id_trans
                             and tpd.type_trans = 3
                        left join transaksi_retail_detail rtd
                          on rtd.is_active = 'Y'
                             and tr.id_trans = rtd.id_trans
                        left join cc_ticket ct
                          on rtd.item_id = ct.ticket_id
                where   tpd.id_payment = ".$p_id_payment;


            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }



    // payment_view_detail
    public function payment_view_detail(Request $request){
        
        try {
            $p_id_payment_detail  = $request->get('p_id_payment_detail'); 


            $query = "  
                select  tpd.type_trans, 
                        case
                        when tpd.type_trans in (2,4) then
                          mi.image_file
                        end photo,
                        case
                        when tpd.type_trans = 1 then
                          concat('IPL Periode ',date_format(tid.periode_pembayaran,'%b-%Y'))
                        when tpd.type_trans in (2,4) then
                          mi.item_name
                        when tpd.type_trans = 3 then
                          ct.subject
                        end description,
                        ifnull(trd.item_qty,1) as qty,        
                        case
                        when tpd.type_trans = 1 then
                          tid.nominal
                        else
                          trd.item_price
                        end price
                from    transaksi_payment_detail tpd
                        left join transaksi_ipl_detail tid
                          on tpd.id_trans = tid.id_trans
                             and tid.is_active = 'Y'
                             and tpd.type_trans = 1
                        left join transaksi_retail_detail trd
                          on tpd.id_trans = trd.id_trans
                             and trd.is_active = 'Y'
                             and tpd.type_trans in (2,3)
                        left join master_item mi
                          on trd.item_id = mi.item_id
                        left join cc_ticket ct
                          on trd.item_id = ct.ticket_id
                where   tpd.is_active = 'Y'
                        and tpd.id_payment_detail = ".$p_id_payment_detail;


            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


/*====================== INBOX ===============================*/

    // inbox_list
    public function inbox_list(Request $request){
        
        try {
            $p_user_id           = !empty($request->get('p_user_id')) ? $request->get('p_user_id') : 0; 
            $p_last_id           = !empty($request->get('p_last_id')) ? $request->get('p_last_id') : '99999999999'; 


            if ($p_last_id == '0')
                $p_last_id = '9999999999';


            $query = "  
                select  i.inbox_id, i.inbox_date, i.msg_subject, i.msg_body, i.msg_type, i.source_id, i.is_read,
                        concat(date_format(i.inbox_date,'%y%m%d'),lpad(i.inbox_id,8,'0')) as concat_data
                from    inbox i
                where   i.is_active = 'Y'
                        and i.user_id = ".$p_user_id."
                        and concat(date_format(i.inbox_date,'%y%m%d'),lpad(i.inbox_id,8,'0')) < '".$p_last_id."'
                order by concat(date_format(i.inbox_date,'%y%m%d'),lpad(i.inbox_id,8,'0')) desc
                limit 30";


            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


    // inbox_del
    public function inbox_del(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'    => 'required',
            'p_user_id'         => 'required',
            'p_inbox_id'        => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token     = $request->get('p_access_token');
            $p_user_id          = $request->get('p_user_id');
            $p_inbox_id         = $request->get('p_inbox_id');

            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL inbox_del_pc(@o_status, @o_message, :p_access_token, :p_user_id, :p_inbox_id)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_inbox_id', $p_inbox_id, PDO::PARAM_INT);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
        

            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message")->fetch(PDO::FETCH_ASSOC);   
        
            $o_status = $row['o_status'];
            $o_message = $row['o_message'];


            \DB::commit();
            return response()->json([[
                'o_status'         => $o_status,
                'o_message'        => $o_message,
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }  


    // inbox_open
    public function inbox_open(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_user_id'         => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_user_id          = $request->get('p_user_id');

            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL inbox_open_pc(:p_user_id)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
    

            \DB::commit();
            return response()->json([[
                'o_status'         => 1,
                'o_message'        => "",
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }  


    // inbox_read
    public function inbox_read(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_inbox_id'         => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_inbox_id  = $request->get('p_inbox_id');

            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL inbox_read_pc(:p_inbox_id)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_inbox_id', $p_inbox_id, PDO::PARAM_INT);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
    

            \DB::commit();
            return response()->json([[
                'o_status'         => 1,
                'o_message'        => "",
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }  

/*====================== N  E W S =============================*/

    // news_list
    public function news_list(Request $request){
        
        try {
            $p_news_type   = $request->get('p_news_type'); 
            $p_is_banner   = $request->get('p_is_banner'); 
            $p_keyword     = $request->get('p_keyword'); 
            $p_last_id     = !empty($request->get('p_last_id')) ? $request->get('p_last_id') : '99999999999'; 


            if ($p_last_id == '0')
                $p_last_id = '9999999999';

            $keyword_str = "";
            if (strlen($p_keyword) > 0) {
                $keyword_str = " and subject like '%".$p_keyword."%'";
            }


            $banner_str = '';
            if ($p_is_banner == 'Y')
                $banner_str = ' and is_banner = "Y"
                                and and date(now()) between ifnull(start_date,date(now())) and ifnull(end_date,date(now()))';


            $query = "  
                select  news_id, subject, description, file_name, start_date, end_date,
                        lpad(news_id, 8, '0') as concat_data
                from    news
                where   is_active = 'Y'
                        and lpad(news_id,8,'0') < '".$p_last_id."'
                        and news_type = '".$p_news_type."'".
                        $keyword_str.
                        $banner_str."
                order by lpad(news_id,8,'0') desc
                limit 30";



            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


    // news_data
    public function news_data(Request $request){
        
        $validation = Validator::make($request->all(),[ 
            'p_news_id'   => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_news_id  = $request->get('p_news_id');


            $query = "  
                select  n.subject, n.description, n.creation_date as news_date, n.file_name, n.start_date, n.end_date, n.is_banner,
                        n.created_by, uld1.user_name as created_name, n.creation_date,
                        n.last_updated_by, uld2.user_name as last_updated_name, n.last_update_date
                from    news n
                        left join user_login_data uld1
                          on n.created_by = uld1.user_id
                        left join user_login_data uld2
                          on n.last_updated_by = uld2.user_id          
                where   n.news_id = ".$p_news_id;



            $result = \DB::select(\DB::raw($query));

            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }

    // news_save
    public function news_save(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'    => 'required',
            'p_user_id'         => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token       = $request->get('p_access_token');
            $p_user_id            = $request->get('p_user_id');
            $p_news_id            = !empty($request->get('p_news_id')) ? $request->get('p_news_id') : 0;
            $p_news_type          = $request->get('p_news_type');
            $p_subject            = $request->get('p_subject');
            $p_description        = $request->get('p_description');
            $p_start_date         = $request->get('p_start_date');
            $p_end_date           = $request->get('p_end_date');
            $p_is_banner          = $request->get('p_is_banner');
            $p_news_photo         = $request->get('p_news_photo');


            $imageName = "";
            if (strlen($p_news_photo) > 0) {

                try {
                    $base64                 = base64_decode($p_news_photo);
                    $imageName              = md5($p_subject.'_'.$this->now->format('dmY_His')).'.jpg';
                    $originalPath           = '../public/sys_contents/img_news/';

            
                    // insert atau update
                    if (strlen($p_news_photo) > 0)
                        file_put_contents($originalPath.$imageName, $base64);                    

                } catch (Exception $e) {
                }
            }



            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL news_save_pc(@o_status, @o_message, @o_news_id, :p_access_token, :p_user_id, :p_news_id, :p_news_type, :p_subject, :p_description,  :p_start_date, :p_end_date, :p_file_name, :p_is_banner)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_news_id', $p_news_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_news_type', $p_news_type, PDO::PARAM_STR);
            $stmt->bindParam(':p_subject', $p_subject, PDO::PARAM_STR);
            $stmt->bindParam(':p_description', $p_description, PDO::PARAM_STR);
            $stmt->bindParam(':p_start_date', $p_start_date, PDO::PARAM_STR);
            $stmt->bindParam(':p_end_date', $p_end_date, PDO::PARAM_STR);
            $stmt->bindParam(':p_file_name', $imageName, PDO::PARAM_STR);
            $stmt->bindParam(':p_is_banner', $p_is_banner, PDO::PARAM_STR);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
        

            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message, @o_news_id as o_news_id")->fetch(PDO::FETCH_ASSOC);   
        
            $o_status = $row['o_status'];
            $o_message = $row['o_message'];
            $o_news_id = $row['o_news_id'];


            \DB::commit();
            return response()->json([[
                'o_status'           => $o_status,
                'o_message'          => $o_message,
                'o_news_id'          => $o_news_id,
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }       


    // news_del
    public function news_del(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'    => 'required',
            'p_user_id'         => 'required',
            'p_news_id'         => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token       = $request->get('p_access_token');
            $p_user_id            = $request->get('p_user_id');
            $p_news_id            = $request->get('p_news_id');

            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL news_del_pc(@o_status, @o_message, :p_access_token, :p_user_id, :p_news_id)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_news_id', $p_news_id, PDO::PARAM_INT);

            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
        

            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message")->fetch(PDO::FETCH_ASSOC);   
        

            $o_status = $row['o_status'];
            $o_message = $row['o_message'];

            \DB::commit();
            return response()->json([[
                'o_status'         => $o_status,
                'o_message'        => $o_message,
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }   

/*======================= R E T A I L  ==========================*/

    // item_list
    public function item_list(Request $request){
        
        try {
            $p_item_category_id   = $request->get('p_item_category_id'); 
            $p_keyword            = $request->get('p_keyword'); 
            $p_last_id            = !empty($request->get('p_last_id')) ? $request->get('p_last_id') : '99999999999'; 


            if ($p_last_id == '0')
                $p_last_id = '9999999999';

            $keyword_str = "";
            if (strlen($p_keyword) > 0) {
                $keyword_str = " and mi.item_name like '%".$p_keyword."%'";
            }



            $query = "  
                select  mi.item_id, mi.item_name, mi.item_price, image_file as photo,
                        lpad(mi.item_id,14,'0') as concat_data
                from    master_item mi
                where   mi.is_active = 'Y'
                        and mi.item_category_id = ".$p_item_category_id."
                        and lpad(mi.item_id,14,'0') < '".$p_last_id."'".
                        $keyword_str."
                order by lpad(mi.item_id,14,'0') desc
                limit 30";


            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


    // cart_list
    public function cart_list(Request $request){
        
        try {
            $p_user_id   = $request->get('p_user_id'); 
            $p_cart_type   = $request->get('p_cart_type'); 


            $query = "                  
                select  c.cart_id, c.item_id, mi.item_name, mi.image_file as photo, c.qty, mi.item_price, mi.qty as stock_qty
                from    cart c
                        join master_item mi
                          on c.item_id = mi.item_id
                             and mi.is_active = 'Y'
                where   c.is_active = 'Y'
                        and c.user_id = ".$p_user_id."
                        and c.cart_type = '".$p_cart_type."'";


            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


    // cart_add
    public function cart_add(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'    => 'required',
            'p_user_id'         => 'required',
            'p_cart_type'        => 'required',
            'p_item_id'        => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token     = $request->get('p_access_token');
            $p_user_id          = $request->get('p_user_id');
            $p_cart_type        = $request->get('p_cart_type');
            $p_item_id          = $request->get('p_item_id');

            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL cart_add_pc(@o_status, @o_message, :p_access_token, :p_user_id, :p_cart_type, :p_item_id)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_cart_type', $p_cart_type, PDO::PARAM_STR);
            $stmt->bindParam(':p_item_id', $p_item_id, PDO::PARAM_INT);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
        

            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message")->fetch(PDO::FETCH_ASSOC);   
        
            $o_status = $row['o_status'];
            $o_message = $row['o_message'];


            \DB::commit();
            return response()->json([[
                'o_status'         => $o_status,
                'o_message'        => $o_message,
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }  



    // cart_qty
    public function cart_qty(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'    => 'required',
            'p_user_id'         => 'required',
            'p_cart_id'         => 'required',
            'p_qty'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token     = $request->get('p_access_token');
            $p_user_id          = $request->get('p_user_id');
            $p_cart_id          = $request->get('p_cart_id');
            $p_qty              = $request->get('p_qty');

            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL cart_qty_upd_pc(@o_status, @o_message, :p_access_token, :p_user_id, :p_cart_id, :p_qty)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_cart_id', $p_cart_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_qty', $p_qty, PDO::PARAM_INT);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
        

            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message")->fetch(PDO::FETCH_ASSOC);   
        
            $o_status = $row['o_status'];
            $o_message = $row['o_message'];


            \DB::commit();
            return response()->json([[
                'o_status'         => $o_status,
                'o_message'        => $o_message,
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }  



    // cart_del
    public function cart_del(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'    => 'required',
            'p_user_id'         => 'required',
            'p_cart_id'         => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token     = $request->get('p_access_token');
            $p_user_id          = $request->get('p_user_id');
            $p_cart_id          = $request->get('p_cart_id');

            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL cart_del_pc(@o_status, @o_message, :p_access_token, :p_user_id, :p_cart_id)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_cart_id', $p_cart_id, PDO::PARAM_INT);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
        

            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message")->fetch(PDO::FETCH_ASSOC);   
        
            $o_status = $row['o_status'];
            $o_message = $row['o_message'];


            \DB::commit();
            return response()->json([[
                'o_status'         => $o_status,
                'o_message'        => $o_message,
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }  


    // cart_checkout
    public function cart_checkout(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'    => 'required',
            'p_user_id'         => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token       = $request->get('p_access_token');
            $p_user_id            = $request->get('p_user_id');
            $p_type_trans         = $request->get('p_type_trans');
            $p_kavling_id         = $request->get('p_kavling_id');
            $p_nominal            = $request->get('p_nominal');
            $p_detail             = json_decode($request->get('p_detail'));


            $temp = "";
            // cek dulu stok ok semua atau tidak
            $flag_stok = true;
            if (is_array($p_detail) || is_object($p_detail)) {                
                foreach ($p_detail as $detail) {

                    if ($flag_stok) {
                        $query = "                  
                            select  mi.qty - ".$detail->qty." as cek_stok
                            from    master_item mi
                            where   mi.item_id = ".$detail->item_id;

       
                        $result = \DB::select(\DB::raw($query));

                        if ($result[0]->cek_stok < 0)
                            $flag_stok = false;

                    }
                }

            }

            if (!$flag_stok) {

                \DB::rollback();
                return response()->json([[
                    'o_status'  => -1,
                    'o_message' => "Maaf Stok tidak mencukupi",
                ]], 200);

            } else {

                // jika cek stok aman
                $pdo = \DB::connection()->getPdo();

                // calling stored procedure command
                $sql = "CALL transaksi_retail_save_pc(@o_status, @o_message, @o_id_trans, :p_access_token, :p_user_id, :p_type_trans, :p_kavling_id, :p_nominal)";

                // prepare for execution of the stored procedure
                $stmt = $pdo->prepare($sql);

                $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
                $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
                $stmt->bindParam(':p_type_trans', $p_type_trans, PDO::PARAM_INT);
                $stmt->bindParam(':p_kavling_id', $p_kavling_id, PDO::PARAM_INT);
                $stmt->bindParam(':p_nominal', $p_nominal, PDO::PARAM_STR);


                // execute the stored procedure
                $stmt->execute();
                $stmt->closeCursor(); 
            

                $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message, @o_id_trans as o_id_trans")->fetch(PDO::FETCH_ASSOC);   
            
                $o_status = $row['o_status'];
                $o_message = $row['o_message'];
                $o_id_trans = $row['o_id_trans'];


                if ($o_status == "1") {

                    // save lampiran
                    if (is_array($p_detail) || is_object($p_detail))
                    {
                        foreach ($p_detail as $detail) {
                            try {
                                   
                                // calling stored procedure command
                                $sql = "CALL transaksi_retail_detail_save_pc(@o_status, @o_message, :p_access_token, :p_user_id, :p_id_trans, :p_item_id, :p_item_qty, :p_item_price)";

                                // prepare for execution of the stored procedure
                                $stmt = $pdo->prepare($sql);

                                $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
                                $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
                                $stmt->bindParam(':p_id_trans', $o_id_trans, PDO::PARAM_INT);
                                $stmt->bindParam(':p_item_id', $detail->item_id, PDO::PARAM_INT);
                                $stmt->bindParam(':p_item_qty', $detail->qty, PDO::PARAM_INT);
                                $stmt->bindParam(':p_item_price', $detail->item_price, PDO::PARAM_STR);

                                // execute the stored procedure
                                $stmt->execute();
                                $stmt->closeCursor();  


                            } catch (Exception $e) {
                                $flag = false;
                            }
                        }            
                    } 
                }




                \DB::commit();
                return response()->json([[
                    'o_status'         => $o_status,
                    'o_message'        => $o_message,
                    'o_id_trans'       => $o_id_trans,
                ]], 200);

            }

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }          


/*======================================= PRODUCT ===========================================*/

    // product_list
    public function product_list(Request $request){
        
        try {
            $p_item_category_id   = $request->get('p_item_category_id'); 
            $p_keyword     = $request->get('p_keyword'); 
            $p_last_id     = !empty($request->get('p_last_id')) ? $request->get('p_last_id') : '99999999999'; 


            if ($p_last_id == '0')
                $p_last_id = '9999999999';

            $keyword_str = "";
            if (strlen($p_keyword) > 0) {
                $keyword_str = " and mi.item_name like '%".$p_keyword."%'";
            }


            $query = "  
                select  mi.item_id, mi.item_name, mi.unit_id, mu.unit_name, 
                        mi.item_hpp, mi.item_price, mi.qty, mi.item_jasa, mi.item_category_id, mi.image_file,
                        mi.item_name as concat_data
                from    master_item mi
                        join master_unit mu
                          on mi.unit_id = mu.unit_id
                where   mi.is_active = 'Y'
                        and mi.item_name > '".$p_last_id."'
                        and mi.item_category_id = ".$p_item_category_id.
                        $keyword_str."
                order by mi.item_name
                limit 30";



            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


    // product_data
    public function product_data(Request $request){
        
        $validation = Validator::make($request->all(),[ 
            'p_item_id'   => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_item_id  = $request->get('p_item_id');


            $query = "  
                select  mi.item_id, mi.item_name, mi.unit_id, mu.unit_name, 
                        mi.item_hpp, mi.item_price, mi.qty, mi.item_jasa, mi.item_category_id, mi.image_file,
                        mi.created_by, uld1.user_name as created_name, mi.creation_date,
                        mi.last_updated_by, uld2.user_name as last_updated_name, mi.last_update_date
                from    master_item mi
                        join master_unit mu
                          on mi.unit_id = mu.unit_id
                        left join user_login_data uld1
                          on mi.created_by = uld1.user_id
                        left join user_login_data uld2
                          on mi.last_updated_by = uld2.user_id          
                where   mi.item_id = ".$p_item_id;



            $result = \DB::select(\DB::raw($query));

            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }

    // product_save
    public function product_save(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'    => 'required',
            'p_user_id'         => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token       = $request->get('p_access_token');
            $p_user_id            = $request->get('p_user_id');
            $p_item_id            = !empty($request->get('p_item_id')) ? $request->get('p_item_id') : 0;
            $p_item_name          = $request->get('p_item_name');
            $p_unit_id            = $request->get('p_unit_id');
            $p_item_hpp           = $request->get('p_item_hpp');
            $p_item_price         = $request->get('p_item_price');
            $p_qty                = $request->get('p_qty');
            $p_item_jasa          = $request->get('p_item_jasa');
            $p_item_category_id   = $request->get('p_item_category_id');
            $p_photo              = $request->get('p_photo');


            $imageName = "";
            if (strlen($p_photo) > 0) {

                try {
                    $base64                 = base64_decode($p_photo);
                    $imageName              = md5($p_item_name.'_'.$this->now->format('dmY_His')).'.jpg';
                    $originalPath           = '../public/sys_contents/img_products/';

            
                    // insert atau update
                    if (strlen($p_photo) > 0)
                        file_put_contents($originalPath.$imageName, $base64);                    

                } catch (Exception $e) {
                }
            }



            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL master_item_save_pc(@o_status, @o_message, @o_item_id, :p_access_token, :p_user_id, :p_item_id, :p_item_name, :p_unit_id, :p_item_hpp,  :p_item_price, :p_qty, :p_item_jasa, :p_item_category_id, :p_image_file)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_item_id', $p_item_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_item_name', $p_item_name, PDO::PARAM_STR);
            $stmt->bindParam(':p_unit_id', $p_unit_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_item_hpp', $p_item_hpp, PDO::PARAM_STR);
            $stmt->bindParam(':p_item_price', $p_item_price, PDO::PARAM_STR);
            $stmt->bindParam(':p_qty', $p_qty, PDO::PARAM_INT);
            $stmt->bindParam(':p_item_jasa', $p_item_jasa, PDO::PARAM_STR);
            $stmt->bindParam(':p_item_category_id', $p_item_category_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_image_file', $imageName, PDO::PARAM_STR);


            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
        

            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message, @o_item_id as o_item_id")->fetch(PDO::FETCH_ASSOC);   
        
            $o_status = $row['o_status'];
            $o_message = $row['o_message'];
            $o_item_id = $row['o_item_id'];


            \DB::commit();
            return response()->json([[
                'o_status'           => $o_status,
                'o_message'          => $o_message,
                'o_item_id'          => $o_item_id,
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }       


    // product_del
    public function product_del(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'    => 'required',
            'p_user_id'         => 'required',
            'p_product_id'         => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token       = $request->get('p_access_token');
            $p_user_id            = $request->get('p_user_id');
            $p_item_id            = $request->get('p_item_id');

            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL master_item_del_pc(@o_status, @o_message, :p_access_token, :p_user_id, :p_item_id)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_item_id', $p_item_id, PDO::PARAM_INT);

            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
        

            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message")->fetch(PDO::FETCH_ASSOC);   
        

            $o_status = $row['o_status'];
            $o_message = $row['o_message'];

            \DB::commit();
            return response()->json([[
                'o_status'         => $o_status,
                'o_message'        => $o_message,
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }   


/*============================== ADMIN MINIMARKET & APOTEK ==================================*/

    // retail_undeliver_list
    public function retail_undeliver_list(Request $request){
        
        try {
            $p_type_trans   = $request->get('p_type_trans'); 
            $p_last_id      = !empty($request->get('p_last_id')) ? $request->get('p_last_id') : '99999999999'; 


            $query = "                  
                select  tr.id_trans, tr.payment_id, tr.date_paid, tr.kavling_id,
                        concat(mb.blok_name, ' ', mkav.house_no, ', ', mkom.kompleks_name) as kavling_name, tr.nominal,
                        concat(date_format(tr.date_paid,'%y%m%d'),lpad(tr.id_trans,5,'0')) as concat_data
                from    transaksi_retail tr
                        join master_kavling mkav
                          on tr.kavling_id = mkav.kavling_id
                        join master_kompleks mkom
                          on mkav.kompleks_id = mkom.kompleks_id
                        join master_blok mb
                          on mkav.blok_id = mb.blok_id
                where   tr.is_active = 'Y'
                        and tr.type_trans = ".$p_type_trans."
                        and tr.status_id = 2 
                        and tr.deliver_status = 'undelivered'
                        and concat(date_format(tr.date_paid,'%y%m%d'),lpad(tr.id_trans,5,'0')) < '".$p_last_id."'
                order by concat(date_format(tr.date_paid,'%y%m%d'),lpad(tr.id_trans,5,'0')) desc
                limit 30";


            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }



    // retail_undeliver_data
    public function retail_undeliver_data(Request $request){
        
        try {
            $p_id_trans   = $request->get('p_id_trans'); 


            $query = "                  
                select  tr.id_trans, tr.payment_id, tr.date_issued, tr.date_paid, tr.kavling_id, tr.nominal,
                        concat(mb.blok_name, ' ', mkav.house_no, ', ', mkom.kompleks_name) as kavling_name, uld.user_full_name
                from    transaksi_retail tr
                        join master_kavling mkav
                          on tr.kavling_id = mkav.kavling_id
                        join master_kompleks mkom
                          on mkav.kompleks_id = mkom.kompleks_id
                        join master_blok mb
                          on mkav.blok_id = mb.blok_id
                        join user_login_data uld
                          on tr.user_id = uld.user_id
                where   tr.is_active = 'Y'
                        and tr.id_trans = ".$p_id_trans;


            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


    // retail_undeliver_detail_list
    public function retail_undeliver_detail_list(Request $request){
        
        try {
            $p_id_trans   = $request->get('p_id_trans'); 


            $query = "                  
                select  trd.id_trans, trd.item_id, mi.item_name, trd.item_qty, trd.item_hpp, trd.item_price, trd.subtotal,
                        mi.image_file
                from    transaksi_retail_detail trd
                        join master_item mi
                          on trd.item_id = mi.item_id
                where   trd.is_active = 'Y'
                        and trd.id_trans = ".$p_id_trans."
                order by trd.id desc";


            $result = \DB::select(\DB::raw($query));
            return response()->json($result, 200);

        } catch (Exception $e) {
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }


    // retail_undeliver_status
    public function retail_undeliver_status(Request $request){
        $this->now = new \DateTime();

        \DB::beginTransaction();

         $validation = Validator::make($request->all(),[ 
            'p_access_token'    => 'required',
            'p_user_id'         => 'required',
            'p_id_trans'        => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        try {
            $p_access_token       = $request->get('p_access_token');
            $p_user_id            = $request->get('p_user_id');
            $p_id_trans           = $request->get('p_id_trans');
            $p_status             = $request->get('p_status');

            $pdo = \DB::connection()->getPdo();

            // calling stored procedure command
            $sql = "CALL undeliver_status_pc(@o_status, @o_message, :p_access_token, :p_user_id, :p_id_trans, :p_status)";

            // prepare for execution of the stored procedure
            $stmt = $pdo->prepare($sql);

            $stmt->bindParam(':p_access_token', $p_access_token, PDO::PARAM_STR);
            $stmt->bindParam(':p_user_id', $p_user_id, PDO::PARAM_INT);
            $stmt->bindParam(':p_id_trans', $p_id_trans, PDO::PARAM_INT);
            $stmt->bindParam(':p_status', $p_status, PDO::PARAM_STR);

            // execute the stored procedure
            $stmt->execute();
            $stmt->closeCursor(); 
        

            $row = $pdo->query("SELECT @o_status as o_status, @o_message as o_message")->fetch(PDO::FETCH_ASSOC);   
        

            $o_status = $row['o_status'];
            $o_message = $row['o_message'];

            \DB::commit();
            return response()->json([[
                'o_status'         => $o_status,
                'o_message'        => $o_message,
            ]], 200);

        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([[
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }   
}
