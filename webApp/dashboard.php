<?php
	require_once dirname(__FILE__) . '/include/dbconnect.php';
	require_once dirname(__FILE__) . '/include/checklogin.php';
	require_once dirname(__FILE__) . '/include/strings.php';
	
	$sUniqueKey = $_SESSION['uniquekey'];
	if (strlen($sUniqueKey) == 0) {
		$IPAddress = str_replace(".", "", $_SERVER['REMOTE_ADDR']);
		$sUniqueKey = session_id() . $IPAddress;
		$_SESSION['uniquekey'] = $sUniqueKey;
	}
	if (strlen($sUniqueKey) > 29) {
		$sUniqueKey = substr($sUniqueKey, 0, 29);
	}
	$query_string = parse_str($_SERVER["QUERY_STRING"], $param);
	if (empty($param) || !isset($param['orderby'])) {
		$sOrderBy = "";
	}
	else {
		$sOrderBy = safeQueryString($param['orderby']);	
	}
	if (empty($param) || !isset($param['sortBy'])) {
		$sSortBy =  "";
	}
	else {
		$sSortBy = safeQueryString($param['sortBy']);
	}
	if ((strtoupper($sSortBy) != "ASC") && (strtoupper($sSortBy) != "DESC")) {
		$sSortBy = "DESC";
	}

	$sOrder = "";
	$sOrder1 = "";
	$sOrder2 = "";
	$sOrder3 = "";
	$sOrder4 = "";
	$sOrder5 = "";
	
	$sUserInput = "";
	$iCurrentPage = 1;
	$iNumPerPage = 10;
	
	$sColumn1 = "DERIVED_TABLE.date_issued";
	$sColumn2 = "type_transDesc";
	$sColumn3 = "DERIVED_TABLE.nominal";
	if ($sOrderBy == "") {
		$sOrderBy = "1";
		$sSortBy = "DESC";
		$sOrder1 = "DESC";
		$sOrderCriteria = $sColumn1 . " " . $sOrder1;
	}
	else {
		if ($sSortBy != "") {
			if ((strtoupper($sSortBy) != "ASC") && (strtoupper($sSortBy) != "DESC")) {
				$sSortBy = "";
			}
		}
		if (strtolower(trim($sOrderBy)) == "1") {
			if ($sSortBy == "") {
				$sOrder1 = "DESC";
			}
			else {
				$sOrder1 = $sSortBy;
			}
			if (trim($sOrder1) == "ASC") {
				$sSortBy = "DESC";
			}
			else {
				$sSortBy = "ASC";
			}
			$sOrderCriteria = $sColumn1 . " " . $sOrder1;
		}
		else if (strtolower(trim($sOrderBy)) == "2") {
			if ($sSortBy == "") {
				$sOrder2 = "ASC";
			}
			else {
				$sOrder2 = $sSortBy;
			}
			if (trim($sOrder2) == "ASC") {
				$sSortBy = "DESC";
			}
			else {
				$sSortBy = "ASC";
			}
			$sOrderCriteria = $sColumn2 . " " . $sOrder2;
		}
		else if (strtolower(trim($sOrderBy)) == "3") {
			if ($sSortBy == "") {
				$sOrder3 = "ASC";
			}
			else {
				$sOrder3 = $sSortBy;
			}
			if (trim($sOrder3) == "ASC") {
				$sSortBy = "DESC";
			}
			else {
				$sSortBy = "ASC";
			}
			$sOrderCriteria = $sColumn3 . " " . $sOrder3;
		}
	}
	$bHaveRecord = false;
	$sSQL = " SELECT tblusertocheckedrecords.recID ".
	        " FROM tblusertocheckedrecords ".
			" WHERE tblusertocheckedrecords.userID = " . $iUserID .
			" AND tblusertocheckedrecords.uniqueKey = '" . validText($sUniqueKey) . "'".
			" AND tblusertocheckedrecords.type = 1 ";
	$result = mysqli_query($conn,$sSQL);
	if ($row = mysqli_fetch_array($result)) {
		$bHaveRecord = true;
	}
	if ($bHaveRecord){
		$sSQL = " UPDATE tblusertocheckedrecords SET " .
				"	tblusertocheckedrecords.query = '' " .
				" WHERE tblusertocheckedrecords.userID = " . $iUserID .
				" AND tblusertocheckedrecords.uniqueKey = '" . validText($sUniqueKey) . "'".
				" AND tblusertocheckedrecords.type = 1 ";
		mysqli_query($conn, $sSQL);
	}
	else {
		$sSQL = " INSERT INTO tblusertocheckedrecords " .
				" (userID, uniqueKey, query, type) " .
				" VALUES " .
				" (" . $iUserID . ", '" . validText($sUniqueKey) . "', '', 1) ";
		mysqli_query($conn, $sSQL);
	}
	
	$iLastTransaction = 0;
	$sSQL = " SELECT COUNT(transaksi_payment.id_payment) AS totalTransaction ".
			" FROM transaksi_payment ".
			" WHERE transaksi_payment.created_by = " . $iUserID .
			" AND transaksi_payment.is_active = 'Y' ";
	$result = mysqli_query($conn,$sSQL);
	if ($row = mysqli_fetch_array($result)) {
		$iLastTransaction = $row['totalTransaction'];
	}
	$id_payment = 0;
	$id_trans = 0;
	$type_trans = 0;
	$sSQL = " SELECT transaksi_payment.id_payment ".
			" FROM transaksi_payment " .
			" WHERE TIMESTAMPDIFF(HOUR, NOW(), DATE_ADD(creation_date, INTERVAL 12 HOUR)) <= 0 " .
			" AND transaksi_payment.is_active = 'Y' ";
	$result = mysqli_query($conn,$sSQL);
	if(mysqli_num_rows($result) > 0){
		while ($row = mysqli_fetch_array($result)) {
			$id_payment =  $row['id_payment'];
			$sSQL = " UPDATE transaksi_payment SET ".
					"	transaksi_payment.status_id = 4 " .
					" WHERE transaksi_payment.id_payment = " . $id_payment;
					
			$sSQL = " SELECT transaksi_payment_detail.id_trans, transaksi_payment_detail.type_trans ".
				    " FROM transaksi_payment_detail ".
				    " WHERE transaksi_payment_detail.id_payment = ". $id_payment .
 				    " AND transaksi_payment_detail.is_active = 'Y' ";
			$resultN = mysqli_query($conn,$sSQL);	
			if(mysqli_num_rows($resultN) > 0){
				while ($rowN = mysqli_fetch_array($resultN)) {
					$id_trans = $rowN['id_trans'];
					$type_trans = $rowN['type_trans'];
					if ($type_trans == 1) {
						// IPL
						$sSQL = " UPDATE transaksi_ipl SET " .
								"	transaksi_ipl.status_id = 0 ".
								" WHERE transaksi_ipl.id_trans = ". $id_trans;
					}
					else {
						// Retail
						$sSQL = " UPDATE transaksi_retail SET " .
								"	transaksi_retail.status_id = 0 ".
								" WHERE transaksi_retail.id_trans = ". $id_trans;
					}
				}
			}
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>
		Menara Santosa | Transaksi
	</title>
	<link href="/contents/css/main.css" rel="stylesheet" type="text/css" />
	<link href="/contents/css/main_custom.css" rel="stylesheet" type="text/css" />
	<link href="/contents/css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<meta name="robots" content="noindex, nofollow" />
	<link rel="shortcut icon" href="/contents/images/favicon.ico"/>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext" rel="stylesheet">
	<script type="text/javascript" src="/scripts/jquery.min.js"></script>
	<script type="text/javascript" src="/scripts/jquery-migrate.min.js"></script>
	<script type="text/javascript" src="/scripts/modernizr.js"></script>
	<script type="text/javascript" src="/scripts/general.js"></script>
	<script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="SB-Mid-client-nJRVTCGB7w5wk08b"></script>
    <!-- Note: replace with src="https://app.midtrans.com/snap/snap.js" for Production environment -->
</head>
<body onload="resetImage();" onUnLoad="destroyPopup()" >
	<div id="wrapdoc">
		<table id="page" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<!-- Header -->
					<div id="wraphdr" style="height: 134px; border-bottom: 0px;">
						<div class="wrapper">
							<h1><a href="/dashboard.php"><img src="/contents/images/menara_top_logo.png" alt="Menara Santosa" height="50" /></a></h1>
							<div id="wrapnav">
								<div id="wrapprinav">
									<div id="wrapadminlinks">
										<ul class="linkuser">
											<li>
												<a href="#" class="loginname"><?=$sUserName?></a>
												<ul>
													<li><a href="/logout.php" class="logout">Logout</a></li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
								<div id="wrapsubnav">
									<ul class="navpri2">
										<li class="active" >
											<a href="#"><span class="subbgnav"></span>&nbsp;</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2" id="td-content">
					<div id="wrapbcrmbs">
						<div class="pri">
							<ul class="breadcrumbs"></ul> 
						</div>
					</div>
					<div id="wrapcontent">
						<div class="pri">
							<div class="content-block">
								<div class="contenttitle">
									<div class="title"><h2>Transaksi Aktif</h2></div>
									<div class="menu"></div>
								</div>
								<div class="listing">
									<div id="lyrForm">
											
										<table class="BodyText" cellpadding="0" cellspacing="0" border="0" width="99%" ID="Table14">
											<tr><td align="left" colspan="2"><img src="/contents/images/spacer.gif" height="5" width="1" border="0"/></td></tr>
											<tr>
												<td align="right" valign="bottom">
													<!--Pencarian&nbsp;<input name="txtSearch" id="txtSearch" OnKeyPress="catchIt(this, event)" value="<?=$sUserInput?>" style="width: 150px" size="20" maxlength="250"/>-->
												</td>
											</tr>
										</table>
										
										<div class="tabContent contentlist" id="list-querylist">
											<div id="listing">
												<table cellpadding="0" cellspacing="0" border="0" width="100%">
													<tr valign="top" align="left" height="100%">
														<td>
															<table class="BodyText" cellpadding="0" cellspacing="3" border="0" bordercolor="blue" width="100%" height="100%">
																<tr>
																	<td valign="top" align="left" width="100%" border="0">
																		<div id="dvActive">
																			<table class="tbllist" cellpadding="0" cellspacing="0" border="0" width="100%">
																				<tr class="headerList" height="18">
																					<td width="100" style="padding-left:10px;"><input type="checkbox" id="chkAll" name="chkAll" value="1"/></td>
																					<td align="left" width="220"><a href="javascript:orderIt('1')" title="" style="text-decoration:none;" onMouseOver="viewAlt(this)"><font color="#FFFFFF">Tanggal Transaksi</font>&nbsp;<img name="img1" src="/contents/images/sort_blank.gif" alt="" border="0" /></a></td>
																					<td align="left" width="250"><a href="javascript:orderIt('2')" title="" style="text-decoration:none;" onMouseOver="viewAlt(this)"><font color="#FFFFFF">Jenis Transaksi</font>&nbsp;<img name="img2" src="/contents/images/sort_blank.gif" alt="" border="0" /></a></td>
																					<td align="left" width="250"><font color="#FFFFFF">Keterangan</font>&nbsp;</td>
																					<td align="left" width="250"><font color="#FFFFFF">Periode</font>&nbsp;</td>
																					<td align="left" width="250"><a href="javascript:orderIt('3')" title="" style="text-decoration:none;" onMouseOver="viewAlt(this)"><font color="#FFFFFF">Nominal</font>&nbsp;<img name="img3" src="/contents/images/sort_blank.gif" alt="" border="0" /></a></td>
																					<td align="left" width="50"><font color="#FFFFFF">&nbsp;</font>&nbsp;</td>
																				</tr>
																				<?php
																					$sql_query = " SELECT DERIVED_TABLE.id_trans, DERIVED_TABLE.date_issued, DERIVED_TABLE.nominal, ".
																								 " DERIVED_TABLE.type_trans, ".
																								 " CASE DERIVED_TABLE.type_trans ".
																								 "	WHEN 1 THEN 'Pembayaran IPL'".
																								 "	WHEN 2 THEN 'Retail'".
																								 "	WHEN 3 THEN 'Service'".
																								 " END AS type_transDesc, ".
																								 " CASE DERIVED_TABLE.type_trans ".
																								 "	WHEN 1 THEN DERIVED_TABLE.start_ipl ".
																								 "	ELSE ''".
																								 " END AS start_ipl, ".
																								 " CASE DERIVED_TABLE.type_trans ".
																								 "	WHEN 1 THEN DERIVED_TABLE.end_ipl ".
																								 "	ELSE ''".
																								 " END AS end_ipl ".
																								 " FROM (".
																								 " 	SELECT transaksi_ipl.id_trans, transaksi_ipl.date_issued, transaksi_ipl.nominal, ".
																								 " 	transaksi_ipl.type_trans, transaksi_ipl.start_ipl, transaksi_ipl.end_ipl ".
																								 " 	FROM transaksi_ipl ".
																								 " 	WHERE transaksi_ipl.user_id = ". $iUserID.
																								 " 	AND transaksi_ipl.is_active = 'Y'".
																								 "	AND transaksi_ipl.status_id = 0 ".
																								 " 	UNION ALL ".
																								 " 	SELECT transaksi_retail.id_trans, transaksi_retail.date_issued, transaksi_retail.nominal, ".
																								 " 	transaksi_retail.type_trans, '1/1/1900' AS start_ipl, '1/1/1900' AS end_ipl ".
																								 " 	FROM transaksi_retail ".
																								 " 	WHERE transaksi_retail.user_id = ". $iUserID.
																								 " 	AND transaksi_retail.is_active = 'Y'".
																								 "	AND transaksi_retail.status_id = 0 ".
																								 " ) AS DERIVED_TABLE ";
																					if ($sOrderCriteria != "") {
																						$sql_query = $sql_query . " ORDER BY ". $sOrderCriteria;
																					}
																					$result = mysqli_query($conn,$sql_query);
																					if(mysqli_num_rows($result) > 0){
																						while ($row = mysqli_fetch_array($result)) {
																						?>	
																							<tr>
																								<td width="100" style="padding-left:10px;"><input type="checkbox" name="changeCheckbox" id="batchCheckbox_<?=$row['id_trans']?>_<?=$row['type_trans']?>" value="<?=$row['id_trans']?>" data-type="<?=$row['type_trans']?>" onclick="toggleBatchCheckbox(this.value,<?=$row['type_trans']?>);"/></td>
																								<td align="left" width="220">
																									<?php
																										echo date('d-m-Y', strtotime($row['date_issued']));
																									?>
																								</td>
																								<td align="left" width="250"><?=$row['type_transDesc']?></td>
																								<td align="left" width="250">
																									<?php
																										if ($row['type_trans'] == 1) {
																											// IPL
																											$numBulan = 0;
																											if (($row['start_ipl'] != "") && ($row['end_ipl'] != "")) {
																												$numBulan += date('m',strtotime($row['end_ipl']))-date('m',strtotime($row['start_ipl']));
																											}
																											if ($numBulan > 0) {
																												echo ($numBulan + 1). ' bulan';
																											}
																											if ($row['start_ipl'] != "") {
																												echo ", periode ". $bulan[date('m',strtotime($row['start_ipl']))];
																											}
																											if ($row['end_ipl'] != "") {
																												echo ' - ' . $bulan[date('m',strtotime($row['end_ipl']))];
																											}
																										}
																										else {
																											// Retail
																											
																											$sDesc = "";
																											$iItemID = 0;
																											$iItemQty = 0;
																											$sItemName = "";
																											$sSQL = " SELECT transaksi_retail_detail.item_id, transaksi_retail_detail.item_qty, ".
																													" master_item.item_name ".
																													" FROM transaksi_retail_detail ".
																													" INNER JOIN master_item ON (master_item.item_id = transaksi_retail_detail.item_id) AND (master_item.is_active = 'Y') ".
																													" WHERE id_trans = ". $row['id_trans'];
																											$resultD = mysqli_query($conn,$sSQL);
																											while ($rowD = mysqli_fetch_array($resultD)) {	
																												$iItemID = $rowD['item_id'];
																												$iItemQty = $rowD['item_qty'];
																												$sItemName = $rowD['item_name'];
																												if (strlen($sDesc) == 0) {
																													$sDesc = $sItemName . " (" . $iItemQty . " pcs)";
																												}
																												else {
																													$sDesc = $sDesc . ", ". $sItemName . " (" . $iItemQty . " pcs)";
																												}
																											}
																											echo $sDesc;
																											
																										}
																									?>
																								</td>
																								<td align="left" width="250">
																									<?php
																										if ($row['type_trans'] == 1) {
																											if ($row['start_ipl'] != "") {
																												echo $bulan[date('m',strtotime($row['start_ipl']))];
																											}
																											if ($row['end_ipl'] != "") {
																												echo ' - ' . $bulan[date('m',strtotime($row['end_ipl']))];
																											}
																										}
																										else {
																											echo "-";
																										}
																									?>
																								</td>
																								<td align="left" width="250"><?=rupiah($row['nominal'])?></td>
																								<td align="left" width="50">
																									<a id="lnkDetil_<?=$row['id_trans']?>_<?=$row['type_trans']?>">Detil</a>
																								</td>
																							</tr>
																						<?php	
																						}
																					}	
																					else {
																						?><tr><td align="center" colspan="7">-Untuk saat ini, tidak ada data-</td></tr><?php
																					}
																				?>
																			</table>
																		</div>
																		<input type="hidden" id="hOrderBy" value="<?=$sOrderBy?>"/>
																		<input type="hidden" id="hSortBy" value="<?=$sSortBy?>"/>
																		<!-----------END CONTENT AREA----------->
																		<br/><br/>
																		<input type="button" id="btnBayar" class="button" value="Bayar" disabled="disabled"/>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</div>
										</div> <!-- end #wrapcontent .pri #listing -->
	        						</div>
	        						<br />
								</div>
								<?php
									if ($iLastTransaction > 0) {
										?>
											<div class="contenttitle">
												<div class="title"><h2>Transaksi Terakhir</h2></div>
												<div class="menu"></div>
											</div>
											<div class="listing">
												<div id="lyrForm">
													<table class="BodyText" cellpadding="0" cellspacing="0" border="0" width="99%" ID="Table14">
														<tr><td align="left" colspan="2"><img src="/contents/images/spacer.gif" height="5" width="1" border="0"/></td></tr>
														<tr>
															<td align="right" valign="bottom">
																<!--Pencarian&nbsp;<input name="txtSearch" id="txtSearch" OnKeyPress="catchIt(this, event)" value="<?=$sUserInput?>" style="width: 150px" size="20" maxlength="250"/>-->
															</td>
														</tr>
													</table>
													
													<div class="tabContent contentlist" id="list-querylist">
														<div id="listing">
															<table cellpadding="0" cellspacing="0" border="0" width="100%">
																<tr valign="top" align="left" height="100%">
																	<td>
																		<table class="BodyText" cellpadding="0" cellspacing="3" border="0" bordercolor="blue" width="100%" height="100%">
																			<tr>
																				<td valign="top" align="left" width="100%" border="0">
																					<!-----------BEGIN CONTENT AREA----------->												
																					<div id="dvLast">
																						<table class="tbllist" cellpadding="0" cellspacing="0" border="0" width="100%">
																							<tr class="headerList" height="18">
																								<td align="left" width="220"><a href="javascript:orderIt2('1')" title="" style="text-decoration:none;" onMouseOver="viewAlt(this)"><font color="#FFFFFF">Tanggal Transaksi</font>&nbsp;<img name="imgLast1" src="/contents/images/sort_blank.gif" alt="" border="0" /></a></td>
																								<td align="left" width="250"><a href="javascript:orderIt2('2')" title="" style="text-decoration:none;" onMouseOver="viewAlt(this)"><font color="#FFFFFF">Jenis Transaksi</font>&nbsp;<img name="imgLast2" src="/contents/images/sort_blank.gif" alt="" border="0" /></a></td>
																								<td align="left" width="250"><font color="#FFFFFF">Keterangan</font>&nbsp;</td>
																								<td align="left" width="250"><font color="#FFFFFF">Periode</font>&nbsp;</td>
																								<td align="left" width="250"><a href="javascript:orderIt2('3')" title="" style="text-decoration:none;" onMouseOver="viewAlt(this)"><font color="#FFFFFF">Nominal</font>&nbsp;<img name="imgLast3" src="/contents/images/sort_blank.gif" alt="" border="0" /></a></td>
																								<td align="left" width="250"><a href="javascript:orderIt2('4')" title="" style="text-decoration:none;" onMouseOver="viewAlt(this)"><font color="#FFFFFF">Metode Pembayaran</font>&nbsp;<img name="imgLast4" src="/contents/images/sort_blank.gif" alt="" border="0" /></a></td>
																								<td align="left" width="250"><a href="javascript:orderIt2('5')" title="" style="text-decoration:none;" onMouseOver="viewAlt(this)"><font color="#FFFFFF">Status Pembayaran</font>&nbsp;<img name="imgLast5" src="/contents/images/sort_blank.gif" alt="" border="0" /></a></td>
																							</tr>
																							<?php
																								$sql_query = " SELECT DERIVED_TABLE.id_trans, DERIVED_TABLE.date_issued, DERIVED_TABLE.nominal, ".
																											 " DERIVED_TABLE.type_trans, ".
																											 " CASE DERIVED_TABLE.type_trans ".
																											 "	WHEN 1 THEN 'Pembayaran IPL'".
																											 "	WHEN 2 THEN 'Retail'".
																											 "	WHEN 3 THEN 'Service'".
																											 " END AS type_transDesc, ".
																											 " CASE DERIVED_TABLE.type_trans ".
																											 "	WHEN 1 THEN DERIVED_TABLE.start_ipl ".
																											 "	ELSE ''".
																											 " END AS start_ipl, ".
																											 " CASE DERIVED_TABLE.type_trans ".
																											 "	WHEN 1 THEN DERIVED_TABLE.end_ipl ".
																											 "	ELSE ''".
																											 " END AS end_ipl, ".
																											 " DERIVED_TABLE.payment_type_name, ".
																											 " CASE DERIVED_TABLE.status_id ".
																											 "	WHEN 0 THEN 'Belum dibayar' ".
																											 "	WHEN 1 THEN 'Pending' " .
																											 "	WHEN 2 THEN 'Sukses' ".
																											 "	WHEN 3 THEN 'Gagal' ".
																											 "	WHEN 4 THEN 'Kadaluarsa' ".
																											 " END AS statusDesc ".
																											 " FROM (".
																											 " 	SELECT transaksi_ipl.id_trans, transaksi_ipl.date_issued, transaksi_ipl.nominal, transaksi_payment.status_id, ".
																											 " 	transaksi_ipl.type_trans, transaksi_ipl.start_ipl, transaksi_ipl.end_ipl, master_payment_mode.payment_type_name ".
																											 " 	FROM transaksi_ipl ".
																											 "  INNER JOIN transaksi_payment_detail ON (transaksi_payment_detail.id_trans = transaksi_ipl.id_trans) " .
																											 "  AND (transaksi_payment_detail.type_trans = transaksi_ipl.type_trans) AND (transaksi_payment_detail.is_active = 'Y') ".
																											 "  INNER JOIN transaksi_payment ON transaksi_payment.id_payment = transaksi_payment_detail.id_payment" .
																											 "  AND transaksi_payment.is_active ='Y' ".
																											 "  INNER JOIN master_payment_mode ON master_payment_mode.payment_type = transaksi_payment.payment_type ".
																											 "	AND master_payment_mode.is_active = 'Y'".
																											 " 	WHERE transaksi_ipl.user_id = ". $iUserID.
																											 " 	AND transaksi_ipl.is_active = 'Y'".
																											 "	AND transaksi_payment.status_id > 0 ".
																											 " 	UNION ALL ".
																											 " 	SELECT transaksi_retail.id_trans, transaksi_retail.date_issued, transaksi_retail.nominal, transaksi_payment.status_id, ".
																											 " 	transaksi_retail.type_trans, '1/1/1900' AS start_ipl, '1/1/1900' AS end_ipl, master_payment_mode.payment_type_name ".
																											 " 	FROM transaksi_retail ".
																											 "  INNER JOIN transaksi_payment_detail ON (transaksi_payment_detail.id_trans = transaksi_retail.id_trans) " .
																											 "  AND (transaksi_payment_detail.type_trans = transaksi_retail.type_trans) AND (transaksi_payment_detail.is_active = 'Y') ".
																											 "  INNER JOIN transaksi_payment ON transaksi_payment.id_payment = transaksi_payment_detail.id_payment" .
																											 "  AND transaksi_payment.is_active ='Y' ".
																											 "  INNER JOIN master_payment_mode ON master_payment_mode.payment_type = transaksi_payment.payment_type ".
																											 "	AND master_payment_mode.is_active = 'Y'".
																											 " 	WHERE transaksi_retail.user_id = ". $iUserID.
																											 " 	AND transaksi_retail.is_active = 'Y'".
																											 "	AND transaksi_payment.status_id > 0 ".
																											 " ) AS DERIVED_TABLE ";
																								if ($sOrderCriteria != "") {
																									$sql_query = $sql_query . " ORDER BY ". $sOrderCriteria;
																									
																								}
																								$result = mysqli_query($conn,$sql_query);
																								if(mysqli_num_rows($result) > 0){
																									while ($row = mysqli_fetch_array($result)) {
																									?>	
																										<tr>
																											<td align="left" width="220">
																												<?php
																													echo date('d-m-Y', strtotime($row['date_issued']));
																												?>
																											</td>
																											<td align="left" width="250"><?=$row['type_transDesc']?></td>
																											<td align="left" width="250">
																												<?php
																													if ($row['type_trans'] == 1) {
																														// IPL
																														$numBulan = 0;
																														if (($row['start_ipl'] != "") && ($row['end_ipl'] != "")) {
																															$numBulan += date('m',strtotime($row['end_ipl']))-date('m',strtotime($row['start_ipl']));
																														}
																														if ($numBulan > 0) {
																															echo ($numBulan + 1). ' bulan';
																														}
																														if ($row['start_ipl'] != "") {
																															echo ", periode ". $bulan[date('m',strtotime($row['start_ipl']))];
																														}
																														if ($row['end_ipl'] != "") {
																															echo ' - ' . $bulan[date('m',strtotime($row['end_ipl']))];
																														}
																													}
																													else {
																														// Retail
																														
																														$sDesc = "";
																														$iItemID = 0;
																														$iItemQty = 0;
																														$sItemName = "";
																														$sSQL = " SELECT transaksi_retail_detail.item_id, transaksi_retail_detail.item_qty, ".
																																" master_item.item_name ".
																																" FROM transaksi_retail_detail ".
																																" INNER JOIN master_item ON (master_item.item_id = transaksi_retail_detail.item_id) AND (master_item.is_active = 'Y') ".
																																" WHERE id_trans = ". $row['id_trans'];
																														$resultD = mysqli_query($conn,$sSQL);
																														while ($rowD = mysqli_fetch_array($resultD)) {	
																															$iItemID = $rowD['item_id'];
																															$iItemQty = $rowD['item_qty'];
																															$sItemName = $rowD['item_name'];
																															if (strlen($sDesc) == 0) {
																																$sDesc = $sItemName . " (" . $iItemQty . " pcs)";
																															}
																															else {
																																$sDesc = $sDesc . ", ". $sItemName . " (" . $iItemQty . " pcs)";
																															}
																														}
																														echo $sDesc;
																														
																													}
																												?>
																											</td>
																											<td align="left" width="250">
																												<?php
																													if ($row['type_trans'] == 1) {
																														if ($row['start_ipl'] != "") {
																															echo $bulan[date('m',strtotime($row['start_ipl']))];
																														}
																														if ($row['end_ipl'] != "") {
																															echo ' - ' . $bulan[date('m',strtotime($row['end_ipl']))];
																														}
																													}
																													else {
																														echo "-";
																													}
																												?>
																											</td>
																											<td align="left" width="250"><?=rupiah($row['nominal'])?></td>
																											<td align="left" width="250"><?=$row['payment_type_name']?></td>
																											<td align="left" width="250"><?=$row['statusDesc']?></td>
																										</tr>
																									<?php	
																									}
																								}	
																								else {
																									?><tr><td align="center" colspan="7">-Untuk saat ini, tidak ada data-</td></tr><?php
																								}
																							?>
																						</table>
																					</div>
																					<input type="hidden" id="hOrderBy2" value="<?=$sOrderBy?>"/>
																					<input type="hidden" id="hSortBy2" value="<?=$sSortBy?>"/>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</div>
													</div> <!-- end #wrapcontent .pri #listing -->
												</div>
												<br />
											</div>
										<?php
									}
								?>
							</div> <!-- end .content-block -->
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div id="wrapfooter"> 
						<p>
							&copy; <?=date("Y")?>, AlphaSoft
						</p>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<div id="dialogDetail" title="Detil Transaksi"></div>
	<div id="dialogPay" title="Metode Pembayaran" style="display:none;">
		<div class="errorBox validation" role="alert" style="display: none;"></div>
		<table>
			<tr>
				<th style="text-align:left;vertical-align:top;">Pilih Metode Pembayaran</th>
			</tr>
			<tr>
				<?php
					$sSQL = " SELECT master_payment_mode.payment_type, master_payment_mode.payment_type_name ".
							" FROM master_payment_mode ".
							" WHERE master_payment_mode.is_active = 'Y'" .
							" ORDER BY master_payment_mode.seqno ";
					$result = mysqli_query($conn,$sSQL);
					if(mysqli_num_rows($result) > 0){
						?>
							<td style="text-align:left;vertical-align:top;padding-left:5px;">
							<?php
								while ($row = mysqli_fetch_array($result)) {
									$sSelectedRadio = "";
									if ($row['payment_type'] == 1) {
										$sSelectedRadio = "checked";	
									}
									?>
										<input type="radio" id="rdMethod_<?=$row['payment_type']?>" name="rdMethod" value="<?=$row['payment_type']?>"  <?=$sSelectedRadio?>/><?=$row['payment_type_name']?><br/>
									<?php
								}
							?>
							</td>
						<?php
					}
				?>
			</tr>
		</table>
	</div>
	<script type="text/javascript" src="/scripts/dashboard.js"></script>
	<script type="text/javascript" src="/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript">
		function viewAlt(pObj) {
			<?php
				if ($sOrder1 == "ASC") {
					?>pObj.title = "Sort Descending";<?php
				}
				else {
					?>pObj.title = "Sort Ascending";<?php
				}
				if ($sOrder2 == "ASC") {
					?>pObj.title = "Sort Descending";<?php
				}
				else {
					?>pObj.title = "Sort Ascending";<?php
				}
				if ($sOrder3 == "ASC") {
					?>pObj.title = "Sort Descending";<?php
				}
				else {
					?>pObj.title = "Sort Ascending";<?php
				}
				if ($sOrder4 == "ASC") {
					?>pObj.title = "Sort Descending";<?php
				}
				else {
					?>pObj.title = "Sort Ascending";<?php
				}
				if ($sOrder5 == "ASC") {
					?>pObj.title = "Sort Descending";<?php
				}
				else {
					?>pObj.title = "Sort Ascending";<?php
				}
			?>
		}

		function resetImage() {
			//-- Published Image
			document.images["img1"].src = "/contents/images/sort_blank.gif";
			document.images["img2"].src = "/contents/images/sort_blank.gif";
			document.images["img3"].src = "/contents/images/sort_blank.gif";
			<?php
				if ($iLastTransaction > 0) {
					?>
						document.images["imgLast1"].src = "/contents/images/sort_blank.gif";
						document.images["imgLast2"].src = "/contents/images/sort_blank.gif";
						document.images["imgLast3"].src = "/contents/images/sort_blank.gif";
						document.images["imgLast4"].src = "/contents/images/sort_blank.gif";
						document.images["imgLast5"].src = "/contents/images/sort_blank.gif";
					<?php
				}
			?>
			
			<?php
				if ($sOrderBy == "1") {
					if ($sOrder1 == "ASC") {
					?>
						document.images["img1"].src = "/contents/images/sort_up.gif";
						document.images["img1"].alt = "Sort Descending";
						
						<?php
							if ($iLastTransaction > 0) {
								?>
									document.images["imgLast1"].src = "/contents/images/sort_up.gif";
									document.images["imgLast1"].alt = "Sort Descending";
								<?php
							}
						?>
						
						
					<?php
					} 
					else {
					?>
						document.images["img1"].src = "/contents/images/sort_down.gif";
						document.images["img1"].alt = "Sort Ascending";
						
						<?php
							if ($iLastTransaction > 0) {
								?>
									document.images["imgLast1"].src = "/contents/images/sort_down.gif";
									document.images["imgLast1"].alt = "Sort Ascending";
								<?php
							}
						?>
					<?php
					}
				}
				
				if (trim($sOrderBy) == "2") {
					if ($sOrder2 == "" || $sOrder2 == "ASC") {
					?>
						document.images["img2"].src = "/contents/images/sort_up.gif";
						document.images["img2"].alt = "Sort Descending";
						
						<?php	
							if ($iLastTransaction > 0) {
								?>
									document.images["imgLast2"].src = "/contents/images/sort_up.gif";
									document.images["imgLast2"].alt = "Sort Descending";
								<?php
							}
						?>
					<?php
					} 
					else {
					?>
						document.images["img2"].src = "/contents/images/sort_down.gif";
						document.images["img2"].alt = "Sort Ascending";
						
						<?php
							if ($iLastTransaction > 0) {
								?>
									document.images["imgLast2"].src = "/contents/images/sort_down.gif";
									document.images["imgLast2"].alt = "Sort Ascending";
								<?php
							}
						?>
					<?php
					}
				}
				if (trim($sOrderBy) == "3") {
					if ($sOrder3 == "" || $sOrder3 == "ASC") {
					?>
						document.images["img3"].src = "/contents/images/sort_up.gif";
						document.images["img3"].alt = "Sort Descending";
						
						<?php
							if ($iLastTransaction > 0) {
								?>
									document.images["imgLast3"].src = "/contents/images/sort_up.gif";
									document.images["imgLast3"].alt = "Sort Descending";
								<?php
							}
						?>
					<?php
					} 
					else {
					?>
						document.images["img3"].src = "/contents/images/sort_down.gif";
						document.images["img3"].alt = "Sort Ascending";
						
						<?php
							if ($iLastTransaction > 0) {
								?>
									document.images["imgLast3"].src = "/contents/images/sort_down.gif";
									document.images["imgLast3"].alt = "Sort Ascending";
								<?php
							}
						?>
					<?php
					}
				}
				
				if (trim($sOrderBy) == "4") {
					if ($sOrder4 == "" || $sOrder4 == "ASC") {
						if ($iLastTransaction > 0) {
							?>
								document.images["imgLast4"].src = "/contents/images/sort_up.gif";
								document.images["imgLast4"].alt = "Sort Descending";
							<?php
						}
					} 
					else {
						if ($iLastTransaction > 0) {
							?>
								document.images["imgLast4"].src = "/contents/images/sort_down.gif";
								document.images["imgLast4"].alt = "Sort Ascending";
							<?php
						}
					}
				}
				
				if (trim($sOrderBy) == "5") {
					if ($sOrder5 == "" || $sOrder5 == "ASC") {
						if ($iLastTransaction > 0) {
							?>
								document.images["imgLast5"].src = "/contents/images/sort_up.gif";
								document.images["imgLast5"].alt = "Sort Descending";
							<?php
						}
					} 
					else {
						if ($iLastTransaction > 0) {
							?>
								document.images["imgLast5"].src = "/contents/images/sort_down.gif";
								document.images["imgLast5"].alt = "Sort Ascending";
							<?php
						}
					}
				}
			?>
		}
		
		function catchIt(obj, e) {
			var isNN = (navigator.appName.indexOf("Netscape")!=-1);
			var keyCode = (isNN) ? e.which : e.keyCode;
			if (keyCode == 13) {
				refreshURL();
			}
			return true;
		}

		function refreshURL() {
			var obj1  = document.getElementById("txtSearch");
			self.location = "dashboard.php?numPerPage=<?=$iNumPerPage?>" +
							"&txtsearch=" + escapeFormInput(htmlEncode(iVal1));
		}

		function orderIt(pOrderBy) {
			if ($("#hSortBy").val() == "ASC") {
				$("#hSortBy").val("DESC");
			}
			else {
				$("#hSortBy").val("ASC");
			}
			//redrawList(pOrderBy, $("#hSortBy").val(), $.trim($("#search").val()), $("#sel_page").val(), 1);
			redrawList(1, pOrderBy, $("#hSortBy").val(), "", 10, 1);
		}
		
		function orderIt2(pOrderBy) {
			if ($("#hSortBy2").val() == "ASC") {
				$("#hSortBy2").val("DESC");
			}
			else {
				$("#hSortBy2").val("ASC");
			}
			//redrawList(pOrderBy, $("#hSortBy").val(), $.trim($("#search").val()), $("#sel_page").val(), 1);
			redrawList(2, pOrderBy, $("#hSortBy2").val(), "", 10, 1);
		}
		
		function filterURL() {
			self.location = "dashboard.php?orderby=<?=$sOrderBy?>&sortBy=<?=$sSortBy?>";
		}
		
		function redrawList(pType, pOrderBy, pSortBy, pSearch, pNumPerPage, pPage) {
			var sURL = "";
			if (parseInt(pType) == 1) {
				sURL = "ajax_reload_active_transaction.php";
			}
			else {
				sURL = "ajax_reload_last_transaction.php";
			}
			$.ajax({
				url : sURL,
				data : "orderby="+escape(pOrderBy)+"&sortby=" + pSortBy + "&search=" + escape(pSearch) + "&numPerPage=" + pNumPerPage + "&page=" + pPage,
				cache : false,
				dataType : "html",
				type : "POST",
				success	: function (data, textStatus, jqXHR) {
					var aArrData = data.split("~[*splitter*]~");
					if (parseInt(pType) == 1) {
						$("#dvActive").html(aArrData[0]);
						$("#hOrderBy").val(aArrData[1]);
					}
					else {
						$("#dvLast").html(aArrData[0]);
						$("#hOrderBy2").val(aArrData[1]);
					}
					if ($.trim(aArrData[2]) == "ASC") {
						if (parseInt(pType) == 1) {
							document.images["img" + aArrData[1]].src = "/contents/images/sort_up.gif";
							document.images["img" + aArrData[1]].alt = "Sort Descending";
						}
						else {
							document.images["imgLast" + aArrData[1]].src = "/contents/images/sort_up.gif";
							document.images["imgLast" + aArrData[1]].alt = "Sort Descending";
						}
					}
					else {
						if (parseInt(pType) == 1) {
							document.images["img" + aArrData[1]].src = "/contents/images/sort_down.gif";
							document.images["img" + aArrData[1]].alt = "Sort Ascending";
						}
						else {
							document.images["imgLast" + aArrData[1]].src = "/contents/images/sort_down.gif";
							document.images["imgLast" + aArrData[1]].alt = "Sort Ascending";
						}
					}
					i1 = $.trim(aArrData[3]);
					if (parseInt(pType) == 1) {
						$('[id*="lnkDetil_"]').click(function() {
							var $id = $(this).attr("id");
							var arrID = $id.replace("lnkDetil_", "").split("_");
							var iTransID = arrID[0];
							var iTransType = arrID[1];
							var sURL = "get_transaction_detail.php";
							$.ajax({
								type: "POST",
								url: sURL,
								data: "id=" + iTransID + "&t=" + iTransType,
								cache: false,
								dataType: 'html',
								success: function (data, textStatus, jqXHR) {
									$("#dialogDetail").html($.trim(data));	
									$("#dialogDetail").dialog("open");	
								},
								error: function (data, textStatus, jqXHR) {
									alert(textStatus);
								}
							});
							
						});
						checkBatchCheckbox();
					}
					/*
					$("a").click(
						function(event) {
							if ($(this).attr("id") != null) {
								if ($(this).attr("id").substring(0, 10) == "lnkpaging_") {
									arrID = $(this).attr("id").split("_");
									if (arrID.length == 2) {
										if (arrID[1] != "" && !isNaN(arrID[1])) {
											event.preventDefault();
											redrawList($("#hOrderBy").val(), $("#hSortBy").val(), $.trim($("#search").val()), $("#sel_page").val(), arrID[1]);
										}
									}
								}
							}
						}
					);
					$("#sel_page").change(function() {
						redrawList($("#hOrderBy").val(), $("#hSortBy").val(), $.trim($("#search").val()), $("#sel_page").val(), 1);
					})
					*/
				},
				error: function (jqXHR, textStatus, errorThrown) {
					var sErrorText = "";
					if (textStatus == "timeout") {
						sErrorText = "We're sorry, your request has been timeout. Please try again later.";
					}
					else if (textStatus == "abort") {
						sErrorText = "We're sorry, your request has been aborted. Please try again later.";
					}
					else {
						sErrorText = "We're sorry, an error occured when trying to open the popup window";
						if (errorThrown != "") sErrorText += " (" + errorThrown + ")";
					}
					alert(sErrorText);
				}
			});
		}
	</script>
</body>
</html>
<?php
	$conn->close();
?>