<?php
	require_once dirname(__FILE__) . '/include/dbconnect.php';
	require_once dirname(__FILE__) . '/midtrans/Midtrans.php';
	require_once dirname(__FILE__) . '/include/checklogin.php';
	require_once dirname(__FILE__) . '/include/strings.php';
	require_once dirname(__FILE__) . '/include/midtrans_settings.php';

	$iSelectedMethod = $_REQUEST['m'];
	
	if ($iSelectedMethod > 0) {
		$sUniqueKey = $_SESSION['uniquekey'];
		if (strlen($sUniqueKey) == 0) {
			$IPAddress = str_replace(".", "", $_SERVER['REMOTE_ADDR']);
			$sUniqueKey = session_id() . $IPAddress;
			$_SESSION['uniquekey'] = $sUniqueKey;
		}
		if (strlen($sUniqueKey) > 29) {
			$sUniqueKey = substr($sUniqueKey, 0, 29);
		}
		$iRecID = 0;
		$sSQL = " SELECT tblusertocheckedrecords.recID, tblusertocheckedrecords.query ".
				" FROM tblusertocheckedrecords ".
				" WHERE tblusertocheckedrecords.userID = ". $iUserID.
				" AND tblusertocheckedrecords.uniqueKey = '" . validText($sUniqueKey) . "'".
				" AND tblusertocheckedrecords.type = 1 ";
		$result = mysqli_query($conn,$sSQL);
		if ($row = mysqli_fetch_array($result)) {
			$iRecID = $row['recID'];
		}
		
		$iTransID = 0;
		$iTransType = 0;
		$iTotalAmount = 0;
		if ($iRecID > 0) {
			$t = microtime(true);
			$micro = sprintf("%06d",($t - floor($t)) * 1000000);
			$d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
			$miliseconds = substr($d->format("u"), 0, 2);
			$sOrderID = $d->format("dmYHis") . $miliseconds;
				
			$sSQL = " UPDATE tblusertocheckedrecords SET ".
					"	tblusertocheckedrecords.payment_type = " . $iSelectedMethod .
					" WHERE tblusertocheckedrecords.recID = " . $iRecID;
			mysqli_query($conn, $sSQL);
			
			$bHaveDetailRecord = false;
			$sSQL = " SELECT tblusertocheckedrecordsdetail.recordID, tblusertocheckedrecordsdetail.trans_type ".
					" FROM tblusertocheckedrecordsdetail ".
					" WHERE tblusertocheckedrecordsdetail.recID = " . $iRecID;
			$result = mysqli_query($conn,$sSQL) or die("Query error : " . mysqli_error($result));
			if(mysqli_num_rows($result) > 0){
				$bHaveDetailRecord = true;
				while  ($row = mysqli_fetch_array($result)) {
					$iTransID = $row['recordID'];
					$iTransType = $row['trans_type'];
					$sSQLN = "";
					$sSQLQ = "";
					$iNominal = 0;
					
					if ($iTransType == 1) {
						// IPL
						$sSQLQ = " UPDATE transaksi_ipl SET ".
								 " 	transaksi_ipl.payment_id = '" . validText($sOrderID) . "', ".
								 "	transaksi_ipl.last_updated_by = " . $iUserID . ", " .
								 "	transaksi_ipl.last_update_date = CURRENT_TIMESTAMP() " .
								 " WHERE transaksi_ipl.id_trans = ". $iTransID;
						mysqli_query($conn, $sSQLQ);		 
						
						$sSQLN = " SELECT transaksi_ipl.nominal ".
								 " FROM transaksi_ipl ".
								 " WHERE transaksi_ipl.id_trans = ". $iTransID;
					}
					else {
						// Retail
						$sSQLQ = " UPDATE transaksi_retail SET ".
								 "  transaksi_retail.payment_id = '" . validText($sOrderID) . "', " .
								 "	transaksi_retail.last_updated_by = " . $iUserID . ", " .
								 "	transaksi_retail.last_update_date = CURRENT_TIMESTAMP() " .
								 " WHERE transaksi_retail.id_trans = ". $iTransID;
						mysqli_query($conn, $sSQLQ);		 
						
						$sSQLN = " SELECT transaksi_retail.nominal ".
								 " FROM transaksi_retail ".
								 " WHERE transaksi_retail.id_trans = ". $iTransID;
					}
					if (strlen($sSQLN) > 0) {
						$resultN = mysqli_query($conn,$sSQLN) or die("Query error : " . mysqli_error($resultN));
						if ($rowN = mysqli_fetch_array($resultN)) {
							$iNominal = $rowN['nominal'];
						}
					}
					$iTotalAmount = $iTotalAmount + $iNominal;
				}
			}
			else {
				$bHaveDetailRecord = false;
			}
			if ($bHaveDetailRecord) {
				$iTransFee = 0;
				$iTransFeePercent = 0;
				$iPaymentType = 0;
				$sSQL = " SELECT tblusertocheckedrecords.payment_type, sys_config_transaksi.biaya_transaksi, ". 
						" sys_config_transaksi.biaya_transaksi_percent ".
						" FROM tblusertocheckedrecords ".
						" INNER JOIN sys_config_transaksi ON sys_config_transaksi.payment_type = tblusertocheckedrecords.payment_type ".
						" AND sys_config_transaksi.is_active = 'Y'".
						" WHERE tblusertocheckedrecords.recID = ". $iRecID;
				$result = mysqli_query($conn,$sSQL);
				if ($row = mysqli_fetch_array($result)) {
					$iTransFee = $row['biaya_transaksi'];
					$iTransFeePercent = $row['biaya_transaksi_percent'];
					$iPaymentType = $row['payment_type'];
				}
				if ($iTransFee > 0) {
					$iTotalAmount = $iTotalAmount + $iTransFee;
				}
				if ($iTransFeePercent > 0) {
					$iTotalAmount = $iTotalAmount + (($iTransFeePercent/100) * $iTotalAmount);
				}
				$sUserFullName = "";
				$sUserFirstName = "";
				$sUserLastName = "";
				$sUserEmailAddress = "";
				$sUserPhone = "";
				$sUserPhone1 = "";
				$sUserPhone2 = "";
				$sSQL = " SELECT user_login_data.user_full_name, user_login_data.user_email_address, ".
						" user_login_data.user_phone_1, user_login_data.user_phone_2 ".
						" FROM user_login_data ".
						" WHERE user_login_data.user_id = ".$iUserID .
						" AND user_login_data.is_active = 'Y'";
				$result = mysqli_query($conn,$sSQL);
				if  ($row = mysqli_fetch_array($result)) {
					$sUserFullName = $row['user_full_name'];
					$sUserEmailAddress = $row['user_email_address'];
					$sUserPhone1 = $row['user_phone_1'];
					$sUserPhone2 = $row['user_phone_2'];
				}		
				if (strlen($sUserPhone1) > 0) {
					$sUserPhone = $sUserPhone1;
				}
				else {
					if (strlen($sUserPhone2) > 0) {	
						$sUserPhone = $sUserPhone2;
					}
				}
				if (strpos($sUserFullName, " ") === false) {
					$sUserFirstName = $sUserFullName;
					$sUserLastName = "";
				}
				else {
					$arrUserFullName = explode(" ", $sUserFullName);
					$sUserFirstName = $arrUserFullName[0];
					$sUserLastName = str_replace($sUserFirstName . " ", "", $sUserFullName);
				}
				
				if ($iPaymentType == 1) {
					// BRI Virtual Account
					$params = array(
						'transaction_details' => array(
							'order_id' => $sOrderID,
							'gross_amount' => $iTotalAmount,
						),
						'customer_details' => array(
							'first_name' => validText($sUserFirstName),
							'last_name' => validText($sUserLastName),
							'email' => validText($sUserEmailAddress),
							'phone' => validText($sUserPhone),
						),
						'enabled_payments' => array('bri_va')
					);
				}
				else if ($iPaymentType == 2) {
					// Gopay
					$params = array(
						'transaction_details' => array(
							'order_id' => $sOrderID,
							'gross_amount' => $iTotalAmount,
						),
						'customer_details' => array(
							'first_name' => validText($sUserFirstName),
							'last_name' => validText($sUserLastName),
							'email' => validText($sUserEmailAddress),
							'phone' => validText($sUserPhone),
						),
						'enabled_payments' => array('gopay')
					);
				}
				else if ($iPaymentType == 3) {
					// ShopeePay
					$params = array(
						'transaction_details' => array(
							'order_id' => $sOrderID,
							'gross_amount' => $iTotalAmount,
						),
						'customer_details' => array(
							'first_name' => validText($sUserFirstName),
							'last_name' => validText($sUserLastName),
							'email' => validText($sUserEmailAddress),
							'phone' => validText($sUserPhone),
						),
						'enabled_payments' => array('shopeepay')
					);
				}
				else if ($iPaymentType == 4) {
					// Indomaret
					$params = array(
						'transaction_details' => array(
							'order_id' => $sOrderID,
							'gross_amount' => $iTotalAmount,
						),
						'customer_details' => array(
							'first_name' => validText($sUserFirstName),
							'last_name' => validText($sUserLastName),
							'email' => validText($sUserEmailAddress),
							'phone' => validText($sUserPhone),
						),
						'enabled_payments' => array('indomaret')
					);
				}
				else if ($iPaymentType == 5) {
					// Alfa Group
					$params = array(
						'transaction_details' => array(
							'order_id' => $sOrderID,
							'gross_amount' => $iTotalAmount,
						),
						'customer_details' => array(
							'first_name' => validText($sUserFirstName),
							'last_name' => validText($sUserLastName),
							'email' => validText($sUserEmailAddress),
							'phone' => validText($sUserPhone),
						),
						'enabled_payments' => array('alfamart')
					);
				}
				else if ($iPaymentType == 6) {
					// BNI Virtual Account
					$params = array(
						'transaction_details' => array(
							'order_id' => $sOrderID,
							'gross_amount' => $iTotalAmount,
						),
						'customer_details' => array(
							'first_name' => validText($sUserFirstName),
							'last_name' => validText($sUserLastName),
							'email' => validText($sUserEmailAddress),
							'phone' => validText($sUserPhone),
						),
						'enabled_payments' => array('bni_va')
					);
				}
				else if ($iPaymentType == 7) {
					// Mandiri Virtual Account
					$params = array(
						'transaction_details' => array(
							'order_id' => $sOrderID,
							'gross_amount' => $iTotalAmount,
						),
						'customer_details' => array(
							'first_name' => validText($sUserFirstName),
							'last_name' => validText($sUserLastName),
							'email' => validText($sUserEmailAddress),
							'phone' => validText($sUserPhone),
						),
						'enabled_payments' => array('echannel')
					);
				}
				else if ($iPaymentType == 8) {
					// Permata Virtual Account
					$params = array(
						'transaction_details' => array(
							'order_id' => $sOrderID,
							'gross_amount' => $iTotalAmount,
						),
						'customer_details' => array(
							'first_name' => validText($sUserFirstName),
							'last_name' => validText($sUserLastName),
							'email' => validText($sUserEmailAddress),
							'phone' => validText($sUserPhone),
						),
						'enabled_payments' => array('permata_va')
					);
				}
				$snapToken = \Midtrans\Snap::getSnapToken($params);
				echo $snapToken;
			}
			else {
				echo "failed";
			}
		}
	}
	else {
		echo "failed";
	}
?>