<?php
	require_once dirname(__FILE__) . '/include/dbconnect.php';
	require_once dirname(__FILE__) . '/include/checklogin.php';
	require_once dirname(__FILE__) . '/include/strings.php';
	
	if ($iUserID == 0) {
		echo "";
	}
	$sUniqueKey = $_SESSION['uniquekey'];
	if (strlen($sUniqueKey) == 0) {
		$IPAddress = str_replace(".", "", $_SERVER['REMOTE_ADDR']);
		$sUniqueKey = session_id() . $IPAddress;
		$_SESSION['uniquekey'] = $sUniqueKey;
	}
	if (strlen($sUniqueKey) > 29) {
		$sUniqueKey = substr($sUniqueKey, 0, 29);
	}
	$iRecID = 0;
	$sQuery = "";
	$sSQL = " SELECT tblusertocheckedrecords.recID, tblusertocheckedrecords.query ".
	  	    " FROM tblusertocheckedrecords ".
			" WHERE tblusertocheckedrecords.userID = ". $iUserID.
			" AND tblusertocheckedrecords.uniqueKey = '" . validText($sUniqueKey) . "'".
			" AND tblusertocheckedrecords.type = 1 ";
	$result = mysqli_query($conn,$sSQL);
	if ($row = mysqli_fetch_array($result)) {
		$iRecID = $row['recID'];
		$sQuery = $row['query'];
	}
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		switch ($_REQUEST['action']) {
			case "saveBatchTransIDs" :
				saveCheckedTransaction($_REQUEST['checkedTransIDcsv'],$_REQUEST['t'],$iRecID,$conn);
				break;
			case "isCheckedBatchTransID" :
				checkedTransID($_REQUEST['pageTransIDs'],$iRecID,$sQuery,$conn);
				break;
			case "addBatchTransID":
				addTransID($_REQUEST['transID'],$_REQUEST['t'],$iRecID,$conn);
				break;
			case "removeBatchTransID":
				removeTransID($_REQUEST['transID'],$_REQUEST['t'],$iRecID,$conn);
				break;
			case "clear":
				removeAll($iRecID,$conn);
				break;
		}
	}
	function checkedTransID($sPageAppIDcsv,$iRecID,$sQuery,$conn) {
		$sCheckedTransID = "";
		$sCheckedTransType = "";
		if ($sQuery == "") {
			$sSQL = " SELECT tblusertocheckedrecordsdetail.recordID, ".
					" tblusertocheckedrecordsdetail.trans_type ".
					" FROM tblusertocheckedrecordsdetail ".
					" WHERE recID = " . $iRecID;
			$result = mysqli_query($conn,$sSQL);
			while ($row = mysqli_fetch_array($result)) {
				if (strlen($sCheckedTransID) == 0) {
					$sCheckedTransID = $row['recordID'];
				}
				else {
					$sCheckedTransID = $sCheckedTransID . "," . $row['recordID'];
				}
				
				if (strlen($sCheckedTransType) == 0) {
					$sCheckedTransType = $row['trans_type'];
				}
				else {
					$sCheckedTransType = $sCheckedTransType . "," . $row['trans_type'];
				}
			}
		}
		else {
			$sSQL = $sQuery;
			$result = mysqli_query($conn,$sSQL);
			while ($row = mysqli_fetch_array($result)) {
				if (strlen($sCheckedTransID) == 0) {
					$sCheckedTransID = $row['id_trans'];
				}
				else {
					$sCheckedTransID = $sCheckedTransID . "," . $row['id_trans'];
				}
				if (strlen($sCheckedTransType) == 0) {
					$sCheckedTransType = $row['type_trans'];
				}
				else {
					$sCheckedTransType = $sCheckedTransType . "," . $row['type_trans'];
				}
			}
		}
		echo $sCheckedTransID . "~[|*splitter*|]~" . $sCheckedTransType;
	}
	
	function saveCheckedTransaction($sCheckedTransIDcsv,$sCheckedTransTypecsv,$iRecID,$conn) {
		$arrCheckedTransID = explode(",", $sCheckedTransIDcsv);
		$arrCheckedTransType = explode(",", $sCheckedTransTypecsv);
		for ($i = 0; $i < count($arrCheckedTransID); $i++) {
			if ($iRecID > 0) {
				$bHaveRecord = false;
				$sSQL = " SELECT tblusertocheckedrecordsdetail.id ".
						" FROM tblusertocheckedrecordsdetail ".
						" WHERE tblusertocheckedrecordsdetail.recID = " . $iRecID .
						" AND tblusertocheckedrecordsdetail.recordID = " . $arrCheckedTransID[$i] .
						" AND tblusertocheckedrecordsdetail.trans_type = " . $arrCheckedTransType[$i];
				$result = mysqli_query($conn,$sSQL);
				if ($row = mysqli_fetch_array($result)) {
					$bHaveRecord = true;
				}
				if (!$bHaveRecord) {
					$sSQL = " INSERT INTO tblusertocheckedrecordsdetail (recID, recordID, trans_type) VALUES (" . $iRecID . ", " . $arrCheckedTransID[$i] . ", " . $arrCheckedTransType[$i] . ") ";
					mysqli_query($conn, $sSQL);
				}
			}
		}
	}
	
	function addTransID($transID,$transType,$iRecID,$conn) {
		$sSQL = " UPDATE tblusertocheckedrecords SET " .
		 	    " 	tblusertocheckedrecords.query = '' " .
			    " WHERE tblusertocheckedrecords.recID = " . $iRecID;
		mysqli_query($conn, $sSQL);

		$bHaveRecord = false;
		$sSQL = " SELECT tblusertocheckedrecordsdetail.id ".
				" FROM tblusertocheckedrecordsdetail ".
				" WHERE tblusertocheckedrecordsdetail.recID = " . $iRecID .
				" AND tblusertocheckedrecordsdetail.recordID = " . $transID.
				" AND tblusertocheckedrecordsdetail.trans_type = ". $transType;
		$result = mysqli_query($conn,$sSQL);
		if ($row = mysqli_fetch_array($result)) {
			$bHaveRecord = true;
		}
		if (!$bHaveRecord) {
			$sSQL = " INSERT INTO tblusertocheckedrecordsdetail (recID, recordID, trans_type) VALUES (" . $iRecID . ", " . $transID . ", " . $transType . ") ";
			mysqli_query($conn, $sSQL);
		}
	}
	
	function removeTransID($transID,$transType,$iRecID,$conn) {
		$sSQL = " UPDATE tblusertocheckedrecords SET " .
				" 	tblusertocheckedrecords.query = '' " .
				" WHERE tblusertocheckedrecords.recID = " . $iRecID;
		mysqli_query($conn, $sSQL);
		
		$sSQL = " DELETE FROM tblusertocheckedrecordsdetail ". 
				" WHERE recID = " . $iRecID . 
				" AND recordID = " . $transID .
				" AND trans_type = " . $transType;
		mysqli_query($conn, $sSQL);
	}
	
	function removeAll($iRecID,$conn) {
		if ($iRecID > 0) {
			$sSQL = " DELETE FROM tblusertocheckedrecordsdetail WHERE recID = " . $iRecID;
			mysqli_query($conn, $sSQL);
			
			$sSQL = " UPDATE tblusertocheckedrecords SET " .
					" 	tblusertocheckedrecords.query = '' " .
					" WHERE tblusertocheckedrecords.recID = " . $iRecID;
			mysqli_query($conn, $sSQL);
		}
	}
?>
