<?php
	require_once dirname(__FILE__) . '/include/strings.php';
	$sErrMsg = "";
	$sErrCode = "";
	$cookie_name = "gGkHIcDyEPlF";
	if(isset($_COOKIE[$cookie_name])) {
		$sErrCode = $_COOKIE[$cookie_name];
		switch ($sErrCode) {
			case "AU":
				$sErrMsg = "Username dan password tidak valid.";
				break;
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600|PT+Sans:700" rel="stylesheet" />
    <title>Menara Santosa - Halaman Login</title>
    <meta name="robots" content="noindex, nofollow" />
    <script type="text/javascript" src="/scripts/jquery.min.js"></script>
	<script type="text/javascript" src="/scripts/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="/scripts/modernizr.js"></script>
	<link href="/contents/css/login_style.css" rel="stylesheet" type="text/css" />
	<link href="/contents/css/accessibility.css" rel="stylesheet" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body class="main-body">
	<a href="#" class="accessibility-skiptomain">Skip To Main Content</a>
    <section class="login">
        <div class="half-screen left-side">
            <div class="login-wrapper">
                <img alt="eresources logo" src="/contents/images/menara_top_logo.png" class="logo" />
				<div id="mainarea" class="login-box" role="main" tabindex="-1">
					<div class="errorBox validation" role="alert" style="display: none;"></div>
					<?php 
						if ($sErrMsg != "") {
						?>	
							 <div class="errorBox" id="lyrErrorServer" role="alert">
								<p><?php echo $sErrMsg?></p>
							 </div>
						<?php	 
						}
					?>
					<form id="frmLogOn" action="authenticate.php" method="post" class="login-form">
						<label for="txtUserLogin" class="input-label small">Username</label>
						<input type="text" id="txtUserLogin" name="txtUserLogin" maxlength="15" class="input-field full" pattern="[^\s]*" autofocus autocomplete="off" />
						<br />
						<label for="txtUserPwd" class="input-label small">Password</label>
						<input type="password" id="txtUserPwd" name="txtUserPwd" pattern="[^\s]*" class="input-field full" maxlength="15" autocomplete="off" />
						<input type="hidden" name="hCsrf" id="hCsrf" class="hCsrf" value="" readonly />
						<input type="hidden" name="fghRtyxIoQk5" id="fghRtyxIoQk5" class="fghRtyxIoQk5" value="" readonly />
						<button class="bttn pt-font full btnLogin" id="btnLogOn">Login</button>
						<div class="lyrProcessing" id="lyrProcessing" style="display:none; color: red; margin-top: 5px;" role="alert">Processing...</div>
						
					</form>
				</div>
            </div>
            <footer class="footer align-center">
                <p class="text-row thin-text">
					<table class="center">
						<tr>
							<td style="text-align:left;vertical-align:middle;">Powered by </td>
							<td style="text-align:left;vertical-align:middle;"><img alt="eresources logo" src="/contents/images/logo_alphasoft.png" class="logo" style="width:20px;" /></td>
							<td style="text-align:left;vertical-align:middle;">AlphaSoft <?php echo date("Y"); ?></td>
						</tr>
					</table>
                </p>
            </footer>
        </div>
        <div class="half-screen right-side">
        </div>
    </section>
    <div class="modal"></div>
	<script type="text/javascript" src="/scripts/jquery.validate.min.js"></script>
	<script type="text/javascript" src="/scripts/jquery.validate.unobtrusive.min.js"></script>
	<script type="text/javascript" src="/scripts/general.js"></script>
	<script type="text/javascript" src="/scripts/login.js"></script>
	<script type="text/javascript" src="/scripts/login_custom.js"></script>
</body>
</html>
<?php
	setcookie($cookie_name, "", time() - 3600);
?>