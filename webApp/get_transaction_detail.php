<?php
	require_once dirname(__FILE__) . '/include/dbconnect.php';
	require_once dirname(__FILE__) . '/include/checklogin.php';
	require_once dirname(__FILE__) . '/include/strings.php';

	$iTransID = 0;
	$iTransType = 0 ;
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (empty($_REQUEST['id']) || !isset($_REQUEST['id'])) {
			$iTransID = 0;
		}
		else {
			$iTransID = trim($_REQUEST['id']);
		}
		if (empty($_REQUEST['t']) || !isset($_REQUEST['t'])) {
			$iTransType = 0;
		}
		else {
			$iTransType = trim($_REQUEST['t']);
		}
		if (empty($_REQUEST['orderby']) || !isset($_REQUEST['orderby'])) {
			$sOrderBy = "";
		}
		else {
			$sOrderBy = safeQueryString($_REQUEST['orderby']);	
		}
		if (empty($_REQUEST['sortby']) || !isset($_REQUEST['sortby'])) {
			$sSortBy =  "";
		}
		else {
			$sSortBy = safeQueryString($_REQUEST['sortby']);
		}
	}
	if ((strtoupper($sSortBy) != "ASC") && (strtoupper($sSortBy) != "DESC")) {
		$sSortBy = "ASC";
	}

	$sOrder = "";
	$sOrder1 = "";
	$sOrder2 = "";
	$sOrder3 = "";
	$sOrder4 = "";
	
	
	$sUserInput = "";
	$iCurrentPage = 1;
	$iNumPerPage = 10;
	
	$sOrder = "";
	$sColumn1 = "";
	$sColumn2 = "";
	$sColumn3 = "";
	$sColumn4 = "";
	if ($iTransType == 1) {
		$sColumn1 = "transaksi_ipl_detail.nominal";
		$sColumn2 = "transaksi_ipl_detail.periode_pembayaran";
		$sColumn3 = "";
		$sColumn4 = "";	
	}
	else {
		$sColumn1 = "master_item.item_name";
		$sColumn2 = "transaksi_retail_detail.item_qty";
		$sColumn3 = "transaksi_retail_detail.ietm_price";
		$sColumn4 = "transaksi_retail_detail.subtotal";	
	}
	if ($sOrderBy == "") {
		$sOrderBy = "1";
		$sSortBy = "ASC";
		$sOrder1 = "DESC";
		$sOrderCriteria = $sColumn1 . " " . $sOrder1;
		$sOrder = $sOrder1;
	}
	else {
		if ($sSortBy != "") {
			if ((strtoupper($sSortBy) != "ASC") && (strtoupper($sSortBy) != "DESC")) {
				$sSortBy = "";
			}
		}
		if (strtolower(trim($sOrderBy)) == "1") {
			if ($sSortBy == "") {
				$sOrder1 = "DESC";
			}
			else {
				$sOrder1 = $sSortBy;
			}
			if (trim($sOrder1) == "ASC") {
				$sSortBy = "DESC";
			}
			else {
				$sSortBy = "ASC";
			}
			$sOrderCriteria = $sColumn1 . " " . $sOrder1;
			$sOrder = $sOrder1;
		}
		else if (strtolower(trim($sOrderBy)) == "2") {
			if ($sSortBy == "") {
				$sOrder2 = "ASC";
			}
			else {
				$sOrder2 = $sSortBy;
			}
			if (trim($sOrder2) == "ASC") {
				$sSortBy = "DESC";
			}
			else {
				$sSortBy = "ASC";
			}
			$sOrderCriteria = $sColumn2 . " " . $sOrder2;
			$sOrder = $sOrder2;
		}
		else if (strtolower(trim($sOrderBy)) == "3") {
			if ($sSortBy == "") {
				$sOrder3 = "ASC";
			}
			else {
				$sOrder3 = $sSortBy;
			}
			if (trim($sOrder3) == "ASC") {
				$sSortBy = "DESC";
			}
			else {
				$sSortBy = "ASC";
			}
			$sOrderCriteria = $sColumn3 . " " . $sOrder3;
			$sOrder = $sOrder3;
		}
		else if (strtolower(trim($sOrderBy)) == "4") {
			if ($sSortBy == "") {
				$sOrder4 = "ASC";
			}
			else {
				$sOrder4 = $sSortBy;
			}
			if (trim($sOrder4) == "ASC") {
				$sSortBy = "DESC";
			}
			else {
				$sSortBy = "ASC";
			}
			$sOrderCriteria = $sColumn4 . " " . $sOrder4;
			$sOrder = $sOrder4;
		}
	}
	if ($iTransType == 1) {
		?>
			<table class="tbllist" cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr class="headerList" height="18">
					<td align="left" width="220"><font color="#FFFFFF">Nominal</font>&nbsp;</td>
					<td align="left" width="250"><font color="#FFFFFF">Periode Pembayaran</font>&nbsp;</td>
				</tr>
				<?php
					$sql_query = " SELECT transaksi_ipl_detail.nominal, transaksi_ipl_detail.periode_pembayaran ".
								 " FROM transaksi_ipl_detail ".
								 " WHERE transaksi_ipl_detail.is_active = 'Y'".
								 " AND transaksi_ipl_detail.id_trans = " . $iTransID;
					if ($sOrderCriteria != "") {
						$sql_query = $sql_query . " ORDER BY ". $sOrderCriteria;
					}
					$result = mysqli_query($conn,$sql_query);
					if(mysqli_num_rows($result) > 0){
						while ($row = mysqli_fetch_array($result)) {
						?>	
							<tr>
								<td align="left" width="220"><?=rupiah($row['nominal'])?></td>
								<td align="left" width="250">
									<?php
										echo $bulan[date('m',strtotime($row['periode_pembayaran']))];
									?>
								</td>
							</tr>
						<?php	
						}
					}	
					else {
						?><tr><td align="center" colspan="2">-Untuk saat ini, tidak ada data-</td></tr><?php
					}
				?>
			</table>
		<?php
	}
	else {
		?>
			<table class="tbllist" cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr class="headerList" height="18">
					<td align="left" width="220"><font color="#FFFFFF">Item Name</font>&nbsp;</td>
					<td align="left" width="250"><font color="#FFFFFF">Qty</font>&nbsp;</td>
					<td align="left" width="250"><font color="#FFFFFF">Harga</font>&nbsp;</td>
					<td align="left" width="250"><font color="#FFFFFF">Subtotal</font>&nbsp;</td>
				</tr>
				<?php
					$sql_query = " SELECT master_item.item_name, transaksi_retail_detail.item_qty, ".
								 " transaksi_retail_detail.item_price, transaksi_retail_detail.subtotal ".
								 " FROM transaksi_retail_detail ".
								 " INNER JOIN master_item ON (master_item.item_id = transaksi_retail_detail.item_id) AND (master_item.is_active = 'Y')".
								 " WHERE transaksi_retail_detail.is_active = 'Y'".
								 " AND transaksi_retail_detail.id_trans = " . $iTransID;
					if ($sOrderCriteria != "") {
						$sql_query = $sql_query . " ORDER BY ". $sOrderCriteria;
					}
					$result = mysqli_query($conn,$sql_query);
					if(mysqli_num_rows($result) > 0){
						while ($row = mysqli_fetch_array($result)) {
						?>	
							<tr>
								<td align="left" width="220"><?=$row['item_name']?></td>
								<td align="left" width="250"><?=$row['item_qty']?></td>
								<td align="left" width="250"><?=rupiah($row['item_price'])?></td>
								<td align="left" width="250"><?=rupiah($row['subtotal'])?></td>
							</tr>
						<?php	
						}
					}	
					else {
						?><tr><td align="center" colspan="4">-Untuk saat ini, tidak ada data-</td></tr><?php
					}
				?>
			</table>
		<?php
	}
?>
<script type="text/javascript">
	function viewAlt_popup(pObj) {
		<?php
			if ($sOrder1 == "ASC") {
				?>pObj.title = "Sort Descending";<?php
			}
			else {
				?>pObj.title = "Sort Ascending";<?php
			}
			if ($sOrder2 == "ASC") {
				?>pObj.title = "Sort Descending";<?php
			}
			else {
				?>pObj.title = "Sort Ascending";<?php
			}
			if ($sOrder3 == "ASC") {
				?>pObj.title = "Sort Descending";<?php
			}
			else {
				?>pObj.title = "Sort Ascending";<?php
			}
			if ($sOrder4 == "ASC") {
				?>pObj.title = "Sort Descending";<?php
			}
			else {
				?>pObj.title = "Sort Ascending";<?php
			}
		?>
	}
</script>