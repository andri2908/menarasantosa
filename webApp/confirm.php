<?php
	require_once dirname(__FILE__) . '/include/dbconnect.php';
	require_once dirname(__FILE__) . '/include/checklogin.php';
	require_once dirname(__FILE__) . '/include/strings.php';
	require_once dirname(__FILE__) . '/include/Aes.php';
	use PhpAes\Aes;
	$aes = new Aes('ur3m05tw3lc0m32Alph4S0fT', 'CBC', '1234567890abcdef');
	$query_string = parse_str($_SERVER["QUERY_STRING"], $param);
	
	
	$sOrderID = "";
	$encrypt_key = "";
	$sTransactionStatus = "";
	if (empty($param) || !isset($param['transaction_status'])) {
		$sTransactionStatus = "";
	}
	else {
		$sTransactionStatus = safeQueryString($param['transaction_status']);	
	}
	if (empty($param) || !isset($param['order_id'])) {
		$sOrderID = "";
	}
	else {
		$sOrderID = safeQueryString($param['order_id']);	
	}
	
	if (empty($param) || !isset($param['id'])) {
		$encrypt_key = "";
	}
	else {
		$encrypt_key = safeQueryString($param['id']);	
	}
	
	$id_payment = $aes->decrypt(hex2bin($encrypt_key));
	$id_payment = str_replace("paymentid", "", $id_payment);
	
	$iStatusID = 0;
	if ($id_payment > 0) {
		$sSQL = " SELECT transaksi_payment.status_id ".
				" FROM transaksi_payment " .
				" WHERE transaksi_payment.is_active = 'Y' " .
				" AND transaksi_payment.id_payment = " . $id_payment;
		$result = mysqli_query($conn,$sSQL);
		if ($row = mysqli_fetch_array($result)) {
			$iStatusID = $row['status_id'];
		}		
	}
	
	//$iStatus = 0;
	$sIconImages = "box-success.gif";
	$sStatusDesc = "Pembayaran Berhasil";
	/*
	if ($sTransactionStatus != "") {
		if (($sTransactionStatus == 'capture') || ($sTransactionStatus == 'settlement')) {
			$iStatus = 2;
			$sIconImages = "box-success.gif";
			$sStatusDesc = "Pembayaran Berhasil";
		}
		else if ($sTransactionStatus == 'pending'){
			$iStatus = 1;
			$sIconImages = "box-pending.png";
			$sStatusDesc = "Pembayaran sedang diproses";
		}
		else if ($sTransactionStatus == 'deny'){
			$iStatus = 3;
			$sIconImages = "box-error.gif";
			$sStatusDesc = "Pembayaran Gagal";
		}
	}
	*/
	if ($iStatusID > 0) {
		if ($iStatusID == 2) {
			// Success
			$sIconImages = "box-success.gif";
			$sStatusDesc = "Pembayaran Berhasil";
		}
		else if ($iStatusID == 1) {
			// Pending
			$sIconImages = "box-pending.png";
			$sStatusDesc = "Pembayaran sedang diproses";
		}
		else if ($iStatusID == 3) {
			$sIconImages = "box-error.gif";
			$sStatusDesc = "Pembayaran Gagal";
		}
		else if ($iStatusID == 4) {
			$sIconImages = "box-error.gif";
			$sStatusDesc = "Pembayaran Kadaluarsa";
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>
		Menara Santosa | Transaksi | Konfirmasi
	</title>
	<link href="/contents/css/main.css" rel="stylesheet" type="text/css" />
	<link href="/contents/css/main_custom.css" rel="stylesheet" type="text/css" />
	<meta name="robots" content="noindex, nofollow" />
	<link rel="shortcut icon" href="/contents/images/favicon.ico"/>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext" rel="stylesheet">
	<script type="text/javascript" src="/scripts/jquery.min.js"></script>
	<script type="text/javascript" src="/scripts/jquery-migrate.min.js"></script>
	<script type="text/javascript" src="/scripts/modernizr.js"></script>
	<script type="text/javascript" src="/scripts/general.js"></script>
</head>
<body>
	<div id="wrapdoc">
		<table id="page" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<!-- Header -->
					<div id="wraphdr" style="height: 134px; border-bottom: 0px;">
						<div class="wrapper">
							<h1><a href="/dashboard.php"><img src="/contents/images/menara_top_logo.png" alt="Menara Santosa" height="50" /></a></h1>
							<div id="wrapnav">
								<div id="wrapprinav">
									<div id="wrapadminlinks">
										<ul class="linkuser">
											<li>
												<a href="#" class="loginname"><?=$sUserName?></a>
												<ul>
													<li><a href="/logout.php" class="logout">Logout</a></li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
								<div id="wrapsubnav">
									<ul class="navpri2">
										<li class="active" >
											<a href="#"><span class="subbgnav"></span>&nbsp;</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2" id="td-content">
					<div id="wrapbcrmbs">
						<div class="pri">
							<ul class="breadcrumbs"></ul> 
						</div>
					</div>
					<div id="wrapcontent">
						<div class="pri">
							<div class="content-block">
								
								<div class="listing">
									<div id="lyrForm">
										<div class="tabContent contentlist" id="list-querylist">
											<div id="listing">
												<table cellpadding="0" cellspacing="0" border="0" width="100%">
													<tr valign="top" align="middle" height="100%">
														<td style="text-align:middle;vertical-align:middle;">
															<table>
																<tr>
																	<td style="text-align:left;"><img src="/contents/images/<?=$sIconImages?>"></td>
																	<td style="text-align:left;"><?=$sStatusDesc?></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr valign="top" align="middle" height="100%">
														<td>
															<input type="button" id="btnBack" class="button" value="Kembali ke halaman utama" style="width:300px;">
														</td>
													</tr>
													<tr valign="top" align="middle" height="100%">
														<td>
															<input type="button" id="btnLogout" class="button" value="Logout" style="width:300px;">
														</td>
													</tr>
												</table>
											</div>
										</div> <!-- end #wrapcontent .pri #listing -->
	        						</div>
	        						<br />
								</div>
							</div> <!-- end .content-block -->
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div id="wrapfooter"> 
						<p>
							&copy; <?=date("Y")?>, AlphaSoft
						</p>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#btnBack").click(function() {
				self.location = "dashboard.php";
			});
			$("#btnLogout").click(function() {
				self.location = "logout.php";
			});
		});
	</script>
</body>
</html>
<?php
	$conn->close();
?>

