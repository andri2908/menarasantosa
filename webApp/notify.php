<?php
	require_once dirname(__FILE__) . '/include/dbconnect.php';
	require_once dirname(__FILE__) . '/include/strings.php';
	require_once dirname(__FILE__) . '/midtrans/Midtrans.php';
	require_once dirname(__FILE__) . '/include/midtrans_settings.php';

	$notif = new \Midtrans\Notification();

	
	$sOrderID = $notif->order_id;
	$iStatusCode = $notif->status_code;
	$sTransactionID = $notif->transaction_id;
	$sTransactionStatus = $notif->transaction_status;
	$dTransactionTime = $notif->transaction_time;
	$sPaymentType = $notif->payment_type;
	$iMidTransAmount = $notif->gross_amount;
	
	$fraud = $notif->fraud_status;
	
	$sVANumber = "";
	$sBillKey = "";
	$sBillCode = "";
	$sBankName = "";
	$sPaymentCode = "";
	$iPaymentType = 0;
	$sStoreName = "";
	if ($sPaymentType == "bank_transfer") {
		// Permata, BNI, BRI
		if (!isset($notif->permata_va_number)) {
			foreach($notif->va_numbers as $item) {
				$sVANumber = $item['va_number'];
				$sBankName = ucfirst($item['bank']);
				if (strtolower($sBankName) == "bri") {
					$iPaymentType = 1;
				}
				else if (strtolower($sBankName) == "bni") {
					$iPaymentType = 6;
				}
			}		
		}
		else {
			$iPaymentType = 8;
			$sBankName = "Permata";
			$sVANumber = $notif->permata_va_number;
		}
	}
	else if ($sPaymentType == "echannel") {
		// Mandiri
		$iPaymentType = 7;
		$sBankName = "Mandiri";
		$sBillKey = $notif->bill_key;
		$sBillCode = $notif->biller_code;
	}
	else if ($sPaymentType == "cstore") {
		$sStoreName = $notif->store;
		$sPaymentCode = $notif->payment_code;	
		if (strtolower($sStoreName) == "indomaret") {
			$iPaymentType = 4;
		}
		else {
			$iPaymentType = 5;
		}
	}
	else if ($sPaymentType == "gopay") {
		$iPaymentType = 2;
	}
	else if ($sPaymentType == "shopeepay") {
		$iPaymentType = 3;
	}
	$iStatus = 0;
	if (($sTransactionStatus == 'settlement') || ($sTransactionStatus == 'capture')) {
		// TODO set payment status in merchant's database to 'Settlement'
		$iStatus = 2;
	}
	else if($sTransactionStatus == 'pending'){
		// TODO set payment status in merchant's database to 'Pending'
		$iStatus = 1;
	}
	else if ($sTransactionStatus == 'deny') {
		// TODO set payment status in merchant's database to 'Denied'
		$iStatus = 3;
	}
	else if ($sTransactionStatus == 'expire') {		// TODO set payment status in merchant's database to 'expire'
		$iStatus = 4;
	}
	else if ($sTransactionStatus == 'cancel') {
		// TODO set payment status in merchant's database to 'Denied'
		$iStatus = 3;
	}
	if ($iStatus > 0) {
		$iNewPaymentID = 0;
		$bHavePaymentRecord = false;
		$sSQL = " SELECT transaksi_payment.payment_id ".
				" FROM transaksi_payment ".
				" WHERE transaksi_payment.payment_id = '". validText($sOrderID) . "'" .
				" AND transaksi_payment.is_active = 'Y' ";
		$result = mysqli_query($conn,$sSQL) or die("Query error : " . mysqli_error($result));
		if(mysqli_num_rows($result) > 0){
			$bHavePaymentRecord = true;
		}
		if (!$bHavePaymentRecord) {
			$iUserID = 0;
			$iTotalAmount = 0;
			$iNominal = 0;
			$sSQL = " SELECT DERIVED_TABLE.user_id, DERIVED_TABLE.nominal ".
					" FROM ( " .
					" 	SELECT transaksi_ipl.user_id, transaksi_ipl.nominal ".
					" 	FROM transaksi_ipl ".
					" 	WHERE transaksi_ipl.payment_id = '". validText($sOrderID) . "'" .
					"	UNION ALL " .
					" 	SELECT transaksi_retail.user_id, transaksi_retail.nominal ".
					" 	FROM transaksi_retail ".
					" 	WHERE transaksi_retail.payment_id = '". validText($sOrderID) . "'" .
					" ) AS DERIVED_TABLE ";
			$result = mysqli_query($conn,$sSQL) or die("Query error : " . mysqli_error($result));
			if(mysqli_num_rows($result) > 0){
				while  ($row = mysqli_fetch_array($result)) {
					$iUserID = $row['user_id'];
					$iNominal = $row['nominal'];
					$iTotalAmount = $iTotalAmount + $iNominal;
				}
			}
			$iPaymentFee = 0;
			$iPaymentFee = doubleval($iMidTransAmount) - doubleval($iTotalAmount);
			$sSQL = " INSERT INTO transaksi_payment( ". 
					" 	date_issued, user_id, payment_id, total, status_id, date_paid, is_active, created_by, creation_date, ". 
					"	payment_type, biaya_transaksi, ".
					" 	status_code, transaction_id, transaction_status, transaction_time, midtrans_payment_type, " .
					"	bank_name, va_number, bill_key, biller_code, payment_code ".
					" ) ".
					" VALUES(".
					"	CURRENT_TIMESTAMP(), " . $iUserID . ", '" . validText($sOrderID). "', " . $iTotalAmount . ", " . $iStatus . ", CURRENT_TIMESTAMP(), 'Y', " . $iUserID .", CURRENT_TIMESTAMP(), ". 
					$iPaymentType . ", " .$iPaymentFee . ", ".
					$iStatusCode. ", '". validText($sTransactionID) . "', '" . validText($sTransactionStatus) . "', '" . validText($dTransactionTime) . "', '". validText($sPaymentType) . "', ".
					"'" . validText($sBankName) . "', '". validText($sVANumber) . "', '" . validText($sBillKey) . "', '" . validText($sBillCode) . "', '" . validText($sPaymentCode) . "'" .
					" ) ";
			mysqli_query($conn, $sSQL);
			$iNewPaymentID = $conn->insert_id;
			if ($iNewPaymentID > 0) {
				$sSQL = " SELECT DERIVED_TABLE.id_trans, DERIVED_TABLE.type_trans, DERIVED_TABLE.nominal ".
						" FROM ( " .
						" 	SELECT transaksi_ipl.id_trans, transaksi_ipl.type_trans, transaksi_ipl.nominal ".
						" 	FROM transaksi_ipl ".
						" 	WHERE transaksi_ipl.payment_id = '". validText($sOrderID) . "'" .
						"	UNION ALL " .
						" 	SELECT transaksi_retail.id_trans, transaksi_retail.type_trans, transaksi_retail.nominal ".
						" 	FROM transaksi_retail ".
						" 	WHERE transaksi_retail.payment_id = '". validText($sOrderID) . "'" .
						" ) AS DERIVED_TABLE ";
				$result = mysqli_query($conn,$sSQL) or die("Query error : " . mysqli_error($result));
				if(mysqli_num_rows($result) > 0){
					while  ($row = mysqli_fetch_array($result)) {
						$iTransID = $row['id_trans'];
						$iTransType = $row['type_trans'];
						$iNominal = $row['nominal'];
						
						$bHavePaymentDetailRecord = false;
						$sSQL = " SELECT transaksi_payment_detail.id_payment_detail ".
								" FROM transaksi_payment_detail ".
								" WHERE transaksi_payment_detail.id_payment = ". $iNewPaymentID .
								" AND transaksi_payment_detail.id_trans = ". $iTransID .
								" AND transaksi_payment_detail.type_trans = ". $iTransType .
								" AND transaksi_payment_detail.is_active = 'Y' ";
						$resultN = mysqli_query($conn,$sSQL) or die("Query error : " . mysqli_error($resultN));
						if(mysqli_num_rows($resultN) > 0){
							$bHavePaymentDetailRecord = true;
						}
						if (!$bHavePaymentDetailRecord) {
							$sSQL = " INSERT INTO transaksi_payment_detail(id_payment, id_trans, type_trans, total, is_active, created_by, creation_date) ".
									" VALUES(" . $iNewPaymentID . ", " . $iTransID . ", " . $iTransType . ", " . $iNominal . ", 'Y', " . $iUserID . ", CURRENT_TIMESTAMP()) ";
							mysqli_query($conn, $sSQL);			
						}
					}
				}
			}				
		}
		else {
			$sSQL = " UPDATE transaksi_payment SET ".
					"	transaksi_payment.status_id = " . $iStatus .
					" WHERE transaksi_payment.payment_id = '". validText($sOrderID) . "'";
			mysqli_query($conn, $sSQL);	
		}
		$iCurrentStatus = $iStatus;
		if (($iStatus == 3) || ($iStatus == 4)) {
			$iCurrentStatus = 0;
		}
		$sSQL = " UPDATE transaksi_ipl SET ".
				"	transaksi_ipl.status_id = " . $iCurrentStatus .
				" WHERE transaksi_ipl.payment_id = '". validText($sOrderID) . "'";
		mysqli_query($conn, $sSQL);	
		
		$sSQL = " UPDATE transaksi_retail SET ".
				"	transaksi_retail.status_id = " . $iCurrentStatus .
				" WHERE transaksi_retail.payment_id = '". validText($sOrderID) . "'";
		mysqli_query($conn, $sSQL);	
	}
	$conn->close();
?>