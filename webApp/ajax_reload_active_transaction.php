<?php
	require_once dirname(__FILE__) . '/include/dbconnect.php';
	require_once dirname(__FILE__) . '/include/checklogin.php';
	require_once dirname(__FILE__) . '/include/strings.php';
	
	
	$sUniqueKey = $_SESSION['uniquekey'];
	if (strlen($sUniqueKey) == 0) {
		$IPAddress = str_replace(".", "", $_SERVER['REMOTE_ADDR']);
		$sUniqueKey = session_id() . $IPAddress;
		$_SESSION['uniquekey'] = $sUniqueKey;
	}
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (empty($_REQUEST['orderby']) || !isset($_REQUEST['orderby'])) {
			$sOrderBy = "";
		}
		else {
			$sOrderBy = safeQueryString($_REQUEST['orderby']);	
		}
		if (empty($_REQUEST['sortby']) || !isset($_REQUEST['sortby'])) {
			$sSortBy =  "";
		}
		else {
			$sSortBy = safeQueryString($_REQUEST['sortby']);
		}
	}
	if ((strtoupper($sSortBy) != "ASC") && (strtoupper($sSortBy) != "DESC")) {
		$sSortBy = "ASC";
	}

	$sOrder = "";
	$sOrder1 = "";
	$sOrder2 = "";
	$sOrder3 = "";
	
	$sUserInput = "";
	$iCurrentPage = 1;
	$iNumPerPage = 10;
	
	$sOrder = "";
	$sColumn1 = "DERIVED_TABLE.date_issued";
	$sColumn2 = "type_transDesc";
	$sColumn3 = "DERIVED_TABLE.nominal";
	if ($sOrderBy == "") {
		$sOrderBy = "1";
		$sSortBy = "ASC";
		$sOrder1 = "DESC";
		$sOrderCriteria = $sColumn1 . " " . $sOrder1;
		$sOrder = $sOrder1;
	}
	else {
		if ($sSortBy != "") {
			if ((strtoupper($sSortBy) != "ASC") && (strtoupper($sSortBy) != "DESC")) {
				$sSortBy = "";
			}
		}
		if (strtolower(trim($sOrderBy)) == "1") {
			if ($sSortBy == "") {
				$sOrder1 = "DESC";
			}
			else {
				$sOrder1 = $sSortBy;
			}
			if (trim($sOrder1) == "ASC") {
				$sSortBy = "DESC";
			}
			else {
				$sSortBy = "ASC";
			}
			$sOrderCriteria = $sColumn1 . " " . $sOrder1;
			$sOrder = $sOrder1;
		}
		else if (strtolower(trim($sOrderBy)) == "2") {
			if ($sSortBy == "") {
				$sOrder2 = "ASC";
			}
			else {
				$sOrder2 = $sSortBy;
			}
			if (trim($sOrder2) == "ASC") {
				$sSortBy = "DESC";
			}
			else {
				$sSortBy = "ASC";
			}
			$sOrderCriteria = $sColumn2 . " " . $sOrder2;
			$sOrder = $sOrder2;
		}
		else if (strtolower(trim($sOrderBy)) == "3") {
			if ($sSortBy == "") {
				$sOrder3 = "ASC";
			}
			else {
				$sOrder3 = $sSortBy;
			}
			if (trim($sOrder3) == "ASC") {
				$sSortBy = "DESC";
			}
			else {
				$sSortBy = "ASC";
			}
			$sOrderCriteria = $sColumn3 . " " . $sOrder3;
			$sOrder = $sOrder3;
		}
	}
?>
<table class="tbllist" cellpadding="0" cellspacing="0" border="0" width="100%">
<tr class="headerList" height="18">
	<td width="100" style="padding-left:10px;"><input type="checkbox" id="chkAll" name="chkAll" value="1"/></td>
	<td align="left" width="220"><a href="javascript:orderIt('1')" title="" style="text-decoration:none;" onMouseOver="viewAlt(this)"><font color="#FFFFFF">Tanggal Transaksi</font>&nbsp;<img name="img1" src="/contents/images/sort_blank.gif" alt="" border="0" /></a></td>
	<td align="left" width="250"><a href="javascript:orderIt('2')" title="" style="text-decoration:none;" onMouseOver="viewAlt(this)"><font color="#FFFFFF">Jenis Transaksi</font>&nbsp;<img name="img2" src="/contents/images/sort_blank.gif" alt="" border="0" /></a></td>
	<td align="left" width="250"><font color="#FFFFFF">Keterangan</font>&nbsp;</td>
	<td align="left" width="250"><font color="#FFFFFF">Periode</font>&nbsp;</td>
	<td align="left" width="250"><a href="javascript:orderIt('3')" title="" style="text-decoration:none;" onMouseOver="viewAlt(this)"><font color="#FFFFFF">Nominal</font>&nbsp;<img name="img3" src="/contents/images/sort_blank.gif" alt="" border="0" /></a></td>
	<td align="left" width="50"><font color="#FFFFFF">&nbsp;</font>&nbsp;</td>
</tr>
<?php
	$sql_query = " SELECT DERIVED_TABLE.id_trans, DERIVED_TABLE.date_issued, DERIVED_TABLE.nominal, ".
				 " DERIVED_TABLE.type_trans, ".
				 " CASE DERIVED_TABLE.type_trans ".
				 "	WHEN 1 THEN 'Pembayaran IPL'".
				 "	WHEN 2 THEN 'Retail'".
				 "	WHEN 3 THEN 'Service'".
				 " END AS type_transDesc, ".
				 " CASE DERIVED_TABLE.type_trans ".
				 "	WHEN 1 THEN DERIVED_TABLE.start_ipl ".
				 "	ELSE ''".
				 " END AS start_ipl, ".
				 " CASE DERIVED_TABLE.type_trans ".
				 "	WHEN 1 THEN DERIVED_TABLE.end_ipl ".
				 "	ELSE ''".
				 " END AS end_ipl ".
				 " FROM (".
				 " 	SELECT transaksi_ipl.id_trans, transaksi_ipl.date_issued, transaksi_ipl.nominal, ".
				 " 	transaksi_ipl.type_trans, transaksi_ipl.start_ipl, transaksi_ipl.end_ipl ".
				 " 	FROM transaksi_ipl ".
				 " 	WHERE transaksi_ipl.user_id = ". $iUserID.
				 " 	AND transaksi_ipl.is_active = 'Y'".
				 "	AND transaksi_ipl.status_id = 0 ".
				 " 	UNION ALL ".
				 " 	SELECT transaksi_retail.id_trans, transaksi_retail.date_issued, transaksi_retail.nominal, ".
				 " 	transaksi_retail.type_trans, '1/1/1900' AS start_ipl, '1/1/1900' AS end_ipl ".
				 " 	FROM transaksi_retail ".
				 " 	WHERE transaksi_retail.user_id = ". $iUserID.
				 " 	AND transaksi_retail.is_active = 'Y'".
				 "	AND transaksi_retail.status_id = 0 ".
				 " ) AS DERIVED_TABLE ";
	if ($sOrderCriteria != "") {
		$sql_query = $sql_query . " ORDER BY ". $sOrderCriteria;
		
	}
	$result = mysqli_query($conn,$sql_query);
	if(mysqli_num_rows($result) > 0){
		while ($row = mysqli_fetch_array($result)) {
		?>	
			<tr>
				<td width="100" style="padding-left:10px;"><input type="checkbox" name="changeCheckbox" id="batchCheckbox_<?=$row['id_trans']?>_<?=$row['type_trans']?>" value="<?=$row['id_trans']?>" data-type="<?=$row['type_trans']?>" onclick="toggleBatchCheckbox(this.value,<?=$row['type_trans']?>);"/></td>
				<td align="left" width="220">
					<?php
						echo date('d-m-Y', strtotime($row['date_issued']));
					?>
				</td>
				<td align="left" width="250"><?=$row['type_transDesc']?></td>
				<td align="left" width="250">
					<?php
						if ($row['type_trans'] == 1) {
							// IPL
							$numBulan = 0;
							if (($row['start_ipl'] != "") && ($row['end_ipl'] != "")) {
								$numBulan += date('m',strtotime($row['end_ipl']))-date('m',strtotime($row['start_ipl']));
							}
							if ($numBulan > 0) {
								echo ($numBulan + 1). ' bulan';
							}
							if ($row['start_ipl'] != "") {
								echo ", periode ". $bulan[date('m',strtotime($row['start_ipl']))];
							}
							if ($row['end_ipl'] != "") {
								echo ' - ' . $bulan[date('m',strtotime($row['end_ipl']))];
							}
						}
						else {
							// Retail
							
							$sDesc = "";
							$iItemID = 0;
							$iItemQty = 0;
							$sItemName = "";
							$sSQL = " SELECT transaksi_retail_detail.item_id, transaksi_retail_detail.item_qty, ".
									" master_item.item_name ".
									" FROM transaksi_retail_detail ".
									" INNER JOIN master_item ON (master_item.item_id = transaksi_retail_detail.item_id) AND (master_item.is_active = 'Y') ".
									" WHERE id_trans = ". $row['id_trans'];
							$resultD = mysqli_query($conn,$sSQL);
							while ($rowD = mysqli_fetch_array($resultD)) {	
								$iItemID = $rowD['item_id'];
								$iItemQty = $rowD['item_qty'];
								$sItemName = $rowD['item_name'];
								if (strlen($sDesc) == 0) {
									$sDesc = $sItemName . " (" . $iItemQty . " pcs)";
								}
								else {
									$sDesc = $sDesc . ", ". $sItemName . " (" . $iItemQty . " pcs)";
								}
							}
							echo $sDesc;
							
						}
					?>
				</td>
				<td align="left" width="250">
					<?php
						if ($row['type_trans'] == 1) {
							if ($row['start_ipl'] != "") {
								echo $bulan[date('m',strtotime($row['start_ipl']))];
							}
							if ($row['end_ipl'] != "") {
								echo ' - ' . $bulan[date('m',strtotime($row['end_ipl']))];
							}
						}
						else {
							echo "-";
						}
					?>
				</td>
				<td align="left" width="250"><?=rupiah($row['nominal'])?></td>
				<td align="left" width="50">
					<a id="lnkDetil_<?=$row['id_trans']?>_<?=$row['type_trans']?>">Detil</a>
				</td>
			</tr>
		<?php	
		}
	}	
	else {
		?><tr><td align="center" colspan="7">-Untuk saat ini, tidak ada data-</td></tr><?php
	}
?>
</table>
<?php
	echo "~[*splitter*]~". $sOrderBy . "~[*splitter*]~" . $sOrder . "~[*splitter*]~1";
?>
