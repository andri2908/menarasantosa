<?php
	$bulan = array(
			'01' => 'JANUARI',
			'02' => 'FEBRUARI',
			'03' => 'MARET',
			'04' => 'APRIL',
			'05' => 'MEI',
			'06' => 'JUNI',
			'07' => 'JULI',
			'08' => 'AGUSTUS',
			'09' => 'SEPTEMBER',
			'10' => 'OKTOBER',
			'11' => 'NOVEMBER',
			'12' => 'DESEMBER',
	);
	
	function get_client_ip() {
		$ipaddress = '';
		if (isset($_SERVER['HTTP_CLIENT_IP']))
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_X_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		else if(isset($_SERVER['REMOTE_ADDR']))
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}

	function validText($pStr) {
		$sTemp = "";
		if ((!is_null($pStr)) && ($pStr != "")) {
			$sTemp = str_replace("'", "''", $pStr);
		}
		return $sTemp;
	}
	
	function getAutoVersion($sFilePath) {
		$sReturn = "";
		$sPhysicalFilePath = "";
		$sTimeStamp = "";
		$sRootPath = $_SERVER["DOCUMENT_ROOT"];
		if (trim($sFilePath) != "") {
			$sPhysicalFilePath = str_replace('/','\\', $sFilePath);
			$sTimeStamp = time() - filemtime($sRootPath.$sPhysicalFilePath);
			$sReturn = $sFilePath."?v=".$sTimeStamp;
		}
		return $sReturn;
	}
	
	function rupiah($angka){
		$hasil_rupiah = "Rp " . number_format($angka,2,',','.');
		return $hasil_rupiah;
	}
	
	function safeQueryString($pStr) {
		$sTemp = "";
		if (strlen($pStr) > 0) {
			$sTemp = strip_tags(trim($pStr));
			$sTemp = htmlentities($sTemp);
		}
		else {
			$sTemp = "";
		}
		return $sTemp;
	}
	
	function DrawRecordNavigationAJAX($pTotalRecord, $pNumPerPage, $pCurrentPage) {
		$sQueryString = "";
		$iDecide = 0;
		$iPos = 0;
		$iRecNo = 0;
		$iPrev = 0;
		$sFront = "";
		$sEnd = "";
		$bLoop = false;
		echo "<table  width=\"99%\" class=\"BodyText\" cellpadding=\"1\" cellspacing=\"2\">";
		if ($pTotalRecord > $pNumPerPage) {
			echo "<tr><td><img src=\"/contents/images/spacer.gif\" width=\"1\" height=\"1\" alt=\"spacer\" /></td></tr>";
			echo "<tr>";
			echo "	<td>&nbsp;</td>";
			echo "	<td align=\"right\">";
			$iSignPerPage = 5;
			if ($pNumPerPage > 0) {
				$iTotalPage = floor($pTotalRecord / $pNumPerPage);
				if (($iTotalPage > 0) && (($pTotalRecord % $pNumPerPage) > 0)) {
					$iTotalPage = $iTotalPage + 1;
				}
			}
			$iCurrentPage = $pCurrentPage;
			if ($iCurrentPage == 0) {
				$iCurrentPage = 1;
			}
			$iSmallest = floor($iCurrentPage / $iSignPerPage) * $iSignPerPage;
			if (floor($iCurrentPage / $iSignPerPage) > 0 && ($iCurrentPage % $iSignPerPage == 0)) {
				$iSmallest = $iSmallest - $iSignPerPage;
			}
			$iLargest  = $iSmallest + $iSignPerPage + 1;
			if ($iLargest > $iTotalPage) {
				$iLargest = $iTotalPage + 1;
			}
			if ($iCurrentPage > $iSignPerPage) {
				echo "<a id=\"lnkpaging_1\" href=\"javascript;\">&lt;&lt;</a>&nbsp;";
			}
			if ($iSmallest > $iSignPerPage) {
				echo "<a id=\"lnkpaging_" . $iSmallest . "\" href=\"javascript:;\">&lt;</a>&nbsp;";
			}
			for ($iCount = $iSmallest + 1; $iCount <= $iLargest - 1; $iCount++) {
				if ($iCount == $iCurrentPage) {
					echo "<b>" . $iCount . "</b>&nbsp;";
				}
				else {
					echo "<a id=\"lnkpaging_" . $iCount . "\" href=\"javascript:;\">" . $iCount . "</a>&nbsp;";
				}
			}
			if ($iLargest <= $iTotalPage) {
				 echo "<a id=\"lnkpaging_" . $iLargest . "\" href=\"javascript:;\">&gt;</a>&nbsp;";
			}
			$iNextPerPage = $iLargest + 1;
			if ($iNextPerPage < $iTotalPage) {
				echo "<a id=\"lnkpaging_" . $iTotalPage . "\" href=\"javascript:;\">&gt;&gt;</a>&nbsp;";
			}
			echo "	</td>";
			echo "</tr>";
			
		}
		echo "<tr><td><img src=\"/contents/images/spacer.gif\" width=\"1\" height=\"1\" alt=\"spacer\" /></td></tr>";
		echo "<tr>";
		echo "	<td align=\"left\"><strong>Total Number of records:</strong>&nbsp;&nbsp;" . $pTotalRecord . "</td>";
		echo "	<th align=\"right\">";
		echo "		Records to display per page&nbsp;";
		echo "		<select id=\"sel_page\" name=\"sel_page\">";
		echo "			<option value=\"5\"  ";
		if ($pNumPerPage == 5) {
			echo "selected";
		}
		echo "			>5</option>";
		echo "			<option value=\"10\"  ";
		if ($pNumPerPage == 10) {
			echo "selected";
		}
		echo "			>10</option>";
		echo "			<option value=\"20\"  ";
		if ($pNumPerPage == 20) {
			echo "selected";
		}
		echo "			>20</option>";
		echo "			<option value=\"50\"  ";
		if ($pNumPerPage == 50) {
			echo "selected";
		}
		echo "			>50</option>";
		echo "			<option value=\"100\"  ";
		if ($pNumPerPage == 100) {
			echo "selected";
		}
		echo "			>100</option>";
		echo "		</select>";
		echo "	</th>";
		echo "</tr>";
		echo "</table>";
	}
?>