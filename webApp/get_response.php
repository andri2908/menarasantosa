<?php
	require_once dirname(__FILE__) . '/include/dbconnect.php';
	require_once dirname(__FILE__) . '/include/checklogin.php';
	require_once dirname(__FILE__) . '/include/strings.php';
	require_once dirname(__FILE__) . '/include/Aes.php';
	use PhpAes\Aes;
	
	// takes raw data from the request 
	$json = file_get_contents('php://input');
	// Converts it into a PHP object 
	$data = json_decode($json, true);
	
	$sOrderID = $data['order_id'];
	$iStatusCode = $data['status_code'];
	$sTransactionID = $data['transaction_id'];
	$sTransactionStatus = $data['transaction_status'];
	$dTransactionTime = $data['transaction_time'];
	$sPaymentType = $data['payment_type'];
	//$sFinishRedirectURL = $data['finish_redirect_url'];
	$sFinishRedirectURL = "confirm.php";
	
	$sVANumber = "";
	$sBillKey = "";
	$sBillCode = "";
	$sBankName = "";
	$sPaymentCode = "";
	if ($data['payment_type'] == "bank_transfer") {
		// Permata, BNI, BRI
		if (!isset($data['permata_va_number'])) {
			foreach($data['va_numbers'] as $item) {
				$sVANumber = $item['va_number'];
				$sBankName = ucfirst($item['bank']);
			}		
		}
		else {
			$sBankName = "Permata";
			$sVANumber = $data['permata_va_number'];
		}
	}
	else if ($data['payment_type'] == "echannel") {
		// Mandiri
		$sBankName = "Mandiri";
		$sBillKey = $data['bill_key'];
		$sBillCode = $data['biller_code'];
	}
	else if ($data['payment_type'] == "cstore") {
		$sPaymentCode = $data['payment_code'];	
	}
	
	$iStatus = 0;
	if ($sTransactionStatus != "") {
		if (($sTransactionStatus == 'capture') || ($sTransactionStatus == 'settlement')) {
			$iStatus = 2;
		}
		else if ($sTransactionStatus == 'pending'){
			$iStatus = 1;
		}
		else if ($sTransactionStatus == 'deny'){
			$iStatus = 3;
		}
	}
	
	if ($iStatus > 0) {
		$sUniqueKey = $_SESSION['uniquekey'];
		if (strlen($sUniqueKey) == 0) {
			$IPAddress = str_replace(".", "", $_SERVER['REMOTE_ADDR']);
			$sUniqueKey = session_id() . $IPAddress;
			$_SESSION['uniquekey'] = $sUniqueKey;
		}
		if (strlen($sUniqueKey) > 29) {
			$sUniqueKey = substr($sUniqueKey, 0, 29);
		}
		$iRecID = 0;
		$iPaymentType = 0;
		$iTransFee = 0;
		$iTransFeePercent = 0;
		$sSQL = " SELECT tblusertocheckedrecords.recID, tblusertocheckedrecords.query, ".
				" tblusertocheckedrecords.payment_type, sys_config_transaksi.biaya_transaksi, ". 
				" sys_config_transaksi.biaya_transaksi_percent ".
				" FROM tblusertocheckedrecords ".
				" INNER JOIN sys_config_transaksi ON sys_config_transaksi.payment_type = tblusertocheckedrecords.payment_type ".
				" AND sys_config_transaksi.is_active = 'Y'".
				" WHERE tblusertocheckedrecords.userID = ". $iUserID.
				" AND tblusertocheckedrecords.uniqueKey = '" . validText($sUniqueKey) . "'".
				" AND tblusertocheckedrecords.type = 1 ";
		$result = mysqli_query($conn,$sSQL);
		if ($row = mysqli_fetch_array($result)) {
			$iRecID = $row['recID'];
			$iPaymentType = $row['payment_type'];
			$iTransFee = $row['biaya_transaksi'];
			$iTransFeePercent = $row['biaya_transaksi_percent'];
		}
		$iTotalAmount = 0;
		if ($iRecID > 0) {
			$sSQL = " SELECT tblusertocheckedrecordsdetail.recordID, tblusertocheckedrecordsdetail.trans_type ".
					" FROM tblusertocheckedrecordsdetail ".
					" WHERE tblusertocheckedrecordsdetail.recID = " . $iRecID;
			$result = mysqli_query($conn,$sSQL) or die("Query error : " . mysqli_error($result));
			if(mysqli_num_rows($result) > 0){
				while  ($row = mysqli_fetch_array($result)) {
					$iTransID = $row['recordID'];
					$iTransType = $row['trans_type'];
					$sSQLN = "";
					if ($iTransType == 1) {
						// IPL
						$sSQLN = " UPDATE transaksi_ipl SET ".
								 " 	transaksi_ipl.status_id = " . $iStatus. ", ".
								 "  transaksi_ipl.payment_id = '" . validText($sOrderID) . "', ".
								 "  transaksi_ipl.date_paid = '" . validText($dTransactionTime) . "'".
								 " WHERE transaksi_ipl.id_trans = ". $iTransID .
								 " AND transaksi_ipl.status_id = 0 ";
					}
					else {
						// Retail
						$sSQLN = " UPDATE transaksi_retail SET ".
								 " 	transaksi_retail.status_id = " . $iStatus. ", ".
								 "  transaksi_retail.payment_id = '" . validText($sOrderID) . "', ".
								 "  transaksi_retail.date_paid = '" . validText($dTransactionTime) . "'".
								 " WHERE transaksi_retail.id_trans = ". $iTransID .
								 " AND transaksi_retail.status_id = 0 ";
					}
					if (strlen($sSQLN) > 0) {
						mysqli_query($conn, $sSQLN);
					}
					$sSQLN = "";
					$iNominal = 0;
					if ($iTransType == 1) {
						// IPL
						$sSQLN = " SELECT transaksi_ipl.nominal ".
								 " FROM transaksi_ipl ".
								 " WHERE transaksi_ipl.id_trans = ". $iTransID;
					}
					else {
						// Retail
						$sSQLN = " SELECT transaksi_retail.nominal ".
								 " FROM transaksi_retail ".
								 " WHERE transaksi_retail.id_trans = ". $iTransID;
					}
					if (strlen($sSQLN) > 0) {
						$resultN = mysqli_query($conn,$sSQLN) or die("Query error : " . mysqli_error($resultN));
						if ($rowN = mysqli_fetch_array($resultN)) {
							$iNominal = $rowN['nominal'];
						}
					}
					$iTotalAmount = $iTotalAmount + $iNominal;
				}
			}
		}
		if ($iTransFeePercent > 0) {
			$iTransFeePercent = (($iTransFeePercent/100) * $iTotalAmount);
		}
		$iPaymentFee = 0;
		if ($iTransFee > 0) {
			$iPaymentFee = $iTransFee;
		}
		if ($iTransFeePercent > 0) {
			$iPaymentFee = $iTransFeePercent;
		}
		$iNewPaymentID = 0;
		$bHavePaymentRecord = false;
		$sSQL = " SELECT transaksi_payment.payment_id ".
				" FROM transaksi_payment ".
				" WHERE transaksi_payment.payment_id = '". validText($sOrderID) . "'" .
				" AND transaksi_payment.is_active = 'Y' ";
		$result = mysqli_query($conn,$sSQL) or die("Query error : " . mysqli_error($result));
		if(mysqli_num_rows($result) > 0){
			$bHavePaymentRecord = true;
		}			
		if (!$bHavePaymentRecord) {
			$sSQL = " INSERT INTO transaksi_payment( ". 
					" 	date_issued, user_id, payment_id, total, status_id, date_paid, is_active, created_by, creation_date, ". 
					"	payment_type, biaya_transaksi, ".
					" 	status_code, transaction_id, transaction_status, transaction_time, midtrans_payment_type, " .
					"	bank_name, va_number, bill_key, biller_code, payment_code ".
					" ) ".
					" VALUES(".
					"	CURRENT_TIMESTAMP(), " . $iUserID . ", '" . validText($sOrderID). "', " . $iTotalAmount . ", " . $iStatus . ", CURRENT_TIMESTAMP(), 'Y', " . $iUserID .", CURRENT_TIMESTAMP(), ". 
					$iPaymentType . ", " .$iPaymentFee . ", ".
					$iStatusCode. ", '". validText($sTransactionID) . "', '" . validText($sTransactionStatus) . "', '" . validText($dTransactionTime) . "', '". validText($sPaymentType) . "', ".
					"'" . validText($sBankName) . "', '". validText($sVANumber) . "', '" . validText($sBillKey) . "', '" . validText($sBillCode) . "', '" . validText($sPaymentCode) . "'" .
					" ) ";
			mysqli_query($conn, $sSQL);
			$iNewPaymentID = $conn->insert_id;
			if ($iNewPaymentID > 0) {
				if ($iRecID > 0) {
					$sSQL = " SELECT tblusertocheckedrecordsdetail.recordID, tblusertocheckedrecordsdetail.trans_type ".
							" FROM tblusertocheckedrecordsdetail ".
							" WHERE tblusertocheckedrecordsdetail.recID = " . $iRecID;
					$result = mysqli_query($conn,$sSQL) or die("Query error : " . mysqli_error($result));
					if(mysqli_num_rows($result) > 0){
						while  ($row = mysqli_fetch_array($result)) {
							$iTransID = $row['recordID'];
							$iTransType = $row['trans_type'];
							$iNominal = 0;
							if ($iTransType == 1) {
								// IPL
								$sSQLN = " SELECT transaksi_ipl.nominal ".
										 " FROM transaksi_ipl ".
										 " WHERE transaksi_ipl.id_trans = ". $iTransID;
							}
							else {
								// Retail
								$sSQLN = " SELECT transaksi_retail.nominal ".
										 " FROM transaksi_retail ".
										 " WHERE transaksi_retail.id_trans = ". $iTransID;
							}
							if (strlen($sSQLN) > 0) {
								$resultN = mysqli_query($conn,$sSQLN) or die("Query error : " . mysqli_error($resultN));
								if ($rowN = mysqli_fetch_array($resultN)) {
									$iNominal = $rowN['nominal'];
								}
							}
							$bHavePaymentDetailRecord = false;
							$sSQL = " SELECT transaksi_payment_detail.id_payment_detail ".
									" FROM transaksi_payment_detail ".
									" WHERE transaksi_payment_detail.id_payment = ". $iNewPaymentID .
									" AND transaksi_payment_detail.id_trans = ". $iTransID .
									" AND transaksi_payment_detail.type_trans = ". $iTransType .
									" AND transaksi_payment_detail.is_active = 'Y' ";
							$resultN = mysqli_query($conn,$sSQL) or die("Query error : " . mysqli_error($resultN));
							if(mysqli_num_rows($resultN) > 0){
								$bHavePaymentDetailRecord = true;
							}
							if (!$bHavePaymentDetailRecord) {
								$sSQL = " INSERT INTO transaksi_payment_detail(id_payment, id_trans, type_trans, total, is_active, created_by, creation_date) ".
										" VALUES(" . $iNewPaymentID . ", " . $iTransID . ", " . $iTransType . ", " . $iNominal . ", 'Y', " . $iUserID . ", CURRENT_TIMESTAMP()) ";
								mysqli_query($conn, $sSQL);			
							}
						}
					}
					$sSQL = " DELETE FROM tblusertocheckedrecordsdetail WHERE tblusertocheckedrecordsdetail.recID = " . $iRecID;
					mysqli_query($conn, $sSQL);	
				}
			}
		}
	}
	$aes = new Aes('ur3m05tw3lc0m32Alph4S0fT', 'CBC', '1234567890abcdef');
	$encrypt_key = $aes->encrypt("paymentid" . $iNewPaymentID);
	echo $sFinishRedirectURL . "?id=" . bin2hex($encrypt_key);
	exit();
?>