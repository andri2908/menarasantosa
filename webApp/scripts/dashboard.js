$(document).ready(function() {
    $("#chkAll").change(function() {
		if ($(this).prop('checked') == true){ 
			checkAllOnPage();
		}
		else {
			clearAll();
			$("#btnBayar").prop("disabled", true);
		}
	});
	$("#btnBayar").click(function() {
		$("#dialogPay").dialog( "open" );	
	});
	
	$('[id*="lnkDetil_"]').click(function() {
		var $id = $(this).attr("id");
		var arrID = $id.replace("lnkDetil_", "").split("_");
		var iTransID = arrID[0];
		var iTransType = arrID[1];
		var sURL = "get_transaction_detail.php";
		$.ajax({
			type: "POST",
			url: sURL,
			data: "id=" + iTransID + "&t=" + iTransType,
			cache: false,
			dataType: 'html',
			success: function (data, textStatus, jqXHR) {
				$("#dialogDetail").html($.trim(data));	
				$("#dialogDetail").dialog("open");	
			},
			error: function (data, textStatus, jqXHR) {
				alert(textStatus);
			}
		});
	});
	checkBatchCheckbox();
	
	$('input[type=radio][name=rdMethod]').change(function() {
		$(".validation").hide();
	});
	
	$( "#dialogDetail" ).dialog({
      resizable: false,
	  autoOpen: false,
      height: "auto",
      width: 750,
      modal: true,
	  open: function(event, ui) { $(this).scrollTop(0); },
      buttons: {
        'Tutup': function() {
          $( this ).dialog( "close" );
        }
      }
    });
	
	$( "#dialogPay" ).dialog({
      resizable: false,
	  autoOpen: false,
      height: "auto",
      width: 400,
      modal: true,
      buttons: {
		 'Bayar': function() {
				  var iSelectedMethod = 0;
				  if (typeof $('input[name="rdMethod"]:checked').val() != "undefined") {
					  iSelectedMethod = $('input[name="rdMethod"]:checked').val();
				  }
				  if (iSelectedMethod > 0) {
					$( this ).dialog( "close" );
					var sURL = "generate_snapToken.php";
					$.ajax({
						type: "POST",
						url: sURL,
						data: "m=" + iSelectedMethod,
						cache: false,
						dataType: 'html',
						success: function (data, textStatus, jqXHR) {
							//do nothing
							if ($.trim(data) != "failed") {
								window.snap.pay(data, {
								  // Optional
								  onSuccess: function(result){
									//document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
									getMidTransResponses(result);
								  },
								  // Optional
								  onPending: function(result){
									
									//document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
									//console.log(JSON.stringify(result, null, 2));
									getMidTransResponses(result);
								  },
								  // Optional
								  onError: function(result){
									
									//document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
									getMidTransResponses(result);
								  }
								}); 	
								
							}
						},
						error: function (data, textStatus, jqXHR) {
							alert(textStatus);
						}
					});
				  }
				
		 },
        'Tutup': function() {
          $( this ).dialog( "close" );
        }
      }
    });
	
});


function getMidTransResponses(result) {
	$.ajax({
		url: 'get_response.php',
		type: 'post',
		dataType: 'html',
		contentType: 'application/json',
		data: JSON.stringify(result, null, 2),
		success: function (data, textStatus, jqXHR) {
			self.location = $.trim(data);
		}
		
	});
}

function showCheckbox() {
	var dataElements = document.getElementsByName("changeCheckbox");
	var actionElement = document.getElementById("changeCheckboxAction");
	var dataElement;
	if ((actionElement.style.display == "") || (actionElement.style.display == "none")) {
		for(var i=0; i<dataElements.length; i++){
			dataElement = dataElements[i];
			dataElement.style.display = "inline";
		}
		actionElement.style.display = "block";
	} 
	else {
		for(var i=0; i<dataElements.length; i++){
			dataElement = dataElements[i];
			dataElement.style.display = "none";
		}
		actionElement.style.display = "none";
	}
}

function showCheckboxTop() {
	var dataElements = document.getElementsByName("changeCheckbox");
	var actionElement = document.getElementById("changeCheckboxAction");
	var dataElement;
	if ((actionElement.style.display == "") || (actionElement.style.display == "none")) {
		for(var i=0; i<dataElements.length; i++){
			dataElement = dataElements[i];
			dataElement.style.display = "inline";
		}
		actionElement.style.display = "block";
		$("html, body").animate({ scrollTop: $(document).height() }, "slow");
	} else {
		$("html, body").animate({ scrollTop: $(document).height() }, "slow");
	}
}

function checkAllOnPage() {
	var dataElements = document.getElementsByName("changeCheckbox");
	var dataElement;
	var poststr = "";
	var posttype = "";
	var transType = "";
	for(var i=0; i<dataElements.length; i++){
		dataElement = dataElements[i];
		transType = $("#" + dataElements[i].id).data("type");
		dataElement.checked = true;
		poststr += dataElement.value;
		posttype += transType;
		if(i<dataElements.length - 1){
			poststr += ",";
			posttype += ",";
		}
	}
	if (poststr.length > 0) {
		var sURL = "ajax_batchprocessed.php";
		$.ajax({
			type: "POST",
			url: sURL,
			data: "action=saveBatchTransIDs&checkedTransIDcsv=" + poststr + "&t=" + posttype,
			cache: false,
			dataType: 'html',
			success: function (data, textStatus, jqXHR) {
				//do nothing
				$("#btnBayar").prop("disabled", false);
			},
			error: function (data, textStatus, jqXHR) {
				alert(textStatus);
			}
		});
	}
}

function checkAllOnScope() {
	var dataElements = document.getElementsByName("changeCheckbox");
	var dataElement;

	for(var i=0; i<dataElements.length; i++){
		dataElement = dataElements[i];
		dataElement.checked = true;
	}
	var sURL = "ajax_batchprocessed.php";
	$.ajax({
		type: "POST",
		url: sURL,
		data: "action=saveAllBatchTransIDs",
		cache: false,
		dataType: 'html',
		success: function (data, textStatus, jqXHR) {
			//do nothing
		},
		error: function (data, textStatus, jqXHR) {
			alert(textStatus);
		}
	});
}

function clearAll() {
	var sURL = "ajax_batchprocessed.php";
	$.ajax({
		type: "POST",
		url: sURL,
		data: "action=clear",
		cache: false,
		dataType: 'html',
		success: function (data, textStatus, jqXHR) {
			var dataElements = document.getElementsByName("changeCheckbox");
			var dataElement;

			for(var i=0; i<dataElements.length; i++){
				dataElement = dataElements[i];
				dataElement.checked = false;
			}
		},
		error: function (data, textStatus, jqXHR) {
			if (textStatus != ''){
				alert(textStatus);
			}
		}
	});
}

function checkBatchCheckbox() {
	var dataElements = document.getElementsByName("changeCheckbox");
	var dataElement;
	var poststr = ""	
	for(var i=0; i<dataElements.length; i++){
		dataElement = dataElements[i];
		poststr += dataElement.value;
		if(i<dataElements.length - 1){
			poststr += ",";
		}
	}
	var sURL = "ajax_batchprocessed.php";
	$.ajax({
		type: "POST",
		url: sURL,
		data: "action=isCheckedBatchTransID&pageTransIDs="+poststr,
		cache: false,
		dataType: 'html',
		success: function (data, textStatus, jqXHR) {
			arrData = data.split("~[|*splitter*|]~");
			arrCheckedTransID = arrData[0].split(",");
			arrCheckedTransType = arrData[1].split(",");
			var dataElement;
			if (arrCheckedTransID.length > 0) {
				for(var i=0; i<arrCheckedTransID.length; i++){
					var dataElement = document.getElementById("batchCheckbox_"+arrCheckedTransID[i] + "_" + arrCheckedTransType[i]);
					if(dataElement != null) {
						dataElement.checked = true;	
						$("#btnBayar").prop("disabled", false);
					}
				}
				//showCheckbox();
			}
		},
		error: function (data, textStatus, jqXHR) {
			if (textStatus != ''){
				alert(textStatus);
			}
		}
	});
}

function toggleBatchCheckbox(transID, transType) {
	var dataElement = document.getElementById("batchCheckbox_" + transID + "_" + transType);
	var sURL = "";
	var sData = "";
	if(dataElement.checked){
		$("#btnBayar").prop("disabled", false);
		sURL = "ajax_batchprocessed.php";
		sData = "action=addBatchTransID&transID=" + transID + "&t=" + transType;
	}
	else{
		if ($('input[name="changeCheckbox"]:checked').length == 0) {
			$("#btnBayar").prop("disabled", true);	
		}
		sURL = "ajax_batchprocessed.php";
		sData = "action=removeBatchTransID&transID=" + transID + "&t=" + transType;
	}
	$.ajax({
		type: "POST",
		url: sURL,
		data: sData,
		cache: false,
		dataType: 'html',
		success: function (data, textStatus, jqXHR) {
			//do nothing
		},
		error: function (data, textStatus, jqXHR) {
			if (textStatus != ''){
				alert(textStatus);
			}
		}
	});
}