<?php
	require_once dirname(__FILE__) . '/include/dbconnect.php';
	require_once dirname(__FILE__) . '/include/strings.php';
	if(isset($_POST['txtUserLogin'])){
		$uname = mysqli_real_escape_string($conn,$_POST['txtUserLogin']);
		if (strlen($uname) > 15) {
			$uname = substr($uname, 15);
		}
		$password = mysqli_real_escape_string($conn,$_POST['txtUserPwd']);
		if (strlen($password) > 15) {
			$password = substr($password, 15);
		}
		if ($uname != "" && $password != ""){
			$sql_query = " SELECT user_login_data.user_id , user_login_data.user_name".
			             " FROM user_login_data ".
						 " WHERE user_name = '".validText($uname)."'".
						 " AND user_password = '".validText(trim(md5($password)))."'".
						 " AND is_active = 'Y'";
			$result = mysqli_query($conn,$sql_query);
			$row = mysqli_fetch_array($result);
			if($row['user_id'] > 0){
				$IPAddress = str_replace(".", "", $_SERVER['REMOTE_ADDR']);
				$sUniqueKey = session_id() . $IPAddress;
				if (strlen($sUniqueKey) > 29) {
					$sUniqueKey = substr($sUniqueKey, 0, 29);
				}
				$_SESSION['uid'] = $row['user_id'];
				$_SESSION['uname'] = $row['user_name'];
				$_SESSION['uniquekey'] = $sUniqueKey;
				$_SESSION['start'] = time(); // Taking now logged in time.
				// Ending a session in 4 hour from the starting time.
				$_SESSION['expire'] = $_SESSION['start'] + (240 * 60);
				
				$iRecID = 0;
				$sSQL = " SELECT tblusertocheckedrecords.recID ".
						" FROM tblusertocheckedrecords ".
						" WHERE tblusertocheckedrecords.userID = ". $row['user_id'] .
						" AND tblusertocheckedrecords.uniqueKey = '" . validText($sUniqueKey) . "'".
						" AND tblusertocheckedrecords.type = 1 ";
				$result = mysqli_query($conn,$sSQL);
				if ($row = mysqli_fetch_array($result)) {
					$iRecID = $row['recID'];
				}
				
				if ($iRecID > 0) {
					$sSQL = " DELETE FROM tblusertocheckedrecordsdetail WHERE recID = " . $iRecID;
					mysqli_query($conn, $sSQL);	
					
					$sSQL = " DELETE FROM tblusertocheckedrecords WHERE recID = " . $iRecID;
					mysqli_query($conn, $sSQL);	
				}
				header('Location: dashboard.php');
			}
			else{
				$cookie_name = "gGkHIcDyEPlF";
				$cookie_value = "AU";
				// 1 hour
				setcookie($cookie_name, $cookie_value, time() + 3600); 
				header('Location: index.php');
			}
		}
	}
	$conn->close();
?>