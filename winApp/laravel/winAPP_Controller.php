<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Validator;
use Intervention\image\Facades\Image;
use Carbon\Carbon;

date_default_timezone_set('Asia/Jakarta');

class emp{
    public $Status;
    public $CardNo;
    public $FullName;
    public $UTC;
    public $Message;
    public $recordCount;
}            

class userAccessList{
    public $user_full_name;
    public $terminal_id;
    public $access_id;
    public $access_type;
    public $access_status;
    public $message;
}

class returnStatus{
    public $Status;
}


class returnStatus_Update{
    public $Status;
    public $Message;
}

class winAPP_Controller extends Controller
{  
    public function testQuery(){
        $query = 
        "select 	case

						        when date(ti.start_ipl) = date(ti.end_ipl) then

						          concat('Biaya IPL ', date_format(ti.start_ipl,'%b-%y'))

						        else

						          concat('Biaya IPL ', date_format(ti.start_ipl,'%b-%y'), ' s/d ', date_format(ti.end_ipl,'%b-%y'), ' (',TIMESTAMPDIFF(MONTH, ti.start_ipl, ti.end_ipl) + 1,' bulan)')

						        end description

						from 	transaksi_ipl ti

						where 	id_trans = 1";

        $hasil = \DB::select(\DB::raw($query));

        $desc = $hasil[0]->description;

        return $desc;
    }

    public function testDiff()
    {
    //     $sqlComm = "
    //     select datediff(now(), max(end_ipl)) as endDate 
    //     from transaksi_ipl ipl, master_kavling mk, kompleks_terminal kt
    //     where ipl.user_id = 59
    //     and ipl.status_id = 2 
    //     and ipl.is_active = 'Y'
    //     and ipl.kavling_id = mk.kavling_id 
    //     and kt.kompleks_id = mk.kompleks_id
    //     and kt.terminal_id = '001'";

    //     $endIPL = \DB::select(\DB::raw($sqlComm));

    //     //$diff = Carbon::now()->diffInDays(Carbon::now());
    //     //$diff = ($endIPL[0]->endDate)->diffInDays(Carbon::now());

    //    return $endIPL[0]->endDate;
    $sqlComm = "
                                select count(1)
                                from transaksi_ipl ipl,
                                master_kavling mk,
                                kompleks_terminal kt
                                where ipl.user_id = 59
                                and ipl.status_id <> 2 
                                and ipl.is_active = 'Y'  
                                and ipl.start_ipl <= NOW() 
                                and ipl.kavling_id = mk.kavling_id
                                and mk.kompleks_id = kt.kompleks_id
                                and kt.terminal_id = '001'
                                ";
                    $data = \DB::select(\DB::raw($sqlComm));
                    return $data;
    }

    public function testUserList()
    {
        $responseResult = array();

        $this->dropCreateTempTable();              
        $this->fillInTempTable('001', 'QR');    


        $query = "select * from user_access_temp";

                $data = \DB::select(\DB::raw($query));
                $recordCount = count($data);

                $i = 0;
                foreach ($data as $queryData) {                
                    $response = new emp();          
                    $response->Status = intval($queryData->access_status);    
                    $response->CardNo = $queryData->access_id;    
                    $response->FullName = $queryData->user_full_name;    
                    $response->UTC = gmdate("d-m-Y H:i:s");    
                    $response->Message =  $queryData->message;    
                    $response->recordCount = $recordCount;
                    $responseResult[] = $response;        
                }

                $result = json_encode($responseResult);

                return $result;
    }

    public function testDate(){
        $current = Carbon::now();
        echo $current->format('Y-m-d H:i:s') . "\n";
        echo "<br>";
        $current->addHour(12);
        echo $current->format('Y-m-d H:i:s') . "\n";
    }

    public function checkToken($userID, $accessToken){
        try
        {
            $query = "  
            select  count(1) as num
            from    user_token ut
            where   ut.user_id = ".$userID." 
                    and ut.access_token = '". $accessToken ."'";

            $result = \DB::select(\DB::raw($query));

            return $result[0]->num;
        }
        catch(Exception $e) 
        { 
            return 0;
        }
    }

    public function checkExternalToken($accessToken){
     
        $tokenDefault = "037f6587a54504d3f512881c7ce30209"; // menarasantosa

        try
        {
            if ($accessToken == $tokenDefault)
                return 1;
            else
                return 0;
        }
        catch(Exception $e) 
        { 
            return 0;
        }
    }   

    public function saveLastLogin(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_loginType            = $request->get('p_loginType');
            $fieldToUpdate          = "last_login";     

            if ($p_loginType == 2)
                $fieldToUpdate = "last_logout";

            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $rows = \DB::table('user_login_data')
                ->where('user_id', $p_user_id)
                ->update([$fieldToUpdate => new \DateTime()]);
                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }
    
    public function getDataSingleValue(Request $request){
       
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
            'sqlQuery'              => 'required',
        ]);

        if($validation->fails())
        {
            $errors = $validation->errors();
            return $errors->toJson();
        }

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
    
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $query   = $request->get('sqlQuery');

                $result = \DB::select(\DB::raw($query));
    
                $result = response()->json([[
                            'data' => $result[0]->resultQuery, 
                            'o_status' => 1,
                            'o_message' => "success",        
                            ]], 200);       
                }
        } 
        catch (Exception $e) 
        {
            $result = response()->json([[
                'data' => 0,
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function getDataSingleValueNoParam(Request $request){
       
        $validation = Validator::make($request->all(),[ 
            'sqlQuery'              => 'required',
        ]);

        if($validation->fails())
        {
            $errors = $validation->errors();
            return $errors->toJson();
        }

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $query   = $request->get('sqlQuery');

            $result = \DB::select(\DB::raw($query));

            $result = response()->json([[
                        'data' => $result[0]->resultQuery, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);       
        } 
        catch (Exception $e) 
        {
            $result = response()->json([[
                'data' => 0,
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function getData(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
            'sqlQuery'              => 'required',
        ]);

        if($validation->fails())
        {
            $errors = $validation->errors();
            return $errors->toJson();
        }

        $result = response()->json(
            ['dataList' => "error",
            'dataStatus' => 
                ['data' => 0,
                'o_status'  => -2,
                'o_message' => "fail",]
            ], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $query                  = $request->get('sqlQuery');

            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $data = \DB::select(\DB::raw($query));

                $result = response()->json(
                    ['dataList' => $data,
                    'dataStatus' => 
                        ['data' => 0,
                        'o_status'  => 1,
                        'o_message' => "success",]
                    ], 200);
            }
        } 
        catch (Exception $e) 
        {
            $result = response()->json(
                ['dataList' => "error",
                'dataStatus' => 
                    ['data' => 0,
                    'o_status'  => -1,
                    'o_message' => $e->getMessage(),]
                ], 200);
        }

        return $result;    
    }

    public function getDataNoParam(Request $request){
        $validation = Validator::make($request->all(),[ 
            'sqlQuery'              => 'required',
        ]);

        if($validation->fails())
        {
            $errors = $validation->errors();
            return $errors->toJson();
        }

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $query = $request->get('sqlQuery');
            $data = \DB::select(\DB::raw($query));

            $result = response()->json(
                ['dataList' => $data,
                'dataStatus' => 
                    ['data' => 0,
                    'o_status'  => 1,
                    'o_message' => "success",]
                ], 200);
        } 
        catch (Exception $e) 
        {
            $result = response()->json([[
                'data' => 0,
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;    
    }

    public function getUserGroupAccess(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
            'p_group_user_id'      => 'required',
        ]);

        if($validation->fails())
        {
            $errors = $validation->errors();
            return $errors->toJson();
        }

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_group_user_id        = $request->get('p_group_user_id');

            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $query   = "select ms.module_id, ms.user_access_option 
                            from master_module_access ms, master_module ma
                            where ms.module_id = ma.module_id 
                            and ma.module_active = 1
                            and ms.group_id = ". $p_group_user_id;

                $data = \DB::select(\DB::raw($query));

                $result = response()->json(
                    ['dataList' => $data,
                    'dataStatus' => 
                        ['data' => 0,
                        'o_status'  => 1,
                        'o_message' => "success",]
                    ], 200);
            }
        } 
        catch (Exception $e) 
        {
            $result = response()->json([[
                'data' => 0,
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;    
    }
   
    public function changePassword(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
            'p_new_password'      => 'required',
        ]);

        if($validation->fails())
        {
            $errors = $validation->errors();
            return $errors->toJson();
        }

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        \DB::beginTransaction();

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_new_password         = $request->get('p_new_password');

            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $rows = \DB::table('user_login_data')
                ->where('user_id', $p_user_id)
                ->update([
                    'last_update_date' => new \DateTime(), 
                    'last_updated_by' => $p_user_id, 
                    'user_password' => $p_new_password,
                ]);
                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }

    public function saveGroupUser(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));
                $queryData = $records[0];    

                if ($p_opType == 1) // INSERT
                {
                    $rows = \DB::table('master_group')
                            ->insertGetId([
                                'group_name' => $queryData->group_name,
                                'group_description' => $queryData->group_description,
                                'group_type' => $queryData->group_type,
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime()
                            ]);
                }
                else // UPDATE
                {
                    $rows = \DB::table('master_group')
                        ->where('group_id', $queryData->group_id)
                        ->update([
                            'group_name' => $queryData->group_name,
                            'group_description' => $queryData->group_description,
                            'group_type' => $queryData->group_type,
                            'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime()
                        ]);
                }

                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveUserData(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));
                $queryData = $records[0];    

                $recordsToken = json_decode($request->get('records_token'));
                $queryDataToken = $recordsToken[0];    

                if ($p_opType == 1) // INSERT
                {
                    $rows = \DB::table('user_login_data')
                            ->insertGetId([
                                'user_name' => $queryData->user_name,
                                'user_password' => $queryData->user_password,
                                'group_id' => $queryData->group_id,
                                'user_full_name' => $queryData->user_full_name,
                                'user_id_type' => $queryData->user_id_type,
                                'user_id_no' => $queryData->user_id_no,
                                'user_phone_1' => $queryData->user_phone_1,
                                'user_phone_2' => $queryData->user_phone_2,
                                'user_email_address' => $queryData->user_email_address,
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime()
                            ]);

                    $rowsToken = \DB::table('user_token')
                                ->insert([
                                'user_id' => $rows, //$queryDataToken->user_id,
                                'access_token' => $queryDataToken->access_token,
                            ]);
                }
                else // UPDATE
                {
                    $rows = \DB::table('user_login_data')
                        ->where('user_id', $queryData->user_id)
                        ->update([
                            'user_name' => $queryData->user_name,
                            'user_password' => $queryData->user_password,
                            'group_id' => $queryData->group_id,
                            'user_full_name' => $queryData->user_full_name,
                            'user_id_type' => $queryData->user_id_type,
                            'user_id_no' => $queryData->user_id_no,
                            'user_phone_1' => $queryData->user_phone_1,
                            'user_phone_2' => $queryData->user_phone_2,
                            'user_email_address' => $queryData->user_email_address,
                            'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime()
                        ]);

                    $rowsToken = \DB::table('user_token')
                        ->where('user_id', $queryDataToken->user_id)
                        ->update([
                            'access_token' => $queryDataToken->access_token,
                        ]);
                }

                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveModuleAccess(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_group_id        = $request->get('p_group_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));

                foreach ($records as $queryData) 
                {             
                    if ($queryData->uam_id > 0)
                    {
                        $rows = \DB::table('master_module_access')
                        ->where('id', $queryData->uam_id)
                        ->update([
                            'user_access_option' => $queryData->user_access_option,
                        ]);
                    }
                    else
                    {
                        $rows = \DB::table('master_module_access')
                            ->insert([
                            'group_id' => $p_group_id,
                            'module_id' => $queryData->module_id,
                            'user_access_option' => $queryData->user_access_option,
                        ]);
                    }
                }

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
                }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveSysConfig(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('sysConfigParam'));
                $sysConfigTrans = json_decode($request->get('sysConfigTransParam'));
                $sysConfigSchedule = json_decode($request->get('sysConfigScheduleParam'));

                $sysConfig = $records[0];

                $rows = \DB::table('sys_config')
                    ->where('id', 1)
                    ->update([
                        'company_name' => $sysConfig->company_name,
                        'company_address' => $sysConfig->company_address,
                        'company_phone' => $sysConfig->company_phone,
                        'company_email' => $sysConfig->company_email,
                        'default_printer' => $sysConfig->default_printer,
                        'print_preview' => $sysConfig->print_preview,
                        'message_template' => $sysConfig->message_template,
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \DateTime(),
                    ]);

                foreach ($sysConfigTrans as $queryData) 
                {             
                    if ($queryData->id > 0)
                    {
                        $rowsTrans = \DB::table('sys_config_transaksi')
                        ->where('id', $queryData->id)
                        ->update([
                            'biaya_transaksi' => $queryData->biaya_transaksi,
                            'biaya_transaksi_percent' => $queryData->biaya_transaksi_percent,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime(),
                        ]);
                    }
                    else
                    {
                        $rowsTrans = \DB::table('sys_config_transaksi')
                            ->insert([
                                'payment_type' => $queryData->payment_type,
                                'biaya_transaksi' => $queryData->biaya_transaksi,
                                'biaya_transaksi_percent' => $queryData->biaya_transaksi_percent,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime(),
                        ]);
                    }
                }

                foreach ($sysConfigSchedule as $scheduleData) 
                {             
                    $rowsTrans = \DB::table('operational_schedule')
                    ->where('id', $scheduleData->id)
                    ->update([
                        'flag' => $scheduleData->flag,
                        'start_time' => $scheduleData->start_time,
                        'end_time' => $scheduleData->end_time,
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \DateTime(),
                    ]);
                }

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
                }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveMasterKompleks(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);
            
        
            if ($num > 0)
            {          
                $records = json_decode($request->get('kompleksParam'));
                $blokData = json_decode($request->get('blokParam'));

                $kompleksData = $records[0];

                if ($p_opType == 1) // INSERT
                {
                    $rows = \DB::table('master_kompleks')
                    ->insertGetId([
                        'kompleks_name' => $kompleksData->kompleks_name,
                        'kompleks_address' => $kompleksData->kompleks_address,
                        'biaya_ipl' => $kompleksData->biaya_ipl,
                        'durasi_pembayaran' => $kompleksData->durasi_pembayaran,
                        'img_kompleks' => $kompleksData->img_kompleks,
                        'is_active' => $kompleksData->is_active,
                        'created_by' => $p_user_id,
                        'creation_date' => new \DateTime()
                    ]);

                    $kompleksData->kompleks_id = $rows;
                }
                else // UPDATE
                {
                    $rows = \DB::table('master_kompleks')
                    ->where('kompleks_id', $kompleksData->kompleks_id)
                    ->update([
                        'kompleks_name' => $kompleksData->kompleks_name,
                        'kompleks_address' => $kompleksData->kompleks_address,
                        'biaya_ipl' => $kompleksData->biaya_ipl,
                        'durasi_pembayaran' => $kompleksData->durasi_pembayaran,
                        'img_kompleks' => $kompleksData->img_kompleks,
                        'is_active' => $kompleksData->is_active,
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \DateTime(),
                    ]);
                }

                foreach ($blokData as $queryData) 
                {             
                    if ($queryData->blok_id > 0)
                    {
                        $rowsTrans = \DB::table('master_blok')
                        ->where('blok_id', $queryData->blok_id)
                        ->update([
                            'blok_name' => $queryData->blok_name,
                            'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime(),
                        ]);
                    }
                    else
                    {
                        $rowsTrans = \DB::table('master_blok')
                            ->insert([
                                'kompleks_id' => $kompleksData->kompleks_id,
                                'blok_name' => $queryData->blok_name,
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime(),
                        ]);
                    }
                }

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
                }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveMasterKavling(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));
                $queryData = $records[0];    

                if ($p_opType == 1) // INSERT
                {
                    $rows = \DB::table('master_kavling')
                            ->insertGetId([
                                'kompleks_id' => $queryData->kompleks_id,
                                'blok_id' => $queryData->blok_id,
                                'house_no' => $queryData->house_no,
                                'biaya_ipl' => $queryData->biaya_ipl,
                                'luas_tanah' => $queryData->luas_tanah,
                                'luas_bangunan' => $queryData->luas_bangunan,
                                'durasi_pembayaran' => $queryData->durasi_pembayaran,
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime()
                            ]);
                }
                else // UPDATE
                {
                    $rows = \DB::table('master_kavling')
                        ->where('kavling_id', $queryData->kavling_id)
                        ->update([
                            'kompleks_id' => $queryData->kompleks_id,
                            'blok_id' => $queryData->blok_id,
                            'house_no' => $queryData->house_no,
                            'biaya_ipl' => $queryData->biaya_ipl,
                            'luas_tanah' => $queryData->luas_tanah,
                            'luas_bangunan' => $queryData->luas_bangunan,
                            'durasi_pembayaran' => $queryData->durasi_pembayaran,
                            'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime()
                        ]);
                }

                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveUserKavling(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -2,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));
                foreach ($records as $queryData) 
                {          
                    if ($queryData->id > 0)
                    {
                        $rows = \DB::table('user_kavling')
                        ->where('id', $queryData->id)
                        ->update([
                            'user_id' => $queryData->user_id,
                            'kavling_id' => $queryData->kavling_id,
                            'tgl_bast' => $queryData->tgl_bast,
                            'bast_created' => $queryData->bast_created,
                            'start_ipl' => $queryData->start_ipl,
                            'remarks' => $queryData->remarks,
                            'user_qr_code' => $queryData->user_qr_code,
                            'user_qr_enabled' => $queryData->user_qr_enabled,
                            'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \Datetime(),
                        ]);
                    }
                    else
                    {
                        $rows = \DB::table('user_kavling')
                        ->insert([
                            'user_id' => $queryData->user_id,
                            'kavling_id' => $queryData->kavling_id,
                            'tgl_bast' => $queryData->tgl_bast,
                            'bast_created' => $queryData->bast_created,
                            'start_ipl' => $queryData->start_ipl,
                            'remarks' => $queryData->remarks,
                            'user_qr_code' => $queryData->user_qr_code,
                            'user_qr_enabled' => $queryData->user_qr_enabled,
                            'is_active' => $queryData->is_active,
                            'created_by' => $p_user_id,
                            'creation_date' => new \DateTime()
                        ]);
                    }
                }

                $recordsUpd = json_decode($request->get('updKavling'));
                foreach ($recordsUpd as $queryUpd) 
                {
                    $rows = \DB::table('master_kavling')
                    ->where('kavling_id', $queryUpd->kavling_id)
                    ->update([
                        'status' => $queryUpd->status,
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \Datetime(),
                    ]);
                }

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveMasterItem(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));
                $queryData = $records[0];    

                if ($p_opType == 1) // INSERT
                {
                    $rows = \DB::table('master_item')
                            ->insertGetId([
                                'item_name' => $queryData->item_name,
                                'unit_id' => $queryData->unit_id,
                                'item_hpp' => $queryData->item_hpp,
                                'item_price' => $queryData->item_price,
                                'qty' => $queryData->qty,
                                'item_jasa' => $queryData->item_jasa,
                                'image_file' => $queryData->image_file,
                                'item_category_id' => $queryData->item_category_id,
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime()
                            ]);
                }
                else // UPDATE
                {
                    $rows = \DB::table('master_item')
                        ->where('item_id', $queryData->item_id)
                        ->update([
                            'item_name' => $queryData->item_name,
                            'unit_id' => $queryData->unit_id,
                            'item_hpp' => $queryData->item_hpp,
                            'item_price' => $queryData->item_price,
                            'qty' => $queryData->qty,
                            'item_jasa' => $queryData->item_jasa,
                            'image_file' => $queryData->image_file,
                            'item_category_id' => $queryData->item_category_id,
                        'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime()
                        ]);
                }

                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveMasterUnit(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));
                $queryData = $records[0];    

                if ($p_opType == 1) // INSERT
                {
                    $rows = \DB::table('master_unit')
                            ->insertGetId([
                                'unit_name' => $queryData->unit_name,                            
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime()
                            ]);
                }
                else // UPDATE
                {
                    $rows = \DB::table('master_unit')
                        ->where('unit_id', $queryData->unit_id)
                        ->update([
                            'unit_name' => $queryData->unit_name,       
                            'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime()
                        ]);
                }

                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveSummaryIPL(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('headerParam'));

                foreach ($records as $headerData) 
                {             
                    if ($p_opType == 1) // INSERT
                    {
                        $rows = \DB::table('transaksi_ipl')
                        ->insertGetId([
                            'type_trans' => $headerData->type_trans,
                            'date_issued' => $headerData->date_issued,
                            'kavling_id' => $headerData->kavling_id,
                            'user_id' => $headerData->user_id,
                            'nominal' => $headerData->nominal,
                            'start_ipl' => $headerData->start_ipl,
                            'end_ipl' => $headerData->end_ipl,
                            'is_active' => $headerData->is_active,
                            'created_by' => $p_user_id,
                            'creation_date' => new \DateTime()
                        ]);
    
                        $headerData->id_trans = $rows;

                        foreach ($headerData->detail_data as $queryData) 
                        {             
                            $rowsTrans = \DB::table('transaksi_ipl_detail')
                            ->insert([
                                'id_trans' => $headerData->id_trans,
                                'nominal' => $queryData->nominal,
                                'periode_pembayaran' => $queryData->periode_pembayaran,
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime(),
                                ]);
                        }
                    }
                    else // UPDATE
                    {
                        $rows = \DB::table('transaksi_ipl')
                        ->where('id_trans', $headerData->id_trans)
                        ->update([
                            'is_active' => $headerData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime(),
                        ]);

                        if ($headerData->is_active == 'N')
                        {
                            $rows = \DB::table('transaksi_payment_manual')
                            ->where('id_trans', $headerData->id_trans)
                            ->update([
                                'is_active' => 'N',
                                'last_updated_by' => $p_user_id,
                                'last_update_date' => new \DateTime()
                            ]);
                        }
                    }
                }

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
                }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function addQty($p_itemID, $p_qty){
        $query = "  
        update
        master_item
        set qty = qty + ".$p_qty."
        where item_id = ".$p_itemID;

        $rows = \DB::update($query);

        return $rows;
    }

    public function reduceQty($p_itemID, $p_qty){
        $query = "  
        update
        master_item
        set qty = qty - ".$p_qty."
        where item_id = ".$p_itemID;

        $rows = \DB::update($query);

        return $rows;
    }

    public function updateQtyRaw($p_itemID, $p_qty){
        $query = "  
        update
        master_item
        set qty = ".$p_qty."
        where item_id = ".$p_itemID;

        $rows = \DB::update($query);

        return $rows;
    }

    public function returnQtyBecauseOfVoid(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_idTrans              = $request->get('p_idTrans');
            $p_transType            = $request->get('p_transType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                if ($p_transType == 1) // TRANSAKSI RETAIL
                {
                    $query = "
                    SELECT td.item_id, td.item_qty 
                    FROM transaksi_retail_detail td, master_item mi 
                    WHERE td.item_id = mi.item_id 
                    AND td.is_active = 'Y' 
                    AND mi.item_jasa = 'N' 
                    AND td.id_trans = ". $p_idTrans;
    
                    $records = \DB::select(\DB::raw($query));
    
                    foreach ($records as $queryData) 
                    {             
                        $rows = $this->addQty($queryData->item_id, $queryData->item_qty);
                    }
                }
                else // PENERIMAAN DATA
                {
                    $query = "
                    SELECT td.item_id, td.item_qty 
                    FROM penerimaan_detail td, master_item mi 
                    WHERE td.item_id = mi.item_id 
                    AND td.is_active = 'Y' 
                    AND mi.item_jasa = 'N' 
                    AND td.penerimaan_id = ". $p_idTrans;

                    $records = \DB::select(\DB::raw($query));
    
                    foreach ($records as $queryData) 
                    {             
                        $rows = $this->reduceQty($queryData->item_id, $queryData->item_qty);
                    }
                }
                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function updateItemQty(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $p_itemID               = $request->get('p_itemID');
            $p_qty                  = $request->get('p_qty');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $rows = $this->addQty($p_itemID, $p_qty);

                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveDataTransaksi(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('headerTransParam'));
                $detailData = json_decode($request->get('detailTransParam'));

                $headerData = $records[0];

                if ($p_opType == 1) // INSERT
                {
                    $expiredDT = Carbon::now();
                    $expiredDT->addHour(12);                             

                    $rows = \DB::table('transaksi_retail')
                    ->insertGetId([
                        'type_trans' => $headerData->type_trans,
                        'date_issued' => $headerData->date_issued,
                        'kavling_id' => $headerData->kavling_id,
                        'user_id' => $headerData->user_id,
                        'nominal' => $headerData->nominal,
                        'date_expired' => $expiredDT,
                        'deliver_status' => $headerData->deliver_status,
                        'is_active' => $headerData->is_active,
                        'created_by' => $p_user_id,
                        'creation_date' => new \DateTime()
                    ]);

                    $headerData->id_trans = $rows;
                }
                else // UPDATE
                {
                    $rows = \DB::table('transaksi_retail')
                    ->where('id_trans', $headerData->id_trans)
                    ->update([
                        'type_trans' => $headerData->type_trans,
                        'date_issued' => $headerData->date_issued,
                        'kavling_id' => $headerData->kavling_id,
                        'user_id' => $headerData->user_id,
                        'nominal' => $headerData->nominal,
                        'deliver_status' => $headerData->deliver_status,
                        'is_active' => $headerData->is_active,
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \DateTime(),
                    ]);
                }

                foreach ($detailData as $queryData) 
                {   
                    if ($queryData->is_active == 'Y')
                    {
                        if ($queryData->item_jasa == 'N')
                            $rowsTrans = $this->reduceQty($queryData->item_id, ($queryData->item_qty - $queryData->old_qty));

                        if ($queryData->id > 0)
                        {
                            $rowsTrans = \DB::table('transaksi_retail_detail')
                            ->where('id', $queryData->id)
                            ->update([
                                'id_trans' => $headerData->id_trans,
                                'item_id' => $queryData->item_id,
                                'item_qty' => $queryData->item_qty,
                                'item_hpp' => $queryData->item_hpp,
                                'item_price' => $queryData->item_price,
                                'subtotal' => $queryData->subtotal,
                                'is_active' => $queryData->is_active,
                                'last_updated_by' => $p_user_id,
                                'last_update_date' => new \DateTime(),
                                ]);
                        }
                        else
                        {
                            $rowsTrans = \DB::table('transaksi_retail_detail')
                            ->insert([
                                'id_trans' => $headerData->id_trans,
                                'item_id' => $queryData->item_id,
                                'item_qty' => $queryData->item_qty,
                                'item_hpp' => $queryData->item_hpp,
                                'item_price' => $queryData->item_price,
                                'subtotal' => $queryData->subtotal,
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime(),
                                ]);
                        }
                    }
                    else
                    {
                        if ($queryData->item_jasa == 'N')
                            $rowsTrans = $this->addQty($queryData->item_id, $queryData->old_qty);

                        $rowsTrans = \DB::table('transaksi_retail_detail')
                            ->where('id', $queryData->id)
                            ->update([
                                'is_active' => $queryData->is_active,
                                'last_updated_by' => $p_user_id,
                                'last_update_date' => new \DateTime(),
                                ]);
                    }          
                }

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
                }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function penyesuaianStok(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));

                $headerData = $records[0];

                $rowsTrans = $this->updateQtyRaw($headerData->item_id, $headerData->qty_baru);

                $rows = \DB::table('penyesuaian_stok')
                ->insertGetId([
                    'item_id' => $headerData->item_id,
                    'qty_awal' => $headerData->qty_awal,
                    'qty_baru' => $headerData->qty_baru,
                    'keterangan' => $headerData->keterangan,
                    'created_by' => $p_user_id,
                    'creation_date' => new \DateTime()
                ]);

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
                }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function savePaymentManual(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records        = json_decode($request->get('payParam'));
                $headerData     = $records[0];

                $rows = \DB::table('transaksi_payment_manual')
                ->insertGetId([
                    'id_trans' => $headerData->id_trans,
                    'trans_type' => $headerData->trans_type,
                    'payment_type' => $headerData->payment_type,
                    'total' => $headerData->total,
                    'date_paid' => $headerData->date_paid,
                    'payment_remark' => $headerData->payment_remark,
                    'created_by' => $p_user_id,
                    'creation_date' => new \DateTime()
                ]);

                if ($headerData->trans_type == "IPL")
                {
                    $rowsTrans = \DB::table('transaksi_ipl')
                    ->where('id_trans', $headerData->id_trans)
                    ->update([
                        'payment_id' => "M" . $rows,       
                        'status_id' => 2,       
                        'date_paid' => $headerData->date_paid,
                        'is_manual_paid' => "Y",       
                        'is_active' => 1,
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \DateTime()
                    ]);
    
                    }
                else
                {
                    $rowsTrans = \DB::table('transaksi_handyman')
                    ->where('id_trans', $headerData->id_trans)
                    ->update([
                        'payment_id' => "M" . $rows,       
                        'status_id' => 2,       
                        'date_paid' => $headerData->date_paid,       
                        'is_manual_paid' => "Y",       
                        'is_active' => 1,
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \DateTime()
                    ]);
    
                }

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
                }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function cancelPaymentManual(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $idTrans              = $request->get('p_idTrans');
                $transType              = $request->get('p_transType');

                $rows = \DB::table('transaksi_payment_manual')
                ->where('id_trans', $idTrans)
                ->update([
                    'is_active' => 'N',
                    'last_updated_by' => $p_user_id,
                    'last_update_date' => new \DateTime()
                ]);

                if ($transType == "IPL")
                {
                    $rowsTrans = \DB::table('transaksi_ipl')
                    ->where('id_trans', $idTrans)
                    ->update([
                        'payment_id' => null,   
                        'date_paid' => null,    
                        'status_id' => 0,       
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \DateTime()
                    ]);
    
                    }
                else
                {
                    $rowsTrans = \DB::table('transaksi_handyman')
                    ->where('id_trans', $idTrans)
                    ->update([
                        'payment_id' => null,       
                        'date_paid' => null,    
                        'status_id' => 0,       
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \DateTime()
                    ]);
    
                }

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
                }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function savePenerimaan(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num                    = $this->checkToken($p_user_id, $p_access_token);
            
            if ($num > 0)
            {
                $json = $request->get('headerTransParam');
                $records = json_decode($json);
                $headerData = $records[0];

                $jsonDetail = $request->get('detailTransParam');
                $detailData = json_decode($jsonDetail);

                if ($p_opType == 1) // INSERT
                {
                    $rows = \DB::table('penerimaan_header')
                    ->insertGetId([
                        'penerimaan_datetime' => $headerData->penerimaan_datetime,
                        'type_trans' => $headerData->type_trans,
                        'penerimaan_total' => $headerData->penerimaan_total,
                        'is_active' => $headerData->is_active,
                        'created_by' => $p_user_id,
                        'creation_date' => new \DateTime()
                    ]);

                    $headerData->penerimaan_id = $rows;
                }
                else // UPDATE
                {
                    $rows = \DB::table('penerimaan_header')
                    ->where('penerimaan_id', $headerData->penerimaan_id)
                    ->update([
                        'penerimaan_datetime' => $headerData->penerimaan_datetime,
                        'type_trans' => $headerData->type_trans,
                        'penerimaan_total' => $headerData->penerimaan_total,
                        'is_active' => $headerData->is_active,
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \DateTime(),
                    ]);
                }

                foreach ($detailData as $queryData) 
                {   
                    if ($queryData->is_active == 'Y')
                    {
                        $rowsTrans = $this->addQty($queryData->item_id, ($queryData->item_qty - $queryData->old_qty));

                        if ($queryData->id > 0)
                        {
                            $rowsTrans = \DB::table('penerimaan_detail')
                            ->where('id', $queryData->id)
                            ->update([
                                'penerimaan_id' => $headerData->penerimaan_id,
                                'item_id' => $queryData->item_id,
                                'item_qty' => $queryData->item_qty,
                                'item_hpp' => $queryData->item_hpp,
                                'item_subtotal' => $queryData->item_subtotal,
                                'is_active' => $queryData->is_active,
                                'last_updated_by' => $p_user_id,
                                'last_update_date' => new \DateTime(),
                                ]);
                        }
                        else
                        {
                            $rowsTrans = \DB::table('penerimaan_detail')
                            ->insert([
                                'penerimaan_id' => $headerData->penerimaan_id,
                                'item_id' => $queryData->item_id,
                                'item_qty' => $queryData->item_qty,
                                'item_hpp' => $queryData->item_hpp,
                                'item_subtotal' => $queryData->item_subtotal,
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime(),
                                ]);
                        }
                    }
                    else
                    {
                        $rowsTrans = $this->reduceQty($queryData->item_id, $queryData->old_qty);

                        $rowsTrans = \DB::table('penerimaan_detail')
                            ->where('id', $queryData->id)
                            ->update([
                                'is_active' => $queryData->is_active,
                                'last_updated_by' => $p_user_id,
                                'last_update_date' => new \DateTime(),
                                ]);
                    }          
                }

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveCustCareDetail(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $json = $request->get('headerTransParam');
                $records = json_decode($json);

                if (json_last_error() == 0 && is_array($records))
                {
                    $headerData = $records[0];

                    $rows = \DB::table('cc_ticket')
                    ->where('ticket_id', $headerData->ticket_id)
                    ->update([
                        'status' => $headerData->status,
                        'status_date' => $headerData->status_date,
                        'status_by' => $headerData->status_by,
                        'mtc_detail' => $headerData->mtc_detail,
                        'mtc_cost' => $headerData->mtc_cost,
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \DateTime()
                    ]);
                }  

                $jsonDetail = $request->get('detailTransParam');
                $detailData = json_decode($jsonDetail);
                $queryData = $detailData[0];    

                if ($queryData->history_id == 0) // INSERT
                {
                    $rows = \DB::table('cc_ticket_history')
                            ->insertGetId([
                                'ticket_id' => $queryData->ticket_id,
                                'sent_by' => $queryData->sent_by,
                                'send_date' => $queryData->send_date,
                                'description' => $queryData->description,
                                'is_mtc' => $queryData->is_mtc,
                                'mtc_cost' => $queryData->mtc_cost,
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime()
                            ]);

                    $queryData->history_id = $rows;
                }
                else // UPDATE
                {
                    $rows = \DB::table('cc_ticket_history')
                        ->where('history_id', $queryData->history_id)
                        ->update([
                            'ticket_id' => $queryData->ticket_id,
                            'sent_by' => $queryData->sent_by,
                            'send_date' => $queryData->send_date,
                            'description' => $queryData->description,
                            'is_mtc' => $queryData->is_mtc,
                            'mtc_cost' => $queryData->mtc_cost,
                            'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime()
                        ]);
                }

                $jsonImg = $request->get('imgAttachmentParam');
                $imgDataList = json_decode($jsonImg);
                foreach ($imgDataList as $imgData) 
                {
                    if ($imgData->ftpFileName != "")
                    {
                        if ($imgData->attach_id == 0) // INSERT
                        {
                            $rows = \DB::table('cc_ticket_history_attach')
                                    ->insertGetId([
                                        'history_id' => $queryData->history_id,
                                        'ticket_id' => $queryData->ticket_id,
                                        'seqno' => $imgData->seqno,
                                        'attach_file' => $imgData->ftpFileName ,
                                        'is_active' => $imgData->is_active,
                                        'created_by' => $p_user_id,
                                        'creation_date' => new \DateTime()
                                    ]);
                        }
                        else // UPDATE
                        {
                            $rows = \DB::table('cc_ticket_history_attach')
                                ->where('attach_id', $imgData->attach_id)
                                ->update([
                                    'history_id' => $queryData->history_id,
                                    'ticket_id' => $queryData->ticket_id,
                                    'seqno' => $imgData->seqno,
                                    'attach_file' => $imgData->ftpFileName ,
                                    'is_active' => $imgData->is_active,
                                    'last_updated_by' => $p_user_id,
                                    'last_update_date' => new \DateTime()
                            ]);
                        } 
                    }   
                }

                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function updateCustCare(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));
                $queryData = $records[0];    

                $rows = \DB::table('cc_ticket')
                ->where('ticket_id', $queryData->ticket_id)
                ->update([
                    'status' => $queryData->status,
                    'status_date' => $queryData->status_date,
                    'status_by' => $queryData->status_by,
                    'is_closed' => $queryData->is_closed,
                    'is_active' => $queryData->is_active,
                    'last_updated_by' => $p_user_id,
                    'last_update_date' => new \DateTime()
                ]);

                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function resetExpiredTrans(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $query = $request->get('sqlQuery');
                $data = \DB::select(\DB::raw($query));

                foreach ($data as $transData)
                {
                    $rows = \DB::table('transaksi_retail')
                    ->where('id_trans', $transData->id_trans)
                    ->update([
                        'is_active' => 'N',
                        'last_updated_by' => 0,
                        'last_update_date' => new \DateTime()
                    ]);
   
                    $queryDetail = "  
                    SELECT
                    td.item_id, td.item_qty
                    FROM transaksi_retail_detail td, master_item mi
                    where td.id_trans = ". $transData->id_trans . "
                    AND td.item_id = mi.item_id 
                    AND mi.item_jasa = 'N'";
                    
                    $itemData = \DB::select(\DB::raw($queryDetail));
                    foreach ($itemData as $transDetailData)
                    {
                        $detailProc = $this->addQty($transDetailData->item_id, $transDetailData->item_qty);
                    }
                } 

                \DB::commit();
      
                $result = response()->json([[
                        'data' => 1, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }
 
    public function saveNews(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));
                $queryData = $records[0];    

                if ($p_opType == 1) // INSERT
                {
                    if ($queryData->is_expired == "Y")
                    {
                        $rows = \DB::table('news')
                        ->insertGetId([
                            'news_type' => $queryData->news_type,
                            'subject' => $queryData->subject,
                            'description' => $queryData->description,
                            'file_name' => $queryData->file_name,
                            'file_name_fullscreen' => $queryData->file_name_fullscreen,
                            'start_date' => $queryData->start_date,
                            'end_date' => $queryData->end_date,
                            'is_banner' => $queryData->is_banner,
                            'is_active' => $queryData->is_active,
                            'created_by' => $p_user_id,
                            'creation_date' => new \DateTime()
                        ]);
                    }
                    else
                    {
                        $rows = \DB::table('news')
                        ->insertGetId([
                            'news_type' => $queryData->news_type,
                            'subject' => $queryData->subject,
                            'description' => $queryData->description,                      
                            'file_name' => $queryData->file_name,
                            'file_name_fullscreen' => $queryData->file_name_fullscreen,
                            'is_banner' => $queryData->is_banner,
                            'is_active' => $queryData->is_active,
                            'created_by' => $p_user_id,
                            'creation_date' => new \DateTime()
                        ]);
                    }
                }
                else // UPDATE
                {
                    if ($queryData->is_expired == "Y")
                    {
                        $rows = \DB::table('news')
                        ->where('news_id', $queryData->news_id)
                        ->update([
                            'news_type' => $queryData->news_type,
                            'subject' => $queryData->subject,
                            'description' => $queryData->description,
                            'start_date' => $queryData->start_date,
                            'end_date' => $queryData->end_date,
                            'file_name' => $queryData->file_name,
                            'file_name_fullscreen' => $queryData->file_name_fullscreen,
                            'is_banner' => $queryData->is_banner,
                            'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime()
                        ]);
                    }
                    else
                    {
                        $rows = \DB::table('news')
                        ->where('news_id', $queryData->news_id)
                        ->update([
                            'news_type' => $queryData->news_type,
                            'subject' => $queryData->subject,
                            'description' => $queryData->description,
                            'start_date' => null,
                            'end_date' => null,
                            'file_name' => $queryData->file_name,
                            'file_name_fullscreen' => $queryData->file_name_fullscreen,
                            'is_banner' => $queryData->is_banner,
                            'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime()
                        ]);
                    }
                }

                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function clearTagihan($userID, $kavlingID){
        $query = "  
        update
        transaksi_ipl
        set is_active = 'N',
        last_updated_by = ".$p_user_id.", 
        last_update_date = ".new \DateTime()."
        where user_id = ".$userID."
        and kavling_id = ".$kavlingID;

        $rows = \DB::update($query);

        return $rows;
    }

    public function clearTagihanIPL(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $p_user_kavling_id  = $request->get('p_user_kavling_id');
                $p_kavling_id       = $request->get('p_kavling_id');

                $rows = \DB::table('transaksi_ipl')
                ->where([
                    ['user_id', '=', $p_user_kavling_id],
                    ['kavling_id', '=', $p_kavling_id]
                ])
                ->update([
                    'is_active' => 'N',
                    'last_updated_by' => $p_user_id,
                    'last_update_date' => new \DateTime()
                ]);

                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveUserKavlingRFID(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));
                $queryData = $records[0];    

                foreach ($records as $queryData) 
                {
                    if ($queryData->id == 0) // INSERT
                    {
                        $rows = \DB::table('user_kavling_rfid')
                                ->insertGetId([
                                    'user_kavling_id' => $queryData->user_kavling_id,
                                    'rfid_value' => $queryData->rfid_value,
                                    'rfid_enabled' => $queryData->rfid_enabled,
                                    'is_active' => $queryData->is_active,
                                    'created_by' => $p_user_id,
                                    'creation_date' => new \DateTime()
                                ]);
    
                        $queryData->id = $rows;
                    }
                    else // UPDATE
                    {
                        $rows = \DB::table('user_kavling_rfid')
                            ->where('id', $queryData->id)
                            ->update([
                                'user_kavling_id' => $queryData->user_kavling_id,
                                'rfid_value' => $queryData->rfid_value,
                                'rfid_enabled' => $queryData->rfid_enabled,
                                'is_active' => $queryData->is_active,
                                'last_updated_by' => $p_user_id,
                                'last_update_date' => new \DateTime()
                            ]);
                    }
                }
                
                \DB::commit();
    
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveKompleksTerminal(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));
                $queryData = $records[0];    

                foreach ($records as $queryData) 
                {
                    if ($queryData->id == 0) // INSERT
                    {
                        $rows = \DB::table('kompleks_terminal')
                                ->insertGetId([
                                    'kompleks_id' => $queryData->kompleks_id,
                                    'terminal_id' => $queryData->terminal_id,
                                    'terminal_type' => $queryData->terminal_type,
                                    'is_active' => $queryData->is_active,
                                    'created_by' => $p_user_id,
                                    'creation_date' => new \DateTime()
                                ]);
    
                        $queryData->id = $rows;
                    }
                    else // UPDATE
                    {
                        $rows = \DB::table('kompleks_terminal')
                            ->where('id', $queryData->id)
                            ->update([
                                'kompleks_id' => $queryData->kompleks_id,
                                'terminal_id' => $queryData->terminal_id,
                                'terminal_type' => $queryData->terminal_type,
                                'is_active' => $queryData->is_active,
                                'last_updated_by' => $p_user_id,
                                'last_update_date' => new \DateTime()
                            ]);
                    }
                }
                
                \DB::commit();
    
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveSatpamAkses(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));
                foreach ($records as $queryData) {
                    if ($queryData->id == 0) // INSERT
                    {
                        $rows = \DB::table('satpam_access_list')
                                ->insertGetId([
                                    'kompleks_id' => $queryData->kompleks_id,
                                    'user_full_name' => $queryData->user_full_name,
                                    'access_id' => $queryData->access_id,
                                    'access_type' => $queryData->access_type,
                                    'is_active' => $queryData->is_active,
                                    'created_by' => $p_user_id,
                                    'creation_date' => new \DateTime()
                                ]);
    
                        $queryData->history_id = $rows;
                    }
                    else // UPDATE
                    {
                        $rows = \DB::table('satpam_access_list')
                            ->where('id', $queryData->id)
                            ->update([
                                'kompleks_id' => $queryData->kompleks_id,
                                'user_full_name' => $queryData->user_full_name,
                                'access_id' => $queryData->access_id,
                                'access_type' => $queryData->access_type,
                                'is_active' => $queryData->is_active,
                                'last_updated_by' => $p_user_id,
                                'last_update_date' => new \DateTime()
                            ]);
                    }
                }

                
                \DB::commit();
    
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function deleteSatpamAkses(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $p_access_id  = $request->get('p_access_id');
                $rows = \DB::table('satpam_access_list')
                ->where('id', $p_access_id)
                ->delete();
                
                \DB::commit();
    
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function getUserAccessListID($userID, $kompleksID, $accessID, $accessType){
        try
        {
            $query = "  
            select  count(1) as id
            from    user_access_list ut
            where   ut.user_id = ".$userID." 
                    and ut.kompleks_id = ". $kompleksID ." 
                    and ut.access_id = '". $accessID ."'   
                    and ut.access_type = '". $accessType ."'";

            $result = \DB::select(\DB::raw($query));

            if ($result[0]->id > 0)
            {
                $query = "  
                select  id
                from    user_access_list ut
                where   ut.user_id = ".$userID." 
                        and ut.kompleks_id = ". $kompleksID ." 
                        and ut.access_id = '". $accessID ."'   
                        and ut.access_type = '". $accessType ."'";
    
                $result = \DB::select(\DB::raw($query));
                return $result[0]->id;
            }
            else
            {
                return 0;
            }
        }
        catch(Exception $e) 
        { 
            return 0;
        }
    }

    public function saveUserAccessList(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $jsonDetail = $request->get('records');
                $detailData = json_decode($jsonDetail);

                if (is_array($detailData) || is_object($detailData))
                {
                    foreach ($detailData as $queryData) 
                    {
                        $idEntry = $this->getUserAccessListID($queryData->user_id, $queryData->kompleks_id, 
                        $queryData->access_id, $queryData->access_type);
    
                        if ($idEntry == 0) // INSERT
                        {
                            $rows = \DB::table('user_access_list')
                            ->insertGetId([
                                'user_id' => $queryData->user_id,
                                'kompleks_id' => $queryData->kompleks_id,
                                'kavling_id' => $queryData->kavling_id,
                                'access_id' => $queryData->access_id,
                                'access_type' => $queryData->access_type,
                                'access_status' => $queryData->access_status,
                                'message' => $queryData->message,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime()
                            ]);
    
                            $queryData->history_id = $rows;
                        }    
                        else
                        {
                            $rows = \DB::table('user_access_list')
                            ->where('id', $idEntry)
                            ->update([
                                'user_id' => $queryData->user_id,
                                'kompleks_id' => $queryData->kompleks_id,
                                'kavling_id' => $queryData->kavling_id,
                                'access_id' => $queryData->access_id,
                                'access_type' => $queryData->access_type,
                                'access_status' => $queryData->access_status,
                                'message' => $queryData->message,
                                'is_active' => $queryData->is_active,
                                'last_updated_by' => $p_user_id,
                                'last_update_date' => new \DateTime()
                            ]);
                        }
                    }                  
                }
                
                \DB::commit();
    
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function generateDummyData(){
                // DUMMY DATA
                //=========================================================================
                $responseResult = array();
                $response = new emp();          
                $response->Status = 1;    
                $response->CardNo = "";    
                $response->FullName = "Widarto Juni Hartono";    
                $response->UTC = gmdate("d-m-Y H:i:s");    
                $response->Message = "Welcome...";    
                $responseResult[] = $response;
    
                $response = new emp();          
                $response->Status = 1;    
                $response->CardNo = null;    
                $response->FullName = "JANUAR RACHMANTO SE";    
                $response->UTC =gmdate("d-m-Y H:i:s"); 
                $response->Message = "Welcome...";    
                $responseResult[] = $response;
    
                $response = new emp();          
                $response->Status = 1;    
                $response->CardNo = null;    
                $response->FullName = "JULY";    
                $response->UTC = gmdate("d-m-Y H:i:s"); 
                $response->Message = "Welcome...";    
                $responseResult[] = $response;
    
                $response = new emp();          
                $response->Status = 1;    
                $response->CardNo = null;    
                $response->FullName = "Bejo";    
                $response->UTC = gmdate("d-m-Y H:i:s");  
                $response->Message = "Welcome...";    
                $responseResult[] = $response;
    
                $response = new emp();          
                $response->Status = 1;    
                $response->CardNo = "2653652367";    
                $response->FullName = "Wibisono Eko Putro";    
                $response->UTC = gmdate("d-m-Y H:i:s");
                $response->Message = "Welcome...";    
                $responseResult[] = $response;
                //=========================================================================     
                
                return $responseResult;                
    }

    public function dropCreateTempTable(){
        $result = 1;
        try{
            $queryDrop = "DROP TABLE IF EXISTS `user_access_temp`";    
            $rows = \DB::update($queryDrop);   
    
            $queryTable ="CREATE TABLE user_access_temp (
                id bigint(10) unsigned NOT NULL AUTO_INCREMENT,
                user_full_name varchar(100) DEFAULT '',
                access_id varchar(100) DEFAULT '',
                access_type enum('QR','RFID') DEFAULT 'QR',
                access_status tinyint(3) DEFAULT '1',
                message varchar(100) DEFAULT 'Welcome...',
                PRIMARY KEY (id)
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1"; 
        
            $rows = \DB::update($queryTable);                
            }
        catch (Exception $e)
        {
            $result = 0;
        }

        return $result;
    }

    public function dropCreateTempTableAll(){
        $result = 1;
        try{
            $queryDrop = "DROP TABLE IF EXISTS `user_access_temp_all`";    
            $rows = \DB::update($queryDrop);   
    
            $queryTable ="CREATE TABLE user_access_temp_all (
                id bigint(10) unsigned NOT NULL AUTO_INCREMENT,
                user_full_name varchar(100) DEFAULT '',
                terminal_id varchar(50) DEFAULT '',
                access_id varchar(100) DEFAULT '',
                access_type enum('QR','RFID') DEFAULT 'QR',
                access_status tinyint(3) DEFAULT '1',
                message varchar(100) DEFAULT 'Welcome...',
                PRIMARY KEY (id)
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1"; 
        
            $rows = \DB::update($queryTable);                
            }
        catch (Exception $e)
        {
            $result = 0;
        }

        return $result;
    }

    public function saveSyncLog($terminalID, $accessType){
        $result = 1;    
        try
        {
            $rows = \DB::table('sync_log')
                ->insertGetId([
                    'terminal_id' => $terminalID,
                    'access_type' => $accessType,
                    'sync_datetime' => new \DateTime()
                ]);         

            \DB::commit();   
        }
        catch(Exception $e)
        {
            $result = 0;
        }

        return $result;
    }

    public function fillInTempTable($terminalID, $accessType){
        $result = 1;    
        try
        {
            // SATPAM
            //$query = "  
            //select  sa.access_id, sa.user_full_name
            //from    satpam_access_list sa, kompleks_terminal kt, master_kompleks mk
            //where   sa.kompleks_id = kt.kompleks_id 
            //        and kt.kompleks_id = mk.kompleks_id
            //        and mk.is_active = 'Y'
            //        and kt.is_active = 'Y'
            //        and kt.terminal_id = '". $terminalID ."' 
            //        and sa.access_type = '". $accessType ."' 
            //        and sa.is_active = 'Y'";
            //$data = \DB::select(\DB::raw($query));
        
            //foreach ($data as $queryData) {       
            //    $rows = \DB::table('user_access_temp')
            //    ->insertGetId([
            //        'user_full_name' => $queryData->user_full_name,
            //        'access_id' => $queryData->access_id,
            //        'access_type' => $accessType,
            //        'access_status' => 1,
            //        'message' => "Welcome..."
            //    ]);         
            //}

            // STAFF
            $query = "  
            select  sa.employee_qr_code, sa.employee_full_name
            from    employee_login_data sa, employee_access_list eal, kompleks_terminal kt, master_kompleks mk
            where   eal.kompleks_id = kt.kompleks_id 
                    and eal.employee_id = sa.employee_id
                    and kt.kompleks_id = mk.kompleks_id
                    and mk.is_active = 'Y'
                    and kt.is_active = 'Y'
                    and kt.terminal_id = '". $terminalID ."' 
                    and eal.is_active = 'Y' 
                    and sa.is_active = 'Y'";
            $data = \DB::select(\DB::raw($query));
        
            foreach ($data as $queryData) {       
                $rows = \DB::table('user_access_temp')
                ->insertGetId([
                    'user_full_name' => $queryData->employee_full_name,
                    'access_id' => $queryData->employee_qr_code,
                    'access_type' => $accessType,
                    'access_status' => 1,
                    'message' => "Welcome..."
                ]);         
            }

            if ($accessType == "QR")
            {
                $query = "
                    select uk.user_id, if(uk.user_qr_enabled = 'Y', 1, 3) as access_status, uk.user_qr_code as access_id, 
                    if(uk.user_qr_enabled = 'Y', 'Welcome....', 'QR Blocked.....') as message, ul.user_full_name
                    from user_kavling uk, 
                         user_login_data ul,
                         master_kavling mk,
                         kompleks_terminal kt
                    where uk.is_active = 'Y'
                    and uk.user_id = ul.user_id
                    and uk.kavling_id = mk.kavling_id
                    and mk.kompleks_id = kt.kompleks_id
                    and kt.terminal_id = '". $terminalID . "' 
                    and kt.is_active = 'Y'
                    and mk.is_active = 'Y'
                    and ul.is_active = 'Y'
                ";
            }
            else
            {
                $query = "
                select uk.user_id, if(ukr.rfid_enabled = 'Y', 1, 3) as access_status, ukr.rfid_value as access_id, 
                if(ukr.rfid_enabled = 'Y', 'Welcome....', 'RFID Blocked.....') as message, ul.user_full_name
                from user_kavling uk, 
                     user_kavling_rfid ukr,
                     user_login_data ul,
                     master_kavling mk,
                     kompleks_terminal kt
                where ukr.is_active = 'Y'
                and ukr.user_kavling_id = uk.id
                and uk.is_active = 'Y'
                and uk.user_id = ul.user_id
                and uk.kavling_id = mk.kavling_id
                and mk.kompleks_id = kt.kompleks_id
                and kt.terminal_id = '". $terminalID . "' 
                and kt.is_active = 'Y'
                and mk.is_active = 'Y'
                and ul.is_active = 'Y'
            ";
            }
                    
            $data = \DB::select(\DB::raw($query));

            foreach ($data as $queryData) {       
                if ($queryData->access_status == 1)
                {
                    $userID = $queryData->user_id;
                    $sqlComm = "
                                select count(1) as numRec
                                from transaksi_ipl ipl,
                                master_kavling mk,
                                kompleks_terminal kt
                                where ipl.user_id = " . $userID . "
                                and ipl.status_id <> 2 
                                and ipl.is_active = 'Y'  
                                and ipl.start_ipl <= NOW() 
                                and ipl.kavling_id = mk.kavling_id
                                and mk.kompleks_id = kt.kompleks_id
                                and kt.terminal_id = '". $terminalID . "'
                                ";
                    $data = \DB::select(\DB::raw($sqlComm));

                    if ($data[0]->numRec > 0)
                    {
                        // ada outstanding
                        $queryData->access_status = 2;
                        $queryData->message = "IPL Terlambat.....";
                    }
                    else
                    {
                        $sqlCountRec = "
                        select count(1) as numRec 
                        from transaksi_ipl ipl
                        where ipl.user_id = " . $userID ."
                        and ipl.is_active = 'Y'";
                        $numRec = \DB::select(\DB::raw($sqlCountRec));
                        $userCount = $numRec[0]->numRec;

                        if ($userCount <=0)
                        {
                            $sqlComm = "
                            select uk.bast_created 
                            from user_kavling uk, 
                                master_kavling mk, 
                                kompleks_terminal kt
                            where uk.user_id = " . $userID . "
                            and uk.is_active = 'Y'
                            and uk.kavling_id = mk.kavling_id 
                            and kt.kompleks_id = mk.kompleks_id
                            and kt.terminal_id = '". $terminalID . "'";                          
                            $bastFlag = \DB::select(\DB::raw($sqlComm));

                            if ($bastFlag[0]->bast_created == 'Y')
                            {
                                // belum ada tagihan tapi sudah ada bast
                                $queryData->access_status = 2;
                                $queryData->message = "User belum aktif.....";
                            }
                            else
                            {
                                $queryData->message = "User baru......";
                            }
                        }
                        else
                        {
                            $sqlComm = "
                            select datediff(now(), max(end_ipl)) as endDate 
                            from transaksi_ipl ipl, 
                                master_kavling mk, 
                                kompleks_terminal kt
                            where ipl.user_id = " . $userID . "
                            and ipl.status_id = 2 
                            and ipl.is_active = 'Y'
                            and ipl.kavling_id = mk.kavling_id 
                            and kt.kompleks_id = mk.kompleks_id
                            and kt.terminal_id = '". $terminalID . "'";
    
                            $endIPL = \DB::select(\DB::raw($sqlComm));
    
                            $diff = $endIPL[0]->endDate;
    
                            if ($diff > 1)
                            {
                                // ada outstanding
                                $queryData->access_status = 2;
                                $queryData->message = "Tagihan IPL terbaru belum ada.....";
                            }
                        }    
                    }
                }

                $rows = \DB::table('user_access_temp')
                ->insertGetId([
                    'user_full_name' => $queryData->user_full_name,
                    'access_id' => $queryData->access_id,
                    'access_type' => $accessType,
                    'access_status' => $queryData->access_status,
                    'message' => $queryData->message
                ]);         
            }

            \DB::commit();   
        }
        catch(Exception $e)
        {
            $result = 0;
        }

        return $result;
    }

    public function fillInTempTableAll(){
        $result = 1;    
        try
        {
            // STAFF
            $query = "  
            select  kt.terminal_id, sa.employee_qr_code, sa.employee_full_name
            from    employee_login_data sa, employee_access_list eal, kompleks_terminal kt, master_kompleks mk
            where   eal.kompleks_id = kt.kompleks_id 
                    and eal.employee_id = sa.employee_id
                    and kt.kompleks_id = mk.kompleks_id
                    and mk.is_active = 'Y'
                    and kt.is_active = 'Y'
                    and eal.is_active = 'Y' 
                    and sa.is_active = 'Y'";
            $data = \DB::select(\DB::raw($query));
        
            foreach ($data as $queryData) {       
                $rows = \DB::table('user_access_temp_all')
                ->insertGetId([
                    'user_full_name' => $queryData->employee_full_name,
                    'terminal_id' => $queryData->terminal_id,
                    'access_id' => $queryData->employee_qr_code,
                    'access_type' => "QR",
                    'access_status' => 1,
                    'message' => "Welcome..."
                ]);         
            }

            $query = "
            select kt.terminal_id,'QR' as access_type, uk.user_id, if(uk.user_qr_enabled = 'Y', 1, 3) as access_status, uk.user_qr_code as access_id, 
            if(uk.user_qr_enabled = 'Y', 'Welcome....', 'QR Blocked.....') as message, ul.user_full_name
            from user_kavling uk, 
                 user_login_data ul,
                 master_kavling mk,
                 kompleks_terminal kt
            where uk.is_active = 'Y'
            and uk.user_id = ul.user_id
            and uk.kavling_id = mk.kavling_id
            and mk.kompleks_id = kt.kompleks_id
           and kt.is_active = 'Y'
            and mk.is_active = 'Y'
            and ul.is_active = 'Y'
            UNION ALL
            select kt.terminal_id,'RFID' as access_type, uk.user_id, if(ukr.rfid_enabled = 'Y', 1, 3) as access_status, ukr.rfid_value as access_id, 
            if(ukr.rfid_enabled = 'Y', 'Welcome....', 'RFID Blocked.....') as message, ul.user_full_name
            from user_kavling uk, 
                 user_kavling_rfid ukr,
                 user_login_data ul,
                 master_kavling mk,
                 kompleks_terminal kt
            where ukr.is_active = 'Y'
            and ukr.user_kavling_id = uk.id
            and uk.is_active = 'Y'
            and uk.user_id = ul.user_id
            and uk.kavling_id = mk.kavling_id
            and mk.kompleks_id = kt.kompleks_id
            and kt.is_active = 'Y'
            and mk.is_active = 'Y'
            and ul.is_active = 'Y'
            ";
                    
            $data = \DB::select(\DB::raw($query));

            foreach ($data as $queryData) {       
                if ($queryData->access_status == 1)
                {
                    $userID = $queryData->user_id;
                    $terminalID = $queryData->terminal_id;

                    $sqlComm = "
                                select count(1) as numRec
                                from transaksi_ipl ipl,
                                master_kavling mk,
                                kompleks_terminal kt
                                where ipl.user_id = " . $userID . "
                                and ipl.status_id <> 2 
                                and ipl.is_active = 'Y'  
                                and ipl.start_ipl <= NOW() 
                                and ipl.kavling_id = mk.kavling_id
                                and mk.kompleks_id = kt.kompleks_id
                                and kt.terminal_id = '". $terminalID . "'
                                ";
                    $data = \DB::select(\DB::raw($sqlComm));

                    if ($data[0]->numRec > 0)
                    {
                        // ada outstanding
                        $queryData->access_status = 2;
                        $queryData->message = "IPL Terlambat.....";
                    }
                    else
                    {
                        $sqlCountRec = "
                        select count(1) as numRec 
                        from transaksi_ipl ipl
                        where ipl.user_id = " . $userID ."
                        and ipl.is_active = 'Y'";
                        $numRec = \DB::select(\DB::raw($sqlCountRec));
                        $userCount = $numRec[0]->numRec;

                        if ($userCount <=0)
                        {
                            $sqlComm = "
                            select uk.bast_created 
                            from user_kavling uk, 
                                master_kavling mk, 
                                kompleks_terminal kt
                            where uk.user_id = " . $userID . "
                            and uk.is_active = 'Y'
                            and uk.kavling_id = mk.kavling_id 
                            and kt.kompleks_id = mk.kompleks_id
                            and kt.terminal_id = '". $terminalID . "'";                          
                            $bastFlag = \DB::select(\DB::raw($sqlComm));

                            if ($bastFlag[0]->bast_created == 'Y')
                            {
                                // belum ada tagihan tapi sudah ada bast
                                $queryData->access_status = 2;
                                $queryData->message = "User belum aktif.....";
                            }
                            else
                            {
                                $queryData->message = "User baru......";
                            }
                        }
                        else{
                            $sqlComm = "
                            select datediff(now(), max(end_ipl)) as endDate 
                            from transaksi_ipl ipl, 
                                master_kavling mk, 
                                kompleks_terminal kt
                            where ipl.user_id = " . $userID . "
                            and ipl.status_id = 2 
                            and ipl.is_active = 'Y'
                            and ipl.kavling_id = mk.kavling_id 
                            and kt.kompleks_id = mk.kompleks_id
                            and kt.terminal_id = '". $terminalID . "'";
    
                            $endIPL = \DB::select(\DB::raw($sqlComm));
    
                            $diff = $endIPL[0]->endDate;
    
                            if ($diff > 1)
                            {
                                // ada outstanding
                                $queryData->access_status = 2;
                                $queryData->message = "Tagihan IPL terbaru belum ada.....";
                            }
                        }    
                    }
                }

                $rows = \DB::table('user_access_temp_all')
                ->insertGetId([
                    'user_full_name' => $queryData->user_full_name,
                    'terminal_id' => $queryData->terminal_id,
                    'access_id' => $queryData->access_id,
                    'access_type' => $queryData->access_type,
                    'access_status' => $queryData->access_status,
                    'message' => $queryData->message
                ]);         
            }

            \DB::commit();   
        }
        catch(Exception $e)
        {
            $result = 0;
        }

        return $result;
    }

    public function getUserList(Request $request){
        $validation = Validator::make($request->all(),[ 
            'token'        => 'required',
        ]);

        if($validation->fails()){
            $errResponse = new emp();	
            $errResponse->Status = 0;
            $errResponse->CardNo = null;
            $errResponse->FullName = "Unknown";
            $errResponse->UTC = gmdate("d-m-Y H:i:s");
            $errResponse->Message = "Failed [vl]!";
            $result = json_encode($errResponse);

            return $result;
        }

        $token = $request->get('token');
        $accessType = $request->get('accessType');
        $terminalID = $request->get('terminalID');
        $seqNo =  $request->get('seqNo');

        $maxRecord = 5;//20;

        $tokenValid = $this->checkExternalToken($token);
        if ($tokenValid == 1)
        {
            if ($seqNo == 1)
            {
                $this->dropCreateTempTable();              
                $this->fillInTempTable($terminalID, $accessType);    
                $this->saveSyncLog($terminalID, $accessType);    
            }

            try 
            {    
                $responseResult = array();

                $query = "select * from user_access_temp";

                $data = \DB::select(\DB::raw($query));
                $recordCount = count($data);

                $i = 0;
                foreach ($data as $queryData) {                
                    if ($i >= (($seqNo-1) * $maxRecord))
                    {
                        $response = new emp();          
                        $response->Status = intval($queryData->access_status);    
                        $response->CardNo = $queryData->access_id;    
                        $response->FullName = $queryData->user_full_name;    
                        $response->UTC = gmdate("d-m-Y H:i:s");    
                        $response->Message =  $queryData->message;    
                        $response->recordCount = $recordCount;
                        $responseResult[] = $response;        
                    }

                    $i+=1;
                    if ($i >= ($seqNo * $maxRecord))
                    {
                        break;
                    }
                }

                $result = json_encode($responseResult);
            }
            catch (Exception $e) 
            {
                $errResponse = new emp();	
                $errResponse->Status = 0;
                $errResponse->CardNo = null;
                $errResponse->FullName = "Unknown";
                $errResponse->UTC = gmdate("d-m-Y H:i:s");
                $errResponse->Message = "Failed [tc]!";
                $response->recordCount = 0;
                $result = json_encode($errResponse);
            }    
        }   
        else
        {
            $errResponse = new emp();	
            $errResponse->Status = 0;
            $errResponse->CardNo = null;
            $errResponse->FullName = "Unknown";
            $errResponse->UTC = gmdate("d-m-Y H:i:s");
            $errResponse->Message = "Failed [tk]!";
            $response->recordCount = 0;
            $result = json_encode($errResponse);
        } 

        return $result;
    }

    public function realtimeData(Request $request){
        $errResponse = new returnStatus();
        $errResponse->Status = 0;
        
        $validation = Validator::make($request->all(),[ 
            'token'        => 'required',
        ]);

        if($validation->fails()){
            $result = json_encode($errResponse);
            return $result;
        }

        $token = $request->get('token');
        $accessStatus = $request->get('Status');
        $userFullName =  $request->get('FullName');
        $accessID = $request->get('CardNo');
        $terminalID = $request->get('terminalID');
        $timeutc = $request->get('UTC');

        date_default_timezone_set('Asia/Jakarta');
        $time1 = strtotime($timeutc.' UTC');
        $timeStamp = date("Y-m-d H:i:s", $time1);
        
        $tokenValid = $this->checkExternalToken($token);
        if ($tokenValid == 1)
        {
            try 
            {    
                $query = "select kompleks_id from kompleks_terminal where terminal_id = '". $terminalID ."'";
                $data = \DB::select(\DB::raw($query));

                $rows = \DB::table('access_log')
                ->insertGetId([
                    'access_status' => $accessStatus,
                    'user_full_name' => $userFullName,
                    'access_id' => $accessID,
                    'terminal_id' => $terminalID,
                    'kompleks_id' => $data[0]->kompleks_id,
                    'timestamp' => $timeStamp ,
                    'creation_date' => new \DateTime()
                ]);   

                \DB::commit();   

                $errResponse->Status = 1;
            }
            catch (Exception $e) 
            {
            }    
        }   

        $result = json_encode($errResponse);
        return $result;
    }

    public function displaySatpam(Request $request){
        $errResponse = new returnStatus();
        $errResponse->Status = 0;

        $validation = Validator::make($request->all(),[ 
            'token'        => 'required',
        ]);

        if($validation->fails()){
            $result = json_encode($errResponse);
            return $result;
        }

        $token = $request->get('token');
        $accessStatus = $request->get('Status');
        $userFullName =  $request->get('FullName');
        $accessID = $request->get('CardNo');
        $timeutc = $request->get('UTC');

        date_default_timezone_set('Asia/Jakarta');
        $time1 = strtotime($timeutc.' UTC');
        $timeStamp = date("Y-m-d H:i:s", $time1);
        
        $tokenValid = $this->checkExternalToken($token);
        if ($tokenValid == 1)
        {
            try 
            {    
                $rows = \DB::table('satpam_log')
                ->insertGetId([
                    'access_status' => $accessStatus,
                    'user_full_name' => $userFullName,
                    'access_id' => $accessID,
                    'timestamp' => $timeStamp ,
                    'creation_date' => new \DateTime()
                ]);   

                \DB::commit();   

                $errResponse->Status = 1;
            }
            catch (Exception $e) 
            {
            }    
        }   

        $result = json_encode($errResponse);
        return $result;
    }

    public function updateStatusKavling(Request $request){
        $errResponse = new returnStatus_Update();
        $errResponse->Status = 0;
        $errResponse->Message = '';

        $validation = Validator::make($request->all(),[ 
            'token'        => 'required',
        ]);

        if($validation->fails()){
            $errResponse->Message = "Token not found";
            $result = json_encode($errResponse);
            return $result;
        }

        $token = $request->get('token');
        $kavlingID = $request->get('kavling_id');
        $statusSewa =  $request->get('status_sewa');
        $statusJual = $request->get('status_jual');
        $urlLink = $request->get('url_link');

        // if ($statusSewa > 2 || $statusJual > 2){
        //     $errResponse->Message = "Input salah";
        //     $result = json_encode($errResponse);
        //     return $result;
        // }
        
        $tokenValid = $this->checkExternalToken($token);
        if ($tokenValid == 1)
        {
            try 
            { 
                if ($statusSewa == 1 || $statusSewa == 2)
                {
                    if ($statusSewa == 2)
                        $urlSewa = $urlLink;
                    else
                        $urlSewa = '';    

                    $rows = \DB::table('master_kavling')
                    ->where('kavling_id', $kavlingID)
                    ->update([
                        'rent_status' => $statusSewa,
                        'rent_url' => $urlSewa,
                        'last_updated_by' => 0,
                        'last_update_date' => new \DateTime()
                    ]);
                }   
                else if ($statusJual == 1 || $statusJual == 2)
                {
                    if ($statusJual == 2)
                        $urlJual = $urlLink;
                    else
                        $urlJual = '';    

                    $rows = \DB::table('master_kavling')
                    ->where('kavling_id', $kavlingID)
                    ->update([
                        'sell_status' => $statusJual,
                        'sell_url' => $urlJual,
                        'last_updated_by' => 0,
                        'last_update_date' => new \DateTime()
                    ]);
                }            

                \DB::commit();   

                $errResponse->Status = 1;
            }
            catch (Exception $e) 
            {
            }    
        }   

        $result = json_encode($errResponse);
        return $result;
    }

    public function saveEmployee(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $loginData = json_decode($request->get('p_loginData'));
                $accessData = json_decode($request->get('p_accessData'));
            
                $recordsToken = json_decode($request->get('records_token'));
                $queryDataToken = $recordsToken[0];    
                
                // SAVE HEADER
                if ($p_opType == 1) // INSERT
                {
                    $rows = \DB::table('employee_login_data')
                            ->insertGetId([
                                'employee_user_name' => $loginData->employee_user_name,
                                'employee_password' => $loginData->employee_password,
                                'employee_full_name' => $loginData->employee_full_name,
                                'employee_phone' => $loginData->employee_phone,
                                'employee_qr_code' => $loginData->employee_qr_code,
                                'is_active' => $loginData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime()
                            ]);

                    $loginData->employee_id = $rows;
                    
                    $rowsToken = \DB::table('employee_token')
                                ->insert([
                                'employee_id' => $rows, //$queryDataToken->user_id,
                                'access_token' => $queryDataToken->access_token,
                            ]);
                }
                else // UPDATE
                {
                    $rows = \DB::table('employee_login_data')
                        ->where('employee_id', $loginData->employee_id)
                        ->update([
                            'employee_user_name' => $loginData->employee_user_name,
                            'employee_password' => $loginData->employee_password,
                            'employee_full_name' => $loginData->employee_full_name,
                            'employee_phone' => $loginData->employee_phone,
                            'employee_qr_code' => $loginData->employee_qr_code,
                            'is_active' => $loginData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime()
                        ]);
                }

                // SAVE DETAIL
                foreach ($accessData as $queryData) {
                    if ($queryData->id == 0) // INSERT
                    {
                        $rows = \DB::table('employee_access_list')
                                ->insertGetId([
                                    'employee_id' => $loginData->employee_id,
                                    'kompleks_id' => $queryData->kompleks_id,
                                    'is_active' => $queryData->is_active,
                                    'created_by' => $p_user_id,
                                    'creation_date' => new \DateTime()
                                ]);
    
                        $queryData->id = $rows;
                    }
                    else // UPDATE
                    {
                        $rows = \DB::table('employee_access_list')
                            ->where('id', $queryData->id)
                            ->update([
                                'employee_id' => $loginData->employee_id,
                                'kompleks_id' => $queryData->kompleks_id,
                                'is_active' => $queryData->is_active,
                                'last_updated_by' => $p_user_id,
                                'last_update_date' => new \DateTime()
                            ]);
                    }
                }
    
                \DB::commit();
    
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function resetStatusKavling(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $p_kavling_id = $request->get('p_kavling_id');
                // SAVE HEADER
                if ($p_opType == 1) // SEWA
                {
                    $rows = \DB::table('master_kavling')
                        ->where('kavling_id', $p_kavling_id)
                        ->update([
                            'rent_status' => null,
                            'rent_url' => null,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime()
                        ]);
                }
                else 
                {
                    $rows = \DB::table('master_kavling')
                        ->where('kavling_id', $p_kavling_id)
                        ->update([
                            'sell_status' => null,
                            'sell_url' => null,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime()
                        ]);
                }

                \DB::commit();
    
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveMessage(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $p_msgID              = $request->get('p_msgID');
                $p_moduleID           = $request->get('p_moduleID');
                $p_msgRef             = $request->get('p_msgRef');
                $p_msgContent         = $request->get('p_msgContent');
                $p_opType             = 1;
                $query = "select count(1) as numRec from master_message where module_id = ". $p_moduleID . " and msg_status = 'unread'";

                $data = \DB::select(\DB::raw($query));
                $recordCount = $data[0]->numRec;

                if ($recordCount > 0)
                {
                    $p_opType = 2;

                    $query = "select msg_id from master_message where module_id = ". $p_moduleID . " and msg_status = 'unread'";

                    $data = \DB::select(\DB::raw($query));
                    $p_msgID = $data[0]->msg_id;
                }

                // SAVE HEADER
                if ($p_opType == 1) // INSERT
                {
                    $rows = \DB::table('master_message')
                            ->insertGetId([
                                'module_id' => $p_moduleID,
                                'msg_ref' => $p_msgRef,
                                'msg_content' => $p_msgContent,
                                'msg_datetime' => new \DateTime(),
                            ]);
                }
                else // UPDATE
                {
                    $rows = \DB::table('master_message')
                        ->where('msg_id', $p_msgID)
                        ->update([
                            'module_id' => $p_moduleID,
                            'msg_ref' => $p_msgRef,
                            'msg_content' => $p_msgContent,
                            'msg_datetime' => new \DateTime(),
                        ]);
                }

                \DB::commit();
    
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function updateMsgStatus(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $p_msgID              = $request->get('p_msgID');

                $rows = \DB::table('master_message')
                ->where('msg_id', $p_msgID)
                ->update([
                    'msg_status' => 'read',
                    'msg_read_datetime' => new \DateTime(),
                ]);

                \DB::commit();
    
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function getAllUserList(Request $request){
        $validation = Validator::make($request->all(),[ 
            'token'        => 'required',
        ]);

        if($validation->fails()){
            $errResponse = new userAccessList();	
            $errResponse->user_full_name = '';
            $errResponse->terminal_id = '';
            $errResponse->access_id= '';
            $errResponse->access_type= '';
            $errResponse->access_status= '0';
            $errResponse->message = 'FAIL [v1]';        
            $result = json_encode($errResponse);

            return $result;
        }

        $token = $request->get('token');
        $tokenValid = $this->checkExternalToken($token);
        if ($tokenValid == 1)
        {
            $this->dropCreateTempTableAll();              
            $this->fillInTempTableAll();
            $this->saveSyncLog('ALL','ALL');    

            try 
            {    
                $responseResult = array();

                $query = "select * from user_access_temp_all";

                $data = \DB::select(\DB::raw($query));
                $recordCount = count($data);

                foreach ($data as $queryData) {                
                    $response = new userAccessList();	
                    $response->user_full_name = $queryData->user_full_name;
                    $response->terminal_id = $queryData->terminal_id;
                    $response->access_id = $queryData->access_id;
                    $response->access_type = $queryData->access_type;
                    $response->access_status = $queryData->access_status;
                    $response->message = $queryData->message;
                    $responseResult[] = $response;     
                }

                $result = json_encode($responseResult);
            }
            catch (Exception $e) 
            {
                $errResponse = new userAccessList();	
                $errResponse->user_full_name = '';
                $errResponse->terminal_id = '';
                $errResponse->access_id= '';
                $errResponse->access_type= '';
                $errResponse->access_status= '0';
                $errResponse->message = 'FAIL [tc]';        
                $result = json_encode($errResponse);
            }    
        }   
        else
        {
            $errResponse = new userAccessList();	
            $errResponse->user_full_name = '';
            $errResponse->terminal_id = '';
            $errResponse->access_id= '';
            $errResponse->access_type= '';
            $errResponse->access_status= '0';
            $errResponse->message = 'FAIL [tk]';        
            $result = json_encode($errResponse);
        } 

        return $result;
    }

    public function getStatusConn(Request $request){
        $validation = Validator::make($request->all(),[ 
            'token'        => 'required',
        ]);

        if($validation->fails()){
            return 0;
        }

        $token = $request->get('token');

        $tokenValid = $this->checkExternalToken($token);
        if ($tokenValid == 1)
        {
            $result = 1;
        }     

        return $result;
    }
}