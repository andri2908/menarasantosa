ALTER TABLE `u1311857_sys_residential`.`user_kavling` 
ADD COLUMN `user_qr_enabled` ENUM('Y', 'N') NULL DEFAULT 'Y' AFTER `user_qr_code`;

CREATE TABLE `u1311857_sys_residential`.`user_kavling_rfid` (
  `id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_kavling_id` BIGINT(10) NULL DEFAULT 0,
  `rfid_value` VARCHAR(100) NULL DEFAULT '',
  `rfid_enabled` ENUM('Y', 'N') NULL DEFAULT 'Y',
  `is_active` ENUM('Y', 'N') NULL DEFAULT 'Y',
  `created_by` INT(10) NULL DEFAULT 0,
  `creation_date` DATETIME NULL,
  `last_updated_by` INT(10) NULL DEFAULT 0,
  `last_update_date` DATETIME NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `u1311857_sys_residential`.`kavling_status` (
  `id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kavling_id` BIGINT(10) NULL DEFAULT 0,
  `kavling_enabled` ENUM('Y', 'N') NULL DEFAULT 'Y',
  `is_active` ENUM('Y', 'N') NULL DEFAULT 'Y',
  `created_by` INT(10) UNSIGNED NULL DEFAULT 0,
  `creation_date` DATETIME NULL,
  `last_updated_by` INT(10) UNSIGNED NULL DEFAULT 0,
  `last_update_date` DATETIME NULL,
  PRIMARY KEY (`id`));
