ALTER TABLE `u1311857_sys_residential`.`master_blok` 
ADD COLUMN `luas_tanah` DOUBLE NULL DEFAULT 0 AFTER `blok_name`,
ADD COLUMN `luas_bangunan` DOUBLE NULL DEFAULT 0 AFTER `luas_tanah`;

ALTER TABLE `u1311857_sys_residential`.`user_kavling` 
ADD COLUMN `tgl_bast` DATETIME NULL AFTER `start_ipl`;

UPDATE user_kavling SET tgl_bast = now();

public function saveMasterKompleks(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);
            
        
            if ($num > 0)
            {          
                $records = json_decode($request->get('kompleksParam'));
                $blokData = json_decode($request->get('blokParam'));

                $kompleksData = $records[0];

                if ($p_opType == 1) // INSERT
                {
                    $rows = \DB::table('master_kompleks')
                    ->insertGetId([
                        'kompleks_name' => $kompleksData->kompleks_name,
                        'kompleks_address' => $kompleksData->kompleks_address,
                        'biaya_ipl' => $kompleksData->biaya_ipl,
                        'durasi_pembayaran' => $kompleksData->durasi_pembayaran,
                        'img_kompleks' => $kompleksData->img_kompleks,
                        'is_active' => $kompleksData->is_active,
                        'created_by' => $p_user_id,
                        'creation_date' => new \DateTime()
                    ]);

                    $kompleksData->kompleks_id = $rows;
                }
                else // UPDATE
                {
                    $rows = \DB::table('master_kompleks')
                    ->where('kompleks_id', $kompleksData->kompleks_id)
                    ->update([
                        'kompleks_name' => $kompleksData->kompleks_name,
                        'kompleks_address' => $kompleksData->kompleks_address,
                        'biaya_ipl' => $kompleksData->biaya_ipl,
                        'durasi_pembayaran' => $kompleksData->durasi_pembayaran,
                        'img_kompleks' => $kompleksData->img_kompleks,
                        'is_active' => $kompleksData->is_active,
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \DateTime(),
                    ]);
                }

                foreach ($blokData as $queryData) 
                {             
                    if ($queryData->blok_id > 0)
                    {
                        $rowsTrans = \DB::table('master_blok')
                        ->where('blok_id', $queryData->blok_id)
                        ->update([
                            'blok_name' => $queryData->blok_name,
                            'luas_tanah' => $queryData->luas_tanah,
                            'luas_bangunan' => $queryData->luas_bangunan,
                            'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime(),
                        ]);
                    }
                    else
                    {
                        $rowsTrans = \DB::table('master_blok')
                            ->insert([
                                'kompleks_id' => $kompleksData->kompleks_id,
                                'blok_name' => $queryData->blok_name,
                                'luas_tanah' => $queryData->luas_tanah,
                                'luas_bangunan' => $queryData->luas_bangunan,
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime(),
                        ]);
                    }
                }

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
                }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

public function saveUserKavling(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -2,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));
                foreach ($records as $queryData) 
                {          
                    if ($queryData->id > 0)
                    {
                        $rows = \DB::table('user_kavling')
                        ->where('id', $queryData->id)
                        ->update([
                            'user_id' => $queryData->user_id,
                            'kavling_id' => $queryData->kavling_id,
                            'tgl_bast' => $queryData->tgl_bast,
                            'start_ipl' => $queryData->start_ipl,
                            'remarks' => $queryData->remarks,
                            'user_qr_code' => $queryData->user_qr_code,
                            'user_qr_enabled' => $queryData->user_qr_enabled,
                            'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \Datetime(),
                        ]);
                    }
                    else
                    {
                        $rows = \DB::table('user_kavling')
                        ->insert([
                            'user_id' => $queryData->user_id,
                            'kavling_id' => $queryData->kavling_id,
                            'tgl_bast' => $queryData->tgl_bast,
                            'start_ipl' => $queryData->start_ipl,
                            'remarks' => $queryData->remarks,
                            'user_qr_code' => $queryData->user_qr_code,
                            'user_qr_enabled' => $queryData->user_qr_enabled,
                            'is_active' => $queryData->is_active,
                            'created_by' => $p_user_id,
                            'creation_date' => new \DateTime()
                        ]);
                    }
                }

                $recordsUpd = json_decode($request->get('updKavling'));
                foreach ($recordsUpd as $queryUpd) 
                {
                    $rows = \DB::table('master_kavling')
                    ->where('kavling_id', $queryUpd->kavling_id)
                    ->update([
                        'status' => $queryUpd->status,
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \Datetime(),
                    ]);
                }

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }