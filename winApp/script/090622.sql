CREATE TABLE `employee_login_data` (
  `employee_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_user_name` varchar(15) DEFAULT '',
  `employee_password` varchar(50) DEFAULT '',
  `employee_full_name` varchar(100) DEFAULT '',
  `employee_phone` varchar(45) DEFAULT '',
  `employee_qr_code` varchar(100) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`employee_id`),
  KEY `SECONDARY` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `employee_access_list` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(10) DEFAULT '0',
  `kompleks_id` bigint(10) DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `u1311857_sys_residential`.`master_module` (`module_id`, `module_name`, `module_features`, `module_active`) VALUES ('48', 'TAMBAH / HAPUS STAFF', '2', '1');
