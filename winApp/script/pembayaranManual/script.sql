CREATE TABLE `u1311857_sys_residential`.`transaksi_payment_manual` (
  `id_payment` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `payment_id` VARCHAR(45) NULL,
  `payment_type` INT(11) NULL,
  `total` DOUBLE NULL,
  `date_paid` DATETIME NULL,
  `payment_remark` VARCHAR(200) NULL,
  `is_active` ENUM('Y', 'N') NULL,
  `created_by` INT(10) NULL,
  `creation_date` DATETIME NULL,
  `last_updated_by` INT(10) NULL,
  `last_update_date` DATETIME NULL,
  PRIMARY KEY (`id_payment`));
  
  
