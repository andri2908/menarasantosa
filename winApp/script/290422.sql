CREATE TABLE `u1311857_sys_residential`.`sync_log` (
  `id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `terminal_id` VARCHAR(50) NULL DEFAULT '',
  `access_type` ENUM('QR', 'RFID') NULL DEFAULT 'QR',
  PRIMARY KEY (`id`));
ALTER TABLE `u1311857_sys_residential`.`sync_log` 
ADD COLUMN `sync_datetime` DATETIME NULL AFTER `access_type`;
