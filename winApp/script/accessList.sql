CREATE TABLE `user_access_list` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) DEFAULT '0',
  `kompleks_id` bigint(10) DEFAULT '0',
  `kavling_id` bigint(10) DEFAULT '0',
  `access_id` varchar(100) DEFAULT '',
  `access_type` enum('QR','RFID') DEFAULT 'QR',
  `access_status` tinyint(3) DEFAULT '1',
  `message` varchar(100) DEFAULT 'Welcome...',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `u1311857_sys_residential`.`user_kavling` 
ADD COLUMN `user_qr_enabled` ENUM('Y', 'N') NULL DEFAULT 'Y' AFTER `user_qr_code`;

CREATE TABLE `u1311857_sys_residential`.`user_kavling_rfid` (
  `id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_kavling_id` BIGINT(10) NULL DEFAULT 0,
  `rfid_value` VARCHAR(100) NULL DEFAULT '',
  `rfid_enabled` ENUM('Y', 'N') NULL DEFAULT 'Y',
  `is_active` ENUM('Y', 'N') NULL DEFAULT 'Y',
  `created_by` INT(10) NULL DEFAULT 0,
  `creation_date` DATETIME NULL,
  `last_updated_by` INT(10) NULL DEFAULT 0,
  `last_update_date` DATETIME NULL,
  PRIMARY KEY (`id`));


CREATE TABLE `u1311857_sys_residential`.`satpam_access_list` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `kompleks_id` bigint(10) DEFAULT '0',
  `user_full_name` varchar(100) DEFAULT '',
  `access_id` varchar(100) DEFAULT '',
  `access_type` enum('QR','RFID') DEFAULT 'QR',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `u1311857_sys_residential`.`kompleks_terminal` (
  `id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kompleks_id` BIGINT(10) UNSIGNED NULL DEFAULT 0,
  `terminal_id` VARCHAR(50) NULL DEFAULT '',
  `is_active` ENUM('Y', 'N') NULL DEFAULT 'Y',
  `created_by` INT(10) UNSIGNED NULL DEFAULT 0,
  `creation_date` DATETIME NULL,
  `last_updated_by` INT(10) UNSIGNED NULL DEFAULT 0,
  `last_update_date` DATETIME NULL,
  PRIMARY KEY (`id`));
