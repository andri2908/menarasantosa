ALTER TABLE `u1311857_sys_residential`.`master_kavling` 
ADD COLUMN `status` ENUM('Dihuni sendiri', 'Disewakan', 'Kosong') NULL DEFAULT 'Kosong' AFTER `durasi_pembayaran`;

ALTER TABLE `u1311857_sys_residential`.`master_kompleks` 
ADD COLUMN `img_kompleks` VARCHAR(100) NULL DEFAULT '' AFTER `durasi_pembayaran`;
