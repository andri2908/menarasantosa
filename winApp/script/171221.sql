ALTER TABLE `u1311857_sys_residential`.`kompleks_terminal` 
ADD COLUMN `terminal_type` ENUM('IN', 'OUT') NULL DEFAULT 'IN' AFTER `terminal_id`;

CREATE TABLE `u1311857_sys_residential`.`access_log` (
  `id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `access_status` TINYINT(3) NULL DEFAULT 1,
  `user_full_name` VARCHAR(100) NULL DEFAULT '',
  `access_id` VARCHAR(100) NULL DEFAULT '',
  `terminal_id` VARCHAR(50) NULL DEFAULT '',
  `kompleks_id` BIGINT(10) NULL DEFAULT 0,
  `timestamp` DATETIME NULL,
  `creation_date` DATETIME NULL,
  PRIMARY KEY (`id`));


CREATE TABLE `satpam_log` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `access_status` tinyint(3) DEFAULT '1',
  `user_full_name` varchar(100) DEFAULT '',
  `access_id` varchar(100) DEFAULT '',
  `timestamp` datetime DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
