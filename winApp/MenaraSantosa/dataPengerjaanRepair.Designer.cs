﻿namespace AlphaSoft
{
    partial class dataPengerjaanRepair
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.checkBoxMTC = new System.Windows.Forms.CheckBox();
            this.biayaTextBox = new System.Windows.Forms.TextBox();
            this.descTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.subjectTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.noTicketTextBox = new System.Windows.Forms.TextBox();
            this.vendorTextBox = new System.Windows.Forms.TextBox();
            this.startDTPicker = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.SaveButton = new System.Windows.Forms.Button();
            this.confirmationButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(884, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.checkBoxMTC);
            this.panel2.Controls.Add(this.biayaTextBox);
            this.panel2.Controls.Add(this.descTextBox);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.subjectTextBox);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.noTicketTextBox);
            this.panel2.Controls.Add(this.vendorTextBox);
            this.panel2.Controls.Add(this.startDTPicker);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(12, 48);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(852, 259);
            this.panel2.TabIndex = 60;
            // 
            // checkBoxMTC
            // 
            this.checkBoxMTC.AutoSize = true;
            this.checkBoxMTC.Enabled = false;
            this.checkBoxMTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxMTC.Location = new System.Drawing.Point(106, 154);
            this.checkBoxMTC.Name = "checkBoxMTC";
            this.checkBoxMTC.Size = new System.Drawing.Size(156, 22);
            this.checkBoxMTC.TabIndex = 222;
            this.checkBoxMTC.Text = "Biaya Perbaikan Rp";
            this.checkBoxMTC.UseVisualStyleBackColor = true;
            // 
            // biayaTextBox
            // 
            this.biayaTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.biayaTextBox.Enabled = false;
            this.biayaTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.biayaTextBox.Location = new System.Drawing.Point(263, 152);
            this.biayaTextBox.MaxLength = 20;
            this.biayaTextBox.Name = "biayaTextBox";
            this.biayaTextBox.Size = new System.Drawing.Size(241, 27);
            this.biayaTextBox.TabIndex = 221;
            this.biayaTextBox.Text = "0";
            this.biayaTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // descTextBox
            // 
            this.descTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.descTextBox.Location = new System.Drawing.Point(106, 83);
            this.descTextBox.MaxLength = 30;
            this.descTextBox.Multiline = true;
            this.descTextBox.Name = "descTextBox";
            this.descTextBox.ReadOnly = true;
            this.descTextBox.Size = new System.Drawing.Size(685, 63);
            this.descTextBox.TabIndex = 220;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(28, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 18);
            this.label1.TabIndex = 218;
            this.label1.Text = "Subyek";
            // 
            // subjectTextBox
            // 
            this.subjectTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectTextBox.Location = new System.Drawing.Point(106, 50);
            this.subjectTextBox.MaxLength = 30;
            this.subjectTextBox.Name = "subjectTextBox";
            this.subjectTextBox.ReadOnly = true;
            this.subjectTextBox.Size = new System.Drawing.Size(502, 27);
            this.subjectTextBox.TabIndex = 219;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(28, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 18);
            this.label3.TabIndex = 216;
            this.label3.Text = "Nomor";
            // 
            // noTicketTextBox
            // 
            this.noTicketTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noTicketTextBox.Location = new System.Drawing.Point(106, 14);
            this.noTicketTextBox.MaxLength = 30;
            this.noTicketTextBox.Name = "noTicketTextBox";
            this.noTicketTextBox.ReadOnly = true;
            this.noTicketTextBox.Size = new System.Drawing.Size(300, 27);
            this.noTicketTextBox.TabIndex = 217;
            // 
            // vendorTextBox
            // 
            this.vendorTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vendorTextBox.Location = new System.Drawing.Point(208, 211);
            this.vendorTextBox.MaxLength = 3000;
            this.vendorTextBox.Name = "vendorTextBox";
            this.vendorTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.vendorTextBox.Size = new System.Drawing.Size(626, 27);
            this.vendorTextBox.TabIndex = 197;
            // 
            // startDTPicker
            // 
            this.startDTPicker.CalendarFont = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startDTPicker.CustomFormat = "dd MMM yyyy";
            this.startDTPicker.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startDTPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startDTPicker.Location = new System.Drawing.Point(31, 211);
            this.startDTPicker.Name = "startDTPicker";
            this.startDTPicker.Size = new System.Drawing.Size(171, 27);
            this.startDTPicker.TabIndex = 195;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(28, 190);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 18);
            this.label4.TabIndex = 194;
            this.label4.Text = "Perbaikan";
            // 
            // SaveButton
            // 
            this.SaveButton.BackColor = System.Drawing.Color.FloralWhite;
            this.SaveButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveButton.ForeColor = System.Drawing.Color.Black;
            this.SaveButton.Location = new System.Drawing.Point(220, 332);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(182, 37);
            this.SaveButton.TabIndex = 61;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = false;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // confirmationButton
            // 
            this.confirmationButton.BackColor = System.Drawing.Color.FloralWhite;
            this.confirmationButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirmationButton.ForeColor = System.Drawing.Color.Black;
            this.confirmationButton.Location = new System.Drawing.Point(469, 332);
            this.confirmationButton.Name = "confirmationButton";
            this.confirmationButton.Size = new System.Drawing.Size(182, 37);
            this.confirmationButton.TabIndex = 62;
            this.confirmationButton.Text = "Konfirmasi Selesai";
            this.confirmationButton.UseVisualStyleBackColor = false;
            this.confirmationButton.Click += new System.EventHandler(this.confirmationButton_Click);
            // 
            // dataPengerjaanRepair
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(884, 411);
            this.Controls.Add(this.confirmationButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.panel2);
            this.Name = "dataPengerjaanRepair";
            this.Load += new System.EventHandler(this.dataPengerjaanRepair_Load);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.SaveButton, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.confirmationButton, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        protected System.Windows.Forms.Label label4;
        protected System.Windows.Forms.DateTimePicker startDTPicker;
        protected System.Windows.Forms.TextBox vendorTextBox;
        protected System.Windows.Forms.Button SaveButton;
        protected System.Windows.Forms.TextBox descTextBox;
        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.TextBox subjectTextBox;
        protected System.Windows.Forms.Label label3;
        protected System.Windows.Forms.TextBox noTicketTextBox;
        private System.Windows.Forms.CheckBox checkBoxMTC;
        protected System.Windows.Forms.TextBox biayaTextBox;
        protected System.Windows.Forms.Button confirmationButton;
    }
}
