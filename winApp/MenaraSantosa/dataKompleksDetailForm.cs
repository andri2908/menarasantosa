﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using DBGridExtension;

namespace AlphaSoft
{
    public partial class dataKompleksDetailForm : AlphaSoft.basic2TextBoxInputsForm
    {
        private int originModuleID = 0;
        private int selectedKompleksID = 0;
        private globalUserUtil gUser;
        private globalRestAPI gRest;
        private globalImageLib gImg;

        private CultureInfo culture = new CultureInfo("id-ID");
        private string selectedImgFile = "";
        private string selectedLogoImgFile = "";
        private string outputImgFile = "";
        private string outputLogoImgFile = "";

        private bool isFtpImage = false;
        private bool isLogoFtpImage = false;
        public int returnKompleksIDValue = 0;

        public dataKompleksDetailForm(int kompleksID = 0)
        {
            InitializeComponent();
            selectedKompleksID = kompleksID;

            gUser = new globalUserUtil();
            gRest = new globalRestAPI();
            gImg = new globalImageLib();

            if (selectedKompleksID > 0)
            {
                originModuleID = globalConstants.EDIT_KOMPLEKS;

                if (!gUser.RS_userHasAccessTo(MMConstants.TAMBAH_HAPUS_KOMPLEKS,
                MMConstants.HAK_UPDATE_DATA))
                {
                    setSaveButtonEnable(false);
                    setResetButtonEnable(false);
                }
            }
            else
                originModuleID = globalConstants.NEW_KOMPLEKS;
        }

        private void dataKompleksDetailForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            returnKompleksIDValue = selectedKompleksID;
        }

        private void loadKompleksDataInformation()
        {
            DataTable dt = new DataTable();
            REST_masterKompleks kompleksData = new REST_masterKompleks();
            REST_masterBlok blokData = new REST_masterBlok();

            #region LOAD MASTER KOMPLEKS
            string sqlCommand = "SELECT * " +
                                                "FROM master_kompleks " +
                                                "WHERE kompleks_id =  " + selectedKompleksID;

            if (gRest.getMasterKompleksData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref kompleksData))
            {
                if (kompleksData.dataStatus.o_status == 1)
                {
                    namaTextBox.Text = kompleksData.dataList[0].kompleks_name;
                    deskripsiTextBox.Text = kompleksData.dataList[0].kompleks_address;
                    nonAktifCheckbox.Checked = (kompleksData.dataList[0].is_active == "Y" ? false : true);
                    biayaIPLTextBox.Text = kompleksData.dataList[0].biaya_ipl.ToString("N0", culture);
                    durasiBayarTextBox.Text = kompleksData.dataList[0].durasi_pembayaran.ToString("N0", culture);

                    //LOAD IMAGE
                    outputImgFile = kompleksData.dataList[0].img_kompleks;

                    if (outputImgFile.Length > 0)
                    {
                        try
                        {
                            Bitmap bm;
                            bm = gImg.ByteToImage(outputImgFile, globalImageLib.IMG_KOMPLEKS);
                            pbBox.Image = bm;

                            isFtpImage = true;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("File gambar tidak ditemukan " + ex.Message);
                            pbBox.Load(gImg.localImgDirectory + "errImg.jpeg");
                        }
                    }

                    outputLogoImgFile = kompleksData.dataList[0].img_logo;
                    if (outputLogoImgFile.Length > 0)
                    {
                        try
                        {
                            Bitmap bm;
                            bm = gImg.ByteToImage(outputLogoImgFile, globalImageLib.IMG_KOMPLEKS);
                            pbLogo.Image = bm;

                            isLogoFtpImage = true;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("File gambar tidak ditemukan " + ex.Message);
                            pbLogo.Load(gImg.localImgDirectory + "errImg.jpeg");
                        }
                    }
                }
            }
            #endregion

            #region LOAD BLOK
            sqlCommand = "SELECT * " +
                                            "FROM master_blok " +
                                            "WHERE kompleks_id =  " + selectedKompleksID + " " +
                                            "AND is_active = 'Y'";

            if (gRest.getMasterBlokData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref blokData))
            {
                blokGridView.Rows.Clear();
                if (blokData.dataStatus.o_status == 1)
                {
                    for (int i = 0;i<blokData.dataList.Count;i++)
                    {
                        blokGridView.Rows.Add(
                           1,
                           blokData.dataList[i].blok_id,
                           blokData.dataList[i].blok_name
                           );
                    }
                }
            }
            #endregion
        }

        private void dataKompleksDetailForm_Load(object sender, EventArgs e)
        {
            if (selectedKompleksID > 0)
            {
                loadKompleksDataInformation();
            }

            namaTextBox.Focus();
            gUser.reArrangeTabOrder(this);

            biayaIPLTextBox.Enter += TextBox_Enter;
            biayaIPLTextBox.Leave += TextBox_Int32_Leave;

            durasiBayarTextBox.Enter += TextBox_Enter;
            durasiBayarTextBox.Leave += TextBox_Int32_Leave;

            blokGridView.CellValueChanged += CUSTOM_CellValueChanged;
            blokGridView.DoubleBuffered(true);
        }

        private void resizeAndGetNewName()
        {
            string sqlCommand = "";
            string inputName = String.Format(culture, "{0:yyyyMMddhhmmss}", DateTime.Now) + gUser.getRandomString(15);

            if (outputImgFile.Length <= 0)
            {
                while (true)
                {
                    outputImgFile = gUser.getMD5Value(inputName) + ".jpg";

                    sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                            "FROM master_kompleks mi " +
                                            "WHERE mi.img_kompleks = '" + outputImgFile + "' " +
                                            "AND mi.is_active = 'Y'";

                    genericReply replyResult = new genericReply();
                    if (gRest.checkDataExist(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
                    {
                        if (Convert.ToInt32(replyResult.data) <= 0)
                        {
                            break;
                        }
                    }
                }
            }

            gImg.ResizeImage(selectedImgFile, outputImgFile);
        }

        private void resizeLogoAndGetNewName()
        {
            string sqlCommand = "";
            string inputName = "logo_" + String.Format(culture, "{0:yyyyMMddhhmmss}", DateTime.Now) + gUser.getRandomString(15);

            if (outputLogoImgFile.Length <= 0 || outputLogoImgFile == globalImageLib.LOGO_IMG_DEFAULT)
            {
                while (true)
                {
                    outputLogoImgFile = gUser.getMD5Value(inputName) + ".png";

                    sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                            "FROM master_kompleks mi " +
                                            "WHERE mi.img_logo = '" + outputLogoImgFile + "' " +
                                            "AND mi.is_active = 'Y'";

                    genericReply replyResult = new genericReply();
                    if (gRest.checkDataExist(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
                    {
                        if (Convert.ToInt32(replyResult.data) <= 0)
                        {
                            break;
                        }
                    }
                }
            }

            gImg.ResizeImage(selectedLogoImgFile, outputLogoImgFile, 250, 500, true, false);
        }

        protected override bool saveDataTransaction()
        {
            bool result = false;
            int opType = 0;
            double biayaIPL = 0;
            int durasiBayar = 0;

            List<master_kompleks> listKompleks = new List<master_kompleks>();
            List<master_blok> listBlok = new List<master_blok>();

            master_kompleks kompleksData = new master_kompleks();
            master_blok blokData;

            genericReply replyResult = new genericReply();
            string errMsg = "";
            try
            {
                double.TryParse(biayaIPLTextBox.Text, NumberStyles.Number, culture, out biayaIPL);
                int.TryParse(durasiBayarTextBox.Text, NumberStyles.Number, culture, out durasiBayar);

                if (selectedImgFile.Length > 0)
                {
                    resizeAndGetNewName();
                    if (!gImg.uploadToFTP(outputImgFile, globalImageLib.IMG_KOMPLEKS))
                    {
                        throw new Exception("Upload Gambar Gagal");
                    }
                }

                if (selectedLogoImgFile.Length > 0)
                {
                    resizeLogoAndGetNewName();
                    if (!gImg.uploadToFTP(outputLogoImgFile, globalImageLib.IMG_KOMPLEKS))
                    {
                        throw new Exception("Upload Gambar Gagal");
                    }
                }
                else
                {
                    outputLogoImgFile = globalImageLib.LOGO_IMG_DEFAULT;
                }

                #region MASTER KOMPLEKS
                switch (originModuleID)
                {
                    case globalConstants.NEW_KOMPLEKS:
                        opType = 1;
                        kompleksData.kompleks_id = 0;
                        break;

                    case globalConstants.EDIT_KOMPLEKS:
                        opType = 2;
                        kompleksData.kompleks_id = selectedKompleksID;
                        break;
                }

                kompleksData.kompleks_name = namaTextBox.Text;
                kompleksData.kompleks_address = deskripsiTextBox.Text;
                kompleksData.biaya_ipl = biayaIPL;
                kompleksData.durasi_pembayaran= durasiBayar;
                kompleksData.img_kompleks = outputImgFile;
                kompleksData.img_logo = outputLogoImgFile;
                kompleksData.is_active = (nonAktifCheckbox.Checked ? "N" : "Y");

                listKompleks.Add(kompleksData);
                #endregion

                #region MASTER BLOK
                DataGridViewRow sRow;
                int flag = 1;
                int blokID = 0;
                string blokName = "";

                for (int i = 0; i < blokGridView.Rows.Count - 1; i++)
                {
                    sRow = blokGridView.Rows[i];
                    blokData = new master_blok();

                    if (null != sRow.Cells["isActive"].Value)
                        flag = Convert.ToInt32(sRow.Cells["isActive"].Value);
                    else
                        flag = 1;

                    if (null != sRow.Cells["blokID"].Value)
                        blokID = Convert.ToInt32(sRow.Cells["blokID"].Value);
                    else
                        blokID = 0;

                    if (null != sRow.Cells["blokName"].Value &&
                        sRow.Cells["blokName"].Value.ToString().Length > 0)
                        blokName = sRow.Cells["blokName"].Value.ToString();

                    blokData.blok_id = blokID;
                    blokData.kompleks_id = kompleksData.kompleks_id;
                    blokData.blok_name = blokName.ToUpper();
                    blokData.is_active = (flag == 1 ? "Y" : "N");

                    listBlok.Add(blokData);
                }
                #endregion

                int newId = 0;
                if (!gRest.saveMasterKompleks(gUser.getUserID(), gUser.getUserToken(), listKompleks, listBlok, opType, out errMsg, out newId))
                {
                    throw new Exception(errMsg);
                }

                if (opType == 1)
                    selectedKompleksID = newId;

                result = true;
            }
            catch(Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        protected override bool dataValidated()
        {
            bool result = false;
            string sqlComm;
            result = base.dataValidated();

            genericReply replyResult = new genericReply();

            if (!result)
                return false;

            errorLabel.Text = "";

            if (namaTextBox.Text.Length <= 0)
            {
                errorLabel.Text = "Nama kompleks kosong";
                return false;
            }

            sqlComm = "SELECT COUNT(1) AS resultQuery " +
                                "FROM master_kompleks " +
                                "WHERE kompleks_name = '" + namaTextBox.Text + "' " +
                                "AND kompleks_id <> " + selectedKompleksID;
            if (gRest.checkDataExist(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                {
                    errorLabel.Text = "Nama kompleks sudah ada";
                    return false;
                }
            }

            double tempVal = 0;
            if (biayaIPLTextBox.Text.Length <= 0 ||
                !double.TryParse(biayaIPLTextBox.Text, NumberStyles.Number, culture, out tempVal))
            {
                errorLabel.Text = "Input biaya IPL salah";
                return false;
            }

            if (durasiBayarTextBox.Text.Length <= 0 ||
                !double.TryParse(durasiBayarTextBox.Text, NumberStyles.Number, culture, out tempVal))
            {
                errorLabel.Text = "Input durasi pembayaran salah";
                return false;
            }

            List<string> blokList = new List<string>();
            DataGridViewRow sRow;
            string blokName = "";
            int blokID = 0;
            double tempValue = 0;
            for (int i = 0; i<blokGridView.Rows.Count - 1;i++)
            {
                sRow = blokGridView.Rows[i];
                if (null != sRow.Cells["blokID"].Value)
                    blokID = Convert.ToInt32(sRow.Cells["blokID"].Value);
                else
                    blokID = 0;

                if (null != sRow.Cells["blokName"].Value && 
                    sRow.Cells["blokName"].Value.ToString().Length > 0)
                {
                    blokName = sRow.Cells["blokName"].Value.ToString();

                    sqlComm = "SELECT COUNT(1) AS resultQuery " +
                                    "FROM master_blok " +
                                    "WHERE blok_name = '" + blokName + "' " +
                                    //"AND kompleks_id = " + selectedKompleksID + " " +
                                    "AND blok_id <> " + blokID;
                    if (
                        (gRest.checkDataExist(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult)) ||
                        (blokList.Contains(blokName)))
                    {
                        if (Convert.ToInt32(replyResult.data) > 0)
                        {
                            errorLabel.Text = "Nama blok sudah pernah ada";
                            return false;
                        }                            
                    }
                    else
                        blokList.Add(blokName);

                    //if (null == sRow.Cells["luasTanah"].Value || 
                    //    !double.TryParse(sRow.Cells["luasTanah"].Value.ToString(), out tempValue))
                    //{
                    //    errorLabel.Text = "Input luas tanah untuk " + blokName + " salah";
                    //    return false;
                    //}

                    //if (null == sRow.Cells["luasBangunan"].Value ||
                    //    !double.TryParse(sRow.Cells["luasBangunan"].Value.ToString(), out tempValue))
                    //{
                    //    errorLabel.Text = "Input luas bangunan untuk " + blokName + " salah";
                    //    return false;
                    //}
                }
                else
                {
                    if (blokID > 0)
                    {
                        errorLabel.Text = "Nama blok tidak boleh kosong";
                        return false;
                    }
                }
            }

            return true;
        }

        private void blokGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (blokGridView.Rows.Count > 0)
            {
                int selectedrowindex = blokGridView.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = blokGridView.Rows[selectedrowindex];

                if (e.KeyCode == Keys.Delete)
                {
                    string blokName = "";

                    if (null != selectedRow.Cells["blokName"].Value && selectedRow.Cells["blokName"].Value.ToString().Length > 0)
                        blokName = selectedRow.Cells["blokName"].Value.ToString();

                    if (DialogResult.Yes == MessageBox.Show("Hapus data blok " + blokName, "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    {
                        selectedRow.Cells["isActive"].Value = 0;
                        selectedRow.Visible = false;
                    }
                }
            }
        }

        private void img1Button_Click(object sender, EventArgs e)
        {
            string fileName = "";

            imgFileDialog.FileName = "";
            imgFileDialog.Filter = "Image Files(*.JPG; *.JPEG)| *.JPG; *.JPEG";
            if (imgFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    fileName = imgFileDialog.FileName;
                    pbBox.Load(fileName);
                    selectedImgFile = imgFileDialog.FileName;
                   // outputImgFile = "";
                    isFtpImage = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("File gambar tidak bisa dibuka : " + ex.Message);
                    pbBox.Load(gImg.localImgDirectory + "errImg.jpeg");
                }
            }
        }

        private void blokGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            //if ((blokGridView.Columns[e.ColumnIndex].Name == "luasTanah" ||
            // blokGridView.Columns[e.ColumnIndex].Name == "luasBangunan")
            //    && null != e.Value)
            //{
            //    try
            //    {
            //        double d = double.Parse(e.Value.ToString());
            //        e.Value = d.ToString("N2", culture);
            //    }
            //    catch (Exception ex)
            //    {
            //    }
            //}
        }

        private void CUSTOM_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (blokGridView.Rows.Count <= 0)
                return;

            var cell = blokGridView[e.ColumnIndex, e.RowIndex];
            DataGridViewRow sRow = blokGridView.Rows[e.RowIndex];

            string columnName = blokGridView.Columns[e.ColumnIndex].Name;
            double tempValue = 0;

            //if (columnName == "luasTanah" || columnName == "luasBangunan")
            //{
            //    if (null == sRow.Cells[columnName].Value ||
            //        sRow.Cells[columnName].Value.ToString().Length <= 0 ||
            //        !double.TryParse(sRow.Cells[columnName].Value.ToString(), NumberStyles.Number, culture, out tempValue))
            //    {
            //        errorLabel.Text = "Input " + sRow.Cells[columnName].OwningColumn.HeaderText + " untuk " + sRow.Cells["blokName"].Value.ToString() + " salah";
            //    }
            //    else
            //    {
            //        errorLabel.Text = "";

            //        blokGridView.CellValueChanged -= CUSTOM_CellValueChanged;
            //        cell.Value = tempValue;
            //        blokGridView.CellValueChanged += CUSTOM_CellValueChanged;
            //    }
            //}
        }

        private void blokGridView_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            //if (blokGridView.Rows.Count > 0)
            //{
            //    blokGridView.Rows[blokGridView.Rows.Count - 1].Cells["luasTanah"].Value = 0;
            //    blokGridView.Rows[blokGridView.Rows.Count - 1].Cells["luasBangunan"].Value = 0;
            //}
        }

        private void imgLogoButton_Click(object sender, EventArgs e)
        {
            string fileName = "";

            imgFileDialog.FileName = "";
            imgFileDialog.Filter = "Logo Image Files(*.PNG)| *.PNG";
            //imgFileDialog.Filter = "Image Files(*.JPG; *.JPEG)| *.JPG; *.JPEG";
            if (imgFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    fileName = imgFileDialog.FileName;
                    pbLogo.Load(fileName);
                    selectedLogoImgFile = imgFileDialog.FileName;
                    //outputLogoImgFile = "";
                    isLogoFtpImage = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("File gambar tidak bisa dibuka : " + ex.Message);
                    pbLogo.Load(gImg.localImgDirectory + "errImg.jpeg");
                }
            }
        }
    }
}
