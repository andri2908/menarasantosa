﻿namespace AlphaSoft
{
    partial class dataBrowse_TransaksiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dataBrowse_TransaksiForm));
            this.searchButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.kompleksTextBox = new System.Windows.Forms.TextBox();
            this.searchKompleks = new System.Windows.Forms.Button();
            this.statusCombo = new System.Windows.Forms.ComboBox();
            this.labelStatus = new System.Windows.Forms.Label();
            this.statusBayarCombo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.statusBayarCombo);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.statusCombo);
            this.groupBox1.Controls.Add(this.labelStatus);
            this.groupBox1.Controls.Add(this.searchKompleks);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.kompleksTextBox);
            this.groupBox1.Controls.Add(this.searchButton);
            this.groupBox1.Size = new System.Drawing.Size(1105, 129);
            this.groupBox1.Controls.SetChildIndex(this.namaTextBox, 0);
            this.groupBox1.Controls.SetChildIndex(this.nonActiveCheckBox, 0);
            this.groupBox1.Controls.SetChildIndex(this.label1, 0);
            this.groupBox1.Controls.SetChildIndex(this.label2, 0);
            this.groupBox1.Controls.SetChildIndex(this.startDTPicker, 0);
            this.groupBox1.Controls.SetChildIndex(this.label3, 0);
            this.groupBox1.Controls.SetChildIndex(this.endDTPicker, 0);
            this.groupBox1.Controls.SetChildIndex(this.searchButton, 0);
            this.groupBox1.Controls.SetChildIndex(this.kompleksTextBox, 0);
            this.groupBox1.Controls.SetChildIndex(this.label4, 0);
            this.groupBox1.Controls.SetChildIndex(this.searchKompleks, 0);
            this.groupBox1.Controls.SetChildIndex(this.labelStatus, 0);
            this.groupBox1.Controls.SetChildIndex(this.statusCombo, 0);
            this.groupBox1.Controls.SetChildIndex(this.label5, 0);
            this.groupBox1.Controls.SetChildIndex(this.statusBayarCombo, 0);
            // 
            // displayButton
            // 
            this.displayButton.Location = new System.Drawing.Point(474, 143);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(50, 74);
            this.label1.Size = new System.Drawing.Size(49, 18);
            this.label1.Text = "User";
            // 
            // newButton
            // 
            this.newButton.Location = new System.Drawing.Point(300, 143);
            // 
            // namaTextBox
            // 
            this.namaTextBox.Location = new System.Drawing.Point(105, 70);
            this.namaTextBox.ReadOnly = true;
            // 
            // searchButton
            // 
            this.searchButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchButton.BackgroundImage")));
            this.searchButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchButton.Location = new System.Drawing.Point(394, 68);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(29, 30);
            this.searchButton.TabIndex = 59;
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(431, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 18);
            this.label4.TabIndex = 60;
            this.label4.Text = "Kompleks";
            // 
            // kompleksTextBox
            // 
            this.kompleksTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.kompleksTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kompleksTextBox.Location = new System.Drawing.Point(529, 71);
            this.kompleksTextBox.Name = "kompleksTextBox";
            this.kompleksTextBox.ReadOnly = true;
            this.kompleksTextBox.Size = new System.Drawing.Size(250, 27);
            this.kompleksTextBox.TabIndex = 61;
            // 
            // searchKompleks
            // 
            this.searchKompleks.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchKompleks.BackgroundImage")));
            this.searchKompleks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchKompleks.Location = new System.Drawing.Point(785, 68);
            this.searchKompleks.Name = "searchKompleks";
            this.searchKompleks.Size = new System.Drawing.Size(29, 30);
            this.searchKompleks.TabIndex = 62;
            this.searchKompleks.UseVisualStyleBackColor = true;
            this.searchKompleks.Click += new System.EventHandler(this.searchKompleks_Click);
            // 
            // statusCombo
            // 
            this.statusCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.statusCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.statusCombo.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusCombo.FormattingEnabled = true;
            this.statusCombo.Location = new System.Drawing.Point(573, 32);
            this.statusCombo.Name = "statusCombo";
            this.statusCombo.Size = new System.Drawing.Size(241, 26);
            this.statusCombo.TabIndex = 177;
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatus.ForeColor = System.Drawing.Color.Black;
            this.labelStatus.Location = new System.Drawing.Point(503, 35);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(64, 18);
            this.labelStatus.TabIndex = 176;
            this.labelStatus.Text = "Status";
            // 
            // statusBayarCombo
            // 
            this.statusBayarCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.statusBayarCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.statusBayarCombo.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusBayarCombo.FormattingEnabled = true;
            this.statusBayarCombo.Location = new System.Drawing.Point(839, 70);
            this.statusBayarCombo.Name = "statusBayarCombo";
            this.statusBayarCombo.Size = new System.Drawing.Size(241, 26);
            this.statusBayarCombo.TabIndex = 179;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(836, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 18);
            this.label5.TabIndex = 178;
            this.label5.Text = "Status Bayar";
            // 
            // dataBrowse_TransaksiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1168, 640);
            this.Name = "dataBrowse_TransaksiForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.dataBrowse_TransaksiForm_FormClosed);
            this.Load += new System.EventHandler(this.dataBrowse_TransaksiForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Button searchKompleks;
        protected System.Windows.Forms.Label label4;
        protected System.Windows.Forms.TextBox kompleksTextBox;
        private System.Windows.Forms.ComboBox statusCombo;
        protected System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.ComboBox statusBayarCombo;
        protected System.Windows.Forms.Label label5;
    }
}
