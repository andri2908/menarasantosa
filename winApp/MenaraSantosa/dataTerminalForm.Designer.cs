﻿namespace AlphaSoft
{
    partial class dataTerminalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dataTerminalForm));
            this.SaveButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.newTerminal = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.terminalGrid = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.terminalIDValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.terminalType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.searchKompleksButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.kompleksTextBox = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.terminalGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(610, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // SaveButton
            // 
            this.SaveButton.BackColor = System.Drawing.Color.FloralWhite;
            this.SaveButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveButton.ForeColor = System.Drawing.Color.Black;
            this.SaveButton.Location = new System.Drawing.Point(229, 508);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(182, 37);
            this.SaveButton.TabIndex = 65;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = false;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.newTerminal);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.terminalGrid);
            this.panel2.Controls.Add(this.searchKompleksButton);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.kompleksTextBox);
            this.panel2.Location = new System.Drawing.Point(29, 45);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(553, 446);
            this.panel2.TabIndex = 64;
            // 
            // newTerminal
            // 
            this.newTerminal.BackColor = System.Drawing.Color.FloralWhite;
            this.newTerminal.Enabled = false;
            this.newTerminal.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newTerminal.ForeColor = System.Drawing.Color.Black;
            this.newTerminal.Location = new System.Drawing.Point(140, 48);
            this.newTerminal.Name = "newTerminal";
            this.newTerminal.Size = new System.Drawing.Size(156, 37);
            this.newTerminal.TabIndex = 106;
            this.newTerminal.Text = "New Terminal";
            this.newTerminal.UseVisualStyleBackColor = false;
            this.newTerminal.Click += new System.EventHandler(this.newTerminal_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(24, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 18);
            this.label4.TabIndex = 105;
            this.label4.Text = "Terminal";
            // 
            // terminalGrid
            // 
            this.terminalGrid.AllowUserToAddRows = false;
            this.terminalGrid.AllowUserToDeleteRows = false;
            this.terminalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.terminalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.terminalGrid.BackgroundColor = System.Drawing.Color.FloralWhite;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.terminalGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.terminalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.terminalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.flag,
            this.terminalIDValue,
            this.terminalType});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.terminalGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.terminalGrid.Location = new System.Drawing.Point(27, 108);
            this.terminalGrid.MultiSelect = false;
            this.terminalGrid.Name = "terminalGrid";
            this.terminalGrid.RowHeadersVisible = false;
            this.terminalGrid.Size = new System.Drawing.Size(491, 319);
            this.terminalGrid.TabIndex = 64;
            this.terminalGrid.CurrentCellDirtyStateChanged += new System.EventHandler(this.terminalGrid_CurrentCellDirtyStateChanged);
            this.terminalGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.terminalGrid_KeyDown);
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.Visible = false;
            this.id.Width = 29;
            // 
            // flag
            // 
            this.flag.HeaderText = "flag";
            this.flag.Name = "flag";
            this.flag.Visible = false;
            this.flag.Width = 45;
            // 
            // terminalIDValue
            // 
            this.terminalIDValue.HeaderText = "Terminal ID";
            this.terminalIDValue.Name = "terminalIDValue";
            this.terminalIDValue.Width = 127;
            // 
            // terminalType
            // 
            this.terminalType.HeaderText = "Tipe";
            this.terminalType.Name = "terminalType";
            this.terminalType.Width = 49;
            // 
            // searchKompleksButton
            // 
            this.searchKompleksButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchKompleksButton.BackgroundImage")));
            this.searchKompleksButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchKompleksButton.Location = new System.Drawing.Point(447, 13);
            this.searchKompleksButton.Name = "searchKompleksButton";
            this.searchKompleksButton.Size = new System.Drawing.Size(29, 30);
            this.searchKompleksButton.TabIndex = 60;
            this.searchKompleksButton.UseVisualStyleBackColor = true;
            this.searchKompleksButton.Click += new System.EventHandler(this.searchKompleksButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(44, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 18);
            this.label2.TabIndex = 58;
            this.label2.Text = "Kompleks";
            // 
            // kompleksTextBox
            // 
            this.kompleksTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.kompleksTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kompleksTextBox.Location = new System.Drawing.Point(140, 15);
            this.kompleksTextBox.MaxLength = 30;
            this.kompleksTextBox.Name = "kompleksTextBox";
            this.kompleksTextBox.ReadOnly = true;
            this.kompleksTextBox.Size = new System.Drawing.Size(300, 27);
            this.kompleksTextBox.TabIndex = 59;
            // 
            // dataTerminalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(610, 590);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.panel2);
            this.Name = "dataTerminalForm";
            this.Load += new System.EventHandler(this.dataTerminalForm_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.SaveButton, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.terminalGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Panel panel2;
        protected System.Windows.Forms.Button newTerminal;
        protected System.Windows.Forms.Label label4;
        protected System.Windows.Forms.DataGridView terminalGrid;
        private System.Windows.Forms.Button searchKompleksButton;
        protected System.Windows.Forms.Label label2;
        protected System.Windows.Forms.TextBox kompleksTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn terminalIDValue;
        private System.Windows.Forms.DataGridViewComboBoxColumn terminalType;
    }
}
