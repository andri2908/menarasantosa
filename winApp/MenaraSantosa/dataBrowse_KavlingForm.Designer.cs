﻿namespace AlphaSoft
{
    partial class dataBrowse_KavlingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.blokTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Size = new System.Drawing.Size(92, 18);
            this.label1.Text = "Kompleks";
            // 
            // namaTextBox
            // 
            this.namaTextBox.Location = new System.Drawing.Point(107, 27);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.blokTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(52, 18);
            this.groupBox1.Size = new System.Drawing.Size(671, 91);
            this.groupBox1.Controls.SetChildIndex(this.namaTextBox, 0);
            this.groupBox1.Controls.SetChildIndex(this.nonActiveCheckBox, 0);
            this.groupBox1.Controls.SetChildIndex(this.label1, 0);
            this.groupBox1.Controls.SetChildIndex(this.label2, 0);
            this.groupBox1.Controls.SetChildIndex(this.blokTextBox, 0);
            // 
            // displayAllButton
            // 
            this.displayAllButton.Location = new System.Drawing.Point(376, 115);
            // 
            // nonActiveCheckBox
            // 
            this.nonActiveCheckBox.Location = new System.Drawing.Point(107, 60);
            // 
            // newButton
            // 
            this.newButton.Location = new System.Drawing.Point(187, 115);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(396, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 18);
            this.label2.TabIndex = 34;
            this.label2.Text = "Blok";
            // 
            // blokTextBox
            // 
            this.blokTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.blokTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.blokTextBox.Location = new System.Drawing.Point(447, 27);
            this.blokTextBox.Name = "blokTextBox";
            this.blokTextBox.Size = new System.Drawing.Size(205, 27);
            this.blokTextBox.TabIndex = 35;
            this.blokTextBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // dataBrowse_KavlingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(754, 629);
            this.Name = "dataBrowse_KavlingForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.dataBrowse_KavlingForm_FormClosed);
            this.Load += new System.EventHandler(this.dataBrowse_KavlingForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Label label2;
        protected System.Windows.Forms.TextBox blokTextBox;
    }
}
