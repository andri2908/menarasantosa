﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace AlphaSoft
{
    public partial class dataKavlingDetailForm : basicHotkeysForm
    {
        private int originModuleID = 0;
        private int selectedKavlingID = 0;
        private int selectedBlokID = 0;
        private int selectedKompleksID = 0;

        private globalUserUtil gUser;
        private globalRestAPI gRest;

        private CultureInfo culture = new CultureInfo("id-ID");

        public int returnKavlingIDValue = 0;

        public dataKavlingDetailForm(int kavlingID = 0, int moduleId = 0)
        {
            InitializeComponent();
            selectedKavlingID = kavlingID;

            gUser = new globalUserUtil();
            gRest = new globalRestAPI();

            if (selectedKavlingID > 0)
            {
                if (moduleId == 0)
                    originModuleID = globalConstants.EDIT_KAVLING;
                else
                    originModuleID = moduleId;

                if (!gUser.RS_userHasAccessTo(MMConstants.TAMBAH_HAPUS_KAVLING,
                MMConstants.HAK_UPDATE_DATA))
                {
                    SaveButton.Enabled = false;
                    ResetButton.Enabled = false;
                }
            }
            else
                originModuleID = globalConstants.NEW_KAVLING;
        }

        private void dataKavlingDetailForm_Load(object sender, EventArgs e)
        {
            if (selectedKavlingID > 0)
            {
                loadKavlingDataInformation();
                resetJual.Enabled = true;
                resetSewa.Enabled = true;
            }

            searchKompleksButton.Focus();
            gUser.reArrangeTabOrder(this);

            biayaIPLTextBox.Enter += TextBox_Enter;
            biayaIPLTextBox.Leave += TextBox_Int32_Leave;

            durasiBayarTextBox.Enter += TextBox_Enter;
            durasiBayarTextBox.Leave += TextBox_Int32_Leave;

            luasTanahTextBox.Enter += TextBox_Enter;
            luasTanahTextBox.Leave += TextBox_Leave;

            luasBangunanTextBox.Enter += TextBox_Enter;
            luasBangunanTextBox.Leave += TextBox_Leave;

            if (originModuleID == globalConstants.VIEW_ONLY)
            {
                gUser.disableAllControls(this);
            }
        }

        private void dataKavlingDetailForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            returnKavlingIDValue = selectedKavlingID;
        }

        private void loadKavlingDataInformation()
        {
            DataTable dt = new DataTable();
            REST_masterKavling kavlingData = new REST_masterKavling();

            string sqlCommand = "SELECT mk.*, IFNULL(ms.kompleks_name, '') AS kompleks_name, IFNULL(mb.blok_name, '') AS blok_name " +
                                            "FROM master_kavling mk " +
                                            "LEFT OUTER JOIN master_kompleks ms ON (mk.kompleks_id = ms.kompleks_id) " +
                                            "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id) " +
                                            "WHERE kavling_id =  " + selectedKavlingID + " ";

            if (gRest.getMasterKavlingData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref kavlingData))
            {
                if (kavlingData.dataStatus.o_status == 1)
                {
                    selectedKompleksID = kavlingData.dataList[0].kompleks_id;
                    kompleksTextBox.Text = kavlingData.dataList[0].kompleks_name;

                    selectedBlokID = kavlingData.dataList[0].blok_id;
                    namaBlokTextBox.Text = kavlingData.dataList[0].blok_name;

                    noRumahTextBox.Text = kavlingData.dataList[0].house_no;

                    biayaIPLTextBox.Text = kavlingData.dataList[0].biaya_ipl.ToString("N0", culture);
                    durasiBayarTextBox.Text = kavlingData.dataList[0].durasi_pembayaran.ToString("N0", culture);

                    luasTanahTextBox.Text = kavlingData.dataList[0].luas_tanah.ToString("N2", culture);
                    luasBangunanTextBox.Text = kavlingData.dataList[0].luas_bangunan.ToString("N2", culture);

                    nonAktifCheckbox.Checked = (kavlingData.dataList[0].is_active == "Y" ? false : true);
                }
            }
        }

        private void searchKompleksButton_Click(object sender, EventArgs e)
        {
            dataBrowse_KompleksForm displayForm = new dataBrowse_KompleksForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                searchBlokButton.Enabled = true;

                selectedKompleksID = Convert.ToInt32(displayForm.ReturnValue1);
                kompleksTextBox.Text = displayForm.ReturnValue2;
                biayaIPLTextBox.Text = Convert.ToDouble(displayForm.ret_biayaIPL).ToString("N0", culture);
                durasiBayarTextBox.Text = Convert.ToDouble(displayForm.ret_durasiBayar).ToString("N0", culture);
            }
        }

        private void searchBlokButton_Click(object sender, EventArgs e)
        {
            dataBrowse_BlokForm displayForm = new dataBrowse_BlokForm(globalConstants.MODULE_DEFAULT, selectedKompleksID);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                selectedBlokID = Convert.ToInt32(displayForm.ReturnValue1);
                namaBlokTextBox.Text = displayForm.ReturnValue2;
            }
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            gUser.ResetAllControls(this);

            originModuleID = globalConstants.NEW_KAVLING;

            selectedBlokID = 0;
            selectedKompleksID = 0;
            selectedKavlingID = 0;

            searchKompleksButton.Select();
        }

        private bool dataValid()
        {
            errorLabel.Text = "";
            genericReply replyResult = new genericReply();

            if (nonAktifCheckbox.Checked)
            {
                if (DialogResult.No == MessageBox.Show("Non aktifkan kavling ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    return false;
                }

                string sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                                    "FROM user_kavling " +
                                                    "WHERE kavling_id = " + selectedKavlingID + " " +
                                                    "AND is_active = 'Y'";

                if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
                {
                    if (replyResult.o_status == 1)
                    {
                        if (Convert.ToInt32(replyResult.data) > 0)
                        {
                            errorLabel.Text = "Masih ada user terdaftar sebagai pemilik kavling";
                            return false;
                        }
                    }
                }

                return true;
            }

            if (selectedKompleksID == 0)
            {
                errorLabel.Text = "Kompleks belum dipilih";
                return false;
            }

            if (selectedBlokID == 0)
            {
                errorLabel.Text = "Blok belum dipilih";
                return false;
            }

            if (gUser.allTrim(noRumahTextBox.Text).Length <= 0)
            {
                errorLabel.Text = "No rumah kosong";
                return false;
            }

            double tempVal = 0;
            TextBox[] txtBox = { biayaIPLTextBox, durasiBayarTextBox, luasBangunanTextBox, luasTanahTextBox };
            string[] txtWarning = { "biaya IPL", "durasi pembayaran", "Luas Bangunan", "Luas Tanah" };
            for (int i = 0;i<txtBox.Length;i++)
            {
                if (null == txtBox[i].Text || 
                    !double.TryParse(txtBox[i].Text, NumberStyles.Number, culture, out tempVal))
                {
                    errorLabel.Text = "Input " + txtWarning[i] + " salah";
                    return false;
                }
            }

            return true;
        }

        private bool saveTrans()
        {
            bool result = false;
            int opType = 0;
            double biayaIPL = 0;
            double luasTanah = 0;
            double luasBangunan = 0;
            int durasiBayar = 0;
            string errMsg = "";
            int newID = 0;

            List<master_kavling> listKavling = new List<master_kavling>();
            master_kavling kavlingData = new master_kavling();

            try
            {
                double.TryParse(biayaIPLTextBox.Text, NumberStyles.Number, culture, out biayaIPL);
                int.TryParse(durasiBayarTextBox.Text, NumberStyles.Number, culture, out durasiBayar);

                double.TryParse(luasTanahTextBox.Text, NumberStyles.Number, culture, out luasTanah);
                double.TryParse(luasBangunanTextBox.Text, NumberStyles.Number, culture, out luasBangunan);

                switch (originModuleID)
                {
                    case globalConstants.NEW_KAVLING:
                        opType = 1;
                        kavlingData.kavling_id = 0;
                        break;

                    case globalConstants.EDIT_KAVLING:
                        opType = 2;
                        kavlingData.kavling_id = selectedKavlingID;
                        break;
                }

                kavlingData.kompleks_id = selectedKompleksID;
                kavlingData.blok_id = selectedBlokID;
                kavlingData.house_no = noRumahTextBox.Text;
                kavlingData.biaya_ipl = biayaIPL;
                kavlingData.luas_bangunan = luasBangunan;
                kavlingData.luas_tanah = luasTanah;
                kavlingData.durasi_pembayaran = durasiBayar;
                kavlingData.is_active = (nonAktifCheckbox.Checked ? "N" : "Y");

                listKavling.Add(kavlingData);

                if (!gRest.saveMasterKavling(gUser.getUserID(), gUser.getUserToken(), listKavling, opType, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                if (opType == 1)
                    selectedKavlingID = newID;

                result = true;
            }
            catch(Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private bool saveData()
        {
            if (dataValid())
                return saveTrans();

            return false;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (saveData())
            {
                if (DialogResult.Yes == MessageBox.Show("Success, Input lagi ?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    ResetButton.PerformClick();
                }
                else
                    this.Close();
            }
            else
            {
                MessageBox.Show("Fail");
            }
        }

        private void resetSewa_Click(object sender, EventArgs e)
        {
            string errMsg="";

            if (DialogResult.No == MessageBox.Show("Reset status sewa ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                return;

            try
            {
                if (!gRest.resetStatusKavling(gUser.getUserID(), gUser.getUserToken(), 1, selectedKavlingID, out errMsg))
                {
                    throw new Exception(errMsg);
                }
            }
            catch (Exception ex)
            {
                errorLabel.Text = errMsg;
            }
        }

        private void resetJual_Click(object sender, EventArgs e)
        {
            string errMsg = "";

            if (DialogResult.No == MessageBox.Show("Reset status jual ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                return;

            try
            {
                if (!gRest.resetStatusKavling(gUser.getUserID(), gUser.getUserToken(), 2, selectedKavlingID, out errMsg))
                {
                    throw new Exception(errMsg);
                }
            }
            catch (Exception ex)
            {
                errorLabel.Text = errMsg;
            }
        }
    }
}
