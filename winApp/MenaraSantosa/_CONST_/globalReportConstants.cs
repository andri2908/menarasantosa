﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaSoft
{
    class globalReportConstants
    {
        public const int _START_PRINTOUT_ = 300;

        #region PRINTOUT CONST
        public const int PRINTOUT_IPL = 301;
        public const int PRINTOUT_TRANSAKSI_RETAIL = 302;
        public const int PRINTOUT_TRANSAKSI_PENERIMAAN = 303;
        public const int PRINTOUT_PENGIRIMAN_TRANSAKSI = 304;
        public const int PRINTOUT_TAGIHAN_IPL_MANUAL = 305;
        #endregion

        public const int _END_PRINTOUT_ = 400;


        public const int START_REPORT = 501;

        #region REPORT CONST
        public const int REPORT_SUMMARY_IPL = 502;
        public const int REPORT_IPL_TAHUNAN = 503;
        public const int REPORT_AKSES_KOMPLEKS = 504;
        public const int REPORT_KAVLING = 505;
        #endregion

        public const int END_REPORT = 600;


        public const string transaksiXML = "transaksi.xml";
        public const string tagihanIPLXML = "tagihanIPL.xml";

        public const string reportIPLKompleksXML = "reportIPLKompleks.xml";
        public const string reportIPLTahunanXML = "reportIPLTahunan.xml";
        public const string reportAksesKompleksXML = "reportAksesKompleks.xml";
        public const string reportKavlingXML = "reportKavling.xml";
    }
}
