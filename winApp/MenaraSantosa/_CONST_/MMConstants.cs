﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaSoft
{
    class MMConstants
    {
        public const int HAK_AKSES = 1;
        public const int HAK_NEW_DATA = 2;
        public const int HAK_UPDATE_DATA = 4;

        public const int MANAJEMEN_SISTEM = 1;
        public const int TAMBAH_HAPUS_GROUP_USER = 2;
        public const int TAMBAH_HAPUS_USER = 3;
        public const int PENGATURAN_AKSES_MODUL = 4;
        public const int BACKUP_RESTORE_DATABASE = 5;
        public const int PENGATURAN_SISTEM_APLIKASI = 6;

        public const int TAMBAH_HAPUS_KOMPLEKS = 21;
        public const int TAMBAH_HAPUS_KAVLING = 22;

        public const int TAMBAH_HAPUS_ITEM = 23;
        public const int TAMBAH_HAPUS_UNIT = 24;
        public const int TAMBAH_HAPUS_PEMILIK_KAVLING = 25;
        public const int TAMBAH_HAPUS_ITEM_DRUGS = 26;
        public const int TAMBAH_HAPUS_NEWS = 27;

        public const int RINGKASAN_IPL = 41;
        public const int TAMBAH_HAPUS_RETAIL = 42;
        public const int PENYESUAIAN_STOK = 43;
        public const int PENERIMAAN_BARANG = 44;

        public const int TAMBAH_HAPUS_DRUGS = 45;
        public const int PENYESUAIAN_DRUGS = 46;
        public const int PENERIMAAN_DRUGS = 47;

        public const int TAMBAH_HAPUS_EMPLOYEE = 48;


        public const int SECOND_AUTH = 999;
    }
}
