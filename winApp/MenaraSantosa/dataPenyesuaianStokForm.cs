﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace AlphaSoft
{
    public partial class dataPenyesuaianStokForm : AlphaSoft.basicHotkeysForm
    {
        private CultureInfo culture = new CultureInfo("id-ID");

        private int selectedItemID = 0;
        private globalUserUtil gUser;
        private globalItemLib gItem;
        private globalRestAPI gRest;

        public dataPenyesuaianStokForm(int itemID = 0)
        {
            InitializeComponent();
            selectedItemID = itemID;

            gUser = new globalUserUtil();
            gItem = new globalItemLib();
            gRest = new globalRestAPI();
        }

        private void dataPenyesuaianStokForm_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void loadDataInformation()
        {
            DataTable dt = new DataTable();
            REST_masterItem itemData = new REST_masterItem();

            string sqlCommand = "SELECT mi.*, IFNULL(mu.unit_name, '') AS unit_name " +
                                            "FROM master_item mi " +
                                            "LEFT OUTER JOIN master_unit mu ON (mi.unit_id = mu.unit_id) " +
                                            "WHERE item_id =  " + selectedItemID;

            if (gRest.getMasterItemData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref itemData))
            {
                if (itemData.dataStatus.o_status == 1)
                {
                    namaTextBox.Text = itemData.dataList[0].item_name;
                    unitTextBox.Text = itemData.dataList[0].unit_name;

                    qtyTextBox.Text = Convert.ToDouble(itemData.dataList[0].qty).ToString("N0", culture);
                    qtyBaruTextBox.Text = Convert.ToDouble(itemData.dataList[0].qty).ToString("N0", culture);
                }
            }
        }

        private void dataPenyesuaianStokForm_Load(object sender, EventArgs e)
        {
            loadDataInformation();

            qtyBaruTextBox.Focus();
            gUser.reArrangeTabOrder(this);

            qtyTextBox.Enter += TextBox_Enter;
            qtyTextBox.Leave += TextBox_Int32_Leave;

            qtyBaruTextBox.Enter += TextBox_Enter;
            qtyBaruTextBox.Leave += TextBox_Int32_Leave;
        }

        private bool saveDataTransaction()
        {
            bool result = false;
            string sqlCommand = "";
            string errMsg = "";
            double qtyValue = 0;
            double qtyBaru = 0;

            genericReply replyResult = new genericReply();

            List<penyesuaian_stok> listPenyesuaian = new List<penyesuaian_stok>();
            penyesuaian_stok stokData;

            try
            {
                double.TryParse(qtyTextBox.Text, NumberStyles.Number, culture, out qtyValue);
                double.TryParse(qtyBaruTextBox.Text, NumberStyles.Number, culture, out qtyBaru);

                stokData = new penyesuaian_stok();
                stokData.item_id = selectedItemID;
                stokData.qty_awal = qtyValue;
                stokData.qty_baru = qtyBaru;
                stokData.keterangan = remarkTextBox.Text;

                listPenyesuaian.Add(stokData);

                int newID = 0;
                if (!gRest.savePenyesuaian(gUser.getUserID(), gUser.getUserToken(), listPenyesuaian, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }
                
                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private bool dataValidated()
        {
            errorLabel.Text = "";

            if (remarkTextBox.Text.Length <= 0)
            {
                errorLabel.Text = "Keterangan kosong";
                return false;
            }

            double tempVal = 0;
            TextBox[] txtBox = { qtyTextBox, qtyBaruTextBox};
            string[] errMsg = { "Qty Asal", "Qty Baru", "HPP" };

            for (int i = 0; i < txtBox.Length; i++)
            {
                if (txtBox[i].Text.Length <= 0 ||
                        !double.TryParse(txtBox[i].Text, NumberStyles.Number, culture, out tempVal))
                {
                    errorLabel.Text = "Input " + errMsg[i] + "salah";
                    return false;
                }
            }

            return true;
        }

        private bool saveData()
        {
            if (dataValidated())
                return saveDataTransaction();

            return false;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (saveData())
            {
                MessageBox.Show("Success");

                this.Close();
            }
            else
            {
                MessageBox.Show("Fail");
            }
        }
    }
}
