﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace AlphaSoft
{
    public partial class dataBrowse_PenerimaanForm : AlphaSoft.browseRangeTextBoxForm
    {
        private int originModuleID = 0;
        private globalUserUtil gUtil;
        private globalRestAPI gRest;

        private CultureInfo culture = new CultureInfo("id-ID");

        public dataBrowse_PenerimaanForm(int moduleID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();
        }

        protected override void newButtonAction()
        {
            dataPenerimaanForm displayForm = new dataPenerimaanForm(originModuleID);
            displayForm.ShowDialog(this);
        }

        private void dataBrowse_PenerimaanForm_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void dataBrowse_PenerimaanForm_Load(object sender, EventArgs e)
        {
            displayAllAction();

            if (!gUtil.RS_userHasAccessTo(MMConstants.PENERIMAAN_BARANG,
                MMConstants.HAK_NEW_DATA))
                setNewButtonEnable(false);
        }

        protected override void displayAllAction()
        {
            switch (originModuleID)
            {
                default:
                    displayDefault();
                    break;
            }
        }

        private void displayDefault()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";

            string startDate = String.Format(culture, "{0:yyyyMMdd}", startDTPicker.Value.Date);
            string endDate = String.Format(culture, "{0:yyyyMMdd}", endDTPicker.Value.Date);

            REST_penerimaanHeader transData = new REST_penerimaanHeader();
            dataGridView.DataSource = null;

            sqlCommand = "SELECT * " + 
                                    "FROM penerimaan_header th " +
                                    "WHERE 1 =1 " +
                                    "AND DATE_FORMAT(penerimaan_datetime, '%Y%m%d') >= '" + startDate + "' " +
                                    "AND DATE_FORMAT(penerimaan_datetime, '%Y%m%d') <= '" + endDate + "' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND th.is_active = 'Y' ";

            if (originModuleID == globalConstants.MODULE_PENERIMAAN_BARANG)
            {
                sqlCommand += "AND th.type_trans = " + globalConstants.TRANS_TYPE_RETAIL + " ";
            }
            else if (originModuleID == globalConstants.MODULE_PENERIMAAN_DRUGS)
            {
                sqlCommand += "AND th.type_trans = " + globalConstants.TRANS_TYPE_RETAIL_DRUGS + " ";
            }

            sqlCommand = sqlCommand + "ORDER BY th.penerimaan_datetime ASC";

            if (gRest.getPenerimaanHeaderData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref transData))
            {
                if (transData.dataStatus.o_status == 1)
                {
                    dataGridView.DataSource = null;

                    dt.Columns.Add("PENERIMAAN_ID");
                    dt.Columns.Add("TGL");
                    dt.Columns.Add("TOTAL", typeof(double));

                    for (int i = 0; i < transData.dataList.Count; i++)
                    {
                        DataRow r = dt.Rows.Add();
                        r["PENERIMAAN_ID"] = transData.dataList[i].penerimaan_id;
                        r["TGL"] = String.Format(culture, "{0:dd MMMM yyyy}", transData.dataList[i].penerimaan_datetime);
                        r["TOTAL"] = transData.dataList[i].penerimaan_total;
                    }

                    dataGridView.DataSource = dt;
                    dataGridView.Columns["PENERIMAAN_ID"].Visible = false;

                    dataGridView.Columns["TOTAL"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridView.Columns["TOTAL"].DefaultCellStyle.Format = "N0";
                    dataGridView.Columns["TOTAL"].DefaultCellStyle.FormatProvider = new CultureInfo("id-ID");
                }
            }
        }

        protected override void dbGridDoubleClickAction(DataGridViewRow selectedRow)
        {
            int selectedID = Convert.ToInt32(selectedRow.Cells["PENERIMAAN_ID"].Value);

            switch (originModuleID)
            {
                case globalConstants.MODULE_DEFAULT:
                    ReturnValue1 = selectedID.ToString();

                    this.Close();
                    break;

                default:
                    dataPenerimaanForm editForm = new dataPenerimaanForm(originModuleID, selectedID);
                    editForm.ShowDialog(this);
                    break;
            }
        }
    }
}
