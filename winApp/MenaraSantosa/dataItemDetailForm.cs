﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace AlphaSoft
{
    public partial class dataItemDetailForm : AlphaSoft.basicHotkeysForm
    {
        private int originModuleID = 0;
        private int catItem = 0;
        private int selectedItemID = 0;
        private int selectedUnitID = 0;
        private string selectedImgFile = "";
        private string outputImgFile = "";

        private bool isFtpImage = false;

        private globalUserUtil gUser;
        private globalRestAPI gRest;
        private globalImageLib gImg;

        private CultureInfo culture = new CultureInfo("id-ID");

        public int returnItemIDValue = 0;

        public dataItemDetailForm(int itemID = 0, int moduleID = 0)
        {
            InitializeComponent();
            selectedItemID = itemID;

            gUser = new globalUserUtil();
            gRest = new globalRestAPI();
            gImg = new globalImageLib();
            catItem = moduleID;

            if (selectedItemID > 0)
            {
                originModuleID = globalConstants.EDIT_ITEM;

                if (!gUser.RS_userHasAccessTo(MMConstants.TAMBAH_HAPUS_ITEM,
                MMConstants.HAK_UPDATE_DATA))
                {
                    SaveButton.Enabled = false;
                    ResetButton.Enabled = false;
                }
            }
            else
                originModuleID = globalConstants.NEW_ITEM;

        }

        private void dataItemDetailForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            returnItemIDValue = selectedItemID;
            gImg.clearLocalImgDir(outputImgFile);
        }

        private void loadDataInformation()
        {
            DataTable dt = new DataTable();
            REST_masterItem itemData = new REST_masterItem();

            string sqlCommand = "SELECT mi.*, IFNULL(mu.unit_name, '') AS unit_name " +
                                            "FROM master_item mi " +
                                            "LEFT OUTER JOIN master_unit mu ON (mi.unit_id = mu.unit_id) " +
                                            "WHERE item_id =  " + selectedItemID;

            if (gRest.getMasterItemData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref itemData))
            {
                if (itemData.dataStatus.o_status == 1)
                {
                    namaTextBox.Text = itemData.dataList[0].item_name;
                    selectedUnitID = itemData.dataList[0].unit_id;
                    unitTextBox.Text = itemData.dataList[0].unit_name;

                    nonAktifCheckbox.Checked = (itemData.dataList[0].is_active == "Y" ? false : true);
                    checkBoxJasa.Checked = (itemData.dataList[0].item_jasa == "Y" ? true : false);

                    qtyTextBox.Text = Convert.ToDouble(itemData.dataList[0].qty).ToString("N0", culture);
                    hargaTextBox.Text = Convert.ToDouble(itemData.dataList[0].item_price).ToString("N0", culture);
                    hppTextBox.Text = Convert.ToDouble(itemData.dataList[0].item_hpp).ToString("N0", culture);

                    if (itemData.dataList[0].item_category_id > 0)
                    {
                        kategoriCombo.SelectedValue = itemData.dataList[0].item_category_id.ToString();
                    }

                    // LOAD IMAGE
                    outputImgFile = itemData.dataList[0].image_file;

                    if (outputImgFile.Length > 0)
                    {
                        try
                        {
                            Bitmap bm;
                            bm = gImg.ByteToImage(outputImgFile, globalImageLib.IMG_PRODUCTS);
                            pictureBox1.Image = bm;

                            //pictureBox1.Load(gImg.localImgDirectory + outputImgFile);

                            isFtpImage = true;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("File gambar tidak ditemukan " + ex.Message);
                            pictureBox1.Load(gImg.localImgDirectory + "errImg.jpeg");
                        }
                    }       
                }
            }
        }

        private void loadDataKategoriItem()
        {
            string sqlCommand = "";

            REST_itemCategory listItem = new REST_itemCategory();
            DataTable dt = new DataTable();

            sqlCommand = "SELECT item_category_id, item_category_name " +
                                    "FROM item_category " +
                                    "WHERE is_active = 'Y' " +
                                    "ORDER BY item_category_id ASC";

            if (gRest.getItemCategoryData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref listItem))
            {
                if (listItem.dataStatus.o_status == 1)
                {
                    dt.Columns.Add("item_category_id");
                    dt.Columns.Add("item_category_name");

                    for (int i= 0;i<listItem.dataList.Count;i++)
                    {
                        DataRow r = dt.Rows.Add();
                        r["item_category_id"] = listItem.dataList[i].item_category_id;
                        r["item_category_name"] = listItem.dataList[i].item_category_name;
                    }

                    kategoriCombo.DataSource = dt;
                    kategoriCombo.ValueMember = "item_category_id";
                    kategoriCombo.DisplayMember = "item_category_name";
                    kategoriCombo.SelectedIndex = 0;
                }
            }
        }

        private void dataItemDetailForm_Load(object sender, EventArgs e)
        {
            loadDataKategoriItem();

            if (selectedItemID > 0)
            {
                loadDataInformation();
            }

            if (catItem == globalConstants.MODULE_ITEM_DRUGS ||
                catItem == globalConstants.MODULE_TRANSAKSI_DRUGS ||
                catItem == globalConstants.MODULE_PENERIMAAN_DRUGS)
                kategoriCombo.SelectedValue = "2";

            kategoriCombo.Enabled = false;

            namaTextBox.Focus();
            gUser.reArrangeTabOrder(this);

            qtyTextBox.Enter += TextBox_Enter;
            qtyTextBox.Leave += TextBox_Int32_Leave;

            hargaTextBox.Enter += TextBox_Enter;
            hargaTextBox.Leave += TextBox_Int32_Leave;

            hppTextBox.Enter += TextBox_Enter;
            hppTextBox.Leave += TextBox_Int32_Leave;
        }

        private bool saveDataTransaction()
        {
            bool result = false;
            string errMsg = "";

            List<master_item> listItem = new List<master_item>();
            master_item itemData = new master_item();

            double qtyValue = 0;
            double hargaValue = 0;
            double hppValue = 0;
            int opType = 0;

            try
            {
                double.TryParse(qtyTextBox.Text, NumberStyles.Number, culture, out qtyValue);
                double.TryParse(hargaTextBox.Text, NumberStyles.Number, culture, out hargaValue);
                double.TryParse(hppTextBox.Text, NumberStyles.Number, culture, out hppValue);

                if (selectedImgFile.Length > 0)
                {
                    resizeAndGetNewName();
                    if (!gImg.uploadToFTP(outputImgFile, globalImageLib.IMG_PRODUCTS))
                    {
                        throw new Exception("Upload Gambar Gagal");
                    }
                }

                switch (originModuleID)
                {
                    case globalConstants.NEW_ITEM:
                        opType = 1;
                        itemData.item_id = 0;

                        break;

                    case globalConstants.EDIT_ITEM:
                        opType = 2;
                        itemData.item_id = selectedItemID;
                        break;
                }

                itemData.item_name = namaTextBox.Text;
                itemData.unit_id = selectedUnitID;
                itemData.item_hpp= hppValue;
                itemData.item_price= hargaValue;
                itemData.qty= qtyValue;
                itemData.item_jasa = (checkBoxJasa.Checked ? "Y" : "N");
                itemData.image_file = outputImgFile;
                itemData.item_category_id = Convert.ToInt32(kategoriCombo.SelectedValue);
                itemData.is_active = (nonAktifCheckbox.Checked ? "N" : "Y");

                listItem.Add(itemData);

                int newID = 0;
                if (!gRest.saveMasterItem(gUser.getUserID(), gUser.getUserToken(), listItem, opType, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                if (opType == 1)
                    selectedItemID = newID;

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private bool dataValidated()
        {
            errorLabel.Text = "";
            genericReply replyResult = new genericReply();
            string sqlCommand = "";

            if (namaTextBox.Text.Length <= 0)
            {
                errorLabel.Text = "Nama item kosong";
                return false;
            }

            sqlCommand = "SELECT COUNT(1) AS resultQuery FROM master_item " +
                                    "WHERE item_name = '" + namaTextBox.Text + "' " +
                                    "AND item_id <> " + selectedItemID;

            if (gRest.checkDataExist(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                {
                    errorLabel.Text = "Nama item sudah ada";
                    return false;
                }
            }

            if (selectedUnitID == 0)
            {
                errorLabel.Text = "Satuan belum dipilih";
                return false;
            }

            double tempVal = 0;
            TextBox[] txtBox = { qtyTextBox, hargaTextBox, hppTextBox };
            string[] errMsg = { "Qty", "Harga", "HPP" };

            for (int i =0;i<txtBox.Length;i++)
            {
                if (txtBox[i].Text.Length <= 0 ||
                        !double.TryParse(txtBox[i].Text, NumberStyles.Number, culture, out tempVal))
                {
                    errorLabel.Text = "Input " + errMsg[i] + " salah";
                    return false;
                }
            }

            return true;
        }

        private bool saveData()
        {
            if (dataValidated())
                return saveDataTransaction();

            return false;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (saveData())
            {
                if (DialogResult.Yes == MessageBox.Show("Success, input lagi ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    ResetButton.PerformClick();
                }
                else
                {
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("FAIL");
            }
        }

        private void searchUnitButton_Click(object sender, EventArgs e)
        {
            dataBrowse_UnitForm displayForm = new dataBrowse_UnitForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                selectedUnitID = Convert.ToInt32(displayForm.ReturnValue1);
                unitTextBox.Text = displayForm.ReturnValue2;
            }
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            gUser.ResetAllControls(this);

            originModuleID = globalConstants.NEW_ITEM;
            selectedItemID = 0;
            selectedUnitID = 0;

            loadDataKategoriItem();
            if (catItem == globalConstants.MODULE_ITEM_DRUGS ||
                catItem == globalConstants.MODULE_TRANSAKSI_DRUGS ||
                catItem == globalConstants.MODULE_PENERIMAAN_DRUGS)
                kategoriCombo.SelectedValue = "2";

            pictureBox1.Image= null;
        }

        private void checkBoxJasa_CheckedChanged(object sender, EventArgs e)
        {
            qtyTextBox.Enabled = !checkBoxJasa.Checked;

            if (checkBoxJasa.Checked)
                qtyTextBox.Text = "0";
        }

        private void searchImageButton_Click(object sender, EventArgs e)
        {
            string fileName = "";

            imgFileDialog.Filter = "Image Files(*.JPG; *.JPEG)| *.JPG; *.JPEG";
            if (imgFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    fileName = imgFileDialog.FileName;
                    pictureBox1.Load(fileName);
                    selectedImgFile = imgFileDialog.FileName;

                    isFtpImage = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("File gambar tidak bisa dibuka : " + ex.Message);
                    pictureBox1.Load(gImg.localImgDirectory + "errImg.jpeg");
                }
            }
        }

        private void resizeAndGetNewName()
        {
            string sqlCommand = "";
            string inputName = String.Format(culture, "{0:yyyyMMddhhmmss}", DateTime.Now) + gUser.getRandomString(15);

           // if (outputImgFile.Length <= 0)
            {
                while (true)
                {
                    outputImgFile = gUser.getMD5Value(inputName) + ".jpg";

                    sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                            "FROM master_item mi " +
                                            "WHERE mi.image_file = '" + outputImgFile + "' " +
                                            "AND mi.is_active = 'Y'";

                    genericReply replyResult = new genericReply();
                    if (gRest.checkDataExist(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
                    {
                        if (Convert.ToInt32(replyResult.data) <= 0)
                        {
                            break;
                        }
                    }
                }
            }

            gImg.ResizeImage(selectedImgFile, outputImgFile);
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            string fileName = "";

            if (isFtpImage)
                fileName = outputImgFile;
            else
                fileName = selectedImgFile;

            if (fileName.Length > 0)
            {
                displayImageForm displayForm = new displayImageForm(fileName, globalImageLib.IMG_PRODUCTS, isFtpImage);
                displayForm.ShowDialog(this);
            }
        }
    }
}
