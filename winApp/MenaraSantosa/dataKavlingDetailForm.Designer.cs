﻿namespace AlphaSoft
{
    partial class dataKavlingDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dataKavlingDetailForm));
            this.ResetButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.resetJual = new System.Windows.Forms.Button();
            this.resetSewa = new System.Windows.Forms.Button();
            this.luasBangunanTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.luasTanahTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.durasiBayarTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.biayaIPLTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.searchBlokButton = new System.Windows.Forms.Button();
            this.searchKompleksButton = new System.Windows.Forms.Button();
            this.noRumahTextBox = new System.Windows.Forms.TextBox();
            this.nonAktifCheckbox = new System.Windows.Forms.CheckBox();
            this.namaBlokTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.kompleksTextBox = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(656, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // ResetButton
            // 
            this.ResetButton.BackColor = System.Drawing.Color.FloralWhite;
            this.ResetButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResetButton.ForeColor = System.Drawing.Color.Black;
            this.ResetButton.Location = new System.Drawing.Point(344, 326);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(180, 37);
            this.ResetButton.TabIndex = 58;
            this.ResetButton.Text = "Reset";
            this.ResetButton.UseVisualStyleBackColor = false;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.BackColor = System.Drawing.Color.FloralWhite;
            this.SaveButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveButton.ForeColor = System.Drawing.Color.Black;
            this.SaveButton.Location = new System.Drawing.Point(132, 326);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(182, 37);
            this.SaveButton.TabIndex = 57;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = false;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox1.Controls.Add(this.resetJual);
            this.groupBox1.Controls.Add(this.resetSewa);
            this.groupBox1.Controls.Add(this.luasBangunanTextBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.luasTanahTextBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.durasiBayarTextBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.biayaIPLTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.searchBlokButton);
            this.groupBox1.Controls.Add(this.searchKompleksButton);
            this.groupBox1.Controls.Add(this.noRumahTextBox);
            this.groupBox1.Controls.Add(this.nonAktifCheckbox);
            this.groupBox1.Controls.Add(this.namaBlokTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.kompleksTextBox);
            this.groupBox1.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(18, 45);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(626, 259);
            this.groupBox1.TabIndex = 56;
            this.groupBox1.TabStop = false;
            // 
            // resetJual
            // 
            this.resetJual.BackColor = System.Drawing.Color.FloralWhite;
            this.resetJual.Enabled = false;
            this.resetJual.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetJual.ForeColor = System.Drawing.Color.Black;
            this.resetJual.Location = new System.Drawing.Point(436, 207);
            this.resetJual.Name = "resetJual";
            this.resetJual.Size = new System.Drawing.Size(184, 37);
            this.resetJual.TabIndex = 69;
            this.resetJual.Text = "Reset Status Jual";
            this.resetJual.UseVisualStyleBackColor = false;
            this.resetJual.Click += new System.EventHandler(this.resetJual_Click);
            // 
            // resetSewa
            // 
            this.resetSewa.BackColor = System.Drawing.Color.FloralWhite;
            this.resetSewa.Enabled = false;
            this.resetSewa.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetSewa.ForeColor = System.Drawing.Color.Black;
            this.resetSewa.Location = new System.Drawing.Point(436, 164);
            this.resetSewa.Name = "resetSewa";
            this.resetSewa.Size = new System.Drawing.Size(184, 37);
            this.resetSewa.TabIndex = 68;
            this.resetSewa.Text = "Reset Status Sewa";
            this.resetSewa.UseVisualStyleBackColor = false;
            this.resetSewa.Click += new System.EventHandler(this.resetSewa_Click);
            // 
            // luasBangunanTextBox
            // 
            this.luasBangunanTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.luasBangunanTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.luasBangunanTextBox.Location = new System.Drawing.Point(157, 122);
            this.luasBangunanTextBox.MaxLength = 5;
            this.luasBangunanTextBox.Name = "luasBangunanTextBox";
            this.luasBangunanTextBox.Size = new System.Drawing.Size(164, 27);
            this.luasBangunanTextBox.TabIndex = 67;
            this.luasBangunanTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(12, 125);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(143, 18);
            this.label7.TabIndex = 66;
            this.label7.Text = "Luas Bangunan";
            // 
            // luasTanahTextBox
            // 
            this.luasTanahTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.luasTanahTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.luasTanahTextBox.Location = new System.Drawing.Point(157, 89);
            this.luasTanahTextBox.MaxLength = 5;
            this.luasTanahTextBox.Name = "luasTanahTextBox";
            this.luasTanahTextBox.Size = new System.Drawing.Size(164, 27);
            this.luasTanahTextBox.TabIndex = 65;
            this.luasTanahTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(45, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 18);
            this.label6.TabIndex = 64;
            this.label6.Text = "Luas Tanah";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(206, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 16);
            this.label5.TabIndex = 63;
            this.label5.Text = "bulan";
            // 
            // durasiBayarTextBox
            // 
            this.durasiBayarTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.durasiBayarTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.durasiBayarTextBox.Location = new System.Drawing.Point(157, 188);
            this.durasiBayarTextBox.MaxLength = 2;
            this.durasiBayarTextBox.Name = "durasiBayarTextBox";
            this.durasiBayarTextBox.Size = new System.Drawing.Size(43, 27);
            this.durasiBayarTextBox.TabIndex = 62;
            this.durasiBayarTextBox.Text = "1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(35, 191);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 18);
            this.label4.TabIndex = 61;
            this.label4.Text = "Pembayaran";
            // 
            // biayaIPLTextBox
            // 
            this.biayaIPLTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.biayaIPLTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.biayaIPLTextBox.Location = new System.Drawing.Point(157, 155);
            this.biayaIPLTextBox.MaxLength = 20;
            this.biayaIPLTextBox.Name = "biayaIPLTextBox";
            this.biayaIPLTextBox.Size = new System.Drawing.Size(255, 27);
            this.biayaIPLTextBox.TabIndex = 60;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(117, 158);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 18);
            this.label1.TabIndex = 59;
            this.label1.Text = "IPL";
            // 
            // searchBlokButton
            // 
            this.searchBlokButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchBlokButton.BackgroundImage")));
            this.searchBlokButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchBlokButton.Enabled = false;
            this.searchBlokButton.Location = new System.Drawing.Point(327, 54);
            this.searchBlokButton.Name = "searchBlokButton";
            this.searchBlokButton.Size = new System.Drawing.Size(29, 30);
            this.searchBlokButton.TabIndex = 58;
            this.searchBlokButton.UseVisualStyleBackColor = true;
            this.searchBlokButton.Click += new System.EventHandler(this.searchBlokButton_Click);
            // 
            // searchKompleksButton
            // 
            this.searchKompleksButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchKompleksButton.BackgroundImage")));
            this.searchKompleksButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchKompleksButton.Location = new System.Drawing.Point(464, 18);
            this.searchKompleksButton.Name = "searchKompleksButton";
            this.searchKompleksButton.Size = new System.Drawing.Size(29, 30);
            this.searchKompleksButton.TabIndex = 57;
            this.searchKompleksButton.UseVisualStyleBackColor = true;
            this.searchKompleksButton.Click += new System.EventHandler(this.searchKompleksButton_Click);
            // 
            // noRumahTextBox
            // 
            this.noRumahTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.noRumahTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noRumahTextBox.Location = new System.Drawing.Point(363, 56);
            this.noRumahTextBox.MaxLength = 10;
            this.noRumahTextBox.Name = "noRumahTextBox";
            this.noRumahTextBox.Size = new System.Drawing.Size(130, 27);
            this.noRumahTextBox.TabIndex = 52;
            // 
            // nonAktifCheckbox
            // 
            this.nonAktifCheckbox.AutoSize = true;
            this.nonAktifCheckbox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nonAktifCheckbox.ForeColor = System.Drawing.Color.Black;
            this.nonAktifCheckbox.Location = new System.Drawing.Point(157, 221);
            this.nonAktifCheckbox.Name = "nonAktifCheckbox";
            this.nonAktifCheckbox.Size = new System.Drawing.Size(103, 22);
            this.nonAktifCheckbox.TabIndex = 51;
            this.nonAktifCheckbox.Text = "Non Aktif";
            this.nonAktifCheckbox.UseVisualStyleBackColor = true;
            // 
            // namaBlokTextBox
            // 
            this.namaBlokTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.namaBlokTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.namaBlokTextBox.Location = new System.Drawing.Point(157, 56);
            this.namaBlokTextBox.MaxLength = 5;
            this.namaBlokTextBox.Name = "namaBlokTextBox";
            this.namaBlokTextBox.ReadOnly = true;
            this.namaBlokTextBox.Size = new System.Drawing.Size(164, 27);
            this.namaBlokTextBox.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(63, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 18);
            this.label2.TabIndex = 7;
            this.label2.Text = "Kompleks";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(110, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 18);
            this.label3.TabIndex = 8;
            this.label3.Text = "Blok";
            // 
            // kompleksTextBox
            // 
            this.kompleksTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.kompleksTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kompleksTextBox.Location = new System.Drawing.Point(157, 20);
            this.kompleksTextBox.MaxLength = 30;
            this.kompleksTextBox.Name = "kompleksTextBox";
            this.kompleksTextBox.ReadOnly = true;
            this.kompleksTextBox.Size = new System.Drawing.Size(300, 27);
            this.kompleksTextBox.TabIndex = 15;
            // 
            // dataKavlingDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(656, 412);
            this.Controls.Add(this.ResetButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.groupBox1);
            this.Name = "dataKavlingDetailForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.dataKavlingDetailForm_FormClosed);
            this.Load += new System.EventHandler(this.dataKavlingDetailForm_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.SaveButton, 0);
            this.Controls.SetChildIndex(this.ResetButton, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.Button ResetButton;
        protected System.Windows.Forms.Button SaveButton;
        protected System.Windows.Forms.GroupBox groupBox1;
        protected System.Windows.Forms.CheckBox nonAktifCheckbox;
        protected System.Windows.Forms.TextBox namaBlokTextBox;
        protected System.Windows.Forms.Label label2;
        protected System.Windows.Forms.Label label3;
        protected System.Windows.Forms.TextBox kompleksTextBox;
        protected System.Windows.Forms.TextBox noRumahTextBox;
        private System.Windows.Forms.Button searchKompleksButton;
        private System.Windows.Forms.Button searchBlokButton;
        protected System.Windows.Forms.Label label5;
        protected System.Windows.Forms.TextBox durasiBayarTextBox;
        protected System.Windows.Forms.Label label4;
        protected System.Windows.Forms.TextBox biayaIPLTextBox;
        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.TextBox luasBangunanTextBox;
        protected System.Windows.Forms.Label label7;
        protected System.Windows.Forms.TextBox luasTanahTextBox;
        protected System.Windows.Forms.Label label6;
        protected System.Windows.Forms.Button resetSewa;
        protected System.Windows.Forms.Button resetJual;
    }
}
