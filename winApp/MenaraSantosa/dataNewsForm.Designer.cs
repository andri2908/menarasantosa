﻿namespace AlphaSoft
{
    partial class dataNewsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dataNewsForm));
            this.ResetButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.img2Button = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lifestyleRadio = new System.Windows.Forms.RadioButton();
            this.buletinRadio = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.notExpiredCheckBox = new System.Windows.Forms.CheckBox();
            this.endDTPicker = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.startDTPicker = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.subjectTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.img1Button = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.nonAktifCheckBox = new System.Windows.Forms.CheckBox();
            this.checkBoxBanner = new System.Windows.Forms.CheckBox();
            this.descTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.imgFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(781, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // ResetButton
            // 
            this.ResetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ResetButton.BackColor = System.Drawing.Color.FloralWhite;
            this.ResetButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResetButton.ForeColor = System.Drawing.Color.Black;
            this.ResetButton.Location = new System.Drawing.Point(378, 575);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(180, 37);
            this.ResetButton.TabIndex = 62;
            this.ResetButton.Text = "Reset";
            this.ResetButton.UseVisualStyleBackColor = false;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SaveButton.BackColor = System.Drawing.Color.FloralWhite;
            this.SaveButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveButton.ForeColor = System.Drawing.Color.Black;
            this.SaveButton.Location = new System.Drawing.Point(166, 575);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(182, 37);
            this.SaveButton.TabIndex = 61;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = false;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.img2Button);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.lifestyleRadio);
            this.panel2.Controls.Add(this.buletinRadio);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.notExpiredCheckBox);
            this.panel2.Controls.Add(this.endDTPicker);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.startDTPicker);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.subjectTextBox);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.img1Button);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.nonAktifCheckBox);
            this.panel2.Controls.Add(this.checkBoxBanner);
            this.panel2.Controls.Add(this.descTextBox);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Location = new System.Drawing.Point(8, 49);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(761, 509);
            this.panel2.TabIndex = 63;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(315, 222);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 18);
            this.label6.TabIndex = 216;
            this.label6.Text = "Fullscreen";
            // 
            // img2Button
            // 
            this.img2Button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("img2Button.BackgroundImage")));
            this.img2Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.img2Button.Location = new System.Drawing.Point(474, 243);
            this.img2Button.Name = "img2Button";
            this.img2Button.Size = new System.Drawing.Size(29, 30);
            this.img2Button.TabIndex = 215;
            this.img2Button.UseVisualStyleBackColor = true;
            this.img2Button.Click += new System.EventHandler(this.img2Button_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(318, 243);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(150, 250);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 214;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(116, 222);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 18);
            this.label5.TabIndex = 213;
            this.label5.Text = "Thumbnail";
            // 
            // lifestyleRadio
            // 
            this.lifestyleRadio.AutoSize = true;
            this.lifestyleRadio.Checked = true;
            this.lifestyleRadio.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lifestyleRadio.Location = new System.Drawing.Point(142, 508);
            this.lifestyleRadio.Name = "lifestyleRadio";
            this.lifestyleRadio.Size = new System.Drawing.Size(96, 22);
            this.lifestyleRadio.TabIndex = 212;
            this.lifestyleRadio.TabStop = true;
            this.lifestyleRadio.Text = "Lifestyle";
            this.lifestyleRadio.UseVisualStyleBackColor = true;
            this.lifestyleRadio.Visible = false;
            // 
            // buletinRadio
            // 
            this.buletinRadio.AutoSize = true;
            this.buletinRadio.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buletinRadio.Location = new System.Drawing.Point(60, 508);
            this.buletinRadio.Name = "buletinRadio";
            this.buletinRadio.Size = new System.Drawing.Size(84, 22);
            this.buletinRadio.TabIndex = 211;
            this.buletinRadio.Text = "Buletin";
            this.buletinRadio.UseVisualStyleBackColor = true;
            this.buletinRadio.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(8, 508);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 18);
            this.label4.TabIndex = 210;
            this.label4.Text = "Tipe";
            this.label4.Visible = false;
            // 
            // notExpiredCheckBox
            // 
            this.notExpiredCheckBox.AutoSize = true;
            this.notExpiredCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.notExpiredCheckBox.Location = new System.Drawing.Point(495, 169);
            this.notExpiredCheckBox.Name = "notExpiredCheckBox";
            this.notExpiredCheckBox.Size = new System.Drawing.Size(161, 22);
            this.notExpiredCheckBox.TabIndex = 209;
            this.notExpiredCheckBox.Text = "Tanpa masa berlaku";
            this.notExpiredCheckBox.UseVisualStyleBackColor = true;
            this.notExpiredCheckBox.CheckedChanged += new System.EventHandler(this.isExpiredCheckBox_CheckedChanged);
            // 
            // endDTPicker
            // 
            this.endDTPicker.CustomFormat = "dd MMM yyyy";
            this.endDTPicker.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.endDTPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endDTPicker.Location = new System.Drawing.Point(318, 167);
            this.endDTPicker.Name = "endDTPicker";
            this.endDTPicker.Size = new System.Drawing.Size(171, 27);
            this.endDTPicker.TabIndex = 208;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(296, 173);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 18);
            this.label3.TabIndex = 207;
            this.label3.Text = "-";
            // 
            // startDTPicker
            // 
            this.startDTPicker.CustomFormat = "dd MMM yyyy";
            this.startDTPicker.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startDTPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startDTPicker.Location = new System.Drawing.Point(119, 167);
            this.startDTPicker.Name = "startDTPicker";
            this.startDTPicker.Size = new System.Drawing.Size(171, 27);
            this.startDTPicker.TabIndex = 206;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(8, 173);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 18);
            this.label2.TabIndex = 205;
            this.label2.Text = "Tgl Berlaku";
            // 
            // subjectTextBox
            // 
            this.subjectTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectTextBox.Location = new System.Drawing.Point(121, 10);
            this.subjectTextBox.MaxLength = 200;
            this.subjectTextBox.Name = "subjectTextBox";
            this.subjectTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.subjectTextBox.Size = new System.Drawing.Size(616, 27);
            this.subjectTextBox.TabIndex = 204;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(43, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 18);
            this.label1.TabIndex = 203;
            this.label1.Text = "Subject";
            // 
            // img1Button
            // 
            this.img1Button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("img1Button.BackgroundImage")));
            this.img1Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.img1Button.Location = new System.Drawing.Point(275, 243);
            this.img1Button.Name = "img1Button";
            this.img1Button.Size = new System.Drawing.Size(29, 30);
            this.img1Button.TabIndex = 202;
            this.img1Button.UseVisualStyleBackColor = true;
            this.img1Button.Click += new System.EventHandler(this.img1Button_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(119, 243);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 198;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            // 
            // nonAktifCheckBox
            // 
            this.nonAktifCheckBox.AutoSize = true;
            this.nonAktifCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nonAktifCheckBox.Location = new System.Drawing.Point(569, 251);
            this.nonAktifCheckBox.Name = "nonAktifCheckBox";
            this.nonAktifCheckBox.Size = new System.Drawing.Size(87, 22);
            this.nonAktifCheckBox.TabIndex = 192;
            this.nonAktifCheckBox.Text = "Non Aktif";
            this.nonAktifCheckBox.UseVisualStyleBackColor = true;
            // 
            // checkBoxBanner
            // 
            this.checkBoxBanner.AutoSize = true;
            this.checkBoxBanner.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxBanner.Location = new System.Drawing.Point(569, 223);
            this.checkBoxBanner.Name = "checkBoxBanner";
            this.checkBoxBanner.Size = new System.Drawing.Size(74, 22);
            this.checkBoxBanner.TabIndex = 191;
            this.checkBoxBanner.Text = "Banner";
            this.checkBoxBanner.UseVisualStyleBackColor = true;
            // 
            // descTextBox
            // 
            this.descTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.descTextBox.Location = new System.Drawing.Point(121, 43);
            this.descTextBox.MaxLength = 200;
            this.descTextBox.Multiline = true;
            this.descTextBox.Name = "descTextBox";
            this.descTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.descTextBox.Size = new System.Drawing.Size(616, 111);
            this.descTextBox.TabIndex = 187;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(30, 45);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 18);
            this.label7.TabIndex = 186;
            this.label7.Text = "Deskripsi";
            // 
            // dataNewsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(781, 646);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.ResetButton);
            this.Controls.Add(this.SaveButton);
            this.Name = "dataNewsForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.dataNewsForm_FormClosed);
            this.Load += new System.EventHandler(this.dataNewsForm_Load);
            this.Controls.SetChildIndex(this.SaveButton, 0);
            this.Controls.SetChildIndex(this.ResetButton, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.Button ResetButton;
        protected System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button img1Button;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox nonAktifCheckBox;
        private System.Windows.Forms.CheckBox checkBoxBanner;
        protected System.Windows.Forms.TextBox descTextBox;
        protected System.Windows.Forms.Label label7;
        protected System.Windows.Forms.TextBox subjectTextBox;
        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox notExpiredCheckBox;
        protected System.Windows.Forms.DateTimePicker endDTPicker;
        protected System.Windows.Forms.Label label3;
        protected System.Windows.Forms.DateTimePicker startDTPicker;
        protected System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton lifestyleRadio;
        private System.Windows.Forms.RadioButton buletinRadio;
        private System.Windows.Forms.OpenFileDialog imgFileDialog;
        protected System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button img2Button;
        private System.Windows.Forms.PictureBox pictureBox2;
        protected System.Windows.Forms.Label label5;
    }
}
