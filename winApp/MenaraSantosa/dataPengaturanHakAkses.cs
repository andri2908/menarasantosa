﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Hotkeys;

namespace AlphaSoft
{
    public partial class dataPengaturanHakAkses : AlphaSoft.basicHotkeysForm
    {
        int selectedGroupID = 0;
        string selectedGroupName = "";
        string selectedGroupDesc = "";

        globalUserUtil gUser;
        globalRestAPI gRest;
        
        private Hotkeys.GlobalHotkey ghk_F9;

        private REST_masterModule mmData = new REST_masterModule();

        public dataPengaturanHakAkses(int groupID = 0, string groupName = "", string groupDesc = "")
        {
            InitializeComponent();
            
            gUser = new globalUserUtil();
            gRest = new globalRestAPI();

            selectedGroupID = groupID;
            selectedGroupName = groupName;
            selectedGroupDesc = groupDesc;
        }

        protected override void captureAll(Keys key)
        {
            base.captureAll(key);

            switch (key)
            {
                case Keys.F9:
                    saveButton.PerformClick();
                    break;
            }
        }

        protected override void registerGlobalHotkey()
        {
            base.registerGlobalHotkey();

            ghk_F9 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F9, this);
            ghk_F9.Register();
        }

        protected override void unregisterGlobalHotkey()
        {
            base.unregisterGlobalHotkey();

            ghk_F9.Unregister();
        }

        private void pengaturanHakAkses_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void loadDataModule()
        {
            DataTable dt = new DataTable();
            int moduleFeatures;
            int userAccess;
            int moduleID;
            bool akses, insert, update;

            string sqlCommand = "";

            namaGroupTextBox.Text = selectedGroupName;
            deskripsiTextBox.Text = selectedGroupDesc;

            sqlCommand = "SELECT mm.module_id, mm.module_name, " +
                                   "mm.module_features, " +
                                   "IFNULL(uam.user_access_option, 0) AS user_access_option, IFNULL(uam.id, 0) AS uam_id " +
                                   "FROM master_module mm " +
                                   "LEFT OUTER JOIN master_module_access uam ON (mm.module_id = uam.module_id AND uam.group_id = " + selectedGroupID + ") " +
                                   "WHERE mm.module_active = 1";

            if (gRest.getMasterModuleData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref mmData))
            {
                for (int i = 0;i<mmData.dataList.Count;i++)
                {
                    akses = false;
                    insert = false;
                    update = false;
                    moduleID = mmData.dataList[i].module_id;
                    moduleFeatures = mmData.dataList[i].module_features;
                    userAccess = mmData.dataList[i].user_access_option;

                    switch (userAccess)
                    {
                        case 1:
                            akses = true;
                            break;
                        case 3:
                            akses = true;
                            insert = true;
                            break;
                        case 4:
                            akses = true;
                            update = true;
                            break;
                        case 7:
                            akses = true;
                            insert = true;
                            update = true;
                            break;
                    }

                    dataGridView.Rows.Add(
                        moduleID,
                        moduleFeatures,
                        mmData.dataList[i].module_name,
                        akses, insert, update,
                        mmData.dataList[i].uam_id
                        );

                    switch (moduleFeatures)
                    {
                        case 1:
                            dataGridView.Rows[dataGridView.Rows.Count - 1].Cells["newData"].ReadOnly = true;
                            dataGridView.Rows[dataGridView.Rows.Count - 1].Cells["newData"].Style.BackColor = Color.DarkGray;

                            dataGridView.Rows[dataGridView.Rows.Count - 1].Cells["updateData"].ReadOnly = true;
                            dataGridView.Rows[dataGridView.Rows.Count - 1].Cells["updateData"].Style.BackColor = Color.DarkGray;
                            break;
                    }
                }
            }
        }

        private void pengaturanHakAkses_Load(object sender, EventArgs e)
        {
            loadDataModule();
            dataGridView.CellValueChanged += dataGridView_CustomCellValueChanged;
        }

        private void newGroupButton_Click(object sender, EventArgs e)
        {
            dataGroupUserDetailForm displayForm = new dataGroupUserDetailForm(0);
            displayForm.ShowDialog(this);

            selectedGroupID = displayForm.returnGroupUserIDValue;

            dataGridView.CellValueChanged -= dataGridView_CustomCellValueChanged;
            loadDataModule();
            dataGridView.CellValueChanged += dataGridView_CustomCellValueChanged;
        }

        private bool saveDataTransaction()
        {
            bool result = false;
            int userAccessOption = 0;
            int moduleID = 0;
            int lineID = 0;
            string errMsg = "";

            List<master_module> listMMData = new List<master_module>();
            master_module mmData;

            try
            {
                for (int i = 0;i<dataGridView.Rows.Count;i++)
                {
                    moduleID = Convert.ToInt32(dataGridView.Rows[i].Cells["moduleID"].Value);
                    lineID = Convert.ToInt32(dataGridView.Rows[i].Cells["uamID"].Value);

                    userAccessOption = 0;

                    if (Convert.ToBoolean(dataGridView.Rows[i].Cells["hakAkses"].Value))
                        userAccessOption += 1;

                    if (Convert.ToBoolean(dataGridView.Rows[i].Cells["newData"].Value))
                        userAccessOption += 2;

                    if (Convert.ToBoolean(dataGridView.Rows[i].Cells["updateData"].Value))
                        userAccessOption += 4;

                    mmData = new master_module();
                    mmData.module_id = moduleID;
                    mmData.module_name = dataGridView.Rows[i].Cells["moduleName"].Value.ToString();
                    mmData.user_access_option = userAccessOption;
                    mmData.uam_id = lineID;

                    listMMData.Add(mmData);
                }

                if (!gRest.saveHakAkses(gUser.getUserID(), gUser.getUserToken(), selectedGroupID, listMMData, out errMsg))
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch (SqlException ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (saveDataTransaction())
            {
                errorLabel.Text = "";
                MessageBox.Show("Success");
            }
            else
                MessageBox.Show("Fail");
        }

        private void dataGridView_CustomCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            int colIndex = e.ColumnIndex;
            DataGridViewRow selectedRow = dataGridView.Rows[rowIndex];
            string columnName = dataGridView.Columns[colIndex].Name;

            if (columnName == "newData" || columnName == "updateData")
            {
                if (Convert.ToBoolean(selectedRow.Cells[colIndex].Value))
                {
                    selectedRow.Cells["hakAkses"].Value = true;
                }
            }
        }

        private void dataGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataGridView.IsCurrentCellDirty)
            {
                dataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void checkAll_CheckedChanged(object sender, EventArgs e)
        {
            dataGridView.CellValueChanged -= dataGridView_CustomCellValueChanged;

            for (int i =0;i<dataGridView.Rows.Count;i++)
            {
                dataGridView.Rows[i].Cells["hakAkses"].Value = checkAll.Checked;

                if (!dataGridView.Rows[i].Cells["newData"].ReadOnly)
                    dataGridView.Rows[i].Cells["newData"].Value = checkAll.Checked;

                if (!dataGridView.Rows[i].Cells["updateData"].ReadOnly)
                    dataGridView.Rows[i].Cells["updateData"].Value = checkAll.Checked;
            }

            dataGridView.CellValueChanged += dataGridView_CustomCellValueChanged;

        }
    }
}
