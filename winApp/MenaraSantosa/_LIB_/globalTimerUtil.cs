﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Media;
using System.Windows.Forms;

namespace AlphaSoft
{
    class globalTimerUtil
    {
        private CultureInfo culture = new CultureInfo("id-ID");

        private globalUserUtil gUser;
        private globalRestAPI gRest;
        private globalIPL gIPL;
        string appPath = Application.StartupPath;

        public globalTimerUtil()
        {
            gUser = new globalUserUtil();
            gRest = new globalRestAPI();
            gIPL = new globalIPL();
        }

        public void checkForExpiredTrans()
        {
            string sqlCommand = "";
            REST_expiredTrans listTrans = new REST_expiredTrans();

            try
            {
                sqlCommand = "SELECT ID_TRANS " +
                                        "FROM TRANSAKSI_RETAIL " +
                                        "WHERE IS_ACTIVE = 'Y' " +
                                        "AND STATUS_ID not in (1, 2) " +
                                        "AND DATE_EXPIRED < NOW()";
                gRest.checkForExpiredTrans(gUser.getUserID(), gUser.getUserToken(), sqlCommand);
            }
            catch (Exception ex) { }

        }

        public void checkUserAccessStatus()
        {
            string sqlCommand = "";
            REST_userAccessList userList = new REST_userAccessList();

            List<user_access_list> listUserAccessList = new List<user_access_list>();
            user_access_list userAccessList;

            int accessStatus = 0;
            string accessMessage = "";

            try
            {
                sqlCommand = "SELECT ul.* " +
                                        "FROM user_access_list ul, master_kompleks mk, user_login_data uld " +
                                        "WHERE ul.access_status = 2 " +
                                        "AND ul.kompleks_id = mk.kompleks_id " +
                                        "AND ul.user_id = uld.user_id " +
                                        "AND uld.is_active = 'Y' " +
                                        "AND ul.is_active = 'Y' " +
                                        "AND mk.is_active = 'Y' ";

                if (gRest.getUserAccessListData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref userList))
                {
                    if (userList.dataStatus.o_status == 1)
                    {
                        for (int i = 0;i<userList.dataList.Count;i++)
                        {
                            accessStatus = 0;
                            if (!gIPL.isLateOutstandingTagihanExist(userList.dataList[i].user_id, userList.dataList[i].kavling_id))
                            {
                                accessStatus = 1;
                                accessMessage = "Welcome....";
                                if (userList.dataList[i].access_type == "QR")
                                {
                                    if (!gIPL.isQREnabledStatus(userList.dataList[i].user_id, userList.dataList[i].kavling_id))
                                    {
                                        accessStatus = 3;
                                        accessMessage = "QR Blocked....";
                                    }
                                }
                                else
                                {
                                    if (!gIPL.isRFIDEnabledStatus(userList.dataList[i].user_id, userList.dataList[i].kavling_id, userList.dataList[i].access_id))
                                    {
                                        accessStatus = 3;
                                        accessMessage = "RFID Blocked....";
                                    }
                                }
                            }

                            if (accessStatus != 0)
                            {
                                userAccessList = new user_access_list();
                                userAccessList.id = userList.dataList[i].id;
                                userAccessList.user_id = userList.dataList[i].user_id;
                                userAccessList.kompleks_id = userList.dataList[i].kompleks_id;
                                userAccessList.kavling_id = userList.dataList[i].kavling_id;
                                userAccessList.access_id = userList.dataList[i].access_id;
                                userAccessList.access_type = userList.dataList[i].access_type;
                                userAccessList.access_status = accessStatus;
                                userAccessList.message = accessMessage;
                                userAccessList.is_active = userList.dataList[i].is_active;

                                listUserAccessList.Add(userAccessList);
                            }
                        }

                        if (listUserAccessList.Count > 0)
                        {
                            string errMsg = "";
                            if (!gRest.saveDataUserAccessList(gUser.getUserID(), gUser.getUserToken(), listUserAccessList, out errMsg))
                            {
                                throw new Exception(errMsg);
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { }

        }

        public void checkUnreadTicket()
        {
            int numUnread = 0;
            //string sqlCommand = "SELECT count(1) AS resultQuery " +
            //                        "FROM cc_ticket WHERE status <> 'admin_response' " +
            //                        "AND is_active = 'Y'";

            genericReply replyResult = new genericReply();
            if (gRest.getPendingTicket(gUser.getUserID(), gUser.getUserToken(), ref replyResult))
            {
                if (replyResult.o_status == 1)
                {
                    numUnread = Convert.ToInt32(replyResult.data);
                }
            }

            if (numUnread > 0)
            {
                SoundPlayer simpleSound = new SoundPlayer(@appPath + "\\res\\notify.wav");
                simpleSound.Play();

                string errMsg = "";
                gRest.saveMessageTable(gUser.getUserID(), gUser.getUserToken(), 0, globalConstants.MODULE_TICKET_REPAIR, "", 
                    "Ada " + numUnread + " tiket belum direspon", out errMsg);
            }
        }

        public void checkUnreadRepairTicket()
        {
            int numUnread = 0;
            //string sqlCommand = "SELECT count(1) AS resultQuery " +
            //                        "FROM cc_ticket WHERE status <> 'admin_response' " +
            //                        "AND is_active = 'Y'";

            genericReply replyResult = new genericReply();
            if (gRest.getPendingRepairTicket(gUser.getUserID(), gUser.getUserToken(), ref replyResult))
            {
                if (replyResult.o_status == 1)
                {
                    numUnread = Convert.ToInt32(replyResult.data);
                }
            }

            if (numUnread > 0)
            {
                SoundPlayer simpleSound = new SoundPlayer(@appPath + "\\res\\notify.wav");
                simpleSound.Play();

                string errMsg = "";
                gRest.saveMessageTable(gUser.getUserID(), gUser.getUserToken(), 0, globalConstants.MODULE_PENGERJAAN_REPAIR, "",
                    "Ada " + numUnread + " tiket repair belum direspon", out errMsg);
            }
        }

        public void getUndeliverTrans()
        {
            REST_dataTransaksi dataTrans = new REST_dataTransaksi();
            List<master_message> msgData = new List<master_message>();
            master_message msgContent;

            string sqlCommand = "SELECT th.id_trans, th.deliver_status, " +
                                    "CONCAT(IF(th.type_trans = " + globalConstants.TRANS_TYPE_RETAIL + ", 'RETAIL', 'DRUGS'), ' [', DATE_FORMAT(th.date_issued, '%d %M %Y'), '] ') AS 'TGL', " +
                                    "ul.user_name, ml.kompleks_name, " +
                                    "CONCAT(IFNULL(mb.blok_name, ''), '-', mk.house_no) AS 'blok', th.nominal " +
                                    "FROM transaksi_retail th " +
                                    "LEFT OUTER JOIN user_login_data ul ON (th.user_id = ul.user_id) " +
                                    "LEFT OUTER JOIN master_kavling mk ON (th.kavling_id = mk.kavling_id AND mk.is_active = 'Y') " +
                                    "LEFT OUTER JOIN master_kompleks ml ON (mk.kompleks_id = ml.kompleks_id AND ml.is_active = 'Y') " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id AND mb.is_active = 'Y') " +
                                    "WHERE th.is_active = 'Y' " +
                                    "AND th.status_id = 2 " +
                                    "AND th.deliver_status = 'undelivered' " +
                                    "AND th.id_trans NOT IN (SELECT msg_ref FROM master_message WHERE module_id = " + globalConstants.MODULE_TRANSAKSI_RETAIL + " AND msg_status = 'unread')";

            if (gRest.getTransData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref dataTrans))
            {
                if (dataTrans.dataStatus.o_status == 1)
                {
                    for (int i = 0; i < dataTrans.dataList.Count; i++)
                    {
                        msgContent = new master_message();
                        msgContent.module_id = globalConstants.MODULE_TRANSAKSI_RETAIL;
                        msgContent.msg_id = 0;
                        msgContent.msg_ref = dataTrans.dataList[i].id_trans.ToString();
                        msgContent.msg_content = dataTrans.dataList[i].tgl + " " + dataTrans.dataList[i].kompleks_name + "/" + dataTrans.dataList[i].blok + " - " + dataTrans.dataList[i].user_name;

                        msgData.Add(msgContent);
                    }

                    string errMsg = "";
                    gRest.saveMultipleMessageTable(gUser.getUserID(), gUser.getUserToken(), msgData, out errMsg);
                }
            }
        }

        public void checkUserAccessIPL()
        {

        }
    }
}
