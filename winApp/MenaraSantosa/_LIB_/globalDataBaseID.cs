﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaSoft
{
    class globalDataBaseID : globalUtilities
    {
        private Data_Access DS;
        private globalDBUtil globalDB;

        public globalDataBaseID(Data_Access DSParam)
        {
            DS = DSParam;
            globalDB = new globalDBUtil(DS);
        }

        public string getNewPaymentID()
        {
            string result = "";

            result = globalDB.getAutoGenerateID("DEBT_PAYMENT", "PY", "-", "PAYMENT_ID");

            return result;
        }

        public string getNewJOID(int originType)
        {
            string whereCondition = "AND ORDER_TYPE = " + originType;
            return globalDB.getAutoGenerateID("ORDER_HEADER", "JO", "-", "ORDER_ID", whereCondition);
        }

        public string getNewWOID(int originType)
        {
            string whereCondition = "AND ORDER_TYPE = " + originType;
            return globalDB.getAutoGenerateID("ORDER_HEADER", "WO", "-", "ORDER_ID", whereCondition);
        }

        //public string getNewOrderID(int originType)
        //{
        //    //if (originType == globalConstants.MODULE_JOB_ORDER)
        //    //    return getNewJOID(originType);
        //    //else if (originType == globalConstants.MODULE_WORK_ORDER)
        //    //    return getNewWOID(originType);
        //    //else
        //    //    return "";
        //}

        //public string getNewPenerimaanID(int originType, out int penerimaanType, out int orderType)
        //{
        //    string prefix = "";

        //    penerimaanType = globalConstants.PURCHASE_ORDER;
        //    orderType = globalConstants.MODULE_PO;

        //    switch (originType)
        //    {
        //        case globalConstants.MODULE_PENERIMAAN_PO:
        //            prefix = "POPR";
        //            penerimaanType = globalConstants.PURCHASE_ORDER;
        //            orderType = globalConstants.MODULE_PO;
        //            break;

        //        case globalConstants.MODULE_PENERIMAAN_JO:
        //            prefix = "JOPR";
        //            penerimaanType = globalConstants.JOB_ORDER;
        //            orderType = globalConstants.MODULE_JOB_ORDER;
        //            break;

        //        case globalConstants.MODULE_PENERIMAAN_WO:
        //            prefix = "WOPR";
        //            penerimaanType = globalConstants.WORK_ORDER;
        //            orderType = globalConstants.MODULE_WORK_ORDER;
        //            break;

        //        case globalConstants.MODULE_REPACKING:
        //            prefix = "RPCK";
        //            penerimaanType = globalConstants.REPACKING;
        //            orderType = globalConstants.MODULE_REPACKING;
        //            break;

        //        case globalConstants.MODULE_PENERIMAAN_BS:
        //            prefix = "BSPR";
        //            penerimaanType = globalConstants.BS;
        //            orderType = globalConstants.MODULE_PENERIMAAN_BS;
        //            break;

        //        case globalConstants.MODULE_PENERIMAAN_SISA:
        //            prefix = "SSPR";
        //            penerimaanType = globalConstants.SISA;
        //            orderType = globalConstants.MODULE_PENERIMAAN_SISA;
        //            break;

        //        case globalConstants.MODULE_PENERIMAAN_RETUR:
        //            prefix = "RSPR";
        //            penerimaanType = globalConstants.RETUR;
        //            orderType = globalConstants.MODULE_PENERIMAAN_RETUR;
        //            break;
        //    }

        //    return globalDB.getAutoGenerateID("PENERIMAAN_HEADER", prefix, "-", "PENERIMAAN_ID", "");
        //}

        //public string getNewDebtID(int originType)
        //{
        //    string prefix = "";

        //    switch (originType)
        //    {
        //        case globalConstants.MODULE_PENERIMAAN_PO:
        //            prefix = "POD";
        //            break;

        //        case globalConstants.MODULE_PENERIMAAN_JO:
        //            prefix = "JOD";
        //            break;

        //        case globalConstants.MODULE_PENERIMAAN_WO:
        //            prefix = "WOD";
        //            break;

        //        case globalConstants.MODULE_PENJUALAN:
        //            prefix = "SOD";
        //            break;
        //    }

        //    return globalDB.getAutoGenerateID("DEBT", prefix, "-", "DEBT_ID", "");
        //}

        public string getNewProductID()
        {
            return globalDB.getAutoGenerateID("MASTER_PRODUCT", "", "", "PRODUCT_ID");
        }

        public string getNewPOID()
        {
            return globalDB.getAutoGenerateID("PURCHASE_ORDER_HEADER", "PO", "-", "PO_ID", "");
        }

        public int getNewPMID()
        {
            return Convert.ToInt32(globalDB.getAutoGenerateID("PAYMENT_METHOD", "", "", "PM_ID"));
        }

        public string getNewSalesInvoiceID()
        {
            return globalDB.getAutoGenerateID("SALES_HEADER", "SO", "-", "INVOICE_NO", "");
        }

        public string getNewSalesContractID()
        {
            return globalDB.getAutoGenerateID("SALES_CONTRACT_HEADER", "SC", "-", "INVOICE_NO", "");
        }

        public string getNewDOID()
        {
            return globalDB.getAutoGenerateID("DELIVERY_ORDER_HEADER", "SJ", "-", "DO_ID", "");
        }

        public string getNewReturSOID()
        {
            return globalDB.getAutoGenerateID("RETURN_SO_HEADER", "RS", "-", "RETUR_ID", "");
        }

        public string getNewReturPenerimaanID()
        {
            return globalDB.getAutoGenerateID("RETURN_PO_HEADER", "RP", "-", "RETUR_ID", "");
        }

        public string getNewContactID()
        {
            return globalDB.getAutoGenerateID("MASTER_CONTACT", "", "", "CONTACT_ID", "");
        }
    }
}
