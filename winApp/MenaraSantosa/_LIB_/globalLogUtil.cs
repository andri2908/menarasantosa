﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace AlphaSoft
{
    class globalLogUtil
    {
        private Data_Access DS;
        private globalDBUtil gDB;
        private globalUserUtil gUser;
        private globalMessageUtil gMsg;

        private CultureInfo culture = new CultureInfo("id-ID");

        private bool msgSaved = false;

        private string[] logMsg =
        {
            "LOG_TYPE",
            "USER_ID",
            "LOG_REF_ID",
            "LOG_MSG",
            "LOG_REMARK"
        };

        public globalLogUtil(Data_Access DSParam = null)
        {
            if (null != DSParam)
                DS = DSParam;
            else
                DS = new Data_Access();

            gDB = new globalDBUtil(DS);
            gUser = new globalUserUtil(DS);
            gMsg = new globalMessageUtil(DS);
        }

        public void saveLog(int logType, int logRefID, int userID, string logMessage = "")
        {
            string sqlCommand = "";
            MySqlException inEx = null;

            List<string> pList = new List<string>();
            List<object> pVal = new List<object>();

            pList.Clear();
            pVal.Clear();

            gDB.addParamList(pList, logMsg, "_log_");

            pVal.Add(logType);
            pVal.Add(userID);
            pVal.Add(logRefID);
            pVal.Add(logMessage);

            sqlCommand = gDB.constructMasterQuery("SYS_LOG_MSG", logMsg, pList);

            if (!DS.executeNonQueryCommandWithParameters(sqlCommand, ref inEx, pList, pVal))
                throw inEx;
        }

        public void saveLog(int logType, int logRefID, int userID, string logMessage, MySqlException inEx)
        {
            string sqlCommand = "";

            List<string> pList = new List<string>();
            List<object> pVal = new List<object>();

            pList.Clear();
            pVal.Clear();

            gDB.addParamList(pList, logMsg, "_log_");

            pVal.Add(logType);
            pVal.Add(userID);
            pVal.Add(logRefID);
            pVal.Add(logMessage);

            sqlCommand = gDB.constructMasterQuery("SYS_LOG_MSG", logMsg, pList);

            if (!DS.executeNonQueryCommandWithParameters(sqlCommand, ref inEx, pList, pVal))
                throw inEx;
        }

        private string getMasterContactLogValue(string fieldNames, object fieldValues)
        {
            string result = "";
            string sqlCommand = "";

            switch (fieldNames)
            {
                case "ORIGIN_ID":
                    string tmp = fieldValues.ToString();
                    result = "BLANK";
                    if (tmp != "0")
                    {
                        sqlCommand = "SELECT ORIGIN_NAME FROM MASTER_ORIGIN_CONTACT WHERE ORIGIN_ID = " + fieldValues.ToString();
                        result = DS.getDataSingleValue(sqlCommand).ToString();
                    }
                    break;
            }

            return result;
        }

        private string getMasterUserLogValue(string fieldNames, object fieldValues)
        {
            string result = "";
            string sqlCommand = "";

            switch (fieldNames)
            {
                case "GROUP_ID":
                    sqlCommand = "SELECT GROUP_USER_NAME FROM MASTER_GROUP WHERE GROUP_ID = " + fieldValues.ToString();
                    result = DS.getDataSingleValue(sqlCommand).ToString();
                    break;

                case "JENIS_ID":
                    if (fieldValues.ToString() == "1")
                        result = "KTP";
                    else
                        result = "SIM";
                    break;

                case "USER_2ND_AUTH":
                case "USER_ACTIVE":
                    if (fieldValues.ToString() == "1")
                        result = "AKTIF";
                    else
                        result = "NON-AKTIF";
                    break;
            }

            return result;
        }

        private string getMasterOriginLogValue(string fieldNames, object fieldValues)
        {
            string result = "";
 
            switch (fieldNames)
            {
                case "ORIGIN_ACTIVE":
                    if (fieldValues.ToString() == "1")
                        result = "AKTIF";
                    else
                        result = "NON-AKTIF";
                    break;
            }

            return result;
        }

        private string getMasterFUActionLogValue(string fieldNames, object fieldValues)
        {
            string result = "";

            switch (fieldNames)
            {
                case "IS_ACTIVE":
                    if (fieldValues.ToString() == "1")
                        result = "AKTIF";
                    else
                        result = "NON-AKTIF";
                    break;
            }

            return result;
        }

        private string getMasterFUStatusLogValue(string fieldNames, object fieldValues)
        {
            string result = "";
            string sqlCommand = "";

            switch (fieldNames)
            {
                case "STATUS_ID":
                    sqlCommand = "SELECT STATUS_NAME FROM MASTER_FOLLOWUP_CONTACT_STATUS WHERE ID = " + fieldValues.ToString();
                    result = DS.getDataSingleValue(sqlCommand).ToString();
                    break;
            }

            return result;
        }

        private string getFUHeaderLogValue(string fieldNames, object fieldValues)
        {
            string result = "";
            string sqlCommand = "";

            switch (fieldNames)
            {
                case "MARKETING_ID":
                    sqlCommand = "SELECT USER_NAME FROM MASTER_USER WHERE ID = " + fieldValues.ToString();
                    result = DS.getDataSingleValue(sqlCommand).ToString();
                    break;

                case "REFERAL_ID":
                    sqlCommand = "SELECT CONTACT_NAME FROM MASTER_CONTACT WHERE CONTACT_ID = " + fieldValues.ToString();
                    result = DS.getDataSingleValue(sqlCommand).ToString();
                    break;
            }

            return result;
        }

        private string getFUDetailLogValue(string fieldNames, object fieldValues)
        {
            string result = "";
            string sqlCommand = "";

            switch (fieldNames)
            {
                case "ACTION_ID":
                    sqlCommand = "SELECT FOLLOW_UP_ACTION FROM MASTER_FOLLOWUP_ACTION WHERE ACTION_ID = " + fieldValues.ToString();
                    result = DS.getDataSingleValue(sqlCommand).ToString();
                    break;

                case "RESULT_ID":
                    if (fieldValues.ToString() != "0")
                    {
                        sqlCommand = "SELECT HASIL_VALUE FROM MASTER_FOLLOWUP_HASIL WHERE RESULT_ID = " + fieldValues.ToString();
                        result = DS.getDataSingleValue(sqlCommand).ToString();
                    }
                    break;

                default:
                    break;
            }

            return result;
        }

        private string getMasterGroupLogValue(string fieldNames, object fieldValues)
        {
            string result = "";
      
            switch (fieldNames)
            {
                case "GROUP_TYPE":
                    switch (fieldValues.ToString())
                    {
                        case "0": result = "UMUM"; break;
                        case "1": result = "MARKETING"; break;
                        case "2": result = "SUPERVISOR"; break;
                        case "3": result = "KEPALA BAGIAN"; break;
                        default: break;
                    }
                    break;

                case "GROUP_USER_ACTIVE":
                    if (fieldValues.ToString() == "1")
                        result = "AKTIF";
                    else
                        result = "NON-AKTIF";
                    break;
            }

            return result;
        }

        private string getDetailGroupTargetLogValue(string fieldNames, object fieldValues)
        {
            string result = "";

            switch (fieldNames)
            {
                case "FLAG":
                    if (fieldValues.ToString() == "1")
                        result = "AKTIF";
                    else
                        result = "NON-AKTIF";
                    break;
            }

            return result;
        }

        private string getLogDisplayValue(string tableName, string fieldNames, object fieldValues)
        {
            string result = "";

            if (fieldValues.GetType() == typeof(double))
                result = Convert.ToDouble(fieldValues).ToString("N2", culture);
            if (fieldValues.GetType() == typeof(DateTime))
                result = String.Format(culture, "{0:dd/MM/yyyy}", Convert.ToDateTime(fieldValues));
            else
                result = fieldValues.ToString();

            switch (tableName)
            {
                case "MASTER_CONTACT":
                    if (fieldNames == "ORIGIN_ID")
                    {
                        result = getMasterContactLogValue(fieldNames, fieldValues);
                    }
                    break;

                case "MASTER_USER":
                    if (fieldNames == "USER_2ND_AUTH" || 
                        fieldNames == "USER_ACTIVE" || 
                        fieldNames == "GROUP_ID" ||
                        fieldNames == "JENIS_ID")
                    {
                        result = getMasterUserLogValue(fieldNames, fieldValues);
                    }
                    break;

                case "MASTER_GROUP":
                    if (fieldNames == "GROUP_USER_ACTIVE" ||
                        fieldNames == "GROUP_TYPE")
                    {
                        result = getMasterGroupLogValue(fieldNames, fieldValues);
                    }
                    break;

                case "MASTER_ORIGIN_CONTACT":
                    if (fieldNames == "ORIGIN_ACTIVE")
                    {
                        result = getMasterOriginLogValue(fieldNames, fieldValues);
                    }
                    break;

                case "MASTER_FOLLOWUP_ACTION":
                    if (fieldNames == "IS_ACTIVE")
                    {
                        result = getMasterFUActionLogValue(fieldNames, fieldValues);
                    }
                    break;

                case "FOLLOWUP_CONTACT_STATUS":
                    if (fieldNames == "STATUS_ID")
                    {
                        result = getMasterFUStatusLogValue(fieldNames, fieldValues);
                    }
                    break;

                case "FOLLOWUP_HEADER":
                    if (fieldNames == "MARKETING_ID" || 
                        fieldNames == "REFERAL_ID" || 
                        fieldNames == "FINAL_STATUS_ID")
                    {
                        result = getFUHeaderLogValue(fieldNames, fieldValues);
                    }
                    break;

                case "FOLLOWUP_DETAIL":
                    if (fieldNames == "ACTION_ID" ||
                        fieldNames == "RESULT_ID" )
                    {
                        result = getFUDetailLogValue(fieldNames, fieldValues);
                    }
                    break;

                case "DETAIL_GROUP_TARGET":
                    if (fieldNames == "FLAG")
                    {
                        result = getDetailGroupTargetLogValue(fieldNames, fieldValues);
                    }
                    break;

                default:
                    break;
            }

            return result;
        }

        private string getFieldNameValue(string fieldName)
        {
            string result = fieldName.Replace("_", " ");

            switch(fieldName)
            {
                case "ORIGIN_ID":
                    result = "ASAL CUSTOMER";
                    break;

                case "USER_2ND_AUTH":
                    result = "AUTENTIKASI";
                    break;

                case "GROUP_ID":
                    result = "GROUP USER";
                    break;

                case "GROUP_TYPE":
                    result = "TIPE GROUP";
                    break;

                case "STATUS_ID":
                    result = "STATUS";
                    break;

                case "MARKETING_ID":
                    result = "MARKETING";
                    break;

                case "REFERAL_ID":
                    result = "REFERAL";
                    break;

                case "FINAL_STATUS_ID":
                    result = "FU FINAL STATUS";
                    break;

                case "ACTION_ID":
                    result = "FU ACTION";
                    break;

                case "RESULT_ID":
                    result = "HASIL FU";
                    break;

                case "FLAG":
                    result = "STATUS TARGET";
                    break;

                case "START_DATE":
                    result = "TANGGAL MULAI";
                    break;

                default:
                    break;
            }

            return result;
        }

        public string compareAndSaveDataLog(int userID, int moduleID, string tableName, 
            List<string> fieldNames, List<object> fieldValues, int searchIndex, MySqlException inEx, 
            string keyWord = "", string refID = "", 
            Form parentForm = null, string logRemarkParam = "", string idIdx = "")
        {
            string sqlCommand = "";
            string whereCommand = "";
            object resultValues;
            string logMsgContent = "";
            int i = 0;
            string fieldNamesValue = "";
            string logRemark = "";
            msgSaved = false;

            List<string> pList = new List<string>();
            List<object> pVal = new List<object>();

            whereCommand = "";
            for (i = fieldNames.Count - searchIndex; i < fieldNames.Count; i++)
            {
                whereCommand += " AND " + fieldNames[i] + " = " + fieldValues[i] + " ";
            }

            for (i = 0;i<fieldNames.Count - searchIndex;i++)
            {
                sqlCommand = "SELECT " + fieldNames[i] + " FROM " + tableName + " WHERE 1 = 1 ";
                sqlCommand = sqlCommand + whereCommand;

                resultValues = DS.getDataSingleValue(sqlCommand);

                if (null == resultValues || null == fieldValues[i])
                    continue;

                if (resultValues.ToString() != fieldValues[i].ToString())
                {
                    if (null != parentForm && logRemark.Length <= 0)
                    {
                        if (logRemarkParam.Length <= 0)
                            logRemark = getLogRemark(parentForm);
                        else
                            logRemark = logRemarkParam;
                    }

                    fieldNamesValue = getFieldNameValue(fieldNames[i]);

                    // SAVE LOG
                    logMsgContent = "UPDATE " + (keyWord.Length > 0 ? "[" +keyWord + "] " : "") + fieldNamesValue;

                    if (tableName == "DETAIL_GROUP_TARGET" ||
                        tableName == "DETAIL_USER_TARGET")
                    {
                        string targetName = DS.getDataSingleValue("SELECT TARGET_NAME FROM MASTER_TARGET WHERE ID = " + refID).ToString();
                        logMsgContent += " " + targetName;
                    }

                    if (resultValues.ToString().Length > 0 
                        && fieldNames[i] != "RESULT_ID")
                    {
                        logMsgContent += " [" + getLogDisplayValue(tableName, fieldNames[i], resultValues) + "] -> [" + getLogDisplayValue(tableName, fieldNames[i], fieldValues[i]) + "]";
                    }
                    else
                    {
                        logMsgContent += " = [" + getLogDisplayValue(tableName, fieldNames[i], fieldValues[i]) + "]";
                    }

                    pList.Clear();
                    pVal.Clear();

                    gDB.addParamList(pList, logMsg, "_" + tableName + "_" + idIdx + "_" + i);

                    pVal.Add(logConstants.LOG_TYPE_DEFAULT);
                    pVal.Add(userID);
                    pVal.Add(moduleID);
                    pVal.Add(logMsgContent);
                    pVal.Add(logRemark);

                    sqlCommand = gDB.constructMasterQuery("SYS_LOG_MSG", logMsg, pList);

                    if (!DS.executeNonQueryCommandWithParameters(sqlCommand, ref inEx, pList, pVal))
                        throw inEx;

                    if (!msgSaved)
                        msgSaved = gMsg.saveToMessageTable(inEx, moduleID, 0, "", //MMConstants.LOG_PERUBAHAN, "", 
                            globalMessageUtil.MSG_LOG_CHANGES, globalMessageUtil.MSG_LOG_CHANGES, idIdx + "_" + i, DateTime.Now.Date);
                }
            }

            return logRemark;
        }

        public void saveLogContactType(int userID, List<string> prevValues, List<string> newValues, 
            MySqlException inEx, string keyWord = "", Form parentForm = null, string logRemarkParam = "")
        {
            int i = 0;
            string logMsgContent = "UPDATE " + (keyWord.Length > 0 ? "[" + keyWord + "] " : "") + "TIPE KONTAK [";
            string sqlCommand = "";
            List<string> pList = new List<string>();
            List<object> pVal = new List<object>();
            string logRemark = "";

            if ((newValues.Count != prevValues.Count) || 
                (!prevValues.OrderBy(x=>x).SequenceEqual(newValues.OrderBy(x=>x))))
            {
                if (null != parentForm)
                {
                    if (logRemark.Length <= 0)
                    {
                        if (logRemarkParam.Length <= 0)
                            logRemark = getLogRemark(parentForm);
                        else
                            logRemark = logRemarkParam;
                    }
                }

                for (i = 0; i < prevValues.Count; i++)
                    logMsgContent += prevValues[i] + ", ";

                logMsgContent = logMsgContent.Substring(0, logMsgContent.Length - 2);
                logMsgContent += "] -> [";

                for (i = 0; i < newValues.Count; i++)
                    logMsgContent += newValues[i] + ", ";

                logMsgContent = logMsgContent.Substring(0, logMsgContent.Length - 2);
                logMsgContent += "] ";

                pList.Clear();
                pVal.Clear();

                gDB.addParamList(pList, logMsg, "_ctType_");

                pVal.Add(logConstants.LOG_TYPE_DEFAULT);
                pVal.Add(userID);
                pVal.Add(MMConstants.PENGATURAN_CONTACT);
                pVal.Add(logMsgContent);
                pVal.Add(logRemark);

                sqlCommand = gDB.constructMasterQuery("SYS_LOG_MSG", logMsg, pList);

                if (!DS.executeNonQueryCommandWithParameters(sqlCommand, ref inEx, pList, pVal))
                    throw inEx;

                gMsg.saveToMessageTable(inEx, MMConstants.PENGATURAN_CONTACT, 0, "", //MMConstants.LOG_PERUBAHAN, "", 
                    globalMessageUtil.MSG_LOG_CHANGES, globalMessageUtil.MSG_LOG_CHANGES, "0", DateTime.Now);
            }
        }

        public string getLogRemark(Form parentForm)
        {
            string retValue = "";

            remarkEntryForm displayForm = new remarkEntryForm("REMARK", 200, true);
            displayForm.ShowDialog(parentForm);

            if (displayForm.retValue.Length > 0)
            {
                retValue = displayForm.retValue;
            }

            return retValue;
        }

    }
}
