﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.Globalization;
using System.IO;
using System.Net;
using System.Windows.Forms;
using System.Diagnostics;

namespace AlphaSoft
{
    class remoteFileLib
    {
        private Data_Access DS;
        private static string remoteUserName = "";
        private static string remoteUserPassword = "";
        private static string remoteFileDirectory = "";
        private static progressBarForm displayProgress;

        public remoteFileLib(Data_Access DSParam)
        {
            DS = DSParam;
            remoteUserName = getRemoteUserName();
            remoteUserPassword = getRemoteUserPassword();
            remoteFileDirectory = getRemoteFileDirectory();
        }

        public remoteFileLib(string userName, string password, string filePath)
        {
            remoteUserName = userName;
            remoteUserPassword = password;
            remoteFileDirectory = filePath;
        }

        private string getRemoteUserName()
        {
            string sqlCommand = "SELECT IFNULL(DOC_FILE_SERVER_USERNAME, '') FROM SYS_CONFIG_FILE_SERVER";

            return DS.getDataSingleValue(sqlCommand).ToString();
        }

        private string getRemoteUserPassword()
        {
            string sqlCommand = "SELECT IFNULL(DOC_FILE_SERVER_PASSWORD, '') FROM SYS_CONFIG_FILE_SERVER";

            return DS.getDataSingleValue(sqlCommand).ToString();
        }

        private string getRemoteFileDirectory()
        {
            string sqlCommand = "SELECT IFNULL(DOC_FILE_SERVER_PATH, '') FROM SYS_CONFIG_FILE_SERVER";

            return DS.getDataSingleValue(sqlCommand).ToString();
        }

        public bool testConnection()
        {
            try
            {
                Stream fileStream;

                establishConnection();

                WebClient client = new WebClient();
                Uri destURL = new Uri(@remoteFileDirectory);

                fileStream = client.OpenWrite(destURL + "/testRemoteConn.txt");

                if (null != fileStream)
                {
                    fileStream.Close();
                    fileStream.Dispose();

                    terminateConnection();
                    return true;
                }
            }
            catch(Exception ex)
            {

            }

            terminateConnection();
            return false;
        }

        private static void UploadProgressCallback(object sender, UploadProgressChangedEventArgs e)
        {
            displayProgress.setProgressValue(unchecked((int)e.BytesSent));
        }
     
        public int remoteFileUpload(string sourceFilepath, string fileName)
        {
            try
            {
                establishConnection();

                byte[] bytes = System.IO.File.ReadAllBytes(sourceFilepath);

                WebClient client = new WebClient();
                Uri destURL = new Uri(@remoteFileDirectory + "/" + fileName);

                client.UploadProgressChanged += new UploadProgressChangedEventHandler(UploadProgressCallback);
                client.UploadDataAsync(destURL, "POST", bytes);

                displayProgress = new progressBarForm(bytes.Count());
                displayProgress.Show();

                while (client.IsBusy)
                {
                    Application.DoEvents();
                }

                displayProgress.Close();

                terminateConnection();
                return 1;
            }
            catch (Exception ex1)
            {
                terminateConnection();
                return 0;
            }
        }
        
        public string[] retrieveFileList()
        {
            string[] listOfFile;

            establishConnection();
            listOfFile = Directory.GetFiles(remoteFileDirectory);
            terminateConnection();

            return listOfFile;
        } 

        public static int ExecuteCommand(string command, int timeout)
        {
            var processInfo = new ProcessStartInfo("cmd.exe", "/C " + command)
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                WorkingDirectory = "C:\\",
            };

            var process = Process.Start(processInfo);
            process.WaitForExit(timeout);
            var exitCode = process.ExitCode;
            process.Close();
            return exitCode;
        }

        private void establishConnection()
        {
            var command = "NET USE " + remoteFileDirectory + " /user:" + remoteUserName + " " + remoteUserPassword;
            ExecuteCommand(command, 5000);
        }

        private void terminateConnection()
        {
            var command = "NET USE " + remoteFileDirectory + " /delete";
            ExecuteCommand(command, 5000);
        }
    }
}
