﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Text.RegularExpressions;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace AlphaSoft
{
    class globalUtilities
    {
        public const string REGEX_NUMBER_WITH_2_DECIMAL = @"^[0-9]+\.?\d{0,2}$";
        public const string REGEX_NUMBER_ONLY = @"^[0-9]*$";
        public const string REGEX_ALPHANUMERIC_ONLY = @"^[0-9A-Za-z_-]*$";
        public const string CUSTOM_DATE_FORMAT = "dd MMM yyyy";
        public const string CUSTOM_TIME_FORMAT = "HH:mm";
        public const string CUSTOM_MONTH_FORMAT = "MMMM yyyy";
        public const string CUSTOM_SHORT_MONTH_FORMAT = "MMM yyyy";
        public const string CUSTOM_DATE_TIME_FORMAT = "dd MMM yyyy HH:mm";
        public const string CELL_FORMATTING_NUMERIC_FORMAT = "#,0.##";
        public const string INDO_CELL_FORMATTING_NUMERIC_FORMAT = "#.0,##";

        public const int PAPER_POS_RECEIPT = 0;
        public const int PAPER_HALF_KWARTO = 1;
        public const int PAPER_FULL_KWARTO = 2;

        public const int INS = 1;
        public const int UPD = 2;
        public string appPath = Application.StartupPath + "\\conf";

        private CultureInfo culture = new CultureInfo("id-ID");

        private static Random random = new Random();

        public string allTrim(string valueToTrim)
        {
            string temp = "";

            temp = valueToTrim.Replace(" ", "");

            return temp;
        }

        public void ClearControls(Control ctrl)
        {
            foreach (Control control in ctrl.Controls)
            {
                if (control is TextBox)
                {
                    TextBox textBox = (TextBox)control;
                    textBox.Text = "";
                }

                if (control is MaskedTextBox)
                {
                    MaskedTextBox maskedtextBox = (MaskedTextBox)control;
                    maskedtextBox.Text = "";
                }

                if (control is ComboBox)
                {
                    ComboBox comboBox = (ComboBox)control;
                    if (comboBox.Items.Count > 0)
                    {
                        comboBox.SelectedIndex = 0;
                        comboBox.Text = "";
                    }
                }

                if (control is CheckBox)
                {
                    CheckBox checkBox = (CheckBox)control;
                    checkBox.Checked = false;
                }

                if (control is ListBox)
                {
                    ListBox listBox = (ListBox)control;
                    listBox.ClearSelected();
                }

                if (control is DataGridView)
                {
                    DataGridView DGView = (DataGridView)control;
                    DGView.DataSource = null;
                    DGView.Rows.Clear();
                }

                if (control is DateTimePicker)
                {
                    DateTimePicker DTView = (DateTimePicker)control;
                    DTView.Value = DateTime.Now;
                }
            }
        }

        public void ResetAllControls(Control form)
        {
            String typectrl = "";

            TabControl tabControlHandler;
            ClearControls(form); //if controls are not nested
            for (int i = 0; i <= form.Controls.Count - 1; i++) //if controls are nested
            {

                typectrl = "" + form.Controls[i].GetType();
                Control ctrl = form.Controls[i];

                if ((typectrl.Equals("System.Windows.Forms.Panel")) || (typectrl.Equals("System.Windows.Forms.TableLayoutPanel")) ||
                    (typectrl.Equals("System.Windows.Forms.TabControl")) || (typectrl.Equals("System.Windows.Forms.GroupBox")))
                {
                    if (typectrl.Equals("System.Windows.Forms.TabControl"))
                    {
                        tabControlHandler = (TabControl)ctrl;
                        for (int j = 0; j < tabControlHandler.TabPages.Count; j++)
                            ResetAllControls(tabControlHandler.TabPages[j]);
                    }
                    else
                        ResetAllControls(ctrl);
                }
                else
                    ClearControls(ctrl);
            }
        }

        public void disableControls(Control ctrl)
        {
            foreach (Control control in ctrl.Controls)
            {
                if (control is TextBox)
                {
                    TextBox textBox = (TextBox)control;
                    textBox.ReadOnly = true;
                }

                if (control is MaskedTextBox)
                {
                    MaskedTextBox maskedtextBox = (MaskedTextBox)control;
                    maskedtextBox.ReadOnly = true;
                }

                if (control is ComboBox)
                {
                    ComboBox comboBox = (ComboBox)control;
                    comboBox.Enabled = false;
                }

                if (control is CheckBox)
                {
                    CheckBox checkBox = (CheckBox)control;
                    checkBox.Enabled = false;
                }

                if (control is ListBox)
                {
                    ListBox listBox = (ListBox)control;
                    listBox.Enabled = false;
                }

                if (control is DataGridView)
                {
                    DataGridView dgView = (DataGridView)control;
                    dgView.ReadOnly = true;
                }

                if (control is Button)
                {
                    Button button = (Button)control;
                    button.Enabled = false;
                }

                if (control is DateTimePicker)
                {
                    DateTimePicker dtPicker = (DateTimePicker)control;
                    dtPicker.Enabled = false;
                }

                if (control is RadioButton)
                {
                    RadioButton rdButton = (RadioButton)control;
                    rdButton.Enabled = false;
                }
            }
        }

        public void disableAllControls(Control form)
        {
            String typectrl = "";
            disableControls(form); //if controls are not nested

            for (int i = 0; i <= form.Controls.Count - 1; i++) //if controls are nested
            {
                typectrl = "" + form.Controls[i].GetType();
                if ((typectrl.Equals("System.Windows.Forms.Panel")) || (typectrl.Equals("System.Windows.Forms.TableLayoutPanel")) ||
                    (typectrl.Equals("System.Windows.Forms.TabControl")) || (typectrl.Equals("System.Windows.Forms.TabPage")) ||
                    (typectrl.Equals("System.Windows.Forms.GroupBox")))
                {
                    Control ctrl = form.Controls[i];
                    disableAllControls(ctrl);
                }
            }
        }

        public void reArrangeTabOrder(Control form, int mode = 0)
        {
            TabOrderManager.TabScheme scheme;

            if (mode == 0)
            {
                scheme = TabOrderManager.TabScheme.DownFirst;
            }
            else
            {
                scheme = TabOrderManager.TabScheme.AcrossFirst;
            }
            TabOrderManager tom = new TabOrderManager(form);
            tom.SetTabOrder(scheme);
        }

        public void reArrangeTabPageOrder(TabPage tabPageComponent, int mode = 0)
        {
            TabOrderManager.TabScheme scheme;

            if (mode == 0)
            {
                scheme = TabOrderManager.TabScheme.DownFirst;
            }
            else
            {
                scheme = TabOrderManager.TabScheme.AcrossFirst;
            }
            TabOrderManager tom = new TabOrderManager(tabPageComponent);
            tom.SetTabOrder(scheme);
        }

        public void reArrangeButtonPosition(Button[] buttonArr, int startPositionY, int formWidth)
        {
            int buttonCount = 0;
            int buttonTotalWidth = 0;
            int buttonDistancePixel = 5;
            int startPositionX = 0;
            double startingPoint = 0;
            int i = 0;

            for (i = 0; i < buttonArr.GetLength(0); i++)
            {
                if (buttonArr[i].Visible == true)
                {
                    buttonCount++;
                    buttonTotalWidth += buttonArr[i].Width;
                    buttonTotalWidth += buttonDistancePixel;
                }
            }

            if (buttonTotalWidth > 0)
                buttonTotalWidth -= buttonDistancePixel;

            startingPoint = (formWidth / 2) - (buttonTotalWidth / 2);
            startPositionX = Convert.ToInt32(Math.Round(startingPoint));

            if (startPositionX > 0)
            {
                for (i = 0; i < buttonArr.Count(); i++)
                {
                    if (buttonArr[i].Visible == true)
                    {
                        buttonArr[i].Left = startPositionX;
                        buttonArr[i].Top = startPositionY;
                        startPositionX += buttonArr[i].Width;
                        startPositionX += buttonDistancePixel;
                    }
                }
            }
        }

        public void showSuccess(int options)
        {
            String successcaption = "Success Message";
            String successmessage = "";
            if (options == INS) //insert success
            {
                successmessage = "Saving data to table success!";
            }
            else
            {
                if (options == UPD)
                {
                    successmessage = "Update data to table success!";

                }
            }

            DialogResult res1 = MessageBox.Show(successmessage, successcaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public bool matchRegEx(string textToMatch, string regExValue)
        {
            if (textToMatch.Length > 0)
            {
                Regex r = new Regex(regExValue); // This is the main part, can be altered to match any desired form or limitations
                Match m = r.Match(textToMatch);

                return m.Success;
            }
            else
                return false;
        }

        public void writeFile(string fileName, string content)
        {
            string path = fileName;

            try
            {
                if (File.Exists(@path))
                    File.Delete(@path);

                File.WriteAllText(@path, content);
            }
            catch (Exception ex) { }
        }

        public void appendFile(string fileName, string contentToAppend)
        {
            string path = fileName;

            if (File.Exists(@path))
                File.AppendAllText(@path, contentToAppend);
        }

        public string addBackslahToAString(string input)
        {
            string result = "";

            for (int i = 0; i < input.Length; i++)
            {
                result = result + input[i];

                if (input[i] == '\\')
                    result = result + "\\";
            }

            return result;
        }

        public string loadAutoBackupDir()
        {
            string path = "";
            string configFile = Application.StartupPath + "\\conf\\autoBak.cfg";

            if (File.Exists(configFile))
            {
                using (StreamReader sr = File.OpenText(configFile))
                {
                    if ((path = sr.ReadLine()) != null)
                    {
                    }
                }
            }

            return path;
        }

        public string getCustomStringFormatDate(DateTime inputDateTime, bool dateOnly = false, bool longdate = false)
        {
            string result = "";

            string dateInput = "";
            string hourInput = "";
            string minuteInput = "";

            if (!longdate)
                dateInput = String.Format(culture, "{0:dd-MM-yyyy}", inputDateTime);
            else
                dateInput = String.Format(culture, "{0:dd-MMM-yyyy}", inputDateTime);

            hourInput = String.Format(culture, "{0:HH}", inputDateTime);
            minuteInput = String.Format(culture, "{0:mm}", inputDateTime);

            result = dateInput;

            if (!dateOnly)
                result = result + " " + hourInput + ":" + minuteInput;

            return result;
        }

        public string getCustomStringFormatTime(DateTime inputDateTime)
        {
            string result = "";

            string hourInput = "";
            string minuteInput = "";

            hourInput = String.Format(culture, "{0:HH}", inputDateTime);
            minuteInput = String.Format(culture, "{0:mm}", inputDateTime);

            result = hourInput + ":" + minuteInput;

            return result;
        }

        public string getRomeLetter(int valueToCheck)
        {
            string result = "";

            switch (valueToCheck)
            {
                case 1: result = "I"; break;
                case 2: result = "II"; break;
                case 3: result = "III"; break;
                case 4: result = "IV"; break;
                case 5: result = "V"; break;
                case 6: result = "VI"; break;
                case 7: result = "VII"; break;
                case 8: result = "VIII"; break;
                case 9: result = "IX"; break;
                case 10: result = "X"; break;
                case 11: result = "XI"; break;
                case 12: result = "XII"; break;
                default: result = "I"; break;
            }


            return result;
        }

        public int getMaxDate(string monthValue, string yearValue)
        {
            int maxDate = 30;
            switch (monthValue)
            {
                case "01":
                case "1":
                case "03":
                case "3":
                case "05":
                case "5":
                case "07":
                case "7":
                case "08":
                case "8":
                case "10":
                case "12":
                    maxDate = 31;
                    break;

                case "02":
                case "2":
                    if (Convert.ToInt32(yearValue) % 4 == 0)
                        maxDate = 29;
                    else
                        maxDate = 28;
                    break;

                default:
                    maxDate = 30;
                    break;
            }

            return maxDate;
        }

        public string getConnectionCFG()
        {
            string locPath = appPath + "\\pos.cfg";
            string result = "";

            if (File.Exists(locPath))
                result = File.ReadAllText(locPath);

            return result;
        }

        public int getLocalLocationID()
        {
            int locationID = 0;

            string locPath = appPath + "\\location.cfg";
            string result = "";

            if (File.Exists(locPath))
                result = File.ReadAllText(locPath);

            int.TryParse(result, out locationID);

            return locationID;
        }

        public double getRoundingMoney(double moneyToRoundParam, double roundingLimit = 100, double direction = 1)
        {
            double result = 0;
            double tempValue = 0;
            double leftOver = 0;
            double moneyToRound = moneyToRoundParam;
            double leftOverLimit = roundingLimit / 2;

            leftOver = Math.Round(moneyToRound % roundingLimit);
            tempValue = Math.Floor(moneyToRound / roundingLimit);

            if (Math.Abs(leftOver) > 0 && direction == 1)
            {
                if (leftOver < leftOverLimit)
                    leftOver = 0;
                else
                    leftOver = roundingLimit;
            }
            else if (direction == 0)
                leftOver = 0;

            tempValue = (tempValue * roundingLimit) + leftOver;
            result = tempValue;

            return result;
        }

        public string getRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string CreateMD5(string input, int maxLength = 0)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("x2"));
                }
                return sb.ToString();
            }
        }


        public string getMD5Value(string inputParam, int maxLength = 0)
        {
            string md5Value = "";

            md5Value = CreateMD5(inputParam, maxLength);

            return md5Value;
        }
    }
}
