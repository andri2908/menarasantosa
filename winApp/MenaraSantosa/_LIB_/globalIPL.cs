﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;
using System.Windows.Forms;
using System.Data;
using System.Globalization;

namespace AlphaSoft
{
    class globalIPL
    {
        private CultureInfo culture = new CultureInfo("id-ID");
        private globalUserUtil gUser;
        private globalRestAPI gRest;

        public globalIPL()
        {
            gUser = new globalUserUtil();
            gRest = new globalRestAPI();
        }

        public bool userAssignedExist(int kavlingID)
        {
            bool result = false;
            string sqlCommand = "";

            genericReply replyResult = new genericReply();

            sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                    "FROM user_kavling " +
                                    "WHERE kavling_id = " + kavlingID + " " +
                                    "AND is_active = 'Y'";

            if (gRest.checkDataExist(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    result = true;
            }

            return result;
        }

        public DateTime getStartIPL(int kavlingID, int userID = 0)
        {
            DateTime startIPL = new DateTime(3000, 1, 1);
            string sqlCommand = "";
            genericReply replyResult = new genericReply();

            sqlCommand = "SELECT start_ipl AS resultQuery " +
                                    "FROM user_kavling " +
                                    "WHERE kavling_id = " + kavlingID + " " +
                                    "AND is_active = 'Y' ";

            if (userID > 0)
                sqlCommand += "AND user_id = " + userID;

            try
            {
                if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
                {
                    if (replyResult.o_status == 1)
                    {
                        startIPL = Convert.ToDateTime(replyResult.data);
                    }
                }
            }
            catch (Exception ex) { }

            return startIPL;
        }

        public bool outstandingIPLExist(int kavlingID)
        {
            string sqlCommand = "";
            DateTime startIPL = getStartIPL(kavlingID);
            genericReply replyResult = new genericReply();

            string dateFrom = String.Format(culture, "{0:yyyyMM}", DateTime.Now);

            // IPL BELUM DIMULAI
            //if (startIPL > DateTime.Now.Date)
            //    return false;

            // ADA TAGIHAN IPL DENGAN RANGE > DARI TGL HARI INI
            //sqlCommand = "SELECT COUNT(1) AS resultQuery " +
            //                        "FROM transaksi_ipl ih " +
            //                        "WHERE is_active = 'Y' " +
            //                        "AND ih.kavling_id = " + kavlingID + " " +
            //                        "AND DATE_FORMAT(ih.end_ipl, '%Y%m') >= '" + dateFrom + "'";

            //if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
            //{
            //    if (Convert.ToInt32(replyResult.data) > 0)
            //    {
            //        // check tgl generate lebih besar dari tgl 15, dan pada bulan dan tahun pembayaran terakhir
            //        DateTime dtStartTagihan = getLastIPLPayment(kavlingID);

            //        if (DateTime.Now.Month == dtStartTagihan.Month &&
            //                DateTime.Now.Year == dtStartTagihan.Year &&
            //                DateTime.Now.Day > 15)
            //        {
            //            return true;
            //        }

            //        if (dtStartTagihan.Date < DateTime.Now.Date && 
            //            (DateTime.Now.Month != dtStartTagihan.Month ||
            //                DateTime.Now.Year != dtStartTagihan.Year)
            //                )
            //        {
            //            return true;
            //        }

            //        //if (dtStartTagihan.Date < DateTime.Now.Date)
            //        //{
            //        //    if (DateTime.Now.Month != dtStartTagihan.Month &&
            //        //        DateTime.Now.Year != dtStartTagihan.Year )
            //        //    {
            //        //        return true;
            //        //    }
            //        //}
            //        //else
            //        //{
            //        //    if (DateTime.Now.Month == dtStartTagihan.Month &&
            //        //        DateTime.Now.Year == dtStartTagihan.Year &&
            //        //        DateTime.Now.Day > 15)
            //        //    {
            //        //        return true;
            //        //    }
            //        //}

            //            //if (DateTime.Now.Month != dtStartTagihan.Month ||
            //            //    DateTime.Now.Year != dtStartTagihan.Year ||
            //            //    DateTime.Now.Day < 15)
            //            //{
            //            //    return false;
            //            //}

            //            //if (dtStartTagihan.Date > DateTime.Now.Date)
            //            //{
            //            //    return false;
            //            //}
            //    }
            //}

            // check tgl generate lebih besar dari tgl 15, dan pada bulan dan tahun pembayaran terakhir
            DateTime dtStartTagihan = getLastIPLPayment(kavlingID);

            if (DateTime.Now.Month == dtStartTagihan.Month &&
                    DateTime.Now.Year == dtStartTagihan.Year &&
                    DateTime.Now.Day > 15)
            {
                return true;
            }

            if (dtStartTagihan.Date < DateTime.Now.Date &&
                (DateTime.Now.Month != dtStartTagihan.Month ||
                    DateTime.Now.Year != dtStartTagihan.Year)
                    )
            {
                return true;
            }

            return false;
        }

        public int getDurasiIPL(int kavlingID)
        {
            int result = 0;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            try
            {
                sqlComm = "SELECT durasi_pembayaran AS resultQuery " +
                                    "FROM master_kavling " +
                                    "WHERE kavling_id = " + kavlingID;

                if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
                {
                    if (Convert.ToInt32(replyResult.data) > 0)
                        result = Convert.ToInt32(replyResult.data);
                }

            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public double getNilaiIPL(int kavlingID)
        {
            double result = 0;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            try
            {
                sqlComm = "SELECT biaya_ipl AS resultQuery " +
                                    "FROM master_kavling " +
                                    "WHERE kavling_id = " + kavlingID;

                if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
                {
                    if (Convert.ToInt32(replyResult.data) > 0)
                        result = Convert.ToDouble(replyResult.data);
                }

            }
            catch (Exception ex)
            { }

            return result;
        }

        public int getUserKavling(int kavlingID)
        {
            int result = 0;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            try
            {
                sqlComm = "SELECT user_id AS resultQuery " +
                                    "FROM user_kavling " +
                                    "WHERE kavling_id = " + kavlingID + " " +
                                    "AND is_active = 'Y'";

                if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
                {
                    if (Convert.ToInt32(replyResult.data) > 0)
                        result = Convert.ToInt32(replyResult.data);
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public DateTime getLastIPLPayment(int kavlingID)
        {
            DateTime dtResult = new DateTime(3000, 1, 1);
            string sqlCommand = "";
            genericReply replyResult = new genericReply();

            sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                    "FROM transaksi_ipl " +
                                    "WHERE is_active = 'Y' " +
                                    "AND kavling_id = " + kavlingID + " ";

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                {
                    sqlCommand = "SELECT end_ipl AS resultQuery  " +
                                            "FROM transaksi_ipl " +
                                            "WHERE is_active = 'Y' " +
                                            "AND kavling_id = " + kavlingID + " " +
                                            "ORDER BY end_ipl DESC " +
                                            "LIMIT 1";

                    if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
                    {
                        dtResult = Convert.ToDateTime(replyResult.data);
                    }
                }
                else
                {
                    dtResult = getStartIPL(kavlingID);
                    // startIPL diminus 1 bulan, nanti pas mulai looping untuk generate IPL ada proses tambah 1 bulan untuk dapat tgl tagihan berikutnya
                    dtResult = dtResult.AddMonths(-1);
                }
            }

            return dtResult;
        }

        public bool generateOutstandingIPL(int kavlingID, int userID, bool isRounded, out string errMsg)
        {
            bool result = false;
            errMsg = "";

            double nilaiIPL = 0;
            int durasiBayar = 0;
            DateTime dtStartTagihan = DateTime.Now;
            DateTime dtEndTagihan = DateTime.Now;
            int month, year; 

            List<trans_ipl_header> listHeader = new List<trans_ipl_header>();
            trans_ipl_header headerData;

            List<trans_ipl_detail> listDetail = new List<trans_ipl_detail>();
            trans_ipl_detail detailData;

            genericReply replyResult = new genericReply();

            try
            {
                dtStartTagihan = getLastIPLPayment(kavlingID);
                if (dtStartTagihan.Year >= 3000)
                    throw new Exception("error getting start IPL");
                dtEndTagihan = dtStartTagihan;

                durasiBayar = getDurasiIPL(kavlingID);
                if (durasiBayar <= 0)
                    throw new Exception("error getting durasi bayar IPL");

                nilaiIPL = getNilaiIPL(kavlingID);
                if (nilaiIPL <= 0)
                    throw new Exception("error getting nilai IPL");


                dtStartTagihan = dtStartTagihan.AddMonths(1);
                dtStartTagihan = new DateTime(dtStartTagihan.Year, dtStartTagihan.Month, 1);
                while (dtStartTagihan.Date <= DateTime.Now.Date)
                {
                    #region HEADER DATA
                    headerData = new trans_ipl_header();

                    headerData.id_trans = 0;
                    headerData.type_trans = globalConstants.TRANS_TYPE_IPL;
                    headerData.date_issued = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    headerData.kavling_id = kavlingID;
                    headerData.user_id = userID;

                    if (isRounded)
                        headerData.nominal = gUser.getRoundingMoney(nilaiIPL * durasiBayar, 10000);
                    else
                        headerData.nominal = nilaiIPL * durasiBayar;

                    headerData.nominal_awal = nilaiIPL * durasiBayar;
                    headerData.nominal_disc = headerData.nominal_awal - headerData.nominal;
                    headerData.start_ipl = new DateTime(dtStartTagihan.Year, dtStartTagihan.Month, 1);

                    month = dtStartTagihan.Date.AddMonths(durasiBayar - 1).Month;
                    year = dtStartTagihan.Date.AddMonths(durasiBayar - 1).Year;

                    headerData.end_ipl = new DateTime(year, month, DateTime.DaysInMonth(year, month));//;
                    headerData.is_active = "Y";
                    headerData.detail_data = new List<trans_ipl_detail>();
                    #endregion

                    #region DETAIL
                    for (int i = 0; i < durasiBayar; i++)
                    {
                        detailData = new trans_ipl_detail();

                        detailData.id = 0;
                        detailData.id_trans = 0;
                        detailData.nominal = nilaiIPL;
                        detailData.periode_pembayaran = dtStartTagihan.Date.AddMonths(i);
                        detailData.is_active = "Y";

                        headerData.detail_data.Add(detailData);
                    }
                    #endregion

                    listHeader.Add(headerData);
                    dtStartTagihan = dtStartTagihan.AddMonths(durasiBayar);
                }

                #region TAMBAH 1 TAGIHAN UNTUK MASA DEPAN JIKA MELEBIHI TGL 15, dan ada pada bulan dan tahun yg sama
                month = dtEndTagihan.Month;
                year = dtEndTagihan.Year;

                if (DateTime.Now.Day >= 15 &&
                    DateTime.Now.Month == month &&
                    DateTime.Now.Year == year)
                {
                    #region HEADER DATA
                    headerData = new trans_ipl_header();

                    headerData.id_trans = 0;
                    headerData.type_trans = globalConstants.TRANS_TYPE_IPL;
                    headerData.date_issued = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    headerData.kavling_id = kavlingID;
                    headerData.user_id = userID;

                    if (isRounded)
                        headerData.nominal = gUser.getRoundingMoney(nilaiIPL * durasiBayar, 10000);
                    else
                        headerData.nominal = nilaiIPL * durasiBayar;

                    headerData.nominal_awal = nilaiIPL * durasiBayar;
                    headerData.nominal_disc = headerData.nominal_awal - headerData.nominal;
                    headerData.start_ipl = new DateTime(dtStartTagihan.Year, dtStartTagihan.Month, 1);

                    month = dtStartTagihan.Date.AddMonths(durasiBayar - 1).Month;
                    year = dtStartTagihan.Date.AddMonths(durasiBayar - 1).Year;

                    headerData.end_ipl = new DateTime(year, month, DateTime.DaysInMonth(year, month));//;
                    headerData.is_active = "Y";
                    headerData.detail_data = new List<trans_ipl_detail>();
                    #endregion

                    #region DETAIL
                    for (int j = 0; j < durasiBayar; j++)
                    {
                        detailData = new trans_ipl_detail();

                        detailData.id = 0;
                        detailData.id_trans = 0;
                        detailData.nominal = nilaiIPL;
                        detailData.periode_pembayaran = dtStartTagihan.Date.AddMonths(j);
                        detailData.is_active = "Y";

                        headerData.detail_data.Add(detailData);
                    }
                    #endregion

                    listHeader.Add(headerData);
                    dtStartTagihan = dtStartTagihan.AddMonths(durasiBayar);
                }
                #endregion

                int newID = 0;
                if (!gRest.saveDataIPL(gUser.getUserID(), gUser.getUserToken(), listHeader, 1, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
            }

            return result;
        }

        public bool generateOutstandingIPLKompleks(int kompleksID, ProgressBar progressBar, List<kompleksIPL> kompleksStatus, bool isRounded, 
            out string errMsg)
        {
            bool result = false;
            string sqlCommand = "";
            errMsg = "";

            double nilaiIPL = 0;
            int durasiBayar = 0;
            DateTime dtStartTagihan = DateTime.Now;
            DateTime dtEndTagihan = DateTime.Now;
            int month, year;
            int kavlingID = 0;
            int userID = 0;
            kompleksIPL kompleksIPLData;

            #region GET LIST OF KAVLING
            REST_masterKavling kavlingData = new REST_masterKavling();

            sqlCommand = "SELECT mk.kavling_id, ml.kompleks_name, " +
                                    "IFNULL(mb.blok_name, '') AS blok_name, mk.house_no AS house_no " +
                                    "FROM master_kavling mk " +
                                    "LEFT OUTER JOIN master_kompleks ml ON (mk.kompleks_id = ml.kompleks_id) " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id) " +
                                    "WHERE mk.kompleks_id = " + kompleksID + " " +
                                    "AND mk.is_active = 'Y' ";

            if (gRest.getMasterKavlingData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref kavlingData))
            {
                if (kavlingData.dataStatus.o_status == 1)
                {
                }
                else
                {
                    errMsg = "Tidak ada informasi kavling";
                    return false;
                }
            }
            #endregion

            List<trans_ipl_header> listHeader = new List<trans_ipl_header>();
            trans_ipl_header headerData;

            trans_ipl_detail detailData;

            genericReply replyResult = new genericReply();

            try
            {
                progressBar.Value = 0;
                progressBar.Maximum = kavlingData.dataList.Count;

                for (int i = 0; i < kavlingData.dataList.Count; i++)
                {
                    Application.DoEvents();

                    listHeader.Clear();

                    kavlingID = kavlingData.dataList[i].kavling_id;

                    dtStartTagihan = getLastIPLPayment(kavlingID);
                    dtEndTagihan = dtStartTagihan;
                    if (dtStartTagihan.Year >= 3000)
                    {
                        //throw new Exception("error getting start IPL");
                        continue;
                    }

                    durasiBayar = getDurasiIPL(kavlingID);
                    if (durasiBayar <= 0)
                    {
                        //throw new Exception("error getting durasi bayar IPL");
                        continue;
                    }

                    nilaiIPL = getNilaiIPL(kavlingID);
                    if (nilaiIPL <= 0)
                    {
                        //throw new Exception("error getting nilai IPL");
                        continue;
                    }

                    userID = getUserKavling(kavlingID);
                    //if (userID == 0)
                    //{
                    //    continue;
                    //}

                    dtStartTagihan = dtStartTagihan.AddMonths(1);
                    dtStartTagihan = new DateTime(dtStartTagihan.Year, dtStartTagihan.Month, 1);

                    while (dtStartTagihan.Date <= DateTime.Now.Date)
                    {
                        #region HEADER DATA
                        headerData = new trans_ipl_header();

                        headerData.id_trans = 0;
                        headerData.type_trans = globalConstants.TRANS_TYPE_IPL;
                        headerData.date_issued = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                        headerData.kavling_id = kavlingID;
                        headerData.user_id = userID;

                        if (isRounded)
                            headerData.nominal = gUser.getRoundingMoney(nilaiIPL * durasiBayar, 10000);
                        else
                            headerData.nominal = nilaiIPL * durasiBayar;

                        headerData.nominal_awal = nilaiIPL * durasiBayar;
                        headerData.nominal_disc = headerData.nominal_awal - headerData.nominal;
                        headerData.start_ipl = new DateTime(dtStartTagihan.Year, dtStartTagihan.Month, 1);

                        month = dtStartTagihan.Date.AddMonths(durasiBayar - 1).Month;
                        year = dtStartTagihan.Date.AddMonths(durasiBayar - 1).Year;

                        dtEndTagihan = new DateTime(year, month, DateTime.DaysInMonth(year, month));

                        headerData.end_ipl = dtEndTagihan;
                        headerData.is_active = "Y";
                        headerData.detail_data = new List<trans_ipl_detail>();
                        #endregion

                        #region DETAIL
                        for (int j = 0; j < durasiBayar; j++)
                        {
                            detailData = new trans_ipl_detail();

                            detailData.id = 0;
                            detailData.id_trans = 0;
                            detailData.nominal = nilaiIPL;
                            detailData.periode_pembayaran = dtStartTagihan.Date.AddMonths(j);
                            detailData.is_active = "Y";

                            headerData.detail_data.Add(detailData);
                        }
                        #endregion

                        listHeader.Add(headerData);
                        dtStartTagihan = dtStartTagihan.AddMonths(durasiBayar);
                    }

                    #region TAMBAH 1 TAGIHAN UNTUK MASA DEPAN JIKA MELEBIHI TGL 15, dan ada pada bulan dan tahun yg sama
                    month = dtEndTagihan.Month;
                    year = dtEndTagihan.Year;

                    if (DateTime.Now.Day >= 15 && 
                        DateTime.Now.Month == month && 
                        DateTime.Now.Year == year)
                    {
                        #region HEADER DATA
                        headerData = new trans_ipl_header();

                        headerData.id_trans = 0;
                        headerData.type_trans = globalConstants.TRANS_TYPE_IPL;
                        headerData.date_issued = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                        headerData.kavling_id = kavlingID;
                        headerData.user_id = userID;

                        if (isRounded)
                            headerData.nominal = gUser.getRoundingMoney(nilaiIPL * durasiBayar, 10000);
                        else
                            headerData.nominal = nilaiIPL * durasiBayar;

                        headerData.nominal_awal = nilaiIPL * durasiBayar;
                        headerData.nominal_disc = headerData.nominal_awal - headerData.nominal;
                        headerData.start_ipl = new DateTime(dtStartTagihan.Year, dtStartTagihan.Month, 1);

                        month = dtStartTagihan.Date.AddMonths(durasiBayar - 1).Month;
                        year = dtStartTagihan.Date.AddMonths(durasiBayar - 1).Year;

                        headerData.end_ipl = new DateTime(year, month, DateTime.DaysInMonth(year, month));//;
                        headerData.is_active = "Y";
                        headerData.detail_data = new List<trans_ipl_detail>();
                        #endregion

                        #region DETAIL
                        for (int j = 0; j < durasiBayar; j++)
                        {
                            detailData = new trans_ipl_detail();

                            detailData.id = 0;
                            detailData.id_trans = 0;
                            detailData.nominal = nilaiIPL;
                            detailData.periode_pembayaran = dtStartTagihan.Date.AddMonths(j);
                            detailData.is_active = "Y";

                            headerData.detail_data.Add(detailData);
                        }
                        #endregion

                        listHeader.Add(headerData);
                        dtStartTagihan = dtStartTagihan.AddMonths(durasiBayar);
                    }
                    #endregion

                    int newID = 0;
                    if (listHeader.Count > 0)
                    {
                        if (!gRest.saveDataIPL(gUser.getUserID(), gUser.getUserToken(), listHeader, 1, out errMsg, out newID))
                        {
                            //throw new Exception(errMsg);
                        }
                    }
                    else
                    {
                        if (userID == 0)
                            errMsg = "Tidak ada user pemilik";
                        else
                            errMsg = "Tidak ada tagihan baru";
                    }

                    kompleksIPLData = new kompleksIPL();
                    kompleksIPLData.generateStatus = (errMsg == "" ? "Success" : "Fail - " + errMsg);
                    kompleksIPLData.kavling_id = kavlingID;
                    kompleksIPLData.kavlingName = kavlingData.dataList[i].blok_name + " - " + kavlingData.dataList[i].house_no;
                    kompleksStatus.Add(kompleksIPLData);

                    progressBar.Value += 1;
                }

                progressBar.Value = progressBar.Maximum;

                result = true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
            }

            return result;
        }

        public bool deleteIPL(int idTrans, out string errMsg)
        {
            errMsg = "";
            bool result = false;

            List<trans_ipl_header> listHeader = new List<trans_ipl_header>();
            trans_ipl_header headerData;

            List<trans_ipl_detail> listDetail = new List<trans_ipl_detail>();

            try
            {
                headerData = new trans_ipl_header();

                headerData.id_trans = idTrans;
                headerData.is_active = "N";

                listHeader.Add(headerData);

                int newID = 0;
                if (!gRest.saveDataIPL(gUser.getUserID(), gUser.getUserToken(), listHeader, 2, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch(Exception ex)
            {
                errMsg = ex.Message;
            }

            return result;
        }

        public int getNumKavling(int userID)
        {
            int result = 0;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT COUNT(1) AS resultQuery " +
                                "FROM user_kavling " +
                                "WHERE user_id = " + userID + " " +
                                "AND is_active = 'Y'";

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    result = Convert.ToInt32(replyResult.data);
            }

            return result;
        }

        public bool isOutstandingTagihanExist(int userID, int kavlingID = 0)
        {
            bool result = false;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT count(1) AS resultQuery " +
                                "FROM transaksi_ipl " +
                                "WHERE user_id = " + userID + " " +
                                "AND status_id <> 2 " +
                                "AND is_active = 'Y'";

            if (kavlingID > 0)
            {
                sqlComm += "AND kavling_id = " + kavlingID;
            }

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    result = true;
            }

            return result;
        }

        public bool isLateOutstandingTagihanExist(int userID, int kavlingID = 0)
        {
            bool result = false;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT count(1) AS resultQuery " +
                                "FROM transaksi_ipl " +
                                "WHERE user_id = " + userID + " " +
                                "AND status_id <> 2 " +
                                "AND is_active = 'Y' " +
                                "AND start_ipl <= NOW() ";

            if (kavlingID > 0)
            {
                sqlComm += "AND kavling_id = " + kavlingID;
            }

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    result = true;
            }

            return result;
        }

        public bool rfidExist(string rfidValue, int userRFID)
        {
            bool result = false;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT count(1) AS resultQuery " +
                                "FROM user_kavling_rfid " +
                                "WHERE rfid_value = '" + rfidValue + "' " +
                                "AND id <> " + userRFID;

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    result = true;
            }

            return result;
        }

        public int getRFIDID(string rfidValue)
        {
            int result = 0;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT id AS resultQuery " +
                                "FROM user_kavling_rfid " +
                                "WHERE rfid_value = '" + rfidValue + "' ";

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    result = Convert.ToInt32(replyResult.data);
            }

            return result;
        }

        public bool terminalExist(string terminalValue, int lineID)
        {
            bool result = false;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT count(1) AS resultQuery " +
                                "FROM kompleks_terminal " +
                                "WHERE UPPER(terminal_id) = '" + terminalValue.ToUpper() + "' " +
                                "AND id <> " + lineID;

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    result = true;
            }

            return result;
        }

        public bool getTerminalStatus(string terminalValue)
        {
            bool result = false;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT is_active AS resultQuery " +
                                "FROM kompleks_terminal " +
                                "WHERE UPPER(terminal_id) = '" + terminalValue.ToUpper() + "' ";

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (replyResult.data.ToString() == "Y")
                    result = true;
            }

            return result;
        }

        public int getTerminalID(string terminalValue)
        {
            int result = 0;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT id AS resultQuery " +
                                "FROM kompleks_terminal " +
                                "WHERE UPPER(terminal_id) = '" + terminalValue.ToUpper() + "' ";

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                result = Convert.ToInt32(replyResult.data);
            }

            return result;
        }

        public bool accessSatpamExist(string accessIDValue, int aksesID, int kompleksID, List<string> deletedID)
        {
            bool result = false;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT count(1) AS resultQuery " +
                                "FROM satpam_access_list " +
                                "WHERE UPPER(access_id) = '" + accessIDValue.ToUpper() + "' " +
                                "AND id <> " + aksesID + " " +
                                "AND kompleks_id = " + kompleksID + " " +
                                "AND is_active = 'Y'";

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    result = true;
            }

            if (deletedID.Contains(accessIDValue.ToUpper()))
                result = false;

            return result;
        }

        public bool isQREnabledStatus(int userID, int kavlingID)
        {
            bool result = false;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT user_qr_enabled AS resultQuery " +
                                "FROM user_kavling " +
                                "WHERE user_id = " + userID + " " +
                                "AND kavling_id = " + kavlingID;

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (replyResult.data.ToString() == "Y")
                    result = true;
            }

            return result;
        }

        public bool isRFIDEnabledStatus(int userID, int kavlingID, string rfidValue)
        {
            bool result = false;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT rfid_enabled AS resultQuery " +
                                "FROM user_kavling uk, user_kavling_rfid ur " +
                                "WHERE uk.user_id = " + userID + " " +
                                "AND uk.kavling_id = " + kavlingID + " " +
                                "AND ur.user_kavling_id = uk.id " +
                                "AND ur.rfid_value = '" + rfidValue + "'";

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (replyResult.data.ToString() == "Y")
                    result = true;
            }

            return result;
        }

        public string getUnpaidData(int userParam, string accessToken, int kavlingID, int userID, 
            out int numData, out DateTime startIPLParam)
        {
            string sqlCommand = "";
            string result = "";
            string startIPL = "";
            string endIPL = "";
            startIPLParam = new DateTime(3000, 1, 1);
            numData = 0;

            REST_IPLUnpaid transData = new REST_IPLUnpaid();

            sqlCommand = "SELECT start_ipl, end_ipl " +
                                    "FROM transaksi_ipl " +
                                    "WHERE kavling_id = " + kavlingID + " " +
                                    "AND user_id = " + userID + " " +
                                    "AND is_active = 'Y' " +
                                    "AND status_id = 0 " +
                                    "ORDER BY start_ipl ASC";

            if (gRest.getUnpaidIPL(userParam, accessToken, sqlCommand, ref transData))
            {
                numData = transData.dataList.Count;

                if (numData > 0)
                {
                    startIPLParam = transData.dataList[0].start_ipl;

                    for (int i = 0; i < transData.dataList.Count - 1; i++)
                    {
                        startIPL = String.Format(culture, "{0:MMM/yy}", transData.dataList[i].start_ipl);
                        endIPL = String.Format(culture, "{0:MMM/yy}", transData.dataList[i].end_ipl);

                        result += startIPL + "-" + endIPL + ", ";
                    }

                    startIPL = String.Format(culture, "{0:MMM/yy}", transData.dataList[transData.dataList.Count - 1].start_ipl);
                    endIPL = String.Format(culture, "{0:MMM/yy}", transData.dataList[transData.dataList.Count - 1].end_ipl);
                    result += startIPL + "-" + endIPL;
                }
            }

            return result;
        }
    }
}
