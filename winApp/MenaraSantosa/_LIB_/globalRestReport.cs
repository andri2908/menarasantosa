﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RestSharp;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace AlphaSoft
{
    class globalRestReport
    {
        private globalRestAPI gRest = new globalRestAPI();
        private globalUserUtil gUser = new globalUserUtil();
        private globalIPL gIPL = new globalIPL();

        public bool generatePrintOutTransaksiIPL(string sqlCommand, ref REST_printoutTransaksi transData)
        {
            REST_printoutTransaksi printoutTrans = new REST_printoutTransaksi();

            var client = new RestClient(globalRestAPI.SERVER_URL + globalRestAPI.FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", gUser.getUserToken());
            request.AddParameter("p_user_id", gUser.getUserID());
            request.AddParameter("sqlQuery", sqlCommand);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_printoutTransaksi>(response.Content);

            transData = (REST_printoutTransaksi)jsonResponse;

            if (transData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool generateLaporanIPLKompleks(string sqlCommand, ref REST_laporanIPL transData)
        {
            REST_laporanIPL reportData = new REST_laporanIPL();

            var client = new RestClient(globalRestAPI.SERVER_URL + globalRestAPI.FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", gUser.getUserToken());
            request.AddParameter("p_user_id", gUser.getUserID());
            request.AddParameter("sqlQuery", sqlCommand);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_laporanIPL>(response.Content);

            transData = (REST_laporanIPL)jsonResponse;

            if (transData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool generateLaporanIPLTahunan(string sqlCommand, ref REST_laporanIPLTahunan transData)
        {
            REST_laporanIPL reportData = new REST_laporanIPL();

            var client = new RestClient(globalRestAPI.SERVER_URL + globalRestAPI.FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", gUser.getUserToken());
            request.AddParameter("p_user_id", gUser.getUserID());
            request.AddParameter("sqlQuery", sqlCommand);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_laporanIPLTahunan>(response.Content);

            transData = (REST_laporanIPLTahunan)jsonResponse;

            if (transData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool generateLaporanAksesKompleks(string sqlCommand, ref REST_laporanAksesKompleks transData)
        {
            REST_laporanIPL reportData = new REST_laporanIPL();

            var client = new RestClient(globalRestAPI.SERVER_URL + globalRestAPI.FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", gUser.getUserToken());
            request.AddParameter("p_user_id", gUser.getUserID());
            request.AddParameter("sqlQuery", sqlCommand);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_laporanAksesKompleks>(response.Content);

            transData = (REST_laporanAksesKompleks)jsonResponse;

            if (transData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool generateLaporanKavling(string sqlCommand, ref REST_laporanKavling transData)
        {
            REST_laporanIPL reportData = new REST_laporanIPL();

            var client = new RestClient(globalRestAPI.SERVER_URL + globalRestAPI.FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", gUser.getUserToken());
            request.AddParameter("p_user_id", gUser.getUserID());
            request.AddParameter("sqlQuery", sqlCommand);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_laporanKavling>(response.Content);

            transData = (REST_laporanKavling)jsonResponse;

            if (transData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool generateTagihanIPL(string sqlCommand, ref REST_tagihanIPLManual transData)
        {
            REST_laporanIPL reportData = new REST_laporanIPL();

            var client = new RestClient(globalRestAPI.SERVER_URL + globalRestAPI.FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", gUser.getUserToken());
            request.AddParameter("p_user_id", gUser.getUserID());
            request.AddParameter("sqlQuery", sqlCommand);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_tagihanIPLManual>(response.Content);

            transData = (REST_tagihanIPLManual)jsonResponse;

            if (transData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }
    }
}
