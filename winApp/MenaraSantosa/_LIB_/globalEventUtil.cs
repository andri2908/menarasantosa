﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;

using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace AlphaSoft
{
    class globalTargetUtil
    {
        public const int EVENT_NEW_KONTAK = 1;
        public const int EVENT_NEW_PENJUALAN = 2;
        public const int EVENT_LATE_FU = 3;

        public const int EVENT_SET_SCRAP = 4;
        public const int EVENT_SET_BUYER = 5;

        public const string EVENT_NEW_KONTAK_MSG = "TARGET KONTAK TIDAK TERPENUHI";
        public const string EVENT_NEW_PENJUALAN_MSG = "TARGET PENJUALAN TIDAK TERPENUHI";

        public const string EVENT_EVALUATION_MSG = "ADA DATA EVALUASI BARU";

        private Data_Access DS;
        private globalDBUtil gDB;
        private globalUserUtil gUser;

        private CultureInfo culture = new CultureInfo("id-ID");

        private string[] targetLog =
        {
            "USER_ID",
            "EVENT_ID",
            "EVENT_REF_ID",
            "EVENT_VALUE"
        };

        private string[] targetLogSummary =
        {
            "USER_ID",
            "EVENT_ID",
            "EVENT_DATE",
            "EVENT_SUMMARY_VALUE"
        };

        private string[] targetLogSummaryMonthly =
        {
            "USER_ID",
            "EVENT_ID",
            "EVENT_DATE_START",
            "EVENT_DATE_END",
            "EVENT_SUMMARY_VALUE",
            "EVENT_DAILY_AVERAGE"
        };

        private string[] targetLogMonthlyAverage =
        {
            "USER_ID",
            "EVENT_ID",
            "EVENT_DATE_START",
            "EVENT_DATE_END",
            "EVENT_SUMMARY_VALUE",
            "EVENT_MONTHLY_AVERAGE"
        };

        public double getNumOfDays(DateTime startD, DateTime endD, bool saturdayIncluded = true)
        {
            int multiplyValue = 6;

            if (!saturdayIncluded)
                multiplyValue = 5;

            double calcBusinessDays = 
                1 + ((endD - startD).TotalDays * multiplyValue - (startD.DayOfWeek - endD.DayOfWeek) * 2) / 7;

            if (!saturdayIncluded)
                if (endD.DayOfWeek == DayOfWeek.Saturday) calcBusinessDays--;

            if (startD.DayOfWeek == DayOfWeek.Sunday) calcBusinessDays--;

            return calcBusinessDays;
        }

        public globalTargetUtil(Data_Access DSParam = null)
        {
            if (null == DSParam)
                DS = new Data_Access();
            else
                DS = DSParam;

            gDB = new globalDBUtil(DS);
            gUser = new globalUserUtil(DS);
        }

        public void saveToUserLog(MySqlException inEx, int userID, int targetID, string targetRefID, double targetValue, string idIDx = "")
        {
            string sqlCommand = "";
            List<string> pList = new List<string>();
            List<object> pVal = new List<object>();

            pList.Clear();
            pVal.Clear();

            gDB.addParamList(pList, targetLog, "_target_" + idIDx);
            pVal.Add(userID);
            pVal.Add(targetID);
            pVal.Add(targetRefID);
            pVal.Add(targetValue);

            sqlCommand = gDB.constructMasterQuery("USER_EVENT_LOG", targetLog, pList);

            if (!DS.executeNonQueryCommandWithParameters(sqlCommand, ref inEx, pList, pVal))
                throw inEx;
        }

        public void saveToUserLogSummary(int userID, int targetID, double targetValue, DateTime dtStart, string idIDx = "")
        {
            string sqlCommand = "";
            MySqlException inEx = null;
            List<string> pList = new List<string>();
            List<object> pVal = new List<object>();

            pList.Clear();
            pVal.Clear();

            DS.beginTransaction();

            try
            {
                gDB.addParamList(pList, targetLogSummary, "_trgtSummary_" + idIDx);

                pVal.Add(userID);
                pVal.Add(targetID);
                pVal.Add(dtStart.Date);
                pVal.Add(targetValue);

                sqlCommand = gDB.constructMasterQuery("USER_EVENT_LOG_SUMMARY", targetLogSummary, pList);

                if (!DS.executeNonQueryCommandWithParameters(sqlCommand, ref inEx, pList, pVal))
                    throw inEx;

                DS.commit();
            }
            catch (Exception ex) { }
        }

        public void saveToUserLogSummary(MySqlException inEx, int userID, int targetID, double targetValue, 
            DateTime dtStart, string idIDx = "")
        {
            string sqlCommand = "";
            List<string> pList = new List<string>();
            List<object> pVal = new List<object>();

            pList.Clear();
            pVal.Clear();

            gDB.addParamList(pList, targetLogSummary, "_sumDaily_" + idIDx);

            pVal.Add(userID);
            pVal.Add(targetID);
            pVal.Add(dtStart.Date);
            pVal.Add(targetValue);

            sqlCommand = gDB.constructMasterQuery("USER_EVENT_LOG_SUMMARY_DAILY", targetLogSummary, pList);

            if (!DS.executeNonQueryCommandWithParameters(sqlCommand, ref inEx, pList, pVal))
                throw inEx;
        }

        public void saveToUserLogSummaryMonthly(MySqlException inEx, int userID, int targetID, double targetValue,
            DateTime dtStart, DateTime dtEnd, string idIDx = "")
        {
            string sqlCommand = "";
            List<string> pList = new List<string>();
            List<object> pVal = new List<object>();
            double totalEfektifDate = 0;
            double dailyAverage = 0;

            totalEfektifDate = getNumOfDays(dtStart, dtEnd);

            if (targetID == globalTargetUtil.EVENT_LATE_FU)
                dailyAverage = Math.Round((totalEfektifDate - targetValue) / totalEfektifDate * 100, 2);
            else
                dailyAverage = Math.Round(targetValue / totalEfektifDate, 2);

            pList.Clear();
            pVal.Clear();

            gDB.addParamList(pList, targetLogSummaryMonthly, "_sumMonthly_" + idIDx);

            pVal.Add(userID);
            pVal.Add(targetID);
            pVal.Add(dtStart.Date);
            pVal.Add(dtEnd.Date);
            pVal.Add(targetValue);
            pVal.Add(dailyAverage);

            sqlCommand = gDB.constructMasterQuery("USER_EVENT_LOG_SUMMARY_MONTHLY", targetLogSummaryMonthly, pList);

            if (!DS.executeNonQueryCommandWithParameters(sqlCommand, ref inEx, pList, pVal))
                throw inEx;
        }

        public void saveToUserLogSummaryMonthlyAverage(MySqlException inEx, int userID, int targetID, double targetValue, DateTime dtStart, DateTime dtEnd, string idIDx = "")
        {
            string sqlCommand = "";
            List<string> pList = new List<string>();
            List<object> pVal = new List<object>();
            double totalMonth = 0;
            double averageValue = 0;

            totalMonth = ((dtEnd.Year - dtStart.Year) * 12 + dtEnd.Month - dtStart.Month) + 1;
            averageValue = Math.Round(targetValue / totalMonth, 2);

            pList.Clear();
            pVal.Clear();

            gDB.addParamList(pList, targetLogMonthlyAverage, "_monthlyAverage_" + idIDx);

            pVal.Add(userID);
            pVal.Add(targetID);
            pVal.Add(dtStart.Date);
            pVal.Add(dtEnd.Date);
            pVal.Add(targetValue);
            pVal.Add(averageValue);

            sqlCommand = gDB.constructMasterQuery("USER_EVENT_LOG_MONTHLY_AVERAGE", targetLogMonthlyAverage, pList);

            if (!DS.executeNonQueryCommandWithParameters(sqlCommand, ref inEx, pList, pVal))
                throw inEx;
        }
    }

}
