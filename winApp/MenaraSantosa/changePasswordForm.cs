﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data;
using MySql.Data.MySqlClient;
using Hotkeys;
using Microsoft.SqlServer;
using System.Data.SqlClient;

namespace AlphaSoft
{
    public partial class changePasswordForm : basicHotkeysForm
    {
        private globalUserUtil gUtil;
        
        public changePasswordForm()
        {
            InitializeComponent();

            gUtil = new globalUserUtil();
        }

        private bool validateOldPassword()
        {
            string oldPassword = oldPasswordTextBox.Text;
            oldPassword = MySqlHelper.EscapeString(oldPassword);

            return gUtil.RS_validateUser(oldPassword);
        }

        private bool dataValidated()
        {
            if (oldPasswordTextBox.Text.Trim().Equals(""))
            {
                errorLabel.Text = "Password lama kosong";
                return false;
            }

            if (!validateOldPassword())
            {
                errorLabel.Text = "Password lama salah";
                return false;
            }
            
            if (newPasswordTextBox.Text.Trim().Equals(""))
            {
                errorLabel.Text = "Password baru kosong";
                return false;
            }

            if (!gUtil.matchRegEx(newPasswordTextBox.Text, globalUtilities.REGEX_ALPHANUMERIC_ONLY))
            {
                errorLabel.Text = "Password hanya boleh mengandung angka dan huruf";
                return false;
            }

            if (!newPasswordTextBox.Text.Equals(newPassword2TextBox.Text))
            {
                errorLabel.Text = "New password dan re-type password tidak sama";
                return false;
            }

            return true;
        }

        private bool saveDataTransaction()
        {
            bool result = false;

            try
            {
                string newPassword = newPasswordTextBox.Text;

                newPassword = gUtil.getMD5Value(newPassword);

                gUtil.RS_changePassword(newPassword);
                result = true;
            }
            catch(Exception ex)
            {
                errorLabel.Text = ex.Message;
            }
            
            return result;
        }

        private bool saveData()
        {
            bool result = false;
            if (dataValidated())
            {
                //  ALlow main UI thread to properly display please wait form.
                //Application.DoEvents();
                result = saveDataTransaction();

                return result;
            }

            return result;
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (saveData())
            {
                gUtil.showSuccess(globalUtilities.UPD);
                gUtil.ResetAllControls(this);                
            }
        }

        private void changePasswordForm_Load(object sender, EventArgs e)
        {
            Button[] arrButton = new Button[2];

            arrButton[0] = loginButton;
            arrButton[1] = button1;
            gUtil.reArrangeButtonPosition(arrButton, arrButton[0].Top, this.Width);

            gUtil.reArrangeTabOrder(this);
            oldPasswordTextBox.Select();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            gUtil.ResetAllControls(this);
        }

        private void changePasswordForm_Activated(object sender, EventArgs e)
        {
            errorLabel.Text = "";
            registerGlobalHotkey();
        }

        private void changePasswordForm_Deactivate(object sender, EventArgs e)
        {
            unregisterGlobalHotkey();
        }

        private void showPassCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (showPassCheckBox.Checked)
            {
                oldPasswordTextBox.PasswordChar = '\0';
                newPassword2TextBox.PasswordChar = '\0';
                newPasswordTextBox.PasswordChar = '\0';
            }
            else
            {
                oldPasswordTextBox.PasswordChar = '*';
                newPassword2TextBox.PasswordChar = '*';
                newPasswordTextBox.PasswordChar = '*';
            }
        }
    }
}
