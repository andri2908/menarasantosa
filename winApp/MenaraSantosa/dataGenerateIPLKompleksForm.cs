﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace AlphaSoft
{
    public partial class dataGenerateIPLKompleksForm : AlphaSoft.basicHotkeysForm
    {
        private int originModuleID = 0;
        private int selectedKavlingID = 0;
        private int selectedKompleksID = 0;
        private int selectedUserID = 0;

        private globalUserUtil gUser;
        private globalIPL gIPL;
        private globalRestAPI gRest;

        private CultureInfo culture = new CultureInfo("id-ID");


        public dataGenerateIPLKompleksForm()
        {
            InitializeComponent();

            gUser = new globalUserUtil();
            gIPL = new globalIPL();
            gRest = new globalRestAPI();
        }

        private void dataGenerateIPLKompleksForm_Load(object sender, EventArgs e)
        {
        }

        private void searchKompleksButton_Click(object sender, EventArgs e)
        {
            dataBrowse_KompleksForm displayForm = new dataBrowse_KompleksForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                selectedKompleksID = Convert.ToInt32(displayForm.ReturnValue1);
                kompleksTextBox.Text = displayForm.ReturnValue2;

                SaveButton.Enabled = true;
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            string errMsg = "";
            List<kompleksIPL> kompleksData = new List<kompleksIPL>();

            SaveButton.Enabled = false;
            searchKompleksButton.Enabled = false;
            roundingIPL.Enabled = false;

            if (!gIPL.generateOutstandingIPLKompleks(selectedKompleksID, progressBar1, kompleksData, roundingIPL.Checked,
                out errMsg))
            {
                errorLabel.Text = errMsg;
            }

            iplGridView.Rows.Clear();
            for (int i = 0;i<kompleksData.Count;i++)
            {
                iplGridView.Rows.Add(
                    kompleksData[i].kavling_id,
                    kompleksData[i].kavlingName,
                    kompleksData[i].generateStatus
                    );
            }

            SaveButton.Enabled = true;
            searchKompleksButton.Enabled = true;
            roundingIPL.Enabled = true;
        }
    }
}
