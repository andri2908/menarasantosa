﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DBGridExtension;
using System.Globalization;
using System.Runtime.InteropServices;
using System.IO;

namespace AlphaSoft
{
    public partial class dataTiketCC : AlphaSoft.basicHotkeysForm
    {
        private int ticketID = 0;
        private int employeeID = 0;
        private string employeeToken = "";

        private CultureInfo culture = new CultureInfo("id-ID");

        private globalRestAPI gRest;
        private globalUserUtil gUser;
        private globalImageLib gImg;

        private bool isViewOnly = false;

        private int startX = 0;
        private int startY = 0;
        private int midX = 0;
        private int newID = 0;
        private int lastID = 0;

        private int lastX = 0;
        private int lastY = 0;

        private string lastTxtID = "";

        List<ccTicket> listCCTicket = new List<ccTicket>();

        public dataTiketCC(int ticketIDParam)
        {
            InitializeComponent();

            ticketID = ticketIDParam;

            gRest = new globalRestAPI();
            gUser = new globalUserUtil();
            gImg = new globalImageLib();

            loadDataTicket();
        }

        private void loadDataTicket()
        {
            string sqlCommand = "";
            REST_custCare headerData = new REST_custCare();

            string sqlUser = "SELECT 'user' as sender_type, mu.user_id, mu.user_name, mu.user_full_name, mg.group_id " +
                                   "FROM user_login_data mu, master_group mg " +
                                   "WHERE mg.group_id = mu.group_id " +
                                   "UNION ALL " +
                                   "SELECT 'admin' as sender_type, me.employee_id as user_id, me.employee_user_name as user_name, me.employee_full_name as user_full_name, 2 as group_id " +
                                   "FROM employee_login_data me";

            #region HEADER
            sqlCommand = "SELECT cc.ticket_id, cc.ticket_num, cc.ticket_date, cc.request_by, cc.kavling_id, cc.kompleks_id, cc.subject, cc.description, cc.status, cc.is_closed, cc.is_active, " +
                               "ul.user_name, ul.user_full_name, ml.kompleks_name, " +
                               "mb.blok_name, mk.house_no, cc.ticket_type as category_name " +
                               "FROM cc_ticket cc " +
                               "LEFT OUTER JOIN (" + sqlUser + ") ul ON (cc.request_by = ul.user_id AND if(cc.sender_type = null, '', convert(cc.sender_type, char)) = ul.sender_type) " +
                               "LEFT OUTER JOIN master_kavling mk ON (cc.kavling_id = mk.kavling_id AND mk.is_active = 'Y') " +
                               "LEFT OUTER JOIN master_kompleks ml ON (mk.kompleks_id = ml.kompleks_id AND ml.is_active = 'Y') " +
                               "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id AND mb.is_active = 'Y') " +
                               "WHERE cc.ticket_id = " + ticketID;

            if (gRest.getCustCareData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref headerData))
            {
                if (headerData.dataStatus.o_status == 1)
                {
                    noTicketTextBox.Text = headerData.dataList[0].ticket_num;
                    subjectTextBox.Text = headerData.dataList[0].subject;
                    isViewOnly = (headerData.dataList[0].is_active == "N" || headerData.dataList[0].is_closed == "Y" ? true : false);
                }
            }
            #endregion

            loadDataDetailChat();

            if (isViewOnly)
            {
                gUser.disableAllControls(this);
            }

            txtBoxKirim.Select();
        }

        private void AutoSizeTextBox(TextBox txt)
        {
            int txtWidth = 0;
            int txtHeight = 0;

            const int x_margin = 0;
            const int y_margin = 2;

            int midX = panelComponent.Width / 2;

            Size size = TextRenderer.MeasureText(txt.Text, txt.Font);

            if (size.Width > midX)
            {
                int numLines = 0;

                txtWidth = midX;
                numLines = size.Width / midX;

                txtHeight = size.Height * (numLines + 1);
            }
            else
            {
                txtWidth = size.Width + 10;
                txtHeight = size.Height;
            }

            txt.ClientSize = new Size(txtWidth + x_margin, txtHeight+ y_margin);
        }

        private void readjustAdminChat()
        {
            int maxX = panelComponent.Width - 20;

            for (int i = 0; i < listCCTicket.Count; i++)
            {
                if (listCCTicket[i].isAdmin)
                {
                    if (listCCTicket[i].imgName.Length > 0)
                    {
                        listCCTicket[i].pbBox.Left = maxX - listCCTicket[i].pbBox.Width;
                    }
                    else
                    {
                        listCCTicket[i].txtBox.Left = maxX - listCCTicket[i].txtBox.Width;
                    }
                }
            }
        }

        private void clearDetailChat()
        {
            for (int i = 0;i< listCCTicket.Count;i++)
            {
                if (listCCTicket[i].imgName.Length > 0)
                {
                    listCCTicket[i].pbBox.Dispose();
                }
                else
                {
                    listCCTicket[i].txtBox.Dispose();
                }
            }
        }

        private void loadDataDetailChat()
        {
            string sqlCommand = "";
            REST_custCareDetail detailData = new REST_custCareDetail();
            ccTicket ccElement;
            bool isAdmin = false;

            string sqlUser = "SELECT 'user' as sender_type, mu.user_id, mu.user_name, mu.user_full_name, mg.group_id " +
                                    "FROM user_login_data mu, master_group mg " +
                                    "WHERE mg.group_id = mu.group_id " +
                                    "UNION ALL " +
                                    "SELECT 'admin' as sender_type, me.employee_id as user_id, me.employee_user_name as user_name, me.employee_full_name as user_full_name, 2 as group_id " +
                                    "FROM employee_login_data me";

            #region DETAIL
            sqlCommand = "SELECT cc.history_id, cc.ticket_id, cc.sent_by, " +
                                    "ifnull(ul.user_name, '') as user_name, ifnull(ul.user_full_name, '') as user_full_name, cc.send_date, ifnull(cc.description, '') as description, " +
                                    "cc.is_mtc, ifnull(cc.mtc_cost, 0) as mtc_cost, cc.is_active, " +
                                    "ifnull(cct.attach_file, '') as attach_file, ul.group_id " + 
                                    "FROM cc_ticket_history cc " +
                                    "LEFT OUTER JOIN (" + sqlUser + ") ul ON (cc.sent_by = ul.user_id AND if(cc.sender_type = null, '', convert(cc.sender_type, char)) = ul.sender_type) " +
                                    "LEFT OUTER JOIN cc_ticket_history_attach cct ON (cct.history_id = cc.history_id)" +
                                    "WHERE cc.ticket_id = " + ticketID + " " +
                                    "ORDER BY cc.history_id ASC";

            if (gRest.getCustCareDetailData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref detailData))
            {
                if (detailData.dataStatus.o_status == 1)
                {
                    string attachFileName = "";

                    startX = 10;
                    startY = 10;

                    for (int i = 0; i < detailData.dataList.Count; i++)
                    {
                        attachFileName = detailData.dataList[i].attach_file;
                        lastID = detailData.dataList[i].history_id;

                        if (attachFileName.Length > 0)
                        {
                            try
                            {
                                Bitmap bm;
                                bm = gImg.ByteToImage(attachFileName, globalImageLib.IMG_CS);

                                PictureBox pbBox = new PictureBox();

                                pbBox.Parent = panelComponent;
                                pbBox.BorderStyle = BorderStyle.None;
                                pbBox.Top = startY;
                                pbBox.Left = startX;
                                pbBox.Width = 150;
                                pbBox.Height = 150;
                                pbBox.Name = "attach_" + detailData.dataList[i].history_id;
                                lastTxtID = "attach_" + detailData.dataList[i].history_id;

                                pbBox.Image = bm;
                                pbBox.SizeMode = PictureBoxSizeMode.StretchImage;

                                pbBox.DoubleClick += pbBox_DoubleClick;

                                if (detailData.dataList[i].group_id == globalConstants.GROUP_ADMIN)
                                {
                                    isAdmin = true;
                                }
                                else
                                {
                                    isAdmin = false;
                                }

                                ccElement = new ccTicket();
                                ccElement.component_name = "attach_" + detailData.dataList[i].history_id;
                                ccElement.history_id = detailData.dataList[i].history_id;
                                ccElement.imgName = attachFileName;
                                ccElement.isAdmin = isAdmin;
                                ccElement.pbBox = pbBox;
                                listCCTicket.Add(ccElement);

                                startY = startY + pbBox.Height + 5;
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        else
                        {
                            TextBox txtBox = new TextBox();
                            txtBox.Parent = panelComponent;

                            txtBox.Top = startY;
                            txtBox.Left = startX;
                            txtBox.BorderStyle = BorderStyle.None;

                            txtBox.Multiline = true;
                            txtBox.ReadOnly = true;

                            if (detailData.dataList[i].group_id == globalConstants.GROUP_ADMIN)
                            {
                                txtBox.BackColor = Color.LightGreen;
                                txtBox.ForeColor = Color.Black;
                                isAdmin = true;
                            }
                            else
                            {
                                txtBox.BackColor = Color.White;
                                txtBox.ForeColor = Color.Black;
                                isAdmin = false;
                            }

                            txtBox.Font = new Font("Verdana", 12);//, FontStyle.Bold);
                            txtBox.Name = "txt_" + detailData.dataList[i].history_id;
                            lastTxtID = "txt_" + detailData.dataList[i].history_id;

                            txtBox.TextChanged += txtBox_TextChanged;
                           
                            txtBox.Text = detailData.dataList[i].description;

                            startY = startY + txtBox.Height + 5;

                            ccElement = new ccTicket();
                            ccElement.component_name = "txt_" + detailData.dataList[i].history_id;
                            ccElement.history_id = detailData.dataList[i].history_id;
                            ccElement.imgName = "";
                            ccElement.isAdmin = isAdmin;
                            ccElement.txtBox = txtBox;

                            listCCTicket.Add(ccElement);
                        }
                    }

                    lastX = startX;
                    lastY = startY;
                }
            }
            #endregion

            readjustAdminChat();
        }

        private void loadSingleDetailChat(int newIDParam)
        {
            string sqlCommand = "";
            REST_custCareDetail detailData = new REST_custCareDetail();
            ccTicket ccElement = null;
            bool isAdmin = false;

            string sqlUser = "SELECT 'user' as sender_type, mu.user_id, mu.user_name, mu.user_full_name, mg.group_id " +
                                   "FROM user_login_data mu, master_group mg " +
                                   "WHERE mg.group_id = mu.group_id " +
                                   "UNION ALL " +
                                   "SELECT 'admin' as sender_type, me.employee_id as user_id, me.employee_user_name as user_name, me.employee_full_name as user_full_name, 2 as group_id " +
                                   "FROM employee_login_data me";

            #region DETAIL
            sqlCommand = "SELECT cc.history_id, cc.ticket_id, cc.sent_by, " +
                                    "ul.user_name, ul.user_full_name, cc.send_date, ifnull(cc.description, '') as description, " +
                                    "cc.is_mtc, ifnull(cc.mtc_cost, 0) as mtc_cost, cc.is_active, " +
                                    "ifnull(cct.attach_file, '') as attach_file, ul.group_id " +
                                    "FROM cc_ticket_history cc " +
                                    "LEFT OUTER JOIN (" + sqlUser + ") ul ON (cc.sent_by = ul.user_id AND if(cc.sender_type = null, '', convert(cc.sender_type, char)) = ul.sender_type) " +
                                    "LEFT OUTER JOIN cc_ticket_history_attach cct ON (cct.history_id = cc.history_id)" +
                                    "WHERE cc.ticket_id = " + ticketID + " " +
                                    "AND cc.history_id = " + newIDParam + " " +
                                    "ORDER BY cc.history_id ASC";

            if (gRest.getCustCareDetailData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref detailData))
            {
                if (detailData.dataStatus.o_status == 1)
                {
                    string attachFileName = "";

                    if (lastTxtID.IndexOf("txt") >= 0)
                    {
                        TextBox txtLast = this.Controls.Find(lastTxtID, true)[0] as TextBox;
                        startY = txtLast.Top + txtLast.Height + 5;
                    }
                    else
                    {
                        PictureBox pbLast = this.Controls.Find(lastTxtID, true)[0] as PictureBox;
                        startY = pbLast.Top + pbLast.Height + 5;
                    }

                    startX = lastX;

                    for (int i = 0; i < detailData.dataList.Count; i++)
                    {
                        attachFileName = detailData.dataList[i].attach_file;
                        lastID = detailData.dataList[i].history_id;
                        if (attachFileName.Length > 0)
                        {
                            try
                            {
                                Bitmap bm;
                                bm = gImg.ByteToImage(attachFileName, globalImageLib.IMG_CS);

                                PictureBox pbBox = new PictureBox();

                                pbBox.Parent = panelComponent;
                                pbBox.BorderStyle = BorderStyle.None;
                                pbBox.Top = startY;
                                pbBox.Left = startX;
                                pbBox.Width = 150;
                                pbBox.Height = 150;
                                pbBox.Name = "attach_" + detailData.dataList[i].history_id;
                                lastTxtID = "attach_" + detailData.dataList[i].history_id;

                                pbBox.Image = bm;
                                pbBox.SizeMode = PictureBoxSizeMode.StretchImage;

                                pbBox.DoubleClick += pbBox_DoubleClick;

                                if (detailData.dataList[i].group_id == globalConstants.GROUP_ADMIN)
                                {
                                    isAdmin = true;
                                }
                                else
                                {
                                    isAdmin = false;
                                }

                                ccElement = new ccTicket();
                                ccElement.component_name = "attach_" + detailData.dataList[i].history_id;
                                ccElement.history_id = detailData.dataList[i].history_id;
                                ccElement.imgName = attachFileName;
                                ccElement.isAdmin = isAdmin;
                                ccElement.pbBox = pbBox;
                                listCCTicket.Add(ccElement);

                                startY += pbBox.Height + 5;
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        else
                        {
                            TextBox txtBox = new TextBox();
                            txtBox.Parent = panelComponent;

                            txtBox.Top = startY;
                            txtBox.Left = startX;
                            txtBox.BorderStyle = BorderStyle.None;

                            txtBox.Multiline = true;
                            txtBox.ReadOnly = true;

                            if (detailData.dataList[i].group_id == globalConstants.GROUP_ADMIN)
                            {
                                txtBox.BackColor = Color.LightGreen;
                                txtBox.ForeColor = Color.Black;
                                isAdmin = true;
                            }
                            else
                            {
                                txtBox.BackColor = Color.White;
                                txtBox.ForeColor = Color.Black;
                                isAdmin = false;
                            }

                            txtBox.Font = new Font("Verdana", 12);//, FontStyle.Bold);
                            txtBox.Name = "txt_" + detailData.dataList[i].history_id;
                            lastTxtID = "txt_" + detailData.dataList[i].history_id;

                            txtBox.TextChanged += txtBox_TextChanged;

                            txtBox.Text = detailData.dataList[i].description;

                            startY += txtBox.Height + 5;

                            ccElement = new ccTicket();
                            ccElement.component_name = "txt_" + detailData.dataList[i].history_id;
                            ccElement.history_id = detailData.dataList[i].history_id;
                            ccElement.imgName = "";
                            ccElement.isAdmin = isAdmin;
                            ccElement.txtBox = txtBox;

                            listCCTicket.Add(ccElement);
                        }
                    }

                    lastX = startX;
                    lastY = startY;

                    if (isAdmin)
                    {
                        int maxX = panelComponent.Width - 20;
                        if (attachFileName.Length > 0)
                        {
                            ccElement.pbBox.Left = maxX - ccElement.pbBox.Width;
                        }
                        else
                        {
                            ccElement.txtBox.Left = maxX - ccElement.txtBox.Width;
                        }
                    }
                }
            }
            #endregion
        }

        private void refreshDetailChat()
        {
            string sqlCommand = "";
            REST_custCareDetail detailData = new REST_custCareDetail();
            ccTicket ccElement = null;
            bool isAdmin = false;

            string sqlUser = "SELECT 'user' as sender_type, mu.user_id, mu.user_name, mu.user_full_name, mg.group_id " +
                                "FROM user_login_data mu, master_group mg " +
                                "WHERE mg.group_id = mu.group_id " +
                                "UNION ALL " +
                                "SELECT 'admin' as sender_type, me.employee_id as user_id, me.employee_user_name as user_name, me.employee_full_name as user_full_name, 2 as group_id " +
                                "FROM employee_login_data me";
            
            #region DETAIL
            sqlCommand = "SELECT cc.history_id, cc.ticket_id, cc.sent_by, " +
                                    "ul.user_name, ul.user_full_name, cc.send_date, ifnull(cc.description, '') as description, " +
                                    "cc.is_mtc, ifnull(cc.mtc_cost, 0) as mtc_cost, cc.is_active, " +
                                    "ifnull(cct.attach_file, '') as attach_file, ul.group_id " +
                                    "FROM cc_ticket_history cc " +
                                    "LEFT OUTER JOIN (" + sqlUser + ") ul ON (cc.sent_by = ul.user_id AND if(cc.sender_type = null, '', convert(cc.sender_type, char)) = ul.sender_type) " +
                                    "LEFT OUTER JOIN cc_ticket_history_attach cct ON (cct.history_id = cc.history_id)" +
                                    "WHERE cc.ticket_id = " + ticketID + " " +
                                    "AND cc.history_id > " + lastID + " " +
                                    "ORDER BY cc.history_id ASC";

            if (gRest.getCustCareDetailData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref detailData))
            {
                if (detailData.dataStatus.o_status == 1)
                {
                    string attachFileName = "";

                    if (lastTxtID.IndexOf("txt") >= 0)
                    {
                        TextBox txtLast = this.Controls.Find(lastTxtID, true)[0] as TextBox;
                        startY = txtLast.Top + txtLast.Height + 5;
                    }
                    else
                    {
                        PictureBox pbLast = this.Controls.Find(lastTxtID, true)[0] as PictureBox;
                        startY = pbLast.Top + pbLast.Height + 5;
                    }

                    startX = lastX;
                    
                    for (int i = 0; i < detailData.dataList.Count; i++)
                    {
                        attachFileName = detailData.dataList[i].attach_file;
                        lastID = detailData.dataList[i].history_id;

                        if (attachFileName.Length > 0)
                        {
                            try
                            {
                                Bitmap bm;
                                bm = gImg.ByteToImage(attachFileName, globalImageLib.IMG_CS);

                                PictureBox pbBox = new PictureBox();

                                pbBox.Parent = panelComponent;
                                pbBox.BorderStyle = BorderStyle.None;
                                pbBox.Top = startY;
                                pbBox.Left = startX;
                                pbBox.Width = 150;
                                pbBox.Height = 150;
                                pbBox.Name = "attach_" + detailData.dataList[i].history_id;
                                lastTxtID = "attach_" + detailData.dataList[i].history_id;
                                pbBox.Image = bm;
                                pbBox.SizeMode = PictureBoxSizeMode.StretchImage;

                                pbBox.DoubleClick += pbBox_DoubleClick;

                                if (detailData.dataList[i].group_id == globalConstants.GROUP_ADMIN)
                                {
                                    isAdmin = true;
                                }
                                else
                                {
                                    isAdmin = false;
                                }

                                ccElement = new ccTicket();
                                ccElement.component_name = "attach_" + detailData.dataList[i].history_id;
                                ccElement.history_id = detailData.dataList[i].history_id;
                                ccElement.imgName = attachFileName;
                                ccElement.isAdmin = isAdmin;
                                ccElement.pbBox = pbBox;
                                listCCTicket.Add(ccElement);

                                startY += pbBox.Height + 5;
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        else
                        {
                            TextBox txtBox = new TextBox();
                            txtBox.Parent = panelComponent;

                            txtBox.Top = startY;
                            txtBox.Left = startX;
                            txtBox.BorderStyle = BorderStyle.None;

                            txtBox.Multiline = true;
                            txtBox.ReadOnly = true;

                            if (detailData.dataList[i].group_id == globalConstants.GROUP_ADMIN)
                            {
                                txtBox.BackColor = Color.LightGreen;
                                txtBox.ForeColor = Color.Black;
                                isAdmin = true;
                            }
                            else
                            {
                                txtBox.BackColor = Color.White;
                                txtBox.ForeColor = Color.Black;
                                isAdmin = false;
                            }

                            txtBox.Font = new Font("Verdana", 12);//, FontStyle.Bold);
                            txtBox.Name = "txt_" + detailData.dataList[i].history_id;
                            lastTxtID = "txt_" + detailData.dataList[i].history_id;

                            txtBox.TextChanged += txtBox_TextChanged;

                            txtBox.Text = detailData.dataList[i].description;

                            startY += txtBox.Height + 5;

                            ccElement = new ccTicket();
                            ccElement.component_name = "txt_" + detailData.dataList[i].history_id;
                            ccElement.history_id = detailData.dataList[i].history_id;
                            ccElement.imgName = "";
                            ccElement.isAdmin = isAdmin;
                            ccElement.txtBox = txtBox;

                            listCCTicket.Add(ccElement);
                        }
                    }

                    lastX = startX;
                    lastY = startY;

                    if (isAdmin)
                    {
                        int maxX = panelComponent.Width - 20;
                        if (attachFileName.Length > 0)
                        {
                            ccElement.pbBox.Left = maxX - ccElement.pbBox.Width;
                        }
                        else
                        {
                            ccElement.txtBox.Left = maxX - ccElement.txtBox.Width;
                        }
                    }
                }
            }
            #endregion
        }

        private void selectEmployeeID()
        {
            while (employeeID == 0)
            {
                dataBrowse_employee displayForm = new dataBrowse_employee(globalConstants.MODULE_DEFAULT, 0, "Reply as ");
                displayForm.ShowDialog(this);

                if (displayForm.ReturnValue1 != "0")
                {
                    employeeID = Convert.ToInt32(displayForm.ReturnValue1);
                    labelLogin.Text = "Reply as [" + displayForm.ReturnValue2 + "] " + displayForm.retUserFullName;

                    genericReply replyResult = new genericReply();
                    gRest.getEmployeeToken(gUser.getUserID(), gUser.getUserToken(), employeeID, ref replyResult);

                    employeeToken = replyResult.data.ToString();
                }
            }
        }

        private void dataTiketCC_Load(object sender, EventArgs e)
        {
            readjustAdminChat();

            selectEmployeeID();

            timerRefresh.Start();
        }

        private void txtBox_TextChanged(object sender, EventArgs e)
        {
            TextBox txtBox = (TextBox)sender;

            AutoSizeTextBox(txtBox);
        }

        private void displayImage(string komponenName)
        {
            string fileName = "";
            ccTicket ccT;

            ccT = listCCTicket.Find(x => x.component_name == komponenName);
            if (null != ccT)
            {
                fileName = ccT.imgName;
            }

            if (fileName.Length > 0)
            {
                displayImageForm displayForm = new displayImageForm(fileName, globalImageLib.IMG_CS, true);
                displayForm.ShowDialog(this);
            }
        }

        private bool saveData(bool isImage = false, string imgFileName = "")
        {
            bool result = false;
            string errMsg = "";
            string commentValue = "";

            cc_ticket_history detailData = new cc_ticket_history();
            List<cc_ticket_history> listDetail = new List<cc_ticket_history>();
            List<cc_ticket> headerData = new List<cc_ticket>();
            List<ccAttach> listAttach = new List<ccAttach>();
            ccAttach attachImg;
            cc_ticket ticketData = new cc_ticket();

            try
            {
                if (!isImage)
                    commentValue = txtBoxKirim.Text;

                if (isImage)
                {
                    if (imgFileName.Length > 0)
                    {
                        PictureBox pb = new PictureBox();
                        pb.Load(imgFileName);

                        attachImg = new ccAttach();

                        using (Image image = pb.Image)
                        {
                            using (MemoryStream m = new MemoryStream())
                            {
                                image.Save(m, image.RawFormat);
                                byte[] imageBytes = m.ToArray();

                                // Convert byte[] to Base64 String
                                string base64String = Convert.ToBase64String(imageBytes);
                                attachImg.raw_photo = base64String;
                                attachImg.seqno = 0;
                            }
                        }

                        listAttach.Add(attachImg);
                    }
                }

                ccAttach[] array = listAttach.ToArray();
                if (!gRest.saveCCDetail(gUser.getUserID(), employeeToken, employeeID, //gUser.getUserToken(), employeeID,
                    ticketID, commentValue, "N", 0, "N",
                   "admin", array, out errMsg, out newID)
                    )
                {
                    throw new Exception(errMsg);
                }

                if (detailData.history_id == 0)
                    detailData.history_id = newID;

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (txtBoxKirim.Text.Length > 0)
            {
                if (saveData())
                {
                    MessageBox.Show("Success");
                    txtBoxKirim.Clear();

                    loadSingleDetailChat(newID);
                }
                else
                {
                    MessageBox.Show("Fail");
                }
            }
            else
            {
                MessageBox.Show("Pesan Kosong");
            }
        }

        private void pbBox_DoubleClick(object sender, EventArgs e)
        {
            PictureBox pbBox = (PictureBox)sender;
            displayImage(pbBox.Name);
        }

        private void addImage()
        {
            string fileName = "";

            imgFileDialog.Filter = "Image Files(*.JPG; *.JPEG)| *.JPG; *.JPEG";
            if (imgFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    fileName = imgFileDialog.FileName;

                    if (saveData(true, fileName))
                    {
                        MessageBox.Show("Success");

                        loadSingleDetailChat(newID);
                    }
                    else
                    {
                        MessageBox.Show("Fail");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("File gambar tidak bisa dibuka : " + ex.Message);
                }
            }
        }

        private void imgButton_Click(object sender, EventArgs e)
        {
            addImage();
        }

        private bool closeTicket()
        {
            bool result = false;
            string errMsg = "";

            try
            {
                if (!gRest.closeCCTicket(employeeID, employeeToken, // gUser.getUserID(), gUser.getUserToken(),
                    ticketID, out errMsg, out newID)
                    )
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private void finishButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Tiket selesai?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {
                if (closeTicket())
                {
                    MessageBox.Show("Success");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Fail");
                }
            }
        }

        private void timerRefresh_Tick(object sender, EventArgs e)
        {
            //clearDetailChat();
            //loadDataDetailChat();
            refreshDetailChat();
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            //clearDetailChat();
            //loadDataDetailChat();
            refreshDetailChat();
        }
    }
}
