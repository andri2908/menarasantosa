﻿namespace AlphaSoft
{
    partial class dataCustCare_detailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dataCustCare_detailForm));
            this.panel2 = new System.Windows.Forms.Panel();
            this.descTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.subjectTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.noTicketTextBox = new System.Windows.Forms.TextBox();
            this.confirmationCheckBox = new System.Windows.Forms.CheckBox();
            this.delImg4Button = new System.Windows.Forms.Button();
            this.delImg3Button = new System.Windows.Forms.Button();
            this.delImg1Button = new System.Windows.Forms.Button();
            this.delImg2Button = new System.Windows.Forms.Button();
            this.img4Button = new System.Windows.Forms.Button();
            this.img3Button = new System.Windows.Forms.Button();
            this.img2Button = new System.Windows.Forms.Button();
            this.img1Button = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.nonAktifCheckBox = new System.Windows.Forms.CheckBox();
            this.checkBoxMTC = new System.Windows.Forms.CheckBox();
            this.biayaTextBox = new System.Windows.Forms.TextBox();
            this.commentTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.imgFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.labelLogin = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(900, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.labelLogin);
            this.panel2.Controls.Add(this.descTextBox);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.subjectTextBox);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.noTicketTextBox);
            this.panel2.Controls.Add(this.confirmationCheckBox);
            this.panel2.Controls.Add(this.delImg4Button);
            this.panel2.Controls.Add(this.delImg3Button);
            this.panel2.Controls.Add(this.delImg1Button);
            this.panel2.Controls.Add(this.delImg2Button);
            this.panel2.Controls.Add(this.img4Button);
            this.panel2.Controls.Add(this.img3Button);
            this.panel2.Controls.Add(this.img2Button);
            this.panel2.Controls.Add(this.img1Button);
            this.panel2.Controls.Add(this.pictureBox4);
            this.panel2.Controls.Add(this.pictureBox3);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.nonAktifCheckBox);
            this.panel2.Controls.Add(this.checkBoxMTC);
            this.panel2.Controls.Add(this.biayaTextBox);
            this.panel2.Controls.Add(this.commentTextBox);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Location = new System.Drawing.Point(27, 40);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(852, 513);
            this.panel2.TabIndex = 59;
            // 
            // descTextBox
            // 
            this.descTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.descTextBox.Location = new System.Drawing.Point(117, 83);
            this.descTextBox.MaxLength = 30;
            this.descTextBox.Multiline = true;
            this.descTextBox.Name = "descTextBox";
            this.descTextBox.ReadOnly = true;
            this.descTextBox.Size = new System.Drawing.Size(685, 63);
            this.descTextBox.TabIndex = 215;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(39, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 18);
            this.label5.TabIndex = 213;
            this.label5.Text = "Subyek";
            // 
            // subjectTextBox
            // 
            this.subjectTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectTextBox.Location = new System.Drawing.Point(117, 50);
            this.subjectTextBox.MaxLength = 30;
            this.subjectTextBox.Name = "subjectTextBox";
            this.subjectTextBox.ReadOnly = true;
            this.subjectTextBox.Size = new System.Drawing.Size(502, 27);
            this.subjectTextBox.TabIndex = 214;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(39, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 18);
            this.label3.TabIndex = 211;
            this.label3.Text = "Nomor";
            // 
            // noTicketTextBox
            // 
            this.noTicketTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noTicketTextBox.Location = new System.Drawing.Point(117, 14);
            this.noTicketTextBox.MaxLength = 30;
            this.noTicketTextBox.Name = "noTicketTextBox";
            this.noTicketTextBox.ReadOnly = true;
            this.noTicketTextBox.Size = new System.Drawing.Size(300, 27);
            this.noTicketTextBox.TabIndex = 212;
            // 
            // confirmationCheckBox
            // 
            this.confirmationCheckBox.AutoSize = true;
            this.confirmationCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirmationCheckBox.Location = new System.Drawing.Point(571, 271);
            this.confirmationCheckBox.Name = "confirmationCheckBox";
            this.confirmationCheckBox.Size = new System.Drawing.Size(83, 22);
            this.confirmationCheckBox.TabIndex = 210;
            this.confirmationCheckBox.Text = "Disetujui";
            this.confirmationCheckBox.UseVisualStyleBackColor = true;
            this.confirmationCheckBox.Visible = false;
            // 
            // delImg4Button
            // 
            this.delImg4Button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("delImg4Button.BackgroundImage")));
            this.delImg4Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.delImg4Button.Location = new System.Drawing.Point(687, 466);
            this.delImg4Button.Name = "delImg4Button";
            this.delImg4Button.Size = new System.Drawing.Size(29, 30);
            this.delImg4Button.TabIndex = 209;
            this.delImg4Button.UseVisualStyleBackColor = true;
            this.delImg4Button.Click += new System.EventHandler(this.delImg4Button_Click);
            // 
            // delImg3Button
            // 
            this.delImg3Button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("delImg3Button.BackgroundImage")));
            this.delImg3Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.delImg3Button.Location = new System.Drawing.Point(486, 466);
            this.delImg3Button.Name = "delImg3Button";
            this.delImg3Button.Size = new System.Drawing.Size(29, 30);
            this.delImg3Button.TabIndex = 208;
            this.delImg3Button.UseVisualStyleBackColor = true;
            this.delImg3Button.Click += new System.EventHandler(this.delImg3Button_Click);
            // 
            // delImg1Button
            // 
            this.delImg1Button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("delImg1Button.BackgroundImage")));
            this.delImg1Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.delImg1Button.Location = new System.Drawing.Point(95, 466);
            this.delImg1Button.Name = "delImg1Button";
            this.delImg1Button.Size = new System.Drawing.Size(29, 30);
            this.delImg1Button.TabIndex = 207;
            this.delImg1Button.UseVisualStyleBackColor = true;
            this.delImg1Button.Click += new System.EventHandler(this.delImg1Button_Click);
            // 
            // delImg2Button
            // 
            this.delImg2Button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("delImg2Button.BackgroundImage")));
            this.delImg2Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.delImg2Button.Location = new System.Drawing.Point(292, 466);
            this.delImg2Button.Name = "delImg2Button";
            this.delImg2Button.Size = new System.Drawing.Size(29, 30);
            this.delImg2Button.TabIndex = 206;
            this.delImg2Button.UseVisualStyleBackColor = true;
            this.delImg2Button.Click += new System.EventHandler(this.delImg2Button_Click);
            // 
            // img4Button
            // 
            this.img4Button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("img4Button.BackgroundImage")));
            this.img4Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.img4Button.Location = new System.Drawing.Point(652, 466);
            this.img4Button.Name = "img4Button";
            this.img4Button.Size = new System.Drawing.Size(29, 30);
            this.img4Button.TabIndex = 205;
            this.img4Button.UseVisualStyleBackColor = true;
            this.img4Button.Click += new System.EventHandler(this.img4Button_Click);
            // 
            // img3Button
            // 
            this.img3Button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("img3Button.BackgroundImage")));
            this.img3Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.img3Button.Location = new System.Drawing.Point(451, 466);
            this.img3Button.Name = "img3Button";
            this.img3Button.Size = new System.Drawing.Size(29, 30);
            this.img3Button.TabIndex = 204;
            this.img3Button.UseVisualStyleBackColor = true;
            this.img3Button.Click += new System.EventHandler(this.img3Button_Click);
            // 
            // img2Button
            // 
            this.img2Button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("img2Button.BackgroundImage")));
            this.img2Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.img2Button.Location = new System.Drawing.Point(257, 466);
            this.img2Button.Name = "img2Button";
            this.img2Button.Size = new System.Drawing.Size(29, 30);
            this.img2Button.TabIndex = 203;
            this.img2Button.UseVisualStyleBackColor = true;
            this.img2Button.Click += new System.EventHandler(this.img2Button_Click);
            // 
            // img1Button
            // 
            this.img1Button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("img1Button.BackgroundImage")));
            this.img1Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.img1Button.Location = new System.Drawing.Point(60, 466);
            this.img1Button.Name = "img1Button";
            this.img1Button.Size = new System.Drawing.Size(29, 30);
            this.img1Button.TabIndex = 202;
            this.img1Button.UseVisualStyleBackColor = true;
            this.img1Button.Click += new System.EventHandler(this.img1Button_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4.Location = new System.Drawing.Point(652, 310);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(150, 150);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 201;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.DoubleClick += new System.EventHandler(this.pictureBox4_DoubleClick);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Location = new System.Drawing.Point(451, 310);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(150, 150);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 200;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.DoubleClick += new System.EventHandler(this.pictureBox3_DoubleClick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(257, 310);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(150, 150);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 199;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.DoubleClick += new System.EventHandler(this.pictureBox2_DoubleClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(59, 310);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 150);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 198;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            // 
            // nonAktifCheckBox
            // 
            this.nonAktifCheckBox.AutoSize = true;
            this.nonAktifCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nonAktifCheckBox.Location = new System.Drawing.Point(715, 270);
            this.nonAktifCheckBox.Name = "nonAktifCheckBox";
            this.nonAktifCheckBox.Size = new System.Drawing.Size(87, 22);
            this.nonAktifCheckBox.TabIndex = 192;
            this.nonAktifCheckBox.Text = "Non Aktif";
            this.nonAktifCheckBox.UseVisualStyleBackColor = true;
            // 
            // checkBoxMTC
            // 
            this.checkBoxMTC.AutoSize = true;
            this.checkBoxMTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxMTC.Location = new System.Drawing.Point(117, 271);
            this.checkBoxMTC.Name = "checkBoxMTC";
            this.checkBoxMTC.Size = new System.Drawing.Size(156, 22);
            this.checkBoxMTC.TabIndex = 191;
            this.checkBoxMTC.Text = "Biaya Perbaikan Rp";
            this.checkBoxMTC.UseVisualStyleBackColor = true;
            this.checkBoxMTC.CheckedChanged += new System.EventHandler(this.checkBoxMTC_CheckedChanged);
            // 
            // biayaTextBox
            // 
            this.biayaTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.biayaTextBox.Enabled = false;
            this.biayaTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.biayaTextBox.Location = new System.Drawing.Point(274, 269);
            this.biayaTextBox.MaxLength = 20;
            this.biayaTextBox.Name = "biayaTextBox";
            this.biayaTextBox.Size = new System.Drawing.Size(241, 27);
            this.biayaTextBox.TabIndex = 189;
            this.biayaTextBox.Text = "0";
            this.biayaTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // commentTextBox
            // 
            this.commentTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commentTextBox.Location = new System.Drawing.Point(117, 152);
            this.commentTextBox.MaxLength = 3000;
            this.commentTextBox.Multiline = true;
            this.commentTextBox.Name = "commentTextBox";
            this.commentTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.commentTextBox.Size = new System.Drawing.Size(685, 110);
            this.commentTextBox.TabIndex = 187;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(19, 155);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 18);
            this.label7.TabIndex = 186;
            this.label7.Text = "Comment";
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.Color.FloralWhite;
            this.saveButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.ForeColor = System.Drawing.Color.Black;
            this.saveButton.Location = new System.Drawing.Point(360, 572);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(182, 37);
            this.saveButton.TabIndex = 190;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // imgFileDialog
            // 
            this.imgFileDialog.FileName = "openFileDialog1";
            // 
            // labelLogin
            // 
            this.labelLogin.AutoSize = true;
            this.labelLogin.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLogin.ForeColor = System.Drawing.Color.Black;
            this.labelLogin.Location = new System.Drawing.Point(448, 17);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(87, 18);
            this.labelLogin.TabIndex = 216;
            this.labelLogin.Text = "Reply as ";
            // 
            // dataCustCare_detailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(900, 661);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.saveButton);
            this.Name = "dataCustCare_detailForm";
            this.Load += new System.EventHandler(this.dataCustCare_detailForm_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.saveButton, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox nonAktifCheckBox;
        private System.Windows.Forms.CheckBox checkBoxMTC;
        protected System.Windows.Forms.TextBox biayaTextBox;
        protected System.Windows.Forms.TextBox commentTextBox;
        protected System.Windows.Forms.Label label7;
        protected System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.OpenFileDialog imgFileDialog;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button img4Button;
        private System.Windows.Forms.Button img3Button;
        private System.Windows.Forms.Button img2Button;
        private System.Windows.Forms.Button img1Button;
        private System.Windows.Forms.Button delImg1Button;
        private System.Windows.Forms.Button delImg2Button;
        private System.Windows.Forms.Button delImg4Button;
        private System.Windows.Forms.Button delImg3Button;
        private System.Windows.Forms.CheckBox confirmationCheckBox;
        protected System.Windows.Forms.Label label5;
        protected System.Windows.Forms.TextBox subjectTextBox;
        protected System.Windows.Forms.Label label3;
        protected System.Windows.Forms.TextBox noTicketTextBox;
        protected System.Windows.Forms.TextBox descTextBox;
        protected System.Windows.Forms.Label labelLogin;
    }
}
