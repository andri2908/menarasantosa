﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace AlphaSoft
{
    public partial class dataBrowse_TransaksiForm : AlphaSoft.browseRangeTextBoxForm
    {
        private int originModuleID = 0;
        private globalUserUtil gUtil;
        private globalRestAPI gRest;

        private CultureInfo culture = new CultureInfo("id-ID");

        private int userID = 0;
        private int kompleksID = 0;

        List<cbDataSource> cbDT = new List<cbDataSource>();
        List<cbDataSource> cbDTBayar = new List<cbDataSource>();

        public dataBrowse_TransaksiForm(int moduleID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();

            switch (originModuleID)
            {
                case globalConstants.MODULE_PEMBAYARAN_IPL:
                case globalConstants.MODULE_PEMBAYARAN_TIKET:
                case globalConstants.MODULE_CANCEL_PEMBAYARAN:
                case globalConstants.MODULE_CETAK_TRANSAKSI_DRUGS:
                case globalConstants.MODULE_CETAK_TRANSAKSI_RETAIL:
                    labelStatus.Visible = false;
                    statusCombo.Visible = false;
                    break;

                default:
                    loadStatusCombo();
                    break;
            }

            loadStatusBayarCombo();
        }

        protected override void newButtonAction()
        {
            dataTransaksiForm displayForm = new dataTransaksiForm(originModuleID);
            displayForm.ShowDialog(this);
        }

        private void dataBrowse_TransaksiForm_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void loadStatusCombo()
        {
            cbDataSource cbDataSourceElement;
            cbDT.Clear();

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "ALL";
            cbDataSourceElement.valueMember = "ALL";
            cbDT.Add(cbDataSourceElement);

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "Undelivered";
            cbDataSourceElement.valueMember = "undelivered";
            cbDT.Add(cbDataSourceElement);

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "Delivered";
            cbDataSourceElement.valueMember = "delivered";
            cbDT.Add(cbDataSourceElement);

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "Cancel";
            cbDataSourceElement.valueMember = "cancel";
            cbDT.Add(cbDataSourceElement);

            statusCombo.DataSource = cbDT;
            statusCombo.DisplayMember = "displayMember";
            statusCombo.ValueMember = "valueMember";

            statusCombo.SelectedValue = "undelivered";
        }

        private void loadStatusBayarCombo()
        {
            cbDataSource cbDataSourceElement;
            cbDTBayar.Clear();

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "ALL";
            cbDataSourceElement.valueMember = "ALL";
            cbDTBayar.Add(cbDataSourceElement);

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "BELUM BAYAR";
            cbDataSourceElement.valueMember = "0";
            cbDTBayar.Add(cbDataSourceElement);

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "PENDING";
            cbDataSourceElement.valueMember = "1";
            cbDTBayar.Add(cbDataSourceElement);

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "TERBAYAR";
            cbDataSourceElement.valueMember = "2";
            cbDTBayar.Add(cbDataSourceElement);

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "DITOLAK";
            cbDataSourceElement.valueMember = "3";
            cbDTBayar.Add(cbDataSourceElement);

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "EXPIRED";
            cbDataSourceElement.valueMember = "4";
            cbDTBayar.Add(cbDataSourceElement);

            statusBayarCombo.DataSource = cbDTBayar;
            statusBayarCombo.DisplayMember = "displayMember";
            statusBayarCombo.ValueMember = "valueMember";

            statusBayarCombo.SelectedValue = "ALL";
        }

        private void dataBrowse_TransaksiForm_Load(object sender, EventArgs e)
        {
            int accessModuleID = MMConstants.TAMBAH_HAPUS_RETAIL;
            if (originModuleID == globalConstants.MODULE_TRANSAKSI_DRUGS)
            {
                accessModuleID = MMConstants.TAMBAH_HAPUS_DRUGS;
            }

            if (originModuleID == globalConstants.MODULE_CANCEL_PEMBAYARAN)
            {
                nonActiveCheckBox.Visible = false;
            }

            displayAllAction();

            if (!gUtil.RS_userHasAccessTo(accessModuleID,
                MMConstants.HAK_NEW_DATA))
                setNewButtonEnable(false);
        }

        protected override void displayAllAction()
        {
            switch (originModuleID)
            {
                case globalConstants.MODULE_PEMBAYARAN_IPL:
                    displayOutstandingIPL();
                    break;

                case globalConstants.MODULE_PEMBAYARAN_TIKET:
                    displayOutstandingTiket();
                    break;

                case globalConstants.MODULE_CANCEL_PEMBAYARAN:
                    displayPaymentManual();
                    break;

                default:
                    displayDefault();
                    break;
            }
        }

        private void displayOutstandingIPL()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";

            string startDate = String.Format(culture, "{0:yyyyMMdd}", startDTPicker.Value.Date);
            string endDate = String.Format(culture, "{0:yyyyMMdd}", endDTPicker.Value.Date);

            REST_dataTransaksiManual dataTrans = new REST_dataTransaksiManual();

            sqlCommand = "SELECT th.id_trans, DATE_FORMAT(th.start_ipl, '%d %M %Y') AS 'TGL', " +
                                    "ul.user_name, ml.kompleks_name, " +
                                    "CONCAT(IFNULL(mb.blok_name, ''), '-', mk.house_no) AS 'blok', th.nominal, " +
                                    "IF(th.status_id = 0, 'outstanding', 'in progress') AS 'STATUS' " +
                                    "FROM transaksi_ipl th " +
                                    "LEFT OUTER JOIN user_login_data ul ON (th.user_id = ul.user_id) " +
                                    "LEFT OUTER JOIN master_kavling mk ON (th.kavling_id = mk.kavling_id AND mk.is_active = 'Y') " +
                                    "LEFT OUTER JOIN master_kompleks ml ON (mk.kompleks_id = ml.kompleks_id AND ml.is_active = 'Y') " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id AND mb.is_active = 'Y') " +
                                    "WHERE 1 = 1 " +
                                    "AND DATE_FORMAT(th.start_ipl, '%Y%m%d') >= '" + startDate + "' " +
                                    "AND DATE_FORMAT(th.start_ipl, '%Y%m%d') <= '" + endDate + "' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND th.is_active = 'Y' ";

            if (userID > 0)
                sqlCommand += "AND th.user_id = " + userID + " ";

            if (kompleksID > 0)
                sqlCommand += "AND th.kavling_id IN (SELECT kavling_id FROM master_kavling WHERE kompleks_id = " + kompleksID + " AND is_active = 'Y') ";

            sqlCommand += "AND th.type_trans = " + globalConstants.TRANS_TYPE_IPL + " ";
            sqlCommand += "AND th.status_id = 0 ";

            sqlCommand += "ORDER BY kompleks_name ASC";

            if (gRest.getTransDataManual(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref dataTrans))
            {
                if (dataTrans.dataStatus.o_status == 1)
                {
                    dataGridView.DataSource = null;

                    dt.Columns.Add("ID_TRANS");
                    dt.Columns.Add("TGL");
                    dt.Columns.Add("USER");
                    dt.Columns.Add("KOMPLEKS");
                    dt.Columns.Add("BLOK");
                    dt.Columns.Add("TOTAL", typeof(double));
                    dt.Columns.Add("STATUS");

                    for (int i = 0; i < dataTrans.dataList.Count; i++)
                    {
                        DataRow r = dt.Rows.Add();
                        r["ID_TRANS"] = dataTrans.dataList[i].id_trans;
                        r["TGL"] = dataTrans.dataList[i].tgl;
                        r["USER"] = dataTrans.dataList[i].user_name;
                        r["KOMPLEKS"] = dataTrans.dataList[i].kompleks_name;
                        r["BLOK"] = dataTrans.dataList[i].blok;
                        r["TOTAL"] = dataTrans.dataList[i].nominal;
                        r["STATUS"] = dataTrans.dataList[i].status;
                    }

                    dataGridView.DataSource = dt;
                    dataGridView.Columns["ID_TRANS"].Visible = false;

                    dataGridView.Columns["TOTAL"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridView.Columns["TOTAL"].DefaultCellStyle.Format = "N0";
                    dataGridView.Columns["TOTAL"].DefaultCellStyle.FormatProvider = new CultureInfo("id-ID");
                }
            }
        }

        private void displayOutstandingTiket()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";

            string startDate = String.Format(culture, "{0:yyyyMMdd}", startDTPicker.Value.Date);
            string endDate = String.Format(culture, "{0:yyyyMMdd}", endDTPicker.Value.Date);

            REST_dataTransaksiManual dataTrans = new REST_dataTransaksiManual();

            sqlCommand = "SELECT th.id_trans, DATE_FORMAT(th.date_issued, '%d %M %Y') AS 'TGL', " +
                                    "ul.user_name, ml.kompleks_name, " +
                                    "CONCAT(IFNULL(mb.blok_name, ''), '-', mk.house_no) AS 'blok', th.nominal, " +
                                    "IF(th.status_id = 0, 'outstanding', 'in progress') AS 'STATUS' " +
                                    "FROM transaksi_handyman th " +
                                    "LEFT OUTER JOIN user_login_data ul ON (th.user_id = ul.user_id) " +
                                    "LEFT OUTER JOIN master_kavling mk ON (th.kavling_id = mk.kavling_id AND mk.is_active = 'Y') " +
                                    "LEFT OUTER JOIN master_kompleks ml ON (mk.kompleks_id = ml.kompleks_id AND ml.is_active = 'Y') " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id AND mb.is_active = 'Y') " +
                                    "WHERE 1 = 1 " +
                                    "AND DATE_FORMAT(th.date_issued, '%Y%m%d') >= '" + startDate + "' " +
                                    "AND DATE_FORMAT(th.date_issued, '%Y%m%d') <= '" + endDate + "' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND th.is_active = 'Y' ";

            if (userID > 0)
                sqlCommand += "AND th.user_id = " + userID + " ";

            if (kompleksID > 0)
                sqlCommand += "AND th.kavling_id IN (SELECT kavling_id FROM master_kavling WHERE kompleks_id = " + kompleksID + " AND is_active = 'Y') ";

            sqlCommand += "AND th.type_trans = " + globalConstants.TRANS_TYPE_HANDYMAN + " ";
            sqlCommand += "AND th.status_id = 0 ";

            sqlCommand += "ORDER BY kompleks_name ASC";

            if (gRest.getTransDataManual(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref dataTrans))
            {
                if (dataTrans.dataStatus.o_status == 1)
                {
                    dataGridView.DataSource = null;

                    dt.Columns.Add("ID_TRANS");
                    dt.Columns.Add("TGL");
                    dt.Columns.Add("USER");
                    dt.Columns.Add("KOMPLEKS");
                    dt.Columns.Add("BLOK");
                    dt.Columns.Add("TOTAL", typeof(double));
                    dt.Columns.Add("STATUS");

                    for (int i = 0; i < dataTrans.dataList.Count; i++)
                    {
                        DataRow r = dt.Rows.Add();
                        r["ID_TRANS"] = dataTrans.dataList[i].id_trans;
                        r["TGL"] = dataTrans.dataList[i].tgl;
                        r["USER"] = dataTrans.dataList[i].user_name;
                        r["KOMPLEKS"] = dataTrans.dataList[i].kompleks_name;
                        r["BLOK"] = dataTrans.dataList[i].blok;
                        r["TOTAL"] = dataTrans.dataList[i].nominal;
                        r["STATUS"] = dataTrans.dataList[i].status;
                    }

                    dataGridView.DataSource = dt;
                    dataGridView.Columns["ID_TRANS"].Visible = false;

                    dataGridView.Columns["TOTAL"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridView.Columns["TOTAL"].DefaultCellStyle.Format = "N0";
                    dataGridView.Columns["TOTAL"].DefaultCellStyle.FormatProvider = new CultureInfo("id-ID");
                }
            }
        }

        private void displayPaymentManual()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";

            string sqlIPL = "";
            string sqlTiket = "";

            string startDate = String.Format(culture, "{0:yyyyMMdd}", startDTPicker.Value.Date);
            string endDate = String.Format(culture, "{0:yyyyMMdd}", endDTPicker.Value.Date);

            REST_dataTransaksiManual dataTrans = new REST_dataTransaksiManual();

            sqlIPL = "SELECT h.id_trans, h.trans_type, DATE_FORMAT(h.date_paid, '%d %M %Y') AS 'TGL', " +
                                    "ul.user_name, ml.kompleks_name, " +
                                    "CONCAT(IFNULL(mb.blok_name, ''), '-', mk.house_no) AS 'blok', h.total as nominal, h.payment_remark, th.kavling_id, th.user_id " +
                                    "FROM transaksi_payment_manual h " +
                                    "LEFT OUTER JOIN transaksi_ipl th ON (h.id_trans = th.id_trans) " +
                                    "LEFT OUTER JOIN user_login_data ul ON (th.user_id = ul.user_id) " +
                                    "LEFT OUTER JOIN master_kavling mk ON (th.kavling_id = mk.kavling_id AND mk.is_active = 'Y') " +
                                    "LEFT OUTER JOIN master_kompleks ml ON (mk.kompleks_id = ml.kompleks_id AND ml.is_active = 'Y') " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id AND mb.is_active = 'Y') " +
                                    "WHERE 1 = 1 " +
                                    "AND DATE_FORMAT(h.date_paid, '%Y%m%d') >= '" + startDate + "' " +
                                    "AND DATE_FORMAT(h.date_paid, '%Y%m%d') <= '" + endDate + "' " +
                                    "AND h.is_active = 'Y' " +
                                    "AND h.trans_type = 'IPL' ";

            sqlTiket = "SELECT h.id_trans, h.trans_type, DATE_FORMAT(h.date_paid, '%d %M %Y') AS 'TGL', " +
                                    "ul.user_name, ml.kompleks_name, " +
                                    "CONCAT(IFNULL(mb.blok_name, ''), '-', mk.house_no) AS 'blok', h.total as nominal, h.payment_remark, th.kavling_id, th.user_id " +
                                    "FROM transaksi_payment_manual h " +
                                    "LEFT OUTER JOIN transaksi_handyman th ON (h.id_trans = th.id_trans) " +
                                    "LEFT OUTER JOIN user_login_data ul ON (th.user_id = ul.user_id) " +
                                    "LEFT OUTER JOIN master_kavling mk ON (th.kavling_id = mk.kavling_id AND mk.is_active = 'Y') " +
                                    "LEFT OUTER JOIN master_kompleks ml ON (mk.kompleks_id = ml.kompleks_id AND ml.is_active = 'Y') " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id AND mb.is_active = 'Y') " +
                                    "WHERE 1 = 1 " +
                                    "AND DATE_FORMAT(h.date_paid, '%Y%m%d') >= '" + startDate + "' " +
                                    "AND DATE_FORMAT(h.date_paid, '%Y%m%d') <= '" + endDate + "' " +
                                    "AND h.is_active = 'Y' " +
                                    "AND h.trans_type = 'TIKET' ";

            sqlCommand = "SELECT * FROM (" + sqlIPL + " UNION ALL " + sqlTiket + ") th " +
                                    "WHERE 1 = 1 ";

            if (userID > 0)
                sqlCommand += "AND th.user_id = " + userID + " ";

            if (kompleksID > 0)
                sqlCommand += "AND th.kavling_id IN (SELECT kavling_id FROM master_kavling WHERE kompleks_id = " + kompleksID + " AND is_active = 'Y') ";

            sqlCommand += "ORDER BY kompleks_name ASC";

            if (gRest.getTransDataManual(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref dataTrans))
            {
                if (dataTrans.dataStatus.o_status == 1)
                {
                    dataGridView.DataSource = null;

                    dt.Columns.Add("ID_TRANS");
                    dt.Columns.Add("TIPE");
                    dt.Columns.Add("TGL");
                    dt.Columns.Add("USER");
                    dt.Columns.Add("KOMPLEKS");
                    dt.Columns.Add("BLOK");
                    dt.Columns.Add("TOTAL", typeof(double));
                    dt.Columns.Add("KETERANGAN");

                    for (int i = 0; i < dataTrans.dataList.Count; i++)
                    {
                        DataRow r = dt.Rows.Add();
                        r["ID_TRANS"] = dataTrans.dataList[i].id_trans;
                        r["TIPE"] = dataTrans.dataList[i].trans_type;
                        r["TGL"] = dataTrans.dataList[i].tgl;
                        r["USER"] = dataTrans.dataList[i].user_name;
                        r["KOMPLEKS"] = dataTrans.dataList[i].kompleks_name;
                        r["BLOK"] = dataTrans.dataList[i].blok;
                        r["TOTAL"] = dataTrans.dataList[i].nominal;
                        r["KETERANGAN"] = dataTrans.dataList[i].remark;
                    }

                    dataGridView.DataSource = dt;
                    dataGridView.Columns["ID_TRANS"].Visible = false;

                    dataGridView.Columns["TOTAL"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridView.Columns["TOTAL"].DefaultCellStyle.Format = "N0";
                    dataGridView.Columns["TOTAL"].DefaultCellStyle.FormatProvider = new CultureInfo("id-ID");
                }
            }
        }

        private string getStatusBayar(int statusID)
        {
            switch (statusID)
            {
                case 0: return "BELUM BAYAR";
                case 1: return "PENDING";
                case 2: return "TERBAYAR"; 
                case 3: return "DITOLAK"; 
                default: return "EXPIRED";
            }
        }

        private void displayDefault()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";

            string startDate = String.Format(culture, "{0:yyyyMMdd}", startDTPicker.Value.Date);
            string endDate = String.Format(culture, "{0:yyyyMMdd}", endDTPicker.Value.Date);

            REST_dataTransaksi dataTrans = new REST_dataTransaksi();

            sqlCommand = "SELECT th.id_trans, th.status_id, th.deliver_status, DATE_FORMAT(th.date_issued, '%d %M %Y') AS 'TGL', " +
                                    "ul.user_name, ml.kompleks_name, " +
                                    "CONCAT(IFNULL(mb.blok_name, ''), '-', mk.house_no) AS 'blok', th.nominal " +
                                    "FROM transaksi_retail th " +
                                    "LEFT OUTER JOIN user_login_data ul ON (th.user_id = ul.user_id) " +
                                    "LEFT OUTER JOIN master_kavling mk ON (th.kavling_id = mk.kavling_id AND mk.is_active = 'Y') " +
                                    "LEFT OUTER JOIN master_kompleks ml ON (mk.kompleks_id = ml.kompleks_id AND ml.is_active = 'Y') " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id AND mb.is_active = 'Y') " +
                                    "WHERE 1 = 1 " +
                                    "AND DATE_FORMAT(th.date_issued, '%Y%m%d') >= '" + startDate + "' " +
                                    "AND DATE_FORMAT(th.date_issued, '%Y%m%d') <= '" + endDate + "' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND th.is_active = 'Y' ";

            if (userID > 0)
                sqlCommand += "AND th.user_id = " + userID + " ";

            if (kompleksID > 0)
                sqlCommand += "AND th.kavling_id IN (SELECT kavling_id FROM master_kavling WHERE kompleks_id = " + kompleksID + " AND is_active = 'Y') ";

            switch (originModuleID)
            {
                case globalConstants.MODULE_TRANSAKSI_RETAIL:
                case globalConstants.MODULE_CETAK_TRANSAKSI_RETAIL:
                    sqlCommand += "AND th.type_trans = " + globalConstants.TRANS_TYPE_RETAIL + " ";
                    break;

                case globalConstants.MODULE_TRANSAKSI_DRUGS:
                case globalConstants.MODULE_CETAK_TRANSAKSI_DRUGS:
                    sqlCommand += "AND th.type_trans = " + globalConstants.TRANS_TYPE_RETAIL_DRUGS + " ";
                    break;
            }

            if (originModuleID == globalConstants.MODULE_CETAK_TRANSAKSI_DRUGS ||
                originModuleID == globalConstants.MODULE_CETAK_TRANSAKSI_RETAIL)
            {
                sqlCommand += "AND th.status_id = 2 " +
                                        "AND th.deliver_status = 'undelivered' ";
            }

            if (statusCombo.Visible 
                && statusCombo.SelectedValue.ToString() != "ALL")
            {
                sqlCommand += "AND th.deliver_status = '" + statusCombo.SelectedValue.ToString() + "' ";
            }

            if (statusBayarCombo.SelectedValue.ToString() != "ALL")
            {
                sqlCommand += "AND th.status_id = '" + statusBayarCombo.SelectedValue.ToString() + "' ";
            }

            sqlCommand += "ORDER BY kompleks_name ASC";

            if (gRest.getTransData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref dataTrans))
            {
                if (dataTrans.dataStatus.o_status == 1)
                {
                    dataGridView.DataSource = null;

                    dt.Columns.Add("ID_TRANS");
                    dt.Columns.Add("STATUS BAYAR");
                    dt.Columns.Add("STATUS");
                    dt.Columns.Add("TGL");
                    dt.Columns.Add("USER");
                    dt.Columns.Add("KOMPLEKS");
                    dt.Columns.Add("BLOK");
                    dt.Columns.Add("TOTAL", typeof(double));

                    for (int i = 0; i < dataTrans.dataList.Count; i++)
                    {
                        DataRow r = dt.Rows.Add();
                        r["ID_TRANS"] = dataTrans.dataList[i].id_trans;
                        r["STATUS BAYAR"] = getStatusBayar(dataTrans.dataList[i].status_id);
                        r["STATUS"] = dataTrans.dataList[i].deliver_status.ToUpper();
                        r["TGL"] = dataTrans.dataList[i].tgl;
                        r["USER"] = dataTrans.dataList[i].user_name;
                        r["KOMPLEKS"] = dataTrans.dataList[i].kompleks_name;
                        r["BLOK"] = dataTrans.dataList[i].blok;
                        r["TOTAL"] = dataTrans.dataList[i].nominal;
                    }

                    dataGridView.DataSource = dt;
                    dataGridView.Columns["ID_TRANS"].Visible = false;

                    dataGridView.Columns["TOTAL"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridView.Columns["TOTAL"].DefaultCellStyle.Format = "N0";
                    dataGridView.Columns["TOTAL"].DefaultCellStyle.FormatProvider = new CultureInfo("id-ID");
                }
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            dataBrowse_UserForm displayForm = new dataBrowse_UserForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                userID = Convert.ToInt32(displayForm.ReturnValue1);
                namaTextBox.Text = displayForm.ReturnValue2;
            }
        }

        private void searchKompleks_Click(object sender, EventArgs e)
        {
            dataBrowse_KompleksForm displayForm = new dataBrowse_KompleksForm(globalConstants.MODULE_TRANSAKSI_RETAIL, userID);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                kompleksID = Convert.ToInt32(displayForm.ReturnValue1);
                kompleksTextBox.Text = displayForm.ReturnValue2;
            }
        }

        private void printTransaksi(int idTrans)
        {
            List<sqlFields> listSql = new List<sqlFields>();
            sqlFields sqlElement;

            sqlElement = new sqlFields();
            sqlElement.fieldName = "ID_TRANS";
            sqlElement.fieldValue = idTrans.ToString();
            listSql.Add(sqlElement);

            RPT_containerForm displayReportForm = new RPT_containerForm(null, globalReportConstants.PRINTOUT_TRANSAKSI_RETAIL, "", listSql);
            displayReportForm.ShowDialog(this);
        }

        protected override void dbGridDoubleClickAction(DataGridViewRow selectedRow)
        {
            int selectedID = Convert.ToInt32(selectedRow.Cells["ID_TRANS"].Value);
            string blokName = selectedRow.Cells["BLOK"].Value.ToString();
            string kompleksName = selectedRow.Cells["KOMPLEKS"].Value.ToString();
            string status = "outstanding";
            string paymentTipe = "";

            switch (originModuleID)
            {
                case globalConstants.MODULE_PEMBAYARAN_IPL:
                case globalConstants.MODULE_PEMBAYARAN_TIKET:
                    status = selectedRow.Cells["STATUS"].Value.ToString();

                    if (status == "outstanding")
                    {
                        dataPaymentManualForm displayPayment = new dataPaymentManualForm(originModuleID, selectedID, kompleksName, blokName, paymentTipe);
                        displayPayment.ShowDialog(this);
                    }
                    else
                    {
                        MessageBox.Show("Proses Pembayaran MidTrans sedang berlangsung");
                    }
                    break;

                case globalConstants.MODULE_CANCEL_PEMBAYARAN:
                    paymentTipe = selectedRow.Cells["TIPE"].Value.ToString();

                    dataPaymentManualForm displayCancel = new dataPaymentManualForm(originModuleID, selectedID, kompleksName, blokName, paymentTipe);
                    displayCancel.ShowDialog(this);
                    break;

                case globalConstants.MODULE_DEFAULT:
                    ReturnValue1 = selectedRow.Cells["ID_TRANS"].Value.ToString();

                    this.Close();
                    break;

                case globalConstants.MODULE_CETAK_TRANSAKSI_RETAIL:
                case globalConstants.MODULE_CETAK_TRANSAKSI_DRUGS:
                    printTransaksi(selectedID);
                    break;

                default:
                    dataTransaksiForm editForm = new dataTransaksiForm(originModuleID, selectedID);
                    editForm.ShowDialog(this);
                    break;
            }
        }

    }
}
