﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Globalization;

namespace AlphaSoft
{
    public partial class dataPaymentManualForm : AlphaSoft.basicHotkeysForm
    {
        private int idTrans = 0;
        private int originModuleID = 0;
        private string transType = "";
        private double totalValue = 0;

        private CultureInfo culture = new CultureInfo("id-ID");

        private globalRestAPI gRest;
        private globalUserUtil gUser;

        List<cbDataSource> cbDT = new List<cbDataSource>();

        public dataPaymentManualForm(int moduleID, int idTransParam, string kompleksNameParam, string blokNameParam, string transTypeParam = "IPL")
        {
            InitializeComponent();

            originModuleID = moduleID;
            idTrans = idTransParam;

            kompleksTextBox.Text = kompleksNameParam;
            namaBlokTextBox.Text = blokNameParam;
            transType = transTypeParam;

            gRest = new globalRestAPI();
            gUser = new globalUserUtil();
        }

        private void loadDataIPL()
        {
            string sqlCommand;
            REST_dataIPL iplData = new REST_dataIPL();

            sqlCommand = "SELECT ih.id_trans, ih.date_issued, ih.start_ipl, ih.end_ipl, ih.status_id, ih.nominal, " +
                                    "IFNULL(ih.date_paid, ih.date_issued) AS date_paid, " +
                                    "IFNULL(mu.user_name, '') AS user_name FROM " +
                                    "transaksi_ipl ih " +
                                    "LEFT OUTER JOIN user_login_data mu ON (ih.user_id = mu.user_id) " +
                                    "WHERE ih.id_trans = " + idTrans;

            if (gRest.getIPLData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref iplData))
            {
                if (iplData.dataStatus.o_status == 1)
                {
                    if (iplData.dataList.Count > 0)
                    {
                        labelTitle.Text = "Transaksi IPL " + string.Format(culture, "{0:dd MMM yyyy}", iplData.dataList[0].start_ipl) + " s/d " + string.Format(culture, "{0:dd MMM yyyy}", iplData.dataList[0].end_ipl);
                        userTextBox.Text = iplData.dataList[0].user_name;
                        totalLabelValue.Text = iplData.dataList[0].nominal.ToString("C0", culture);
                        totalValue = iplData.dataList[0].nominal;
                    }
                }
            }
        }

        private void loadDataTiket()
        {
            string sqlCommand;
            REST_dataIPL iplData = new REST_dataIPL();

            sqlCommand = "SELECT ih.id_trans, ih.date_issued, ih.status_id, ih.nominal, " +
                                    "IFNULL(ih.date_paid, ih.date_issued) AS date_paid, " +
                                    "IFNULL(mu.user_name, '') AS user_name FROM " +
                                    "transaksi_handyman ih " +
                                    "LEFT OUTER JOIN user_login_data mu ON (ih.user_id = mu.user_id) " +
                                    "WHERE ih.id_trans = " + idTrans;

            if (gRest.getIPLData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref iplData))
            {
                if (iplData.dataStatus.o_status == 1)
                {
                    if (iplData.dataList.Count > 0)
                    {
                        labelTitle.Text = "Transaksi Handyman";
                        userTextBox.Text = iplData.dataList[0].user_name;
                        totalLabelValue.Text = iplData.dataList[0].nominal.ToString("C0", culture);
                        totalValue = iplData.dataList[0].nominal;
                    }
                }
            }
        }

        private void loadPaymentTypeCombo()
        {
            cbDataSource cbDataSourceElement;
            cbDT.Clear();

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "Tunai";
            cbDataSourceElement.valueMember = "1";
            cbDT.Add(cbDataSourceElement);

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "Transfer Bank";
            cbDataSourceElement.valueMember = "2";
            cbDT.Add(cbDataSourceElement);

            statusCombo.DataSource = cbDT;
            statusCombo.DisplayMember = "displayMember";
            statusCombo.ValueMember = "valueMember";

            statusCombo.SelectedValue = "1";
        }

        private void loadDataPayment()
        {
            string sqlCommand;
            REST_dataIPL iplData = new REST_dataIPL();

            if (transType == "IPL")
            {
                sqlCommand = "SELECT ih.id_trans, ih.date_issued, ih.start_ipl, ih.end_ipl, ih.status_id, ih.nominal, " +
                                     "IFNULL(ih.date_paid, ih.date_issued) AS date_paid, " +
                                     "IFNULL(mu.user_name, '') AS user_name FROM " +
                                     "transaksi_ipl ih " +
                                     "LEFT OUTER JOIN user_login_data mu ON (ih.user_id = mu.user_id) " +
                                     "WHERE ih.id_trans = " + idTrans;
            }
            else
            {
                sqlCommand = "SELECT ih.id_trans, ih.date_issued, ih.status_id, ih.nominal, " +
                                 "IFNULL(ih.date_paid, ih.date_issued) AS date_paid, " +
                                 "IFNULL(mu.user_name, '') AS user_name FROM " +
                                 "transaksi_handyman ih " +
                                 "LEFT OUTER JOIN user_login_data mu ON (ih.user_id = mu.user_id) " +
                                 "WHERE ih.id_trans = " + idTrans;
            }

            if (gRest.getIPLData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref iplData))
            {
                if (iplData.dataStatus.o_status == 1)
                {
                    if (iplData.dataList.Count > 0)
                    {
                        if (transType == "IPL")
                        {
                            labelTitle.Text = "Transaksi IPL " + string.Format(culture, "{0:dd MMM yyyy}", iplData.dataList[0].start_ipl) + " s/d " + string.Format(culture, "{0:dd MMM yyyy}", iplData.dataList[0].end_ipl);
                        }
                        else
                        {
                            labelTitle.Text = "Transaksi Handyman";
                        }

                        userTextBox.Text = iplData.dataList[0].user_name;
                        totalLabelValue.Text = iplData.dataList[0].nominal.ToString("C0", culture);
                        totalValue = iplData.dataList[0].nominal;
                    }
                }
            }

            gUser.disableAllControls(this);

            SaveButton.Text = "CANCEL";
            SaveButton.Enabled = true;
        }

        private void dataPaymentManualForm_Load(object sender, EventArgs e)
        {
            loadPaymentTypeCombo();

            switch (originModuleID)
            {
                case globalConstants.MODULE_PEMBAYARAN_IPL:
                    loadDataIPL();
                    break;

                case globalConstants.MODULE_PEMBAYARAN_TIKET:
                    loadDataTiket();
                    break;

                case globalConstants.MODULE_CANCEL_PEMBAYARAN:
                    loadDataPayment();
                    break;
            }
        }

        private bool saveData()
        {
            bool result = false;
            string errMsg = "";

            List<payment_manual> listHeader = new List<payment_manual>();
            payment_manual headerData;

            try
            {
                if (originModuleID == globalConstants.MODULE_PEMBAYARAN_IPL)
                    transType = "IPL";
                else
                    transType = "TIKET";

                int newID = 0;

                headerData = new payment_manual();
                headerData.date_paid = new DateTime(dateDTPicker.Value.Year, dateDTPicker.Value.Month, dateDTPicker.Value.Day);
                headerData.id_trans = idTrans;
                headerData.payment_remark = descTextBox.Text;
                headerData.payment_type = Convert.ToInt32(statusCombo.SelectedValue);
                headerData.total = totalValue;
                headerData.trans_type = transType;

                listHeader.Add(headerData);

                if (!gRest.savePaymentManual(gUser.getUserID(), gUser.getUserToken(), listHeader, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private bool cancelTrans()
        {
            bool result = false;
            string errMsg = "";

            List<payment_manual> listHeader = new List<payment_manual>();

            try
            {
                int newID = 0;

                if (!gRest.cancelPaymentManual(gUser.getUserID(), gUser.getUserToken(), idTrans, transType, 
                    out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            errorLabel.Text = "";

            if (originModuleID == globalConstants.MODULE_CANCEL_PEMBAYARAN)
            {
                if (DialogResult.Yes == MessageBox.Show("Cancel Pembayaran ?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    if (cancelTrans())
                    {
                        MessageBox.Show("SUCCESS");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("FAIL");
                    }
                }
            }
            else
            {
                if (DialogResult.Yes == MessageBox.Show("Save Pembayaran ?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    if (saveData())
                    {
                        MessageBox.Show("SUCCESS");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("FAIL");
                    }
                }
            }
        }
    }
}
