﻿namespace AlphaSoft
{
    partial class dataCustomerCareForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.statusLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ticketGridView = new System.Windows.Forms.DataGridView();
            this.historyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendDateValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mtcCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isMTC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isActive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.subjectTextBox = new System.Windows.Forms.TextBox();
            this.namaBlokTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.kompleksTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.userTextBox = new System.Windows.Forms.TextBox();
            this.dateDTPicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.tutupButton = new System.Windows.Forms.Button();
            this.newButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ticketGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(1031, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.statusLabel);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.ticketGridView);
            this.panel2.Controls.Add(this.descriptionTextBox);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.subjectTextBox);
            this.panel2.Controls.Add(this.namaBlokTextBox);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.kompleksTextBox);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.userTextBox);
            this.panel2.Controls.Add(this.dateDTPicker);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(22, 35);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(987, 462);
            this.panel2.TabIndex = 22;
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusLabel.ForeColor = System.Drawing.Color.Black;
            this.statusLabel.Location = new System.Drawing.Point(110, 151);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(64, 18);
            this.statusLabel.TabIndex = 180;
            this.statusLabel.Text = "Status";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(40, 151);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 18);
            this.label7.TabIndex = 179;
            this.label7.Text = "Status";
            // 
            // ticketGridView
            // 
            this.ticketGridView.AllowUserToAddRows = false;
            this.ticketGridView.AllowUserToDeleteRows = false;
            this.ticketGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ticketGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.ticketGridView.BackgroundColor = System.Drawing.Color.FloralWhite;
            this.ticketGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ticketGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.ticketGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ticketGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.historyID,
            this.userID,
            this.sendDateValue,
            this.userName,
            this.sendDate,
            this.description,
            this.mtcCost,
            this.isMTC,
            this.isActive});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ticketGridView.DefaultCellStyle = dataGridViewCellStyle3;
            this.ticketGridView.Location = new System.Drawing.Point(15, 181);
            this.ticketGridView.MultiSelect = false;
            this.ticketGridView.Name = "ticketGridView";
            this.ticketGridView.ReadOnly = true;
            this.ticketGridView.RowHeadersVisible = false;
            this.ticketGridView.Size = new System.Drawing.Size(953, 262);
            this.ticketGridView.TabIndex = 178;
            this.ticketGridView.DoubleClick += new System.EventHandler(this.ticketGridView_DoubleClick);
            // 
            // historyID
            // 
            this.historyID.HeaderText = "historyID";
            this.historyID.Name = "historyID";
            this.historyID.ReadOnly = true;
            this.historyID.Visible = false;
            this.historyID.Width = 89;
            // 
            // userID
            // 
            this.userID.HeaderText = "userID";
            this.userID.Name = "userID";
            this.userID.ReadOnly = true;
            this.userID.Visible = false;
            this.userID.Width = 68;
            // 
            // sendDateValue
            // 
            this.sendDateValue.HeaderText = "sendDateValue";
            this.sendDateValue.Name = "sendDateValue";
            this.sendDateValue.ReadOnly = true;
            this.sendDateValue.Visible = false;
            this.sendDateValue.Width = 137;
            // 
            // userName
            // 
            this.userName.HeaderText = "User";
            this.userName.Name = "userName";
            this.userName.ReadOnly = true;
            this.userName.Width = 70;
            // 
            // sendDate
            // 
            this.sendDate.HeaderText = "Tgl";
            this.sendDate.Name = "sendDate";
            this.sendDate.ReadOnly = true;
            this.sendDate.Width = 56;
            // 
            // description
            // 
            this.description.HeaderText = "Comment";
            this.description.Name = "description";
            this.description.ReadOnly = true;
            this.description.Width = 111;
            // 
            // mtcCost
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.mtcCost.DefaultCellStyle = dataGridViewCellStyle2;
            this.mtcCost.HeaderText = "Biaya";
            this.mtcCost.Name = "mtcCost";
            this.mtcCost.ReadOnly = true;
            this.mtcCost.Width = 78;
            // 
            // isMTC
            // 
            this.isMTC.HeaderText = "isMTC";
            this.isMTC.Name = "isMTC";
            this.isMTC.ReadOnly = true;
            this.isMTC.Visible = false;
            this.isMTC.Width = 81;
            // 
            // isActive
            // 
            this.isActive.HeaderText = "isActive";
            this.isActive.Name = "isActive";
            this.isActive.ReadOnly = true;
            this.isActive.Visible = false;
            this.isActive.Width = 97;
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.descriptionTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.descriptionTextBox.Location = new System.Drawing.Point(534, 49);
            this.descriptionTextBox.MaxLength = 30;
            this.descriptionTextBox.Multiline = true;
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.ReadOnly = true;
            this.descriptionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.descriptionTextBox.Size = new System.Drawing.Size(434, 126);
            this.descriptionTextBox.TabIndex = 177;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(441, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 18);
            this.label6.TabIndex = 176;
            this.label6.Text = "Deskripsi";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(454, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 18);
            this.label5.TabIndex = 174;
            this.label5.Text = "Subject";
            // 
            // subjectTextBox
            // 
            this.subjectTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.subjectTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectTextBox.Location = new System.Drawing.Point(534, 14);
            this.subjectTextBox.MaxLength = 30;
            this.subjectTextBox.Name = "subjectTextBox";
            this.subjectTextBox.ReadOnly = true;
            this.subjectTextBox.Size = new System.Drawing.Size(434, 27);
            this.subjectTextBox.TabIndex = 175;
            // 
            // namaBlokTextBox
            // 
            this.namaBlokTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.namaBlokTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.namaBlokTextBox.Location = new System.Drawing.Point(111, 82);
            this.namaBlokTextBox.MaxLength = 5;
            this.namaBlokTextBox.Name = "namaBlokTextBox";
            this.namaBlokTextBox.ReadOnly = true;
            this.namaBlokTextBox.Size = new System.Drawing.Size(206, 27);
            this.namaBlokTextBox.TabIndex = 73;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(59, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 18);
            this.label4.TabIndex = 72;
            this.label4.Text = "Blok";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(12, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 18);
            this.label3.TabIndex = 69;
            this.label3.Text = "Kompleks";
            // 
            // kompleksTextBox
            // 
            this.kompleksTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.kompleksTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kompleksTextBox.Location = new System.Drawing.Point(111, 49);
            this.kompleksTextBox.MaxLength = 30;
            this.kompleksTextBox.Name = "kompleksTextBox";
            this.kompleksTextBox.ReadOnly = true;
            this.kompleksTextBox.Size = new System.Drawing.Size(300, 27);
            this.kompleksTextBox.TabIndex = 70;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(55, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 18);
            this.label2.TabIndex = 66;
            this.label2.Text = "User";
            // 
            // userTextBox
            // 
            this.userTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.userTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userTextBox.Location = new System.Drawing.Point(111, 115);
            this.userTextBox.MaxLength = 30;
            this.userTextBox.Name = "userTextBox";
            this.userTextBox.ReadOnly = true;
            this.userTextBox.Size = new System.Drawing.Size(300, 27);
            this.userTextBox.TabIndex = 67;
            // 
            // dateDTPicker
            // 
            this.dateDTPicker.CustomFormat = "dd MMM yyyy";
            this.dateDTPicker.Enabled = false;
            this.dateDTPicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateDTPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateDTPicker.Location = new System.Drawing.Point(111, 17);
            this.dateDTPicker.Name = "dateDTPicker";
            this.dateDTPicker.Size = new System.Drawing.Size(136, 26);
            this.dateDTPicker.TabIndex = 65;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(20, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 18);
            this.label1.TabIndex = 64;
            this.label1.Text = "Tgl Tiket";
            // 
            // tutupButton
            // 
            this.tutupButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tutupButton.BackColor = System.Drawing.Color.FloralWhite;
            this.tutupButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tutupButton.ForeColor = System.Drawing.Color.Black;
            this.tutupButton.Location = new System.Drawing.Point(538, 508);
            this.tutupButton.Name = "tutupButton";
            this.tutupButton.Size = new System.Drawing.Size(182, 37);
            this.tutupButton.TabIndex = 184;
            this.tutupButton.Text = "Close Tiket";
            this.tutupButton.UseVisualStyleBackColor = false;
            this.tutupButton.Click += new System.EventHandler(this.tutupButton_Click);
            // 
            // newButton
            // 
            this.newButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.newButton.BackColor = System.Drawing.Color.FloralWhite;
            this.newButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newButton.ForeColor = System.Drawing.Color.Black;
            this.newButton.Location = new System.Drawing.Point(310, 508);
            this.newButton.Name = "newButton";
            this.newButton.Size = new System.Drawing.Size(182, 37);
            this.newButton.TabIndex = 185;
            this.newButton.Text = "New Comment";
            this.newButton.UseVisualStyleBackColor = false;
            this.newButton.Click += new System.EventHandler(this.newButton_Click);
            // 
            // dataCustomerCareForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1031, 581);
            this.Controls.Add(this.tutupButton);
            this.Controls.Add(this.newButton);
            this.Controls.Add(this.panel2);
            this.Name = "dataCustomerCareForm";
            this.Load += new System.EventHandler(this.dataCustomerCareForm_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.newButton, 0);
            this.Controls.SetChildIndex(this.tutupButton, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ticketGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        protected System.Windows.Forms.TextBox descriptionTextBox;
        protected System.Windows.Forms.Label label6;
        protected System.Windows.Forms.Label label5;
        protected System.Windows.Forms.TextBox subjectTextBox;
        protected System.Windows.Forms.TextBox namaBlokTextBox;
        protected System.Windows.Forms.Label label4;
        protected System.Windows.Forms.Label label3;
        protected System.Windows.Forms.TextBox kompleksTextBox;
        protected System.Windows.Forms.Label label2;
        protected System.Windows.Forms.TextBox userTextBox;
        protected System.Windows.Forms.DateTimePicker dateDTPicker;
        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.DataGridView ticketGridView;
        protected System.Windows.Forms.Button tutupButton;
        protected System.Windows.Forms.Button newButton;
        protected System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewTextBoxColumn historyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn userID;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendDateValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn userName;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn description;
        private System.Windows.Forms.DataGridViewTextBoxColumn mtcCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn isMTC;
        private System.Windows.Forms.DataGridViewTextBoxColumn isActive;
        protected System.Windows.Forms.Label statusLabel;
    }
}
