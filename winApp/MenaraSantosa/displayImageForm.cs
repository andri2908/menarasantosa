﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ZXing;

namespace AlphaSoft
{
    public partial class displayImageForm : AlphaSoft.basicHotkeysForm
    {
        private globalImageLib gImg;
        string urlImage = "";
        bool isFTP = false;
        int imgType = globalImageLib.IMG_PRODUCTS;

        string qrCodeValue = "";
        string moduleID = "load_ftp";

        int cnvWidth = 0;
        int cnvHeight = 0;

        public displayImageForm(string qrCodeParam, int width = 300, int height = 300)
        {
            InitializeComponent();
            qrCodeValue = qrCodeParam;

            moduleID = "display_qr";

            cnvWidth = width;
            cnvHeight = height;
        }

        public displayImageForm(string urlParam, int imgParam, bool ftpParam)
        {
            InitializeComponent();

            urlImage = urlParam;
            imgType = imgParam;
            isFTP = ftpParam;

            gImg = new globalImageLib();
        }

        private void displayQR()
        {
            try
            {
                pictureBoxDisplay.SizeMode = PictureBoxSizeMode.CenterImage;
                this.Width = cnvWidth;// 300;
                this.Height = cnvHeight;// 300;

                var QCwriter = new BarcodeWriter();
                QCwriter.Format = BarcodeFormat.QR_CODE;
                QCwriter.Options.Height = cnvHeight;
                QCwriter.Options.Width = cnvWidth;

                var result = QCwriter.Write(qrCodeValue);
                var barcodeBitmap = new Bitmap(result);//, cnvWidth, cnvHeight);
                //barcodeBitmap.SetResolution(cnvWidth, cnvHeight);
                
                pictureBoxDisplay.Image = barcodeBitmap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void displayImage()
        {
            try
            {
                if (isFTP)
                {
                    Bitmap bm;
                    bm = gImg.ByteToImage(urlImage, imgType);
                    pictureBoxDisplay.Image = bm;
                }
                else
                {
                    pictureBoxDisplay.Load(urlImage);
                }
            }
            catch (Exception ex)
            {
                pictureBoxDisplay.Load(gImg.localImgDirectory + "errImg.jpeg");
            }
        }

        private void displayImageForm_Load(object sender, EventArgs e)
        {
            switch (moduleID)
            {
                case "display_qr":
                    displayQR();
                    break;

                default:
                    displayImage();
                    break;    
            }
        }

        private void saveImageQR()
        {
            try
            {
                saveImageDialog.AddExtension = true;
                saveImageDialog.DefaultExt = "jpeg";
                saveImageDialog.Filter = "JPEG File (.jpeg)|*.jpeg";

                if (DialogResult.OK == saveImageDialog.ShowDialog())
                {
                    pictureBoxDisplay.Image.Save(saveImageDialog.FileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pictureBoxDisplay_DoubleClick(object sender, EventArgs e)
        {
            if (moduleID == "display_qr")
            {
                saveImageQR();
            }
        }
    }
}
