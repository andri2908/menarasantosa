﻿namespace AlphaSoft
{
    partial class dataItemDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dataItemDetailForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.kategoriCombo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.hppTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBoxJasa = new System.Windows.Forms.CheckBox();
            this.searchImageButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.hargaTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.qtyTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.searchUnitButton = new System.Windows.Forms.Button();
            this.nonAktifCheckbox = new System.Windows.Forms.CheckBox();
            this.unitTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.namaTextBox = new System.Windows.Forms.TextBox();
            this.ResetButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.imgFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(484, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.kategoriCombo);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.hppTextBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.checkBoxJasa);
            this.groupBox1.Controls.Add(this.searchImageButton);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.hargaTextBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.qtyTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.searchUnitButton);
            this.groupBox1.Controls.Add(this.nonAktifCheckbox);
            this.groupBox1.Controls.Add(this.unitTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.namaTextBox);
            this.groupBox1.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(23, 44);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(440, 403);
            this.groupBox1.TabIndex = 57;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(118, 222);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 150);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 71;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            // 
            // kategoriCombo
            // 
            this.kategoriCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.kategoriCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.kategoriCombo.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kategoriCombo.FormattingEnabled = true;
            this.kategoriCombo.Location = new System.Drawing.Point(118, 86);
            this.kategoriCombo.Name = "kategoriCombo";
            this.kategoriCombo.Size = new System.Drawing.Size(298, 26);
            this.kategoriCombo.TabIndex = 70;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(27, 92);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 18);
            this.label7.TabIndex = 69;
            this.label7.Text = "Kategori";
            // 
            // hppTextBox
            // 
            this.hppTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.hppTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hppTextBox.Location = new System.Drawing.Point(118, 156);
            this.hppTextBox.MaxLength = 20;
            this.hppTextBox.Name = "hppTextBox";
            this.hppTextBox.Size = new System.Drawing.Size(164, 27);
            this.hppTextBox.TabIndex = 68;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(63, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 18);
            this.label6.TabIndex = 67;
            this.label6.Text = "HPP";
            // 
            // checkBoxJasa
            // 
            this.checkBoxJasa.AutoSize = true;
            this.checkBoxJasa.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxJasa.ForeColor = System.Drawing.Color.Black;
            this.checkBoxJasa.Location = new System.Drawing.Point(250, 122);
            this.checkBoxJasa.Name = "checkBoxJasa";
            this.checkBoxJasa.Size = new System.Drawing.Size(106, 22);
            this.checkBoxJasa.TabIndex = 66;
            this.checkBoxJasa.Text = "item Jasa";
            this.checkBoxJasa.UseVisualStyleBackColor = true;
            this.checkBoxJasa.CheckedChanged += new System.EventHandler(this.checkBoxJasa_CheckedChanged);
            // 
            // searchImageButton
            // 
            this.searchImageButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchImageButton.BackgroundImage")));
            this.searchImageButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchImageButton.Location = new System.Drawing.Point(274, 222);
            this.searchImageButton.Name = "searchImageButton";
            this.searchImageButton.Size = new System.Drawing.Size(29, 30);
            this.searchImageButton.TabIndex = 65;
            this.searchImageButton.UseVisualStyleBackColor = true;
            this.searchImageButton.Click += new System.EventHandler(this.searchImageButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(30, 222);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 18);
            this.label5.TabIndex = 64;
            this.label5.Text = "Gambar";
            // 
            // hargaTextBox
            // 
            this.hargaTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.hargaTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hargaTextBox.Location = new System.Drawing.Point(118, 189);
            this.hargaTextBox.MaxLength = 20;
            this.hargaTextBox.Name = "hargaTextBox";
            this.hargaTextBox.Size = new System.Drawing.Size(164, 27);
            this.hargaTextBox.TabIndex = 62;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(6, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 18);
            this.label4.TabIndex = 61;
            this.label4.Text = "Harga Jual";
            // 
            // qtyTextBox
            // 
            this.qtyTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.qtyTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qtyTextBox.Location = new System.Drawing.Point(118, 120);
            this.qtyTextBox.MaxLength = 20;
            this.qtyTextBox.Name = "qtyTextBox";
            this.qtyTextBox.Size = new System.Drawing.Size(118, 27);
            this.qtyTextBox.TabIndex = 60;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(69, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 18);
            this.label1.TabIndex = 59;
            this.label1.Text = "Qty";
            // 
            // searchUnitButton
            // 
            this.searchUnitButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchUnitButton.BackgroundImage")));
            this.searchUnitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchUnitButton.Location = new System.Drawing.Point(379, 50);
            this.searchUnitButton.Name = "searchUnitButton";
            this.searchUnitButton.Size = new System.Drawing.Size(29, 30);
            this.searchUnitButton.TabIndex = 58;
            this.searchUnitButton.UseVisualStyleBackColor = true;
            this.searchUnitButton.Click += new System.EventHandler(this.searchUnitButton_Click);
            // 
            // nonAktifCheckbox
            // 
            this.nonAktifCheckbox.AutoSize = true;
            this.nonAktifCheckbox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nonAktifCheckbox.ForeColor = System.Drawing.Color.Black;
            this.nonAktifCheckbox.Location = new System.Drawing.Point(313, 337);
            this.nonAktifCheckbox.Name = "nonAktifCheckbox";
            this.nonAktifCheckbox.Size = new System.Drawing.Size(103, 22);
            this.nonAktifCheckbox.TabIndex = 51;
            this.nonAktifCheckbox.Text = "Non Aktif";
            this.nonAktifCheckbox.UseVisualStyleBackColor = true;
            // 
            // unitTextBox
            // 
            this.unitTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.unitTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unitTextBox.Location = new System.Drawing.Point(118, 52);
            this.unitTextBox.MaxLength = 5;
            this.unitTextBox.Name = "unitTextBox";
            this.unitTextBox.ReadOnly = true;
            this.unitTextBox.Size = new System.Drawing.Size(255, 27);
            this.unitTextBox.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(51, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 18);
            this.label2.TabIndex = 7;
            this.label2.Text = "Nama";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(40, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 18);
            this.label3.TabIndex = 8;
            this.label3.Text = "Satuan";
            // 
            // namaTextBox
            // 
            this.namaTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.namaTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.namaTextBox.Location = new System.Drawing.Point(118, 17);
            this.namaTextBox.MaxLength = 100;
            this.namaTextBox.Name = "namaTextBox";
            this.namaTextBox.Size = new System.Drawing.Size(300, 27);
            this.namaTextBox.TabIndex = 15;
            // 
            // ResetButton
            // 
            this.ResetButton.BackColor = System.Drawing.Color.FloralWhite;
            this.ResetButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResetButton.ForeColor = System.Drawing.Color.Black;
            this.ResetButton.Location = new System.Drawing.Point(261, 473);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(180, 37);
            this.ResetButton.TabIndex = 60;
            this.ResetButton.Text = "Reset";
            this.ResetButton.UseVisualStyleBackColor = false;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.BackColor = System.Drawing.Color.FloralWhite;
            this.SaveButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveButton.ForeColor = System.Drawing.Color.Black;
            this.SaveButton.Location = new System.Drawing.Point(49, 473);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(182, 37);
            this.SaveButton.TabIndex = 59;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = false;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // dataItemDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(484, 550);
            this.Controls.Add(this.ResetButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "dataItemDetailForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.dataItemDetailForm_FormClosed);
            this.Load += new System.EventHandler(this.dataItemDetailForm_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.SaveButton, 0);
            this.Controls.SetChildIndex(this.ResetButton, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.GroupBox groupBox1;
        protected System.Windows.Forms.TextBox hargaTextBox;
        protected System.Windows.Forms.Label label4;
        protected System.Windows.Forms.TextBox qtyTextBox;
        protected System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button searchUnitButton;
        protected System.Windows.Forms.CheckBox nonAktifCheckbox;
        protected System.Windows.Forms.TextBox unitTextBox;
        protected System.Windows.Forms.Label label2;
        protected System.Windows.Forms.Label label3;
        protected System.Windows.Forms.TextBox namaTextBox;
        private System.Windows.Forms.Button searchImageButton;
        protected System.Windows.Forms.Label label5;
        protected System.Windows.Forms.Button ResetButton;
        protected System.Windows.Forms.Button SaveButton;
        protected System.Windows.Forms.CheckBox checkBoxJasa;
        protected System.Windows.Forms.TextBox hppTextBox;
        protected System.Windows.Forms.Label label6;
        private System.Windows.Forms.OpenFileDialog imgFileDialog;
        protected System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox kategoriCombo;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
