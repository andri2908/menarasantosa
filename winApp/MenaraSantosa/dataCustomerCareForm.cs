﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DBGridExtension;
using System.Globalization;

namespace AlphaSoft
{
    public partial class dataCustomerCareForm : AlphaSoft.basicHotkeysForm
    {
        private int ticketID = 0;
        private CultureInfo culture = new CultureInfo("id-ID");

        private globalRestAPI gRest;
        private globalUserUtil gUser;
        private bool isViewOnly = false;

        cc_ticket_history detailData = new cc_ticket_history();

        private Color[] colorScheme = { Color.AliceBlue, Color.Beige, Color.CadetBlue, Color.DarkBlue, Color.Firebrick, Color.Gainsboro };

        public dataCustomerCareForm(int ticketIDParam)
        {
            InitializeComponent();

            ticketID = ticketIDParam;

            gRest = new globalRestAPI();
            gUser = new globalUserUtil();
        }

        private void loadDataDetail()
        {
            string sqlCommand = "";
            REST_custCareDetail detailData = new REST_custCareDetail();

            #region DETAIL
            sqlCommand = "SELECT cc.history_id, cc.ticket_id, cc.sent_by, " +
                                    "ul.user_name, ul.user_full_name, cc.send_date, cc.description, " +
                                    "cc.is_mtc, ifnull(cc.mtc_cost, 0) as mtc_cost, cc.is_active, mg.group_type " +
                                    "FROM cc_ticket_history cc " +
                                    "LEFT OUTER JOIN user_login_data ul ON (cc.sent_by = ul.user_id) " +
                                    "LEFT OUTER JOIN master_group mg ON (ul.group_id = mg.group_id) " +
                                    "WHERE cc.ticket_id = " + ticketID;

            if (gRest.getCustCareDetailData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref detailData))
            {
                if (detailData.dataStatus.o_status == 1)
                {
                    ticketGridView.Rows.Clear();
                    for (int i = 0; i < detailData.dataList.Count; i++)
                    {
                        ticketGridView.Rows.Add(
                            detailData.dataList[i].history_id,
                            detailData.dataList[i].sent_by,
                            detailData.dataList[i].send_date,
                            detailData.dataList[i].user_name,
                            String.Format(culture, "{0:dd-MM-yyyy}", detailData.dataList[i].send_date),
                            detailData.dataList[i].description,
                            detailData.dataList[i].mtc_cost.ToString("N0", culture),
                            detailData.dataList[i].is_mtc,
                            detailData.dataList[i].is_active
                            );

                        ticketGridView.Rows[ticketGridView.Rows.Count - 1].DefaultCellStyle.BackColor = colorScheme[detailData.dataList[i].group_type];
                    }
                }
            }
            #endregion
        }

        private void loadDataTicket()
        {
            string sqlCommand = "";
            REST_custCare headerData = new REST_custCare();

            #region HEADER
            sqlCommand = "SELECT cc.ticket_id, cc.ticket_num, cc.ticket_date, cc.request_by, cc.kavling_id, cc.kompleks_id, cc.subject, cc.description, cc.status, cc.is_closed, cc.is_active, " +
                               "ul.user_name, ul.user_full_name, ml.kompleks_name, " +
                               "mb.blok_name, mk.house_no, cc.ticket_type as category_name " +
                               "FROM cc_ticket cc " +
                               "LEFT OUTER JOIN user_login_data ul ON (cc.request_by = ul.user_id) " +
                               "LEFT OUTER JOIN master_kavling mk ON (cc.kavling_id = mk.kavling_id AND mk.is_active = 'Y') " +
                               "LEFT OUTER JOIN master_kompleks ml ON (mk.kompleks_id = ml.kompleks_id AND ml.is_active = 'Y') " +
                               "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id AND mb.is_active = 'Y') " +
                               "WHERE cc.ticket_id = " + ticketID;

            if (gRest.getCustCareData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref headerData))
            {
                if (headerData.dataStatus.o_status == 1)
                {
                    dateDTPicker.Value = headerData.dataList[0].ticket_date;
                    kompleksTextBox.Text = headerData.dataList[0].kompleks_name;
                    namaBlokTextBox.Text = headerData.dataList[0].blok_name + "-" + headerData.dataList[0].house_no;
                    userTextBox.Text = headerData.dataList[0].user_name;
                    subjectTextBox.Text = headerData.dataList[0].subject;
                    descriptionTextBox.Text = headerData.dataList[0].description;
                    statusLabel.Text = headerData.dataList[0].status;

                    isViewOnly = (headerData.dataList[0].is_active == "N" || headerData.dataList[0].is_closed == "Y" ? true : false);
                }
            }
            #endregion

            loadDataDetail();

            if (isViewOnly)
            {
                gUser.disableAllControls(this);
            }
        }

        private void dataCustomerCareForm_Load(object sender, EventArgs e)
        {
            loadDataTicket();
        }

        private void newButton_Click(object sender, EventArgs e)
        {
            detailData.resetValue();

            detailData.ticket_id = ticketID;

            dataCustCare_detailForm displayForm = new dataCustCare_detailForm(ref detailData);
            displayForm.ShowDialog(this);

            loadDataTicket();
        }

        private void displayDetailTicket(DataGridViewRow sRow)
        {
            double costValue = 0;
            detailData.resetValue();

            double.TryParse(sRow.Cells["mtcCost"].Value.ToString(), NumberStyles.Number, culture, out costValue);

            detailData.ticket_id = ticketID;
            detailData.history_id = Convert.ToInt32(sRow.Cells["historyID"].Value);
            detailData.sent_by = Convert.ToInt32(sRow.Cells["userID"].Value);
            detailData.send_date = Convert.ToDateTime(sRow.Cells["sendDateValue"].Value);
            detailData.description = sRow.Cells["description"].Value.ToString();
            detailData.mtc_cost = costValue;
            detailData.is_active = sRow.Cells["isActive"].Value.ToString();
            detailData.is_mtc = sRow.Cells["isMTC"].Value.ToString();

            dataCustCare_detailForm displayForm = new dataCustCare_detailForm(ref detailData, isViewOnly);
            displayForm.ShowDialog(this);

            loadDataTicket();
        }

        private void ticketGridView_DoubleClick(object sender, EventArgs e)
        {
            if (ticketGridView.Rows.Count > 0)
            {
                int selectedrowindex = ticketGridView.SelectedCells[0].RowIndex;
                DataGridViewRow sRow = ticketGridView.Rows[selectedrowindex];

                displayDetailTicket(sRow);
            }
        }

        private void tutupButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.No == MessageBox.Show("Close tiket ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                return;

            List<cc_status> listTicket = new List<cc_status>();
            cc_status tiketData = new cc_status();

            tiketData.ticket_id = ticketID;
            tiketData.status = globalConstants.TS_closed;
            tiketData.status_date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            tiketData.status_by = gUser.getUserID();
            tiketData.is_active = "N";
            tiketData.is_closed = "Y";

            listTicket.Add(tiketData);

            string errMsg = "";
            int newID = 0;
            if (!gRest.updateCustCare(gUser.getUserID(), gUser.getUserToken(), listTicket, out errMsg, out newID))
            {
                errorLabel.Text = errMsg;
                MessageBox.Show("FAIL");
            }
            else
            {
                MessageBox.Show("SUCCESS");
                this.Close();
            }
        }
    }
}
