﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class dataBrowse_UserForm : simpleBrowseForm
    {
        private int originModuleID = 0;
        private globalUserUtil gUtil;
        private globalRestAPI gRest;
        private int paramID = 0;

        public string retUserFullName = "";

        REST_userLoginData userLoginData = new REST_userLoginData();

        public dataBrowse_UserForm(int moduleID = 0, int paramIDValue = 0)
        {
            InitializeComponent();
            originModuleID = moduleID;

            paramID = paramIDValue;
            
            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();

            ReturnValue1 = "0";
            ReturnValue2 = "0";
        }

        private void dataUserForm_Load(object sender, EventArgs e)
        {
            displayAllAction();

            if (!gUtil.RS_userHasAccessTo(MMConstants.TAMBAH_HAPUS_USER,
                MMConstants.HAK_NEW_DATA))
                setNewButtonEnable(false);
        }

        private void displayDefault()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string userName = "";

            dataGridView.DataSource = null;

            userName = getNamaTextBox();

            sqlCommand = "SELECT mu.*, mg.group_name " + 
                                    "FROM user_login_data mu, master_group mg " +
                                    "WHERE mu.group_id = mg.group_id ";

            if (userName.Length > 0)
                sqlCommand = sqlCommand + "AND mu.user_name LIKE '%" + userName + "%' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND mu.is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY mu.user_name ASC, mg.group_name ASC";

            if (gRest.getUserLoginData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref userLoginData))
            {
                dt.Columns.Add("USER_ID");
                dt.Columns.Add("NAMA USER");
                dt.Columns.Add("NAMA LENGKAP");
                dt.Columns.Add("NAMA GROUP");

                for (int i = 0; i < userLoginData.dataList.Count; i++)
                {
                    DataRow r = dt.Rows.Add();
                    r["USER_ID"] = userLoginData.dataList[i].user_id;
                    r["NAMA USER"] = userLoginData.dataList[i].user_name;
                    r["NAMA LENGKAP"] = userLoginData.dataList[i].user_full_name;
                    r["NAMA GROUP"] = userLoginData.dataList[i].group_name;
                }

                dataGridView.DataSource = dt;
                dataGridView.Columns["USER_ID"].Visible = false;
            }
        }

        private void displayUserKavling()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string userName = "";
            string sqlKavling = "";

            dataGridView.DataSource = null;

            userName = getNamaTextBox();

            sqlKavling = "SELECT user_id FROM user_kavling WHERE kavling_id = " + paramID + " AND is_active = 'Y'";

            sqlCommand = "SELECT * " + 
                                    "FROM user_login_data mu, master_group mg " +
                                    "WHERE mu.group_id = mg.group_id " +
                                    "AND mg.group_type IN (" + globalConstants.GROUP_LANDLORD + ", " + globalConstants.GROUP_RESIDENT + ") " +
                                    "AND mu.user_id NOT IN (" + sqlKavling + ") ";

            if (userName.Length > 0)
                sqlCommand = sqlCommand + "AND mu.user_name LIKE '%" + userName + "%' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND mu.is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY mu.user_name ASC";

            gRest.getUserLoginData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref userLoginData);

            dt.Columns.Add("USER_ID");
            dt.Columns.Add("NAMA USER");
            dt.Columns.Add("NAMA LENGKAP");

            for (int i = 0; i < userLoginData.dataList.Count; i++)
            {
                DataRow r = dt.Rows.Add();
                r["USER_ID"] = userLoginData.dataList[i].user_id;
                r["NAMA USER"] = userLoginData.dataList[i].user_name;
                r["NAMA LENGKAP"] = userLoginData.dataList[i].user_full_name;
            }

            dataGridView.DataSource = dt;
            dataGridView.Columns["USER_ID"].Visible = false;
        }

        private void displayUserIPL()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string userName = "";
            string sqlKavling = "";

            dataGridView.DataSource = null;

            userName = getNamaTextBox();

            sqlKavling = "SELECT user_id FROM user_kavling WHERE kavling_id = " + paramID + " AND is_active = 'Y'";

            sqlCommand = "SELECT * " +
                                    "FROM user_login_data mu, master_group mg " +
                                    "WHERE mu.group_id = mg.group_id " +
                                    "AND mg.group_type IN (" + globalConstants.GROUP_LANDLORD + ", " + globalConstants.GROUP_RESIDENT + ") " +
                                    "AND mu.user_id IN (" + sqlKavling + ") ";

            if (userName.Length > 0)
                sqlCommand = sqlCommand + "AND mu.user_name LIKE '%" + userName + "%' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND mu.is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY mu.user_name ASC";

            gRest.getUserLoginData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref userLoginData);

            dt.Columns.Add("USER_ID");
            dt.Columns.Add("NAMA USER");
            dt.Columns.Add("NAMA LENGKAP");

            for (int i = 0; i < userLoginData.dataList.Count; i++)
            {
                DataRow r = dt.Rows.Add();
                r["USER_ID"] = userLoginData.dataList[i].user_id;
                r["NAMA USER"] = userLoginData.dataList[i].user_name;
                r["NAMA LENGKAP"] = userLoginData.dataList[i].user_full_name;
            }

            dataGridView.DataSource = dt;
            dataGridView.Columns["USER_ID"].Visible = false;
        }

        protected override void displayAllAction()
        {
            switch (originModuleID)
            {
                case globalConstants.ADD_KAVLING_USER:
                    displayUserKavling();
                    break;

                case globalConstants.SUMMARY_IPL:
                case globalConstants.USER_RFID:
                    displayUserIPL();
                    break;

                default:
                    displayDefault();
                    break;
            }
        }

        protected override void newButtonAction()
        {
            dataUserDetailForm displayForm = new dataUserDetailForm();
            displayForm.ShowDialog(this);          
        }

        protected override void dbGridDoubleClickAction(DataGridViewRow selectedRow)
        {
            int selectedUserID = Convert.ToInt32(selectedRow.Cells["USER_ID"].Value);

            switch (originModuleID)
            {
                case globalConstants.MODULE_DEFAULT:
                case globalConstants.ADD_KAVLING_USER:
                case globalConstants.SUMMARY_IPL:
                case globalConstants.USER_RFID:
                    ReturnValue1 = selectedUserID.ToString();
                    ReturnValue2 = selectedRow.Cells["NAMA USER"].Value.ToString();
                    retUserFullName = selectedRow.Cells["NAMA LENGKAP"].Value.ToString();

                    this.Close();
                    break;

                default:
                    dataUserDetailForm editUserForm = new dataUserDetailForm(selectedUserID);
                    editUserForm.ShowDialog(this);
                    break;
            }
        }

        private void dataUserForm_FormClosed(object sender, FormClosedEventArgs e)
        {
      
        }
    }
}
