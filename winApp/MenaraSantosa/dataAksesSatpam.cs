﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using DBGridExtension;

namespace AlphaSoft
{
    public partial class dataAksesSatpam : AlphaSoft.basicHotkeysForm
    {
        private int selectedKompleksID = 0;

        private globalUserUtil gUser;
        private globalRestAPI gRest;
        private globalIPL gIPL;

        private CultureInfo culture = new CultureInfo("id-ID");
        private userKavling rowUserKavling = new userKavling();

        List<cbDataSource> cbDT = new List<cbDataSource>();
        List<userKavlingRFID> userKavRFID = new List<userKavlingRFID>();

        List<string> deletedID = new List<string>();

        public dataAksesSatpam()
        {
            InitializeComponent();

            gUser = new globalUserUtil();
            gRest = new globalRestAPI();
            gIPL = new globalIPL();
        }

        private void loadTipeCombo()
        {
            cbDataSource cbDataSourceElement;
            cbDT.Clear();

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "QR";
            cbDataSourceElement.valueMember = "QR";
            cbDT.Add(cbDataSourceElement);

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "RFID";
            cbDataSourceElement.valueMember = "RFID";
            cbDT.Add(cbDataSourceElement);

            DataGridViewComboBoxColumn cbColumn = (DataGridViewComboBoxColumn)satpamAksesGrid.Columns["accessType"];
            cbColumn.DataSource = cbDT;
            cbColumn.DisplayMember = "displayMember";
            cbColumn.ValueMember = "valueMember";
        }

        private void loadDataAksesKompleks()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            REST_satpamAccess satpamData = new REST_satpamAccess();

            sqlCommand = "SELECT * " +
                                    "FROM satpam_access_list " +
                                    "WHERE is_active = 'Y' " +
                                    "AND kompleks_id = " + selectedKompleksID + " ";

            sqlCommand += "ORDER BY user_full_name ASC";

            if (gRest.getSatpamAccessData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref satpamData))
            {
                if (satpamData.dataStatus.o_status == 1)
                {
                    satpamAksesGrid.Rows.Clear();
                    for (int i = 0; i < satpamData.dataList.Count; i++)
                    {
                        satpamAksesGrid.Rows.Add(
                            1,
                            satpamData.dataList[i].id,
                            satpamData.dataList[i].user_full_name,
                            satpamData.dataList[i].access_id,
                            satpamData.dataList[i].access_type
                            );
                    }
                }
            }
        }

        private void searchKompleksButton_Click(object sender, EventArgs e)
        {
            dataBrowse_KompleksForm displayForm = new dataBrowse_KompleksForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                selectedKompleksID = Convert.ToInt32(displayForm.ReturnValue1);
                kompleksTextBox.Text = displayForm.ReturnValue2;

                loadDataAksesKompleks();

                SaveButton.Enabled = true;
                newRFID.Enabled = true;
            }
        }

        private void dataAksesSatpam_Load(object sender, EventArgs e)
        {
            loadTipeCombo();
        }

        private string getSatpamQR()
        {
            string satpamQR = "";
            DataGridViewRow sRow;

            while (true)
            {
                satpamQR = gUser.getRandomString(10);

                for (int i = 0;i<satpamAksesGrid.Rows.Count-1;i++)
                {
                    sRow = satpamAksesGrid.Rows[i];

                    if (null != sRow.Cells["accessID"].Value)
                    {
                        if (sRow.Cells["accessID"].Value.ToString() == satpamQR 
                            || gIPL.accessSatpamExist(sRow.Cells["accessID"].Value.ToString(), 0, selectedKompleksID, deletedID))
                            continue;
                    }
                }

                break;
            }

            return satpamQR;
        }

        private void newRFID_Click(object sender, EventArgs e)
        {
            satpamAksesGrid.Rows.Add(
                1,
                0,
                "",
                getSatpamQR(),
                "QR"
                );
        }

        private bool saveTrans(out string errMsg)
        {
            bool result = false;
            errMsg = "";

            List<satpam_access_list> listData = new List<satpam_access_list>();
            satpam_access_list hData;

            try
            {
                DataGridViewRow sRow;
                for (int i = 0; i < satpamAksesGrid.Rows.Count; i++)
                {
                    sRow = satpamAksesGrid.Rows[i];

                    if (sRow.Cells["accessID"].Value != null)
                    {
                        if (Convert.ToInt32(sRow.Cells["id"].Value) == 0 &&
                            sRow.Cells["flag"].Value.ToString() == "0")
                            continue;

                        hData = new satpam_access_list();

                        hData.id = Convert.ToInt32(sRow.Cells["id"].Value);
                        hData.kompleks_id = selectedKompleksID;
                        hData.access_id = sRow.Cells["accessID"].Value.ToString();
                        hData.user_full_name = sRow.Cells["userFullName"].Value.ToString();
                        hData.access_type = sRow.Cells["accessType"].Value.ToString();
                        hData.is_active = (sRow.Cells["flag"].Value.ToString() == "1" ? "Y" : "N");

                        listData.Add(hData);
                    }
                }

                int newID = 0;
                if (!gRest.saveDataAksesSatpam(gUser.getUserID(), gUser.getUserToken(), listData,
                    out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
            }

            return result;
        }

        private bool dataValid()
        {
            errorLabel.Text = "";
            if (satpamAksesGrid.Rows.Count <= 0)
            {
                errorLabel.Text = "Tidak ada data akses";
                return false;
            }

            DataGridViewRow sRow;
            string accessIDValue = "";
            int aksesID = 0;
            int i = 0;
            List<string> satpamQR = new List<string>();
            for (i = 0; i < satpamAksesGrid.Rows.Count; i++)
            {
                sRow = satpamAksesGrid.Rows[i];
                if (Convert.ToInt32(sRow.Cells["flag"].Value) == 0)
                    continue;

                if (null == sRow.Cells["userFullName"].Value || sRow.Cells["userFullName"].Value.ToString().Length <= 0)
                {
                    errorLabel.Text = "Nama kosong";
                    satpamAksesGrid.Rows[i].Cells["userFullName"].Selected = true;
                    return false;
                }

                if (null == sRow.Cells["accessID"].Value || sRow.Cells["accessID"].Value.ToString().Length <= 0)
                {
                    errorLabel.Text = "Akses ID kosong";
                    satpamAksesGrid.Rows[i].Cells["accessID"].Selected = true;
                    return false;
                }

                aksesID = Convert.ToInt32(sRow.Cells["id"].Value);
                accessIDValue = sRow.Cells["accessID"].Value.ToString();

                if (satpamQR.Contains(accessIDValue) ||
                    gIPL.accessSatpamExist(accessIDValue, aksesID, selectedKompleksID, deletedID))
                {
                    errorLabel.Text = "Akses ID [" + accessIDValue + "] sudah ada";
                    return false;
                }

                satpamQR.Add(accessIDValue);
            }
            return true;
        }

        private bool saveData()
        {
            string errMsg = "";

            if (dataValid())
            {
                return saveTrans(out errMsg);
            }

            return false;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Save Data Akses Satpam ?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {
                if (saveData())
                {
                    MessageBox.Show("Success");
                    this.Close();
                }
            }
        }

        private void deleteCurrentRow()
        {
            if (satpamAksesGrid.Rows.Count > 0)
            {
                if (DialogResult.Yes == MessageBox.Show("Hapus baris ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    try
                    {
                        string errMsg = "";
                        int newID = 0;
                        int rowIndex = satpamAksesGrid.SelectedCells[0].RowIndex;
                        DataGridViewRow selectedRow = satpamAksesGrid.Rows[rowIndex];

                        int accessID = Convert.ToInt32(selectedRow.Cells["id"].Value);
                        if (!gRest.deleteDataAksesSatpam(gUser.getUserID(), gUser.getUserToken(), accessID,
                            out errMsg, out newID))
                        {
                            throw new Exception(errMsg);
                        }

                        satpamAksesGrid.Rows.Remove(selectedRow);
                    }
                    catch (Exception ex)
                    {
                        errorLabel.Text = ex.Message;
                    }
                    //if (Convert.ToInt32(selectedRow.Cells["id"].Value) > 0)
                    //    deletedID.Add(selectedRow.Cells["accessID"].Value.ToString().ToUpper());

                    //selectedRow.Cells["flag"].Value = 0;
                    //selectedRow.Visible = false;
                }
            }
        }

        private void satpamAksesGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (satpamAksesGrid.Rows.Count > 0)
                if (e.KeyCode == Keys.Delete)
                {
                    deleteCurrentRow();
                }
        }

        private void satpamAksesGrid_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (satpamAksesGrid.IsCurrentCellDirty)
            {
                satpamAksesGrid.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }
    }
}
