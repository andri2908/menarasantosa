﻿namespace AlphaSoft
{
    partial class dataUnitDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox1.Size = new System.Drawing.Size(539, 87);
            this.groupBox1.TabIndex = 3;
            // 
            // ResetButton
            // 
            this.ResetButton.Location = new System.Drawing.Point(291, 146);
            this.ResetButton.TabIndex = 10;
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(142, 146);
            this.SaveButton.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(6, 20);
            this.label2.Size = new System.Drawing.Size(126, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "Nama Satuan";
            // 
            // label3
            // 
            this.label3.TabIndex = 4;
            this.label3.Visible = false;
            // 
            // nonAktifCheckbox
            // 
            this.nonAktifCheckbox.Location = new System.Drawing.Point(138, 56);
            this.nonAktifCheckbox.TabIndex = 8;
            // 
            // namaTextBox
            // 
            this.namaTextBox.Location = new System.Drawing.Point(138, 17);
            this.namaTextBox.MaxLength = 100;
            this.namaTextBox.Size = new System.Drawing.Size(383, 27);
            this.namaTextBox.TabIndex = 6;
            // 
            // deskripsiTextBox
            // 
            this.deskripsiTextBox.TabIndex = 7;
            this.deskripsiTextBox.Visible = false;
            // 
            // panel1
            // 
            this.panel1.TabIndex = 0;
            // 
            // errorLabel
            // 
            this.errorLabel.TabIndex = 1;
            // 
            // dataUnitDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(558, 224);
            this.Name = "dataUnitDetailForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.dataUnitDetailForm_FormClosed);
            this.Load += new System.EventHandler(this.dataUnitDetailForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
