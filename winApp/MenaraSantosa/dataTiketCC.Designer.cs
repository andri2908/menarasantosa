﻿namespace AlphaSoft
{
    partial class dataTiketCC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dataTiketCC));
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.finishButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.imgButton = new System.Windows.Forms.Button();
            this.txtBoxKirim = new System.Windows.Forms.TextBox();
            this.panelComponent = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.subjectTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.noTicketTextBox = new System.Windows.Forms.TextBox();
            this.imgFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.timerRefresh = new System.Windows.Forms.Timer(this.components);
            this.labelLogin = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Size = new System.Drawing.Size(817, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.AutoScroll = true;
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.labelLogin);
            this.panel2.Controls.Add(this.buttonRefresh);
            this.panel2.Controls.Add(this.finishButton);
            this.panel2.Controls.Add(this.saveButton);
            this.panel2.Controls.Add(this.imgButton);
            this.panel2.Controls.Add(this.txtBoxKirim);
            this.panel2.Controls.Add(this.panelComponent);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.subjectTextBox);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.noTicketTextBox);
            this.panel2.Location = new System.Drawing.Point(6, 35);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(799, 599);
            this.panel2.TabIndex = 23;
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRefresh.BackColor = System.Drawing.Color.FloralWhite;
            this.buttonRefresh.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRefresh.ForeColor = System.Drawing.Color.Black;
            this.buttonRefresh.Location = new System.Drawing.Point(685, 517);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(90, 37);
            this.buttonRefresh.TabIndex = 208;
            this.buttonRefresh.Text = "Refresh";
            this.buttonRefresh.UseVisualStyleBackColor = false;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // finishButton
            // 
            this.finishButton.BackColor = System.Drawing.Color.FloralWhite;
            this.finishButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.finishButton.ForeColor = System.Drawing.Color.Black;
            this.finishButton.Location = new System.Drawing.Point(598, 50);
            this.finishButton.Name = "finishButton";
            this.finishButton.Size = new System.Drawing.Size(177, 37);
            this.finishButton.TabIndex = 207;
            this.finishButton.Text = "Selesai";
            this.finishButton.UseVisualStyleBackColor = false;
            this.finishButton.Click += new System.EventHandler(this.finishButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveButton.BackColor = System.Drawing.Color.FloralWhite;
            this.saveButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.ForeColor = System.Drawing.Color.Black;
            this.saveButton.Location = new System.Drawing.Point(573, 517);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(90, 37);
            this.saveButton.TabIndex = 206;
            this.saveButton.Text = "Kirim";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // imgButton
            // 
            this.imgButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.imgButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("imgButton.BackgroundImage")));
            this.imgButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgButton.Location = new System.Drawing.Point(526, 517);
            this.imgButton.Name = "imgButton";
            this.imgButton.Size = new System.Drawing.Size(29, 30);
            this.imgButton.TabIndex = 205;
            this.imgButton.UseVisualStyleBackColor = true;
            this.imgButton.Click += new System.EventHandler(this.imgButton_Click);
            // 
            // txtBoxKirim
            // 
            this.txtBoxKirim.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxKirim.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxKirim.Location = new System.Drawing.Point(18, 517);
            this.txtBoxKirim.MaxLength = 3000;
            this.txtBoxKirim.Multiline = true;
            this.txtBoxKirim.Name = "txtBoxKirim";
            this.txtBoxKirim.Size = new System.Drawing.Size(502, 71);
            this.txtBoxKirim.TabIndex = 176;
            // 
            // panelComponent
            // 
            this.panelComponent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelComponent.AutoScroll = true;
            this.panelComponent.BackColor = System.Drawing.Color.LightGray;
            this.panelComponent.Location = new System.Drawing.Point(18, 100);
            this.panelComponent.Name = "panelComponent";
            this.panelComponent.Size = new System.Drawing.Size(757, 411);
            this.panelComponent.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(12, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 18);
            this.label5.TabIndex = 174;
            this.label5.Text = "Subyek";
            // 
            // subjectTextBox
            // 
            this.subjectTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectTextBox.Location = new System.Drawing.Point(90, 56);
            this.subjectTextBox.MaxLength = 30;
            this.subjectTextBox.Name = "subjectTextBox";
            this.subjectTextBox.ReadOnly = true;
            this.subjectTextBox.Size = new System.Drawing.Size(502, 27);
            this.subjectTextBox.TabIndex = 175;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(12, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 18);
            this.label3.TabIndex = 69;
            this.label3.Text = "Nomor";
            // 
            // noTicketTextBox
            // 
            this.noTicketTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noTicketTextBox.Location = new System.Drawing.Point(90, 20);
            this.noTicketTextBox.MaxLength = 30;
            this.noTicketTextBox.Name = "noTicketTextBox";
            this.noTicketTextBox.ReadOnly = true;
            this.noTicketTextBox.Size = new System.Drawing.Size(300, 27);
            this.noTicketTextBox.TabIndex = 70;
            // 
            // imgFileDialog
            // 
            this.imgFileDialog.FileName = "openFileDialog1";
            // 
            // timerRefresh
            // 
            this.timerRefresh.Interval = 60000;
            this.timerRefresh.Tick += new System.EventHandler(this.timerRefresh_Tick);
            // 
            // labelLogin
            // 
            this.labelLogin.AutoSize = true;
            this.labelLogin.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLogin.ForeColor = System.Drawing.Color.Black;
            this.labelLogin.Location = new System.Drawing.Point(417, 23);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(87, 18);
            this.labelLogin.TabIndex = 209;
            this.labelLogin.Text = "Reply as ";
            // 
            // dataTiketCC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(817, 668);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "dataTiketCC";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.dataTiketCC_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        protected System.Windows.Forms.Label label5;
        protected System.Windows.Forms.TextBox subjectTextBox;
        protected System.Windows.Forms.Label label3;
        protected System.Windows.Forms.TextBox noTicketTextBox;
        private System.Windows.Forms.Panel panelComponent;
        protected System.Windows.Forms.TextBox txtBoxKirim;
        private System.Windows.Forms.Button imgButton;
        protected System.Windows.Forms.Button saveButton;
        protected System.Windows.Forms.Button finishButton;
        private System.Windows.Forms.OpenFileDialog imgFileDialog;
        private System.Windows.Forms.Timer timerRefresh;
        protected System.Windows.Forms.Button buttonRefresh;
        protected System.Windows.Forms.Label labelLogin;
    }
}
