﻿namespace AlphaSoft
{
    partial class dataEditUserKavlingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.checkBAST = new System.Windows.Forms.CheckBox();
            this.bastDTPicker = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.qrAccessCheckBox = new System.Windows.Forms.CheckBox();
            this.printButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.qrCodeTextBox = new System.Windows.Forms.TextBox();
            this.remarkTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dateDTPicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.userTextBox = new System.Windows.Forms.TextBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(618, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.checkBAST);
            this.panel2.Controls.Add(this.bastDTPicker);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.qrAccessCheckBox);
            this.panel2.Controls.Add(this.printButton);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.qrCodeTextBox);
            this.panel2.Controls.Add(this.remarkTextBox);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.dateDTPicker);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.userTextBox);
            this.panel2.Location = new System.Drawing.Point(20, 47);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(586, 269);
            this.panel2.TabIndex = 21;
            // 
            // checkBAST
            // 
            this.checkBAST.AutoSize = true;
            this.checkBAST.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBAST.Location = new System.Drawing.Point(290, 55);
            this.checkBAST.Name = "checkBAST";
            this.checkBAST.Size = new System.Drawing.Size(112, 20);
            this.checkBAST.TabIndex = 73;
            this.checkBAST.Text = "mulai BAST ";
            this.checkBAST.UseVisualStyleBackColor = true;
            this.checkBAST.CheckedChanged += new System.EventHandler(this.checkBAST_CheckedChanged);
            // 
            // bastDTPicker
            // 
            this.bastDTPicker.CustomFormat = "dd MMM yyyy";
            this.bastDTPicker.Enabled = false;
            this.bastDTPicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bastDTPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.bastDTPicker.Location = new System.Drawing.Point(134, 50);
            this.bastDTPicker.Name = "bastDTPicker";
            this.bastDTPicker.Size = new System.Drawing.Size(150, 26);
            this.bastDTPicker.TabIndex = 72;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(40, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 18);
            this.label5.TabIndex = 71;
            this.label5.Text = "Tgl BAST";
            // 
            // qrAccessCheckBox
            // 
            this.qrAccessCheckBox.AutoSize = true;
            this.qrAccessCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qrAccessCheckBox.Location = new System.Drawing.Point(440, 233);
            this.qrAccessCheckBox.Name = "qrAccessCheckBox";
            this.qrAccessCheckBox.Size = new System.Drawing.Size(96, 20);
            this.qrAccessCheckBox.TabIndex = 70;
            this.qrAccessCheckBox.Text = "QR Akses";
            this.qrAccessCheckBox.UseVisualStyleBackColor = true;
            // 
            // printButton
            // 
            this.printButton.BackColor = System.Drawing.Color.FloralWhite;
            this.printButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.printButton.ForeColor = System.Drawing.Color.Black;
            this.printButton.Location = new System.Drawing.Point(133, 227);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(117, 27);
            this.printButton.TabIndex = 68;
            this.printButton.Text = "Cetak QR";
            this.printButton.UseVisualStyleBackColor = false;
            this.printButton.Click += new System.EventHandler(this.printButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(43, 230);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 18);
            this.label4.TabIndex = 66;
            this.label4.Text = "QR Code";
            // 
            // qrCodeTextBox
            // 
            this.qrCodeTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.qrCodeTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qrCodeTextBox.Location = new System.Drawing.Point(134, 227);
            this.qrCodeTextBox.MaxLength = 30;
            this.qrCodeTextBox.Name = "qrCodeTextBox";
            this.qrCodeTextBox.ReadOnly = true;
            this.qrCodeTextBox.Size = new System.Drawing.Size(300, 27);
            this.qrCodeTextBox.TabIndex = 67;
            this.qrCodeTextBox.Visible = false;
            // 
            // remarkTextBox
            // 
            this.remarkTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.remarkTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.remarkTextBox.Location = new System.Drawing.Point(134, 113);
            this.remarkTextBox.MaxLength = 200;
            this.remarkTextBox.Multiline = true;
            this.remarkTextBox.Name = "remarkTextBox";
            this.remarkTextBox.Size = new System.Drawing.Size(423, 108);
            this.remarkTextBox.TabIndex = 65;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(16, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 18);
            this.label3.TabIndex = 64;
            this.label3.Text = "Keterangan";
            // 
            // dateDTPicker
            // 
            this.dateDTPicker.CustomFormat = "MMM yyyy";
            this.dateDTPicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateDTPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateDTPicker.Location = new System.Drawing.Point(134, 81);
            this.dateDTPicker.Name = "dateDTPicker";
            this.dateDTPicker.Size = new System.Drawing.Size(136, 26);
            this.dateDTPicker.TabIndex = 63;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(40, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 18);
            this.label1.TabIndex = 62;
            this.label1.Text = "Mulai IPL";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(79, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 18);
            this.label2.TabIndex = 60;
            this.label2.Text = "User";
            // 
            // userTextBox
            // 
            this.userTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.userTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userTextBox.Location = new System.Drawing.Point(134, 15);
            this.userTextBox.MaxLength = 30;
            this.userTextBox.Name = "userTextBox";
            this.userTextBox.ReadOnly = true;
            this.userTextBox.Size = new System.Drawing.Size(300, 27);
            this.userTextBox.TabIndex = 61;
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.Color.FloralWhite;
            this.saveButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.ForeColor = System.Drawing.Color.Black;
            this.saveButton.Location = new System.Drawing.Point(224, 331);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(170, 37);
            this.saveButton.TabIndex = 33;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // dataEditUserKavlingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(618, 403);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.panel2);
            this.Name = "dataEditUserKavlingForm";
            this.Load += new System.EventHandler(this.dataEditUserKavlingForm_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.saveButton, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        protected System.Windows.Forms.Label label2;
        protected System.Windows.Forms.TextBox userTextBox;
        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.DateTimePicker dateDTPicker;
        protected System.Windows.Forms.Label label3;
        protected System.Windows.Forms.TextBox remarkTextBox;
        protected System.Windows.Forms.Button saveButton;
        protected System.Windows.Forms.Label label4;
        protected System.Windows.Forms.TextBox qrCodeTextBox;
        protected System.Windows.Forms.Button printButton;
        private System.Windows.Forms.CheckBox qrAccessCheckBox;
        protected System.Windows.Forms.DateTimePicker bastDTPicker;
        protected System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBAST;
    }
}
