﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using DBGridExtension;
using Hotkeys;

namespace AlphaSoft
{
    public partial class dataTransaksiForm : AlphaSoft.basicHotkeysForm
    {
        private int originModuleID = 0;
        private int selectedKavlingID = 0;
        private int selectedKompleksID = 0;
        private int selectedUserID = 0;

        private double globalTotalValue = 0;
        private int transStatus = 1;
        private int selectedIDTrans = 0;
        private int transModule = globalConstants.MODULE_TRANSAKSI_RETAIL;
        private bool isViewOnly = false;

        List<cbDataSource> cbDT = new List<cbDataSource>();

        private globalUserUtil gUser;
        private globalItemLib gItem;
        private globalRestAPI gRest;

        private CultureInfo culture = new CultureInfo("id-ID");

        private Hotkeys.GlobalHotkey ghk_F4;
        private Hotkeys.GlobalHotkey ghk_F5;
        private Hotkeys.GlobalHotkey ghk_F9;
        private Hotkeys.GlobalHotkey ghk_F10;

        protected override void captureAll(Keys key)
        {
            base.captureAll(key);

            if (isViewOnly)
                return;

            switch (key)
            {
                case Keys.F5:
                    resetScreen();
                    break;

                case Keys.F9:
                    saveData();
                    break;

                case Keys.F4:
                    displayProductForm();
                    break;

                case Keys.F10:
                    if (originModuleID ==globalConstants.EDIT_ITEM)
                    {
                        transStatus = 0;
                        saveData();
                    }
                    break;
            }
        }

        protected override void registerGlobalHotkey()
        {
            base.registerGlobalHotkey();

            ghk_F4 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F4, this);
            ghk_F4.Register();

            ghk_F5 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F5, this);
            ghk_F5.Register();

            ghk_F9 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F9, this);
            ghk_F9.Register();

            ghk_F10 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F10, this);
            ghk_F10.Register();
        }

        protected override void unregisterGlobalHotkey()
        {
            base.unregisterGlobalHotkey();

            ghk_F4.Unregister();
            ghk_F5.Unregister();
            ghk_F9.Unregister();
            ghk_F10.Unregister();
        }

        public dataTransaksiForm(int moduleID = 0, int transID = 0, bool viewOnly = false)
        {
            InitializeComponent();

            gUser = new globalUserUtil();
            gItem = new globalItemLib();
            gRest = new globalRestAPI();

            transModule = moduleID;
            isViewOnly = viewOnly;

            if (transID > 0)
            {
                selectedIDTrans = transID;
                originModuleID = globalConstants.EDIT_ITEM;
            }
            else
                originModuleID = globalConstants.NEW_ITEM;
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            dataBrowse_UserForm displayForm = new dataBrowse_UserForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                searchKompleksButton.Enabled = true;
                selectedUserID = Convert.ToInt32(displayForm.ReturnValue1);
                userTextBox.Text = displayForm.ReturnValue2;

                selectedKompleksID = 0;
                kompleksTextBox.Text = "";

                selectedKavlingID = 0;
                namaBlokTextBox.Text = "";
            }

            searchBlokButton.Enabled = false;
        }

        private void searchKompleksButton_Click(object sender, EventArgs e)
        {
            dataBrowse_KompleksForm displayForm = new dataBrowse_KompleksForm(globalConstants.MODULE_TRANSAKSI_RETAIL, selectedUserID);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                selectedKompleksID = Convert.ToInt32(displayForm.ReturnValue1);
                kompleksTextBox.Text = displayForm.ReturnValue2;

                searchBlokButton.Enabled = true;
                selectedKavlingID = 0;
                namaBlokTextBox.Text = "";
            }
        }

        private void searchBlokButton_Click(object sender, EventArgs e)
        {
            dataBrowse_KavlingForm displayForm = new dataBrowse_KavlingForm(globalConstants.MODULE_TRANSAKSI_RETAIL, selectedKompleksID, selectedUserID);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                selectedKavlingID = Convert.ToInt32(displayForm.ReturnValue1);
                namaBlokTextBox.Text = displayForm.ReturnValue2;
            }
        }

        private void loadTransaksi()
        {
            string sqlCommand = "";
            DataTable dt = new DataTable();

            #region LOAD HEADER
            REST_transaksiHeader transHeader = new REST_transaksiHeader();
            sqlCommand = "SELECT ih.*, IFNULL(mu.user_name, '') AS user_name, IFNULL(mo.kompleks_name, '') AS kompleks, " +
                                    "CONCAT(IFNULL(mb.blok_name, ''), '-', IFNULL(mk.house_no, '')) AS nama_kavling, " +
                                    "mk.kompleks_id " +
                                    "FROM " +
                                    "transaksi_retail ih " +
                                    "LEFT OUTER JOIN user_login_data mu ON (ih.user_id = mu.user_id) " +
                                    "LEFT OUTER JOIN master_kavling mk ON (ih.kavling_id = mk.kavling_id) " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id) " +
                                    "LEFT OUTER JOIN master_kompleks mo ON (mk.kompleks_id = mo.kompleks_id) " +
                                    "WHERE ih.is_active = 'Y' " +
                                    "AND ih.id_trans = " + selectedIDTrans + " ";

            if (gRest.getTransHeaderData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref transHeader))
            {
                if (transHeader.dataStatus.o_status == 1)
                {
                    dateDTPicker.Value = transHeader.dataList[0].date_issued;

                    selectedUserID = transHeader.dataList[0].user_id;
                    selectedKompleksID = transHeader.dataList[0].kompleks_id;
                    selectedKavlingID = transHeader.dataList[0].kavling_id;

                    globalTotalValue = transHeader.dataList[0].nominal;
                    totalLabelValue.Text = globalTotalValue.ToString("C0", culture);

                    userTextBox.Text = transHeader.dataList[0].user_name;
                    kompleksTextBox.Text = transHeader.dataList[0].kompleks;
                    namaBlokTextBox.Text = transHeader.dataList[0].nama_kavling;

                    searchKompleksButton.Enabled = true;
                    searchBlokButton.Enabled = true;

                    if (transHeader.dataList[0].is_active == "N" || transHeader.dataList[0].status_id == 1)
                        originModuleID = globalConstants.VIEW_ONLY;

                    if (transHeader.dataList[0].status_id == 2)
                    {
                        labelStatus.Visible = true;
                        statusCombo.Visible = true;

                        //if (null == transHeader.dataList[0].deliver_status)
                        //    statusCombo.SelectedValue = "undelivered";
                        //else
                            statusCombo.SelectedValue = transHeader.dataList[0].deliver_status;
                    }
                }
            }
            #endregion

            #region LOAD DETAIL
            REST_transaksiDetail transDetail = new REST_transaksiDetail();
            sqlCommand = "SELECT id.*, IFNULL(mi.item_name, '') AS item_name, IFNULL(mi.item_jasa, 'N') AS item_jasa, IFNULL(mu.unit_name, '') AS satuan " +
                                    "FROM " +
                                    "transaksi_retail_detail id " +
                                    "LEFT OUTER JOIN master_item mi ON (id.item_id = mi.item_id) " +
                                    "LEFT OUTER JOIN master_unit mu ON (mi.unit_id = mu.unit_id) " +
                                    "WHERE id.is_active = 'Y' " +
                                    "AND id.id_trans = " + selectedIDTrans + " ";

            if (gRest.getTransDetailData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref transDetail))
            {
                if (transDetail.dataStatus.o_status == 1)
                {
                    transGridView.Rows.Clear();
                    for (int i = 0;i<transDetail.dataList.Count;i++)
                    {
                        transGridView.Rows.Add(
                            0,
                            transDetail.dataList[i].id,
                            transDetail.dataList[i].item_id,
                            transDetail.dataList[i].item_name,
                            transDetail.dataList[i].item_qty,
                            transDetail.dataList[i].satuan,
                            transDetail.dataList[i].item_price,
                            transDetail.dataList[i].subtotal,
                            transDetail.dataList[i].item_qty,
                            transDetail.dataList[i].item_hpp,
                            transDetail.dataList[i].item_jasa
                            );
                    }
                }
            }
            #endregion
        }

        private void loadStatusCombo()
        {
            cbDataSource cbDataSourceElement;
            cbDT.Clear();

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "undelivered";
            cbDataSourceElement.valueMember = "undelivered";
            cbDT.Add(cbDataSourceElement);

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "delivered";
            cbDataSourceElement.valueMember = "delivered";
            cbDT.Add(cbDataSourceElement);

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "cancel";
            cbDataSourceElement.valueMember = "cancel";
            cbDT.Add(cbDataSourceElement);

            statusCombo.DataSource = cbDT;
            statusCombo.DisplayMember = "displayMember";
            statusCombo.ValueMember = "valueMember";
        }

        private void dataTransaksiForm_Load(object sender, EventArgs e)
        {
            loadStatusCombo();
            dateDTPicker.Value = DateTime.Now.Date;

            if (selectedIDTrans > 0)
            {
                loadTransaksi();
                dateDTPicker.Enabled = false;
            }

            if (originModuleID == globalConstants.EDIT_ITEM)
            {
                labelHint.Text = "F4 : Pilih Barang | F5 : Reset | F9 : Save | F10 : Void | Del : Hapus Baris";
            }

            if (isViewOnly)
            {
                labelHint.Text = "";
                gUser.disableAllControls(this);
            }

            transGridView.CellValueChanged += CUSTOM_CellValueChanged;
            transGridView.DoubleBuffered(true);
        }

        private int getRowLineNo(string productIDParam)
        {
            int lineNo = -1;
            DataGridViewRow sRow;

            for (int i = 0; i < transGridView.Rows.Count && lineNo < 0; i++)
            {
                sRow = transGridView.Rows[i];

                if (sRow.Cells["itemID"].Value.ToString() == productIDParam &&
                    sRow.Cells["flagDelete"].Value.ToString() == "0")
                    lineNo = i;
            }

            return lineNo;
        }

        private void addRow(string productIDParam)
        {
            string sqlCommand = "";
            int lineNo = -1;

            REST_masterItem itemDetail = new REST_masterItem();

            lineNo = getRowLineNo(productIDParam);

            if (lineNo > -1)
            {
                transGridView.Rows[lineNo].Cells["itemQty"].Selected = true;
                return;
            }

            sqlCommand = "SELECT mi.*, mu.unit_name " +
                                    "FROM master_item mi, master_unit mu " +
                                    "WHERE mi.item_id = '" + productIDParam + "' " +
                                    "AND mi.unit_id = mu.unit_id";

            if (gRest.getMasterItemData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref itemDetail))
            {
                if (itemDetail.dataStatus.o_status == 1)
                {
                    transGridView.CellValueChanged -= CUSTOM_CellValueChanged;

                    transGridView.Rows.Add(
                        0,
                        0,
                        productIDParam,
                        itemDetail.dataList[0].item_name,
                        0,
                        itemDetail.dataList[0].unit_name,
                        itemDetail.dataList[0].item_price,
                        0,
                        0,
                        itemDetail.dataList[0].item_hpp,
                        itemDetail.dataList[0].item_jasa
                        );

                    transGridView.CellValueChanged += CUSTOM_CellValueChanged;
                }
            }

            calculateGlobalTotalValue();
        }

        private void displayProductForm()
        {
            dataBrowse_ItemForm displayForm = new dataBrowse_ItemForm(transModule);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                addRow(displayForm.ReturnValue1);
            }
        }

        private void transGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (transGridView.IsCurrentCellDirty)
            {
                transGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void transGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (transGridView.Rows.Count > 0)
                if (e.KeyCode == Keys.Delete)
                {
                    deleteCurrentRow();
                }
        }

        private void transGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if ((transGridView.Columns[e.ColumnIndex].Name == "itemQty" ||
                transGridView.Columns[e.ColumnIndex].Name == "itemPrice" ||
                transGridView.Columns[e.ColumnIndex].Name == "subtotal")
           && null != e.Value)
            {
                try
                {
                    double d = double.Parse(e.Value.ToString());
                    e.Value = d.ToString("N0", culture);
                }
                catch (Exception ex)
                {
                }
            }
        }

        private void deleteCurrentRow()
        {
            if (transGridView.Rows.Count > 0)
            {
                if (DialogResult.Yes == MessageBox.Show("Hapus baris ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    int rowIndex = transGridView.SelectedCells[0].RowIndex;
                    DataGridViewRow selectedRow = transGridView.Rows[rowIndex];

                    selectedRow.Cells["flagDelete"].Value = 1;
                    selectedRow.Visible = false;

                    calculateGlobalTotalValue();
                }
            }
        }

        private void calculateGlobalTotalValue()
        {
            globalTotalValue = 0;
            DataGridViewRow sRow;

            for (int i = 0; i < transGridView.Rows.Count; i++)
            {
                sRow = transGridView.Rows[i];

                if (sRow.Cells["flagDelete"].Value.ToString() == "0")
                {
                    globalTotalValue += Convert.ToDouble(sRow.Cells["subtotal"].Value);
                }
            }

            totalLabelValue.Text = globalTotalValue.ToString("C0", culture);
        }

        private void CUSTOM_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (transGridView.Rows.Count <= 0)
                return;

            var cell = transGridView[e.ColumnIndex, e.RowIndex];
            DataGridViewRow sRow = transGridView.Rows[e.RowIndex];

            double tempValue = 0;
            double qty = 0;
            double hpp = 0;

            string productID = sRow.Cells["itemID"].Value.ToString();
            int lineID = Convert.ToInt32(sRow.Cells["lineID"].Value);

            string columnName = transGridView.Columns[e.ColumnIndex].Name;

            if (columnName == "itemQty" || columnName == "itemPrice")
            {
                if (null == sRow.Cells[columnName].Value ||
                    sRow.Cells[columnName].Value.ToString().Length <= 0 ||
                    !double.TryParse(sRow.Cells[columnName].Value.ToString(), NumberStyles.Number, culture, out tempValue))
                {
                    errorLabel.Text = "Input " + sRow.Cells[columnName].OwningColumn.HeaderText + " untuk " + sRow.Cells["itemName"].Value.ToString() + " salah";
                }
                else
                {
                    errorLabel.Text = "";
                    transGridView.CellValueChanged -= CUSTOM_CellValueChanged;
                    cell.Value = tempValue;

                    if (null != sRow.Cells["itemQty"].Value)
                        double.TryParse(sRow.Cells["itemQty"].Value.ToString(), out qty);

                    if (null != sRow.Cells["itemPrice"].Value)
                        double.TryParse(sRow.Cells["itemPrice"].Value.ToString(), out hpp);

                    sRow.Cells["subtotal"].Value = Math.Round(qty * hpp, 2);

                    calculateGlobalTotalValue();

                    transGridView.CellValueChanged += CUSTOM_CellValueChanged;
                }
            }
        }

        private bool dataValid()
        {
            errorLabel.Text = "";

            if (selectedUserID == 0)
            {
                errorLabel.Text = "User belum dipilih";
                return false;
            }

            if (selectedKompleksID == 0)
            {
                errorLabel.Text = "Kompleks belum dipilih";
                return false;
            }

            if (selectedKavlingID == 0)
            {
                errorLabel.Text = "Blok belum dipilih";
                return false;
            }

            if (transGridView.Rows.Count <= 0)
            {
                errorLabel.Text = "Tidak ada item pada transaksi";
                return false;
            }

            if (globalTotalValue <= 0)
            {
                errorLabel.Text = "Nilai transaksi = 0 ";
                return false;
            }

            DataGridViewRow sRow;
            string[] colname = { "itemQty", "itemPrice" };
            double tempVal = 0;
            double oldQty = 0;
            string itemID = "";

            for (int i = 0;i<transGridView.Rows.Count;i++)
            {
                sRow = transGridView.Rows[i];

                if (sRow.Cells["flagDelete"].Value.ToString() == "0")
                {
                    for (int j = 0;j<colname.Length;j++)
                    {
                        if (null == sRow.Cells[colname[j]].Value ||
                            !double.TryParse(sRow.Cells[colname[j]].Value.ToString(), out tempVal))
                        {
                            errorLabel.Text = "Input " + sRow.Cells[colname[j]].OwningColumn.HeaderText + " Untuk " + sRow.Cells["itemName"].Value.ToString() + " Salah";
                            return false;
                        }
                    }

                    if (sRow.Cells["itemJasa"].Value.ToString() == "N")
                    {
                        itemID = sRow.Cells["itemID"].Value.ToString();
                        tempVal = Convert.ToDouble(sRow.Cells["itemQty"].Value);
                        oldQty = Convert.ToDouble(sRow.Cells["itemOldQty"].Value);
                        if (!gItem.qtyItemEnough(itemID, tempVal, oldQty))
                        {
                            errorLabel.Text = "Qty untuk " + sRow.Cells["itemName"].Value.ToString() + " tidak cukup";
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        private void returnQtyBecauseOfVoid()
        {
            gRest.returnQtyVoid(gUser.getUserID(), gUser.getUserToken(), selectedIDTrans, 1);
        }

        private bool saveTrans(out string errMsg)
        {
            bool result = false;
            int opType = 0;
            errMsg = "";

            List<transaksi_header> listHeader = new List<transaksi_header>();
            transaksi_header headerData;

            List<transaksi_detail> listDetail = new List<transaksi_detail>();
            transaksi_detail detailData;

            genericReply replyResult = new genericReply();
            try
            {
                #region HEADER
                headerData = new transaksi_header();
                switch (originModuleID)
                {
                    case globalConstants.NEW_ITEM:
                        opType = 1;
                        headerData.id_trans = 0;
                        break;

                    case globalConstants.EDIT_ITEM:
                        opType = 2;
                        headerData.id_trans = selectedIDTrans;
                        break;
                }

                headerData.type_trans = (transModule == globalConstants.MODULE_TRANSAKSI_RETAIL ? globalConstants.TRANS_TYPE_RETAIL : globalConstants.TRANS_TYPE_RETAIL_DRUGS);
                headerData.date_issued = new DateTime(dateDTPicker.Value.Year, dateDTPicker.Value.Month, dateDTPicker.Value.Day);
                headerData.kavling_id = selectedKavlingID;
                headerData.user_id = selectedUserID;
                headerData.nominal = globalTotalValue;
                headerData.deliver_status = statusCombo.SelectedValue.ToString();
                headerData.is_active = (transStatus == 1 ? "Y" : "N");

                listHeader.Add(headerData);
                #endregion

                #region DETAIL
                DataGridViewRow sRow;

                int flagDelete = 0;
                int lineID = 0;
                int itemID = 0;
                double itemPrice = 0;
                double itemHPP = 0;
                double itemQty = 0;
                double itemOldQty = 0;
                double subtotal = 0;
                string itemJasa = "N";
                int i = 0;

                for (i = 0; i < transGridView.Rows.Count && transStatus == 1; i++)
                {
                    sRow = transGridView.Rows[i];

                    flagDelete = Convert.ToInt32(sRow.Cells["flagDelete"].Value);
                    lineID = Convert.ToInt32(sRow.Cells["lineID"].Value);

                    itemID = Convert.ToInt32(sRow.Cells["itemID"].Value);
                    itemHPP = Convert.ToDouble(sRow.Cells["itemHpp"].Value);
                    itemPrice = Convert.ToDouble(sRow.Cells["itemPrice"].Value);
                    itemQty = Convert.ToDouble(sRow.Cells["itemQty"].Value);
                    subtotal = Convert.ToDouble(sRow.Cells["subtotal"].Value);
                    itemOldQty = Convert.ToDouble(sRow.Cells["itemOldQty"].Value);
                    itemJasa = sRow.Cells["itemJasa"].Value.ToString();

                    if (lineID > 0)
                    {
                        detailData = new transaksi_detail();
                        detailData.is_active = "Y";
                        detailData.old_qty = itemOldQty;
                        detailData.item_jasa = itemJasa;
                        detailData.id = lineID;

                        if (flagDelete == 1) // HAPUS BARIS
                        {
                            detailData.is_active = "N";
                        }
                    }
                    else
                    {
                        if (flagDelete == 0)
                        {
                            detailData = new transaksi_detail();
                            detailData.item_jasa = itemJasa;
                            detailData.is_active = "Y";
                            detailData.id = 0;
                        }
                        else
                            continue;
                    }

                    detailData.id_trans = selectedIDTrans;
                    detailData.item_id = itemID;
                    detailData.item_qty = itemQty;
                    detailData.item_hpp = itemHPP;
                    detailData.item_price = itemPrice;
                    detailData.subtotal = subtotal;
                    detailData.item_name = "";
                    detailData.satuan = "";

                    listDetail.Add(detailData);
                }
                #endregion

                int newID = 0;
                if (!gRest.saveDataTransaksi(gUser.getUserID(), gUser.getUserToken(), listHeader, listDetail, opType, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                if (originModuleID == globalConstants.NEW_ITEM)
                {
                    selectedIDTrans = newID;
                }

                if (transStatus == 0) // RETURN QTY
                {
                    returnQtyBecauseOfVoid();
                }

                result = true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
            }

            return result;
        }

        private void printTransaksi()
        {
            List<sqlFields> listSql = new List<sqlFields>();
            sqlFields sqlElement;

            sqlElement = new sqlFields();
            sqlElement.fieldName = "ID_TRANS";
            sqlElement.fieldValue = selectedIDTrans.ToString();
            listSql.Add(sqlElement);

            RPT_containerForm displayReportForm = new RPT_containerForm(null, globalReportConstants.PRINTOUT_TRANSAKSI_RETAIL, "", listSql);
            displayReportForm.ShowDialog(this);
        }

        private void resetScreen()
        {
            gUser.ResetAllControls(this);

            originModuleID = globalConstants.NEW_ITEM;

            transModule = globalConstants.MODULE_TRANSAKSI_RETAIL;
            isViewOnly = false;

            selectedIDTrans = 0;

            selectedUserID = 0;
            selectedKompleksID = 0;
            selectedKavlingID = 0;
            transStatus = 1;

            globalTotalValue = 0;
            totalLabelValue.Text = globalTotalValue.ToString("C0", culture);

            labelHint.Text = "F4 : Pilih Barang | F5 : Reset | F9 : Save | Del : Hapus Baris";
        }

        private void saveData()
        {
            bool result = false;
            string warningMsg = "Save transaksi ?";
            string errMsg = "";

            if (transStatus == 0)
                warningMsg = "Void transaksi ?";

            if (DialogResult.Yes == MessageBox.Show(warningMsg, "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {
                if (dataValid())
                {
                    result = saveTrans(out errMsg);
                }
            }

            if (result)
            {
                MessageBox.Show("Sucess");

                if (transStatus == 1)
                    if (DialogResult.Yes == MessageBox.Show("Cetak transaksi ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    {
                        printTransaksi();
                    }

                if (DialogResult.Yes == MessageBox.Show("Input lagi ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    resetScreen();
                else
                    this.Close();
            }
            else
            {
                MessageBox.Show("Fail " + errMsg);
            }
        }
    }
}

