﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class _reportFilterRangeDate : basicHotkeysForm
    {
        private Data_Access DS = new Data_Access();
        private int originModuleID = 0;

        private globalUserUtil gUser;

        public _reportFilterRangeDate(int moduleID)
        {
            InitializeComponent();

            originModuleID = moduleID;

            gUser = new globalUserUtil(DS);

            switch (originModuleID)
            {
            }
        }

        private void displayDefault()
        {
            string sqlCommand = "";

            List<sqlFields> listSql = new List<sqlFields>();
            sqlFields sqlElement;

            sqlElement = new sqlFields();
            sqlElement.fieldName = "START_DATE";
            sqlElement.fieldValue = new DateTime(startDTPicker.Value.Year, startDTPicker.Value.Month, startDTPicker.Value.Day, 0, 0, 0); //startDTPicker.Value;
            listSql.Add(sqlElement);

            sqlElement = new sqlFields();
            sqlElement.fieldName = "END_DATE";
            sqlElement.fieldValue = new DateTime(endDTPicker.Value.Year, endDTPicker.Value.Month, endDTPicker.Value.Day, 23, 59, 59); //endDTPicker.Value;
            listSql.Add(sqlElement);

            RPT_containerForm displayReportForm = new RPT_containerForm(null, originModuleID, sqlCommand, listSql);
            displayReportForm.ShowDialog(this);
        }

        private void displayButton_Click(object sender, EventArgs e)
        {
            switch (originModuleID)
            {
                default:
                    displayDefault();
                    break;
            }
        }

        private void _reportFilterRangeDate_Load(object sender, EventArgs e)
        {
            displayButton.PerformClick();
        }
    }
}
