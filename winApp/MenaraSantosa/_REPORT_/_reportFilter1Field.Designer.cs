﻿namespace AlphaSoft
{
    partial class _reportFilter1Field
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nonActiveCheckBox = new System.Windows.Forms.CheckBox();
            this.filterValueTextBox = new System.Windows.Forms.TextBox();
            this.displayButton = new System.Windows.Forms.Button();
            this.labelFilter = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(545, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // nonActiveCheckBox
            // 
            this.nonActiveCheckBox.AutoSize = true;
            this.nonActiveCheckBox.Location = new System.Drawing.Point(128, 82);
            this.nonActiveCheckBox.Name = "nonActiveCheckBox";
            this.nonActiveCheckBox.Size = new System.Drawing.Size(142, 17);
            this.nonActiveCheckBox.TabIndex = 41;
            this.nonActiveCheckBox.Text = "tampilkan data Non-Aktif";
            this.nonActiveCheckBox.UseVisualStyleBackColor = true;
            // 
            // filterValueTextBox
            // 
            this.filterValueTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filterValueTextBox.Location = new System.Drawing.Point(128, 49);
            this.filterValueTextBox.Name = "filterValueTextBox";
            this.filterValueTextBox.Size = new System.Drawing.Size(375, 27);
            this.filterValueTextBox.TabIndex = 40;
            // 
            // displayButton
            // 
            this.displayButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayButton.Location = new System.Drawing.Point(170, 122);
            this.displayButton.Name = "displayButton";
            this.displayButton.Size = new System.Drawing.Size(202, 58);
            this.displayButton.TabIndex = 39;
            this.displayButton.Text = "DISPLAY";
            this.displayButton.UseVisualStyleBackColor = true;
            this.displayButton.Click += new System.EventHandler(this.displayButton_Click);
            // 
            // labelFilter
            // 
            this.labelFilter.AutoSize = true;
            this.labelFilter.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFilter.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelFilter.Location = new System.Drawing.Point(11, 52);
            this.labelFilter.Name = "labelFilter";
            this.labelFilter.Size = new System.Drawing.Size(88, 18);
            this.labelFilter.TabIndex = 38;
            this.labelFilter.Text = "FILTER  :";
            // 
            // _reportFilter1Field
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(545, 216);
            this.Controls.Add(this.nonActiveCheckBox);
            this.Controls.Add(this.filterValueTextBox);
            this.Controls.Add(this.displayButton);
            this.Controls.Add(this.labelFilter);
            this.Name = "_reportFilter1Field";
            this.Load += new System.EventHandler(this._reportFilter1Field_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.labelFilter, 0);
            this.Controls.SetChildIndex(this.displayButton, 0);
            this.Controls.SetChildIndex(this.filterValueTextBox, 0);
            this.Controls.SetChildIndex(this.nonActiveCheckBox, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox nonActiveCheckBox;
        private System.Windows.Forms.TextBox filterValueTextBox;
        private System.Windows.Forms.Button displayButton;
        private System.Windows.Forms.Label labelFilter;
    }
}
