﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Globalization;
using System.IO;

namespace AlphaSoft
{
    public partial class _reportFilter1Date : AlphaSoft.basicHotkeysForm
    {
        private Data_Access DS;

        private int originModuleID = 0;
        private CultureInfo culture = new CultureInfo("id-ID");
        private string selectedValue = "0";

        private int selectedKompleksID = 0;

        private globalDBUtil gDB;

        public _reportFilter1Date(int moduleID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;

            switch(originModuleID)
            {
                case globalReportConstants.REPORT_IPL_TAHUNAN:
                    label2.Text = "TAHUN";

                    dateDTPicker.Format = DateTimePickerFormat.Custom;
                    dateDTPicker.CustomFormat = "yyyy";

                    labelFilter.Text = "KOMPLEKS";
                    showAllCheckBox.Visible = false;
                    break;
            }
        }

        private void _report1FilterDate_Load(object sender, EventArgs e)
        {
        }

        private void displayReport(int directPrint = 0)
        {
            string sqlCommand = "";

            List<sqlFields> listSql = new List<sqlFields>();
            sqlFields sqlElement;

            switch (originModuleID)
            {
                case globalReportConstants.REPORT_IPL_TAHUNAN:

                    sqlElement = new sqlFields();
                    sqlElement.fieldName = "kompleks_id";
                    sqlElement.fieldValue = selectedKompleksID;
                    listSql.Add(sqlElement);

                    sqlElement = new sqlFields();
                    sqlElement.fieldName = "periode";
                    sqlElement.fieldValue = dateDTPicker.Value.Year;
                    listSql.Add(sqlElement);
                    break;

                default:
                    break;
            }

            RPT_containerForm displayReportForm = new RPT_containerForm(null, originModuleID, sqlCommand, listSql);
            displayReportForm.ShowDialog(this);
        }

        private void displayButton_Click(object sender, EventArgs e)
        {
            displayReport();  
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            switch(originModuleID)
            {
                case globalReportConstants.REPORT_IPL_TAHUNAN:
                    dataBrowse_KompleksForm browseKompleks = new dataBrowse_KompleksForm(globalConstants.MODULE_DEFAULT);
                    browseKompleks.ShowDialog(this);
                    if (browseKompleks.ReturnValue1 != "0")
                    {
                        selectedKompleksID = Convert.ToInt32(browseKompleks.ReturnValue1);
                        filterValueTextBox.Text = browseKompleks.ReturnValue2;
                    }
                    break;
            }
        }

        private void showAllCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            browseButton.Enabled = !showAllCheckBox.Checked;
        }

        private void _reportFilter1Date_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (null != DS)
                DS.sqlClose();
        }
    }
    
}
