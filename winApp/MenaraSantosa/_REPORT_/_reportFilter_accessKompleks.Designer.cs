﻿namespace AlphaSoft
{
    partial class _reportFilter_accessKompleks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_reportFilter_accessKompleks));
            this.labelFilter = new System.Windows.Forms.Label();
            this.browseButton = new System.Windows.Forms.Button();
            this.kompleksTextBox = new System.Windows.Forms.TextBox();
            this.clearButton = new System.Windows.Forms.Button();
            this.qrCheckBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rfidCheckbox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBoxNonAktif = new System.Windows.Forms.CheckBox();
            this.checkBoxAktif = new System.Windows.Forms.CheckBox();
            this.displayButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(714, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // labelFilter
            // 
            this.labelFilter.AutoSize = true;
            this.labelFilter.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFilter.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelFilter.Location = new System.Drawing.Point(34, 53);
            this.labelFilter.Name = "labelFilter";
            this.labelFilter.Size = new System.Drawing.Size(105, 18);
            this.labelFilter.TabIndex = 103;
            this.labelFilter.Text = "KOMPLEKS";
            // 
            // browseButton
            // 
            this.browseButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("browseButton.BackgroundImage")));
            this.browseButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.browseButton.Location = new System.Drawing.Point(560, 49);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(25, 27);
            this.browseButton.TabIndex = 102;
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // kompleksTextBox
            // 
            this.kompleksTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kompleksTextBox.Location = new System.Drawing.Point(154, 49);
            this.kompleksTextBox.Name = "kompleksTextBox";
            this.kompleksTextBox.ReadOnly = true;
            this.kompleksTextBox.Size = new System.Drawing.Size(400, 27);
            this.kompleksTextBox.TabIndex = 101;
            this.kompleksTextBox.Text = "ALL";
            // 
            // clearButton
            // 
            this.clearButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearButton.Location = new System.Drawing.Point(591, 49);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(95, 27);
            this.clearButton.TabIndex = 106;
            this.clearButton.Text = "CLEAR";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // qrCheckBox
            // 
            this.qrCheckBox.AutoSize = true;
            this.qrCheckBox.Checked = true;
            this.qrCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.qrCheckBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qrCheckBox.Location = new System.Drawing.Point(154, 94);
            this.qrCheckBox.Name = "qrCheckBox";
            this.qrCheckBox.Size = new System.Drawing.Size(51, 22);
            this.qrCheckBox.TabIndex = 127;
            this.qrCheckBox.Text = "QR";
            this.qrCheckBox.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(26, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 18);
            this.label1.TabIndex = 128;
            this.label1.Text = "TIPE AKSES";
            // 
            // rfidCheckbox
            // 
            this.rfidCheckbox.AutoSize = true;
            this.rfidCheckbox.Checked = true;
            this.rfidCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rfidCheckbox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rfidCheckbox.Location = new System.Drawing.Point(211, 94);
            this.rfidCheckbox.Name = "rfidCheckbox";
            this.rfidCheckbox.Size = new System.Drawing.Size(66, 22);
            this.rfidCheckbox.TabIndex = 129;
            this.rfidCheckbox.Text = "RFID";
            this.rfidCheckbox.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(60, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 18);
            this.label2.TabIndex = 130;
            this.label2.Text = "STATUS";
            // 
            // checkBoxNonAktif
            // 
            this.checkBoxNonAktif.AutoSize = true;
            this.checkBoxNonAktif.Checked = true;
            this.checkBoxNonAktif.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxNonAktif.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxNonAktif.Location = new System.Drawing.Point(225, 132);
            this.checkBoxNonAktif.Name = "checkBoxNonAktif";
            this.checkBoxNonAktif.Size = new System.Drawing.Size(115, 22);
            this.checkBoxNonAktif.TabIndex = 132;
            this.checkBoxNonAktif.Text = "Tidak Aktif";
            this.checkBoxNonAktif.UseVisualStyleBackColor = true;
            // 
            // checkBoxAktif
            // 
            this.checkBoxAktif.AutoSize = true;
            this.checkBoxAktif.Checked = true;
            this.checkBoxAktif.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAktif.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxAktif.Location = new System.Drawing.Point(154, 132);
            this.checkBoxAktif.Name = "checkBoxAktif";
            this.checkBoxAktif.Size = new System.Drawing.Size(65, 22);
            this.checkBoxAktif.TabIndex = 131;
            this.checkBoxAktif.Text = "Aktif";
            this.checkBoxAktif.UseVisualStyleBackColor = true;
            // 
            // displayButton
            // 
            this.displayButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayButton.Location = new System.Drawing.Point(154, 172);
            this.displayButton.Name = "displayButton";
            this.displayButton.Size = new System.Drawing.Size(245, 55);
            this.displayButton.TabIndex = 133;
            this.displayButton.Text = "DISPLAY";
            this.displayButton.UseVisualStyleBackColor = true;
            this.displayButton.Click += new System.EventHandler(this.displayButton_Click);
            // 
            // _reportFilter_accessKompleks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(714, 285);
            this.Controls.Add(this.displayButton);
            this.Controls.Add(this.checkBoxNonAktif);
            this.Controls.Add(this.checkBoxAktif);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rfidCheckbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.qrCheckBox);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.labelFilter);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.kompleksTextBox);
            this.Name = "_reportFilter_accessKompleks";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.kompleksTextBox, 0);
            this.Controls.SetChildIndex(this.browseButton, 0);
            this.Controls.SetChildIndex(this.labelFilter, 0);
            this.Controls.SetChildIndex(this.clearButton, 0);
            this.Controls.SetChildIndex(this.qrCheckBox, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.rfidCheckbox, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.checkBoxAktif, 0);
            this.Controls.SetChildIndex(this.checkBoxNonAktif, 0);
            this.Controls.SetChildIndex(this.displayButton, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelFilter;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.TextBox kompleksTextBox;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.CheckBox qrCheckBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox rfidCheckbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBoxNonAktif;
        private System.Windows.Forms.CheckBox checkBoxAktif;
        private System.Windows.Forms.Button displayButton;
    }
}
