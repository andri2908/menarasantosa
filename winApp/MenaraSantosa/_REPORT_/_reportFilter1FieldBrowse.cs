﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Globalization;

namespace AlphaSoft
{
    public partial class _reportFilter1FieldBrowse : AlphaSoft.basicHotkeysForm
    {
        private int originModuleID = 0;
        private CultureInfo culture = new CultureInfo("id-ID");
        private string selectedID = "";

        public _reportFilter1FieldBrowse(int moduleID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;

            switch (originModuleID)
            {
                case globalReportConstants.REPORT_KAVLING:
                    labelFilter.Text = "Kompleks";

                    showAllCheckBox.Visible = false;
                    break;
            }
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            string retVal1 = "";
            string retVal2 = "";

            switch (originModuleID)
            {
                case globalReportConstants.REPORT_KAVLING:
                    dataBrowse_KompleksForm displayKompleks = new dataBrowse_KompleksForm(globalConstants.MODULE_DEFAULT);
                    displayKompleks.ShowDialog(this);

                    if (displayKompleks.ReturnValue1 != "0")
                    {
                        retVal1 = displayKompleks.ReturnValue1;
                        retVal2 = displayKompleks.ReturnValue2;
                    }
                    break;
            }

            selectedID = retVal1;
            filterValueTextBox.Text = retVal2;
        }

        private void displayButton_Click(object sender, EventArgs e)
        {
            string sqlCommand = "";

            List<sqlFields> listSql = new List<sqlFields>();
            sqlFields sqlElement;

            switch (originModuleID)
            {
                case globalReportConstants.REPORT_KAVLING:
                    sqlElement = new sqlFields();
                    sqlElement.fieldName = "kompleks_id";
                    sqlElement.fieldValue = selectedID;

                    listSql.Add(sqlElement);
                    break;

                default:
                    break;
            }

            RPT_containerForm displayReportForm = new RPT_containerForm(null, originModuleID, sqlCommand, listSql);
            displayReportForm.ShowDialog(this);
        }
    }
}
