﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Globalization;
using System.IO;
using DBGridExtension;

namespace AlphaSoft
{
    public partial class _reportFilterRangeDateBrowse : AlphaSoft.basicHotkeysForm
    {
        private Data_Access DS;

        private int originModuleID = 0;
        private CultureInfo culture = new CultureInfo("id-ID");
        private string selectedValue = "0";

        private globalDBUtil gDB;
        private globalUserUtil gUser;

        private List<string> pList = new List<string>();
        private List<object> pVal = new List<object>();

        public _reportFilterRangeDateBrowse(int moduleID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;

            DS = new Data_Access();

            DS.sqlConnect();

            gDB = new globalDBUtil(DS);
            gUser = new globalUserUtil(DS);
        }

        private void displayDefault()
        {
            string sqlCommand = "";
            MySqlDataReader rdr;
            DataTable dt = new DataTable();
            string tableName = "";
        }

        private void displayLogData()
        {
            DateTime startDate = startDTPicker.Value.Date;
            DateTime endDate = endDTPicker.Value.Date;

            int groupUserID = gUser.getUserGroupID();
            string sqlCommand = "";
            string sqlModuleAccess = "";

            List<string> pList = new List<string>();
            List<object> pVal = new List<object>();

            MySqlDataReader rdr;
            DataTable dt = new DataTable();

            sqlModuleAccess = "SELECT MODULE_ID FROM MASTER_MODULE_ACCESS " +
                                            "WHERE GROUP_ID = " + groupUserID + " " +
                                            "AND USER_ACCESS_OPTION > 0";

            sqlCommand = "SELECT LOG.LOG_DATETIME AS TGL, IFNULL(MM.MODULE_NAME, '') AS 'MODULE', IFNULL(MU.USER_NAME, '') AS USER, LOG.LOG_MSG AS LOG " +
                                    "FROM SYS_LOG_MSG LOG " +
                                    "LEFT OUTER JOIN MASTER_MODULE MM ON (LOG.LOG_REF_ID = MM.MODULE_ID) " +
                                    "LEFT OUTER JOIN MASTER_USER MU ON (LOG.USER_ID = MU.ID) " +
                                    "WHERE LOG.LOG_REF_ID IN (" + sqlModuleAccess + ") " +
                                    "AND DATE(LOG_DATETIME) >= @dtStart " +
                                    "AND DATE(LOG_DATETIME) <= @dtEnd " +
                                    "ORDER BY LOG_DATETIME ASC";

            pList.Add("@dtStart");
            pList.Add("@dtEnd");

            pVal.Add(startDate);
            pVal.Add(endDate);

            dbGridView.DataSource = null;
            using (rdr = DS.getDataWithParam(sqlCommand, pList, pVal))
            {
                if (rdr.HasRows)
                {
                    dt.Load(rdr);
                    dbGridView.DataSource = dt;
                }
            }
            rdr.Close();

        }

        private void displayButton_Click(object sender, EventArgs e)
        {
            switch (originModuleID)
            {
                default:
                    displayDefault();
                    break;
            }
        }    

        private void dbClickDefault()
        {
            List<sqlFields> listSql = new List<sqlFields>();
            sqlFields sqlElement;

            string periodeValue = "";
            string dateRange = "";

            if (dbGridView.Rows.Count <= 0)
                return;

            int rowIndex = dbGridView.SelectedCells[0].RowIndex;
            DataGridViewRow sRow = dbGridView.Rows[rowIndex];

            switch (originModuleID)
            {
                default:
                    sqlElement = new sqlFields();
                    sqlElement.fieldName = "DATEPARAM";
                    sqlElement.fieldValue = periodeValue;
                    listSql.Add(sqlElement);

                    sqlElement = new sqlFields();
                    sqlElement.fieldName = "PERIODE";
                    sqlElement.fieldValue = dateRange;
                    listSql.Add(sqlElement);

                    RPT_containerForm displayReportForm = new RPT_containerForm(null, originModuleID, "", listSql);
                    displayReportForm.ShowDialog(this);
                    break;
            }
        }

        private void dataGridView_DoubleClick(object sender, EventArgs e)
        {
            switch(originModuleID)
            {
                default:
                    dbClickDefault();
                    break;
            }
        }

        private void _reportFilterRangeDateBrowse_Load(object sender, EventArgs e)
        {
            switch(originModuleID)
            {
                default:
                    break;
            }


            dbGridView.DoubleBuffered(true);

            displayButton.PerformClick();
        }
    }
}
