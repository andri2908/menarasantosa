﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using System.Globalization;
using CrystalDecisions.ReportSource;
using CrystalDecisions.CrystalReports.Engine;

using AlphaSoft._REPORT_;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace AlphaSoft
{
    partial class RPT_containerForm : Form
    {
        private Data_Access DS;
        private globalUserUtil gUser;
        private globalDBUtil gDB;
        private globalIPL gIPL;
        private globalRestReport gRest = new globalRestReport();
        private CultureInfo culture = new CultureInfo("id-ID");

        private List<sqlFields> sqlFieldsValue;
        private int originModuleID = 0;
        private string xmlReportName = "";
        private string defaultSqlCommand = "";
        private string dateTo, dateFrom;
        private string report_month = "";
        private string appPath = "";

        private int[] arrQuarterKuarto =
        {
       
        };

        private int[] arrHalfKuarto =
        {       
        };

        private int[] arrKartuStok =
        {
        };

        private int[] arrNoWriteXML =
        {
        };

        private int[] arrLangsungPrint =
        {
        };

        private int[] arrCheckPreview =
        {
        };

        private int[] arrSkipCompleteHeader =
        {
            globalReportConstants.PRINTOUT_IPL,
            globalReportConstants.PRINTOUT_TRANSAKSI_RETAIL,
            globalReportConstants.PRINTOUT_TRANSAKSI_PENERIMAAN,
            globalReportConstants.PRINTOUT_TAGIHAN_IPL_MANUAL,
            globalReportConstants.PRINTOUT_PENGIRIMAN_TRANSAKSI,
            globalReportConstants.REPORT_SUMMARY_IPL,
            globalReportConstants.REPORT_IPL_TAHUNAN,
            globalReportConstants.REPORT_AKSES_KOMPLEKS,
            globalReportConstants.REPORT_KAVLING
        };

        private int[] arrNoHeader =
        {
            globalReportConstants.PRINTOUT_IPL,
            globalReportConstants.PRINTOUT_TRANSAKSI_RETAIL,
            globalReportConstants.PRINTOUT_TRANSAKSI_PENERIMAAN,
            globalReportConstants.PRINTOUT_TAGIHAN_IPL_MANUAL,
            globalReportConstants.PRINTOUT_PENGIRIMAN_TRANSAKSI,
            globalReportConstants.REPORT_SUMMARY_IPL,
            globalReportConstants.REPORT_IPL_TAHUNAN,
            globalReportConstants.REPORT_AKSES_KOMPLEKS,
            globalReportConstants.REPORT_KAVLING
        };

        private void RS_writeXML(string sqlCommand, string xmlReportName)
        {

        }

        private void generateXML_printoutIPL(out string sqlCommand)
        {
            string IDParam = "";
            sqlFields sqlFieldsElement;
       
            REST_printoutTransaksi restTrans = new REST_printoutTransaksi();

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "ID_TRANS");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    IDParam = sqlFieldsElement.fieldValue.ToString();
                }
            }

            sqlCommand = "SELECT 'PEMBAYARAN IPL' AS title, " +
                                    "CONCAT(IFNULL(mo.kompleks_name, ''), ' ', IFNULL(mb.blok_name, ''), '-', IFNULL(mk.house_no, '')) AS nama_kavling, " +
                                    "CONCAT('PEMBAYARAN IPL ', IFNULL(mo.kompleks_name, ''), ' ', IFNULL(mb.blok_name, ''), '-', IFNULL(mk.house_no, '')) AS item_1, " +
                                    "CONCAT(DATE_FORMAT(h.start_ipl, '%M %Y'), ' - ', DATE_FORMAT(h.end_ipl, '%M %Y'), IF(h.status_id = 2, ' (LUNAS)', ' (BELUM LUNAS)')) AS ITEM_2, " +
                                    "h.nominal AS harga, 1 AS qty, " +
                                    "h.nominal, ul.user_name, ul.user_full_name, ul.user_phone_1 " +
                                    "FROM transaksi_ipl h " +
                                    "LEFT OUTER JOIN user_login_data ul ON (h.user_id = ul.user_id) " +
                                    "LEFT OUTER JOIN master_kavling mk ON (h.kavling_id= mk.kavling_id) " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id) " +
                                    "LEFT OUTER JOIN master_kompleks mo ON (mk.kompleks_id = mo.kompleks_id) " +
                                    "WHERE h.is_active = 'Y' ";

            if (IDParam != "")
            {
                sqlCommand += "AND h.id_trans = " + IDParam;
            }

            if (gRest.generatePrintOutTransaksiIPL(sqlCommand, ref restTrans))
            {
                DataTable dt = new DataTable("Table");

                dt.Columns.Add("title");
                dt.Columns.Add("user_full_name");
                dt.Columns.Add("user_name");
                dt.Columns.Add("nama_kavling");
                dt.Columns.Add("user_phone_1");
                dt.Columns.Add("item_1");
                dt.Columns.Add("item_2");
                dt.Columns.Add("qty", typeof(double));
                dt.Columns.Add("harga", typeof(double));
                dt.Columns.Add("nominal", typeof(double));

                for (int i = 0; i < restTrans.dataList.Count; i++)
                {
                    DataRow r = dt.Rows.Add();

                    r["title"] = restTrans.dataList[i].title;
                    r["user_full_name"] = restTrans.dataList[i].user_full_name;
                    r["user_name"] = restTrans.dataList[i].user_name;
                    r["nama_kavling"] = restTrans.dataList[i].nama_kavling;
                    r["user_phone_1"] = restTrans.dataList[i].user_phone_1;
                    r["item_1"] = restTrans.dataList[i].item_1;
                    r["item_2"] = restTrans.dataList[i].item_2;
                    r["qty"] = restTrans.dataList[i].qty;
                    r["harga"] = restTrans.dataList[i].harga;
                    r["nominal"] = restTrans.dataList[i].nominal;
                }

                string appPath = Directory.GetCurrentDirectory() + "\\xml\\" + xmlReportName;

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                using (StreamWriter fs = new StreamWriter(appPath)) // XML File Path
                {
                    ds.WriteXml(fs, XmlWriteMode.WriteSchema);
                }
            }
        }

        private void generateXML_printoutTransaksiRetail(out string sqlCommand)
        {
            string IDParam = "";
            sqlFields sqlFieldsElement;
            REST_printoutTransaksi restTrans = new REST_printoutTransaksi();

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "ID_TRANS");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    IDParam = sqlFieldsElement.fieldValue.ToString();
                }
            }

            sqlCommand = "SELECT 'TRANSAKSI RETAIL' AS title, " +
                                    "CONCAT(IFNULL(mo.kompleks_name, ''), ' ', IFNULL(mb.blok_name, ''), '-', IFNULL(mk.house_no, '')) AS nama_kavling, " +
                                    "CONCAT(mi.item_name, ' (', mu.unit_name, ')') AS item_1, " +
                                    "'' AS item_2, " +
                                    "d.item_price AS harga, d.item_qty AS qty, " +
                                    "d.subtotal AS nominal, ul.user_name, ul.user_full_name, ul.user_phone_1 " +
                                    "FROM transaksi_retail h " +
                                    "LEFT OUTER JOIN user_login_data ul ON (h.user_id = ul.user_id) " +
                                    "LEFT OUTER JOIN master_kavling mk ON (h.kavling_id = mk.kavling_id) " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id) " +
                                    "LEFT OUTER JOIN master_kompleks mo ON (mk.kompleks_id = mo.kompleks_id), " +
                                    "transaksi_retail_detail d " +
                                    "LEFT OUTER JOIN master_item mi ON (d.item_id = mi.item_id) " +
                                    "LEFT OUTER JOIN master_unit mu ON (mi.unit_id = mu.unit_id) " +
                                    "WHERE d.id_trans = h.id_trans " +
                                    "AND h.is_active = 'Y' AND d.is_active = 'Y' ";

            if (IDParam != "")
            {
                sqlCommand += "AND h.id_trans = " + IDParam;
            }

            if (gRest.generatePrintOutTransaksiIPL(sqlCommand, ref restTrans))
            {
                DataTable dt = new DataTable("Table");

                dt.Columns.Add("title");
                dt.Columns.Add("user_full_name");
                dt.Columns.Add("user_name");
                dt.Columns.Add("nama_kavling");
                dt.Columns.Add("user_phone_1");
                dt.Columns.Add("item_1");
                dt.Columns.Add("item_2");
                dt.Columns.Add("qty", typeof(double));
                dt.Columns.Add("harga", typeof(double));
                dt.Columns.Add("nominal", typeof(double));

                for (int i = 0; i < restTrans.dataList.Count; i++)
                {
                    DataRow r = dt.Rows.Add();

                    r["title"] = restTrans.dataList[i].title;
                    r["user_full_name"] = restTrans.dataList[i].user_full_name;
                    r["user_name"] = restTrans.dataList[i].user_name;
                    r["nama_kavling"] = restTrans.dataList[i].nama_kavling;
                    r["user_phone_1"] = restTrans.dataList[i].user_phone_1;
                    r["item_1"] = restTrans.dataList[i].item_1;
                    r["item_2"] = restTrans.dataList[i].item_2;
                    r["qty"] = restTrans.dataList[i].qty;
                    r["harga"] = restTrans.dataList[i].harga;
                    r["nominal"] = restTrans.dataList[i].nominal;
                }

                string appPath = Directory.GetCurrentDirectory() + "\\xml\\" + xmlReportName;

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                using (StreamWriter fs = new StreamWriter(appPath)) // XML File Path
                {
                    ds.WriteXml(fs, XmlWriteMode.WriteSchema);
                }
            }
        }

        private void generateXML_printoutTransaksiPenerimaan(out string sqlCommand)
        {
            string IDParam = "";
            sqlFields sqlFieldsElement;
            REST_printoutTransaksi restTrans = new REST_printoutTransaksi();
            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "PENERIMAAN_ID");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    IDParam = sqlFieldsElement.fieldValue.ToString();
                }
            }

            sqlCommand = "SELECT 'TRANSAKSI PENERIMAAN' AS title, " +
                                    "h.penerimaan_datetime, " +
                                    "CONCAT(mi.item_name, ' (', mu.unit_name, ')') AS item_1, " +
                                    "'' AS item_2, " +
                                    "d.item_hpp AS harga, d.item_qty AS qty, " +
                                    "d.item_subtotal AS nominal " +
                                    "FROM penerimaan_header h, " +
                                    "penerimaan_detail d " +
                                    "LEFT OUTER JOIN master_item mi ON (d.item_id = mi.item_id) " +
                                    "LEFT OUTER JOIN master_unit mu ON (mi.unit_id = mu.unit_id) " +
                                    "WHERE d.penerimaan_id = h.penerimaan_id " +
                                    "AND h.is_active = 'Y' AND d.is_active = 'Y' ";

            if (IDParam != "")
            {
                sqlCommand += "AND h.penerimaan_id = " + IDParam;
            }

            if (gRest.generatePrintOutTransaksiIPL(sqlCommand, ref restTrans))
            {
                DataTable dt = new DataTable("Table");

                dt.Columns.Add("title");
                dt.Columns.Add("user_full_name");
                dt.Columns.Add("user_name");
                dt.Columns.Add("nama_kavling");
                dt.Columns.Add("user_phone_1");
                dt.Columns.Add("item_1");
                dt.Columns.Add("item_2");
                dt.Columns.Add("qty", typeof(double));
                dt.Columns.Add("harga", typeof(double));
                dt.Columns.Add("nominal", typeof(double));

                for (int i = 0; i < restTrans.dataList.Count; i++)
                {
                    DataRow r = dt.Rows.Add();

                    r["title"] = restTrans.dataList[i].title;
                    r["user_full_name"] = restTrans.dataList[i].user_full_name;
                    r["user_name"] = restTrans.dataList[i].user_name;
                    r["nama_kavling"] = restTrans.dataList[i].nama_kavling;
                    r["user_phone_1"] = restTrans.dataList[i].user_phone_1;
                    r["item_1"] = restTrans.dataList[i].item_1;
                    r["item_2"] = restTrans.dataList[i].item_2;
                    r["qty"] = restTrans.dataList[i].qty;
                    r["harga"] = restTrans.dataList[i].harga;
                    r["nominal"] = restTrans.dataList[i].nominal;
                }

                string appPath = Directory.GetCurrentDirectory() + "\\xml\\" + xmlReportName;

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                using (StreamWriter fs = new StreamWriter(appPath)) // XML File Path
                {
                    ds.WriteXml(fs, XmlWriteMode.WriteSchema);
                }
            }
        }

        private void generateXML_printoutTagihanIPL(out string sqlCommand)
        {
            string IDParam = "";
            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now;

            string startDateStr = "";
            string endDateStr = "";

            sqlFields sqlFieldsElement;
            REST_tagihanIPLManual restTrans = new REST_tagihanIPLManual();

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "KAVLING_ID");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    IDParam = sqlFieldsElement.fieldValue.ToString();
                }
            }

            string masterKavling = "";
            if (IDParam.Length > 0)
            {
                masterKavling = "AND mk.kavling_id = " + IDParam + " ";
            }


            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "START_DATE");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    startDate = Convert.ToDateTime(sqlFieldsElement.fieldValue);
                    startDateStr = String.Format(culture, "{0:yyyyMMdd}", startDate);
                }
            }

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "END_DATE");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    endDate = Convert.ToDateTime(sqlFieldsElement.fieldValue);
                    endDateStr = String.Format(culture, "{0:yyyyMMdd}", endDate);
                }
            }

            string sqlSingleUser = "SELECT uk.kavling_id, uk.user_id, uk.tgl_bast, uk.start_ipl " +
                                            "FROM user_kavling uk, master_kavling mk " +
                                            "WHERE uk.is_active = 'Y' " +
                                            "AND mk.is_active = 'Y' " +
                                            "AND uk.kavling_id = mk.kavling_id " +
                                            masterKavling +
                                            "GROUP BY uk.kavling_id, uk.user_id, uk.tgl_bast, uk.start_ipl " +
                                            "HAVING COUNT(1) <= 1 ";

            string sqlMultiUser = "SELECT uk.kavling_id, uk.user_id, uk.tgl_bast, uk.start_ipl " +
                                            "FROM user_kavling uk, user_login_data ul, master_group mg, master_kavling mk " +
                                            "WHERE uk.is_active = 'Y' " +
                                            "AND ul.is_active = 'Y' " +
                                            "AND mk.is_active = 'Y' " +
                                            "AND uk.kavling_id = mk.kavling_id " +
                                            masterKavling +
                                            "AND uk.user_id = ul.user_id " +
                                            "AND ul.group_id = mg.group_id " +
                                            "AND mg.group_type = 1 " +
                                            "GROUP BY uk.kavling_id, uk.user_id, uk.tgl_bast, uk.start_ipl " +
                                            "HAVING COUNT(1) > 1";

            string sqlUser = "SELECT tab.* " +
                                    "FROM (" + sqlSingleUser + " UNION ALL " + sqlMultiUser + ") tab";

            string oustandingIPL = "SELECT mk.kavling_id " +
                                                "FROM transaksi_ipl mk " +
                                                "WHERE mk.status_id = 0 " +
                                                "AND DATE_FORMAT(mk.start_ipl, '%Y%m%d') > '" + startDateStr + "' " +
                                                "AND DATE_FORMAT(mk.start_ipl, '%Y%m%d') < '" + endDateStr + "' " +
                                                masterKavling +
                                                "GROUP BY mk.kavling_id " +
                                                "HAVING count(1) > 0";

            sqlCommand = "SELECT ipl.kavling_id, ipl.nominal, ipl.start_ipl, ipl.end_ipl, ipl.status_id, usr.user_id, usr.tgl_bast, usr.start_ipl as usr_start_ipl, " +
                                    "mb.blok_name, mv.house_no, mk.kompleks_name, ul.user_name, ul.user_full_name, mv.luas_tanah, mv.luas_bangunan " +
                                    "FROM transaksi_ipl ipl, " +
                                    "(" + sqlUser + ") usr, " +
                                    "(" + oustandingIPL + ") outStr, " +
                                    "master_kavling mv, master_blok mb, master_kompleks mk, user_login_data ul " +
                                    "WHERE DATE_FORMAT(ipl.start_ipl, '%Y%m%d') > '" + startDateStr + "' " +
                                    "AND DATE_FORMAT(ipl.start_ipl, '%Y%m%d') < '" + endDateStr + "' " +
                                    "AND ipl.kavling_id = outStr.kavling_id " +
                                    "AND ipl.kavling_id = usr.kavling_id " +
                                    "AND ipl.kavling_id = mv.kavling_id " +
                                    "AND ipl.is_active = 'Y' " +
                                    "AND usr.kavling_id = mv.kavling_id " +
                                    "AND mv.blok_id = mb.blok_id " +
                                    "AND mv.is_active = 'Y' " +
                                    "AND mv.kompleks_id = mk.kompleks_id " +
                                    "AND mk.is_active = 'Y' " +
                                    "AND ul.is_active = 'Y' " +
                                    "AND usr.user_id = ul.user_id ";

            if (IDParam != "")
            {
                sqlCommand += "AND ipl.kavling_id = " + IDParam;
            }

            if (gRest.generateTagihanIPL(sqlCommand, ref restTrans))
            {
                double totalBulan = 0;
                double nominalPerBulan = 0;
                DataTable dt = new DataTable("Table");

                dt.Columns.Add("kavling_id");
                dt.Columns.Add("kompleks_name");
                dt.Columns.Add("blok_name");
                dt.Columns.Add("house_no");
                dt.Columns.Add("luas_tanah", typeof(double));
                dt.Columns.Add("luas_bangunan", typeof(double));
                dt.Columns.Add("user_id");
                dt.Columns.Add("user_full_name");
                dt.Columns.Add("user_name");
                dt.Columns.Add("tgl_bast", typeof(DateTime));
                dt.Columns.Add("usr_start_ipl");
                dt.Columns.Add("nominal", typeof(double));
                dt.Columns.Add("nominal_per_bulan", typeof(double));
                dt.Columns.Add("total_bulan", typeof(double));
                dt.Columns.Add("status_id", typeof(int));
                dt.Columns.Add("start_ipl", typeof(DateTime));
                dt.Columns.Add("end_ipl", typeof(DateTime));
                dt.Columns.Add("tgl_bast_string");
                dt.Columns.Add("ipl_periode_string");

                for (int i = 0; i < restTrans.dataList.Count; i++)
                {
                    DataRow r = dt.Rows.Add();

                    totalBulan = ((restTrans.dataList[i].end_ipl.Year - restTrans.dataList[i].start_ipl.Year) * 12) + (restTrans.dataList[i].end_ipl.Month - restTrans.dataList[i].start_ipl.Month) + 1;
                    nominalPerBulan = restTrans.dataList[i].nominal / totalBulan;

                    r["kavling_id"] = restTrans.dataList[i].kavling_id;
                    r["kompleks_name"] = restTrans.dataList[i].kompleks_name;
                    r["blok_name"] = restTrans.dataList[i].blok_name;
                    r["house_no"] = restTrans.dataList[i].house_no;
                    r["luas_tanah"] = restTrans.dataList[i].luas_tanah;
                    r["luas_bangunan"] = restTrans.dataList[i].luas_bangunan;
                    r["user_id"] = restTrans.dataList[i].user_id;
                    r["user_full_name"] = restTrans.dataList[i].user_full_name;
                    r["user_name"] = restTrans.dataList[i].user_name;
                    r["tgl_bast"] = restTrans.dataList[i].tgl_bast;
                    r["usr_start_ipl"] = String.Format(culture, "{0:dd MMMM yyyy}", restTrans.dataList[i].usr_start_ipl);
                    r["nominal"] = restTrans.dataList[i].nominal;
                    r["nominal_per_bulan"] = nominalPerBulan;
                    r["total_bulan"] = totalBulan;
                    r["status_id"] = restTrans.dataList[i].status_id;
                    r["start_ipl"] = restTrans.dataList[i].start_ipl;
                    r["end_ipl"] = restTrans.dataList[i].end_ipl;
                    r["tgl_bast_string"] = String.Format(culture, "{0:dd MMMM yyyy}", restTrans.dataList[i].tgl_bast);
                    r["ipl_periode_string"] = String.Format(culture, "{0:MMMM yyyy}", restTrans.dataList[i].start_ipl) + " - " + String.Format(culture, "{0:MMMM yyyy}", restTrans.dataList[i].end_ipl);
                }

                string appPath = Directory.GetCurrentDirectory() + "\\xml\\" + xmlReportName;

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                using (StreamWriter fs = new StreamWriter(appPath)) // XML File Path
                {
                    ds.WriteXml(fs, XmlWriteMode.WriteSchema);
                }
            }

            //if (gRest.generatePrintOutTransaksiIPL(sqlCommand, ref restTrans))
            //{
            //    DataTable dt = new DataTable("Table");

            //    dt.Columns.Add("title");
            //    dt.Columns.Add("user_full_name");
            //    dt.Columns.Add("user_name");
            //    dt.Columns.Add("nama_kavling");
            //    dt.Columns.Add("user_phone_1");
            //    dt.Columns.Add("item_1");
            //    dt.Columns.Add("item_2");
            //    dt.Columns.Add("qty", typeof(double));
            //    dt.Columns.Add("harga", typeof(double));
            //    dt.Columns.Add("nominal", typeof(double));

            //    for (int i = 0; i < restTrans.dataList.Count; i++)
            //    {
            //        DataRow r = dt.Rows.Add();

            //        r["title"] = restTrans.dataList[i].title;
            //        r["user_full_name"] = restTrans.dataList[i].user_full_name;
            //        r["user_name"] = restTrans.dataList[i].user_name;
            //        r["nama_kavling"] = restTrans.dataList[i].nama_kavling;
            //        r["user_phone_1"] = restTrans.dataList[i].user_phone_1;
            //        r["item_1"] = restTrans.dataList[i].item_1;
            //        r["item_2"] = restTrans.dataList[i].item_2;
            //        r["qty"] = restTrans.dataList[i].qty;
            //        r["harga"] = restTrans.dataList[i].harga;
            //        r["nominal"] = restTrans.dataList[i].nominal;
            //    }

            //    string appPath = Directory.GetCurrentDirectory() + "\\xml\\" + xmlReportName;

            //    DataSet ds = new DataSet();
            //    ds.Tables.Add(dt);
            //    using (StreamWriter fs = new StreamWriter(appPath)) // XML File Path
            //    {
            //        ds.WriteXml(fs, XmlWriteMode.WriteSchema);
            //    }
            //}
        }

        private void generatePRINTOUTXml()
        {
            string sqlCommand = "";

            List<string> paramList = new List<string>();
            List<object> paramValues = new List<object>();

            switch (originModuleID)
            {
                case globalReportConstants.PRINTOUT_IPL:
                    xmlReportName = globalReportConstants.transaksiXML;
                    generateXML_printoutIPL(out sqlCommand);
                    break;

                case globalReportConstants.PRINTOUT_TRANSAKSI_RETAIL:
                case globalReportConstants.PRINTOUT_PENGIRIMAN_TRANSAKSI:
                    xmlReportName = globalReportConstants.transaksiXML;
                    generateXML_printoutTransaksiRetail(out sqlCommand);
                    break;

                case globalReportConstants.PRINTOUT_TRANSAKSI_PENERIMAAN:
                    xmlReportName = globalReportConstants.transaksiXML;
                    generateXML_printoutTransaksiPenerimaan(out sqlCommand);
                    break;

                case globalReportConstants.PRINTOUT_TAGIHAN_IPL_MANUAL:
                    xmlReportName = globalReportConstants.tagihanIPLXML;
                    generateXML_printoutTagihanIPL(out sqlCommand);
                    break;

                default:
                    sqlCommand = defaultSqlCommand;
                    break;
            }
        }

        private void generateXML_reportIPLTahunan(out string sqlCommand)
        {
            int ipl_kompleksID = 0;
            int ipl_year = DateTime.Now.Year;
            List<string> sqlMonth = new List<string>();
            int i = 0;
            REST_laporanIPLTahunan restTrans = new REST_laporanIPLTahunan();

            sqlCommand = "";
            string sqlJan = "";

            sqlFields sqlFieldsElement;
            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "kompleks_id");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    ipl_kompleksID = Convert.ToInt32(sqlFieldsElement.fieldValue.ToString());
                }
            }

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "periode");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    ipl_year = Convert.ToInt32(sqlFieldsElement.fieldValue.ToString());
                }
            }

            string fieldMonth = "";
            string fieldFrom = "";

            for (i = 0;i<12;i++)
            {
                sqlJan = "SELECT DATE_FORMAT(d.periode_pembayaran, '%Y%m') as periode, h.kavling_id, sum(d.nominal) as nominal_" + i + " " +
                            "FROM transaksi_ipl h, transaksi_ipl_detail d " +
                            "WHERE DATE_FORMAT(d.periode_pembayaran, '%Y%m') = '" + ipl_year + (i+1).ToString().PadLeft(2, '0') + "' " +
                            "AND h.status_id = 2 " +
                            "AND h.is_active = 'Y' " +
                            "AND d.id_trans = h.id_trans " +
                            "GROUP BY DATE_FORMAT(d.periode_pembayaran, '%Y%m'), h.kavling_id";

                fieldMonth += ", IFNULL(nominal_" + i + ", 0) as nominal_" + i;
                fieldFrom += "LEFT OUTER JOIN (" + sqlJan + ") tab_" + i + " ON mv.kavling_id = tab_" + i + ".kavling_id ";
                sqlMonth.Add(sqlJan);
            }

            string masterKavling = "";
            if (ipl_kompleksID > 0)
            {
                masterKavling = "AND mk.kompleks_id = " + ipl_kompleksID + " ";
            }


            string sqlSingleUser = "SELECT uk.kavling_id, uk.user_id, uk.tgl_bast " +
                                            "FROM user_kavling uk, master_kavling mk " +
                                            "WHERE uk.is_active = 'Y' " +
                                            "AND mk.is_active = 'Y' " +
                                            "AND uk.kavling_id = mk.kavling_id " +
                                            masterKavling +
                                            "GROUP BY uk.kavling_id, uk.user_id, uk.tgl_bast " +
                                            "HAVING COUNT(1) <= 1 ";

            string sqlMultiUser = "SELECT uk.kavling_id, uk.user_id, uk.tgl_bast " +
                                            "FROM user_kavling uk, user_login_data ul, master_group mg, master_kavling mk " +
                                            "WHERE uk.is_active = 'Y' " +
                                            "AND ul.is_active = 'Y' " +
                                            "AND mk.is_active = 'Y' " +
                                            "AND uk.kavling_id = mk.kavling_id " +
                                            masterKavling +
                                            "AND uk.user_id = ul.user_id " +
                                            "AND ul.group_id = mg.group_id " +
                                            "AND mg.group_type = 1 " +
                                            "GROUP BY uk.kavling_id, uk.user_id, uk.tgl_bast " +
                                            "HAVING COUNT(1) > 1";

            string sqlUser = "SELECT tab.* " +
                                    "FROM (" + sqlSingleUser + " UNION ALL " + sqlMultiUser + ") tab";

            sqlCommand = "SELECT mk.kompleks_id, mk.kompleks_name, " +
                                    "uk.kavling_id, mv.blok_id, mb.blok_name, mv.house_no, " +
                                    "uk.tgl_bast, uk.user_id, ul.user_name, ul.user_full_name " +
                                    fieldMonth + " " +
                                    "FROM master_kompleks mk, master_kavling mv " +
                                    fieldFrom + 
                                    ", master_blok mb, " +
                                    "(" + sqlUser + ") uk, " +
                                    "user_login_data ul " +
                                    "WHERE " +
                                    "mv.kompleks_id = mk.kompleks_id " +
                                    "AND mv.blok_id = mb.blok_id " +
                                    "AND uk.kavling_id = mv.kavling_id " +
                                    "AND uk.user_id = ul.user_id " +
                                    "AND mk.is_active = 'Y' " +
                                    "AND mv.is_active = 'Y' " +
                                    //"AND uk.is_active = 'Y' " +
                                    "AND ul.is_active = 'Y' ";

            if (ipl_kompleksID > 0)
            {
                sqlCommand += "AND mk.kompleks_id = " + ipl_kompleksID + " ";
            }

            if (gRest.generateLaporanIPLTahunan(sqlCommand, ref restTrans))
            {
                DataTable dt = new DataTable("Table");

                dt.Columns.Add("periode");
                dt.Columns.Add("kompleks_id");
                dt.Columns.Add("kompleks_name");
                dt.Columns.Add("kavling_id");
                dt.Columns.Add("blok_id");
                dt.Columns.Add("blok_name");
                dt.Columns.Add("house_no");
                dt.Columns.Add("user_id");
                dt.Columns.Add("user_name");
                dt.Columns.Add("user_full_name");
                dt.Columns.Add("tgl_bast", typeof(DateTime));
                dt.Columns.Add("jan_nominal", typeof(double));
                dt.Columns.Add("feb_nominal", typeof(double));
                dt.Columns.Add("mar_nominal", typeof(double));
                dt.Columns.Add("apr_nominal", typeof(double));
                dt.Columns.Add("may_nominal", typeof(double));
                dt.Columns.Add("jun_nominal", typeof(double));
                dt.Columns.Add("jul_nominal", typeof(double));
                dt.Columns.Add("aug_nominal", typeof(double));
                dt.Columns.Add("sep_nominal", typeof(double));
                dt.Columns.Add("oct_nominal", typeof(double));
                dt.Columns.Add("nov_nominal", typeof(double));
                dt.Columns.Add("dec_nominal", typeof(double));

                for(i = 0;i< restTrans.dataList.Count;i++)
                {
                    DataRow r = dt.Rows.Add();

                    r["periode"] = ipl_year;
                    r["kompleks_id"] = restTrans.dataList[i].kompleks_id;
                    r["kompleks_name"] = restTrans.dataList[i].kompleks_name;
                    r["kavling_id"] = restTrans.dataList[i].kavling_id;
                    r["blok_id"] = restTrans.dataList[i].blok_id;
                    r["blok_name"] = restTrans.dataList[i].blok_name;
                    r["house_no"] = restTrans.dataList[i].house_no;
                    r["user_id"] = restTrans.dataList[i].user_id;
                    r["user_name"] = restTrans.dataList[i].user_name;
                    r["user_full_name"] = restTrans.dataList[i].user_full_name;
                    r["tgl_bast"] = restTrans.dataList[i].tgl_bast;
                    r["jan_nominal"] = restTrans.dataList[i].nominal_0;
                    r["feb_nominal"] = restTrans.dataList[i].nominal_1;
                    r["mar_nominal"] = restTrans.dataList[i].nominal_2;
                    r["apr_nominal"] = restTrans.dataList[i].nominal_3;
                    r["may_nominal"] = restTrans.dataList[i].nominal_4;
                    r["jun_nominal"] = restTrans.dataList[i].nominal_5;
                    r["jul_nominal"] = restTrans.dataList[i].nominal_6;
                    r["aug_nominal"] = restTrans.dataList[i].nominal_7;
                    r["sep_nominal"] = restTrans.dataList[i].nominal_8;
                    r["oct_nominal"] = restTrans.dataList[i].nominal_9;
                    r["nov_nominal"] = restTrans.dataList[i].nominal_10;
                    r["dec_nominal"] = restTrans.dataList[i].nominal_11;
                }

                string appPath = Directory.GetCurrentDirectory() + "\\xml\\" + xmlReportName;

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                using (StreamWriter fs = new StreamWriter(appPath)) // XML File Path
                {
                    ds.WriteXml(fs, XmlWriteMode.WriteSchema);
                }
            }

        }

        private void generateXML_reportAksesKompleks(out string sqlCommand)
        {
            int kompleksID = 0;
            string accessType = "";
            string accessStatus = "";
            int i = 0;
            REST_laporanAksesKompleks restTrans = new REST_laporanAksesKompleks();

            string sqlQR = "";
            string sqlRFID = "";

            sqlCommand = "";
            sqlFields sqlFieldsElement;

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "kompleks_id");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    kompleksID = Convert.ToInt32(sqlFieldsElement.fieldValue.ToString());
                }
            }

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "tipe_akses");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    accessType = sqlFieldsElement.fieldValue.ToString() + "'ALL'";
                }
            }

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "status");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    accessStatus = sqlFieldsElement.fieldValue.ToString() + "'ALL'";
                }
            }

            sqlQR = "SELECT user_id, kavling_id, 'QR' as access_type, user_qr_code as access_value, is_active, user_qr_enabled as access_enabled " +
                        "FROM user_kavling";

            sqlRFID = "SELECT uk.user_id, uk.kavling_id, 'RFID' as access_type, ukr.rfid_value as access_value, ukr.is_active, ukr.rfid_enabled as access_enabled " +
                            "FROM user_kavling uk, user_kavling_rfid as ukr " +
                            "WHERE ukr.user_kavling_id = uk.id";

            sqlCommand = "SELECT mk.kompleks_id, mk.kompleks_name, " +
                                    "tab.kavling_id, mv.blok_id, mb.blok_name, mv.house_no, " +
                                    "tab.*, ul.user_name, ul.user_full_name " +
                                    "FROM master_kompleks mk, master_kavling mv, master_blok mb, " +
                                    "(" + sqlQR + " UNION ALL " + sqlRFID + ") tab, " +
                                    "user_login_data ul " +
                                    "WHERE " +
                                    "mv.kompleks_id = mk.kompleks_id " +
                                    "AND mv.blok_id = mb.blok_id " +
                                    "AND tab.kavling_id = mv.kavling_id " +
                                    "AND tab.user_id = ul.user_id " +
                                    "AND mk.is_active = 'Y' " +
                                    "AND mv.is_active = 'Y' " +
                                    "AND ul.is_active = 'Y' ";

            if (kompleksID > 0)
            {
                sqlCommand += "AND mk.kompleks_id = " + kompleksID + " ";
            }

            if (accessType.Length > 0)
            {
                sqlCommand += "AND tab.access_type IN (" + accessType + ") ";
            }

            if (accessStatus.Length >0)
            {
                sqlCommand += "AND tab.is_active IN (" + accessStatus + ") ";
            }

            if (gRest.generateLaporanAksesKompleks(sqlCommand, ref restTrans))
            {
                DataTable dt = new DataTable("Table");

                dt.Columns.Add("kompleks_id");
                dt.Columns.Add("kavling_id");
                dt.Columns.Add("kompleks_name");
                dt.Columns.Add("blok_name");
                dt.Columns.Add("house_no");
                dt.Columns.Add("user_id");
                dt.Columns.Add("user_name");
                dt.Columns.Add("user_full_name");
                dt.Columns.Add("access_type");
                dt.Columns.Add("access_value");
                dt.Columns.Add("access_status");
                dt.Columns.Add("access_enabled");
                dt.Columns.Add("is_active");

                for (i = 0; i < restTrans.dataList.Count; i++)
                {
                    DataRow r = dt.Rows.Add();

                    r["kompleks_id"] = restTrans.dataList[i].kompleks_id;
                    r["kavling_id"] = restTrans.dataList[i].kavling_id;
                    r["kompleks_name"] = restTrans.dataList[i].kompleks_name;
                    r["blok_name"] = restTrans.dataList[i].blok_name;
                    r["house_no"] = restTrans.dataList[i].house_no;
                    r["user_id"] = restTrans.dataList[i].user_id;
                    r["user_name"] = restTrans.dataList[i].user_name;
                    r["user_full_name"] = restTrans.dataList[i].user_full_name;
                    r["access_type"] = restTrans.dataList[i].access_type;
                    r["access_value"] = restTrans.dataList[i].access_value;
                    r["access_status"] = restTrans.dataList[i].access_status;
                    r["access_enabled"] = restTrans.dataList[i].access_enabled;
                    r["is_active"] = restTrans.dataList[i].is_active;
                }

                string appPath = Directory.GetCurrentDirectory() + "\\xml\\" + xmlReportName;

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                using (StreamWriter fs = new StreamWriter(appPath)) // XML File Path
                {
                    ds.WriteXml(fs, XmlWriteMode.WriteSchema);
                }
            }
        }

        private void generateXML_reportKavling(out string sqlCommand)
        {
            int kompleksID = 0;
            int i = 0;
            REST_laporanKavling restTrans = new REST_laporanKavling();

            sqlCommand = "";
            sqlFields sqlFieldsElement;

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "kompleks_id");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    kompleksID = Convert.ToInt32(sqlFieldsElement.fieldValue.ToString());
                }
            }

            sqlCommand = "SELECT mk.kompleks_id, mk.kompleks_name, " +
                                    "mv.kavling_id, mv.blok_id, mb.blok_name, mv.house_no, " +
                                    "ul.user_name, ul.user_full_name " +
                                    "FROM master_kompleks mk, master_kavling mv, master_blok mb, user_kavling uk, " +
                                    "user_login_data ul " +
                                    "WHERE " +
                                    "mv.kompleks_id = mk.kompleks_id " +
                                    "AND mv.blok_id = mb.blok_id " +
                                    "AND mv.kavling_id = uk.kavling_id " +
                                    "AND uk.user_id = ul.user_id " +
                                    "AND mk.is_active = 'Y' " +
                                    "AND mv.is_active = 'Y' " +
                                    "AND ul.is_active = 'Y' " +
                                    "AND uk.is_active = 'Y'";

            if (kompleksID > 0)
            {
                sqlCommand += "AND mk.kompleks_id = " + kompleksID + " ";
            }

            sqlCommand += "ORDER by mk.kompleks_name ASC, mb.blok_name ASC, mv.house_no ASC";

            if (gRest.generateLaporanKavling(sqlCommand, ref restTrans))
            {
                DataTable dt = new DataTable("Table");

                dt.Columns.Add("kompleks_id");
                dt.Columns.Add("kavling_id");
                dt.Columns.Add("kompleks_name");
                dt.Columns.Add("blok_name");
                dt.Columns.Add("house_no");
                dt.Columns.Add("user_id");
                dt.Columns.Add("user_name");
                dt.Columns.Add("user_full_name");

                for (i = 0; i < restTrans.dataList.Count; i++)
                {
                    DataRow r = dt.Rows.Add();

                    r["kompleks_id"] = restTrans.dataList[i].kompleks_id;
                    r["kavling_id"] = restTrans.dataList[i].kavling_id;
                    r["kompleks_name"] = restTrans.dataList[i].kompleks_name;
                    r["blok_name"] = restTrans.dataList[i].blok_name;
                    r["house_no"] = restTrans.dataList[i].house_no;
                    r["user_id"] = restTrans.dataList[i].user_id;
                    r["user_name"] = restTrans.dataList[i].user_name;
                    r["user_full_name"] = restTrans.dataList[i].user_full_name;
                }

                string appPath = Directory.GetCurrentDirectory() + "\\xml\\" + xmlReportName;

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                using (StreamWriter fs = new StreamWriter(appPath)) // XML File Path
                {
                    ds.WriteXml(fs, XmlWriteMode.WriteSchema);
                }
            }

        }

        private void generateXML_reportSummaryIPL(out string sqlCommand)
        {
            int ipl_kompleksID = 0;
            int ipl_kavlingID = 0;
            int ipl_status = 0;
            int ipl_payType = 0;

            REST_laporanIPL restTrans = new REST_laporanIPL();

            sqlCommand = "";

            sqlFields sqlFieldsElement;
            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "kompleks_id");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    ipl_kompleksID = Convert.ToInt32(sqlFieldsElement.fieldValue.ToString());
                }
            }

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "kavling_id");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    ipl_kavlingID = Convert.ToInt32(sqlFieldsElement.fieldValue.ToString());
                }
            }

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "status");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    ipl_status = Convert.ToInt32(sqlFieldsElement.fieldValue.ToString());
                }
            }

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "paymentType");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    ipl_payType = Convert.ToInt32(sqlFieldsElement.fieldValue.ToString());
                }
            }

            string sqlMaxEndIPL = "SELECT kavling_id, user_id, max(end_ipl) as end_ipl " +
                                                "FROM transaksi_ipl " + 
                                                "WHERE is_active = 'Y' " +
                                                "AND status_id = 2 " +
                                                "GROUP BY kavling_id, user_id";

            string sqlSubQuery = "SELECT ipl.kavling_id, ipl.user_id, ipl.start_ipl, ipl.end_ipl, ipl.nominal, ipl.is_manual_paid " +
                                            "FROM transaksi_ipl ipl, " +
                                            "(" + sqlMaxEndIPL + ") tab " +
                                            "WHERE ipl.kavling_id = tab.kavling_id and ipl.user_id = tab.user_id and ipl.end_ipl = tab.end_ipl " +
                                            "AND ipl.is_active = 'Y' " +
                                            "AND ipl.status_id = 2";

            sqlCommand = "SELECT mk.kompleks_id, mk.kompleks_name, " +
                                    "mv.kavling_id, mb.blok_name, mv.house_no, " +
                                    "uk.tgl_bast, uk.user_id, ul.user_name, ul.user_full_name,  " +
                                    "tab.start_ipl, tab.end_ipl, tab.nominal, tab.is_manual_paid " +
                                    "FROM master_kompleks mk, master_kavling mv, master_blok mb, user_kavling uk, user_login_data ul, " +
                                    "(" + sqlSubQuery + ") tab " +
                                    "WHERE " +
                                    "mv.kompleks_id = mk.kompleks_id " +
                                    "AND mv.blok_id = mb.blok_id " +
                                    "AND uk.kavling_id = mv.kavling_id " +
                                    "AND uk.user_id = ul.user_id " +
                                    "AND tab.kavling_id = uk.kavling_id " +
                                    "AND tab.user_id = uk.user_id " +
                                    "AND mk.is_active = 'Y' " +
                                    "AND mv.is_active = 'Y' " +
                                    "AND uk.is_active = 'Y' " +
                                    "AND ul.is_active = 'Y' ";
            
            if (ipl_kompleksID > 0)
            {
                sqlCommand += "AND mk.kompleks_id = " + ipl_kompleksID + " ";
            }

            if (ipl_kavlingID > 0)
            {
                sqlCommand += "AND mv.kavling_id = " + ipl_kavlingID + " ";
            }

            if (gRest.generateLaporanIPLKompleks(sqlCommand, ref restTrans))
            {
                DataTable dt = new DataTable("Table");

                dt.Columns.Add("kompleks_id");
                dt.Columns.Add("kompleks_name");
                dt.Columns.Add("kavling_id");
                dt.Columns.Add("blok_name");
                dt.Columns.Add("house_no");
                dt.Columns.Add("user_id");
                dt.Columns.Add("user_name");
                dt.Columns.Add("user_full_name");
                dt.Columns.Add("tgl_bast", typeof(DateTime));
                dt.Columns.Add("last_payment");
                dt.Columns.Add("last_payment_nominal", typeof(double));
                dt.Columns.Add("last_payment_type");
                dt.Columns.Add("ipl_belum_bayar");
                dt.Columns.Add("status"); // lancar, kurang lancar, macet

                string startIPL = "";
                string endIPL = "";
                string isManualPaid = "";
                string unpaidList = "";
                DateTime unpaidStartIPL = DateTime.Now;
                string statusValue = "";
                int statusID = 0;
                int lateDuration = 0;
                int numData = 0;

                // payType 1 => MIDTRANS
                // payType 2 => TUNAI/TRANSFER

                for (int i = 0; i < restTrans.dataList.Count; i++)
                {
                    statusID = 0;
                    unpaidStartIPL = DateTime.Now;
                    startIPL = String.Format(culture, "{0:MMM/yy}", restTrans.dataList[i].start_ipl);
                    endIPL = String.Format(culture, "{0:MMM/yy}", restTrans.dataList[i].end_ipl);
                    isManualPaid = restTrans.dataList[i].is_manual_paid;

                    unpaidList = gIPL.getUnpaidData(gUser.getUserID(), gUser.getUserToken(), restTrans.dataList[i].kavling_id,
                                                        restTrans.dataList[i].user_id, out numData, out unpaidStartIPL);
                    if (numData <= 0)
                    {
                        statusValue = "LANCAR";
                        statusID = 1;
                    }
                    else
                    {
                        lateDuration = ((DateTime.Now - unpaidStartIPL).Days / 30);
                        if (lateDuration > 3)
                        {
                            statusValue = "MACET";
                            statusID = 3;
                        }
                        else
                        {
                            statusValue = "KURANG LANCAR";
                            statusID = 2;
                        }
                    }

                    if (ipl_payType == 1 && isManualPaid == "Y" ||
                        ipl_payType == 2 && isManualPaid == "N")
                    {
                        continue;
                    }

                    if (ipl_status != statusID 
                        && ipl_status > 0)
                        continue;

                    DataRow r = dt.Rows.Add();

                    r["kompleks_id"] = restTrans.dataList[i].kompleks_id;
                    r["kompleks_name"] = restTrans.dataList[i].kompleks_name;
                    r["kavling_id"] = restTrans.dataList[i].kavling_id;
                    r["blok_name"] = restTrans.dataList[i].blok_name;
                    r["house_no"] = restTrans.dataList[i].house_no;
                    r["user_id"] = restTrans.dataList[i].user_id;
                    r["user_name"] = restTrans.dataList[i].user_name;
                    r["user_full_name"] = restTrans.dataList[i].user_full_name;
                    r["tgl_bast"] = restTrans.dataList[i].tgl_bast;
                    r["last_payment"] = startIPL + "-" + endIPL;
                    r["last_payment_nominal"] = restTrans.dataList[i].nominal;
                    r["last_payment_type"] = (isManualPaid == "Y" ? "TUNAI/TRANSFER" : "MIDTRANS");
                    r["ipl_belum_bayar"] = unpaidList;
                    r["status"] = statusValue;
                }

                string appPath = Directory.GetCurrentDirectory() + "\\xml\\" + xmlReportName;

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                using (StreamWriter fs = new StreamWriter(appPath)) // XML File Path
                {
                    ds.WriteXml(fs, XmlWriteMode.WriteSchema);
                }
            }

        }

        private void generateREPORTXml()
        {
            string sqlCommand = "";
            string sqlSubCommand = "";

            List<string> paramList = new List<string>();
            List<object> paramValues = new List<object>();
            sqlFields sqlFieldsElement;

            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now;

            string dtStartValue = "";
            string dtEndValue = "";

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "START_DATE");
            if (null != sqlFieldsElement)
                startDate = Convert.ToDateTime(sqlFieldsElement.fieldValue).Date;

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "END_DATE");
            if (null != sqlFieldsElement)
                endDate = Convert.ToDateTime(sqlFieldsElement.fieldValue).Date;

            dtStartValue = String.Format(culture, "{0:dd/MM/yyyy}", startDate);
            dtEndValue = String.Format(culture, "{0:dd/MM/yyyy}", endDate);

            paramList.Clear();
            paramValues.Clear();

            switch (originModuleID)
            {
                case globalReportConstants.REPORT_SUMMARY_IPL:
                    xmlReportName = globalReportConstants.reportIPLKompleksXML;
                    generateXML_reportSummaryIPL(out sqlCommand);
                    break;

                case globalReportConstants.REPORT_IPL_TAHUNAN:
                    xmlReportName = globalReportConstants.reportIPLTahunanXML;
                    generateXML_reportIPLTahunan(out sqlCommand);
                    break;

                case globalReportConstants.REPORT_AKSES_KOMPLEKS:
                    xmlReportName = globalReportConstants.reportAksesKompleksXML;
                    generateXML_reportAksesKompleks(out sqlCommand);
                    break;

                case globalReportConstants.REPORT_KAVLING:
                    xmlReportName = globalReportConstants.reportKavlingXML;
                    generateXML_reportKavling(out sqlCommand);
                    break;

                default:
                    sqlCommand = defaultSqlCommand;
                    break;
            }

            DS.writeXML(sqlCommand, xmlReportName, paramList, paramValues);
        }

        private void generateXMLFile()
        {
            if (originModuleID > globalReportConstants._START_PRINTOUT_
                && originModuleID < globalReportConstants._END_PRINTOUT_)
            {
                generatePRINTOUTXml();
            }
            else if (originModuleID > globalReportConstants.START_REPORT
               && originModuleID < globalReportConstants.END_REPORT)
            {
                generateREPORTXml();
            }
        }

        public RPT_containerForm(Data_Access DSParam, int moduleID, 
            string sqlCommand = "", List<sqlFields> sqlFieldsParam = null)
        {
            InitializeComponent();

            originModuleID = moduleID;

            if (null != DSParam)
                DS = DSParam;
            else
                DS = new Data_Access();

            gUser = new globalUserUtil(DS);
            gDB = new globalDBUtil(DS);
            gIPL = new globalIPL();

            defaultSqlCommand = sqlCommand;
            sqlFieldsValue = sqlFieldsParam;

            appPath = Directory.GetCurrentDirectory() + "\\xml\\";

            if (!Directory.Exists(appPath))
                Directory.CreateDirectory(appPath);

            generateXMLFile();
        }

        public void setDateReport(string dateToF, string dateFromF)
        {
            dateTo = dateToF;
            dateFrom = dateFromF;
        }

        private void RPT_containerForm_Load(object sender, EventArgs e)
        {
            DataSet dsTempReport = new DataSet();

            TextObject txtReportHeader1, txtReportHeader2, txtReportHeader3, txtColumnHeader1, txtTitle;
            TextObject fieldHeader1, fieldHeader2, fieldHeader3, fieldHeader4;

            try
            {
                string appPath = "";

                // if (originModuleID != globalReportConstants.PRINTOUT_KARTU_STOK)
                {
                    appPath = Directory.GetCurrentDirectory() + "\\xml\\" + xmlReportName;
                    dsTempReport.ReadXml(@appPath);
                }

                //ReportClass rptXMLReport = null;
                var rptXMLReport = (dynamic)null;

                switch (originModuleID)
                {
                    case globalReportConstants.PRINTOUT_IPL:
                    case globalReportConstants.PRINTOUT_TRANSAKSI_RETAIL:
                        rptXMLReport = new PRINTOUT_trans();
                        break;

                    case globalReportConstants.PRINTOUT_PENGIRIMAN_TRANSAKSI:
                        rptXMLReport = new PRINTOUT_transDelivery();
                        break;

                    case globalReportConstants.PRINTOUT_TRANSAKSI_PENERIMAAN:
                        rptXMLReport = new PRINTOUT_penerimaan();
                        break;

                    case globalReportConstants.REPORT_SUMMARY_IPL:
                        //rptXMLReport = new REPORT_iplKompleks();
                        rptXMLReport = new REPORT_iplKompleks_2();
                        break;

                    case globalReportConstants.REPORT_IPL_TAHUNAN:
                        rptXMLReport = new REPORT_iplTahunan_2();
                        break;

                    case globalReportConstants.PRINTOUT_TAGIHAN_IPL_MANUAL:
                        rptXMLReport = new PRINTOUT_tagihanIPL();
                        break;

                    case globalReportConstants.REPORT_AKSES_KOMPLEKS:
                        rptXMLReport = new REPORT_accessKompleks();
                        break;

                    case globalReportConstants.REPORT_KAVLING:
                        rptXMLReport = new REPORT_kavling();
                        break;

                    default:
                        rptXMLReport = new dummyReport();
                        break;
                }

                // if (originModuleID != globalReportConstants.PRINTOUT_KARTU_STOK)
                rptXMLReport.Database.Tables[0].SetDataSource(dsTempReport.Tables[0]);

                #region SUB REPORT 
          
                #endregion

                #region GENERAL HEADER INFORMATION
                String nama, alamat, telepon, email;
                if (!gUser.RS_loadInfoSysConfig(out nama, out alamat, out telepon, out email))
                {
                    nama = "NAMA DEFAULT";
                    alamat = "ALAMAT DEFAULT";
                    telepon = "0271-XXXXXXX";
                    email = "A@B.COM";
                }

                if (!arrSkipCompleteHeader.Contains(originModuleID))
                {
                    txtReportHeader1 = rptXMLReport.ReportDefinition.ReportObjects["namaCompLabel"] as TextObject;
                    txtReportHeader2 = rptXMLReport.ReportDefinition.ReportObjects["infoCompLabel"] as TextObject;

                    txtReportHeader1.Text = nama;
                    txtReportHeader2.Text = alamat + Environment.NewLine + telepon + Environment.NewLine + email;
                }
                else if (!arrNoHeader.Contains(originModuleID))
                {
                    txtReportHeader1 = rptXMLReport.ReportDefinition.ReportObjects["namaCompLabel"] as TextObject;
                    txtReportHeader1.Text = nama;
                }
                #endregion

                globalPrinterUtility gPrinter = new globalPrinterUtility();
                rptXMLReport.PrintOptions.PrinterName = gPrinter.getConfigPrinterName(2);

                if (arrQuarterKuarto.Contains(originModuleID))
                {
                    rptXMLReport.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)gPrinter.getReportPaperSize(globalPrinterUtility.QUARTER_KUARTO_PAPER_SIZE);
                }
                else if (arrHalfKuarto.Contains(originModuleID))
                {
                    rptXMLReport.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)gPrinter.getReportPaperSize(globalPrinterUtility.HALF_KUARTO_PAPER_SIZE);
                }
                else
                {
                    rptXMLReport.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)gPrinter.getReportPaperSize(globalPrinterUtility.LETTER_PAPER_SIZE);
                }

                rptXMLReport.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;

                if (arrLangsungPrint.Contains(originModuleID))
                {
                    rptXMLReport.PrintToPrinter(1, false, 0, 0);
                    this.Close();
                }
                else if (arrCheckPreview.Contains(originModuleID))
                {
                    if (gDB.getPrintPreview())
                    {
                        crViewer.ReportSource = rptXMLReport;
                        crViewer.Refresh();
                    }
                    else
                    {
                        rptXMLReport.PrintToPrinter(1, false, 0, 0);
                        this.Close();
                    }
                }
                else
                {
                    this.WindowState = FormWindowState.Maximized;
                    crViewer.ReportSource = rptXMLReport;
                    crViewer.Refresh();
                }
            }
            catch (Exception ex) { }
        }

        public void setReportMonth(string monthName)
        {
            report_month = monthName;
        }
    }
}

