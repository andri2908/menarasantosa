﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Globalization;

namespace AlphaSoft
{
    public partial class _reportFilter1Field : basicHotkeysForm
    {
        private int originModuleID = 0;
        private CultureInfo culture = new CultureInfo("id-ID");

        public _reportFilter1Field(int moduleID)
        {
            InitializeComponent();

            originModuleID = moduleID;
        }

        private void _reportFilter1Field_Load(object sender, EventArgs e)
        {
            switch (originModuleID)
            {
            }
        }

        private void displayButton_Click(object sender, EventArgs e)
        {
            string sqlCommand = "";

            List<sqlFields> listSql = new List<sqlFields>();
            sqlFields sqlElement;

            switch (originModuleID)
            {
                default:
                    break;
            }

            RPT_containerForm displayReportForm = new RPT_containerForm(null, originModuleID, sqlCommand, listSql);
            displayReportForm.ShowDialog(this);
        }
    }
}
