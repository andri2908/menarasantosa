﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using MySql.Data;
using MySql.Data.MySqlClient;

namespace AlphaSoft
{
    public partial class _reportFilter1FieldRangeDate : basicHotkeysForm
    {
        private int originModuleID = 0;
        private string selectedValue = "0";

        public _reportFilter1FieldRangeDate(int moduleID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;
        }
      
        private void browseButton_Click(object sender, EventArgs e)
        {
            switch (originModuleID)
            {
                case globalReportConstants.PRINTOUT_TAGIHAN_IPL_MANUAL:
                    dataBrowse_KavlingForm displayKavling = new dataBrowse_KavlingForm(globalReportConstants.PRINTOUT_TAGIHAN_IPL_MANUAL);
                    displayKavling.ShowDialog(this);

                    if (displayKavling.ReturnValue1 != "0")
                    {
                        selectedValue = displayKavling.ReturnValue1;
                        filterValueTextBox.Text = displayKavling.returnNamaKavling;
                    }
                    break;
            }
        }

        private void displayButton_Click(object sender, EventArgs e)
        {
            string sqlCommand = "";

            List<sqlFields> listSql = new List<sqlFields>();
            sqlFields sqlElement;

            switch (originModuleID)
            {
                case globalReportConstants.PRINTOUT_TAGIHAN_IPL_MANUAL:
                    sqlElement = new sqlFields();
                    sqlElement.fieldName = "KAVLING_ID";
                    sqlElement.fieldValue = selectedValue;
                    listSql.Add(sqlElement);
                    break;

                default:
                    break;
            }

            sqlElement = new sqlFields();
            sqlElement.fieldName = "START_DATE";
            sqlElement.fieldValue = startDTPicker.Value.Date;
            listSql.Add(sqlElement);

            sqlElement = new sqlFields();
            sqlElement.fieldName = "END_DATE";
            sqlElement.fieldValue = endDTPicker.Value.Date;
            listSql.Add(sqlElement);

            RPT_containerForm displayReportForm = new RPT_containerForm(null, originModuleID, sqlCommand, listSql);
            displayReportForm.ShowDialog(this);
        }

        private void _reportFilter1FieldRangeDate_Load(object sender, EventArgs e)
        {
            switch (originModuleID)
            {
                case globalReportConstants.PRINTOUT_TAGIHAN_IPL_MANUAL:
                    labelFilter.Text = "Kavling";
                    break;
            }
        }
    }
}
