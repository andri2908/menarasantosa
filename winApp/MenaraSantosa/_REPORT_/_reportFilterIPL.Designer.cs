﻿namespace AlphaSoft
{
    partial class _reportFilterIPL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_reportFilterIPL));
            this.browseButton = new System.Windows.Forms.Button();
            this.kompleksTextBox = new System.Windows.Forms.TextBox();
            this.labelFilter = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.browseKavling = new System.Windows.Forms.Button();
            this.kavlingTextBox = new System.Windows.Forms.TextBox();
            this.statusCombo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.pembayaranCombo = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.displayButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // browseButton
            // 
            this.browseButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("browseButton.BackgroundImage")));
            this.browseButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.browseButton.Location = new System.Drawing.Point(576, 45);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(25, 27);
            this.browseButton.TabIndex = 96;
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // kompleksTextBox
            // 
            this.kompleksTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kompleksTextBox.Location = new System.Drawing.Point(145, 45);
            this.kompleksTextBox.Name = "kompleksTextBox";
            this.kompleksTextBox.ReadOnly = true;
            this.kompleksTextBox.Size = new System.Drawing.Size(425, 27);
            this.kompleksTextBox.TabIndex = 95;
            // 
            // labelFilter
            // 
            this.labelFilter.AutoSize = true;
            this.labelFilter.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFilter.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelFilter.Location = new System.Drawing.Point(39, 47);
            this.labelFilter.Name = "labelFilter";
            this.labelFilter.Size = new System.Drawing.Size(105, 18);
            this.labelFilter.TabIndex = 97;
            this.labelFilter.Text = "KOMPLEKS";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(56, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 18);
            this.label1.TabIndex = 100;
            this.label1.Text = "KAVLING";
            // 
            // browseKavling
            // 
            this.browseKavling.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("browseKavling.BackgroundImage")));
            this.browseKavling.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.browseKavling.Enabled = false;
            this.browseKavling.Location = new System.Drawing.Point(576, 78);
            this.browseKavling.Name = "browseKavling";
            this.browseKavling.Size = new System.Drawing.Size(25, 27);
            this.browseKavling.TabIndex = 99;
            this.browseKavling.UseVisualStyleBackColor = true;
            this.browseKavling.Click += new System.EventHandler(this.browseKavling_Click);
            // 
            // kavlingTextBox
            // 
            this.kavlingTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kavlingTextBox.Location = new System.Drawing.Point(145, 78);
            this.kavlingTextBox.Name = "kavlingTextBox";
            this.kavlingTextBox.ReadOnly = true;
            this.kavlingTextBox.Size = new System.Drawing.Size(425, 27);
            this.kavlingTextBox.TabIndex = 98;
            // 
            // statusCombo
            // 
            this.statusCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.statusCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.statusCombo.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusCombo.FormattingEnabled = true;
            this.statusCombo.Location = new System.Drawing.Point(145, 111);
            this.statusCombo.Name = "statusCombo";
            this.statusCombo.Size = new System.Drawing.Size(298, 26);
            this.statusCombo.TabIndex = 102;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(65, 115);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 18);
            this.label7.TabIndex = 101;
            this.label7.Text = "STATUS";
            // 
            // pembayaranCombo
            // 
            this.pembayaranCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.pembayaranCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.pembayaranCombo.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pembayaranCombo.FormattingEnabled = true;
            this.pembayaranCombo.Location = new System.Drawing.Point(145, 143);
            this.pembayaranCombo.Name = "pembayaranCombo";
            this.pembayaranCombo.Size = new System.Drawing.Size(298, 26);
            this.pembayaranCombo.TabIndex = 104;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(12, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 18);
            this.label2.TabIndex = 103;
            this.label2.Text = "PEMBAYARAN";
            // 
            // displayButton
            // 
            this.displayButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayButton.Location = new System.Drawing.Point(216, 188);
            this.displayButton.Name = "displayButton";
            this.displayButton.Size = new System.Drawing.Size(202, 58);
            this.displayButton.TabIndex = 105;
            this.displayButton.Text = "DISPLAY";
            this.displayButton.UseVisualStyleBackColor = true;
            this.displayButton.Click += new System.EventHandler(this.displayButton_Click);
            // 
            // _reportFilterIPL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(635, 301);
            this.Controls.Add(this.displayButton);
            this.Controls.Add(this.pembayaranCombo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.statusCombo);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.browseKavling);
            this.Controls.Add(this.kavlingTextBox);
            this.Controls.Add(this.labelFilter);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.kompleksTextBox);
            this.Name = "_reportFilterIPL";
            this.Load += new System.EventHandler(this._reportFilterIPL_Load);
            this.Controls.SetChildIndex(this.kompleksTextBox, 0);
            this.Controls.SetChildIndex(this.browseButton, 0);
            this.Controls.SetChildIndex(this.labelFilter, 0);
            this.Controls.SetChildIndex(this.kavlingTextBox, 0);
            this.Controls.SetChildIndex(this.browseKavling, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.statusCombo, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.pembayaranCombo, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.displayButton, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.TextBox kompleksTextBox;
        private System.Windows.Forms.Label labelFilter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button browseKavling;
        private System.Windows.Forms.TextBox kavlingTextBox;
        private System.Windows.Forms.ComboBox statusCombo;
        protected System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox pembayaranCombo;
        protected System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button displayButton;
    }
}
