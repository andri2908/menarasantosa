﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class _reportFilterIPL : AlphaSoft.basicHotkeysForm
    {
        private int selectedKompleksID = 0;
        private int selectedKavlingID = 0;
        private int statusID = 0;
        private int paymentType = 0;

        List<cbDataSource> cbType = new List<cbDataSource>();
        List<cbDataSource> cbStatus = new List<cbDataSource>();

        public _reportFilterIPL()
        {
            InitializeComponent();
        }

        private void _reportFilterIPL_Load(object sender, EventArgs e)
        {
            fillInStatusCombo();
            fillInPaymentType();
        }

        private void fillInStatusCombo()
        {
            cbDataSource cbElement;

            cbStatus.Clear();

            cbElement = new cbDataSource();
            cbElement.displayMember = "ALL";
            cbElement.valueMember = "0";
            cbStatus.Add(cbElement);

            cbElement = new cbDataSource();
            cbElement.displayMember = "LANCAR";
            cbElement.valueMember = "1";
            cbStatus.Add(cbElement);

            cbElement = new cbDataSource();
            cbElement.displayMember = "KURANG LANCAR";
            cbElement.valueMember = "2";
            cbStatus.Add(cbElement);

            cbElement = new cbDataSource();
            cbElement.displayMember = "MACET";
            cbElement.valueMember = "3";
            cbStatus.Add(cbElement);

            statusCombo.DataSource = cbStatus;
            statusCombo.DisplayMember = "displayMember";
            statusCombo.ValueMember = "valueMember";

            statusCombo.SelectedValue = "0";
            statusCombo.SelectedIndex = 0;
        }

        private void fillInPaymentType()
        {
            cbDataSource cbElement;

            cbType.Clear();

            cbElement = new cbDataSource();
            cbElement.displayMember = "ALL";
            cbElement.valueMember = "0";
            cbType.Add(cbElement);

            cbElement = new cbDataSource();
            cbElement.displayMember = "MIDTRANS";
            cbElement.valueMember = "1";
            cbType.Add(cbElement);

            cbElement = new cbDataSource();
            cbElement.displayMember = "TUNAI/TRANSFER";
            cbElement.valueMember = "2";
            cbType.Add(cbElement);
            
            pembayaranCombo.DataSource = cbType;
            pembayaranCombo.DisplayMember = "displayMember";
            pembayaranCombo.ValueMember = "valueMember";

            pembayaranCombo.SelectedValue = "0";
            pembayaranCombo.SelectedIndex = 0;
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            dataBrowse_KompleksForm displayForm = new dataBrowse_KompleksForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                browseKavling.Enabled = true;
                if (selectedKompleksID != Convert.ToInt32(displayForm.ReturnValue1))
                {
                    selectedKavlingID = 0;
                    kavlingTextBox.Text = "";
                }
                selectedKompleksID = Convert.ToInt32(displayForm.ReturnValue1);
                kompleksTextBox.Text = displayForm.ReturnValue2;
            }
        }

        private void browseKavling_Click(object sender, EventArgs e)
        {
            dataBrowse_KavlingForm displayForm = new dataBrowse_KavlingForm(globalConstants.SUMMARY_IPL, selectedKompleksID);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                selectedKavlingID = Convert.ToInt32(displayForm.ReturnValue1);
                kavlingTextBox.Text = displayForm.ReturnValue2;
            }
        }

        private void displayButton_Click(object sender, EventArgs e)
        {
            string sqlCommand = "";

            List<sqlFields> listSql = new List<sqlFields>();
            sqlFields sqlElement;

            sqlElement = new sqlFields();
            sqlElement.fieldName = "kompleks_id";
            sqlElement.fieldValue = selectedKompleksID;
            listSql.Add(sqlElement);

            sqlElement = new sqlFields();
            sqlElement.fieldName = "kavling_id";
            sqlElement.fieldValue = selectedKavlingID;
            listSql.Add(sqlElement);

            sqlElement = new sqlFields();
            sqlElement.fieldName = "status";
            sqlElement.fieldValue = statusCombo.SelectedValue;
            listSql.Add(sqlElement);

            sqlElement = new sqlFields();
            sqlElement.fieldName = "paymentType";
            sqlElement.fieldValue = pembayaranCombo.SelectedValue;
            listSql.Add(sqlElement);

            RPT_containerForm displayReportForm = new RPT_containerForm(null, globalReportConstants.REPORT_SUMMARY_IPL, sqlCommand, listSql);
            displayReportForm.ShowDialog(this);
        }
    }
}
