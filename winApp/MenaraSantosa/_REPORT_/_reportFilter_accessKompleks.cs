﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class _reportFilter_accessKompleks : AlphaSoft.basicHotkeysForm
    {
        private int selectedKompleksID = 0;

        public _reportFilter_accessKompleks()
        {
            InitializeComponent();
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            dataBrowse_KompleksForm displayForm = new dataBrowse_KompleksForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                selectedKompleksID = Convert.ToInt32(displayForm.ReturnValue1);
                kompleksTextBox.Text = displayForm.ReturnValue2;
            }
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            selectedKompleksID = 0;
            kompleksTextBox.Text = "ALL";
        }

        private void displayButton_Click(object sender, EventArgs e)
        {
            string sqlCommand = "";

            List<sqlFields> listSql = new List<sqlFields>();
            sqlFields sqlElement;

            sqlElement = new sqlFields();
            sqlElement.fieldName = "kompleks_id";
            sqlElement.fieldValue = selectedKompleksID;
            listSql.Add(sqlElement);

            sqlElement = new sqlFields();
            sqlElement.fieldName = "tipe_akses";
            sqlElement.fieldValue = (qrCheckBox.Checked ? "'QR', " : "") + (rfidCheckbox.Checked ? "'RFID', " : "");
            listSql.Add(sqlElement);

            sqlElement = new sqlFields();
            sqlElement.fieldName = "status";
            sqlElement.fieldValue = (checkBoxAktif.Checked ? "'Y', " : "") + (checkBoxNonAktif.Checked ? "'N', " : "");
            listSql.Add(sqlElement);

            RPT_containerForm displayReportForm = new RPT_containerForm(null, globalReportConstants.REPORT_AKSES_KOMPLEKS, sqlCommand, listSql);
            displayReportForm.ShowDialog(this);
        }
    }
}
