﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace AlphaSoft
{
    public partial class dataGroupUserDetailForm : basic2TextBoxInputsForm
    {
        private int originModuleID = 0;
        private int selectedGroupID = 0;

        private globalUserUtil gUser;
        private globalRestAPI gRest;
        private CultureInfo culture = new CultureInfo("id-ID");

        List<cbDataSource> cbType = new List<cbDataSource>();

        public REST_groupUser groupUserData = new REST_groupUser();

        public int returnGroupUserIDValue = 0;

        public dataGroupUserDetailForm(int groupID = 0)
        {
            InitializeComponent();
            selectedGroupID = groupID;
            
            gUser = new globalUserUtil();
            gRest = new globalRestAPI();

            if (selectedGroupID > 0)
            {
                originModuleID = globalConstants.EDIT_GROUP_USER;

                if (!gUser.RS_userHasAccessTo(MMConstants.TAMBAH_HAPUS_GROUP_USER,
                MMConstants.HAK_UPDATE_DATA))
                {
                    setSaveButtonEnable(false);
                    setResetButtonEnable(false);
                }
            }
            else
                originModuleID = globalConstants.NEW_GROUP_USER;
        }

        private void dataGroupUserDetailForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            returnGroupUserIDValue = selectedGroupID;
        }

        private void loadGroupDataInformation()
        {
            DataTable dt = new DataTable();

            string sqlCommand = "SELECT * FROM master_group WHERE group_id = " + selectedGroupID;

            gRest.getMasterGroupData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref groupUserData);

            if (groupUserData.dataStatus.o_status == 1)
            {
                namaTextBox.Text = groupUserData.dataList[0].group_name;
                deskripsiTextBox.Text = groupUserData.dataList[0].group_description;
                nonAktifCheckbox.Checked = (groupUserData.dataList[0].is_active == "Y" ? false : true);

                if (selectedGroupID > 1)
                    nonAktifCheckbox.Enabled = true;

                groupTypeCombo.SelectedValue = groupUserData.dataList[0].group_type.ToString();
            }
        }

        private void fillInGroupCombo()
        {
            cbDataSource cbElement;

            cbType.Clear();

            cbElement = new cbDataSource();
            cbElement.displayMember = "RESIDENT";
            cbElement.valueMember = "0";
            cbType.Add(cbElement);

            cbElement = new cbDataSource();
            cbElement.displayMember = "LANDLORD";
            cbElement.valueMember = "1";
            cbType.Add(cbElement);

            cbElement = new cbDataSource();
            cbElement.displayMember = "ADMIN WINAPP";
            cbElement.valueMember = "2";
            cbType.Add(cbElement);

            cbElement = new cbDataSource();
            cbElement.displayMember = "ADMIN CS";
            cbElement.valueMember = "3";
            cbType.Add(cbElement);

            cbElement = new cbDataSource();
            cbElement.displayMember = "ADMIN SHOP";
            cbElement.valueMember = "4";
            cbType.Add(cbElement);

            cbElement = new cbDataSource();
            cbElement.displayMember = "ADMIN DRUG";
            cbElement.valueMember = "5";
            cbType.Add(cbElement);

            groupTypeCombo.DataSource = cbType;
            groupTypeCombo.DisplayMember = "displayMember";
            groupTypeCombo.ValueMember = "valueMember";

            groupTypeCombo.SelectedValue = "0";
            groupTypeCombo.SelectedIndex = 0;
        }

        private void dataGroupUserDetailForm_Load(object sender, EventArgs e)
        {
            fillInGroupCombo();

            if (selectedGroupID > 0)
            {
                loadGroupDataInformation();
            }

            namaTextBox.Focus();
            gUser.reArrangeTabOrder(this);
        }

        protected override bool saveDataTransaction()
        {
            bool result = false;
            int opType = 0;
            string errMsg = "";

            List<master_group> groupUserData = new List<master_group>();
            master_group groupData = new master_group();

            try
            {
                groupData.group_name = namaTextBox.Text;
                groupData.group_description = deskripsiTextBox.Text;
                groupData.group_type = Convert.ToInt32(groupTypeCombo.SelectedValue);
                groupData.is_active = (nonAktifCheckbox.Checked ? "N" : "Y");

                switch (originModuleID)
                {
                    case globalConstants.NEW_GROUP_USER:
                        opType = 1;
                        groupData.group_id = 0;
                        break;

                    case globalConstants.EDIT_GROUP_USER:
                        opType = 2;
                        groupData.group_id = selectedGroupID;
                        break;
                }

                groupUserData.Add(groupData);

                int newID = 0;
                if (!gRest.saveGroupUser(gUser.getUserID(), gUser.getUserToken(), groupUserData, opType, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                selectedGroupID = newID;

                result = true;
            }
            catch(Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        protected override bool dataValidated()
        {
            errorLabel.Text = "";

            bool result = false;

            result = base.dataValidated();

            if (!result)
                return false;
          
            return true;
        }
    }
}
