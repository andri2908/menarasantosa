﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;


namespace AlphaSoft
{
    public partial class dataTerminalForm : AlphaSoft.basicHotkeysForm
    {
        List<cbDataSource> cbDT = new List<cbDataSource>();

        private int selectedKompleksID = 0;
        
        private globalUserUtil gUser;
        private globalIPL gIPL;
        private globalRestAPI gRest;

        private CultureInfo culture = new CultureInfo("id-ID");

        public dataTerminalForm()
        {
            InitializeComponent();

            gUser = new globalUserUtil();
            gIPL = new globalIPL();
            gRest = new globalRestAPI();
        }

        private void loadDataTerminal()
        {
            string sqlCommand = "";
            REST_kompleksTerminal terminalData = new REST_kompleksTerminal();

            sqlCommand = "SELECT id, terminal_id, terminal_type " +
                                   "FROM kompleks_terminal " +
                                   "WHERE kompleks_id = " + selectedKompleksID + " " +
                                   "AND is_active = 'Y'";

            sqlCommand += "ORDER BY terminal_id ASC";

            if (gRest.getKompleksTerminalData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref terminalData))
            {
                if (terminalData.dataStatus.o_status == 1)
                {
                    terminalGrid.Rows.Clear();
                    for (int i = 0; i < terminalData.dataList.Count; i++)
                    {
                        terminalGrid.Rows.Add(
                            terminalData.dataList[i].id,
                            1,
                            terminalData.dataList[i].terminal_id,
                            terminalData.dataList[i].terminal_type
                            );
                    }
                }
            }
        }

        private void loadStatusCombo()
        {
            cbDataSource cbDataSourceElement;
            cbDT.Clear();

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "IN";
            cbDataSourceElement.valueMember = "IN";
            cbDT.Add(cbDataSourceElement);

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "OUT";
            cbDataSourceElement.valueMember = "OUT";
            cbDT.Add(cbDataSourceElement);

            DataGridViewComboBoxColumn cbColumn = (DataGridViewComboBoxColumn)terminalGrid.Columns["terminalType"];
            cbColumn.DataSource = cbDT;
            cbColumn.DisplayMember = "displayMember";
            cbColumn.ValueMember = "valueMember";
        }

        private void searchKompleksButton_Click(object sender, EventArgs e)
        {
            dataBrowse_KompleksForm displayForm = new dataBrowse_KompleksForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                selectedKompleksID = Convert.ToInt32(displayForm.ReturnValue1);
                kompleksTextBox.Text = displayForm.ReturnValue2;

                newTerminal.Enabled = true;

                loadDataTerminal();
            }
        }

        private void newTerminal_Click(object sender, EventArgs e)
        {
            terminalGrid.Rows.Add(
                0,
                1,
                "",
                "IN");
        }

        private bool saveTrans(out string errMsg)
        {
            bool result = false;
            errMsg = "";
           
            List<kompleks_terminal> listData = new List<kompleks_terminal>();
            kompleks_terminal hData;

            try
            {
                DataGridViewRow sRow;
                for (int i = 0; i < terminalGrid.Rows.Count; i++)
                {
                    sRow = terminalGrid.Rows[i];

                    if (sRow.Cells["terminalIDValue"].Value != null)
                    {
                        if (Convert.ToInt32(sRow.Cells["id"].Value) == 0 &&
                            sRow.Cells["flag"].Value.ToString() == "0")
                            continue;

                        hData = new kompleks_terminal();

                        hData.id = Convert.ToInt32(sRow.Cells["id"].Value);
                        hData.kompleks_id = selectedKompleksID;
                        hData.terminal_id = sRow.Cells["terminalIDValue"].Value.ToString();
                        hData.terminal_type = sRow.Cells["terminalType"].Value.ToString();
                        hData.is_active = (sRow.Cells["flag"].Value.ToString() == "1" ? "Y" : "N");

                        listData.Add(hData);
                    }
                }

                int newID = 0;
                if (!gRest.saveDataKompleksTerminal(gUser.getUserID(), gUser.getUserToken(), listData,
                    out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
            }

            return result;
        }

        private bool dataValid()
        {
            errorLabel.Text = "";
            if (terminalGrid.Rows.Count <= 0)
            {
                errorLabel.Text = "Tidak ada data terminal";
                return false;
            }

            DataGridViewRow sRow;
            string terminalID = "";
            int lineID = 0;
            List<string> idList = new List<string>();

            for (int i = 0; i < terminalGrid.Rows.Count; i++)
            {
                sRow = terminalGrid.Rows[i];

                if (null == sRow.Cells["terminalIDValue"].Value || sRow.Cells["terminalIDValue"].Value.ToString().Length <= 0)
                {
                    errorLabel.Text = "Terminal ID kosong";
                    sRow.Cells["terminalIDValue"].Selected = true;
                    return false;
                }

                lineID = Convert.ToInt32(sRow.Cells["id"].Value);
                terminalID = sRow.Cells["terminalIDValue"].Value.ToString();

                if (idList.Contains(terminalID))
                {
                    errorLabel.Text = "Terminal ID [" + terminalID + "] sudah ada";
                    return false;
                }

                if (gIPL.terminalExist(terminalID, lineID))
                {
                    if (!gIPL.getTerminalStatus(terminalID))
                    {
                        if (DialogResult.No == MessageBox.Show("Terminal [" + terminalID + "] status tidak aktif, diaktifkan di kompleks [" + kompleksTextBox.Text + "] ? ", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                        {
                            return false;
                        }

                        lineID = gIPL.getTerminalID(terminalID);
                        sRow.Cells["id"].Value = lineID;
                    }
                    else
                    {
                        if (lineID == 0 && 
                            sRow.Cells["flag"].Value.ToString() == "1")
                        {
                            errorLabel.Text = "Terminal ID [" + terminalID + "] sudah ada";
                            return false;
                        }
                    }
                }

                idList.Add(terminalID);
            }
            return true;
        }

        private bool saveData()
        {
            string errMsg = "";

            if (dataValid())
            {
                return saveTrans(out errMsg);
            }

            return false;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Save Data Terminal ?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {
                if (saveData())
                {
                    MessageBox.Show("Success");
                    this.Close();
                }
            }
        }

        private void deleteCurrentRow()
        {
            if (terminalGrid.Rows.Count > 0)
            {
                if (DialogResult.Yes == MessageBox.Show("Hapus baris ?", "WARNING", 
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    int rowIndex = terminalGrid.SelectedCells[0].RowIndex;
                    DataGridViewRow selectedRow = terminalGrid.Rows[rowIndex];

                    selectedRow.Cells["flag"].Value = 0;
                    selectedRow.Visible = false;
                }
            }
        }

        private void terminalGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (terminalGrid.Rows.Count > 0)
                if (e.KeyCode == Keys.Delete)
                {
                    deleteCurrentRow();
                }
        }

        private void dataTerminalForm_Load(object sender, EventArgs e)
        {
            loadStatusCombo();
        }

        private void terminalGrid_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (terminalGrid.IsCurrentCellDirty)
            {
                terminalGrid.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }
    }
}
