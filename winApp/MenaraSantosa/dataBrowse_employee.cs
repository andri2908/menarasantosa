﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class dataBrowse_employee : AlphaSoft.simpleBrowseForm
    {
        private int originModuleID = 0;
        private globalUserUtil gUtil;
        private globalRestAPI gRest;
        private int paramID = 0;

        public string retUserFullName = "";

        REST_empLoginData empLoginData = new REST_empLoginData();

        public dataBrowse_employee(int moduleID = 0, int paramIDValue = 0, string formTitle = "")
        {
            InitializeComponent();
            originModuleID = moduleID;

            paramID = paramIDValue;

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();

            ReturnValue1 = "0";
            ReturnValue2 = "0";

            if (formTitle.Length > 0)
            {
                this.Text = formTitle;
            }
        }

        private void dataBrowse_employee_Load(object sender, EventArgs e)
        {
            displayAllAction();

            if (!gUtil.RS_userHasAccessTo(MMConstants.TAMBAH_HAPUS_EMPLOYEE,
                MMConstants.HAK_NEW_DATA))
                setNewButtonEnable(false);
        }

        private void displayDefault()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string userName = "";

            dataGridView.DataSource = null;

            userName = getNamaTextBox();

            sqlCommand = "SELECT mu.* " +
                                    "FROM employee_login_data mu " +
                                    "WHERE 1 = 1 ";

            if (userName.Length > 0)
                sqlCommand = sqlCommand + "AND mu.employee_user_name LIKE '%" + userName + "%' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND mu.is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY mu.employee_user_name ASC";

            if (gRest.getEmployeeLoginData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref empLoginData))
            {
                dt.Columns.Add("EMPLOYEE_ID");
                dt.Columns.Add("USERNAME STAFF");
                dt.Columns.Add("NAMA LENGKAP STAFF");

                for (int i = 0; i < empLoginData.dataList.Count; i++)
                {
                    DataRow r = dt.Rows.Add();
                    r["EMPLOYEE_ID"] = empLoginData.dataList[i].employee_id;
                    r["USERNAME STAFF"] = empLoginData.dataList[i].employee_user_name;
                    r["NAMA LENGKAP STAFF"] = empLoginData.dataList[i].employee_full_name;
                }

                dataGridView.DataSource = dt;
                dataGridView.Columns["EMPLOYEE_ID"].Visible = false;
            }
        }

        protected override void displayAllAction()
        {
            switch (originModuleID)
            {
                default:
                    displayDefault();
                    break;
            }
        }

        protected override void newButtonAction()
        {
            dataEmployeeDetailForm displayForm = new dataEmployeeDetailForm();
            displayForm.ShowDialog(this);
        }

        protected override void dbGridDoubleClickAction(DataGridViewRow selectedRow)
        {
            int selectedUserID = Convert.ToInt32(selectedRow.Cells["EMPLOYEE_ID"].Value);

            switch (originModuleID)
            {
                case globalConstants.MODULE_DEFAULT:
                    ReturnValue1 = selectedUserID.ToString();
                    ReturnValue2 = selectedRow.Cells["USERNAME STAFF"].Value.ToString();
                    retUserFullName = selectedRow.Cells["NAMA LENGKAP STAFF"].Value.ToString();

                    this.Close();
                    break;

                default:
                    dataEmployeeDetailForm displayForm = new dataEmployeeDetailForm(selectedUserID);
                    displayForm.ShowDialog(this);
                    break;
            }
        }
    }
}
