﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace AlphaSoft
{
    public partial class dataRFIDForm : AlphaSoft.basicHotkeysForm
    {
        private int originModuleID = 0;
        private int selectedKavlingID = 0;
        private int selectedKompleksID = 0;
        private int selectedUserID = 0;
        private int userKavlingID = 0;

        private globalUserUtil gUser;
        private globalIPL gIPL;
        private globalRestAPI gRest;

        private CultureInfo culture = new CultureInfo("id-ID");

        public dataRFIDForm()
        {
            InitializeComponent();

            gUser = new globalUserUtil();
            gIPL = new globalIPL();
            gRest = new globalRestAPI();
        }

        private void searchKompleksButton_Click(object sender, EventArgs e)
        {
            dataBrowse_KompleksForm displayForm = new dataBrowse_KompleksForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                selectedKompleksID = Convert.ToInt32(displayForm.ReturnValue1);
                kompleksTextBox.Text = displayForm.ReturnValue2;

                searchBlokButton.Enabled = true;
            }
        }

        private void loadRFIDGrid()
        {
            string sqlCommand = "";
            REST_userKavlingRFID rfidData = new REST_userKavlingRFID();

            rfidGrid.Rows.Clear();

            sqlCommand = "SELECT uk.id, uk.user_kavling_id, uk.rfid_value, uk.rfid_enabled, uk.is_active " +
                                   "FROM user_kavling_rfid uk " +
                                   "WHERE uk.user_kavling_id = " + userKavlingID + " " +
                                   "AND uk.is_active = 'Y'";

            sqlCommand += "ORDER BY uk.rfid_value ASC";

            if (gRest.getUserKavlingRFIDData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref rfidData))
            {
                if (rfidData.dataStatus.o_status == 1)
                {
                    for (int i = 0; i < rfidData.dataList.Count; i++)
                    {
                        rfidGrid.Rows.Add(
                            rfidData.dataList[i].id,
                            (rfidData.dataList[i].rfid_enabled == "Y" ? true : false),
                            rfidData.dataList[i].rfid_value,
                            1
                            );
                    }
                }
            }
        }

        private int getUserKavlingID()
        {
            int userKav = 0;
            REST_userKavling userKavData = new REST_userKavling();

            string sqlCommand = "SELECT uk.id, " +
                                            "uk.user_id, ul.user_full_name, ul.user_name, uk.start_ipl, uk.remarks, uk.user_qr_code, uk.user_qr_enabled " +
                                            "FROM user_kavling uk " +
                                            "LEFT OUTER JOIN user_login_data ul ON (uk.user_id = ul.user_id) " +
                                            "WHERE uk.is_active = 'Y' " +
                                            "AND uk.kavling_id = " + selectedKavlingID + " " +
                                            "AND uk.user_id = " + selectedUserID;

            if (gRest.getUserKavlingData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref userKavData))
            {
                if (userKavData.dataStatus.o_status == 1)
                {
                    userKav = userKavData.dataList[0].id;
                }
            }

            return userKav;
        }

        private void searchBlokButton_Click(object sender, EventArgs e)
        {
            dataBrowse_KavlingForm displayForm = new dataBrowse_KavlingForm(globalConstants.SUMMARY_IPL, selectedKompleksID);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                rfidGrid.Rows.Clear();
                selectedUserID = 0;
                namaUserTextBox.Text = "";

                selectedKavlingID = Convert.ToInt32(displayForm.ReturnValue1);
                namaBlokTextBox.Text = displayForm.ReturnValue2;

                if (gIPL.userAssignedExist(selectedKavlingID))
                {
                    searchUserButton.Enabled = true;
                }
                else
                {
                    searchUserButton.Enabled = false;
                    rfidGrid.Rows.Clear();
                }
            }
        }

        private void searchUserButton_Click(object sender, EventArgs e)
        {
            dataBrowse_UserForm displayForm = new dataBrowse_UserForm(globalConstants.USER_RFID, selectedKavlingID);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                selectedUserID = Convert.ToInt32(displayForm.ReturnValue1);
                namaUserTextBox.Text = displayForm.ReturnValue2;

                userKavlingID = getUserKavlingID();

                loadRFIDGrid();
            }
        }

        private void newRFID_Click(object sender, EventArgs e)
        {
            rfidGrid.Rows.Add(0, true, "", 1);
        }

        private bool saveTrans(out string errMsg)
        {
            bool result = false;
            errMsg = "";
            string enabledValue = "";

            List<user_kavling_rfid> listData = new List<user_kavling_rfid>();
            user_kavling_rfid hData;

            List<user_access_list> listUserAccessList = new List<user_access_list>();
            user_access_list userAccessList;

            int userAccessStatus = 0;
            string userAccessMsg = "";

            try
            {
                DataGridViewRow sRow;
                for (int i = 0;i<rfidGrid.Rows.Count;i++)
                {
                    sRow = rfidGrid.Rows[i];

                    if (sRow.Cells["rfidValue"].Value != null)
                    {
                        if (Convert.ToInt32(sRow.Cells["id"].Value) == 0 &&
                            sRow.Cells["flag"].Value.ToString() == "0")
                            continue;

                        hData = new user_kavling_rfid();

                        hData.id = Convert.ToInt32(sRow.Cells["id"].Value);
                        hData.user_kavling_id = userKavlingID;
                        hData.rfid_enabled = (Convert.ToBoolean(sRow.Cells["accessStatus"].Value) ? "Y" : "N");
                        hData.rfid_value = sRow.Cells["rfidValue"].Value.ToString();
                        hData.is_active = (sRow.Cells["flag"].Value.ToString() == "1" ? "Y" : "N");

                        listData.Add(hData);

                        enabledValue = (Convert.ToBoolean(sRow.Cells["accessStatus"].Value) ? "Y" : "N");

                        if (enabledValue == "Y")
                        {
                            userAccessStatus = 1;
                            userAccessMsg = "Welcome....";

                            if (gIPL.isLateOutstandingTagihanExist(selectedUserID, selectedKavlingID))
                            {
                                userAccessStatus = 2;
                                userAccessMsg = "Tagihan IPL Terlambat....";
                            }
                        }
                        else
                        {
                            userAccessStatus = 3;
                            userAccessMsg = "RFID Blocked....";
                        }

                        userAccessList = new user_access_list();
                        userAccessList.id = 0;
                        userAccessList.user_id = selectedUserID;
                        userAccessList.kompleks_id = selectedKompleksID;
                        userAccessList.kavling_id = selectedKavlingID;
                        userAccessList.access_id = sRow.Cells["rfidValue"].Value.ToString();
                        userAccessList.access_type = "RFID";
                        userAccessList.access_status = userAccessStatus;// (enabledValue == "Y" ? 1 : 3);
                        userAccessList.message = userAccessMsg;// (enabledValue == "Y" ? "Welcome...." : "RFID Blocked....");
                        userAccessList.is_active = (sRow.Cells["flag"].Value.ToString() == "1" ? "Y" : "N");
                        listUserAccessList.Add(userAccessList);
                    }
                }

                int newID = 0;
                if (!gRest.saveDataUserKavlingRFID(gUser.getUserID(), gUser.getUserToken(), listData,
                    out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                if (!gRest.saveDataUserAccessList(gUser.getUserID(), gUser.getUserToken(), listUserAccessList, out errMsg))
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
            }

            return result;
        }

        private bool dataValid()
        {
            errorLabel.Text = "";
            if (rfidGrid.Rows.Count <= 0)
            {
                errorLabel.Text = "Tidak ada data RFID";
                return false;
            }

            if (userKavlingID == 0)
            {
                errorLabel.Text = "User belum dipilih";
                return false;
            }

            DataGridViewRow sRow;
            string rfidValue = "";
            int userRFID = 0;

            List<string> rfidList = new List<string>();

            for (int i = 0; i < rfidGrid.Rows.Count; i++)
            {
                sRow = rfidGrid.Rows[i];

                if (null == sRow.Cells["rfidValue"].Value || sRow.Cells["rfidValue"].Value.ToString().Length <= 0)
                {
                    errorLabel.Text = "RFID kosong";
                    sRow.Cells["rfidValue"].Selected = true;
                    return false;
                }

                if (Convert.ToBoolean(sRow.Cells["accessStatus"].Value) == false ||
                    sRow.Cells["flag"].Value.ToString() == "0")
                    continue;

                userRFID = Convert.ToInt32(sRow.Cells["id"].Value);
                rfidValue = sRow.Cells["rfidValue"].Value.ToString();

                if (rfidList.Contains(rfidValue) || gIPL.rfidExist(rfidValue, userRFID))
                {
                    if (!rfidList.Contains(rfidValue))
                    {
                        if (userRFID == 0) // data baru, tp sudah dipakai di blok lain
                        {
                            if (DialogResult.Yes == MessageBox.Show("RFID [" + rfidValue + "] sudah dipakai, update data yg sudah ada?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                            {
                                sRow.Cells["id"].Value = gIPL.getRFIDID(rfidValue);
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    else
                    {
                        errorLabel.Text = "RFID [" + rfidValue + "] sudah ada";
                        return false;
                    }
                }

                rfidList.Add(rfidValue);
            }
            return true;
        }

        private bool saveData()
        {
            string errMsg = "";

            if (dataValid())
            {
                return saveTrans(out errMsg);
            }

            return false;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Save Data RFID ?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {
                if (saveData())
                {
                    MessageBox.Show("Success");
                    gUser.ResetAllControls(this);
                    selectedKavlingID = 0;
                    selectedKompleksID = 0;
                    selectedUserID = 0;
                    userKavlingID = 0;
                    //this.Close();
                }
            }
        }

        private void deleteCurrentRow()
        {
            if (rfidGrid.Rows.Count > 0)
            {
                if (DialogResult.Yes == MessageBox.Show("Hapus baris ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    int rowIndex = rfidGrid.SelectedCells[0].RowIndex;
                    DataGridViewRow selectedRow = rfidGrid.Rows[rowIndex];

                    selectedRow.Cells["flag"].Value = 0;
                    selectedRow.Visible = false;
                }
            }
        }

        private void rfidGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (rfidGrid.Rows.Count > 0)
                if (e.KeyCode == Keys.Delete)
                {
                    deleteCurrentRow();
                }
        }

        private void rfidGrid_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (rfidGrid.IsCurrentCellDirty)
            {
                rfidGrid.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }
    }
}
