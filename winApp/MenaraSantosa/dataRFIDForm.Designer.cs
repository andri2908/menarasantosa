﻿namespace AlphaSoft
{
    partial class dataRFIDForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dataRFIDForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.newRFID = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.searchUserButton = new System.Windows.Forms.Button();
            this.namaUserTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.searchBlokButton = new System.Windows.Forms.Button();
            this.namaBlokTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rfidGrid = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accessStatus = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.rfidValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.searchKompleksButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.kompleksTextBox = new System.Windows.Forms.TextBox();
            this.SaveButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfidGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Size = new System.Drawing.Size(624, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.newRFID);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.searchUserButton);
            this.panel2.Controls.Add(this.namaUserTextBox);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.searchBlokButton);
            this.panel2.Controls.Add(this.namaBlokTextBox);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.rfidGrid);
            this.panel2.Controls.Add(this.searchKompleksButton);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.kompleksTextBox);
            this.panel2.Location = new System.Drawing.Point(28, 49);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(567, 571);
            this.panel2.TabIndex = 23;
            // 
            // newRFID
            // 
            this.newRFID.BackColor = System.Drawing.Color.FloralWhite;
            this.newRFID.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newRFID.ForeColor = System.Drawing.Color.Black;
            this.newRFID.Location = new System.Drawing.Point(140, 114);
            this.newRFID.Name = "newRFID";
            this.newRFID.Size = new System.Drawing.Size(90, 37);
            this.newRFID.TabIndex = 106;
            this.newRFID.Text = "New RFID";
            this.newRFID.UseVisualStyleBackColor = false;
            this.newRFID.Click += new System.EventHandler(this.newRFID_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(24, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 18);
            this.label4.TabIndex = 105;
            this.label4.Text = "RFID";
            // 
            // searchUserButton
            // 
            this.searchUserButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchUserButton.BackgroundImage")));
            this.searchUserButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchUserButton.Enabled = false;
            this.searchUserButton.Location = new System.Drawing.Point(399, 79);
            this.searchUserButton.Name = "searchUserButton";
            this.searchUserButton.Size = new System.Drawing.Size(29, 30);
            this.searchUserButton.TabIndex = 104;
            this.searchUserButton.UseVisualStyleBackColor = true;
            this.searchUserButton.Click += new System.EventHandler(this.searchUserButton_Click);
            // 
            // namaUserTextBox
            // 
            this.namaUserTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.namaUserTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.namaUserTextBox.Location = new System.Drawing.Point(140, 81);
            this.namaUserTextBox.MaxLength = 5;
            this.namaUserTextBox.Name = "namaUserTextBox";
            this.namaUserTextBox.ReadOnly = true;
            this.namaUserTextBox.Size = new System.Drawing.Size(253, 27);
            this.namaUserTextBox.TabIndex = 103;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(91, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 18);
            this.label1.TabIndex = 102;
            this.label1.Text = "User";
            // 
            // searchBlokButton
            // 
            this.searchBlokButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchBlokButton.BackgroundImage")));
            this.searchBlokButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchBlokButton.Enabled = false;
            this.searchBlokButton.Location = new System.Drawing.Point(399, 46);
            this.searchBlokButton.Name = "searchBlokButton";
            this.searchBlokButton.Size = new System.Drawing.Size(29, 30);
            this.searchBlokButton.TabIndex = 101;
            this.searchBlokButton.UseVisualStyleBackColor = true;
            this.searchBlokButton.Click += new System.EventHandler(this.searchBlokButton_Click);
            // 
            // namaBlokTextBox
            // 
            this.namaBlokTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.namaBlokTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.namaBlokTextBox.Location = new System.Drawing.Point(140, 48);
            this.namaBlokTextBox.MaxLength = 5;
            this.namaBlokTextBox.Name = "namaBlokTextBox";
            this.namaBlokTextBox.ReadOnly = true;
            this.namaBlokTextBox.Size = new System.Drawing.Size(253, 27);
            this.namaBlokTextBox.TabIndex = 100;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(91, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 18);
            this.label3.TabIndex = 99;
            this.label3.Text = "Blok";
            // 
            // rfidGrid
            // 
            this.rfidGrid.AllowUserToAddRows = false;
            this.rfidGrid.AllowUserToDeleteRows = false;
            this.rfidGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rfidGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.rfidGrid.BackgroundColor = System.Drawing.Color.FloralWhite;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.rfidGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.rfidGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rfidGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.accessStatus,
            this.rfidValue,
            this.flag});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.rfidGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.rfidGrid.Location = new System.Drawing.Point(27, 153);
            this.rfidGrid.MultiSelect = false;
            this.rfidGrid.Name = "rfidGrid";
            this.rfidGrid.RowHeadersVisible = false;
            this.rfidGrid.Size = new System.Drawing.Size(505, 399);
            this.rfidGrid.TabIndex = 64;
            this.rfidGrid.CurrentCellDirtyStateChanged += new System.EventHandler(this.rfidGrid_CurrentCellDirtyStateChanged);
            this.rfidGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rfidGrid_KeyDown);
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.Visible = false;
            this.id.Width = 29;
            // 
            // accessStatus
            // 
            this.accessStatus.HeaderText = "";
            this.accessStatus.Name = "accessStatus";
            this.accessStatus.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.accessStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.accessStatus.Width = 19;
            // 
            // rfidValue
            // 
            this.rfidValue.HeaderText = "RFID";
            this.rfidValue.MaxInputLength = 36;
            this.rfidValue.Name = "rfidValue";
            this.rfidValue.Width = 72;
            // 
            // flag
            // 
            this.flag.HeaderText = "flag";
            this.flag.Name = "flag";
            this.flag.Visible = false;
            this.flag.Width = 64;
            // 
            // searchKompleksButton
            // 
            this.searchKompleksButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchKompleksButton.BackgroundImage")));
            this.searchKompleksButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchKompleksButton.Location = new System.Drawing.Point(447, 13);
            this.searchKompleksButton.Name = "searchKompleksButton";
            this.searchKompleksButton.Size = new System.Drawing.Size(29, 30);
            this.searchKompleksButton.TabIndex = 60;
            this.searchKompleksButton.UseVisualStyleBackColor = true;
            this.searchKompleksButton.Click += new System.EventHandler(this.searchKompleksButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(44, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 18);
            this.label2.TabIndex = 58;
            this.label2.Text = "Kompleks";
            // 
            // kompleksTextBox
            // 
            this.kompleksTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.kompleksTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kompleksTextBox.Location = new System.Drawing.Point(140, 15);
            this.kompleksTextBox.MaxLength = 30;
            this.kompleksTextBox.Name = "kompleksTextBox";
            this.kompleksTextBox.ReadOnly = true;
            this.kompleksTextBox.Size = new System.Drawing.Size(300, 27);
            this.kompleksTextBox.TabIndex = 59;
            // 
            // SaveButton
            // 
            this.SaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SaveButton.BackColor = System.Drawing.Color.FloralWhite;
            this.SaveButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveButton.ForeColor = System.Drawing.Color.Black;
            this.SaveButton.Location = new System.Drawing.Point(239, 637);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(182, 37);
            this.SaveButton.TabIndex = 63;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = false;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // dataRFIDForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(624, 715);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.panel2);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "dataRFIDForm";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.SaveButton, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfidGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        protected System.Windows.Forms.DataGridView rfidGrid;
        private System.Windows.Forms.Button searchKompleksButton;
        protected System.Windows.Forms.Label label2;
        protected System.Windows.Forms.TextBox kompleksTextBox;
        private System.Windows.Forms.Button searchBlokButton;
        protected System.Windows.Forms.TextBox namaBlokTextBox;
        protected System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button searchUserButton;
        protected System.Windows.Forms.TextBox namaUserTextBox;
        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.Label label4;
        protected System.Windows.Forms.Button newRFID;
        protected System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewCheckBoxColumn accessStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn rfidValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn flag;
    }
}
