﻿namespace AlphaSoft
{
    partial class dataKompleksDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dataKompleksDetailForm));
            this.biayaIPLTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.durasiBayarTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.blokGridView = new System.Windows.Forms.DataGridView();
            this.isActive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.blokID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.blokName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.img1Button = new System.Windows.Forms.Button();
            this.pbBox = new System.Windows.Forms.PictureBox();
            this.imgFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.imgLogoButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.blokGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.imgLogoButton);
            this.groupBox1.Controls.Add(this.pbLogo);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.img1Button);
            this.groupBox1.Controls.Add(this.pbBox);
            this.groupBox1.Controls.Add(this.blokGridView);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.durasiBayarTextBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.biayaIPLTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Size = new System.Drawing.Size(874, 354);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.Controls.SetChildIndex(this.namaTextBox, 0);
            this.groupBox1.Controls.SetChildIndex(this.label3, 0);
            this.groupBox1.Controls.SetChildIndex(this.label2, 0);
            this.groupBox1.Controls.SetChildIndex(this.deskripsiTextBox, 0);
            this.groupBox1.Controls.SetChildIndex(this.nonAktifCheckbox, 0);
            this.groupBox1.Controls.SetChildIndex(this.label1, 0);
            this.groupBox1.Controls.SetChildIndex(this.biayaIPLTextBox, 0);
            this.groupBox1.Controls.SetChildIndex(this.label4, 0);
            this.groupBox1.Controls.SetChildIndex(this.durasiBayarTextBox, 0);
            this.groupBox1.Controls.SetChildIndex(this.label5, 0);
            this.groupBox1.Controls.SetChildIndex(this.blokGridView, 0);
            this.groupBox1.Controls.SetChildIndex(this.pbBox, 0);
            this.groupBox1.Controls.SetChildIndex(this.img1Button, 0);
            this.groupBox1.Controls.SetChildIndex(this.label6, 0);
            this.groupBox1.Controls.SetChildIndex(this.label7, 0);
            this.groupBox1.Controls.SetChildIndex(this.pbLogo, 0);
            this.groupBox1.Controls.SetChildIndex(this.imgLogoButton, 0);
            // 
            // ResetButton
            // 
            this.ResetButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ResetButton.Location = new System.Drawing.Point(464, 413);
            this.ResetButton.Size = new System.Drawing.Size(172, 37);
            this.ResetButton.TabIndex = 10;
            // 
            // SaveButton
            // 
            this.SaveButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.SaveButton.Location = new System.Drawing.Point(259, 413);
            this.SaveButton.Size = new System.Drawing.Size(172, 37);
            this.SaveButton.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.Location = new System.Drawing.Point(175, 20);
            this.label2.Size = new System.Drawing.Size(148, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "Nama Kompleks";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.Location = new System.Drawing.Point(177, 75);
            this.label3.Size = new System.Drawing.Size(158, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Alamat Kompleks";
            // 
            // nonAktifCheckbox
            // 
            this.nonAktifCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.nonAktifCheckbox.Location = new System.Drawing.Point(178, 314);
            this.nonAktifCheckbox.TabIndex = 8;
            // 
            // namaTextBox
            // 
            this.namaTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.namaTextBox.Location = new System.Drawing.Point(178, 41);
            this.namaTextBox.Size = new System.Drawing.Size(690, 27);
            this.namaTextBox.TabIndex = 6;
            // 
            // deskripsiTextBox
            // 
            this.deskripsiTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.deskripsiTextBox.Location = new System.Drawing.Point(180, 96);
            this.deskripsiTextBox.Multiline = true;
            this.deskripsiTextBox.Size = new System.Drawing.Size(361, 132);
            this.deskripsiTextBox.TabIndex = 7;
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(895, 29);
            this.panel1.TabIndex = 1;
            // 
            // errorLabel
            // 
            this.errorLabel.TabIndex = 2;
            // 
            // biayaIPLTextBox
            // 
            this.biayaIPLTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.biayaIPLTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.biayaIPLTextBox.Location = new System.Drawing.Point(296, 234);
            this.biayaIPLTextBox.MaxLength = 20;
            this.biayaIPLTextBox.Name = "biayaIPLTextBox";
            this.biayaIPLTextBox.Size = new System.Drawing.Size(245, 27);
            this.biayaIPLTextBox.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(173, 237);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 18);
            this.label1.TabIndex = 17;
            this.label1.Text = "Nominal IPL";
            // 
            // durasiBayarTextBox
            // 
            this.durasiBayarTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.durasiBayarTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.durasiBayarTextBox.Location = new System.Drawing.Point(296, 272);
            this.durasiBayarTextBox.MaxLength = 2;
            this.durasiBayarTextBox.Name = "durasiBayarTextBox";
            this.durasiBayarTextBox.Size = new System.Drawing.Size(43, 27);
            this.durasiBayarTextBox.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(173, 275);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 18);
            this.label4.TabIndex = 19;
            this.label4.Text = "Pembayaran";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(345, 277);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 16);
            this.label5.TabIndex = 54;
            this.label5.Text = "bulan";
            // 
            // blokGridView
            // 
            this.blokGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.blokGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.blokGridView.BackgroundColor = System.Drawing.Color.FloralWhite;
            this.blokGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.blokGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.isActive,
            this.blokID,
            this.blokName});
            this.blokGridView.Location = new System.Drawing.Point(547, 75);
            this.blokGridView.Name = "blokGridView";
            this.blokGridView.RowHeadersVisible = false;
            this.blokGridView.Size = new System.Drawing.Size(321, 259);
            this.blokGridView.TabIndex = 55;
            this.blokGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.blokGridView_CellFormatting);
            this.blokGridView.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.blokGridView_RowsAdded);
            this.blokGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.blokGridView_KeyDown);
            // 
            // isActive
            // 
            this.isActive.HeaderText = "isActive";
            this.isActive.Name = "isActive";
            this.isActive.Visible = false;
            this.isActive.Width = 90;
            // 
            // blokID
            // 
            this.blokID.HeaderText = "blokID";
            this.blokID.Name = "blokID";
            this.blokID.Visible = false;
            this.blokID.Width = 79;
            // 
            // blokName
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.blokName.DefaultCellStyle = dataGridViewCellStyle1;
            this.blokName.HeaderText = "Blok";
            this.blokName.Name = "blokName";
            this.blokName.Width = 76;
            // 
            // img1Button
            // 
            this.img1Button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("img1Button.BackgroundImage")));
            this.img1Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.img1Button.Location = new System.Drawing.Point(143, 41);
            this.img1Button.Name = "img1Button";
            this.img1Button.Size = new System.Drawing.Size(29, 30);
            this.img1Button.TabIndex = 204;
            this.img1Button.UseVisualStyleBackColor = true;
            this.img1Button.Click += new System.EventHandler(this.img1Button_Click);
            // 
            // pbBox
            // 
            this.pbBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbBox.Location = new System.Drawing.Point(16, 41);
            this.pbBox.Name = "pbBox";
            this.pbBox.Size = new System.Drawing.Size(124, 174);
            this.pbBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbBox.TabIndex = 203;
            this.pbBox.TabStop = false;
            // 
            // imgFileDialog
            // 
            this.imgFileDialog.FileName = "openImgDialog";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(13, 226);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 16);
            this.label6.TabIndex = 205;
            this.label6.Text = "Logo Kompleks";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(13, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(139, 16);
            this.label7.TabIndex = 206;
            this.label7.Text = "Gambar Kompleks";
            // 
            // pbLogo
            // 
            this.pbLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbLogo.Location = new System.Drawing.Point(16, 245);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(73, 93);
            this.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLogo.TabIndex = 207;
            this.pbLogo.TabStop = false;
            // 
            // imgLogoButton
            // 
            this.imgLogoButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("imgLogoButton.BackgroundImage")));
            this.imgLogoButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgLogoButton.Location = new System.Drawing.Point(95, 245);
            this.imgLogoButton.Name = "imgLogoButton";
            this.imgLogoButton.Size = new System.Drawing.Size(29, 30);
            this.imgLogoButton.TabIndex = 208;
            this.imgLogoButton.UseVisualStyleBackColor = true;
            this.imgLogoButton.Click += new System.EventHandler(this.imgLogoButton_Click);
            // 
            // dataKompleksDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(896, 497);
            this.Name = "dataKompleksDetailForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.dataKompleksDetailForm_FormClosed);
            this.Load += new System.EventHandler(this.dataKompleksDetailForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.blokGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.TextBox biayaIPLTextBox;
        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.TextBox durasiBayarTextBox;
        protected System.Windows.Forms.Label label4;
        protected System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView blokGridView;
        private System.Windows.Forms.Button img1Button;
        private System.Windows.Forms.PictureBox pbBox;
        private System.Windows.Forms.OpenFileDialog imgFileDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn isActive;
        private System.Windows.Forms.DataGridViewTextBoxColumn blokID;
        private System.Windows.Forms.DataGridViewTextBoxColumn blokName;
        protected System.Windows.Forms.Label label7;
        protected System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button imgLogoButton;
        private System.Windows.Forms.PictureBox pbLogo;
    }
}
