﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using MySql.Data;
using MySql.Data.MySqlClient;
using System.Globalization;

namespace AlphaSoft
{
    public partial class dataPMDetailForm : AlphaSoft.basicHotkeysForm
    {
        Data_Access DS = new Data_Access();

        private int originModuleID = 0;
        private int selectedPMID = 0;
        private globalDBUtil globalDB;
        private globalUserUtil gUser;
        private globalLogUtil gLog;

        private CultureInfo culture = new CultureInfo("id-ID");

        private string[] dataPM =
       {
            "PM_NAME",
            "PM_TYPE",
            "PM_DEFAULT_VALUE",
            "PM_ACTIVE",
            "PM_ID"
        };

        public dataPMDetailForm(int moduleID = 0, int idParam = 0)
        {
            InitializeComponent();
            selectedPMID = idParam;

            this.Text = "DATA PAYMENT METHOD";

            DS.sqlConnect();

            globalDB = new globalDBUtil(DS);
            gUser = new globalUserUtil(DS);
            gLog = new globalLogUtil(DS);

            if (selectedPMID > 0)
                originModuleID = globalConstants.EDIT_PM;
            else
                originModuleID = globalConstants.NEW_PM;
        }

        private void dataPMDetailForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            DS.sqlClose();
        }

        private void loadPMInformation()
        {
            MySqlDataReader rdr;
            DataTable dt = new DataTable();

            using (rdr = DS.getData("SELECT * FROM MASTER_PAYMENT_METHOD WHERE PM_ID =  " + selectedPMID))
            {
                if (rdr.HasRows)
                {
                    dt.Load(rdr);
                }
            }
            rdr.Close();

            foreach (DataRow row in dt.Rows)
            {
                namaTextBox.Text = row["PM_NAME"].ToString();

                if (Convert.ToInt32(row["PM_TYPE"]) == globalConstants.PM_TYPE_CASH)
                    radioCash.Checked = true;
                else
                    radioTempo.Checked = true;

                defPembayaranTextBox.Text = Convert.ToInt32(row["PM_DEFAULT_VALUE"]).ToString("N0", culture);
                nonAktifCheckbox.Checked = (row["PM_ACTIVE"].ToString() == "1" ? false : true);
            }
        }

        private void dataPMDetailForm_Load(object sender, EventArgs e)
        {
            if (!gUser.userHasAccessTo(MMConstants.PENGATURAN_PAYMENT_METHOD,
                     MMConstants.HAK_UPDATE_DATA))
            {
                SaveButton.Enabled = false;
                resetButton.Enabled = false;
            }

            if (selectedPMID> 0)
                loadPMInformation();

            namaTextBox.Focus();
            gUser.reArrangeTabOrder(this);
        }

        private bool dataValid()
        {
            errorLabel.Text = "";

            if (namaTextBox.Text.Length <= 0)
            {
                errorLabel.Text = "INPUT NAMA KOSONG";
                return false;
            }

            int tempVal = 0;
            if (defPembayaranTextBox.Text.Length <= 0 ||
                !int.TryParse(defPembayaranTextBox.Text, NumberStyles.Number, culture, out tempVal))
            {
                errorLabel.Text = "INPUT DEFAULT PEMBAYARAN SALAH";
                return false;
            }

            return true;
        }

        private bool saveDataTrans()
        {
            bool result = false;
            string sqlCommand = "";
            MySqlException sqlEx = null;
            int defPembayaran = 0;

            List<string> pList = new List<string>();
            List<object> pVal = new List<object>();

            DS.beginTransaction();

            try
            {
                int.TryParse(defPembayaranTextBox.Text, NumberStyles.Number, culture, out defPembayaran);

                pList.Clear();
                pVal.Clear();

                globalDB.addParamList(pList, dataPM);

                switch (originModuleID)
                {
                    case globalConstants.NEW_PM:
                        sqlCommand = globalDB.constructMasterQuery("MASTER_PAYMENT_METHOD", dataPM, pList);
                        break;

                    case globalConstants.EDIT_PM:
                        sqlCommand = globalDB.constructMasterQuery("MASTER_PAYMENT_METHOD", dataPM, pList, 1, 1);
                        break;
                }

                pVal.Add(namaTextBox.Text);
                pVal.Add((radioCash.Checked ? globalConstants.PM_TYPE_CASH : globalConstants.PM_TYPE_TEMPO));
                pVal.Add(defPembayaran);
                pVal.Add((nonAktifCheckbox.Checked ? 0 : 1));
                pVal.Add(selectedPMID);

                if (!DS.executeNonQueryCommandWithParameters(sqlCommand, ref sqlEx, pList, pVal))
                    throw sqlEx;

                DS.commit();
                result = true;
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        private bool saveData()
        {
            if (dataValid())
            {
                return saveDataTrans();
            }

            return false;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (saveData())
            {
                MessageBox.Show("SUCCESS");
                this.Close();
            }
            else
            {
                MessageBox.Show("FAIL");
            }
        }

        private void TextBox_Enter(object sender, EventArgs e)
        {
            TextBox txtBox;
            txtBox = (TextBox)sender;

            if (txtBox.Text.Length > 0)
            {
                int tempValue = 0;

                if (int.TryParse(txtBox.Text, NumberStyles.Number, culture, out tempValue))
                {
                    txtBox.Text = tempValue.ToString(culture);
                }
            }
        }

        private void TextBox_LEAVE(object sender, EventArgs e)
        {
            TextBox txtBox;
            txtBox = (TextBox)sender;

            if (txtBox.Text.Length > 0)
            {
                int tempValue = 0;

                if (int.TryParse(txtBox.Text, NumberStyles.Number, culture, out tempValue))
                {
                    txtBox.Text = tempValue.ToString("N0", culture);
                }
            }
        }
    }
}
