﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using DBGridExtension;
using Hotkeys;

namespace AlphaSoft
{
    public partial class dataPenerimaanForm : AlphaSoft.basicHotkeysForm
    {
        private int originModuleID = 0;
        
        private double globalTotalValue = 0;
        private int transStatus = 1;
        private int selectedIDTrans = 0;
        private int transModule = 0;
        private bool isViewOnly = false;

        private globalUserUtil gUser;
        private globalItemLib gItem;
        private globalRestAPI gRest;

        private CultureInfo culture = new CultureInfo("id-ID");

        private Hotkeys.GlobalHotkey ghk_F4;
        private Hotkeys.GlobalHotkey ghk_F5;
        private Hotkeys.GlobalHotkey ghk_F9;
        private Hotkeys.GlobalHotkey ghk_F10;

        public dataPenerimaanForm(int moduleID = 0, int penerimaanID = 0, bool viewOnly = false)
        {
            InitializeComponent();

            gUser = new globalUserUtil();
            gItem = new globalItemLib();
            gRest = new globalRestAPI();

            transModule = moduleID;
            isViewOnly = viewOnly;

            if (penerimaanID > 0)
            {
                selectedIDTrans = penerimaanID;
                originModuleID = globalConstants.EDIT_ITEM;
            }
            else
                originModuleID = globalConstants.NEW_ITEM;
        }

        protected override void captureAll(Keys key)
        {
            base.captureAll(key);

            if (isViewOnly)
                return;

            switch (key)
            {
                case Keys.F5:
                    resetScreen();
                    break;

                case Keys.F9:
                    saveData();
                    break;

                case Keys.F4:
                    displayProductForm();
                    break;

                case Keys.F10:
                    if (originModuleID == globalConstants.EDIT_ITEM)
                    {
                        transStatus = 0;
                        saveData();
                    }
                    break;
            }
        }

        protected override void registerGlobalHotkey()
        {
            base.registerGlobalHotkey();

            ghk_F4 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F4, this);
            ghk_F4.Register();

            ghk_F5 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F5, this);
            ghk_F5.Register();

            ghk_F9 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F9, this);
            ghk_F9.Register();

            ghk_F10 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F10, this);
            ghk_F10.Register();
        }

        protected override void unregisterGlobalHotkey()
        {
            base.unregisterGlobalHotkey();

            ghk_F4.Unregister();
            ghk_F5.Unregister();
            ghk_F9.Unregister();
            ghk_F10.Unregister();
        }

        private void loadTransaksi()
        {
            string sqlCommand = "";
            DataTable dt = new DataTable();

            #region LOAD HEADER
            REST_penerimaanHeader headerData = new REST_penerimaanHeader();

            sqlCommand = "SELECT ih.* " +
                                    "FROM " +
                                    "penerimaan_header ih " +
                                    "WHERE ih.is_active = 'Y' " +
                                    "AND ih.penerimaan_id = " + selectedIDTrans + " ";

            if (gRest.getPenerimaanHeaderData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref headerData))
            {
                if (headerData.dataStatus.o_status == 1)
                {
                    dateDTPicker.Value = headerData.dataList[0].penerimaan_datetime;

                    globalTotalValue = headerData.dataList[0].penerimaan_total;
                    totalLabelValue.Text = globalTotalValue.ToString("C0", culture);

                    if (headerData.dataList[0].is_active == "N")
                        originModuleID = globalConstants.VIEW_ONLY;
                }
            }
            #endregion

            #region LOAD DETAIL
            REST_penerimaanDetail detailData = new REST_penerimaanDetail();
            sqlCommand = "SELECT id.*, IFNULL(mi.item_name, '') AS item_name, IFNULL(mu.unit_name, '') AS satuan " +
                                    "FROM " +
                                    "penerimaan_detail id " +
                                    "LEFT OUTER JOIN master_item mi ON (id.item_id = mi.item_id) " +
                                    "LEFT OUTER JOIN master_unit mu ON (mi.unit_id = mu.unit_id) " +
                                    "WHERE id.is_active = 'Y' " +
                                    "AND id.penerimaan_id = " + selectedIDTrans + " ";

            if (gRest.getPenerimaanDetailData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref detailData))
            {
                if (detailData.dataStatus.o_status == 1)
                {
                    transGridView.Rows.Clear();

                    for (int i = 0;i<detailData.dataList.Count;i++)
                    {
                        transGridView.Rows.Add(
                            0,
                            detailData.dataList[i].id,
                            detailData.dataList[i].item_id,
                            detailData.dataList[i].item_name,
                            detailData.dataList[i].item_qty,
                            detailData.dataList[i].satuan,
                            detailData.dataList[i].item_hpp,
                            detailData.dataList[i].item_subtotal,
                            detailData.dataList[i].item_qty
                            );
                    }
                }
            }
            #endregion
        }

        private void dataPenerimaanForm_Load(object sender, EventArgs e)
        {
            if (selectedIDTrans > 0)
            {
                loadTransaksi();
            }

            if (originModuleID == globalConstants.EDIT_ITEM)
            {
                labelHint.Text = "F4 : Pilih Barang | F5 : Reset | F9 : Save | F10 : Void | Del : Hapus Baris";
            }

            if (isViewOnly)
            {
                labelHint.Text = "";
                gUser.disableAllControls(this);
            }

            transGridView.CellValueChanged += CUSTOM_CellValueChanged;
            transGridView.DoubleBuffered(true);
        }

        private void CUSTOM_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (transGridView.Rows.Count <= 0)
                return;

            var cell = transGridView[e.ColumnIndex, e.RowIndex];
            DataGridViewRow sRow = transGridView.Rows[e.RowIndex];

            double tempValue = 0;
            double qty = 0;
            double hpp = 0;

            string productID = sRow.Cells["itemID"].Value.ToString();
            int lineID = Convert.ToInt32(sRow.Cells["lineID"].Value);

            string columnName = transGridView.Columns[e.ColumnIndex].Name;

            if (columnName == "itemQty" || columnName == "itemPrice")
            {
                if (null == sRow.Cells[columnName].Value ||
                    sRow.Cells[columnName].Value.ToString().Length <= 0 ||
                    !double.TryParse(sRow.Cells[columnName].Value.ToString(), NumberStyles.Number, culture, out tempValue))
                {
                    errorLabel.Text = "Input " + sRow.Cells[columnName].OwningColumn.HeaderText + " untuk " + sRow.Cells["itemName"].Value.ToString() + " salah";
                }
                else
                {
                    errorLabel.Text = "";
                    transGridView.CellValueChanged -= CUSTOM_CellValueChanged;
                    cell.Value = tempValue;

                    if (null != sRow.Cells["itemQty"].Value)
                        double.TryParse(sRow.Cells["itemQty"].Value.ToString(), out qty);

                    if (null != sRow.Cells["itemPrice"].Value)
                        double.TryParse(sRow.Cells["itemPrice"].Value.ToString(), out hpp);

                    sRow.Cells["subtotal"].Value = Math.Round(qty * hpp, 2);

                    calculateGlobalTotalValue();

                    transGridView.CellValueChanged += CUSTOM_CellValueChanged;
                }
            }
        }

        private void calculateGlobalTotalValue()
        {
            globalTotalValue = 0;
            DataGridViewRow sRow;

            for (int i = 0; i < transGridView.Rows.Count; i++)
            {
                sRow = transGridView.Rows[i];

                if (sRow.Cells["flagDelete"].Value.ToString() == "0")
                {
                    globalTotalValue += Convert.ToDouble(sRow.Cells["subtotal"].Value);
                }
            }

            totalLabelValue.Text = globalTotalValue.ToString("C0", culture);
        }

        private int getRowLineNo(string productIDParam)
        {
            int lineNo = -1;
            DataGridViewRow sRow;

            for (int i = 0; i < transGridView.Rows.Count && lineNo < 0; i++)
            {
                sRow = transGridView.Rows[i];

                if (sRow.Cells["itemID"].Value.ToString() == productIDParam &&
                    sRow.Cells["flagDelete"].Value.ToString() == "0")
                    lineNo = i;
            }

            return lineNo;
        }

        private void addRow(string productIDParam)
        {
            string sqlCommand = "";
            int lineNo = -1;

            REST_masterItem itemDetail = new REST_masterItem();

            lineNo = getRowLineNo(productIDParam);

            if (lineNo > -1)
            {
                transGridView.Rows[lineNo].Cells["itemQty"].Selected = true;
                return;
            }

            sqlCommand = "SELECT mi.*, mu.unit_name " +
                                    "FROM master_item mi, master_unit mu " +
                                    "WHERE mi.item_id = '" + productIDParam + "' " +
                                    "AND mi.unit_id = mu.unit_id";

            if (gRest.getMasterItemData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref itemDetail))
            {
                if (itemDetail.dataStatus.o_status == 1)
                {
                    transGridView.CellValueChanged -= CUSTOM_CellValueChanged;

                    transGridView.Rows.Add(
                        0,
                        0,
                        productIDParam,
                        itemDetail.dataList[0].item_name,
                        0,
                        itemDetail.dataList[0].unit_name,
                        itemDetail.dataList[0].item_hpp,
                        0,
                        0
                        );

                    transGridView.CellValueChanged += CUSTOM_CellValueChanged;
                }
            }
        }

        private void displayProductForm()
        {
            dataBrowse_ItemForm displayForm = new dataBrowse_ItemForm(transModule);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                addRow(displayForm.ReturnValue1);
            }
        }

        private void transGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (transGridView.IsCurrentCellDirty)
            {
                transGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void transGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if ((transGridView.Columns[e.ColumnIndex].Name == "itemQty" ||
             transGridView.Columns[e.ColumnIndex].Name == "itemPrice" ||
             transGridView.Columns[e.ColumnIndex].Name == "subtotal")
                && null != e.Value)
            {
                try
                {
                    double d = double.Parse(e.Value.ToString());
                    e.Value = d.ToString("N0", culture);
                }
                catch (Exception ex)
                {
                }
            }
        }

        private void deleteCurrentRow()
        {
            if (transGridView.Rows.Count > 0)
            {
                if (DialogResult.Yes == MessageBox.Show("Hapus baris ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    int rowIndex = transGridView.SelectedCells[0].RowIndex;
                    DataGridViewRow selectedRow = transGridView.Rows[rowIndex];

                    selectedRow.Cells["flagDelete"].Value = 1;
                    selectedRow.Visible = false;

                    calculateGlobalTotalValue();
                }
            }
        }

        private void transGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (transGridView.Rows.Count > 0)
                if (e.KeyCode == Keys.Delete)
                {
                    deleteCurrentRow();
                }
        }

        private bool dataValid()
        {
            errorLabel.Text = "";

            if (transGridView.Rows.Count <= 0)
            {
                errorLabel.Text = "Tidak ada item pada transaksi";
                return false;
            }

            if (globalTotalValue <= 0)
            {
                errorLabel.Text = "Nilai transaksi = 0 ";
                return false;
            }

            DataGridViewRow sRow;
            string[] colname = { "itemQty", "itemPrice" };
            double tempVal = 0;

            for (int i = 0; i < transGridView.Rows.Count; i++)
            {
                sRow = transGridView.Rows[i];

                if (sRow.Cells["flagDelete"].Value.ToString() == "0")
                {
                    for (int j = 0; j < colname.Length; j++)
                    {
                        if (null == sRow.Cells[colname[j]].Value ||
                            !double.TryParse(sRow.Cells[colname[j]].Value.ToString(), out tempVal))
                        {
                            errorLabel.Text = "Input " + sRow.Cells[colname[j]].OwningColumn.HeaderText + " untuk " + sRow.Cells["itemName"].Value.ToString() + " salah";
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        private void returnQtyBecauseOfVoid()
        {
            gRest.returnQtyVoid(gUser.getUserID(), gUser.getUserToken(), selectedIDTrans, 2);
        }

        private bool saveTrans(out string errMsg)
        {
            bool result = false;
            int opType = 0;

            errMsg = "";

            List<penerimaan_header> listHeader = new List<penerimaan_header>();
            penerimaan_header hData;

            List<penerimaan_detail> listDetail = new List<penerimaan_detail>();
            penerimaan_detail detailData;

            try
            {
                #region HEADER
                hData = new penerimaan_header();
                switch (originModuleID)
                {
                    case globalConstants.NEW_ITEM:
                        opType = 1;
                        hData.penerimaan_id = 0;
                        break;

                    case globalConstants.EDIT_ITEM:
                        opType = 2;
                        hData.penerimaan_id = selectedIDTrans;
                        break;
                }

                hData.type_trans = (transModule == globalConstants.MODULE_PENERIMAAN_BARANG ? globalConstants.TRANS_TYPE_RETAIL : globalConstants.TRANS_TYPE_RETAIL_DRUGS);
                hData.penerimaan_datetime = new DateTime(dateDTPicker.Value.Year, dateDTPicker.Value.Month, dateDTPicker.Value.Day);
                hData.penerimaan_total = globalTotalValue;
                hData.is_active = (transStatus == 1 ? "Y" : "N");

                listHeader.Add(hData);
                #endregion

                #region DETAIL
                DataGridViewRow sRow;

                int flagDelete = 0;
                int lineID = 0;
                int itemID = 0;
                double itemPrice = 0;
                double itemQty = 0;
                double itemOldQty = 0;
                double subtotal = 0;
                int i = 0;

                for (i = 0; i < transGridView.Rows.Count && transStatus == 1; i++)
                {
                    sRow = transGridView.Rows[i];

                    flagDelete = Convert.ToInt32(sRow.Cells["flagDelete"].Value);
                    lineID = Convert.ToInt32(sRow.Cells["lineID"].Value);

                    itemID = Convert.ToInt32(sRow.Cells["itemID"].Value);
                    itemPrice = Convert.ToDouble(sRow.Cells["itemPrice"].Value);
                    itemQty = Convert.ToDouble(sRow.Cells["itemQty"].Value);
                    subtotal = Convert.ToDouble(sRow.Cells["subtotal"].Value);
                    itemOldQty = Convert.ToDouble(sRow.Cells["itemOldQty"].Value);

                    if (lineID > 0)
                    {
                        detailData = new penerimaan_detail();
                        detailData.is_active = "Y";
                        detailData.old_qty = itemOldQty;
                        detailData.id = lineID;

                        if (flagDelete == 1) // HAPUS BARIS
                        {
                            detailData.is_active = "N";
                        }
                    }
                    else
                    {
                        if (flagDelete == 0)
                        {
                            detailData = new penerimaan_detail();
                            detailData.is_active = "Y";
                            detailData.old_qty = 0;
                            detailData.id = 0;
                        }
                        else
                            continue;
                    }

                    detailData.item_name = "";
                    detailData.satuan = "";
                    detailData.penerimaan_id = selectedIDTrans;
                    detailData.item_id = itemID;
                    detailData.item_qty = itemQty;
                    detailData.item_hpp = itemPrice;
                    detailData.item_subtotal = subtotal;

                    listDetail.Add(detailData);
                }
                #endregion

                int newID = 0;
                if (!gRest.saveDataPenerimaan(gUser.getUserID(), gUser.getUserToken(), listHeader, listDetail, 
                    opType, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                if (originModuleID == globalConstants.NEW_ITEM)
                    selectedIDTrans = newID;

                if (transStatus == 0) // RETURN QTY
                {
                    returnQtyBecauseOfVoid();
                }

                result = true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
            }

            return result;
        }

        private void saveData()
        {
            bool result = false;
            string warningMsg = "Save transaksi ?";
            string errMsg = "";

            if (transStatus == 0)
                warningMsg = "Void transaksi ?";

            if (DialogResult.Yes == MessageBox.Show(warningMsg, "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {
                if (dataValid())
                {
                    result = saveTrans(out errMsg);
                }
            }

            if (result)
            {
                MessageBox.Show("Success");

                if (transStatus == 1)
                    if (DialogResult.Yes == MessageBox.Show("Cetak Transaksi ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    {
                        printTransaksi();
                    }

                if (DialogResult.Yes == MessageBox.Show("Input lagi ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    resetScreen();
                else
                    this.Close();
            }
            else
            {
                MessageBox.Show("Fail " + errMsg);
            }
        }

        private void resetScreen()
        {
            gUser.ResetAllControls(this);

            originModuleID = globalConstants.NEW_ITEM;
            transModule = globalConstants.MODULE_PENERIMAAN_BARANG;

            isViewOnly = false;
            selectedIDTrans = 0;
            transStatus = 1;

            globalTotalValue = 0;
            totalLabelValue.Text = globalTotalValue.ToString("C0", culture);

            labelHint.Text = "F4 : Pilih Barang | F5 : Reset | F9 : Save | Del : Hapus Baris";
        }

        private void printTransaksi()
        {
            List<sqlFields> listSql = new List<sqlFields>();
            sqlFields sqlElement;

            sqlElement = new sqlFields();
            sqlElement.fieldName = "PENERIMAAN_ID";
            sqlElement.fieldValue = selectedIDTrans.ToString();
            listSql.Add(sqlElement);

            RPT_containerForm displayReportForm = new RPT_containerForm(null, globalReportConstants.PRINTOUT_TRANSAKSI_PENERIMAAN, "", listSql);
            displayReportForm.ShowDialog(this);
        }

    }
}
