﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using Hotkeys;

namespace AlphaSoft
{
    public partial class loginForm : Form
    {
        private globalUserUtil gUtil;
        private globalRestAPI gRest;

        private Hotkeys.GlobalHotkey ghk_UP;
        private Hotkeys.GlobalHotkey ghk_DOWN;
        private Hotkeys.GlobalHotkey ghk_ESC;
        private Hotkeys.GlobalHotkey ghk_F9;

        private bool navKeyRegistered = false;

        private int originModuleID = 0;

        public loginForm(int moduleID = 0)
        {
            crReportContainer dummyReport = new crReportContainer();
            dummyReport.ShowDialog();

            gRest = new globalRestAPI();
            gRest.loadURL();

            gUtil = new globalUserUtil();
            
            InitializeComponent();

            originModuleID = moduleID;
        }

        private void captureAll(Keys key)
        {
            switch (key)
            {
                case Keys.Up:
                    SendKeys.Send("+{TAB}");
                    break;

                case Keys.Down:
                    SendKeys.Send("{TAB}");
                    break;

                case Keys.F9:
                    loginButton.PerformClick();
                    break;

                case Keys.Escape:
                    Application.Exit();
                    break;
            }
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == Constants.WM_HOTKEY_MSG_ID)
            {
                Keys key = (Keys)(((int)m.LParam >> 16) & 0xFFFF);
                int modifier = (int)m.LParam & 0xFFFF;

                if (modifier == Constants.NOMOD)
                    captureAll(key);
            }

            base.WndProc(ref m);
        }

        private void registerGlobalHotkey()
        {
            ghk_UP = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.Up, this);
            ghk_UP.Register();

            ghk_DOWN = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.Down, this);
            ghk_DOWN.Register();

            ghk_ESC = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.Escape, this);
            ghk_ESC.Register();

            ghk_F9 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F9, this);
            ghk_F9.Register();

            navKeyRegistered = true;
        }

        private void unregisterGlobalHotkey()
        {
            ghk_UP.Unregister();
            ghk_DOWN.Unregister();
            ghk_ESC.Unregister();
            ghk_F9.Unregister();

            navKeyRegistered = false;
        }

        private void genericControl_Enter(object sender, EventArgs e)
        {
            if (navKeyRegistered)
                unregisterGlobalHotkey();
        }

        private void genericControl_Leave(object sender, EventArgs e)
        {
            registerGlobalHotkey();
        }

        private bool checkUserNamePassword(int moduleID = 0)
        {
            return gUtil.RS_doUserLogin(userNameTextBox.Text, passwordTextBox.Text);
        }

        private bool checkTextBox()
        {
            bool retVal = false;
            string userName;
            string password;

            userName = userNameTextBox.Text;
            password = passwordTextBox.Text;

            if (userName !="" & password != "")
            {
                retVal = true;
            }

            return retVal;
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (checkTextBox())
            {
                if (checkUserNamePassword())
                {
                    this.Hide();

                    gUtil.RS_setUserGroupID();
                    gUtil.RS_saveUserLastLogin();

                    adminForm displayForm = new adminForm(gUtil.getUserID(), gUtil.getUserGroupID());
                    displayForm.ShowDialog(this);

                    gUtil.ResetAllControls(this);

                    gUtil.RS_saveUserLastLogout();
                    this.Show();

                    userNameTextBox.Focus();
                }
                else
                {
                    errorLabel.Text = "Login gagal";
                }
            }
            else
            {
                errorLabel.Text = "Input salah";
            }
        }

        private void loginForm_Load(object sender, EventArgs e)
        {
            hotkeysStatusLabel.Text = "F9: Login | ESC : Close";

            gUtil.reArrangeTabOrder(this);

            if (originModuleID == globalConstants.MODULE_2ND_AUTH)
            {
                loginButton.Text = "Verify";
            }
        }
        
        private void passwordTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 13)
            {
                loginButton.PerformClick();
            }
        }

        private void loginForm_Activated(object sender, EventArgs e)
        {
            errorLabel.Text = "";
            registerGlobalHotkey();
        }

        private void loginForm_Deactivate(object sender, EventArgs e)
        {
            unregisterGlobalHotkey();
        }
    }
}
