﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using DBGridExtension;

namespace AlphaSoft
{
    public partial class dataEmployeeDetailForm : AlphaSoft.basicHotkeysForm
    {
        private int originModuleID = 0;
        private int selectedUserID = 0;
        private int selectedGroupID = 0;
        private string resultToken = "";
        private string oldPassword = "";

        private CultureInfo culture = new CultureInfo("id-ID");

        private globalUserUtil gUtil;
        private globalRestAPI gRest;

        private int selectedKompleksID = 0;

        public REST_empLoginData employeeLoginData = new REST_empLoginData();
        public REST_empAccessList employeeAccessData = new REST_empAccessList();

        public dataEmployeeDetailForm(int empID = 0)
        {
            InitializeComponent();
            selectedUserID = empID;

            if (selectedUserID > 0)
                originModuleID = globalConstants.EDIT_EMPLOYEE;
            else
                originModuleID = globalConstants.NEW_EMPLOYEE;

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();
        }

        private void loadEmployeeDataInformation()
        {
            DataTable dt = new DataTable();

            string sqlCommand = "SELECT ul.* " +
                                            "FROM employee_login_data ul " +
                                            "WHERE ul.employee_id = " + selectedUserID + " ";

            gRest.getEmployeeLoginData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref employeeLoginData);

            if (employeeLoginData.dataStatus.o_status == 1)
            {
                userNameTextBox.Text = employeeLoginData.dataList[0].employee_user_name;
                userFullNameTextBox.Text = employeeLoginData.dataList[0].employee_full_name;
                nonAktifCheckbox.Checked = (employeeLoginData.dataList[0].is_active == "Y" ? false : true);
                userPhoneTextBox.Text = employeeLoginData.dataList[0].employee_phone;
                qrAccessCodeTextBox.Text = employeeLoginData.dataList[0].employee_qr_code;
                oldPassword = employeeLoginData.dataList[0].employee_password;

                #region EMPLOYEE KOMPLEKS ACCESS
                sqlCommand = "SELECT ea.*, IFNULL(mk.kompleks_name, '') AS kompleks_name, IFNULL(mk.is_active, '') as kompleks_active " +
                                        "FROM employee_access_list ea " +
                                        "LEFT OUTER JOIN master_kompleks mk ON (ea.kompleks_id = mk.kompleks_id) " +
                                        "WHERE ea.employee_id = " + selectedUserID;

                gRest.getEmployeeAccessData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref employeeAccessData);
                if (employeeAccessData.dataStatus.o_status == 1)
                {
                    employeeAccessGrid.Rows.Clear();

                    for (int i = 0;i<employeeAccessData.dataList.Count;i++)
                    {
                        employeeAccessGrid.Rows.Add(
                            ((employeeAccessData.dataList[i].is_active == "N" || employeeAccessData.dataList[i].kompleks_active == "N") ? 0 : 1),
                            employeeAccessData.dataList[i].id,
                            employeeAccessData.dataList[i].kompleks_id,
                            employeeAccessData.dataList[i].kompleks_name
                            );

                        if (employeeAccessData.dataList[i].is_active == "N" || 
                            employeeAccessData.dataList[i].kompleks_active == "N")
                        {
                            employeeAccessGrid.Rows[employeeAccessGrid.Rows.Count - 1].Visible = false;
                        }
                    }
                }
                #endregion

                #region USER TOKEN
                sqlCommand = "SELECT access_token AS resultQuery " +
                                        "FROM employee_token WHERE employee_id = " + selectedUserID + " ";

                genericReply replyResult = new genericReply();
                if (gRest.getDataSingleValue(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref replyResult))
                {
                    if (replyResult.o_status == 1)
                    {
                        resultToken = replyResult.data.ToString();
                    }
                }
                #endregion
            }
        }

        public string getUserQRCode(int maxLength = 10)
        {
            string userQrCode = "";

            try
            {
                while (true)
                {
                    userQrCode = gUtil.getRandomString(maxLength);

                    string sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                            "FROM employee_login_data " +
                                            "WHERE employee_qr_code = '" + userQrCode + "' ";

                    genericReply replyResult = new genericReply();
                    if (gRest.checkDataExist(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref replyResult))
                    {
                        if (Convert.ToInt32(replyResult.data) <= 0)
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return userQrCode;
        }

        private void dataEmployeeDetailForm_Load(object sender, EventArgs e)
        {
            if (selectedUserID > 0)
            {
                loadEmployeeDataInformation();
            }
            else
            {
                qrAccessCodeTextBox.Text = getUserQRCode();
            }

            employeeAccessGrid.DoubleBuffered(true);
        }

        private void searchKompleksButton_Click(object sender, EventArgs e)
        {
            dataBrowse_KompleksForm displayForm = new dataBrowse_KompleksForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                selectedKompleksID = Convert.ToInt32(displayForm.ReturnValue1);
                kompleksTextBox.Text = displayForm.ReturnValue2;

                newKompleks.Enabled = true;
            }
        }

        private void showPassCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (showPassCheckBox.Checked)
            {
                passwordTextBox.PasswordChar = '\0';
                password2TextBox.PasswordChar = '\0';
            }
            else
            {
                passwordTextBox.PasswordChar = '*';
                password2TextBox.PasswordChar = '*';
            }
        }

        private void newKompleks_Click(object sender, EventArgs e)
        {
            errorLabel.Text = "";
            DataGridViewRow sRow;
            bool isFound = false;

            for (int i = 0;i<employeeAccessGrid.Rows.Count && !isFound;i++)
            {
                sRow = employeeAccessGrid.Rows[i];

                if (sRow.Cells["kompleksID"].Value.ToString() == selectedKompleksID.ToString())
                {
                    if (sRow.Cells["flag"].Value.ToString() == "1")
                    {
                        errorLabel.Text = "Kompleks " + kompleksTextBox.Text + " sudah ada di daftar akses";
                    }
                    else
                    {
                        sRow.Visible = true;
                        sRow.Cells["flag"].Value = 1;
                    }

                    isFound = true;
                }
            }

            if (!isFound)
            {
                employeeAccessGrid.Rows.Add(
                    1,
                    0,
                    selectedKompleksID,
                    kompleksTextBox.Text
                    );
            }
        }

        private void printQRButton_Click(object sender, EventArgs e)
        {
            if (qrAccessCodeTextBox.Text.Length > 0)
            {
                displayImageForm displayForm = new displayImageForm(qrAccessCodeTextBox.Text, 800, 800);
                displayForm.ShowDialog(this);
            }
        }

        private void employeeAccessGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (employeeAccessGrid.Rows.Count > 0)
                if (e.KeyCode == Keys.Delete)
                {
                    deleteCurrentRow();
                }
        }

        private void deleteCurrentRow()
        {
            if (employeeAccessGrid.Rows.Count > 0)
            {
                if (DialogResult.Yes == MessageBox.Show("Hapus baris ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    try
                    {
                        int rowIndex = employeeAccessGrid.SelectedCells[0].RowIndex;
                        DataGridViewRow selectedRow = employeeAccessGrid.Rows[rowIndex];

                        selectedRow.Cells["flag"].Value = 0;
                        selectedRow.Visible = false;
                    }
                    catch (Exception ex)
                    {
                        errorLabel.Text = ex.Message;
                    }
                }
            }
        }

        private bool dataValid()
        {
            errorLabel.Text = "";
            string sqlCommand = "";

            if (nonAktifCheckbox.Checked)
            {
                if (DialogResult.No == MessageBox.Show("Non aktifkan staff ?", "WARNING",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    return false;
            }

            if (userNameTextBox.Text.Trim().Equals(""))
            {
                errorLabel.Text = "User name tidak boleh kosong";
                return false;
            }

            if (!gUtil.matchRegEx(userNameTextBox.Text, globalUtilities.REGEX_ALPHANUMERIC_ONLY))
            {
                errorLabel.Text = "Username harus alphanumeric (tanpa spasi dan tanda baca)";
                return false;
            }

            sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                            "FROM employee_login_data WHERE employee_user_name = '" + userNameTextBox.Text + "' " +
                                            "AND employee_id <> " + selectedUserID + " " +
                                            "AND is_active = 'Y'";

            genericReply replyResult = new genericReply();
            if (gRest.checkDataExist(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                {
                    errorLabel.Text = "Username sudah ada";
                    return false;
                }
            }

            if (userFullNameTextBox.Text.Trim().Equals(""))
            {
                errorLabel.Text = "User full name kosong";
                return false;
            }

            if (passwordTextBox.Text.Trim().Equals("") && originModuleID == globalConstants.NEW_EMPLOYEE)
            {
                errorLabel.Text = "Password kosong";
                return false;
            }

            if (!gUtil.matchRegEx(passwordTextBox.Text, globalUtilities.REGEX_ALPHANUMERIC_ONLY) && originModuleID == globalConstants.NEW_EMPLOYEE)
            {
                errorLabel.Text = "Password harus alphanumeric";
                return false;
            }

            if (!passwordTextBox.Text.Equals(password2TextBox.Text) && originModuleID == globalConstants.NEW_EMPLOYEE)
            {
                errorLabel.Text = "Input password dan ulang password tidak sama";
                return false;
            }

            if (userPhoneTextBox.Text.Length <= 0)
            {
                errorLabel.Text = "Telepon harus diisi";
                return false;
            }

            if (qrAccessCodeTextBox.Text.Length <= 0)
            {
                errorLabel.Text = "Kode akses harus diisi";
                return false;
            }

            sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                        "FROM employee_login_data WHERE employee_qr_code = '" + qrAccessCodeTextBox.Text + "' " +
                                        "AND employee_id <> " + selectedUserID + " " +
                                        "AND is_active = 'Y'";

            replyResult = new genericReply();
            if (gRest.checkDataExist(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                {
                    errorLabel.Text = "Kode akses sudah ada";
                    return false;
                }
            }

            errorLabel.Text = "";
            return true;
        }

        private bool saveTrans(out string errMsg)
        {
            bool result = false;
            int opType = 0;
            errMsg = "";
            string inputName = String.Format(culture, "{0:yyyyMMddhhmmss}", DateTime.Now);
            string sqlCommand = "";

            employee_login_data loginData = new employee_login_data();

            List<employee_access_list> accessDataList = new List<employee_access_list>();
            employee_access_list accessData = new employee_access_list();

            List<user_token> employeeTokenData = new List<user_token>();
            user_token employeeToken = new user_token();

            try
            {
                loginData.employee_user_name = userNameTextBox.Text;

                if (passwordTextBox.Text.Length > 0)
                {
                    loginData.employee_password = gUtil.getMD5Value(passwordTextBox.Text);
                }
                else
                {
                    loginData.employee_password = oldPassword;
                }

                loginData.employee_full_name = userFullNameTextBox.Text;
                loginData.employee_phone = userPhoneTextBox.Text;
                loginData.employee_qr_code = qrAccessCodeTextBox.Text;
                loginData.is_active = (nonAktifCheckbox.Checked ? "N" : "Y");

                switch (originModuleID)
                {
                    case globalConstants.NEW_EMPLOYEE:
                        opType = 1;
                        loginData.employee_id = 0;

                        employeeToken.user_id = 0;
                        while (true)
                        {
                            inputName += gUtil.allTrim(userNameTextBox.Text);
                            resultToken = gUtil.getMD5Value(inputName);//gUtil.getRandomString(10);

                            sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                                    "FROM employee_login_data ul, employee_token ut WHERE ut.access_token = '" + resultToken + "' " +
                                                    "AND ut.employee_id = ul.employee_id " +
                                                    "AND ul.is_active = 'Y'";

                            genericReply replyResult = new genericReply();
                            if (gRest.checkDataExist(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref replyResult))
                            {
                                if (Convert.ToInt32(replyResult.data) <= 0)
                                {
                                    employeeToken.access_token = resultToken;
                                    break;
                                }
                            }
                        }

                        employeeTokenData.Add(employeeToken);
                        break;

                    case globalConstants.EDIT_EMPLOYEE:
                        opType = 2;
                        loginData.employee_id = selectedUserID;

                        employeeToken.user_id = selectedUserID;
                        employeeToken.access_token = resultToken;
                        employeeTokenData.Add(employeeToken);
                        break;
                }

                DataGridViewRow sRow;
                for (int i = 0;i<employeeAccessGrid.Rows.Count;i++)
                {
                    sRow = employeeAccessGrid.Rows[i];
                    accessData = new employee_access_list();

                    accessData.id = Convert.ToInt32(sRow.Cells["id"].Value);
                    accessData.kompleks_id = Convert.ToInt32(sRow.Cells["kompleksID"].Value);

                    if (sRow.Cells["flag"].Value.ToString() == "0")
                    {
                        if (sRow.Cells["id"].Value.ToString() == "0")
                            continue;   
                        else
                        {
                            accessData.is_active = "N";
                        }
                    }
                    else
                    {
                        accessData.is_active = (sRow.Cells["flag"].Value.ToString() == "0" ? "N" : "Y");
                    }

                    accessDataList.Add(accessData);
                }

                int newID = 0;
                if (!gRest.saveDataEmployee(gUtil.getUserID(), gUtil.getUserToken(), opType, loginData, accessDataList, employeeTokenData, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private bool saveData()
        {
            string errMsg = "";

            if (dataValid())
            {
                return saveTrans(out errMsg);
            }

            return false;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Save Data Staff ?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {
                if (saveData())
                {
                    MessageBox.Show("Success");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Fail");
                }
            }
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            qrAccessCodeTextBox.Text = getUserQRCode();
        }
    }
}
