﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Globalization;
using System.IO;
using DBGridExtension;

namespace AlphaSoft
{
    public partial class dataSistemAplikasi : basicHotkeysForm
    {
        private globalPrinterUtility gUtil;
        private globalUserUtil gUser;
        private globalRestAPI gRest;
        private string appPath = Application.StartupPath + "\\conf";

        private CultureInfo culture = new CultureInfo("id-ID");

        private List<string> printerList = new List<string>();

        public dataSistemAplikasi()
        {
            InitializeComponent();

            gUtil = new globalPrinterUtility();
            gUser = new globalUserUtil();
            gRest = new globalRestAPI();
        }

        private void fillInPrinterCombo()
        {
            gUtil.getListOfPrinter(ref printerList);

            kuartoPrinter.Items.Clear();

            for (int i =0;i<printerList.Count;i++)
            {
                kuartoPrinter.Items.Add(printerList[i]);
            }

            if (printerList.Count > 0)
                kuartoPrinter.SelectedIndex = 0;
        }

        private void pengaturanSistemAplikasi_Load(object sender, EventArgs e)
        {
            errorLabel.Text = "";

            fillInPrinterCombo();

            loadDataSys();
            loadDataSysTransaksi();
            loadDataSchedule();

            gUtil.reArrangeTabOrder(this);

            for (int i = 0; i<tabControl1.TabPages.Count; i++)
                gUtil.reArrangeTabPageOrder(tabControl1.TabPages[i], 1);

            namaTextbox.Focus();

            paymentGridView.DoubleBuffered(true);
            paymentGridView.CellValueChanged += CUSTOM_CellValueChanged;
        }

        private bool dataValidated()
        {
            errorLabel.Text = "";

            if (namaTextbox.Text.Trim().Equals(""))
            {
                errorLabel.Text = "Nama perusahaan belum diisi!";
                return false;
            }
            if (alamatTextbox.Text.Trim().Equals(""))
            {
                errorLabel.Text = "Alamat perusahaan belum diisi!";
                return false;
            }
            if (teleponTextbox.Text.Trim().Equals(""))
            {
                errorLabel.Text = "Telepon perusahaan belum diisi!";
                return false;
            }

            double tempVal = 0;
            DataGridViewRow sRow;
            string[] colName = { "biayaRP", "biayaPercent" };
            for(int i = 0;i<paymentGridView.Rows.Count;i++)
            {
                sRow = paymentGridView.Rows[i];

                for (int j = 0;j<colName.Length;j++)
                {
                    if (null == sRow.Cells[colName[j]].Value || 
                        !double.TryParse(sRow.Cells[colName[j]].Value.ToString(), out tempVal))
                    {
                        errorLabel.Text = "Input " + sRow.Cells[colName[j]].OwningColumn.HeaderText + " untuk " + sRow.Cells["paymentName"].Value.ToString() + " salah";
                        return false;
                    }
                }
            }

            errorLabel.Text = "";
            return true;
        }

        private bool saveDataTransaction()
        {
            bool result = false;
            string errMsg;

            genericReply replyResult = new genericReply();

            List<sys_config> listSysConfigData = new List<sys_config>();
            sys_config sysConfig;

            List<sys_config_trans> listSysConfigTrans = new List<sys_config_trans>();
            sys_config_trans sysConfigTrans;

            List<sys_config_schedule> listSysConfigSchedule = new List<sys_config_schedule>();
            sys_config_schedule sysConfigSchedule;

            CheckBox[] dayCheckBox = { checkBoxSunday, checkBoxMonday, checkBoxTuesday, checkBoxWednesday, checkBoxThursday, checkBoxFriday, checkBoxSaturday };
            DateTimePicker[] startTime = { startSun, startMon, startTue, startWed, startThur, startFri, startSat };
            DateTimePicker[] endTime = { endSun, endMon, endTue, endWed, endThur, endFri, endSat };

            try
            {
                #region SYS_CONFIG
                sysConfig = new sys_config();

                sysConfig.company_name = namaTextbox.Text;
                sysConfig.company_address = alamatTextbox.Text;
                sysConfig.company_phone = teleponTextbox.Text;
                sysConfig.company_email = emailTextbox.Text;
                sysConfig.default_printer = gUtil.addBackslahToAString(kuartoPrinter.Text);
                sysConfig.print_preview = (printPreviewCheckBox.Checked ? 1 : 0);
                sysConfig.message_template = msgTemplate.Text;

                listSysConfigData.Add(sysConfig);
                #endregion

                #region SYS_CONFIG_TRANS
                DataGridViewRow sRow;
                double biayaRP = 0;
                double biayaPercent = 0;
                int lineID = 0;
                int paymentType = 0;
                int i = 0;

                for (i = 0; i < paymentGridView.Rows.Count; i++)
                {
                    sysConfigTrans = new sys_config_trans();
                    sRow = paymentGridView.Rows[i];

                    lineID = Convert.ToInt32(sRow.Cells["lineID"].Value);
                    paymentType = Convert.ToInt32(sRow.Cells["paymentType"].Value);
                    biayaRP = Convert.ToDouble(sRow.Cells["biayaRP"].Value);
                    biayaPercent = Convert.ToDouble(sRow.Cells["biayaPercent"].Value);

                    sysConfigTrans.id = lineID;
                    sysConfigTrans.payment_type = paymentType;
                    sysConfigTrans.payment_type_name = sRow.Cells["paymentName"].Value.ToString();
                    sysConfigTrans.biaya_transaksi = biayaRP;
                    sysConfigTrans.biaya_transaksi_percent = biayaPercent;

                    listSysConfigTrans.Add(sysConfigTrans);
                }
                #endregion

                #region SYS CONFIG SCHEDULE
                for (i = 0; i <= 6; i++) 
                {
                    sysConfigSchedule = new sys_config_schedule();

                    sysConfigSchedule.id = i;
                    sysConfigSchedule.flag = (dayCheckBox[i].Checked ? "Y" : "N");
                    sysConfigSchedule.start_time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, startTime[i].Value.Hour, startTime[i].Value.Minute, 0);
                    sysConfigSchedule.end_time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, endTime[i].Value.Hour, endTime[i].Value.Minute, 0);

                    listSysConfigSchedule.Add(sysConfigSchedule);
                }
                #endregion

                if (!gRest.saveSysConfigData(gUser.getUserID(), gUser.getUserToken(), listSysConfigData, listSysConfigTrans, listSysConfigSchedule, out errMsg))
                {
                    throw new Exception(errMsg);
                }

                if (!Directory.Exists(appPath))
                    Directory.CreateDirectory(appPath);

                gUtil.writeFile(gUtil.appPath + "\\kuartoPath.cfg", kuartoPrinter.Text);

                result = true;
            }
            catch(Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private bool saveData()
        {
            bool result = false;

            if (dataValidated())
            {
                //  Allow main UI thread to properly display please wait form.
                Application.DoEvents();
                result = saveDataTransaction();

                return result;
            }

            return result;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (saveData())
            {
                errorLabel.Text = "";
                MessageBox.Show("Success");

                this.Close();
            }
            else
                MessageBox.Show("Fail");
        }

        private void loadDataSys()
        {
            string sqlComm = "SELECT * FROM sys_config ORDER BY id DESC LIMIT 1";
            REST_sysConfig sysConfigData = new REST_sysConfig();

            if (gRest.getSysConfigData(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref sysConfigData))
            {
                if (sysConfigData.dataStatus.o_status == 1)
                {
                    namaTextbox.Text = sysConfigData.dataList[0].company_name;
                    alamatTextbox.Text = sysConfigData.dataList[0].company_address;
                    teleponTextbox.Text = sysConfigData.dataList[0].company_phone;
                    emailTextbox.Text = sysConfigData.dataList[0].company_email;

                    kuartoPrinter.Text = sysConfigData.dataList[0].default_printer;

                    if (sysConfigData.dataList[0].print_preview == 1)
                        printPreviewCheckBox.Checked = true;

                    msgTemplate.Text = sysConfigData.dataList[0].message_template;
                }
            }
        }

        private void loadDataSysTransaksi()
        {
            string sqlCommand = "";
            DataTable dt = new DataTable();

            REST_sysConfigTrans sysConfigTrans = new REST_sysConfigTrans();

            sqlCommand = "SELECT mp.payment_type_name, IFNULL(s.id, 0) AS id, " +
                                    "IFNULL(s.biaya_transaksi, 0) AS biaya_transaksi, " +
                                    "IFNULL(s.biaya_transaksi_percent, 0) AS biaya_transaksi_percent " +
                                    "FROM master_payment_mode mp " +
                                    "LEFT OUTER JOIN sys_config_transaksi s ON (mp.payment_type = s.payment_type) " +
                                    "WHERE mp.is_active = 'Y'";

            if (gRest.getSysConfigTransData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref sysConfigTrans))
            {
                if (sysConfigTrans.dataStatus.o_status == 1)
                {
                    paymentGridView.Rows.Clear();
                    for (int i = 0; i < sysConfigTrans.dataList.Count; i++)
                    {
                        paymentGridView.Rows.Add(
                            sysConfigTrans.dataList[i].id,
                            sysConfigTrans.dataList[i].payment_type,
                            sysConfigTrans.dataList[i].payment_type_name,
                            sysConfigTrans.dataList[i].biaya_transaksi,
                            sysConfigTrans.dataList[i].biaya_transaksi_percent
                            );
                    }
                }
            }
        }

        private void loadDataSchedule()
        {
            string sqlCommand = "";
            DataTable dt = new DataTable();

            CheckBox[] dayCheckBox = { checkBoxSunday, checkBoxMonday, checkBoxTuesday, checkBoxWednesday, checkBoxThursday, checkBoxFriday, checkBoxSaturday };
            DateTimePicker[] startTime = { startSun, startMon, startTue, startWed, startThur, startFri, startSat };
            DateTimePicker[] endTime = { endSun, endMon, endTue, endWed, endThur, endFri, endSat };

            REST_sysConfigSchedule sysConfigSchedule = new REST_sysConfigSchedule();

            sqlCommand = "SELECT id, flag, ifnull(start_time, NOW()) as start_time, ifnull(end_time, NOW()) as end_time " +
                                    "FROM operational_schedule op ";

            if (gRest.getSysConfigSchedule(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref sysConfigSchedule))
            {
                if (sysConfigSchedule.dataStatus.o_status == 1)
                {
                    for (int i = 0; i < sysConfigSchedule.dataList.Count; i++)
                    {
                        dayCheckBox[sysConfigSchedule.dataList[i].id].Checked = (sysConfigSchedule.dataList[i].flag == "Y" ? true : false);
                        startTime[sysConfigSchedule.dataList[i].id].Value = sysConfigSchedule.dataList[i].start_time;
                        endTime[sysConfigSchedule.dataList[i].id].Value = sysConfigSchedule.dataList[i].end_time;
                    }
                }
            }
        }

        private void pengaturanSistemAplikasi_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void paymentGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                double d = double.Parse(e.Value.ToString());

                if ((paymentGridView.Columns[e.ColumnIndex].Name == "biayaRP")
                    && null != e.Value)
                {
                    e.Value = d.ToString("N0", culture);
                }
                else if ((paymentGridView.Columns[e.ColumnIndex].Name == "biayaPercent")
                    && null != e.Value)
                {
                    e.Value = d.ToString("N2", culture);
                }

            }
            catch (Exception ex)
            {

            }
        }

        private void paymentGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (paymentGridView.IsCurrentCellDirty)
            {
                paymentGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void CUSTOM_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (paymentGridView.Rows.Count <= 0)
                return;

            var cell = paymentGridView[e.ColumnIndex, e.RowIndex];
            DataGridViewRow sRow = paymentGridView.Rows[e.RowIndex];

            double tempValue = 0;
            string columnName = paymentGridView.Columns[e.ColumnIndex].Name;

            if (columnName == "biayaRP" || columnName == "biayaPercent")
            {
                if (null == sRow.Cells[columnName].Value ||
                    sRow.Cells[columnName].Value.ToString().Length <= 0 ||
                    !double.TryParse(sRow.Cells[columnName].Value.ToString(), NumberStyles.Number, culture, out tempValue))
                {
                    errorLabel.Text = "Input " + sRow.Cells[columnName].OwningColumn.HeaderText + " untuk " + sRow.Cells["paymentName"].Value.ToString() + " salah";
                }
                else
                {
                    errorLabel.Text = "";

                    paymentGridView.CellValueChanged -= CUSTOM_CellValueChanged;
                    cell.Value = tempValue;
                    paymentGridView.CellValueChanged += CUSTOM_CellValueChanged;
                }
            }
        }
    }
}
