﻿namespace AlphaSoft
{
    partial class dataTransaksiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dataTransaksiForm));
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelHint = new System.Windows.Forms.Label();
            this.transGridView = new System.Windows.Forms.DataGridView();
            this.flagDelete = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lineID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unitName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subtotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemOldQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemHpp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemJasa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalLabelValue = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.searchBlokButton = new System.Windows.Forms.Button();
            this.namaBlokTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.searchKompleksButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.kompleksTextBox = new System.Windows.Forms.TextBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.userTextBox = new System.Windows.Forms.TextBox();
            this.dateDTPicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.statusCombo = new System.Windows.Forms.ComboBox();
            this.labelStatus = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(801, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.statusCombo);
            this.panel2.Controls.Add(this.labelStatus);
            this.panel2.Controls.Add(this.labelHint);
            this.panel2.Controls.Add(this.transGridView);
            this.panel2.Controls.Add(this.totalLabelValue);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.searchBlokButton);
            this.panel2.Controls.Add(this.namaBlokTextBox);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.searchKompleksButton);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.kompleksTextBox);
            this.panel2.Controls.Add(this.searchButton);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.userTextBox);
            this.panel2.Controls.Add(this.dateDTPicker);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(23, 44);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(757, 523);
            this.panel2.TabIndex = 21;
            // 
            // labelHint
            // 
            this.labelHint.AutoSize = true;
            this.labelHint.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHint.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelHint.Location = new System.Drawing.Point(23, 197);
            this.labelHint.Name = "labelHint";
            this.labelHint.Size = new System.Drawing.Size(497, 18);
            this.labelHint.TabIndex = 173;
            this.labelHint.Text = "F4 : Pilih Barang | F5 : Reset | F9 : Save | Del : Hapus Baris";
            // 
            // transGridView
            // 
            this.transGridView.AllowUserToAddRows = false;
            this.transGridView.AllowUserToDeleteRows = false;
            this.transGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.transGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.transGridView.BackgroundColor = System.Drawing.Color.FloralWhite;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.transGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.transGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.transGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.flagDelete,
            this.lineID,
            this.itemID,
            this.itemName,
            this.itemQty,
            this.unitName,
            this.itemPrice,
            this.subtotal,
            this.itemOldQty,
            this.itemHpp,
            this.itemJasa});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.transGridView.DefaultCellStyle = dataGridViewCellStyle4;
            this.transGridView.Location = new System.Drawing.Point(26, 218);
            this.transGridView.MultiSelect = false;
            this.transGridView.Name = "transGridView";
            this.transGridView.RowHeadersVisible = false;
            this.transGridView.Size = new System.Drawing.Size(711, 284);
            this.transGridView.TabIndex = 77;
            this.transGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.transGridView_CellFormatting);
            this.transGridView.CurrentCellDirtyStateChanged += new System.EventHandler(this.transGridView_CurrentCellDirtyStateChanged);
            this.transGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.transGridView_KeyDown);
            // 
            // flagDelete
            // 
            this.flagDelete.HeaderText = "flagDelete";
            this.flagDelete.Name = "flagDelete";
            this.flagDelete.Visible = false;
            this.flagDelete.Width = 99;
            // 
            // lineID
            // 
            this.lineID.HeaderText = "lineID";
            this.lineID.Name = "lineID";
            this.lineID.Visible = false;
            this.lineID.Width = 63;
            // 
            // itemID
            // 
            this.itemID.HeaderText = "itemID";
            this.itemID.Name = "itemID";
            this.itemID.Visible = false;
            this.itemID.Width = 70;
            // 
            // itemName
            // 
            this.itemName.HeaderText = "Item";
            this.itemName.Name = "itemName";
            this.itemName.Width = 72;
            // 
            // itemQty
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.itemQty.DefaultCellStyle = dataGridViewCellStyle2;
            this.itemQty.HeaderText = "Qty";
            this.itemQty.Name = "itemQty";
            this.itemQty.Width = 62;
            // 
            // unitName
            // 
            this.unitName.HeaderText = "Satuan";
            this.unitName.Name = "unitName";
            this.unitName.Width = 90;
            // 
            // itemPrice
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.itemPrice.DefaultCellStyle = dataGridViewCellStyle3;
            this.itemPrice.HeaderText = "Harga";
            this.itemPrice.Name = "itemPrice";
            this.itemPrice.Width = 81;
            // 
            // subtotal
            // 
            this.subtotal.HeaderText = "Subtotal";
            this.subtotal.Name = "subtotal";
            this.subtotal.Width = 102;
            // 
            // itemOldQty
            // 
            this.itemOldQty.HeaderText = "itemOldQty";
            this.itemOldQty.Name = "itemOldQty";
            this.itemOldQty.Visible = false;
            this.itemOldQty.Width = 127;
            // 
            // itemHpp
            // 
            this.itemHpp.HeaderText = "itemHpp";
            this.itemHpp.Name = "itemHpp";
            this.itemHpp.Visible = false;
            this.itemHpp.Width = 102;
            // 
            // itemJasa
            // 
            this.itemJasa.HeaderText = "itemJasa";
            this.itemJasa.Name = "itemJasa";
            this.itemJasa.Visible = false;
            this.itemJasa.Width = 106;
            // 
            // totalLabelValue
            // 
            this.totalLabelValue.AutoSize = true;
            this.totalLabelValue.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalLabelValue.ForeColor = System.Drawing.Color.Black;
            this.totalLabelValue.Location = new System.Drawing.Point(151, 157);
            this.totalLabelValue.Name = "totalLabelValue";
            this.totalLabelValue.Size = new System.Drawing.Size(54, 18);
            this.totalLabelValue.TabIndex = 76;
            this.totalLabelValue.Text = "Rp. 0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(95, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 18);
            this.label5.TabIndex = 75;
            this.label5.Text = "Total";
            // 
            // searchBlokButton
            // 
            this.searchBlokButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchBlokButton.BackgroundImage")));
            this.searchBlokButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchBlokButton.Enabled = false;
            this.searchBlokButton.Location = new System.Drawing.Point(366, 116);
            this.searchBlokButton.Name = "searchBlokButton";
            this.searchBlokButton.Size = new System.Drawing.Size(29, 30);
            this.searchBlokButton.TabIndex = 74;
            this.searchBlokButton.UseVisualStyleBackColor = true;
            this.searchBlokButton.Click += new System.EventHandler(this.searchBlokButton_Click);
            // 
            // namaBlokTextBox
            // 
            this.namaBlokTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.namaBlokTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.namaBlokTextBox.Location = new System.Drawing.Point(154, 117);
            this.namaBlokTextBox.MaxLength = 5;
            this.namaBlokTextBox.Name = "namaBlokTextBox";
            this.namaBlokTextBox.ReadOnly = true;
            this.namaBlokTextBox.Size = new System.Drawing.Size(206, 27);
            this.namaBlokTextBox.TabIndex = 73;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(107, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 18);
            this.label4.TabIndex = 72;
            this.label4.Text = "Blok";
            // 
            // searchKompleksButton
            // 
            this.searchKompleksButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchKompleksButton.BackgroundImage")));
            this.searchKompleksButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchKompleksButton.Enabled = false;
            this.searchKompleksButton.Location = new System.Drawing.Point(461, 82);
            this.searchKompleksButton.Name = "searchKompleksButton";
            this.searchKompleksButton.Size = new System.Drawing.Size(29, 30);
            this.searchKompleksButton.TabIndex = 71;
            this.searchKompleksButton.UseVisualStyleBackColor = true;
            this.searchKompleksButton.Click += new System.EventHandler(this.searchKompleksButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(60, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 18);
            this.label3.TabIndex = 69;
            this.label3.Text = "Kompleks";
            // 
            // kompleksTextBox
            // 
            this.kompleksTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.kompleksTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kompleksTextBox.Location = new System.Drawing.Point(154, 84);
            this.kompleksTextBox.MaxLength = 30;
            this.kompleksTextBox.Name = "kompleksTextBox";
            this.kompleksTextBox.ReadOnly = true;
            this.kompleksTextBox.Size = new System.Drawing.Size(300, 27);
            this.kompleksTextBox.TabIndex = 70;
            // 
            // searchButton
            // 
            this.searchButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchButton.BackgroundImage")));
            this.searchButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchButton.Location = new System.Drawing.Point(460, 49);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(29, 30);
            this.searchButton.TabIndex = 68;
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(99, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 18);
            this.label2.TabIndex = 66;
            this.label2.Text = "User";
            // 
            // userTextBox
            // 
            this.userTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.userTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userTextBox.Location = new System.Drawing.Point(154, 51);
            this.userTextBox.MaxLength = 30;
            this.userTextBox.Name = "userTextBox";
            this.userTextBox.ReadOnly = true;
            this.userTextBox.Size = new System.Drawing.Size(300, 27);
            this.userTextBox.TabIndex = 67;
            // 
            // dateDTPicker
            // 
            this.dateDTPicker.CustomFormat = "dd MMM yyyy";
            this.dateDTPicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateDTPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateDTPicker.Location = new System.Drawing.Point(154, 19);
            this.dateDTPicker.Name = "dateDTPicker";
            this.dateDTPicker.Size = new System.Drawing.Size(136, 26);
            this.dateDTPicker.TabIndex = 65;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(23, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 18);
            this.label1.TabIndex = 64;
            this.label1.Text = "Tgl Transaksi";
            // 
            // statusCombo
            // 
            this.statusCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.statusCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.statusCombo.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusCombo.FormattingEnabled = true;
            this.statusCombo.Location = new System.Drawing.Point(496, 22);
            this.statusCombo.Name = "statusCombo";
            this.statusCombo.Size = new System.Drawing.Size(241, 26);
            this.statusCombo.TabIndex = 175;
            this.statusCombo.Visible = false;
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatus.ForeColor = System.Drawing.Color.Black;
            this.labelStatus.Location = new System.Drawing.Point(426, 25);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(64, 18);
            this.labelStatus.TabIndex = 174;
            this.labelStatus.Text = "Status";
            this.labelStatus.Visible = false;
            // 
            // dataTransaksiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(801, 606);
            this.Controls.Add(this.panel2);
            this.Name = "dataTransaksiForm";
            this.Load += new System.EventHandler(this.dataTransaksiForm_Load);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        protected System.Windows.Forms.DateTimePicker dateDTPicker;
        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.Label label2;
        protected System.Windows.Forms.TextBox userTextBox;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Button searchKompleksButton;
        protected System.Windows.Forms.Label label3;
        protected System.Windows.Forms.TextBox kompleksTextBox;
        private System.Windows.Forms.Button searchBlokButton;
        protected System.Windows.Forms.TextBox namaBlokTextBox;
        protected System.Windows.Forms.Label label4;
        protected System.Windows.Forms.Label totalLabelValue;
        protected System.Windows.Forms.Label label5;
        protected System.Windows.Forms.DataGridView transGridView;
        protected System.Windows.Forms.Label labelHint;
        private System.Windows.Forms.DataGridViewTextBoxColumn flagDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn lineID;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemID;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn unitName;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn subtotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemOldQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemHpp;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemJasa;
        private System.Windows.Forms.ComboBox statusCombo;
        protected System.Windows.Forms.Label labelStatus;
    }
}
