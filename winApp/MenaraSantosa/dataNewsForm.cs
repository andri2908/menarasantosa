﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace AlphaSoft
{
    public partial class dataNewsForm : AlphaSoft.basicHotkeysForm
    {
        private int originModuleID = 0;
        private int selectedNewsID = 0;
        private string selectedImgFile = "";
        private string selectedImgFileFullScreen = "";
        private string outputImgFile = "";
        private string outputImgFileFullScreen = "";

        private bool isFtpImage = false;
        private bool isFtpImageFullScreen = false;

        private globalUserUtil gUser;
        private globalRestAPI gRest;
        private globalImageLib gImg;

        private CultureInfo culture = new CultureInfo("id-ID");

        public int returnNewsIDValue = 0;

        public dataNewsForm(int newsID = 0, int moduleID = 0)
        {
            InitializeComponent();
            selectedNewsID = newsID;

            gUser = new globalUserUtil();
            gRest = new globalRestAPI();
            gImg = new globalImageLib();

            if (selectedNewsID > 0)
            {
                originModuleID = globalConstants.EDIT_ITEM;

                if (!gUser.RS_userHasAccessTo(MMConstants.TAMBAH_HAPUS_NEWS,
                MMConstants.HAK_UPDATE_DATA))
                {
                    SaveButton.Enabled = false;
                    ResetButton.Enabled = false;
                }
            }
            else
                originModuleID = globalConstants.NEW_ITEM;

        }

        private void dataNewsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            returnNewsIDValue = selectedNewsID;
            gImg.clearLocalImgDir(outputImgFile);
            gImg.clearLocalImgDir(outputImgFileFullScreen);
        }

        private void loadDataInformation()
        {
            DataTable dt = new DataTable();
            REST_news newsData = new REST_news();
            bool noExpiry = true;

            string sqlCommand = "SELECT news_id, news_type, subject, description, file_name, file_name_fullscreen, is_banner, is_active, " +
                                            "IFNULL(start_date, NOW()) as start_date, IFNULL(end_date, NOW()) as end_date, " +
                                            "IF((isnull(start_date) & isnull(end_date)), 'N', 'Y') as is_expired " +
                                            "FROM news " +
                                            "WHERE news_id =  " + selectedNewsID;

            if (gRest.getNewsData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref newsData))
            {
                if (newsData.dataStatus.o_status == 1)
                {
                    subjectTextBox.Text = newsData.dataList[0].subject;
                    descTextBox.Text = newsData.dataList[0].description;

                    startDTPicker.Value = newsData.dataList[0].start_date;
                    endDTPicker.Value = newsData.dataList[0].end_date;

                    notExpiredCheckBox.Checked = (newsData.dataList[0].is_expired == "Y" ? false : true);

                    checkBoxBanner.Checked = (newsData.dataList[0].is_banner == "Y" ? true : false);
                    nonAktifCheckBox.Checked = (newsData.dataList[0].is_active == "N" ? true : false);

                    if (newsData.dataList[0].news_type == "buletin")
                        buletinRadio.Checked = true;
                    else
                        lifestyleRadio.Checked = true;

                    // LOAD IMAGE
                    outputImgFile = newsData.dataList[0].file_name;

                    if (null != outputImgFile &&
                        outputImgFile.Length > 0)
                    {
                        try
                        {
                            Bitmap bm;
                            bm = gImg.ByteToImage(outputImgFile, globalImageLib.IMG_NEWS);
                            pictureBox1.Image = bm;

                            //pictureBox1.Load(gImg.localImgDirectory + outputImgFile);

                            isFtpImage = true;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("File gambar tidak ditemukan " + ex.Message);
                            pictureBox1.Load(gImg.localImgDirectory + "errImg.jpeg");
                        }
                    }
                    else
                        outputImgFile = "";

                    outputImgFileFullScreen = newsData.dataList[0].file_name_fullscreen;

                    if (null != outputImgFileFullScreen &&
                        outputImgFileFullScreen.Length > 0)
                    {
                        try
                        {
                            Bitmap bm;
                            bm = gImg.ByteToImage(outputImgFileFullScreen, globalImageLib.IMG_NEWS);
                            pictureBox2.Image = bm;

                            //pictureBox1.Load(gImg.localImgDirectory + outputImgFile);

                            isFtpImageFullScreen = true;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("File gambar fullscreen tidak ditemukan " + ex.Message);
                            pictureBox2.Load(gImg.localImgDirectory + "errImg.jpeg");
                        }
                    }
                    else
                        outputImgFileFullScreen = "";

                }
            }
        }

        private void isExpiredCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            startDTPicker.Enabled = !notExpiredCheckBox.Checked;
            endDTPicker.Enabled = !notExpiredCheckBox.Checked;
        }

        private void dataNewsForm_Load(object sender, EventArgs e)
        {
            if (selectedNewsID > 0)
            {
                loadDataInformation();
            }

            subjectTextBox.Focus();
            gUser.reArrangeTabOrder(this);
        }

        private void resizeAndGetNewName(string inputFileName, string fieldName, ref string resultFileName, 
            int maxHeight = 500, int maxWidth = 1000)
        {
            string sqlCommand = "";
            string inputName = String.Format(culture, "{0:yyyyMMddhhmmss}", DateTime.Now) + gUser.getRandomString(15);

            if (resultFileName.Length <= 0)
            {
                while (true)
                {
                    resultFileName = gUser.getMD5Value(inputFileName) + ".jpg";

                    sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                            "FROM news mi " +
                                            "WHERE mi." + fieldName + " = '" + resultFileName + "' " +
                                            "AND mi.is_active = 'Y'";

                    genericReply replyResult = new genericReply();
                    if (gRest.checkDataExist(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
                    {
                        if (Convert.ToInt32(replyResult.data) <= 0)
                        {
                            break;
                        }
                    }
                }
            }

            gImg.ResizeImage(inputFileName, resultFileName, maxHeight, maxWidth, (fieldName != "file_name_fullscreen"));
        }

        private bool saveDataTransaction()
        {
            bool result = false;
            string errMsg = "";
            int opType = 0;

            List<news> listNews = new List<news>();
            news newsData = new news();

            try
            {
                if (selectedImgFile.Length > 0)
                {
                    resizeAndGetNewName(selectedImgFile, "file_name", ref outputImgFile);
                    if (!gImg.uploadToFTP(outputImgFile, globalImageLib.IMG_NEWS))
                    {
                        throw new Exception("Upload Gambar Gagal");
                    }
                }

                if (selectedImgFileFullScreen.Length > 0)
                {
                    resizeAndGetNewName(selectedImgFileFullScreen, "file_name_fullscreen", ref outputImgFileFullScreen, 1000, 500);
                    if (!gImg.uploadToFTP(outputImgFileFullScreen, globalImageLib.IMG_NEWS))
                    {
                        throw new Exception("Upload Gambar FullScreen Gagal");
                    }
                }

                switch (originModuleID)
                {
                    case globalConstants.NEW_ITEM:
                        opType = 1;
                        newsData.news_id = 0;

                        break;

                    case globalConstants.EDIT_ITEM:
                        opType = 2;
                        newsData.news_id = selectedNewsID;
                        break;
                }

                newsData.subject = subjectTextBox.Text;
                newsData.description = descTextBox.Text;
                newsData.start_date = new DateTime(startDTPicker.Value.Year, startDTPicker.Value.Month, startDTPicker.Value.Day);
                newsData.end_date = new DateTime(endDTPicker.Value.Year, endDTPicker.Value.Month, endDTPicker.Value.Day);
                newsData.is_banner = (checkBoxBanner.Checked ? "Y" : "N");
                newsData.is_expired = (notExpiredCheckBox.Checked ? "N" : "Y");
                newsData.news_type = "buletin";// "lifestyle";//(buletinRadio.Checked ? "buletin" : "lifestyle");
                newsData.is_active = (nonAktifCheckBox.Checked ? "N" : "Y");
                newsData.file_name = outputImgFile;
                newsData.file_name_fullscreen = outputImgFileFullScreen;

                listNews.Add(newsData);

                int newID = 0;
                if (!gRest.saveNews(gUser.getUserID(), gUser.getUserToken(), listNews, opType, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                if (opType == 1)
                    selectedNewsID = newID;

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            string fileName = "";

            if (isFtpImage)
                fileName = outputImgFile;
            else
                fileName = selectedImgFile;

            if (fileName.Length > 0)
            {
                displayImageForm displayForm = new displayImageForm(fileName, globalImageLib.IMG_NEWS, isFtpImage);
                displayForm.ShowDialog(this);
            }
        }

        private bool dataValidated()
        {
            errorLabel.Text = "";
            genericReply replyResult = new genericReply();

            if (nonAktifCheckBox.Checked)
            {
                if (DialogResult.No == MessageBox.Show("Set news non-aktif ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    return false;
            }

            if (subjectTextBox.Text.Length <= 0)
            {
                errorLabel.Text = "Subject kosong";
                return false;
            }

            return true;
        }

        private bool saveData()
        {
            if (dataValidated())
                return saveDataTransaction();

            return false;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (saveData())
            {
                if (DialogResult.Yes == MessageBox.Show("Success, input lagi ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    ResetButton.PerformClick();
                }
                else
                {
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("FAIL");
            }
        }

        private void img1Button_Click(object sender, EventArgs e)
        {
            string fileName = "";

            imgFileDialog.Filter = "Image Files(*.JPG; *.JPEG)| *.JPG; *.JPEG";
            if (imgFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    fileName = imgFileDialog.FileName;
                    pictureBox1.Load(fileName);
                    selectedImgFile = imgFileDialog.FileName;

                    isFtpImage = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("File gambar tidak bisa dibuka : " + ex.Message);
                    pictureBox1.Load(gImg.localImgDirectory + "errImg.jpeg");
                }
            }
        }

        private void img2Button_Click(object sender, EventArgs e)
        {
            string fileName = "";

            imgFileDialog.Filter = "Image Files(*.JPG; *.JPEG)| *.JPG; *.JPEG";
            if (imgFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    fileName = imgFileDialog.FileName;
                    pictureBox2.Load(fileName);
                    selectedImgFileFullScreen = imgFileDialog.FileName;

                    isFtpImageFullScreen = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("File gambar tidak bisa dibuka : " + ex.Message);
                    pictureBox2.Load(gImg.localImgDirectory + "errImg.jpeg");
                }
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            string fileName = "";

            if (isFtpImageFullScreen)
                fileName = outputImgFileFullScreen;
            else
                fileName = selectedImgFileFullScreen;

            if (fileName.Length > 0)
            {
                displayImageForm displayForm = new displayImageForm(fileName, globalImageLib.IMG_NEWS, isFtpImageFullScreen);
                displayForm.ShowDialog(this);
            }
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            gUser.ResetAllControls(this);
            selectedNewsID = 0;
            selectedImgFile = "";
            selectedImgFileFullScreen = "";
            outputImgFile = "";
            outputImgFileFullScreen = "";

            isFtpImage = false;
            isFtpImageFullScreen = false;

            originModuleID = globalConstants.NEW_ITEM;

            pictureBox1.Image = null;
            pictureBox2.Image = null;
        }
    }
}
