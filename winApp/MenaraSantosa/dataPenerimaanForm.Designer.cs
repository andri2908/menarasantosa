﻿namespace AlphaSoft
{
    partial class dataPenerimaanForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelHint = new System.Windows.Forms.Label();
            this.transGridView = new System.Windows.Forms.DataGridView();
            this.flagDelete = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lineID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unitName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subtotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemOldQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalLabelValue = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dateDTPicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(830, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.labelHint);
            this.panel2.Controls.Add(this.transGridView);
            this.panel2.Controls.Add(this.totalLabelValue);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.dateDTPicker);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(22, 42);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(786, 523);
            this.panel2.TabIndex = 22;
            // 
            // labelHint
            // 
            this.labelHint.AutoSize = true;
            this.labelHint.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHint.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelHint.Location = new System.Drawing.Point(23, 69);
            this.labelHint.Name = "labelHint";
            this.labelHint.Size = new System.Drawing.Size(497, 18);
            this.labelHint.TabIndex = 173;
            this.labelHint.Text = "F4 : Pilih Barang | F5 : Reset | F9 : Save | Del : Hapus Baris";
            // 
            // transGridView
            // 
            this.transGridView.AllowUserToAddRows = false;
            this.transGridView.AllowUserToDeleteRows = false;
            this.transGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.transGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.transGridView.BackgroundColor = System.Drawing.Color.FloralWhite;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.transGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.transGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.transGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.flagDelete,
            this.lineID,
            this.itemID,
            this.itemName,
            this.itemQty,
            this.unitName,
            this.itemPrice,
            this.subtotal,
            this.itemOldQty});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.transGridView.DefaultCellStyle = dataGridViewCellStyle4;
            this.transGridView.Location = new System.Drawing.Point(26, 90);
            this.transGridView.MultiSelect = false;
            this.transGridView.Name = "transGridView";
            this.transGridView.RowHeadersVisible = false;
            this.transGridView.Size = new System.Drawing.Size(740, 412);
            this.transGridView.TabIndex = 77;
            this.transGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.transGridView_CellFormatting);
            this.transGridView.CurrentCellDirtyStateChanged += new System.EventHandler(this.transGridView_CurrentCellDirtyStateChanged);
            this.transGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.transGridView_KeyDown);
            // 
            // flagDelete
            // 
            this.flagDelete.HeaderText = "flagDelete";
            this.flagDelete.Name = "flagDelete";
            this.flagDelete.Visible = false;
            this.flagDelete.Width = 99;
            // 
            // lineID
            // 
            this.lineID.HeaderText = "lineID";
            this.lineID.Name = "lineID";
            this.lineID.Visible = false;
            this.lineID.Width = 63;
            // 
            // itemID
            // 
            this.itemID.HeaderText = "itemID";
            this.itemID.Name = "itemID";
            this.itemID.Visible = false;
            this.itemID.Width = 70;
            // 
            // itemName
            // 
            this.itemName.HeaderText = "Item";
            this.itemName.Name = "itemName";
            this.itemName.Width = 72;
            // 
            // itemQty
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.itemQty.DefaultCellStyle = dataGridViewCellStyle2;
            this.itemQty.HeaderText = "Qty";
            this.itemQty.Name = "itemQty";
            this.itemQty.Width = 62;
            // 
            // unitName
            // 
            this.unitName.HeaderText = "Satuan";
            this.unitName.Name = "unitName";
            this.unitName.Width = 90;
            // 
            // itemPrice
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.itemPrice.DefaultCellStyle = dataGridViewCellStyle3;
            this.itemPrice.HeaderText = "Harga";
            this.itemPrice.Name = "itemPrice";
            this.itemPrice.Width = 81;
            // 
            // subtotal
            // 
            this.subtotal.HeaderText = "Subtotal";
            this.subtotal.Name = "subtotal";
            this.subtotal.Width = 102;
            // 
            // itemOldQty
            // 
            this.itemOldQty.HeaderText = "itemOldQty";
            this.itemOldQty.Name = "itemOldQty";
            this.itemOldQty.Visible = false;
            this.itemOldQty.Width = 127;
            // 
            // totalLabelValue
            // 
            this.totalLabelValue.AutoSize = true;
            this.totalLabelValue.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalLabelValue.ForeColor = System.Drawing.Color.Black;
            this.totalLabelValue.Location = new System.Drawing.Point(433, 23);
            this.totalLabelValue.Name = "totalLabelValue";
            this.totalLabelValue.Size = new System.Drawing.Size(54, 18);
            this.totalLabelValue.TabIndex = 76;
            this.totalLabelValue.Text = "Rp. 0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(377, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 18);
            this.label5.TabIndex = 75;
            this.label5.Text = "Total";
            // 
            // dateDTPicker
            // 
            this.dateDTPicker.CustomFormat = "dd MMM yyyy";
            this.dateDTPicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateDTPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateDTPicker.Location = new System.Drawing.Point(154, 19);
            this.dateDTPicker.Name = "dateDTPicker";
            this.dateDTPicker.Size = new System.Drawing.Size(136, 26);
            this.dateDTPicker.TabIndex = 65;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(23, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 18);
            this.label1.TabIndex = 64;
            this.label1.Text = "Tgl Transaksi";
            // 
            // dataPenerimaanForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(830, 606);
            this.Controls.Add(this.panel2);
            this.Name = "dataPenerimaanForm";
            this.Load += new System.EventHandler(this.dataPenerimaanForm_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        protected System.Windows.Forms.Label labelHint;
        protected System.Windows.Forms.DataGridView transGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn flagDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn lineID;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemID;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn unitName;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn subtotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemOldQty;
        protected System.Windows.Forms.Label totalLabelValue;
        protected System.Windows.Forms.Label label5;
        protected System.Windows.Forms.DateTimePicker dateDTPicker;
        protected System.Windows.Forms.Label label1;
    }
}
