﻿namespace AlphaSoft
{
    partial class dataGroupUserDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.groupTypeCombo = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox1.Controls.Add(this.groupTypeCombo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Size = new System.Drawing.Size(544, 155);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.Controls.SetChildIndex(this.namaTextBox, 0);
            this.groupBox1.Controls.SetChildIndex(this.label3, 0);
            this.groupBox1.Controls.SetChildIndex(this.label2, 0);
            this.groupBox1.Controls.SetChildIndex(this.deskripsiTextBox, 0);
            this.groupBox1.Controls.SetChildIndex(this.nonAktifCheckbox, 0);
            this.groupBox1.Controls.SetChildIndex(this.label1, 0);
            this.groupBox1.Controls.SetChildIndex(this.groupTypeCombo, 0);
            // 
            // ResetButton
            // 
            this.ResetButton.Location = new System.Drawing.Point(294, 209);
            this.ResetButton.Size = new System.Drawing.Size(140, 37);
            this.ResetButton.TabIndex = 12;
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(124, 209);
            this.SaveButton.Size = new System.Drawing.Size(138, 37);
            this.SaveButton.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.TabIndex = 5;
            // 
            // nonAktifCheckbox
            // 
            this.nonAktifCheckbox.Location = new System.Drawing.Point(123, 119);
            this.nonAktifCheckbox.TabIndex = 10;
            // 
            // namaTextBox
            // 
            this.namaTextBox.TabIndex = 8;
            // 
            // deskripsiTextBox
            // 
            this.deskripsiTextBox.TabIndex = 9;
            // 
            // panel1
            // 
            this.panel1.TabIndex = 1;
            // 
            // errorLabel
            // 
            this.errorLabel.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(66, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 18);
            this.label1.TabIndex = 13;
            this.label1.Text = "Tipe";
            // 
            // groupTypeCombo
            // 
            this.groupTypeCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.groupTypeCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.groupTypeCombo.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupTypeCombo.FormattingEnabled = true;
            this.groupTypeCombo.Location = new System.Drawing.Point(123, 87);
            this.groupTypeCombo.Name = "groupTypeCombo";
            this.groupTypeCombo.Size = new System.Drawing.Size(298, 26);
            this.groupTypeCombo.TabIndex = 49;
            // 
            // dataGroupUserDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(559, 286);
            this.Name = "dataGroupUserDetailForm";
            this.Text = "DETAIL DATA GROUP USER";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.dataGroupUserDetailForm_FormClosed);
            this.Load += new System.EventHandler(this.dataGroupUserDetailForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox groupTypeCombo;
    }
}
