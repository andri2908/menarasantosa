﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Diagnostics;
using System.IO;

using Hotkeys;
using System.Globalization;
using MySql.Data.MySqlClient;
using MySql.Data;
using DBGridExtension;
using System.Media;

namespace AlphaSoft
{
    public partial class adminForm : Form
    {
        private const string BG_IMAGE = "bg.jpg";
        private DateTime localDate = DateTime.Now;
        private CultureInfo culture = new CultureInfo("id-ID");
        string appPath = Application.StartupPath;

        private Data_Access DS = new Data_Access();

        private int selectedUserID = 0;
        private int selectedUserGroupID = 0;

        private globalMenuUtil gMenuUtil = new globalMenuUtil();
        private globalUserUtil gUser;
        private globalTimerUtil gTimer;
        private globalRestAPI gRest;

        List<masterModule> moduleAccess = new List<masterModule>();
        masterModule itemModule;

        private REST_groupUserAccess MMAccess = new REST_groupUserAccess();

        private Hotkeys.GlobalHotkey ghk_F2;
        private Hotkeys.GlobalHotkey ghk_F3;
        private Hotkeys.GlobalHotkey ghk_F4;
        private Hotkeys.GlobalHotkey ghk_ESC;

        private int[] shortcutMenuID =
        {
        };

        private int[,] separatorID = 
        {
        };

        private ToolStripSeparator[] tsSeparator = new ToolStripSeparator[4];
        private ToolStripButton[] tsButton = new ToolStripButton[5];

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == Constants.WM_HOTKEY_MSG_ID)
            {
                Keys key = (Keys)(((int)m.LParam >> 16) & 0xFFFF);
                int modifier = (int)m.LParam & 0xFFFF;

                if (modifier == Constants.NOMOD)
                    captureAll(key);
            }

            base.WndProc(ref m);
        }

        protected virtual void captureAll(Keys key)
        {
            switch (key)
            {
                case Keys.F2:
                    break;

                case Keys.F3:
                    break;

                case Keys.F4:
                    break;

                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        protected virtual void registerGlobalHotkey()
        {
            ghk_F2 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F2, this);
            ghk_F2.Register();

            ghk_F3 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F3, this);
            ghk_F3.Register();

            ghk_F4 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F4, this);
            ghk_F4.Register();

            ghk_ESC = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.Escape, this);
            ghk_ESC.Register();
        }

        protected virtual void unregisterGlobalHotkey()
        {
            ghk_F2.Unregister();
            ghk_F3.Unregister();
            ghk_F4.Unregister();
            ghk_ESC.Unregister();
        }

        public adminForm(int userID, int groupID)
        {
            InitializeComponent();

            selectedUserID = userID;
            selectedUserGroupID = groupID;

            gUser = new globalUserUtil();
            gTimer = new globalTimerUtil();
            gRest = new globalRestAPI();

            generateModuleAccessContent();
            applyModuleAccess();
           
            //pullMessages();
        }

        private void updateStatusLabel()
        {
            globalRestAPI gRest = new globalRestAPI();
            welcomeLabel.Text = "WELCOME " + gUser.getUserName() + " [" + gRest.getServerURL() + "]";
        }

        private void updateLabel()
        {
            localDate = DateTime.Now;
            timeStampStatusLabel.Text = String.Format(culture, "{0:dddd, dd-MM-yyyy - HH:mm}", localDate);
        }

        private void genericMenuDropDownOpen(object sender, EventArgs e)
        {
            gMenuUtil.menuDropDownOpened((ToolStripMenuItem)sender);
        }

        private void genericMenuDropDownClosed(object sender, EventArgs e)
        {
            gMenuUtil.menuDropDownClosed((ToolStripMenuItem)sender);
        }

        private void generateModuleAccessContent()
        {
            #region MANAJEMEN SISTEM
            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.MANAJEMEN_SISTEM;
            itemModule.menuItem = MAINMENU_manajemenSistem;
            moduleAccess.Add(itemModule);

            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.TAMBAH_HAPUS_GROUP_USER;
            itemModule.menuItem = MENU_tambahGroupUser;
            moduleAccess.Add(itemModule);

            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.TAMBAH_HAPUS_USER;
            itemModule.menuItem = MENU_tambahUser;
            moduleAccess.Add(itemModule);

            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.PENGATURAN_AKSES_MODUL;
            itemModule.menuItem = MENU_pengaturanGroupAkses;
            moduleAccess.Add(itemModule);

            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.BACKUP_RESTORE_DATABASE;
            itemModule.menuItem = MENU_backUpRestoreDatabase;
            moduleAccess.Add(itemModule);
            
            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.PENGATURAN_SISTEM_APLIKASI;
            itemModule.menuItem = MENU_pengaturanSistemAplikasi;
            moduleAccess.Add(itemModule);
            #endregion

            #region DATA MASTER
            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.TAMBAH_HAPUS_KOMPLEKS;
            itemModule.menuItem = MENU_dataKompleks;
            moduleAccess.Add(itemModule);

            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.TAMBAH_HAPUS_KAVLING;
            itemModule.menuItem = MENU_dataKavling;
            moduleAccess.Add(itemModule);

            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.TAMBAH_HAPUS_ITEM;
            itemModule.menuItem = MENU_dataItem;
            moduleAccess.Add(itemModule);

            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.TAMBAH_HAPUS_ITEM_DRUGS;
            itemModule.menuItem = MENU_dataItemDrugs;
            moduleAccess.Add(itemModule);

            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.TAMBAH_HAPUS_UNIT;
            itemModule.menuItem = MENU_dataUnit;
            moduleAccess.Add(itemModule);

            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.TAMBAH_HAPUS_PEMILIK_KAVLING;
            itemModule.menuItem = MENU_pemilikKavling;
            moduleAccess.Add(itemModule);

            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.TAMBAH_HAPUS_NEWS;
            itemModule.menuItem = MENU_dataNews;
            moduleAccess.Add(itemModule);

            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.TAMBAH_HAPUS_EMPLOYEE;
            itemModule.menuItem = MENU_dataAksesStaff;
            moduleAccess.Add(itemModule);
            #endregion

            #region DATA TRANSAKSI
            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.RINGKASAN_IPL;
            itemModule.menuItem = MENU_summaryIPL;
            moduleAccess.Add(itemModule);

            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.TAMBAH_HAPUS_RETAIL;
            itemModule.menuItem = MENU_transaksiRetail;
            moduleAccess.Add(itemModule);

            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.TAMBAH_HAPUS_DRUGS;
            itemModule.menuItem = MENU_transaksiDrugs;
            moduleAccess.Add(itemModule);

            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.PENYESUAIAN_STOK;
            itemModule.menuItem = MENU_penyesuaianStokRetail;
            moduleAccess.Add(itemModule);

            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.PENYESUAIAN_DRUGS;
            itemModule.menuItem = MENU_penyesuaianStokDrugs;
            moduleAccess.Add(itemModule);

            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.PENERIMAAN_BARANG;
            itemModule.menuItem = MENU_penerimaanBarangRetail;
            moduleAccess.Add(itemModule);

            itemModule = new masterModule();
            itemModule.moduleID = MMConstants.PENERIMAAN_DRUGS;
            itemModule.menuItem = MENU_penerimaanBarangDrugs;
            moduleAccess.Add(itemModule);
            #endregion
        }

        private void applyModuleAccess()
        {
            master_module_access accessRight;
            List<master_module_access> listModule;

            gUser.RS_getGroupUserAccess(ref MMAccess);

            for (int i = 0; i < moduleAccess.Count; i++)
            {
                listModule = MMAccess.dataList;

                accessRight = listModule.Find(x => x.module_ID == moduleAccess[i].moduleID);

                if (null == accessRight || accessRight.user_access_option <= 0)
                {
                    if (null != moduleAccess[i].menuItem)
                        moduleAccess[i].menuItem.Visible = false;
                }
            }

            SHORTCUT_ipl.Visible = MENU_summaryIPL.Visible;
            SHORTCUT_property.Visible = MENU_pemilikKavling.Visible;
            SHORTCUT_cc.Visible = MENU_customerCare.Visible;
            SHORTCUT_miniMarket.Visible = MENU_transaksiRetail.Visible;
            SHORTCUT_transDrugs.Visible = MENU_transaksiDrugs.Visible;
        }
        
        private void fillInMessageGrid()
        {
            string sqlCommand = "";
            MySqlDataReader rdr;
            DataTable dt = new DataTable();

            REST_masterMessage dataTrans = new REST_masterMessage();

            sqlCommand = "SELECT msg_id, module_id, msg_ref, msg_content, msg_datetime FROM master_message " +
                                    "WHERE msg_status = 'unread' " +
                                    "ORDER BY msg_datetime ASC, msg_content ASC";

            if (gRest.getMessageData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref dataTrans))
            {
                if (dataTrans.dataStatus.o_status == 1)
                {
                    messageGridView.Rows.Clear();

                    for (int i = 0; i < dataTrans.dataList.Count; i++)
                    {
                        messageGridView.Rows.Add(
                            dataTrans.dataList[i].msg_id,
                            dataTrans.dataList[i].module_id,
                            dataTrans.dataList[i].msg_ref,
                            gUser.getCustomStringFormatDate(dataTrans.dataList[i].msg_datetime, true),
                            dataTrans.dataList[i].msg_content
                            );
                    }
                }
            }
        }

        private void pullMessages()
        {
            gTimer.checkUnreadTicket();
            gTimer.checkUnreadRepairTicket();
            gTimer.getUndeliverTrans();

            fillInMessageGrid();
        }

        private void adminForm_Load(object sender, EventArgs e)
        {
            updateStatusLabel();
            updateLabel();

            pullMessages();

            if (messageGridView.Rows.Count > 0)
            {
                SoundPlayer simpleSound = new SoundPlayer(@appPath+"\\res\\notify.wav");
                simpleSound.Play();
            }

            MAINMENU_Strip.Renderer = new MyRenderer();

            messageGridView.DoubleBuffered(true);
            gUser.reArrangeTabOrder(this);

            timerStatusLabel.Start();
            timerMessage.Start();
        }

        private void timerStatusLabel_Tick(object sender, EventArgs e)
        {
            updateLabel();
        }

        private void informasiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutUsForm displayedform = new AboutUsForm();
            displayedform.ShowDialog();
        }

        private void MENU_changePassword_Click(object sender, EventArgs e)
        {
            changePasswordForm displayForm = new changePasswordForm();
            displayForm.ShowDialog(this);
        }

        private void MENU_logOut_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MENU_tambahUser_Click(object sender, EventArgs e)
        {
            dataBrowse_UserForm displayForm = new dataBrowse_UserForm();
            displayForm.ShowDialog(this);
        }

        private void adminForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            DS.sqlClose();
        }

        private void MENU_tambahGroupUser_Click(object sender, EventArgs e)
        {
            dataBrowse_GroupUserForm displayForm = new dataBrowse_GroupUserForm();
            displayForm.ShowDialog(this);
        }

        private void MENU_backUpRestoreDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            backupAndRestoreForm displayForm = new backupAndRestoreForm();
            displayForm.ShowDialog(this);
        }

        private void MENU_pengaturanSistemAplikasiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataSistemAplikasi displayForm = new dataSistemAplikasi();
            displayForm.ShowDialog(this);
        }

        private void MENU_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void MENU_pengaturanGroupAkses_Click(object sender, EventArgs e)
        {
            dataBrowse_GroupUserForm displayForm = new dataBrowse_GroupUserForm(globalConstants.MODULE_HAK_AKSES);
            displayForm.ShowDialog(this);
        }

        private void adminForm_Activated(object sender, EventArgs e)
        {
            registerGlobalHotkey();

        }

        private void adminForm_Deactivate(object sender, EventArgs e)
        {
            unregisterGlobalHotkey();
        }

        private void displaySpecificForm(DataGridViewRow sRow)
        {
            int moduleID = Convert.ToInt32(sRow.Cells["refModule"].Value);
            string refID = sRow.Cells["refID"].Value.ToString();
            int msgID = Convert.ToInt32(sRow.Cells["msgID"].Value);
            string errMsg = "";
            //if (!gUser.userHasAccessTo(moduleID, MMConstants.HAK_AKSES, gUser.getUserGroupID()))
            //    return;

            switch (moduleID)
            {
                case globalConstants.MODULE_TICKET_REPAIR:
                    MENU_customerCare.PerformClick();
                    break;

                case globalConstants.MODULE_PENGERJAAN_REPAIR:
                    toolStripMenuItem1.PerformClick();
                    break;

                case globalConstants.MODULE_TRANSAKSI_RETAIL:
                    dataTransaksiForm editForm = new dataTransaksiForm(moduleID, Convert.ToInt32(refID));
                    editForm.ShowDialog(this);
                    break;
            }

            gRest.updateMsgStatus(gUser.getUserID(), gUser.getUserToken(), msgID, out errMsg);
            fillInMessageGrid();
        }

        private void messageGridView_DoubleClick(object sender, EventArgs e)
        {
            if (messageGridView.Rows.Count <= 0)
                return;

            int rowIDX = messageGridView.SelectedCells[0].RowIndex;
            DataGridViewRow sRow = messageGridView.Rows[rowIDX];

            displaySpecificForm(sRow);
        }

        private void timerMessage_Tick(object sender, EventArgs e)
        {
            gTimer.checkForExpiredTrans();

            //gTimer.checkUserAccessStatus();
            pullMessages();
        }

        private void MENU_dataKompleks_Click(object sender, EventArgs e)
        {
            dataBrowse_KompleksForm displayForm = new dataBrowse_KompleksForm();
            displayForm.ShowDialog(this);
        }

        private void MENU_dataKavling_Click(object sender, EventArgs e)
        {
            dataBrowse_KavlingForm displayForm = new dataBrowse_KavlingForm();
            displayForm.ShowDialog(this);
        }

        private void MENU_pemilikKavling_Click(object sender, EventArgs e)
        {
            dataUserKavlingForm displayForm = new dataUserKavlingForm();
            displayForm.ShowDialog(this);
        }

        private void MENU_dataUnit_Click(object sender, EventArgs e)
        {
            dataBrowse_UnitForm displayForm = new dataBrowse_UnitForm();
            displayForm.ShowDialog(this);
        }

        private void MENU_dataItem_Click(object sender, EventArgs e)
        {
            dataBrowse_ItemForm displayForm = new dataBrowse_ItemForm(globalConstants.MODULE_ITEM_RETAIL);
            displayForm.ShowDialog(this);
        }

        private void MENU_summaryIPL_Click(object sender, EventArgs e)
        {
            dataSummaryIPLForm displayForm = new dataSummaryIPLForm();
            displayForm.ShowDialog(this);
        }

        private void MENU_transaksiRetail_Click(object sender, EventArgs e)
        {
            dataBrowse_TransaksiForm displayForm = new dataBrowse_TransaksiForm(globalConstants.MODULE_TRANSAKSI_RETAIL);
            displayForm.ShowDialog(this);
        }

        private void MENU_customerCare_Click(object sender, EventArgs e)
        {
            dataBrowse_customerCareForm displayForm = new dataBrowse_customerCareForm();
            displayForm.ShowDialog(this);
        }

        private void MENU_transaksiDrugs_Click(object sender, EventArgs e)
        {
            dataBrowse_TransaksiForm displayForm = new dataBrowse_TransaksiForm(globalConstants.MODULE_TRANSAKSI_DRUGS);
            displayForm.ShowDialog(this);
        }

        private void MENU_penyesuaianStokRetail_Click(object sender, EventArgs e)
        {
            dataBrowse_ItemForm displayForm = new dataBrowse_ItemForm(globalConstants.MODULE_PENYESUAIAN_STOK);
            displayForm.ShowDialog(this);
        }

        private void MENU_penyesuaianStokDrugs_Click(object sender, EventArgs e)
        {
            dataBrowse_ItemForm displayForm = new dataBrowse_ItemForm(globalConstants.MODULE_PENYESUAIAN_DRUGS);
            displayForm.ShowDialog(this);
        }

        private void MENU_penerimaanBarangRetail_Click(object sender, EventArgs e)
        {
            dataBrowse_PenerimaanForm displayForm = new dataBrowse_PenerimaanForm(globalConstants.MODULE_PENERIMAAN_BARANG);
            displayForm.ShowDialog(this);
        }

        private void MENU_penerimaanBarangDrugs_Click(object sender, EventArgs e)
        {
            dataBrowse_PenerimaanForm displayForm = new dataBrowse_PenerimaanForm(globalConstants.MODULE_PENERIMAAN_DRUGS);
            displayForm.ShowDialog(this);
        }

        private void MENU_dataItemDrugs_Click(object sender, EventArgs e)
        {
            dataBrowse_ItemForm displayForm = new dataBrowse_ItemForm(globalConstants.MODULE_ITEM_DRUGS);
            displayForm.ShowDialog(this);
        }

        private void expiredTransToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gTimer.checkForExpiredTrans();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            MENU_customerCare.PerformClick();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            MENU_transaksiDrugs.PerformClick();
        }

        private void SHORTCUT_ipl_Click(object sender, EventArgs e)
        {
            MENU_summaryIPL.PerformClick();
        }

        private void SHORTCUT_property_Click(object sender, EventArgs e)
        {
            MENU_pemilikKavling.PerformClick();
        }

        private void SHORTCUT_miniMarket_Click(object sender, EventArgs e)
        {
            MENU_transaksiRetail.PerformClick();
        }

        private void MENU_dataNews_Click(object sender, EventArgs e)
        {
            dataBrowse_NewsForm displayForm = new dataBrowse_NewsForm();
            displayForm.ShowDialog(this);
        }

        private void MENU_serviceDanReparasi_Click(object sender, EventArgs e)
        {

        }

        private void MENU_pengerjaanRepair_Click(object sender, EventArgs e)
        {
            
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            dataBrowse_customerCareForm displayForm = new dataBrowse_customerCareForm(globalConstants.MODULE_TICKET_REPAIR);
            displayForm.ShowDialog(this);
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            dataBrowse_customerCareForm displayForm = new dataBrowse_customerCareForm(globalConstants.MODULE_PENGERJAAN_REPAIR);
            displayForm.ShowDialog(this);
        }

        private void MENU_cetakTransaksi_Click(object sender, EventArgs e)
        {
            dataBrowse_TransaksiForm displayForm = new dataBrowse_TransaksiForm(globalConstants.MODULE_CETAK_TRANSAKSI_RETAIL);
            displayForm.ShowDialog(this);
        }

        private void MENU_cetakTransaksiDrugs_Click(object sender, EventArgs e)
        {
            dataBrowse_TransaksiForm displayForm = new dataBrowse_TransaksiForm(globalConstants.MODULE_CETAK_TRANSAKSI_DRUGS);
            displayForm.ShowDialog(this);
        }

        private void MENU_generateIPLKompleks_Click(object sender, EventArgs e)
        {
            dataGenerateIPLKompleksForm displayForm = new dataGenerateIPLKompleksForm();
            displayForm.ShowDialog(this);
        }

        private void MENU_dataRFID_Click(object sender, EventArgs e)
        {
            dataRFIDForm displayForm = new dataRFIDForm();
            displayForm.ShowDialog(this);
        }

        private void MENU_dataAksesSatpam_Click(object sender, EventArgs e)
        {
            dataAksesSatpam displayForm = new dataAksesSatpam();
            displayForm.ShowDialog(this);
        }

        private void MENU_dataTerminal_Click(object sender, EventArgs e)
        {
            dataTerminalForm displayForm = new dataTerminalForm();
            displayForm.ShowDialog(this);
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            gTimer.checkUserAccessStatus();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            dataBrowse_TransaksiForm displayForm = new dataBrowse_TransaksiForm(globalConstants.MODULE_PEMBAYARAN_IPL);
            displayForm.ShowDialog(this);
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            dataBrowse_TransaksiForm displayForm = new dataBrowse_TransaksiForm(globalConstants.MODULE_PEMBAYARAN_TIKET);
            displayForm.ShowDialog(this);
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            dataBrowse_TransaksiForm displayForm = new dataBrowse_TransaksiForm(globalConstants.MODULE_CANCEL_PEMBAYARAN);
            displayForm.ShowDialog(this);
        }

        private void MENU_dataAksesStaff_Click(object sender, EventArgs e)
        {
            dataBrowse_employee displayForm = new dataBrowse_employee();
            displayForm.ShowDialog(this);
        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            _reportFilterIPL displayForm = new _reportFilterIPL();
            displayForm.ShowDialog(this);
        }

        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            _reportFilter1Date displayForm = new _reportFilter1Date(globalReportConstants.REPORT_IPL_TAHUNAN);
            displayForm.ShowDialog(this);
        }

        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {
            _reportFilter1FieldRangeDate displayForm = new _reportFilter1FieldRangeDate(globalReportConstants.PRINTOUT_TAGIHAN_IPL_MANUAL);
            displayForm.ShowDialog(this);
        }

        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
            gTimer.checkUnreadTicket();
            gTimer.checkUnreadRepairTicket();
            gTimer.getUndeliverTrans();

            fillInMessageGrid();
        }

        private void toolStripMenuItem11_Click(object sender, EventArgs e)
        {
            _reportFilter_accessKompleks displayForm = new _reportFilter_accessKompleks();
            displayForm.ShowDialog(this);
        }

        private void toolStripMenuItem12_Click(object sender, EventArgs e)
        {
            _reportFilter1FieldBrowse displayForm = new _reportFilter1FieldBrowse(globalReportConstants.REPORT_KAVLING);
            displayForm.ShowDialog(this);
        }
    }
}
