﻿namespace AlphaSoft
{
    partial class dataBrowse_PenerimaanForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Size = new System.Drawing.Size(490, 103);
            // 
            // displayButton
            // 
            this.displayButton.Location = new System.Drawing.Point(306, 126);
            // 
            // label1
            // 
            this.label1.Visible = false;
            // 
            // nonActiveCheckBox
            // 
            this.nonActiveCheckBox.Location = new System.Drawing.Point(105, 69);
            // 
            // newButton
            // 
            this.newButton.Location = new System.Drawing.Point(132, 126);
            // 
            // namaTextBox
            // 
            this.namaTextBox.Visible = false;
            // 
            // dataBrowse_PenerimaanForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(926, 640);
            this.Name = "dataBrowse_PenerimaanForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.dataBrowse_PenerimaanForm_FormClosed);
            this.Load += new System.EventHandler(this.dataBrowse_PenerimaanForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}
