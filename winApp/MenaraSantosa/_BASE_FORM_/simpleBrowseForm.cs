﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data;
using MySql.Data.MySqlClient;
using Hotkeys;

namespace AlphaSoft
{
    public partial class simpleBrowseForm : Form
    {
        private Hotkeys.GlobalHotkey ghk_UP;
        private Hotkeys.GlobalHotkey ghk_DOWN;
        private Hotkeys.GlobalHotkey ghk_ESC;
        private Hotkeys.GlobalHotkey ghk_CTRL_UP;
        private Hotkeys.GlobalHotkey ghk_CTRL_DOWN;

        private bool navKeyRegistered = false;

        public string ReturnValue1 { get; set; }
        public string ReturnValue2 { get; set; }

        public simpleBrowseForm()
        {
            InitializeComponent();
        }

        protected virtual void captureAll(Keys key)
        {
            switch (key)
            {
                case Keys.Up:
                    SendKeys.Send("+{TAB}");
                    break;

                case Keys.Down:
                    SendKeys.Send("{TAB}");
                    break;

                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void captureCtrlModifier(Keys key)
        {
            switch (key)
            {
                case Keys.Up: // CTRL + UP
                    SendKeys.Send("^{HOME}");
                    break;

                case Keys.Down:
                    SendKeys.Send("^{END}");
                    break;
            }
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == Constants.WM_HOTKEY_MSG_ID)
            {
                Keys key = (Keys)(((int)m.LParam >> 16) & 0xFFFF);
                int modifier = (int)m.LParam & 0xFFFF;

                if (modifier == Constants.NOMOD)
                    captureAll(key);
                else if (modifier == Constants.CTRL)
                    captureCtrlModifier(key);
            }

            base.WndProc(ref m);
        }

        protected virtual void registerGlobalHotkey()
        {
            ghk_UP = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.Up, this);
            ghk_UP.Register();

            ghk_DOWN = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.Down, this);
            ghk_DOWN.Register();

            ghk_ESC = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.Escape, this);
            ghk_ESC.Register();

            ghk_CTRL_UP = new Hotkeys.GlobalHotkey(Constants.CTRL, Keys.Up, this);
            ghk_CTRL_UP.Register();

            ghk_CTRL_DOWN = new Hotkeys.GlobalHotkey(Constants.CTRL, Keys.Down, this);
            ghk_CTRL_DOWN.Register();

            navKeyRegistered = true;
        }

        protected virtual void unregisterGlobalHotkey()
        {
            ghk_UP.Unregister();
            ghk_DOWN.Unregister();
            ghk_ESC.Unregister();
            ghk_CTRL_UP.Unregister();
            ghk_CTRL_DOWN.Unregister();

            navKeyRegistered = false;
        }

        private void newButton_Click(object sender, EventArgs e)
        {
            newButtonAction();
        }

        private void displayAllButton_Click(object sender, EventArgs e)
        {
            displayAllAction();
        }

        private void namaTextBox_TextChanged(object sender, EventArgs e)
        {
            displayAllAction();
        }

        private void nonActiveCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            displayAllAction();
        }

        private void dataGridView_DoubleClick(object sender, EventArgs e)
        {
            if (dataGridView.Rows.Count <= 0)
                return;

            int selectedrowindex = dataGridView.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dataGridView.Rows[selectedrowindex];

            dbGridDoubleClickAction(selectedRow);
        }

        private void dataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (dataGridView.Rows.Count <= 0)
                return;

            int selectedrowindex = dataGridView.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dataGridView.Rows[selectedrowindex];

            if (e.KeyCode == Keys.Enter)
                dbGridDoubleClickAction(selectedRow);
        }

        private void simpleBrowseForm_Activated(object sender, EventArgs e)
        {
            registerGlobalHotkey();

            displayAllAction();
        }

        private void simpleBrowseForm_Load(object sender, EventArgs e)
        {
            namaTextBox.Select();
            displayAllAction();
        }

        private void simpleBrowseForm_Deactivate(object sender, EventArgs e)
        {
            if (navKeyRegistered)
                unregisterGlobalHotkey();
        }

        private void dataGridView_Enter(object sender, EventArgs e)
        {
            if (navKeyRegistered)
                unregisterGlobalHotkey();
        }

        private void dataGridView_Leave(object sender, EventArgs e)
        {
            if (!navKeyRegistered)
                registerGlobalHotkey();
        }

        protected virtual void setVisibility(Button buttonToSet, bool visibilityValue)
        {
            buttonToSet.Visible = visibilityValue;
        }

        protected virtual void setNewButtonEnable(bool enableValue)
        {
            newButton.Enabled = enableValue;
        }

        protected virtual void setNewButtonVisible(bool visibleValue)
        {
            newButton.Visible = visibleValue;
        }

        protected virtual void hideNonActiveCheckBox()
        {
            nonActiveCheckBox.Visible = false;
        }

        protected virtual void disableNonActiveCheckBox()
        {
            nonActiveCheckBox.Enabled = false;
        }

        protected virtual string getNamaTextBox()
        {
            return namaTextBox.Text;
        }

        protected virtual bool nonActiveIncluded()
        {
            return nonActiveCheckBox.Checked;
        }

        protected virtual void newButtonAction() {}

        protected virtual void displayAllAction() { }

        protected virtual void dbGridDoubleClickAction(DataGridViewRow selectedRow){}

        protected virtual void cellFormatting(DataGridView dbGrid) { }

        private void simpleBrowseForm_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void dataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            cellFormatting((DataGridView) sender);
        }
    }
}
