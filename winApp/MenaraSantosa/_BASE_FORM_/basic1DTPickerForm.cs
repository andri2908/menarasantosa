﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class basic1DTPickerForm : AlphaSoft.basicHotkeysForm
    {
        private globalUtilities gUtil = new globalUtilities();

        public DateTime dtValue = DateTime.Now;
        public bool dateChanged = false;

        public basic1DTPickerForm(DateTime dtValueParam, string labelValue = "INPUT")
        {
            InitializeComponent();

            label2.Text = labelValue;
            dtPickerInput.Value = dtValueParam;
        }

        private void basic1DTPickerForm_Load(object sender, EventArgs e)
        {
            gUtil.reArrangeTabOrder(this);
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            dtValue = dtPickerInput.Value;
            dateChanged = true;

            this.Close();
        }

        private void basic1DTPickerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            string dtValueString = gUtil.getCustomStringFormatDate(dtPickerInput.Value, true);

            if (dtPickerInput.Value == DateTime.Now)
            {
                if (DialogResult.Yes == MessageBox.Show("TANGGAL INPUT SAMA DENGAN HARI INI ? \n [" + dtValueString + "]", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    dtValue = dtPickerInput.Value;
                    dateChanged = true;
                }
                else
                    e.Cancel = true;
            }
        }
    }
}
