﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Hotkeys;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace AlphaSoft
{
    public partial class basicItemSelector : basicHotkeysForm
    {
        private List<gridColumns> gridColumnsList;
        public List<gridRowsValue> gridRowsValueList;
        private int originModuleID = 0;
        public double returnPrice = 0;

        public basicItemSelector(List<gridColumns> gridColumnsListParam, List<gridRowsValue> gridRowsValueListParam, int moduleID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;

            gridColumnsList = gridColumnsListParam;
            gridRowsValueList = gridRowsValueListParam;

            dataGridView.Rows.Clear();
            dataGridView.Columns.Clear();

            setGridColumns();
            fillGridValues();
        }

        private void setGridColumns()
        {
            DataGridViewTextBoxColumn textBoxColumn;
            DataGridViewCheckBoxColumn checkBoxColumn;
            DataGridViewColumn gridViewColumn = null;

            for (int i = 0;i<gridColumnsList.Count;i++)
            {
                if (gridColumnsList[i].gridColumnType == 1) // TEXTBOX
                {
                    textBoxColumn = new DataGridViewTextBoxColumn();
                    textBoxColumn.Name = gridColumnsList[i].gridName;
                    textBoxColumn.HeaderText = gridColumnsList[i].gridHeaderText;
                    gridViewColumn = textBoxColumn;
                }
                else if (gridColumnsList[i].gridColumnType == 2) // CHECKBOX
                {
                    checkBoxColumn = new DataGridViewCheckBoxColumn();
                    checkBoxColumn.Name = gridColumnsList[i].gridName;
                    checkBoxColumn.HeaderText = gridColumnsList[i].gridHeaderText;
                    gridViewColumn = checkBoxColumn;
                }

                gridViewColumn.Visible = gridColumnsList[i].gridColumnVisible;
                gridViewColumn.ReadOnly = gridColumnsList[i].gridColumnReadOnly;
                gridViewColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView.Columns.Add(gridViewColumn);
            }
        }

        private void fillGridValues()
        {
            for (int i = 0;i < gridRowsValueList.Count;i++)
            {
                dataGridView.Rows.Add();
                for (int j = 0;j<gridRowsValueList[i].gridValue.Count; j++)
                {
                    dataGridView.Rows[dataGridView.Rows.Count - 1].Cells[j].Value = gridRowsValueList[i].gridValue[j];
                }
            }
        }

        private void setReturnValue()
        {
            for (int i = 0;i<dataGridView.Rows.Count;i++)
            {
                for (int j = 0;j<dataGridView.Columns.Count;j++)
                {
                    gridRowsValueList[i].gridValue[j] = dataGridView.Rows[i].Cells[j].Value;
                }
            }
        }

        private void basicItemSelector_FormClosed(object sender, FormClosedEventArgs e)
        {
            setReturnValue();
        }

        private void dataGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataGridView.IsCurrentCellDirty)
            {
                dataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void returPOdataGridView_DoubleClick(object sender, EventArgs e)
        {
            if (dataGridView.Rows.Count > 0)
            {
                int selectedrowindex = dataGridView.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dataGridView.Rows[selectedrowindex];

                returnPrice = Convert.ToDouble(selectedRow.Cells["productHarga"].Value);
                this.Close();
            }
        }
    }
}
