﻿namespace AlphaSoft
{
    partial class dataBrowse_customerCareForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dataBrowse_customerCareForm));
            this.searchKompleks = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.blokTextBox = new System.Windows.Forms.TextBox();
            this.searchBlok = new System.Windows.Forms.Button();
            this.closedTicketCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.closedTicketCheckBox);
            this.groupBox1.Controls.Add(this.searchBlok);
            this.groupBox1.Controls.Add(this.searchKompleks);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.blokTextBox);
            this.groupBox1.Size = new System.Drawing.Size(865, 129);
            this.groupBox1.Controls.SetChildIndex(this.namaTextBox, 0);
            this.groupBox1.Controls.SetChildIndex(this.nonActiveCheckBox, 0);
            this.groupBox1.Controls.SetChildIndex(this.label1, 0);
            this.groupBox1.Controls.SetChildIndex(this.label2, 0);
            this.groupBox1.Controls.SetChildIndex(this.startDTPicker, 0);
            this.groupBox1.Controls.SetChildIndex(this.label3, 0);
            this.groupBox1.Controls.SetChildIndex(this.endDTPicker, 0);
            this.groupBox1.Controls.SetChildIndex(this.blokTextBox, 0);
            this.groupBox1.Controls.SetChildIndex(this.label4, 0);
            this.groupBox1.Controls.SetChildIndex(this.searchKompleks, 0);
            this.groupBox1.Controls.SetChildIndex(this.searchBlok, 0);
            this.groupBox1.Controls.SetChildIndex(this.closedTicketCheckBox, 0);
            // 
            // displayButton
            // 
            this.displayButton.Location = new System.Drawing.Point(387, 143);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(475, 74);
            this.label1.Size = new System.Drawing.Size(45, 18);
            this.label1.Text = "Blok";
            // 
            // newButton
            // 
            this.newButton.Enabled = false;
            this.newButton.Location = new System.Drawing.Point(764, 143);
            this.newButton.Visible = false;
            // 
            // namaTextBox
            // 
            this.namaTextBox.Location = new System.Drawing.Point(105, 69);
            this.namaTextBox.ReadOnly = true;
            // 
            // searchKompleks
            // 
            this.searchKompleks.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchKompleks.BackgroundImage")));
            this.searchKompleks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchKompleks.Location = new System.Drawing.Point(394, 67);
            this.searchKompleks.Name = "searchKompleks";
            this.searchKompleks.Size = new System.Drawing.Size(29, 30);
            this.searchKompleks.TabIndex = 65;
            this.searchKompleks.UseVisualStyleBackColor = true;
            this.searchKompleks.Click += new System.EventHandler(this.searchKompleks_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(7, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 18);
            this.label4.TabIndex = 63;
            this.label4.Text = "Kompleks";
            // 
            // blokTextBox
            // 
            this.blokTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.blokTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.blokTextBox.Location = new System.Drawing.Point(526, 69);
            this.blokTextBox.Name = "blokTextBox";
            this.blokTextBox.ReadOnly = true;
            this.blokTextBox.Size = new System.Drawing.Size(250, 27);
            this.blokTextBox.TabIndex = 64;
            // 
            // searchBlok
            // 
            this.searchBlok.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchBlok.BackgroundImage")));
            this.searchBlok.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchBlok.Location = new System.Drawing.Point(782, 67);
            this.searchBlok.Name = "searchBlok";
            this.searchBlok.Size = new System.Drawing.Size(29, 30);
            this.searchBlok.TabIndex = 66;
            this.searchBlok.UseVisualStyleBackColor = true;
            this.searchBlok.Click += new System.EventHandler(this.searchBlok_Click);
            // 
            // closedTicketCheckBox
            // 
            this.closedTicketCheckBox.AutoSize = true;
            this.closedTicketCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closedTicketCheckBox.Location = new System.Drawing.Point(324, 100);
            this.closedTicketCheckBox.Name = "closedTicketCheckBox";
            this.closedTicketCheckBox.Size = new System.Drawing.Size(154, 19);
            this.closedTicketCheckBox.TabIndex = 67;
            this.closedTicketCheckBox.Text = "Tampilkan Tiket Closed";
            this.closedTicketCheckBox.UseVisualStyleBackColor = true;
            // 
            // dataBrowse_customerCareForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(926, 640);
            this.Name = "dataBrowse_customerCareForm";
            this.Load += new System.EventHandler(this.dataBrowse_customerCareForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button searchKompleks;
        protected System.Windows.Forms.Label label4;
        protected System.Windows.Forms.TextBox blokTextBox;
        private System.Windows.Forms.Button searchBlok;
        protected System.Windows.Forms.CheckBox closedTicketCheckBox;
    }
}
