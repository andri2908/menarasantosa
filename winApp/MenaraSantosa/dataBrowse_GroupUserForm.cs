﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class dataBrowse_GroupUserForm : simpleBrowseForm
    {
        private int originModuleID = 0;
        private globalUserUtil gUtil;
        private globalRestAPI gRest;

        public REST_groupUser groupUserData = new REST_groupUser();

        public string Ret_groupDesc = "";

        public dataBrowse_GroupUserForm(int moduleID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();
        }

        private void dataGroupForm_Load(object sender, EventArgs e)
        {
            displayAllAction();

            if (!gUtil.RS_userHasAccessTo(MMConstants.TAMBAH_HAPUS_GROUP_USER,
                MMConstants.HAK_NEW_DATA))
                setNewButtonEnable(false);
        }

        protected override void displayAllAction()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string groupName = "";

            dataGridView.DataSource = null;

            groupName = getNamaTextBox();

            sqlCommand = "SELECT * " +
                                    "FROM master_group " +
                                    "WHERE 1 = 1 ";

            if (groupName.Length > 0)
                sqlCommand = sqlCommand + "AND group_name LIKE '%" + groupName + "%' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY group_name ASC";

            gRest.getMasterGroupData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref groupUserData);

            dt.Columns.Add("GROUP_ID");
            dt.Columns.Add("NAMA GROUP");
            dt.Columns.Add("DESKRIPSI");

            for(int i = 0;i<groupUserData.dataList.Count;i++)
            {
                DataRow r = dt.Rows.Add();
                r["GROUP_ID"] = groupUserData.dataList[i].group_id;
                r["NAMA GROUP"] = groupUserData.dataList[i].group_name;
                r["DESKRIPSI"] = groupUserData.dataList[i].group_description;
            }

            dataGridView.DataSource = dt;
            dataGridView.Columns["GROUP_ID"].Visible = false;
        }

        protected override void dbGridDoubleClickAction(DataGridViewRow selectedRow)
        {
            int selectedGroupUserID = Convert.ToInt32(selectedRow.Cells["GROUP_ID"].Value);

            switch (originModuleID)
            {
                case globalConstants.MODULE_HAK_AKSES:
                    dataPengaturanHakAkses displayHakAksesForm = new dataPengaturanHakAkses(selectedGroupUserID, 
                        selectedRow.Cells["NAMA GROUP"].Value.ToString(), selectedRow.Cells["DESKRIPSI"].Value.ToString());
                    displayHakAksesForm.ShowDialog(this);
                    break;

                case globalConstants.MODULE_USER:
                case globalConstants.MODULE_DEFAULT:
                    ReturnValue1 = selectedRow.Cells["GROUP_ID"].Value.ToString();
                    ReturnValue2 = selectedRow.Cells["NAMA GROUP"].Value.ToString();
                    Ret_groupDesc = selectedRow.Cells["DESKRIPSI"].Value.ToString();

                    this.Close();
                    break;

                default:
                    dataGroupUserDetailForm editGroupUserForm = new dataGroupUserDetailForm(selectedGroupUserID);
                    editGroupUserForm.ShowDialog(this);
                    break;
            }
        }

        protected override void newButtonAction()
        {
            dataGroupUserDetailForm displayForm = new dataGroupUserDetailForm();
            displayForm.ShowDialog(this);
        }
    }
}
