﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Globalization;

namespace AlphaSoft
{
    public partial class dataPengerjaanRepair : AlphaSoft.basicHotkeysForm
    {
        private int ticketID = 0;
        private CultureInfo culture = new CultureInfo("id-ID");

        private globalRestAPI gRest;
        private globalUserUtil gUser;

        public dataPengerjaanRepair(int ticketIDParam)
        {
            InitializeComponent();

            ticketID = ticketIDParam;

            gRest = new globalRestAPI();
            gUser = new globalUserUtil();
        }

        private void loadDataDetail()
        {
            string sqlCommand = "";
            REST_custCare detailData = new REST_custCare();

            #region DETAIL
            sqlCommand = "SELECT cc.ticket_num, cc.ticket_id, ifnull(cc.start_date, NOW()) as start_date, cc.vendor_name, " +
                                  "cc.subject, cc.description, cc.is_mtc, ifnull(cc.mtc_cost, 0) as mtc_cost, " +
                                  "ml.kompleks_name, " +
                                  "mb.blok_name, mk.house_no " +
                                  "FROM cc_ticket cc " +
                                  "LEFT OUTER JOIN user_login_data ul ON (cc.request_by = ul.user_id) " +
                                  "LEFT OUTER JOIN master_kavling mk ON (cc.kavling_id = mk.kavling_id AND mk.is_active = 'Y') " +
                                  "LEFT OUTER JOIN master_kompleks ml ON (mk.kompleks_id = ml.kompleks_id AND ml.is_active = 'Y') " +
                                  "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id AND mb.is_active = 'Y') " +
                                  "WHERE cc.ticket_id = " + ticketID;

            if (gRest.getCustCareData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref detailData))
            {
                if (detailData.dataStatus.o_status == 1)
                {
                    noTicketTextBox.Text = detailData.dataList[0].ticket_num.ToString();
                    subjectTextBox.Text = detailData.dataList[0].subject;
                    descTextBox.Text = detailData.dataList[0].description;
                    startDTPicker.Value = detailData.dataList[0].start_date;
                    vendorTextBox.Text = detailData.dataList[0].vendor_name;

                    checkBoxMTC.Checked = (detailData.dataList[0].is_mtc == "Y" ? true : false);
                    biayaTextBox.Text = detailData.dataList[0].mtc_cost.ToString("N", culture);
                }
            }
            #endregion
        }

        private bool saveData()
        {
            bool result = false;
            string errMsg = "";

            DateTime startDate = DateTime.Now;
            string strDate = "";
            try
            {
                int newID = 0;

                startDate = new DateTime(startDTPicker.Value.Year, startDTPicker.Value.Month, startDTPicker.Value.Day);
                strDate = String.Format(culture, "{0:yyyy-MM-dd}", startDate);
                if (!gRest.saveCCWorkDetail(gUser.getUserID(), gUser.getUserToken(), ticketID, strDate, vendorTextBox.Text,
                   out errMsg, out newID)
                    )
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            errorLabel.Text = "";
            if (saveData())
            {
                MessageBox.Show("SUCCESS");
                this.Close();
            }
            else
            {
                MessageBox.Show("FAIL");
            }
        }

        private void dataPengerjaanRepair_Load(object sender, EventArgs e)
        {
            loadDataDetail();
        }

        private bool konfirmasiTicket()
        {
            bool result = false;
            string errMsg = "";
            int newID = 0;

            try
            {
                if (!gRest.cc_confirmation(gUser.getUserID(), gUser.getUserToken(),
                    ticketID, out errMsg, out newID)
                    )
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private void confirmationButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Konfirmasi selesai ke user ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {
                if (konfirmasiTicket())
                {
                    MessageBox.Show("Success");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Fail");
                }
            }
        }
    }
}
