﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class dataBrowse_BlokForm : AlphaSoft.simpleBrowseForm
    {
        private int originModuleID = 0;
        private int selectedKompleksID = 0;

        private globalUserUtil gUtil;
        private globalRestAPI gRest;

        public dataBrowse_BlokForm(int moduleID = 0, int kompleksID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;
            selectedKompleksID = kompleksID;

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();

            ReturnValue1 = "0";
            ReturnValue2 = "0";
        }

        private void dataBrowse_BlokForm_Load(object sender, EventArgs e)
        {
            setNewButtonVisible(false);
        }

        protected override void displayAllAction()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string paramName = "";

            REST_masterBlok blokData = new REST_masterBlok();

            paramName = getNamaTextBox();

            sqlCommand = "SELECT blok_id, blok_name " +
                                    "FROM master_blok " +
                                    "WHERE kompleks_id = " + selectedKompleksID + " ";

            if (paramName.Length > 0)
                sqlCommand = sqlCommand + "AND blok_name LIKE '%" + paramName + "%' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY blok_name ASC";

            if (gRest.getMasterBlokData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref blokData))
            {
                if (blokData.dataStatus.o_status == 1)
                {
                    dataGridView.DataSource = null;

                    dt.Columns.Add("BLOK_ID");
                    dt.Columns.Add("NAMA BLOK");

                    for (int i = 0; i < blokData.dataList.Count; i++)
                    {
                        DataRow r = dt.Rows.Add();
                        r["BLOK_ID"] = blokData.dataList[i].blok_id;
                        r["NAMA BLOK"] = blokData.dataList[i].blok_name;
                    }

                    dataGridView.DataSource = dt;
                    dataGridView.Columns["BLOK_ID"].Visible = false;
                }
            }
        }

        protected override void dbGridDoubleClickAction(DataGridViewRow selectedRow)
        {
            int selectedID = Convert.ToInt32(selectedRow.Cells["BLOK_ID"].Value);

            switch (originModuleID)
            {
                default:
                    ReturnValue1 = selectedRow.Cells["BLOK_ID"].Value.ToString();
                    ReturnValue2 = selectedRow.Cells["NAMA BLOK"].Value.ToString();
                    this.Close();
                    break;
            }
        }


        private void dataBrowse_BlokForm_FormClosed(object sender, FormClosedEventArgs e)
        {
        }
    }
}
