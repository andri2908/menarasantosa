﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaSoft
{
    public class REST_printoutTransaksi
    {
        public List<PRINTOUT_transaksi> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_reportSysConfig
    {
        public List<report_sysConfig> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class report_sysConfig
    {
        public string name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
    }

    public class PRINTOUT_transaksi
    {
        public string title { get; set; }
        public string user_full_name { get; set; }
        public string user_name { get; set; }
        public string nama_kavling { get; set; }
        public string user_phone_1 { get; set; }
        public string item_1 { get; set; }
        public double qty { get; set; }
        public double harga { get; set; }
        public double nominal { get; set; }
        public string item_2 { get; set; }
    }

    public class REST_laporanIPL
    {
        public List<REPORT_summaryIPL> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }


    public class REPORT_summaryIPL
    {
        public int kompleks_id { get; set; }
        public string kompleks_name { get; set; }
        public int user_id { get; set; }
        public string user_name { get; set; }
        public string user_full_name { get; set; }
        public int kavling_id { get; set; }
        public string blok_name { get; set; }
        public string house_no { get; set; }
        public DateTime tgl_bast { get; set; }
        public DateTime start_ipl { get; set; }
        public DateTime end_ipl { get; set; }
        public double nominal { get; set; }
        public string is_manual_paid { get; set; }
    }

    public class REST_IPLUnpaid
    {
        public List<IPL_unpaidPeriode> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class IPL_unpaidPeriode
    {
        public DateTime start_ipl { get; set; }
        public DateTime end_ipl { get; set; }
    }

    public class REST_laporanIPLTahunan
    {
        public List<REPORT_IPLTahunan> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_laporanAksesKompleks
    {
        public List<REPORT_aksesKompleks> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REPORT_aksesKompleks
    {
        public int kompleks_id{ get; set; }
        public int kavling_id { get; set; }
        public string kompleks_name { get; set; }
        public string blok_name { get; set; }
        public string house_no { get; set; }
        public int user_id { get; set; }
        public string user_name { get; set; }
        public string user_full_name { get; set; }
        public string access_type { get; set; }
        public string access_value { get; set; }
        public string access_status { get; set; }
        public string access_enabled { get; set; }
        public string is_active { get; set; }
    }

    public class REST_laporanKavling
    {
        public List<REPORT_kavling_struct> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REPORT_kavling_struct
    {
        public int kompleks_id { get; set; }
        public int kavling_id { get; set; }
        public string kompleks_name { get; set; }
        public string blok_name { get; set; }
        public string house_no { get; set; }
        public int user_id { get; set; }
        public string user_name { get; set; }
        public string user_full_name { get; set; }
    }


    public class REPORT_IPLTahunan
    {
        public int kompleks_id { get; set; }
        public string kompleks_name { get; set; }
        public int kavling_id { get; set; }
        public int blok_id { get; set; }
        public string blok_name { get; set; }
        public string house_no { get; set; }
        public int user_id { get; set; }
        public string user_name { get; set; }
        public string user_full_name { get; set; }
        public DateTime tgl_bast { get; set; }
        public double nominal_0 { get; set; }
        public double nominal_1 { get; set; }
        public double nominal_2 { get; set; }
        public double nominal_3 { get; set; }
        public double nominal_4 { get; set; }
        public double nominal_5 { get; set; }
        public double nominal_6 { get; set; }
        public double nominal_7 { get; set; }
        public double nominal_8 { get; set; }
        public double nominal_9 { get; set; }
        public double nominal_10 { get; set; }
        public double nominal_11 { get; set; }
    }

    public class REST_tagihanIPLManual
    {
        public List<REPORT_tagihanIPL> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REPORT_tagihanIPL
    {
        public int kavling_id { get; set; }
        public string kompleks_name { get; set; }
        public string blok_name { get; set; }
        public string house_no { get; set; }
        public double luas_tanah { get; set; }
        public double luas_bangunan { get; set; }
        public int user_id { get; set; }
        public string user_name { get; set; }
        public string user_full_name { get; set; }
        public DateTime tgl_bast { get; set; }
        public DateTime usr_start_ipl { get; set; }

        public double nominal { get; set; }
        public double nominal_per_bulan { get; set; }
        public double total_bulan { get; set; }
        public int status_id { get; set; }
        public DateTime start_ipl { get; set; }
        public DateTime end_ipl { get; set; }
    }

}
