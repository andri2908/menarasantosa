﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace AlphaSoft
{
    class MyRenderer : ToolStripProfessionalRenderer
    {
        private class MyColors : ProfessionalColorTable //color scheme for menustrip
        {
            public override Color MenuItemSelected
            {
                get { return Color.MediumAquamarine; }
            }
            public override Color MenuItemSelectedGradientBegin
            {
                get { return Color.Aquamarine; }
            }
            public override Color MenuItemSelectedGradientEnd
            {
                get { return Color.DarkBlue; }
            }
            /*public override Color MenuItemBorder
            {
                get { return Color.Green; }
            }*/
            public override Color MenuStripGradientBegin
            {
                get { return Color.Green; }
            }
            public override Color MenuStripGradientEnd
            {
                get { return Color.DarkGreen; }
            }
            /*public override Color ToolStripGradientBegin
            {
                get { return Color.Black; }
            }
            public override Color ToolStripGradientEnd //ToolStripDropDownBackground
            {
                get { return Color.Black; }
            }*/
            public override Color MenuBorder
            {
                get { return Color.Black; }
            }
        }

        public MyRenderer() : base(new MyColors()) { }
    }
}
