﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.IO;

namespace AlphaSoft
{
    public partial class dataCustCare_detailForm : AlphaSoft.basicHotkeysForm
    {
        CultureInfo culture = new CultureInfo("id-ID");
        cc_ticket_history detailData;

        globalRestAPI gRest;
        globalUserUtil gUser;
        globalImageLib gImg;

        private bool isViewOnly = false;

        private List<imgAttachment> imgList = new List<imgAttachment>();
        imgAttachment imgAttach;
        private PictureBox[] pb;

        private int employeeID = 0;
        private string employeeToken = "";

        public dataCustCare_detailForm(ref cc_ticket_history paramData, bool viewOnly = false)
        {
            InitializeComponent();

            detailData = paramData;

            gRest = new globalRestAPI();
            gUser = new globalUserUtil();
            gImg = new globalImageLib();

            selectEmployeeID();

            isViewOnly = viewOnly;

            pb = new PictureBox[4];
            pb[0] = pictureBox1;
            pb[1] = pictureBox2;
            pb[2] = pictureBox3;
            pb[3] = pictureBox4;

            imgAttach = new imgAttachment();
            imgAttach.seqno = 1;
            imgList.Add(imgAttach);

            imgAttach = new imgAttachment();
            imgAttach.seqno = 2;
            imgList.Add(imgAttach);

            imgAttach = new imgAttachment();
            imgAttach.seqno = 3;
            imgList.Add(imgAttach);

            imgAttach = new imgAttachment();
            imgAttach.seqno = 4;
            imgList.Add(imgAttach);
        }

        private void selectEmployeeID()
        {
            while (employeeID == 0)
            {
                dataBrowse_employee displayForm = new dataBrowse_employee(globalConstants.MODULE_DEFAULT);
                displayForm.ShowDialog(this);

                if (displayForm.ReturnValue1 != "0")
                {
                    employeeID = Convert.ToInt32(displayForm.ReturnValue1);
                    labelLogin.Text = "Reply as [" + displayForm.ReturnValue2 + "] " + displayForm.retUserFullName;

                    genericReply replyResult = new genericReply();
                    gRest.getEmployeeToken(gUser.getUserID(), gUser.getUserToken(), employeeID, ref replyResult);

                    employeeToken = replyResult.data.ToString();
                }
            }
        }

        private void loadDetailData()
        {
            if (null != detailData)
            {
                commentTextBox.Text = detailData.description;
                biayaTextBox.Text = detailData.mtc_cost.ToString("N0", culture);

                if (detailData.is_mtc == "Y")
                    checkBoxMTC.Checked = true;

                if (detailData.is_active == "N")
                    nonAktifCheckBox.Checked = true;
            }
        }

        private void loadAttachmentData()
        {
            string sqlCommand = "";
            REST_imgAttachment imgAttach = new REST_imgAttachment();

            sqlCommand = "SELECT attach_id, attach_file as ftpFileName, seqno " +
                                    "FROM cc_ticket_history_attach " +
                                    "WHERE history_id = " + detailData.history_id + " " +
                                    "AND ticket_id = " + detailData.ticket_id + " " +
                                    "AND is_active = 'Y'";

            if (gRest.getCSAttachment(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref imgAttach))
            {
                if (imgAttach.dataStatus.o_status == 1)
                {
                    int idx = 0;

                    for (int i = 0;i<imgAttach.dataList.Count;i++)
                    {
                        idx = imgAttach.dataList[i].seqno - 1;

                        imgList[idx].attach_id = imgAttach.dataList[i].attach_id;
                        imgList[idx].ftpImage = true;
                        imgList[idx].ftpFileName = imgAttach.dataList[i].ftpFileName;
                        imgList[idx].is_active = "Y";

                        try
                        {
                            Bitmap bm;
                            bm = gImg.ByteToImage(imgList[idx].ftpFileName, globalImageLib.IMG_CS);
                            pb[idx].Image = bm;
                        }
                        catch (Exception ex)
                        {
                            pb[idx].Load(gImg.localImgDirectory + "errImg.jpeg");
                        }
                    }
                }
            }
        }

        private void loadHeaderData()
        {
            string sqlCommand = "";
            REST_custCare ccTicket = new REST_custCare();

            string sqlUser = "SELECT 'user' as sender_type, mu.user_id, mu.user_name, mu.user_full_name, mg.group_id " +
                                    "FROM user_login_data mu, master_group mg " +
                                    "WHERE mg.group_id = mu.group_id " +
                                    "UNION ALL " +
                                    "SELECT 'admin' as sender_type, me.employee_id as user_id, me.employee_user_name as user_name, me.employee_full_name as user_full_name, 2 as group_id " +
                                    "FROM employee_login_data me";

            sqlCommand = "SELECT cc.ticket_id, cc.ticket_num, cc.ticket_date, cc.request_by, cc.kavling_id, cc.kompleks_id, " +
                                    "cc.subject, cc.ticket_type AS category_name, cc.description, cc.status, cc.is_closed, cc.is_active, " +
                                    "ul.user_name, ul.user_full_name, ml.kompleks_name, " +
                                    "mb.blok_name, mk.house_no " +
                                    "FROM cc_ticket cc " +
                                    "LEFT OUTER JOIN (" + sqlUser + ") ul ON (cc.request_by = ul.user_id AND if(cc.sender_type = null, '', convert(cc.sender_type, char)) = ul.sender_type) " +
                                    "LEFT OUTER JOIN master_kavling mk ON (cc.kavling_id = mk.kavling_id AND mk.is_active = 'Y') " +
                                    "LEFT OUTER JOIN master_kompleks ml ON (mk.kompleks_id = ml.kompleks_id AND ml.is_active = 'Y') " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id AND mb.is_active = 'Y') " +
                                    "WHERE cc.ticket_id = " + detailData.ticket_id;
            
            if (gRest.getCustCareData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref ccTicket))
            {
                if (ccTicket.dataStatus.o_status == 1)
                {
                    noTicketTextBox.Text = ccTicket.dataList[0].ticket_num;
                    subjectTextBox.Text = ccTicket.dataList[0].subject;
                    descTextBox.Text = ccTicket.dataList[0].description;
                }
            }
        }

        private void dataCustCare_detailForm_Load(object sender, EventArgs e)
        {
            loadHeaderData();

            loadDetailData();
            loadAttachmentData();

            biayaTextBox.Enter += TextBox_Enter;
            biayaTextBox.Leave += TextBox_Int32_Leave;

            if (isViewOnly)
                gUser.disableAllControls(this);
        }

        private bool saveData()
        {
            bool result = false;
            string errMsg = "";
            double biayaValue = 0;

            List<cc_ticket_history> listDetail = new List<cc_ticket_history>();
            List<cc_ticket> headerData = new List<cc_ticket>();
            List<ccAttach> listAttach = new List<ccAttach>();
            ccAttach attachImg;
            cc_ticket ticketData = new cc_ticket();

            try
            {
                double.TryParse(biayaTextBox.Text, NumberStyles.Number, culture, out biayaValue);

                detailData.description = commentTextBox.Text;
                detailData.mtc_cost = biayaValue;

                if (detailData.history_id == 0)
                {
                    detailData.send_date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    detailData.sent_by = gUser.getUserID();
                }

                detailData.is_active = (nonAktifCheckBox.Checked ? "N" : "Y");
                detailData.is_mtc = (checkBoxMTC.Checked ? "Y" : "N");

                listDetail.Add(detailData);

                if (!nonAktifCheckBox.Checked)
                {
                    ticketData.ticket_id = detailData.ticket_id;
                    ticketData.status_by = gUser.getUserID();
                    ticketData.status_date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

                    if (checkBoxMTC.Checked)
                    {
                        ticketData.status = globalConstants.TS_request_approval;
                        ticketData.is_mtc = "Y";
                        ticketData.mtc_cost = biayaValue;
                        ticketData.mtc_detail = commentTextBox.Text;
                    }
                    else
                    {
                        ticketData.status = globalConstants.TS_admin_response;
                        ticketData.is_mtc = "N";
                        ticketData.mtc_cost = 0;
                        ticketData.mtc_detail = "";
                    }

                    headerData.Add(ticketData);
                }


                for (int i = 0;i<imgList.Count;i++)
                {
                   
                    if (imgList[i].fileName.Length > 0)
                    {
                        attachImg = new ccAttach();

                        using (Image image = pb[i].Image)
                        {
                            using (MemoryStream m = new MemoryStream())
                            {
                                image.Save(m, image.RawFormat);
                                byte[] imageBytes = m.ToArray();

                                // Convert byte[] to Base64 String
                                string base64String = Convert.ToBase64String(imageBytes);
                                attachImg.raw_photo = base64String;
                                attachImg.seqno = imgList[i].seqno;
                            }
                        }

                        listAttach.Add(attachImg);
                    }
                }

                int newID = 0;

                ccAttach[] array = listAttach.ToArray();
                if (!gRest.saveCCDetail(gUser.getUserID(), employeeToken, employeeID, // gUser.getUserToken(), gUser.getUserID(),
                   detailData.ticket_id, detailData.description, detailData.is_mtc, detailData.mtc_cost, (confirmationCheckBox.Checked ? "Y" : "N"),
                   "admin", array, out errMsg, out newID)
                    )
                {
                    throw new Exception(errMsg);
                }  

                if (detailData.history_id == 0)
                    detailData.history_id = newID;

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            errorLabel.Text = "";
            if (commentTextBox.Text.Length > 0)
            {
                if (saveData())
                {
                    MessageBox.Show("SUCCESS");

                    for (int i = 0;i<imgList.Count;i++)
                    {
                        gImg.clearLocalImgDir(imgList[i].ftpFileName);
                    }

                    this.Close();
                }
                else
                {
                    MessageBox.Show("FAIL");
                }
            }
            else
            {
                MessageBox.Show("FAIL");
                errorLabel.Text = "Comment kosong";
                return;
            }
        }

        private void addImg(int idx, PictureBox pbBox)
        {
            string fileName = "";
            string ftpFileName = "";
            string inputName = String.Format(culture, "{0:yyyyMMddhhmmss}", DateTime.Now) + gUser.getRandomString(15);
            string sqlCommand = "";

            imgFileDialog.Filter = "Image Files(*.JPG; *.JPEG)| *.JPG; *.JPEG";
            if (imgFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    fileName = imgFileDialog.FileName;
                    pbBox.Load(fileName);

                    imgList[idx].fileName = fileName;
                    imgList[idx].ftpImage = false;
                    imgList[idx].is_active = "Y";

                    if (imgList[idx].attach_id <= 0)
                    {
                        while (true)
                        {
                            ftpFileName = gUser.getMD5Value(inputName) + ".jpg";

                            sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                                    "FROM cc_ticket_history_attach mi " +
                                                    "WHERE mi.attach_file = '" + ftpFileName + "' " +
                                                    "AND mi.is_active = 'Y'";

                            genericReply replyResult = new genericReply();
                            if (gRest.checkDataExist(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
                            {
                                if (Convert.ToInt32(replyResult.data) <= 0)
                                {
                                    break;
                                }
                            }
                        }
                        imgList[idx].ftpFileName = ftpFileName;
                    }

                    gImg.ResizeImage(fileName, imgList[idx].ftpFileName);
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show("File gambar tidak bisa dibuka : " + ex.Message);
                }
            }
        }

        private void delImg(int idx, PictureBox pbBox)
        {
            gImg.clearLocalImgDir(imgList[idx].ftpFileName);
            imgList[idx].is_active = "N";

            pbBox.Image = null;
        }

        private void img1Button_Click(object sender, EventArgs e)
        {
            addImg(0, pictureBox1);
        }

        private void img2Button_Click(object sender, EventArgs e)
        {
            addImg(1, pictureBox2);
        }

        private void img3Button_Click(object sender, EventArgs e)
        {
            addImg(2, pictureBox3);
        }

        private void img4Button_Click(object sender, EventArgs e)
        {
            addImg(3, pictureBox4);
        }

        private void delImg1Button_Click(object sender, EventArgs e)
        {
            delImg(0, pictureBox1);
        }

        private void delImg2Button_Click(object sender, EventArgs e)
        {
            delImg(1, pictureBox2);
        }

        private void delImg3Button_Click(object sender, EventArgs e)
        {
            delImg(2, pictureBox3);
        }

        private void delImg4Button_Click(object sender, EventArgs e)
        {
            delImg(3, pictureBox4);
        }

        private void displayImage(int idx)
        {
            string fileName = "";
            if (imgList[idx].ftpImage)
                fileName = imgList[idx].ftpFileName;
            else
                fileName = imgList[idx].fileName;

            if (fileName.Length > 0)
            {
                displayImageForm displayForm = new displayImageForm(fileName, globalImageLib.IMG_CS, imgList[idx].ftpImage);
                displayForm.ShowDialog(this);
            }
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            displayImage(0);
        }

        private void pictureBox2_DoubleClick(object sender, EventArgs e)
        {
            displayImage(1);
        }

        private void pictureBox3_DoubleClick(object sender, EventArgs e)
        {
            displayImage(2);
        }

        private void pictureBox4_DoubleClick(object sender, EventArgs e)
        {
            displayImage(3);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void checkBoxMTC_CheckedChanged(object sender, EventArgs e)
        {
            biayaTextBox.Enabled = checkBoxMTC.Checked;
        }
    }
}
