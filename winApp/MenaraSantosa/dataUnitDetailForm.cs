﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace AlphaSoft
{
    public partial class dataUnitDetailForm : AlphaSoft.basic2TextBoxInputsForm
    {
        private int originModuleID = 0;
        private int selectedUnitID = 0;
        private globalUserUtil gUtil;
        private globalRestAPI gRest;

        private CultureInfo culture = new CultureInfo("id-ID");

        public int returnUnitIDValue = 0;

        public dataUnitDetailForm(int unitID = 0)
        {
            InitializeComponent();
            selectedUnitID = unitID;

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();

            if (selectedUnitID > 0)
            {
                originModuleID = globalConstants.EDIT_UNIT;

                if (!gUtil.RS_userHasAccessTo(MMConstants.TAMBAH_HAPUS_UNIT,
                MMConstants.HAK_UPDATE_DATA))
                {
                    setSaveButtonEnable(false);
                    setResetButtonEnable(false);
                }
            }
            else
                originModuleID = globalConstants.NEW_UNIT;
        }

        private void dataUnitDetailForm_Load(object sender, EventArgs e)
        {
            if (selectedUnitID > 0)
            {
                loadDataInformation();
            }

            namaTextBox.Focus();
            gUtil.reArrangeTabOrder(this);
        }

        private void loadDataInformation()
        {
            DataTable dt = new DataTable();
            REST_masterUnit unitData = new REST_masterUnit();

            string sqlCommand = "SELECT * FROM master_unit WHERE unit_id =  " + selectedUnitID;

            if (gRest.getMasterUnitData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref unitData))
            {
                if (unitData.dataStatus.o_status == 1)
                {
                    namaTextBox.Text = unitData.dataList[0].unit_name;
                    nonAktifCheckbox.Checked = (unitData.dataList[0].is_active == "Y" ? false : true);
                }
            }
        }

        private void dataUnitDetailForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            returnUnitIDValue = selectedUnitID;
        }

        protected override bool saveDataTransaction()
        {
            bool result = false;
            string errMsg = "";
            int opType = 0;

            List<master_unit> listUnit = new List<master_unit>();
            master_unit unitData = new master_unit();

            try
            {
                switch (originModuleID)
                {
                    case globalConstants.NEW_UNIT:
                        opType = 1;
                        unitData.unit_id = 0;
                        break;

                    case globalConstants.EDIT_UNIT:
                        opType = 2;
                        unitData.unit_id = selectedUnitID;
                        break;
                }

                unitData.unit_name = namaTextBox.Text;
                unitData.is_active = (nonAktifCheckbox.Checked ? "N" : "Y");
                listUnit.Add(unitData);

                int newID = 0;
                if (!gRest.saveMasterUnit(gUtil.getUserID(), gUtil.getUserToken(), listUnit, opType, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                if (opType == 1)
                    selectedUnitID = newID;

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        protected override bool dataValidated()
        {
            bool result = false;

            result = base.dataValidated();

            if (!result)
                return false;

            errorLabel.Text = "";

            return true;
        }

        protected override void resetProcess()
        {
            base.resetProcess();

            selectedUnitID = 0;
            originModuleID = globalConstants.NEW_UNIT;
        }
    }
}
