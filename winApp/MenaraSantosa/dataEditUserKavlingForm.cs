﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class dataEditUserKavlingForm : AlphaSoft.basicHotkeysForm
    {
        userKavling rowUserKavling;

        public dataEditUserKavlingForm(userKavling rowUserKavlingParam)
        {
            InitializeComponent();

            rowUserKavling = rowUserKavlingParam;
        }

        private void dataEditUserKavlingForm_Load(object sender, EventArgs e)
        {
            userTextBox.Text = rowUserKavling.userName;
            dateDTPicker.Value = rowUserKavling.startIPL;
            bastDTPicker.Value = rowUserKavling.tglBAST; 
            remarkTextBox.Text = rowUserKavling.remarkValue;
            qrCodeTextBox.Text = rowUserKavling.qrCodeValue;
            qrAccessCheckBox.Checked = (rowUserKavling.qrFlagValue == "Y" ? true : false);
            checkBAST.Checked = (rowUserKavling.bastValue == "Y" ? true : false);
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Save data?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {
                rowUserKavling.tglBAST = bastDTPicker.Value;
                rowUserKavling.startIPL = new DateTime(dateDTPicker.Value.Year, dateDTPicker.Value.Month, 1);
                rowUserKavling.remarkValue = remarkTextBox.Text;
                rowUserKavling.qrFlagValue = (qrAccessCheckBox.Checked ? "Y": "N");
                rowUserKavling.bastValue = (checkBAST.Checked ? "Y" : "N");

                this.Close();
            }
        }

        private void printButton_Click(object sender, EventArgs e)
        {
            displayImageForm displayForm = new displayImageForm(qrCodeTextBox.Text, 800, 800);
            displayForm.ShowDialog(this);
        }

        private void checkBAST_CheckedChanged(object sender, EventArgs e)
        {
            bastDTPicker.Enabled = checkBAST.Checked;
        }
    }
}
