﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class dataBrowse_UnitForm : AlphaSoft.simpleBrowseForm
    {
        private int originModuleID = 0;
        private globalUserUtil gUtil;
        private globalRestAPI gRest;

        public dataBrowse_UnitForm(int moduleID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();

            ReturnValue1 = "0";
            ReturnValue2 = "0";
        }

        private void dataBrowse_unitForm_Load(object sender, EventArgs e)
        {
            displayAllAction();

            if (!gUtil.RS_userHasAccessTo(MMConstants.TAMBAH_HAPUS_UNIT,
                MMConstants.HAK_NEW_DATA))
                setNewButtonEnable(false);
        }

        protected override void displayAllAction()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string paramName = "";

            REST_masterUnit unitData = new REST_masterUnit();

            paramName = getNamaTextBox();

            sqlCommand = "SELECT unit_id, unit_name " +
                                    "FROM master_unit " +
                                    "WHERE 1 = 1 ";

            if (paramName.Length > 0)
                sqlCommand = sqlCommand + "AND unit_name LIKE '%" + paramName + "%' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY unit_name ASC";

            if (gRest.getMasterUnitData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref unitData))
            {
                if (unitData.dataStatus.o_status == 1)
                {
                    dataGridView.DataSource = null;

                    dt.Columns.Add("UNIT_ID");
                    dt.Columns.Add("SATUAN");

                    for (int i = 0; i < unitData.dataList.Count; i++)
                    {
                        DataRow r = dt.Rows.Add();
                        r["UNIT_ID"] = unitData.dataList[i].unit_id;
                        r["SATUAN"] = unitData.dataList[i].unit_name;
                    }

                    dataGridView.DataSource = dt;
                    dataGridView.Columns["UNIT_ID"].Visible = false;
                }
            }
        }

        protected override void dbGridDoubleClickAction(DataGridViewRow selectedRow)
        {
            int selectedID = Convert.ToInt32(selectedRow.Cells["UNIT_ID"].Value);

            switch (originModuleID)
            {
                case globalConstants.MODULE_DEFAULT:
                    ReturnValue1 = selectedRow.Cells["UNIT_ID"].Value.ToString();
                    ReturnValue2 = selectedRow.Cells["SATUAN"].Value.ToString();
                    this.Close();
                    break;

                default:
                    dataUnitDetailForm editForm = new dataUnitDetailForm(selectedID);
                    editForm.ShowDialog(this);
                    break;
            }
        }

        protected override void newButtonAction()
        {
            dataUnitDetailForm displayForm = new dataUnitDetailForm();
            displayForm.ShowDialog(this);
        }

        private void dataBrowse_unitForm_FormClosed(object sender, FormClosedEventArgs e)
        {
        }
    }
}
