﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using DBGridExtension;

namespace AlphaSoft
{
    public partial class dataUserKavlingForm : AlphaSoft.basicHotkeysForm
    {
        private int selectedKavlingID = 0;
        private int selectedKompleksID = 0;

        private globalUserUtil gUser;
        private globalIPL gIPL;
        private globalRestAPI gRest;

        private CultureInfo culture = new CultureInfo("id-ID");
        private userKavling rowUserKavling = new userKavling();

        List<cbDataSource> cbDT = new List<cbDataSource>();
        List<userKavlingRFID> userKavRFID = new List<userKavlingRFID>();

        public dataUserKavlingForm()
        {
            InitializeComponent();

            gUser = new globalUserUtil();
            gIPL = new globalIPL();
            gRest = new globalRestAPI();
        }

        private void loadKavling()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            REST_masterKavling kavlingData = new REST_masterKavling();

            sqlCommand = "SELECT mk.kavling_id, mk.status, " +
                                    "IFNULL(mb.blok_name, '') AS blok_name, mk.house_no " +
                                    "FROM master_kavling mk " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id) " +
                                    "WHERE mk.is_active = 'Y' " +
                                    "AND mk.kompleks_id = " + selectedKompleksID + " ";
            sqlCommand = sqlCommand + "ORDER BY CONCAT(IFNULL(mb.blok_name, ''), '-', mk.house_no) ASC";

            if (gRest.getMasterKavlingData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref kavlingData))
            {
                if (kavlingData.dataStatus.o_status == 1)
                {
                    kavlingGridView.SelectionChanged -= CUSTOM_kavlingGridView_SelectionChanged;

                    selectedKavlingID = 0;
                    kavlingGridView.Rows.Clear();

                    for (int i = 0;i<kavlingData.dataList.Count;i++)
                    {
                        if (selectedKavlingID == 0)
                            selectedKavlingID = kavlingData.dataList[i].kavling_id;

                        kavlingGridView.Rows.Add(
                            kavlingData.dataList[i].kavling_id,
                            kavlingData.dataList[i].blok_name + "-" + kavlingData.dataList[i].house_no,
                            kavlingData.dataList[i].status
                            );
                    }

                    kavlingGridView.SelectionChanged += CUSTOM_kavlingGridView_SelectionChanged;

                    loadUser(selectedKavlingID);
                }
            }
        }

        private void loadUser(int kavlingID)
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            REST_userKavling userKavData = new REST_userKavling();

            sqlCommand = "SELECT uk.id, " +
                                    "uk.user_id, ul.user_full_name, ul.user_name, uk.start_ipl, uk.remarks, uk.user_qr_code, uk.user_qr_enabled, uk.tgl_bast, uk.bast_created " +
                                    "FROM user_kavling uk " +
                                    "LEFT OUTER JOIN user_login_data ul ON (uk.user_id = ul.user_id) " +
                                    "WHERE uk.is_active = 'Y' " +
                                    "AND uk.kavling_id = " + kavlingID + " " +
                                    "ORDER BY ul.user_name ASC";

            if (gRest.getUserKavlingData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref userKavData))
            {
                if (userKavData.dataStatus.o_status == 1)
                {
                    userGridView.Rows.Clear();

                    for (int i = 0;i<userKavData.dataList.Count;i++)
                    {
                        userGridView.Rows.Add(
                            0,
                            userKavData.dataList[i].id,
                            userKavData.dataList[i].user_id,
                            userKavData.dataList[i].user_name,
                            userKavData.dataList[i].user_full_name,
                            userKavData.dataList[i].tgl_bast,
                            string.Format(culture, "{0:dd-MM-yyyy}", userKavData.dataList[i].tgl_bast),
                            userKavData.dataList[i].start_ipl,
                            string.Format(culture, "{0:dd-MM-yyyy}", userKavData.dataList[i].start_ipl),
                            userKavData.dataList[i].remarks,
                            userKavData.dataList[i].user_qr_code,
                            userKavData.dataList[i].user_qr_enabled,
                            userKavData.dataList[i].bast_created
                            );
                    }
                }
            }
        }

        private void searchKompleksButton_Click(object sender, EventArgs e)
        {
            dataBrowse_KompleksForm displayForm = new dataBrowse_KompleksForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                if (displayForm.ReturnValue1 != selectedKompleksID.ToString())
                {
                    if (kavlingGridView.Rows.Count <= 0 ||
                        DialogResult.Yes == MessageBox.Show("Ganti kompleks ? Data yg belum disave akan hilang", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    {
                        selectedKompleksID = Convert.ToInt32(displayForm.ReturnValue1);
                        kompleksTextBox.Text = displayForm.ReturnValue2;

                        loadKavling();
                    }
                }
            }
        }

        private void loadStatusCombo()
        {
            cbDataSource cbDataSourceElement;
            cbDT.Clear();

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "Dihuni sendiri";
            cbDataSourceElement.valueMember = "Dihuni sendiri";
            cbDT.Add(cbDataSourceElement);

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "Disewakan";
            cbDataSourceElement.valueMember = "Disewakan";
            cbDT.Add(cbDataSourceElement);

            cbDataSourceElement = new cbDataSource();
            cbDataSourceElement.displayMember = "Kosong";
            cbDataSourceElement.valueMember = "Kosong";
            cbDT.Add(cbDataSourceElement);

            DataGridViewComboBoxColumn cbColumn = (DataGridViewComboBoxColumn)kavlingGridView.Columns["status"];
            cbColumn.DataSource = cbDT;
            cbColumn.DisplayMember = "displayMember";
            cbColumn.ValueMember = "valueMember";
        }

        private void dataUserKavlingForm_Load(object sender, EventArgs e)
        {
            loadStatusCombo();

            kavlingGridView.DoubleBuffered(true);
            userGridView.DoubleBuffered(true);

            kavlingGridView.SelectionChanged += CUSTOM_kavlingGridView_SelectionChanged;
        }

        private bool userIDExist(string userID = "0")
        {
            bool result = false;

            for (int i = 0;i<userGridView.Rows.Count && !result;i++)
            {
                if (userGridView.Rows[i].Cells["userID"].Value.ToString() == userID 
                    && userGridView.Rows[i].Cells["flag"].Value.ToString() == "0")
                    result = true;
            }

            return result;
        }

        private void addUserButton_Click(object sender, EventArgs e)
        {
            if (kavlingGridView.Rows.Count <= 0)
                return;

            int groupType = 0;
            int userID = 0;
            DateTime dtAdd = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            dataBrowse_UserForm displayForm = new dataBrowse_UserForm(globalConstants.ADD_KAVLING_USER);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                userID = Convert.ToInt32(displayForm.ReturnValue1);
                groupType = gUser.RS_getUserGroupType(userID);

                if (groupType == globalConstants.GROUP_RESIDENT)
                {
                    if (gIPL.getNumKavling(userID) > 0)
                    {
                        MessageBox.Show("User sudah diassign ke kavling lain");
                        return;
                    }
                }

                if (!userIDExist(displayForm.ReturnValue1))
                {
                    List<string> currentQR = new List<string>();

                    for (int i = 0;i<userGridView.Rows.Count;i++)
                    {
                        if (userGridView.Rows[i].Cells["lineID"].Value.ToString() == "0")
                            currentQR.Add(userGridView.Rows[i].Cells["qrCodeValue"].Value.ToString());
                    }

                    //string md5QrCode = userID + selectedKavlingID + string.Format(culture, "{0:ddMMyy}", dtAdd);
                    string userQRCode = getUserQRCode(currentQR, 10);

                    userGridView.Rows.Add(
                        0,
                        0,
                        displayForm.ReturnValue1,
                        displayForm.ReturnValue2,
                        displayForm.retUserFullName,
                        dtAdd,
                        string.Format(culture, "{0:dd-MM-yyyy}", dtAdd),
                        dtAdd,
                        string.Format(culture, "{0:dd-MM-yyyy}", dtAdd),
                        "",
                        userQRCode, //gUser.getMD5Value(md5QrCode, 10),
                        "Y",
                        "N"
                        );
                }
            }
        }

        private void CUSTOM_kavlingGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (kavlingGridView.Rows.Count <= 0)
                return;

            DataGridViewRow sRow;
            int rowIdx = -1;

            rowIdx = kavlingGridView.SelectedCells[0].RowIndex;
            sRow = kavlingGridView.Rows[rowIdx];

            selectedKavlingID = Convert.ToInt32(sRow.Cells["kavlingID"].Value);
            loadUser(selectedKavlingID);
        }

        private void kavlingGridView_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (userGridView.Rows.Count > 0 && kavlingGridView.Focused)
            {
                if (DialogResult.No == MessageBox.Show("Pilih blok baru ? Data yg belum disave akan hilang", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    e.Cancel = true;
                    return;
                }
            }
        }

        private void userGridView_DoubleClick(object sender, EventArgs e)
        {
            DataGridViewRow sRow;
            int rowIdx = -1;
        
            if (userGridView.Rows.Count <= 0)
                return;

            rowIdx = userGridView.SelectedCells[0].RowIndex;
            sRow = userGridView.Rows[rowIdx];

            rowUserKavling.userName = sRow.Cells["userName"].Value.ToString();
            rowUserKavling.tglBAST = Convert.ToDateTime(sRow.Cells["bastDTValue"].Value);
            rowUserKavling.startIPL = Convert.ToDateTime(sRow.Cells["iplDtValue"].Value);
            rowUserKavling.remarkValue = sRow.Cells["remark"].Value.ToString();
            rowUserKavling.qrCodeValue= sRow.Cells["qrCodeValue"].Value.ToString();
            rowUserKavling.qrFlagValue = sRow.Cells["qrFlag"].Value.ToString();
            rowUserKavling.bastValue = sRow.Cells["bastFlag"].Value.ToString();

            dataEditUserKavlingForm displayForm = new dataEditUserKavlingForm(rowUserKavling);
            displayForm.ShowDialog(this);

            sRow.Cells["bastDTValue"].Value = rowUserKavling.tglBAST;
            sRow.Cells["tglBast"].Value = string.Format(culture, "{0:dd-MM-yyyy}", rowUserKavling.tglBAST);
            sRow.Cells["iplDtValue"].Value = rowUserKavling.startIPL;
            sRow.Cells["iplDate"].Value = string.Format(culture, "{0:dd-MM-yyyy}", rowUserKavling.startIPL);
            sRow.Cells["remark"].Value = rowUserKavling.remarkValue;
            sRow.Cells["qrFlag"].Value = rowUserKavling.qrFlagValue;
            sRow.Cells["bastFlag"].Value = rowUserKavling.bastValue;
        }

        private bool dataValid()
        {
            errorLabel.Text = "";
            DataGridViewRow sRow;
            int userID = 0;

            for (int i = 0; i < userGridView.Rows.Count; i++)
            {
                sRow = userGridView.Rows[i];

                if (sRow.Cells["flag"].Value.ToString() == "1")
                {
                    userID = Convert.ToInt32(sRow.Cells["userID"].Value);
                    if (gIPL.isOutstandingTagihanExist(userID, selectedKavlingID))
                    {
                        if (DialogResult.Yes == MessageBox.Show("User " + sRow.Cells["userName"].Value.ToString() + 
                                " masih ada tagihan yg belum lunas, Hapus Tagihan?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                        {
                            string errMsg = "";
                            if (!gRest.clearTagihanIPL(gUser.getUserID(), gUser.getUserToken(), userID, selectedKavlingID, out errMsg))
                            {
                                errorLabel.Text = errMsg;
                                return false;
                            }
                        }
                    }
                }

                return true;
            }

            errorLabel.Text = "Tidak ada user untuk disave";
            return false;
        }

        private bool saveTrans()
        {
            bool result = false;

            List<upd_master_kavling> listUpdKavling = new List<upd_master_kavling>();
            upd_master_kavling updKavling;

            List<user_kavling> listUserKavling = new List<user_kavling>();
            user_kavling userKavData;

            List<user_access_list> listUserAccessList = new List<user_access_list>();
            user_access_list userAccessList;


            DataGridViewRow sRow;

            int flagValue = 0;
            string qrFlagValue = "";
            string bastFlagValue = "";
            int lineIDValue = 0;
            string errMsg = "";
            int i = 0;
            int userAccessStatus = 0;
            string userAccessMsg = "";

            try
            {
                for (i = 0; i < kavlingGridView.Rows.Count; i++)
                {
                    sRow = kavlingGridView.Rows[i];

                    updKavling = new upd_master_kavling();
                    updKavling.kavling_id = Convert.ToInt32(sRow.Cells["kavlingID"].Value);
                    updKavling.status = sRow.Cells["status"].Value.ToString();

                    listUpdKavling.Add(updKavling);
                }


                for (i = 0;i<userGridView.Rows.Count;i++)
                {
                    sRow = userGridView.Rows[i];

                    flagValue = Convert.ToInt32(sRow.Cells["flag"].Value);
                    qrFlagValue = sRow.Cells["qrFlag"].Value.ToString();
                    bastFlagValue = sRow.Cells["bastFlag"].Value.ToString();
                    lineIDValue = Convert.ToInt32(sRow.Cells["lineID"].Value);

                    userKavData = new user_kavling();
                    userKavData.id = lineIDValue;
                    userKavData.user_id = Convert.ToInt32(sRow.Cells["userID"].Value);
                    userKavData.kavling_id = selectedKavlingID;
                    userKavData.tgl_bast = Convert.ToDateTime(sRow.Cells["bastDTValue"].Value);
                    userKavData.bast_created = bastFlagValue;
                    userKavData.start_ipl = Convert.ToDateTime(sRow.Cells["iplDtValue"].Value);
                    userKavData.remarks = sRow.Cells["remark"].Value.ToString();
                    userKavData.is_active = (flagValue == 0 ? "Y" : "N");
                    userKavData.user_qr_code = sRow.Cells["qrCodeValue"].Value.ToString();
                    userKavData.user_qr_enabled = qrFlagValue;
                    userKavData.user_full_name = sRow.Cells["userFullName"].Value.ToString();
                    userKavData.user_name = "";
                    listUserKavling.Add(userKavData);
                    

                    if (qrFlagValue == "Y")
                    {
                        userAccessStatus = 1;
                        userAccessMsg = "Welcome....";

                        if (gIPL.isLateOutstandingTagihanExist(Convert.ToInt32(sRow.Cells["userID"].Value), selectedKavlingID))
                        {
                            userAccessStatus = 2;
                            userAccessMsg = "Tagihan IPL Terlambat....";
                        }
                    }
                    else
                    {
                        userAccessStatus = 3;
                        userAccessMsg = "QR Blocked....";
                    }

                    userAccessList = new user_access_list();
                    userAccessList.id = 0;
                    userAccessList.user_id = Convert.ToInt32(sRow.Cells["userID"].Value);
                    userAccessList.kompleks_id = selectedKompleksID;
                    userAccessList.kavling_id = selectedKavlingID;
                    userAccessList.access_id = sRow.Cells["qrCodeValue"].Value.ToString();
                    userAccessList.access_type = "QR";
                    userAccessList.access_status = userAccessStatus;
                    userAccessList.message = userAccessMsg;
                    userAccessList.is_active = (flagValue == 0 ? "Y" : "N");
                    listUserAccessList.Add(userAccessList);
                }

                if (listUserKavling.Count > 0)
                {
                    if (!gRest.saveUserKavling(gUser.getUserID(), gUser.getUserToken(), listUserKavling, listUpdKavling, out errMsg))
                    {
                        throw new Exception(errMsg);
                    }

                    if (!gRest.saveDataUserAccessList(gUser.getUserID(), gUser.getUserToken(), listUserAccessList, out errMsg))
                    {
                        throw new Exception(errMsg);
                    }
                }

                result = true;
            }
            catch(Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private bool allowDelete(DataGridViewRow sRow)
        {
            int userID = 0;
            bool result = true;

            userID = Convert.ToInt32(sRow.Cells["userID"].Value);
            if (gIPL.isOutstandingTagihanExist(userID, selectedKavlingID))
            {
                if (DialogResult.Yes == MessageBox.Show("User " + sRow.Cells["userName"].Value.ToString() +
                                    " masih ada tagihan yg belum lunas, Hapus Tagihan?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    string errMsg = "";
                    if (!gRest.clearTagihanIPL(gUser.getUserID(), gUser.getUserToken(), userID, selectedKavlingID, out errMsg))
                    {
                        errorLabel.Text = errMsg;
                        result = false;
                    }
                    else
                        result = true;
                }
            }

            return result;
        }

        private bool delTrans(int rowNo)
        {
            bool result = false;
            List<user_kavling> listUserKavling = new List<user_kavling>();
            user_kavling userKavData;

            List<upd_master_kavling> updKavling = new List<upd_master_kavling>();

            List<user_access_list> listUserAccessList = new List<user_access_list>();
            user_access_list userAccessList;

            List<user_access_list> listUserAccessListRFID = new List<user_access_list>();

            DataGridViewRow sRow;

            REST_userKavlingRFID rfidData = new REST_userKavlingRFID();

            List<user_kavling_rfid> listData = new List<user_kavling_rfid>();
            user_kavling_rfid hData;

            string qrFlagValue = "";
            int lineIDValue = 0;
            string errMsg = "";
            string bastFlagValue = "";

            string sqlCommand = "";

            try
            {
                sRow = userGridView.Rows[rowNo];
                qrFlagValue = sRow.Cells["qrFlag"].Value.ToString();
                lineIDValue = Convert.ToInt32(sRow.Cells["lineID"].Value);
                bastFlagValue = sRow.Cells["bastFlag"].Value.ToString();

                userKavData = new user_kavling();
                userKavData.id = lineIDValue;
                userKavData.user_id = Convert.ToInt32(sRow.Cells["userID"].Value);
                userKavData.kavling_id = selectedKavlingID;
                userKavData.start_ipl = Convert.ToDateTime(sRow.Cells["iplDtValue"].Value);
                userKavData.remarks = sRow.Cells["remark"].Value.ToString();
                userKavData.is_active = "N";
                userKavData.bast_created = bastFlagValue;
                userKavData.user_qr_code = sRow.Cells["qrCodeValue"].Value.ToString();
                userKavData.user_qr_enabled = qrFlagValue;

                listUserKavling.Add(userKavData);

                userAccessList = new user_access_list();
                userAccessList.id = 0;
                userAccessList.user_id = Convert.ToInt32(sRow.Cells["userID"].Value);
                userAccessList.kompleks_id = selectedKompleksID;
                userAccessList.kavling_id = selectedKavlingID;
                userAccessList.access_id = sRow.Cells["qrCodeValue"].Value.ToString();
                userAccessList.access_type = "QR";
                userAccessList.access_status = (qrFlagValue == "Y" ? 1 : 3);
                userAccessList.message = "Deleted";
                userAccessList.is_active = "N";
                listUserAccessList.Add(userAccessList);

                if (!gRest.saveUserKavling(gUser.getUserID(), gUser.getUserToken(), listUserKavling, updKavling, out errMsg))
                {
                    throw new Exception(errMsg);
                }

                if (!gRest.saveDataUserAccessList(gUser.getUserID(), gUser.getUserToken(), listUserAccessList, out errMsg))
                {
                    throw new Exception(errMsg);
                }

                #region Disable RFID
         
                sqlCommand = "SELECT uk.id, uk.user_kavling_id, uk.rfid_value, uk.rfid_enabled, uk.is_active " +
                                       "FROM user_kavling_rfid uk, user_kavling ul " +
                                       "WHERE uk.user_kavling_id = ul.id " +
                                       "AND ul.kavling_id = " + selectedKavlingID + " " +
                                       "AND ul.user_id = " + Convert.ToInt32(sRow.Cells["userID"].Value) + " " +
                                        "AND uk.is_active = 'Y'";
                if (gRest.getUserKavlingRFIDData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref rfidData))
                {
                    if (rfidData.dataStatus.o_status == 1)
                    {
                        for (int i = 0; i < rfidData.dataList.Count; i++)
                        {
                            hData = new user_kavling_rfid();

                            hData.id = rfidData.dataList[i].id;
                            hData.user_kavling_id = rfidData.dataList[i].user_kavling_id;
                            hData.rfid_enabled = rfidData.dataList[i].rfid_enabled;
                            hData.rfid_value = rfidData.dataList[i].rfid_value;
                            hData.is_active = "N";

                            listData.Add(hData);

                            userAccessList = new user_access_list();
                            userAccessList.id = 0;
                            userAccessList.user_id = Convert.ToInt32(sRow.Cells["userID"].Value);
                            userAccessList.kompleks_id = selectedKompleksID;
                            userAccessList.kavling_id = selectedKavlingID;
                            userAccessList.access_id = rfidData.dataList[i].rfid_value;
                            userAccessList.access_type = "RFID";
                            userAccessList.access_status = (rfidData.dataList[i].rfid_enabled == "Y" ? 1 : 3);
                            userAccessList.message = "Deleted";
                            userAccessList.is_active = "N";
                            listUserAccessListRFID.Add(userAccessList);
                        }

                        int newID = 0;
                        if (listData.Count > 0)
                        {
                            if (!gRest.saveDataUserKavlingRFID(gUser.getUserID(), gUser.getUserToken(), listData,
                                out errMsg, out newID))
                            {
                                throw new Exception(errMsg);
                            }
                        }

                        if (listUserAccessListRFID.Count > 0)
                        {
                            if (!gRest.saveDataUserAccessList(gUser.getUserID(), gUser.getUserToken(), listUserAccessListRFID, out errMsg))
                            {
                                throw new Exception(errMsg);
                            }
                        }           
                    }
                }
                #endregion
                
                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }
            
            return result;
        }

        private bool saveData()
        {
            if (dataValid())
            {
                return saveTrans();
            }

            return false;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (saveData())
            {
                MessageBox.Show("Success");
                loadUser(selectedKavlingID);
            }
            else
            {
                MessageBox.Show("Fail");
            }
        }

        private void userGridView_KeyDown(object sender, KeyEventArgs e)
        {
            DataGridViewRow sRow;
            int rowIdx = -1;

            if (userGridView.Rows.Count <= 0)
                return;

            rowIdx = userGridView.SelectedCells[0].RowIndex;
            sRow = userGridView.Rows[rowIdx];

            if (e.KeyCode == Keys.Delete)
            {
                if (DialogResult.Yes == MessageBox.Show("Hapus user " + sRow.Cells["userName"].Value.ToString() + " dari data kavling ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    if (allowDelete(sRow))
                    {
                        sRow.Cells["flag"].Value = 1;
                        if (!delTrans(rowIdx))
                            sRow.Visible = false;
                        else
                            userGridView.Rows.RemoveAt(rowIdx);
                    }
                }
            }
        }

        private void kavlingGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (kavlingGridView.IsCurrentCellDirty)
            {
                kavlingGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        public string getUserQRCode(List<string> currQR, int maxLength = 10)
        {
            string userQrCode = "";

            try
            {
                while (true)
                {
                    userQrCode = gUser.getRandomString(maxLength);
                    if (currQR.Contains(userQrCode))
                        continue;

                    string sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                            "FROM (" +
                                            "SELECT uk.user_qr_code as qrCode FROM user_kavling uk " +
                                            "WHERE uk.user_qr_code = '" + userQrCode + "' " +
                                            "UNION ALL " +
                                            "SELECT uk.rfid_value as qrCode FROM user_kavling_rfid uk " +
                                            "WHERE uk.rfid_value = '" + userQrCode + "') ";

                    genericReply replyResult = new genericReply();
                    if (gRest.checkDataExist(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
                    {
                        if (Convert.ToInt32(replyResult.data) <= 0)
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return userQrCode;
        }
    }
}
