﻿namespace AlphaSoft
{
    partial class dataSummaryIPLForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dataSummaryIPLForm));
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.displayButton = new System.Windows.Forms.Button();
            this.endDTPicker = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.iplGridView = new System.Windows.Forms.DataGridView();
            this.idTrans = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.issuedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nominal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datePaid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isManualPaid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startDTPicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonView = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.searchBlokButton = new System.Windows.Forms.Button();
            this.namaBlokTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.searchKompleksButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.kompleksTextBox = new System.Windows.Forms.TextBox();
            this.roundingIPL = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iplGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(866, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.roundingIPL);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.buttonView);
            this.panel2.Controls.Add(this.SaveButton);
            this.panel2.Controls.Add(this.searchBlokButton);
            this.panel2.Controls.Add(this.namaBlokTextBox);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.searchKompleksButton);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.kompleksTextBox);
            this.panel2.Location = new System.Drawing.Point(28, 52);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(809, 503);
            this.panel2.TabIndex = 21;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.displayButton);
            this.panel3.Controls.Add(this.endDTPicker);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.iplGridView);
            this.panel3.Controls.Add(this.startDTPicker);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(18, 130);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(761, 362);
            this.panel3.TabIndex = 99;
            // 
            // displayButton
            // 
            this.displayButton.BackColor = System.Drawing.Color.FloralWhite;
            this.displayButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayButton.ForeColor = System.Drawing.Color.Black;
            this.displayButton.Location = new System.Drawing.Point(427, 9);
            this.displayButton.Name = "displayButton";
            this.displayButton.Size = new System.Drawing.Size(133, 37);
            this.displayButton.TabIndex = 98;
            this.displayButton.Text = "Display";
            this.displayButton.UseVisualStyleBackColor = false;
            this.displayButton.Click += new System.EventHandler(this.displayButton_Click);
            // 
            // endDTPicker
            // 
            this.endDTPicker.CustomFormat = "MMM yyyy";
            this.endDTPicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.endDTPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endDTPicker.Location = new System.Drawing.Point(284, 19);
            this.endDTPicker.Name = "endDTPicker";
            this.endDTPicker.Size = new System.Drawing.Size(136, 26);
            this.endDTPicker.TabIndex = 96;
            this.endDTPicker.ValueChanged += new System.EventHandler(this.endDTPicker_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(262, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 18);
            this.label4.TabIndex = 95;
            this.label4.Text = "-";
            // 
            // iplGridView
            // 
            this.iplGridView.AllowUserToAddRows = false;
            this.iplGridView.AllowUserToDeleteRows = false;
            this.iplGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.iplGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.iplGridView.BackgroundColor = System.Drawing.Color.FloralWhite;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.iplGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.iplGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.iplGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idTrans,
            this.issuedDate,
            this.startDate,
            this.endDate,
            this.status,
            this.nominal,
            this.datePaid,
            this.userName,
            this.isManualPaid});
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.iplGridView.DefaultCellStyle = dataGridViewCellStyle12;
            this.iplGridView.Location = new System.Drawing.Point(3, 52);
            this.iplGridView.MultiSelect = false;
            this.iplGridView.Name = "iplGridView";
            this.iplGridView.ReadOnly = true;
            this.iplGridView.RowHeadersVisible = false;
            this.iplGridView.Size = new System.Drawing.Size(751, 303);
            this.iplGridView.TabIndex = 64;
            this.iplGridView.DoubleClick += new System.EventHandler(this.iplGridView_DoubleClick);
            this.iplGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.iplGridView_KeyDown);
            // 
            // idTrans
            // 
            this.idTrans.HeaderText = "idTrans";
            this.idTrans.Name = "idTrans";
            this.idTrans.ReadOnly = true;
            this.idTrans.Visible = false;
            this.idTrans.Width = 72;
            // 
            // issuedDate
            // 
            this.issuedDate.HeaderText = "Issued Date";
            this.issuedDate.Name = "issuedDate";
            this.issuedDate.ReadOnly = true;
            this.issuedDate.Width = 122;
            // 
            // startDate
            // 
            this.startDate.HeaderText = "Start Date";
            this.startDate.Name = "startDate";
            this.startDate.ReadOnly = true;
            this.startDate.Width = 73;
            // 
            // endDate
            // 
            this.endDate.HeaderText = "End Date";
            this.endDate.Name = "endDate";
            this.endDate.ReadOnly = true;
            this.endDate.Width = 99;
            // 
            // status
            // 
            this.status.HeaderText = "Status";
            this.status.Name = "status";
            this.status.ReadOnly = true;
            this.status.Width = 86;
            // 
            // nominal
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.nominal.DefaultCellStyle = dataGridViewCellStyle11;
            this.nominal.HeaderText = "Nominal";
            this.nominal.Name = "nominal";
            this.nominal.ReadOnly = true;
            // 
            // datePaid
            // 
            this.datePaid.HeaderText = "Tgl Pembayaran";
            this.datePaid.Name = "datePaid";
            this.datePaid.ReadOnly = true;
            this.datePaid.Width = 148;
            // 
            // userName
            // 
            this.userName.HeaderText = "User";
            this.userName.Name = "userName";
            this.userName.ReadOnly = true;
            this.userName.Width = 70;
            // 
            // isManualPaid
            // 
            this.isManualPaid.HeaderText = "isManualPaid";
            this.isManualPaid.Name = "isManualPaid";
            this.isManualPaid.ReadOnly = true;
            this.isManualPaid.Visible = false;
            this.isManualPaid.Width = 140;
            // 
            // startDTPicker
            // 
            this.startDTPicker.CustomFormat = "MMM yyyy";
            this.startDTPicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startDTPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startDTPicker.Location = new System.Drawing.Point(124, 19);
            this.startDTPicker.Name = "startDTPicker";
            this.startDTPicker.Size = new System.Drawing.Size(132, 26);
            this.startDTPicker.TabIndex = 94;
            this.startDTPicker.ValueChanged += new System.EventHandler(this.startDTPicker_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(7, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 18);
            this.label1.TabIndex = 65;
            this.label1.Text = "Periode IPL";
            // 
            // buttonView
            // 
            this.buttonView.BackColor = System.Drawing.Color.FloralWhite;
            this.buttonView.Enabled = false;
            this.buttonView.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonView.ForeColor = System.Drawing.Color.Black;
            this.buttonView.Location = new System.Drawing.Point(434, 48);
            this.buttonView.Name = "buttonView";
            this.buttonView.Size = new System.Drawing.Size(182, 37);
            this.buttonView.TabIndex = 97;
            this.buttonView.Text = "View Blok Info";
            this.buttonView.UseVisualStyleBackColor = false;
            this.buttonView.Click += new System.EventHandler(this.buttonView_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.BackColor = System.Drawing.Color.FloralWhite;
            this.SaveButton.Enabled = false;
            this.SaveButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveButton.ForeColor = System.Drawing.Color.Black;
            this.SaveButton.Location = new System.Drawing.Point(140, 87);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(182, 37);
            this.SaveButton.TabIndex = 66;
            this.SaveButton.Text = "Generate IPL";
            this.SaveButton.UseVisualStyleBackColor = false;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // searchBlokButton
            // 
            this.searchBlokButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchBlokButton.BackgroundImage")));
            this.searchBlokButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchBlokButton.Enabled = false;
            this.searchBlokButton.Location = new System.Drawing.Point(399, 52);
            this.searchBlokButton.Name = "searchBlokButton";
            this.searchBlokButton.Size = new System.Drawing.Size(29, 30);
            this.searchBlokButton.TabIndex = 63;
            this.searchBlokButton.UseVisualStyleBackColor = true;
            this.searchBlokButton.Click += new System.EventHandler(this.searchBlokButton_Click);
            // 
            // namaBlokTextBox
            // 
            this.namaBlokTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.namaBlokTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.namaBlokTextBox.Location = new System.Drawing.Point(140, 54);
            this.namaBlokTextBox.MaxLength = 5;
            this.namaBlokTextBox.Name = "namaBlokTextBox";
            this.namaBlokTextBox.ReadOnly = true;
            this.namaBlokTextBox.Size = new System.Drawing.Size(253, 27);
            this.namaBlokTextBox.TabIndex = 62;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(93, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 18);
            this.label3.TabIndex = 61;
            this.label3.Text = "Blok";
            // 
            // searchKompleksButton
            // 
            this.searchKompleksButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchKompleksButton.BackgroundImage")));
            this.searchKompleksButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchKompleksButton.Location = new System.Drawing.Point(447, 13);
            this.searchKompleksButton.Name = "searchKompleksButton";
            this.searchKompleksButton.Size = new System.Drawing.Size(29, 30);
            this.searchKompleksButton.TabIndex = 60;
            this.searchKompleksButton.UseVisualStyleBackColor = true;
            this.searchKompleksButton.Click += new System.EventHandler(this.searchKompleksButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(46, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 18);
            this.label2.TabIndex = 58;
            this.label2.Text = "Kompleks";
            // 
            // kompleksTextBox
            // 
            this.kompleksTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.kompleksTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kompleksTextBox.Location = new System.Drawing.Point(140, 15);
            this.kompleksTextBox.MaxLength = 30;
            this.kompleksTextBox.Name = "kompleksTextBox";
            this.kompleksTextBox.ReadOnly = true;
            this.kompleksTextBox.Size = new System.Drawing.Size(300, 27);
            this.kompleksTextBox.TabIndex = 59;
            // 
            // roundingIPL
            // 
            this.roundingIPL.AutoSize = true;
            this.roundingIPL.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.roundingIPL.Location = new System.Drawing.Point(550, 102);
            this.roundingIPL.Name = "roundingIPL";
            this.roundingIPL.Size = new System.Drawing.Size(229, 22);
            this.roundingIPL.TabIndex = 100;
            this.roundingIPL.Text = "Pembulatan Nominal IPL";
            this.roundingIPL.UseVisualStyleBackColor = true;
            // 
            // dataSummaryIPLForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(866, 590);
            this.Controls.Add(this.panel2);
            this.Name = "dataSummaryIPLForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.dataSummaryIPLForm_FormClosed);
            this.Load += new System.EventHandler(this.dataSummaryIPLForm_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iplGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button searchKompleksButton;
        protected System.Windows.Forms.Label label2;
        protected System.Windows.Forms.TextBox kompleksTextBox;
        private System.Windows.Forms.Button searchBlokButton;
        protected System.Windows.Forms.TextBox namaBlokTextBox;
        protected System.Windows.Forms.Label label3;
        protected System.Windows.Forms.Button SaveButton;
        protected System.Windows.Forms.Button buttonView;
        protected System.Windows.Forms.Button displayButton;
        protected System.Windows.Forms.DateTimePicker endDTPicker;
        protected System.Windows.Forms.Label label4;
        protected System.Windows.Forms.DateTimePicker startDTPicker;
        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.DataGridView iplGridView;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridViewTextBoxColumn idTrans;
        private System.Windows.Forms.DataGridViewTextBoxColumn issuedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn startDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn endDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn status;
        private System.Windows.Forms.DataGridViewTextBoxColumn nominal;
        private System.Windows.Forms.DataGridViewTextBoxColumn datePaid;
        private System.Windows.Forms.DataGridViewTextBoxColumn userName;
        private System.Windows.Forms.DataGridViewTextBoxColumn isManualPaid;
        private System.Windows.Forms.CheckBox roundingIPL;
    }
}
