﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace AlphaSoft
{
    public partial class dataBrowse_customerCareForm : AlphaSoft.browseRangeTextBoxForm
    {
        private int originModuleID = 0;
        private globalUserUtil gUtil;
        private globalRestAPI gRest;

        private CultureInfo culture = new CultureInfo("id-ID");

        private int userID = 0;
        private int kompleksID = 0;
        private int blokID = 0;

        public dataBrowse_customerCareForm(int moduleID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();
        }

        private void dataBrowse_customerCareForm_Load(object sender, EventArgs e)
        {
            displayAllAction();
        }

        protected override void displayAllAction()
        {
            switch (originModuleID)
            {
                default:
                    displayDefault();
                    break;
            }
        }

        private void displayDefault()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string blokWhere = "";

            if (blokID > 0)
                blokWhere  = "AND mb.blok_id = " + blokID + " ";

            REST_custCare dataTrans = new REST_custCare();

            string sqlUser = "SELECT 'user' as sender_type, mu.user_id, mu.user_name, mu.user_full_name, mg.group_id " +
                                "FROM user_login_data mu, master_group mg " +
                                "WHERE mg.group_id = mu.group_id " +
                                "UNION ALL " +
                                "SELECT 'admin' as sender_type, me.employee_id as user_id, me.employee_user_name as user_name, me.employee_full_name as user_full_name, 2 as group_id " +
                                "FROM employee_login_data me";

            sqlCommand = "SELECT cc.ticket_id, cc.ticket_num, cc.ticket_date, cc.request_by, cc.kavling_id, cc.kompleks_id, " +
                                    "cc.subject, cc.ticket_type AS category_name, cc.description, cc.status, cc.is_closed, cc.is_active, " +
                                    "ul.user_name, ul.user_full_name, ml.kompleks_name, " +
                                    "mb.blok_name, mk.house_no " +
                                    "FROM cc_ticket cc " +
                                    "LEFT OUTER JOIN (" + sqlUser  + ")  ul ON (cc.request_by = ul.user_id AND if(cc.sender_type = null, '', convert(cc.sender_type, char)) = ul.sender_type) " +
                                    "LEFT OUTER JOIN master_kavling mk ON (cc.kavling_id = mk.kavling_id AND mk.is_active = 'Y') " +
                                    "LEFT OUTER JOIN master_kompleks ml ON (mk.kompleks_id = ml.kompleks_id AND ml.is_active = 'Y') " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id AND mb.is_active = 'Y' " + blokWhere + ") " +
                                    "WHERE 1 = 1 ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND cc.is_active = 'Y' ";

            if (kompleksID > 0)
                sqlCommand += "AND cc.kompleks_id = " + kompleksID + " ";

            if (originModuleID == globalConstants.MODULE_PENGERJAAN_REPAIR || 
                originModuleID == globalConstants.MODULE_TICKET_REPAIR)
                sqlCommand += "AND cc.ticket_type = 'repair' ";
            else
                sqlCommand += "AND cc.ticket_type = 'cs' ";

            if (originModuleID == globalConstants.MODULE_PENGERJAAN_REPAIR)
            {
                sqlCommand += "AND cc.status in ('paid', 'on_progress') ";
            }
            else if (originModuleID == globalConstants.MODULE_TICKET_REPAIR)
            {
                sqlCommand += "AND cc.status in ('submit', 'user_response', 'on_progress') ";
            }

            if (!closedTicketCheckBox.Checked)
                sqlCommand = sqlCommand + "AND cc.is_closed = 'N' ";

            sqlCommand = sqlCommand + "ORDER BY kompleks_name ASC";

            if (gRest.getCustCareData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref dataTrans))
            {
                if (dataTrans.dataStatus.o_status == 1)
                {
                    dataGridView.DataSource = null;

                    dt.Columns.Add("ticket_id");
                    dt.Columns.Add("KATEGORI");
                    dt.Columns.Add("NO TIKET");
                    dt.Columns.Add("TANGGAL");
                    dt.Columns.Add("USER");
                    dt.Columns.Add("KOMPLEKS");
                    dt.Columns.Add("BLOK");
                    dt.Columns.Add("SUBJECT");
                    dt.Columns.Add("STATUS");
                    dt.Columns.Add("IS_CLOSE");
                    dt.Columns.Add("IS_ACTIVE");

                    for (int i = 0; i < dataTrans.dataList.Count; i++)
                    {
                        DataRow r = dt.Rows.Add();
                        r["ticket_id"] = dataTrans.dataList[i].ticket_id;
                        r["KATEGORI"] = dataTrans.dataList[i].category_name;
                        r["NO TIKET"] = dataTrans.dataList[i].ticket_num;
                        r["USER"] = dataTrans.dataList[i].user_name;
                        r["TANGGAL"] = String.Format(culture, "{0:dd-MM-yyyy}", dataTrans.dataList[i].ticket_date); 
                        r["KOMPLEKS"] = dataTrans.dataList[i].kompleks_name;
                        r["BLOK"] = dataTrans.dataList[i].blok_name + "-" + dataTrans.dataList[i].house_no;
                        r["SUBJECT"] = dataTrans.dataList[i].subject;
                        r["STATUS"] = dataTrans.dataList[i].status;
                        r["IS_CLOSE"] = dataTrans.dataList[i].is_closed;
                        r["IS_ACTIVE"] = dataTrans.dataList[i].is_active;
                    }

                    dataGridView.DataSource = dt;
                    dataGridView.Columns["ticket_id"].Visible = false;
                    dataGridView.Columns["IS_CLOSE"].Visible = false;
                    dataGridView.Columns["IS_ACTIVE"].Visible = false;
                }
            }
        }

        private void searchKompleks_Click(object sender, EventArgs e)
        {
            dataBrowse_KompleksForm displayForm = new dataBrowse_KompleksForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                kompleksID = Convert.ToInt32(displayForm.ReturnValue1);
                namaTextBox.Text = displayForm.ReturnValue2;
            }
        }

        private void searchBlok_Click(object sender, EventArgs e)
        {
            if (kompleksID == 0)
                return;

            dataBrowse_BlokForm displayForm = new dataBrowse_BlokForm(globalConstants.MODULE_DEFAULT, kompleksID);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                blokID = Convert.ToInt32(displayForm.ReturnValue1);
                blokTextBox.Text = displayForm.ReturnValue2;
            }
        }

        protected override void dbGridDoubleClickAction(DataGridViewRow selectedRow)
        {
            int selectedID = Convert.ToInt32(selectedRow.Cells["ticket_id"].Value);

            switch (originModuleID)
            {
                case globalConstants.MODULE_DEFAULT:
                    ReturnValue1 = selectedRow.Cells["ticket_id"].Value.ToString();
                    this.Close();
                    break;

                case globalConstants.MODULE_PENGERJAAN_REPAIR:
                    dataPengerjaanRepair displayPengerjaan = new dataPengerjaanRepair(selectedID);
                    displayPengerjaan.ShowDialog(this);
                    break;

                case globalConstants.MODULE_TICKET_REPAIR:
                    dataCustomerCareForm displayRepair = new dataCustomerCareForm(selectedID);
                    displayRepair.ShowDialog(this);
                    break;

                default:
                    dataTiketCC displayForm = new dataTiketCC(selectedID);
                    displayForm.ShowDialog(this);
                    //dataCustomerCareForm editForm = new dataCustomerCareForm(selectedID);
                    //editForm.ShowDialog(this);
                    break;
            }
        }
    }
}
