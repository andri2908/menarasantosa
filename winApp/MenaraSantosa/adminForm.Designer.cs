﻿namespace AlphaSoft
{
    partial class adminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(adminForm));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.welcomeLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timeStampStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.timerStatusLabel = new System.Windows.Forms.Timer(this.components);
            this.MAINMENU_Strip = new System.Windows.Forms.MenuStrip();
            this.MAINMENU_manajemenSistem = new System.Windows.Forms.ToolStripMenuItem();
            this.logInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_changePassword = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_logOut = new System.Windows.Forms.ToolStripMenuItem();
            this.Separator_1 = new System.Windows.Forms.ToolStripSeparator();
            this.MENU_tambahGroupUser = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_tambahUser = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_pengaturanGroupAkses = new System.Windows.Forms.ToolStripMenuItem();
            this.Separator_2 = new System.Windows.Forms.ToolStripSeparator();
            this.MENU_backUpRestoreDatabase = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_pengaturanSistemAplikasi = new System.Windows.Forms.ToolStripMenuItem();
            this.Separator_3 = new System.Windows.Forms.ToolStripSeparator();
            this.MENU_exit = new System.Windows.Forms.ToolStripMenuItem();
            this.MAINMENU_dataMaster = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_dataKompleks = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_dataKavling = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_pemilikKavling = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_dataItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_dataItemDrugs = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_dataUnit = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_dataNews = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_dataRFID = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_dataAksesSatpam = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_dataAksesStaff = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_dataTerminal = new System.Windows.Forms.ToolStripMenuItem();
            this.MAINMENU_transaksi = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_dataIPL = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_summaryIPL = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_generateIPLKompleks = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_penjualan = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_transaksiRetail = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_transaksiDrugs = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_cetakTransaksiRetail = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_cetakTransaksiDrugs = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_serviceDanReparasi = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_penyesuaianStok = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_penyesuaianStokRetail = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_penyesuaianStokDrugs = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_penerimaanBarang = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_penerimaanBarangRetail = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_penerimaanBarangDrugs = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_customerCare = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_pengerjaanRepair = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU_pembayaranManual = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.MAINMENU_laporan = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.MAINMENU_informasi = new System.Windows.Forms.ToolStripMenuItem();
            this.dEBUGMENUToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.expiredTransToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.BGImageFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.panelMessage = new System.Windows.Forms.Panel();
            this.messageGridView = new System.Windows.Forms.DataGridView();
            this.msgID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.refModule = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.refID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtMessage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.messageContent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.SHORTCUT_ipl = new System.Windows.Forms.ToolStripButton();
            this.SHORTCUT_property = new System.Windows.Forms.ToolStripButton();
            this.SHORTCUT_cc = new System.Windows.Forms.ToolStripButton();
            this.SHORTCUT_miniMarket = new System.Windows.Forms.ToolStripButton();
            this.SHORTCUT_transDrugs = new System.Windows.Forms.ToolStripButton();
            this.timerMessage = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1.SuspendLayout();
            this.MAINMENU_Strip.SuspendLayout();
            this.panelMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.messageGridView)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.Silver;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.welcomeLabel,
            this.toolStripStatusLabel1,
            this.timeStampStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 707);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // welcomeLabel
            // 
            this.welcomeLabel.BackColor = System.Drawing.Color.Silver;
            this.welcomeLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.welcomeLabel.ForeColor = System.Drawing.Color.Black;
            this.welcomeLabel.Name = "welcomeLabel";
            this.welcomeLabel.Size = new System.Drawing.Size(76, 17);
            this.welcomeLabel.Text = "WELCOME";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.Silver;
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.Black;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(696, 17);
            this.toolStripStatusLabel1.Spring = true;
            // 
            // timeStampStatusLabel
            // 
            this.timeStampStatusLabel.BackColor = System.Drawing.Color.Silver;
            this.timeStampStatusLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeStampStatusLabel.ForeColor = System.Drawing.Color.Black;
            this.timeStampStatusLabel.Name = "timeStampStatusLabel";
            this.timeStampStatusLabel.Size = new System.Drawing.Size(221, 17);
            this.timeStampStatusLabel.Text = "Kamis, 21 Januari 2016 - 22:47";
            // 
            // timerStatusLabel
            // 
            this.timerStatusLabel.Interval = 60000;
            this.timerStatusLabel.Tick += new System.EventHandler(this.timerStatusLabel_Tick);
            // 
            // MAINMENU_Strip
            // 
            this.MAINMENU_Strip.BackColor = System.Drawing.Color.Silver;
            this.MAINMENU_Strip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MAINMENU_manajemenSistem,
            this.MAINMENU_dataMaster,
            this.MAINMENU_transaksi,
            this.MAINMENU_laporan,
            this.MAINMENU_informasi,
            this.dEBUGMENUToolStripMenuItem});
            this.MAINMENU_Strip.Location = new System.Drawing.Point(0, 0);
            this.MAINMENU_Strip.Name = "MAINMENU_Strip";
            this.MAINMENU_Strip.Size = new System.Drawing.Size(1008, 24);
            this.MAINMENU_Strip.TabIndex = 5;
            this.MAINMENU_Strip.Text = "menuStrip1";
            // 
            // MAINMENU_manajemenSistem
            // 
            this.MAINMENU_manajemenSistem.BackColor = System.Drawing.Color.Silver;
            this.MAINMENU_manajemenSistem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logInToolStripMenuItem,
            this.MENU_changePassword,
            this.MENU_logOut,
            this.Separator_1,
            this.MENU_tambahGroupUser,
            this.MENU_tambahUser,
            this.MENU_pengaturanGroupAkses,
            this.Separator_2,
            this.MENU_backUpRestoreDatabase,
            this.MENU_pengaturanSistemAplikasi,
            this.Separator_3,
            this.MENU_exit});
            this.MAINMENU_manajemenSistem.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MAINMENU_manajemenSistem.ForeColor = System.Drawing.Color.Black;
            this.MAINMENU_manajemenSistem.Name = "MAINMENU_manajemenSistem";
            this.MAINMENU_manajemenSistem.Size = new System.Drawing.Size(159, 20);
            this.MAINMENU_manajemenSistem.Text = "Manajemen Sistem";
            this.MAINMENU_manajemenSistem.DropDownClosed += new System.EventHandler(this.genericMenuDropDownClosed);
            this.MAINMENU_manajemenSistem.DropDownOpened += new System.EventHandler(this.genericMenuDropDownOpen);
            // 
            // logInToolStripMenuItem
            // 
            this.logInToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.logInToolStripMenuItem.Enabled = false;
            this.logInToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.logInToolStripMenuItem.Name = "logInToolStripMenuItem";
            this.logInToolStripMenuItem.Size = new System.Drawing.Size(343, 22);
            this.logInToolStripMenuItem.Text = "Log-&In";
            // 
            // MENU_changePassword
            // 
            this.MENU_changePassword.BackColor = System.Drawing.Color.Silver;
            this.MENU_changePassword.ForeColor = System.Drawing.Color.Black;
            this.MENU_changePassword.Name = "MENU_changePassword";
            this.MENU_changePassword.Size = new System.Drawing.Size(343, 22);
            this.MENU_changePassword.Text = "&Change Password";
            this.MENU_changePassword.Click += new System.EventHandler(this.MENU_changePassword_Click);
            // 
            // MENU_logOut
            // 
            this.MENU_logOut.BackColor = System.Drawing.Color.Silver;
            this.MENU_logOut.ForeColor = System.Drawing.Color.Black;
            this.MENU_logOut.Name = "MENU_logOut";
            this.MENU_logOut.Size = new System.Drawing.Size(343, 22);
            this.MENU_logOut.Text = "Log-&Out";
            this.MENU_logOut.Click += new System.EventHandler(this.MENU_logOut_Click);
            // 
            // Separator_1
            // 
            this.Separator_1.BackColor = System.Drawing.Color.SteelBlue;
            this.Separator_1.ForeColor = System.Drawing.Color.FloralWhite;
            this.Separator_1.Name = "Separator_1";
            this.Separator_1.Size = new System.Drawing.Size(340, 6);
            // 
            // MENU_tambahGroupUser
            // 
            this.MENU_tambahGroupUser.BackColor = System.Drawing.Color.Silver;
            this.MENU_tambahGroupUser.ForeColor = System.Drawing.Color.Black;
            this.MENU_tambahGroupUser.Name = "MENU_tambahGroupUser";
            this.MENU_tambahGroupUser.Size = new System.Drawing.Size(343, 22);
            this.MENU_tambahGroupUser.Text = "Pengaturan Group User";
            this.MENU_tambahGroupUser.Click += new System.EventHandler(this.MENU_tambahGroupUser_Click);
            // 
            // MENU_tambahUser
            // 
            this.MENU_tambahUser.BackColor = System.Drawing.Color.Silver;
            this.MENU_tambahUser.ForeColor = System.Drawing.Color.Black;
            this.MENU_tambahUser.Name = "MENU_tambahUser";
            this.MENU_tambahUser.Size = new System.Drawing.Size(343, 22);
            this.MENU_tambahUser.Text = "Pengaturan User";
            this.MENU_tambahUser.Click += new System.EventHandler(this.MENU_tambahUser_Click);
            // 
            // MENU_pengaturanGroupAkses
            // 
            this.MENU_pengaturanGroupAkses.BackColor = System.Drawing.Color.Silver;
            this.MENU_pengaturanGroupAkses.ForeColor = System.Drawing.Color.Black;
            this.MENU_pengaturanGroupAkses.Name = "MENU_pengaturanGroupAkses";
            this.MENU_pengaturanGroupAkses.Size = new System.Drawing.Size(343, 22);
            this.MENU_pengaturanGroupAkses.Text = "Pengaturan Group Akses";
            this.MENU_pengaturanGroupAkses.Click += new System.EventHandler(this.MENU_pengaturanGroupAkses_Click);
            // 
            // Separator_2
            // 
            this.Separator_2.Name = "Separator_2";
            this.Separator_2.Size = new System.Drawing.Size(340, 6);
            // 
            // MENU_backUpRestoreDatabase
            // 
            this.MENU_backUpRestoreDatabase.BackColor = System.Drawing.Color.Silver;
            this.MENU_backUpRestoreDatabase.ForeColor = System.Drawing.Color.Black;
            this.MENU_backUpRestoreDatabase.Name = "MENU_backUpRestoreDatabase";
            this.MENU_backUpRestoreDatabase.Size = new System.Drawing.Size(343, 22);
            this.MENU_backUpRestoreDatabase.Text = "Backup/ Restore Database [HIDDEN]";
            this.MENU_backUpRestoreDatabase.Visible = false;
            this.MENU_backUpRestoreDatabase.Click += new System.EventHandler(this.MENU_backUpRestoreDatabaseToolStripMenuItem_Click);
            // 
            // MENU_pengaturanSistemAplikasi
            // 
            this.MENU_pengaturanSistemAplikasi.BackColor = System.Drawing.Color.Silver;
            this.MENU_pengaturanSistemAplikasi.ForeColor = System.Drawing.Color.Black;
            this.MENU_pengaturanSistemAplikasi.Name = "MENU_pengaturanSistemAplikasi";
            this.MENU_pengaturanSistemAplikasi.Size = new System.Drawing.Size(343, 22);
            this.MENU_pengaturanSistemAplikasi.Text = "Pengaturan Sistem Aplikasi";
            this.MENU_pengaturanSistemAplikasi.Click += new System.EventHandler(this.MENU_pengaturanSistemAplikasiToolStripMenuItem_Click);
            // 
            // Separator_3
            // 
            this.Separator_3.Name = "Separator_3";
            this.Separator_3.Size = new System.Drawing.Size(340, 6);
            // 
            // MENU_exit
            // 
            this.MENU_exit.BackColor = System.Drawing.Color.Silver;
            this.MENU_exit.ForeColor = System.Drawing.Color.Black;
            this.MENU_exit.Name = "MENU_exit";
            this.MENU_exit.Size = new System.Drawing.Size(343, 22);
            this.MENU_exit.Text = "E&xit";
            this.MENU_exit.Click += new System.EventHandler(this.MENU_exit_Click);
            // 
            // MAINMENU_dataMaster
            // 
            this.MAINMENU_dataMaster.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MENU_dataKompleks,
            this.MENU_dataKavling,
            this.MENU_pemilikKavling,
            this.MENU_dataItem,
            this.MENU_dataItemDrugs,
            this.MENU_dataUnit,
            this.MENU_dataNews,
            this.MENU_dataRFID,
            this.MENU_dataAksesSatpam,
            this.MENU_dataAksesStaff,
            this.MENU_dataTerminal});
            this.MAINMENU_dataMaster.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MAINMENU_dataMaster.ForeColor = System.Drawing.Color.Black;
            this.MAINMENU_dataMaster.Name = "MAINMENU_dataMaster";
            this.MAINMENU_dataMaster.Size = new System.Drawing.Size(108, 20);
            this.MAINMENU_dataMaster.Text = "Data Master";
            this.MAINMENU_dataMaster.DropDownClosed += new System.EventHandler(this.genericMenuDropDownClosed);
            this.MAINMENU_dataMaster.DropDownOpened += new System.EventHandler(this.genericMenuDropDownOpen);
            // 
            // MENU_dataKompleks
            // 
            this.MENU_dataKompleks.BackColor = System.Drawing.Color.Silver;
            this.MENU_dataKompleks.ForeColor = System.Drawing.Color.Black;
            this.MENU_dataKompleks.Name = "MENU_dataKompleks";
            this.MENU_dataKompleks.Size = new System.Drawing.Size(289, 22);
            this.MENU_dataKompleks.Text = "Data Kompleks";
            this.MENU_dataKompleks.Click += new System.EventHandler(this.MENU_dataKompleks_Click);
            // 
            // MENU_dataKavling
            // 
            this.MENU_dataKavling.BackColor = System.Drawing.Color.Silver;
            this.MENU_dataKavling.ForeColor = System.Drawing.Color.Black;
            this.MENU_dataKavling.Name = "MENU_dataKavling";
            this.MENU_dataKavling.Size = new System.Drawing.Size(289, 22);
            this.MENU_dataKavling.Text = "Data Kavling";
            this.MENU_dataKavling.Click += new System.EventHandler(this.MENU_dataKavling_Click);
            // 
            // MENU_pemilikKavling
            // 
            this.MENU_pemilikKavling.BackColor = System.Drawing.Color.Silver;
            this.MENU_pemilikKavling.ForeColor = System.Drawing.Color.Black;
            this.MENU_pemilikKavling.Name = "MENU_pemilikKavling";
            this.MENU_pemilikKavling.Size = new System.Drawing.Size(289, 22);
            this.MENU_pemilikKavling.Text = "Data Pemilik Kavling";
            this.MENU_pemilikKavling.Click += new System.EventHandler(this.MENU_pemilikKavling_Click);
            // 
            // MENU_dataItem
            // 
            this.MENU_dataItem.BackColor = System.Drawing.Color.Silver;
            this.MENU_dataItem.ForeColor = System.Drawing.Color.Black;
            this.MENU_dataItem.Name = "MENU_dataItem";
            this.MENU_dataItem.Size = new System.Drawing.Size(289, 22);
            this.MENU_dataItem.Text = "Data Item Retail";
            this.MENU_dataItem.Click += new System.EventHandler(this.MENU_dataItem_Click);
            // 
            // MENU_dataItemDrugs
            // 
            this.MENU_dataItemDrugs.BackColor = System.Drawing.Color.Silver;
            this.MENU_dataItemDrugs.ForeColor = System.Drawing.Color.Black;
            this.MENU_dataItemDrugs.Name = "MENU_dataItemDrugs";
            this.MENU_dataItemDrugs.Size = new System.Drawing.Size(289, 22);
            this.MENU_dataItemDrugs.Text = "Data Item Drugs";
            this.MENU_dataItemDrugs.Click += new System.EventHandler(this.MENU_dataItemDrugs_Click);
            // 
            // MENU_dataUnit
            // 
            this.MENU_dataUnit.BackColor = System.Drawing.Color.Silver;
            this.MENU_dataUnit.ForeColor = System.Drawing.Color.Black;
            this.MENU_dataUnit.Name = "MENU_dataUnit";
            this.MENU_dataUnit.Size = new System.Drawing.Size(289, 22);
            this.MENU_dataUnit.Text = "Data Satuan Unit";
            this.MENU_dataUnit.Click += new System.EventHandler(this.MENU_dataUnit_Click);
            // 
            // MENU_dataNews
            // 
            this.MENU_dataNews.BackColor = System.Drawing.Color.Silver;
            this.MENU_dataNews.ForeColor = System.Drawing.Color.Black;
            this.MENU_dataNews.Name = "MENU_dataNews";
            this.MENU_dataNews.Size = new System.Drawing.Size(289, 22);
            this.MENU_dataNews.Text = "Data News";
            this.MENU_dataNews.Click += new System.EventHandler(this.MENU_dataNews_Click);
            // 
            // MENU_dataRFID
            // 
            this.MENU_dataRFID.BackColor = System.Drawing.Color.Silver;
            this.MENU_dataRFID.ForeColor = System.Drawing.Color.Black;
            this.MENU_dataRFID.Name = "MENU_dataRFID";
            this.MENU_dataRFID.Size = new System.Drawing.Size(289, 22);
            this.MENU_dataRFID.Text = "Data RFID";
            this.MENU_dataRFID.Click += new System.EventHandler(this.MENU_dataRFID_Click);
            // 
            // MENU_dataAksesSatpam
            // 
            this.MENU_dataAksesSatpam.BackColor = System.Drawing.Color.Silver;
            this.MENU_dataAksesSatpam.ForeColor = System.Drawing.Color.Black;
            this.MENU_dataAksesSatpam.Name = "MENU_dataAksesSatpam";
            this.MENU_dataAksesSatpam.Size = new System.Drawing.Size(289, 22);
            this.MENU_dataAksesSatpam.Text = "Data Akses Satpam [HIDDEN]";
            this.MENU_dataAksesSatpam.Visible = false;
            this.MENU_dataAksesSatpam.Click += new System.EventHandler(this.MENU_dataAksesSatpam_Click);
            // 
            // MENU_dataAksesStaff
            // 
            this.MENU_dataAksesStaff.BackColor = System.Drawing.Color.Silver;
            this.MENU_dataAksesStaff.ForeColor = System.Drawing.Color.Black;
            this.MENU_dataAksesStaff.Name = "MENU_dataAksesStaff";
            this.MENU_dataAksesStaff.Size = new System.Drawing.Size(289, 22);
            this.MENU_dataAksesStaff.Text = "Data Akses Staff";
            this.MENU_dataAksesStaff.Click += new System.EventHandler(this.MENU_dataAksesStaff_Click);
            // 
            // MENU_dataTerminal
            // 
            this.MENU_dataTerminal.BackColor = System.Drawing.Color.Silver;
            this.MENU_dataTerminal.ForeColor = System.Drawing.Color.Black;
            this.MENU_dataTerminal.Name = "MENU_dataTerminal";
            this.MENU_dataTerminal.Size = new System.Drawing.Size(289, 22);
            this.MENU_dataTerminal.Text = "Data Terminal";
            this.MENU_dataTerminal.Click += new System.EventHandler(this.MENU_dataTerminal_Click);
            // 
            // MAINMENU_transaksi
            // 
            this.MAINMENU_transaksi.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MENU_dataIPL,
            this.MENU_penjualan,
            this.MENU_penyesuaianStok,
            this.MENU_penerimaanBarang,
            this.MENU_customerCare,
            this.MENU_pengerjaanRepair,
            this.MENU_pembayaranManual});
            this.MAINMENU_transaksi.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MAINMENU_transaksi.ForeColor = System.Drawing.Color.Black;
            this.MAINMENU_transaksi.Name = "MAINMENU_transaksi";
            this.MAINMENU_transaksi.Size = new System.Drawing.Size(127, 20);
            this.MAINMENU_transaksi.Text = "Data Transaksi";
            this.MAINMENU_transaksi.DropDownClosed += new System.EventHandler(this.genericMenuDropDownClosed);
            this.MAINMENU_transaksi.DropDownOpened += new System.EventHandler(this.genericMenuDropDownOpen);
            // 
            // MENU_dataIPL
            // 
            this.MENU_dataIPL.BackColor = System.Drawing.Color.Silver;
            this.MENU_dataIPL.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MENU_summaryIPL,
            this.MENU_generateIPLKompleks});
            this.MENU_dataIPL.ForeColor = System.Drawing.Color.Black;
            this.MENU_dataIPL.Name = "MENU_dataIPL";
            this.MENU_dataIPL.Size = new System.Drawing.Size(275, 22);
            this.MENU_dataIPL.Text = "Data IPL";
            // 
            // MENU_summaryIPL
            // 
            this.MENU_summaryIPL.BackColor = System.Drawing.Color.Silver;
            this.MENU_summaryIPL.ForeColor = System.Drawing.Color.Black;
            this.MENU_summaryIPL.Name = "MENU_summaryIPL";
            this.MENU_summaryIPL.Size = new System.Drawing.Size(244, 22);
            this.MENU_summaryIPL.Text = "Summary IPL";
            this.MENU_summaryIPL.Click += new System.EventHandler(this.MENU_summaryIPL_Click);
            // 
            // MENU_generateIPLKompleks
            // 
            this.MENU_generateIPLKompleks.BackColor = System.Drawing.Color.Silver;
            this.MENU_generateIPLKompleks.ForeColor = System.Drawing.Color.Black;
            this.MENU_generateIPLKompleks.Name = "MENU_generateIPLKompleks";
            this.MENU_generateIPLKompleks.Size = new System.Drawing.Size(244, 22);
            this.MENU_generateIPLKompleks.Text = "Generate IPL Kompleks";
            this.MENU_generateIPLKompleks.Click += new System.EventHandler(this.MENU_generateIPLKompleks_Click);
            // 
            // MENU_penjualan
            // 
            this.MENU_penjualan.BackColor = System.Drawing.Color.Silver;
            this.MENU_penjualan.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MENU_transaksiRetail,
            this.MENU_transaksiDrugs,
            this.MENU_cetakTransaksiRetail,
            this.MENU_cetakTransaksiDrugs,
            this.MENU_serviceDanReparasi});
            this.MENU_penjualan.ForeColor = System.Drawing.Color.Black;
            this.MENU_penjualan.Name = "MENU_penjualan";
            this.MENU_penjualan.Size = new System.Drawing.Size(275, 22);
            this.MENU_penjualan.Text = "Data Penjualan dan Service";
            // 
            // MENU_transaksiRetail
            // 
            this.MENU_transaksiRetail.BackColor = System.Drawing.Color.Silver;
            this.MENU_transaksiRetail.ForeColor = System.Drawing.Color.Black;
            this.MENU_transaksiRetail.Name = "MENU_transaksiRetail";
            this.MENU_transaksiRetail.Size = new System.Drawing.Size(300, 22);
            this.MENU_transaksiRetail.Text = "Transaksi Retail";
            this.MENU_transaksiRetail.Click += new System.EventHandler(this.MENU_transaksiRetail_Click);
            // 
            // MENU_transaksiDrugs
            // 
            this.MENU_transaksiDrugs.BackColor = System.Drawing.Color.Silver;
            this.MENU_transaksiDrugs.ForeColor = System.Drawing.Color.Black;
            this.MENU_transaksiDrugs.Name = "MENU_transaksiDrugs";
            this.MENU_transaksiDrugs.Size = new System.Drawing.Size(300, 22);
            this.MENU_transaksiDrugs.Text = "Transaksi Drugs";
            this.MENU_transaksiDrugs.Click += new System.EventHandler(this.MENU_transaksiDrugs_Click);
            // 
            // MENU_cetakTransaksiRetail
            // 
            this.MENU_cetakTransaksiRetail.BackColor = System.Drawing.Color.Silver;
            this.MENU_cetakTransaksiRetail.ForeColor = System.Drawing.Color.Black;
            this.MENU_cetakTransaksiRetail.Name = "MENU_cetakTransaksiRetail";
            this.MENU_cetakTransaksiRetail.Size = new System.Drawing.Size(300, 22);
            this.MENU_cetakTransaksiRetail.Text = "Cetak Transaksi Retail";
            this.MENU_cetakTransaksiRetail.Click += new System.EventHandler(this.MENU_cetakTransaksi_Click);
            // 
            // MENU_cetakTransaksiDrugs
            // 
            this.MENU_cetakTransaksiDrugs.BackColor = System.Drawing.Color.Silver;
            this.MENU_cetakTransaksiDrugs.ForeColor = System.Drawing.Color.Black;
            this.MENU_cetakTransaksiDrugs.Name = "MENU_cetakTransaksiDrugs";
            this.MENU_cetakTransaksiDrugs.Size = new System.Drawing.Size(300, 22);
            this.MENU_cetakTransaksiDrugs.Text = "Cetak Transaksi Drugs";
            this.MENU_cetakTransaksiDrugs.Click += new System.EventHandler(this.MENU_cetakTransaksiDrugs_Click);
            // 
            // MENU_serviceDanReparasi
            // 
            this.MENU_serviceDanReparasi.BackColor = System.Drawing.Color.Silver;
            this.MENU_serviceDanReparasi.ForeColor = System.Drawing.Color.Black;
            this.MENU_serviceDanReparasi.Name = "MENU_serviceDanReparasi";
            this.MENU_serviceDanReparasi.Size = new System.Drawing.Size(300, 22);
            this.MENU_serviceDanReparasi.Text = "Service dan Reparasi [HIDDEN]";
            this.MENU_serviceDanReparasi.Visible = false;
            this.MENU_serviceDanReparasi.Click += new System.EventHandler(this.MENU_serviceDanReparasi_Click);
            // 
            // MENU_penyesuaianStok
            // 
            this.MENU_penyesuaianStok.BackColor = System.Drawing.Color.Silver;
            this.MENU_penyesuaianStok.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MENU_penyesuaianStokRetail,
            this.MENU_penyesuaianStokDrugs});
            this.MENU_penyesuaianStok.ForeColor = System.Drawing.Color.Black;
            this.MENU_penyesuaianStok.Name = "MENU_penyesuaianStok";
            this.MENU_penyesuaianStok.Size = new System.Drawing.Size(275, 22);
            this.MENU_penyesuaianStok.Text = "Penyesuaian Stok";
            // 
            // MENU_penyesuaianStokRetail
            // 
            this.MENU_penyesuaianStokRetail.BackColor = System.Drawing.Color.Silver;
            this.MENU_penyesuaianStokRetail.ForeColor = System.Drawing.Color.Black;
            this.MENU_penyesuaianStokRetail.Name = "MENU_penyesuaianStokRetail";
            this.MENU_penyesuaianStokRetail.Size = new System.Drawing.Size(251, 22);
            this.MENU_penyesuaianStokRetail.Text = "Penyesuaian Stok Retail";
            this.MENU_penyesuaianStokRetail.Click += new System.EventHandler(this.MENU_penyesuaianStokRetail_Click);
            // 
            // MENU_penyesuaianStokDrugs
            // 
            this.MENU_penyesuaianStokDrugs.BackColor = System.Drawing.Color.Silver;
            this.MENU_penyesuaianStokDrugs.ForeColor = System.Drawing.Color.Black;
            this.MENU_penyesuaianStokDrugs.Name = "MENU_penyesuaianStokDrugs";
            this.MENU_penyesuaianStokDrugs.Size = new System.Drawing.Size(251, 22);
            this.MENU_penyesuaianStokDrugs.Text = "Penyesuaian Stok Drugs";
            this.MENU_penyesuaianStokDrugs.Click += new System.EventHandler(this.MENU_penyesuaianStokDrugs_Click);
            // 
            // MENU_penerimaanBarang
            // 
            this.MENU_penerimaanBarang.BackColor = System.Drawing.Color.Silver;
            this.MENU_penerimaanBarang.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MENU_penerimaanBarangRetail,
            this.MENU_penerimaanBarangDrugs});
            this.MENU_penerimaanBarang.ForeColor = System.Drawing.Color.Black;
            this.MENU_penerimaanBarang.Name = "MENU_penerimaanBarang";
            this.MENU_penerimaanBarang.Size = new System.Drawing.Size(275, 22);
            this.MENU_penerimaanBarang.Text = "Penerimaan Barang";
            // 
            // MENU_penerimaanBarangRetail
            // 
            this.MENU_penerimaanBarangRetail.BackColor = System.Drawing.Color.Silver;
            this.MENU_penerimaanBarangRetail.ForeColor = System.Drawing.Color.Black;
            this.MENU_penerimaanBarangRetail.Name = "MENU_penerimaanBarangRetail";
            this.MENU_penerimaanBarangRetail.Size = new System.Drawing.Size(265, 22);
            this.MENU_penerimaanBarangRetail.Text = "Penerimaan Barang Retail";
            this.MENU_penerimaanBarangRetail.Click += new System.EventHandler(this.MENU_penerimaanBarangRetail_Click);
            // 
            // MENU_penerimaanBarangDrugs
            // 
            this.MENU_penerimaanBarangDrugs.BackColor = System.Drawing.Color.Silver;
            this.MENU_penerimaanBarangDrugs.ForeColor = System.Drawing.Color.Black;
            this.MENU_penerimaanBarangDrugs.Name = "MENU_penerimaanBarangDrugs";
            this.MENU_penerimaanBarangDrugs.Size = new System.Drawing.Size(265, 22);
            this.MENU_penerimaanBarangDrugs.Text = "Penerimaan Barang Drugs";
            this.MENU_penerimaanBarangDrugs.Click += new System.EventHandler(this.MENU_penerimaanBarangDrugs_Click);
            // 
            // MENU_customerCare
            // 
            this.MENU_customerCare.BackColor = System.Drawing.Color.Silver;
            this.MENU_customerCare.ForeColor = System.Drawing.Color.Black;
            this.MENU_customerCare.Name = "MENU_customerCare";
            this.MENU_customerCare.Size = new System.Drawing.Size(275, 22);
            this.MENU_customerCare.Text = "Customer Care";
            this.MENU_customerCare.Click += new System.EventHandler(this.MENU_customerCare_Click);
            // 
            // MENU_pengerjaanRepair
            // 
            this.MENU_pengerjaanRepair.BackColor = System.Drawing.Color.Silver;
            this.MENU_pengerjaanRepair.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
            this.MENU_pengerjaanRepair.ForeColor = System.Drawing.Color.Black;
            this.MENU_pengerjaanRepair.Name = "MENU_pengerjaanRepair";
            this.MENU_pengerjaanRepair.Size = new System.Drawing.Size(275, 22);
            this.MENU_pengerjaanRepair.Text = "Repair";
            this.MENU_pengerjaanRepair.Click += new System.EventHandler(this.MENU_pengerjaanRepair_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.BackColor = System.Drawing.Color.Silver;
            this.toolStripMenuItem1.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(251, 22);
            this.toolStripMenuItem1.Text = "Update Tiket Repair";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.BackColor = System.Drawing.Color.Silver;
            this.toolStripMenuItem2.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(251, 22);
            this.toolStripMenuItem2.Text = "Mulai Pengerjaan Repair";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // MENU_pembayaranManual
            // 
            this.MENU_pembayaranManual.BackColor = System.Drawing.Color.Silver;
            this.MENU_pembayaranManual.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6});
            this.MENU_pembayaranManual.ForeColor = System.Drawing.Color.Black;
            this.MENU_pembayaranManual.Name = "MENU_pembayaranManual";
            this.MENU_pembayaranManual.Size = new System.Drawing.Size(275, 22);
            this.MENU_pembayaranManual.Text = "Pembayaran Transaksi";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.BackColor = System.Drawing.Color.Silver;
            this.toolStripMenuItem4.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(221, 22);
            this.toolStripMenuItem4.Text = "Pembayaran IPL";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.BackColor = System.Drawing.Color.Silver;
            this.toolStripMenuItem5.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(221, 22);
            this.toolStripMenuItem5.Text = "Pembayaran Repair";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.BackColor = System.Drawing.Color.Silver;
            this.toolStripMenuItem6.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(221, 22);
            this.toolStripMenuItem6.Text = "Cancel Pembayaran";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // MAINMENU_laporan
            // 
            this.MAINMENU_laporan.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem8,
            this.toolStripMenuItem7,
            this.toolStripMenuItem9,
            this.toolStripMenuItem11,
            this.toolStripMenuItem12});
            this.MAINMENU_laporan.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MAINMENU_laporan.ForeColor = System.Drawing.Color.Black;
            this.MAINMENU_laporan.Name = "MAINMENU_laporan";
            this.MAINMENU_laporan.Size = new System.Drawing.Size(79, 20);
            this.MAINMENU_laporan.Text = "Laporan";
            this.MAINMENU_laporan.DropDownClosed += new System.EventHandler(this.genericMenuDropDownClosed);
            this.MAINMENU_laporan.DropDownOpened += new System.EventHandler(this.genericMenuDropDownOpen);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.BackColor = System.Drawing.Color.Silver;
            this.toolStripMenuItem8.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(319, 22);
            this.toolStripMenuItem8.Text = "Laporan IPL Kompleks";
            this.toolStripMenuItem8.Click += new System.EventHandler(this.toolStripMenuItem8_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.BackColor = System.Drawing.Color.Silver;
            this.toolStripMenuItem7.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(319, 22);
            this.toolStripMenuItem7.Text = "Laporan Pendapatan IPL Tahunan";
            this.toolStripMenuItem7.Click += new System.EventHandler(this.toolStripMenuItem7_Click);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.BackColor = System.Drawing.Color.Silver;
            this.toolStripMenuItem9.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(319, 22);
            this.toolStripMenuItem9.Text = "Laporan Penagihan IPL Manual";
            this.toolStripMenuItem9.Click += new System.EventHandler(this.toolStripMenuItem9_Click);
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.BackColor = System.Drawing.Color.Silver;
            this.toolStripMenuItem11.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(319, 22);
            this.toolStripMenuItem11.Text = "Laporan Akses Kompleks";
            this.toolStripMenuItem11.Click += new System.EventHandler(this.toolStripMenuItem11_Click);
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.BackColor = System.Drawing.Color.Silver;
            this.toolStripMenuItem12.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(319, 22);
            this.toolStripMenuItem12.Text = "Laporan Kavling";
            this.toolStripMenuItem12.Click += new System.EventHandler(this.toolStripMenuItem12_Click);
            // 
            // MAINMENU_informasi
            // 
            this.MAINMENU_informasi.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MAINMENU_informasi.ForeColor = System.Drawing.Color.Black;
            this.MAINMENU_informasi.Name = "MAINMENU_informasi";
            this.MAINMENU_informasi.Size = new System.Drawing.Size(90, 20);
            this.MAINMENU_informasi.Text = "Informasi";
            this.MAINMENU_informasi.DropDownClosed += new System.EventHandler(this.genericMenuDropDownClosed);
            this.MAINMENU_informasi.DropDownOpened += new System.EventHandler(this.genericMenuDropDownOpen);
            this.MAINMENU_informasi.Click += new System.EventHandler(this.informasiToolStripMenuItem_Click);
            // 
            // dEBUGMENUToolStripMenuItem
            // 
            this.dEBUGMENUToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.expiredTransToolStripMenuItem,
            this.toolStripMenuItem3,
            this.toolStripMenuItem10});
            this.dEBUGMENUToolStripMenuItem.Name = "dEBUGMENUToolStripMenuItem";
            this.dEBUGMENUToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.dEBUGMENUToolStripMenuItem.Text = "DEBUG MENU";
            this.dEBUGMENUToolStripMenuItem.Visible = false;
            // 
            // expiredTransToolStripMenuItem
            // 
            this.expiredTransToolStripMenuItem.Name = "expiredTransToolStripMenuItem";
            this.expiredTransToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.expiredTransToolStripMenuItem.Text = "Expired Trans";
            this.expiredTransToolStripMenuItem.Click += new System.EventHandler(this.expiredTransToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(168, 22);
            this.toolStripMenuItem3.Text = "Check User Status";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(168, 22);
            this.toolStripMenuItem10.Text = "Save Message";
            this.toolStripMenuItem10.Click += new System.EventHandler(this.toolStripMenuItem10_Click);
            // 
            // BGImageFileDialog
            // 
            this.BGImageFileDialog.FileName = "openFileDialog1";
            // 
            // panelMessage
            // 
            this.panelMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelMessage.Controls.Add(this.messageGridView);
            this.panelMessage.Location = new System.Drawing.Point(0, 100);
            this.panelMessage.Name = "panelMessage";
            this.panelMessage.Size = new System.Drawing.Size(1008, 604);
            this.panelMessage.TabIndex = 6;
            // 
            // messageGridView
            // 
            this.messageGridView.AllowUserToAddRows = false;
            this.messageGridView.AllowUserToDeleteRows = false;
            this.messageGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.messageGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.messageGridView.BackgroundColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.messageGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.messageGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.messageGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.msgID,
            this.refModule,
            this.refID,
            this.dtMessage,
            this.messageContent});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.messageGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.messageGridView.Location = new System.Drawing.Point(0, 11);
            this.messageGridView.MultiSelect = false;
            this.messageGridView.Name = "messageGridView";
            this.messageGridView.ReadOnly = true;
            this.messageGridView.RowHeadersVisible = false;
            this.messageGridView.Size = new System.Drawing.Size(1008, 593);
            this.messageGridView.TabIndex = 0;
            this.messageGridView.DoubleClick += new System.EventHandler(this.messageGridView_DoubleClick);
            // 
            // msgID
            // 
            this.msgID.HeaderText = "msgID";
            this.msgID.Name = "msgID";
            this.msgID.ReadOnly = true;
            this.msgID.Visible = false;
            this.msgID.Width = 53;
            // 
            // refModule
            // 
            this.refModule.HeaderText = "refModule";
            this.refModule.Name = "refModule";
            this.refModule.ReadOnly = true;
            this.refModule.Visible = false;
            this.refModule.Width = 74;
            // 
            // refID
            // 
            this.refID.HeaderText = "ID";
            this.refID.Name = "refID";
            this.refID.ReadOnly = true;
            this.refID.Visible = false;
            this.refID.Width = 27;
            // 
            // dtMessage
            // 
            this.dtMessage.HeaderText = "TANGGAL";
            this.dtMessage.Name = "dtMessage";
            this.dtMessage.ReadOnly = true;
            this.dtMessage.Width = 97;
            // 
            // messageContent
            // 
            this.messageContent.HeaderText = "MESSAGE";
            this.messageContent.Name = "messageContent";
            this.messageContent.ReadOnly = true;
            this.messageContent.Width = 99;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.SlateGray;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(60, 60);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SHORTCUT_ipl,
            this.SHORTCUT_property,
            this.SHORTCUT_cc,
            this.SHORTCUT_miniMarket,
            this.SHORTCUT_transDrugs});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1008, 84);
            this.toolStrip1.TabIndex = 7;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // SHORTCUT_ipl
            // 
            this.SHORTCUT_ipl.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SHORTCUT_ipl.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SHORTCUT_ipl.Image = ((System.Drawing.Image)(resources.GetObject("SHORTCUT_ipl.Image")));
            this.SHORTCUT_ipl.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SHORTCUT_ipl.Margin = new System.Windows.Forms.Padding(0);
            this.SHORTCUT_ipl.Name = "SHORTCUT_ipl";
            this.SHORTCUT_ipl.Padding = new System.Windows.Forms.Padding(10);
            this.SHORTCUT_ipl.Size = new System.Drawing.Size(84, 84);
            this.SHORTCUT_ipl.Text = "IPL";
            this.SHORTCUT_ipl.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.SHORTCUT_ipl.Click += new System.EventHandler(this.SHORTCUT_ipl_Click);
            // 
            // SHORTCUT_property
            // 
            this.SHORTCUT_property.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SHORTCUT_property.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SHORTCUT_property.Image = ((System.Drawing.Image)(resources.GetObject("SHORTCUT_property.Image")));
            this.SHORTCUT_property.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SHORTCUT_property.Margin = new System.Windows.Forms.Padding(5);
            this.SHORTCUT_property.Name = "SHORTCUT_property";
            this.SHORTCUT_property.Padding = new System.Windows.Forms.Padding(5);
            this.SHORTCUT_property.Size = new System.Drawing.Size(74, 74);
            this.SHORTCUT_property.Text = "Property";
            this.SHORTCUT_property.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.SHORTCUT_property.Click += new System.EventHandler(this.SHORTCUT_property_Click);
            // 
            // SHORTCUT_cc
            // 
            this.SHORTCUT_cc.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SHORTCUT_cc.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SHORTCUT_cc.Image = ((System.Drawing.Image)(resources.GetObject("SHORTCUT_cc.Image")));
            this.SHORTCUT_cc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SHORTCUT_cc.Margin = new System.Windows.Forms.Padding(5);
            this.SHORTCUT_cc.Name = "SHORTCUT_cc";
            this.SHORTCUT_cc.Padding = new System.Windows.Forms.Padding(5);
            this.SHORTCUT_cc.Size = new System.Drawing.Size(74, 74);
            this.SHORTCUT_cc.Tag = "";
            this.SHORTCUT_cc.Text = "Customer Care";
            this.SHORTCUT_cc.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.SHORTCUT_cc.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // SHORTCUT_miniMarket
            // 
            this.SHORTCUT_miniMarket.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SHORTCUT_miniMarket.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SHORTCUT_miniMarket.Image = ((System.Drawing.Image)(resources.GetObject("SHORTCUT_miniMarket.Image")));
            this.SHORTCUT_miniMarket.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SHORTCUT_miniMarket.Margin = new System.Windows.Forms.Padding(5);
            this.SHORTCUT_miniMarket.Name = "SHORTCUT_miniMarket";
            this.SHORTCUT_miniMarket.Padding = new System.Windows.Forms.Padding(5);
            this.SHORTCUT_miniMarket.Size = new System.Drawing.Size(74, 74);
            this.SHORTCUT_miniMarket.Text = "Transaksi Mini Market";
            this.SHORTCUT_miniMarket.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.SHORTCUT_miniMarket.Click += new System.EventHandler(this.SHORTCUT_miniMarket_Click);
            // 
            // SHORTCUT_transDrugs
            // 
            this.SHORTCUT_transDrugs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SHORTCUT_transDrugs.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SHORTCUT_transDrugs.Image = ((System.Drawing.Image)(resources.GetObject("SHORTCUT_transDrugs.Image")));
            this.SHORTCUT_transDrugs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SHORTCUT_transDrugs.Margin = new System.Windows.Forms.Padding(5);
            this.SHORTCUT_transDrugs.Name = "SHORTCUT_transDrugs";
            this.SHORTCUT_transDrugs.Padding = new System.Windows.Forms.Padding(5);
            this.SHORTCUT_transDrugs.Size = new System.Drawing.Size(74, 74);
            this.SHORTCUT_transDrugs.Text = "Transaksi Drugs";
            this.SHORTCUT_transDrugs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.SHORTCUT_transDrugs.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // timerMessage
            // 
            this.timerMessage.Interval = 300000;
            this.timerMessage.Tick += new System.EventHandler(this.timerMessage_Tick);
            // 
            // adminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.panelMessage);
            this.Controls.Add(this.MAINMENU_Strip);
            this.Controls.Add(this.statusStrip1);
            this.DoubleBuffered = true;
            this.Name = "adminForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ADMINISTRATOR MODULE";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.adminForm_Activated);
            this.Deactivate += new System.EventHandler(this.adminForm_Deactivate);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.adminForm_FormClosed);
            this.Load += new System.EventHandler(this.adminForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.MAINMENU_Strip.ResumeLayout(false);
            this.MAINMENU_Strip.PerformLayout();
            this.panelMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.messageGridView)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel welcomeLabel;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel timeStampStatusLabel;
        private System.Windows.Forms.Timer timerStatusLabel;
        private System.Windows.Forms.MenuStrip MAINMENU_Strip;
        private System.Windows.Forms.ToolStripMenuItem MAINMENU_manajemenSistem;
        private System.Windows.Forms.ToolStripMenuItem logInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MENU_changePassword;
        private System.Windows.Forms.ToolStripMenuItem MENU_logOut;
        private System.Windows.Forms.ToolStripSeparator Separator_1;
        private System.Windows.Forms.ToolStripSeparator Separator_2;
        private System.Windows.Forms.ToolStripMenuItem MENU_backUpRestoreDatabase;
        private System.Windows.Forms.ToolStripMenuItem MENU_pengaturanSistemAplikasi;
        private System.Windows.Forms.ToolStripSeparator Separator_3;
        private System.Windows.Forms.ToolStripMenuItem MENU_exit;
        private System.Windows.Forms.ToolStripMenuItem MAINMENU_informasi;
        private System.Windows.Forms.OpenFileDialog BGImageFileDialog;
        private System.Windows.Forms.ToolStripMenuItem MENU_tambahGroupUser;
        private System.Windows.Forms.ToolStripMenuItem MENU_tambahUser;
        private System.Windows.Forms.ToolStripMenuItem MENU_pengaturanGroupAkses;
        private System.Windows.Forms.Panel panelMessage;
        private System.Windows.Forms.DataGridView messageGridView;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.DataGridViewTextBoxColumn msgID;
        private System.Windows.Forms.DataGridViewTextBoxColumn refModule;
        private System.Windows.Forms.DataGridViewTextBoxColumn refID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn messageContent;
        private System.Windows.Forms.ToolStripMenuItem MAINMENU_dataMaster;
        private System.Windows.Forms.ToolStripMenuItem MENU_dataKompleks;
        private System.Windows.Forms.ToolStripMenuItem MENU_dataKavling;
        private System.Windows.Forms.ToolStripMenuItem MENU_pemilikKavling;
        private System.Windows.Forms.ToolStripMenuItem MENU_dataItem;
        private System.Windows.Forms.ToolStripMenuItem MENU_dataUnit;
        private System.Windows.Forms.ToolStripMenuItem MAINMENU_transaksi;
        private System.Windows.Forms.ToolStripMenuItem MENU_dataIPL;
        private System.Windows.Forms.ToolStripMenuItem MENU_penjualan;
        private System.Windows.Forms.ToolStripMenuItem MENU_summaryIPL;
        private System.Windows.Forms.ToolStripMenuItem MENU_transaksiRetail;
        private System.Windows.Forms.ToolStripMenuItem MENU_serviceDanReparasi;
        private System.Windows.Forms.ToolStripMenuItem MENU_penyesuaianStok;
        private System.Windows.Forms.ToolStripMenuItem MENU_penerimaanBarang;
        private System.Windows.Forms.ToolStripMenuItem MENU_customerCare;
        private System.Windows.Forms.ToolStripMenuItem MENU_transaksiDrugs;
        private System.Windows.Forms.ToolStripMenuItem MENU_penyesuaianStokRetail;
        private System.Windows.Forms.ToolStripMenuItem MENU_penyesuaianStokDrugs;
        private System.Windows.Forms.ToolStripMenuItem MENU_penerimaanBarangRetail;
        private System.Windows.Forms.ToolStripMenuItem MENU_penerimaanBarangDrugs;
        private System.Windows.Forms.ToolStripMenuItem MENU_dataItemDrugs;
        private System.Windows.Forms.ToolStripMenuItem dEBUGMENUToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem expiredTransToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton SHORTCUT_cc;
        private System.Windows.Forms.ToolStripButton SHORTCUT_transDrugs;
        private System.Windows.Forms.ToolStripButton SHORTCUT_ipl;
        private System.Windows.Forms.ToolStripButton SHORTCUT_property;
        private System.Windows.Forms.ToolStripButton SHORTCUT_miniMarket;
        private System.Windows.Forms.ToolStripMenuItem MENU_dataNews;
        private System.Windows.Forms.ToolStripMenuItem MENU_pengerjaanRepair;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem MENU_cetakTransaksiRetail;
        private System.Windows.Forms.ToolStripMenuItem MENU_cetakTransaksiDrugs;
        private System.Windows.Forms.ToolStripMenuItem MENU_generateIPLKompleks;
        private System.Windows.Forms.ToolStripMenuItem MENU_dataRFID;
        private System.Windows.Forms.ToolStripMenuItem MENU_dataAksesSatpam;
        private System.Windows.Forms.ToolStripMenuItem MENU_dataTerminal;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem MENU_pembayaranManual;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem MENU_dataAksesStaff;
        private System.Windows.Forms.ToolStripMenuItem MAINMENU_laporan;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.Timer timerMessage;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
    }
}