﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace AlphaSoft
{
    public partial class dataSummaryIPLForm : AlphaSoft.basicHotkeysForm
    {
        private int originModuleID = 0;
        private int selectedKavlingID = 0;
        private int selectedKompleksID = 0;
        private int selectedUserID = 0;

        private globalUserUtil gUser;
        private globalIPL gIPL;
        private globalRestAPI gRest;

        private CultureInfo culture = new CultureInfo("id-ID");

        public dataSummaryIPLForm()
        {
            InitializeComponent();

            gUser = new globalUserUtil();
            gIPL = new globalIPL();
            gRest = new globalRestAPI();
        }

        private void dataSummaryIPLForm_Load(object sender, EventArgs e)
        {
            startDTPicker.Value = DateTime.Now.AddYears(-1);
        }

        private void searchKompleksButton_Click(object sender, EventArgs e)
        {
            dataBrowse_KompleksForm displayForm = new dataBrowse_KompleksForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                selectedKompleksID = Convert.ToInt32(displayForm.ReturnValue1);
                kompleksTextBox.Text = displayForm.ReturnValue2;

                searchBlokButton.Enabled = true;
            }
        }

        private void loadIPLGridView()
        {
            string sqlCommand = "";
            DataTable dt = new DataTable();
            string dateFrom = "";
            string dateEnd = "";

            REST_dataIPL iplData = new REST_dataIPL();
            dateFrom = String.Format(culture, "{0:yyyyMM}", Convert.ToDateTime(startDTPicker.Value.Date));
            dateEnd = String.Format(culture, "{0:yyyyMM}", Convert.ToDateTime(endDTPicker.Value.Date));

            sqlCommand = "SELECT ih.id_trans, ih.date_issued, ih.start_ipl, ih.end_ipl, ih.status_id, ih.nominal, ih.is_manual_paid, ih.payment_id, " +
                                    "IFNULL(ih.date_paid, ih.date_issued) AS date_paid, " +
                                    "IFNULL(mu.user_name, '') AS user_name FROM " +
                                    "transaksi_ipl ih " +
                                    "LEFT OUTER JOIN user_login_data mu ON (ih.user_id = mu.user_id) " +
                                    "WHERE ih.is_active = 'Y' " +
                                    "AND ih.kavling_id = " + selectedKavlingID + " " +
                                    "AND DATE_FORMAT(ih.start_ipl, '%Y%m') >= '" + dateFrom + "' " +
                                    "AND DATE_FORMAT(ih.end_ipl, '%Y%m') <= '" + dateEnd + "'";

            if (gRest.getIPLData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref iplData))
            {
                if (iplData.dataStatus.o_status == 1)
                {
                    iplGridView.Rows.Clear();
                    for (int i = 0;i<iplData.dataList.Count;i++)
                    {
                        iplGridView.Rows.Add(
                            iplData.dataList[i].id_trans,
                            string.Format(culture, "{0:dd MMM yyyy}", iplData.dataList[i].date_issued),
                            string.Format(culture, "{0:dd MMM yyyy}", iplData.dataList[i].start_ipl),
                            string.Format(culture, "{0:dd MMM yyyy}", iplData.dataList[i].end_ipl),
                            (iplData.dataList[i].status_id == 2 ? "LUNAS" : (iplData.dataList[i].status_id == 1 ? "PENDING"  : "BELUM LUNAS")),
                            Convert.ToDouble(iplData.dataList[i].nominal).ToString("N0", culture),
                            (iplData.dataList[i].status_id == 2 ? string.Format(culture, "{0:dd MMM yyyy}", iplData.dataList[i].date_paid) : ""),
                            iplData.dataList[i].user_name,
                            iplData.dataList[i].is_manual_paid
                            );
                    }
                }
            }           
        }

        private void searchBlokButton_Click(object sender, EventArgs e)
        {
            dataBrowse_KavlingForm displayForm = new dataBrowse_KavlingForm(globalConstants.SUMMARY_IPL, selectedKompleksID);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                selectedKavlingID = Convert.ToInt32(displayForm.ReturnValue1);
                namaBlokTextBox.Text = displayForm.ReturnValue2;

                if (gIPL.userAssignedExist(selectedKavlingID))
                {
                    SaveButton.Enabled = true;
                    buttonView.Enabled = true;
                    loadIPLGridView();
                }
                else
                {
                    SaveButton.Enabled = false;
                    buttonView.Enabled = false;
                    iplGridView.Rows.Clear();
                }
            }
        }

        private void dataSummaryIPLForm_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void startDTPicker_ValueChanged(object sender, EventArgs e)
        {
            //loadIPLGridView();
        }

        private void endDTPicker_ValueChanged(object sender, EventArgs e)
        {
            //loadIPLGridView();
        }

        private void generateOutstandingIPL()
        {
            string errMsg = "";
            if (!gIPL.outstandingIPLExist(selectedKavlingID))
            {
                MessageBox.Show("Tidak ada outstanding IPL");
                return;
            }

            // PILIH USER YG MENDAPAT  TAGIHAN IPL
            dataBrowse_UserForm displayForm = new dataBrowse_UserForm(globalConstants.SUMMARY_IPL, selectedKavlingID);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                if (gIPL.generateOutstandingIPL(selectedKavlingID, Convert.ToInt32(displayForm.ReturnValue1), roundingIPL.Checked,
                    out errMsg))
                {
                    errorLabel.Text = "";
                    MessageBox.Show("Success");
                }
                else
                {
                    errorLabel.Text = errMsg;
                    MessageBox.Show("Fail");
                }
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            searchKompleksButton.Enabled = false;
            searchBlokButton.Enabled = false;
            SaveButton.Enabled = false;
            roundingIPL.Enabled = false;
            iplGridView.Enabled = false;
            displayButton.Enabled = false;

            generateOutstandingIPL();
            displayButton.PerformClick();

            searchKompleksButton.Enabled = true;

            if (selectedKompleksID > 0)
                searchBlokButton.Enabled = true;

            SaveButton.Enabled = true;
            roundingIPL.Enabled = true;
            iplGridView.Enabled = true;
            displayButton.Enabled = true;
        }

        private void buttonView_Click(object sender, EventArgs e)
        {
            dataKavlingDetailForm displayForm = new dataKavlingDetailForm(selectedKavlingID, globalConstants.VIEW_ONLY);
            displayForm.ShowDialog(this);
        }

        private void printTransaksiIPL(int idTrans)
        {
            List<sqlFields> listSql = new List<sqlFields>();
            sqlFields sqlElement;

            sqlElement = new sqlFields();
            sqlElement.fieldName = "ID_TRANS";
            sqlElement.fieldValue = idTrans.ToString();
            listSql.Add(sqlElement);

            RPT_containerForm displayReportForm = new RPT_containerForm(null, globalReportConstants.PRINTOUT_IPL, "", listSql);
            displayReportForm.ShowDialog(this);
        }

        private void iplGridView_DoubleClick(object sender, EventArgs e)
        {
            if (iplGridView.Rows.Count > 0)
            {
                int selectedrowindex = iplGridView.SelectedCells[0].RowIndex;
                DataGridViewRow sRow = iplGridView.Rows[selectedrowindex];

                printTransaksiIPL(Convert.ToInt32(sRow.Cells["idTrans"].Value));
            }
        }

        private void displayButton_Click(object sender, EventArgs e)
        {
            if (selectedKavlingID > 0)
                loadIPLGridView();
        }

        private bool delTrans(DataGridViewRow sRow, out string errMsg)
        {
            bool result = false;

            result = gIPL.deleteIPL(Convert.ToInt32(sRow.Cells["idTrans"].Value),
                out errMsg);

            return result;
        }

        private bool allowDelete(DataGridViewRow sRow)
        {
            if (sRow.Cells["status"].Value.ToString() == "PENDING")
            {
                return false;
            }

            if (sRow.Cells["status"].Value.ToString() == "LUNAS" &&
                sRow.Cells["isManualPaid"].Value.ToString() == "N")
            {
                return false;
            }

            return true;
        }

        private void iplGridView_KeyDown(object sender, KeyEventArgs e)
        {
            DataGridViewRow sRow;
            int rowIdx = -1;
            string errMsg = "";

            if (iplGridView.Rows.Count <= 0)
                return;

            rowIdx = iplGridView.SelectedCells[0].RowIndex;
            sRow = iplGridView.Rows[rowIdx];

            if (e.KeyCode == Keys.Delete)
            {
                if (allowDelete(sRow))
                {
                    if (DialogResult.Yes == MessageBox.Show("Hapus tagihan IPL " + sRow.Cells["startDate"].Value.ToString() + " - " + sRow.Cells["endDate"].Value.ToString() + " ? ", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    {
                        if (delTrans(sRow, out errMsg))
                            iplGridView.Rows.RemoveAt(rowIdx);
                        else
                            errorLabel.Text = errMsg;
                    }
                }
            }
        }
    }
}
