﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Linq;

namespace AlphaSoft
{
    public partial class dataBrowse_ItemForm : AlphaSoft.simpleBrowseForm
    {
        private int originModuleID = 0;
        private globalUserUtil gUtil;
        private globalRestAPI gRest;

        private int[] arrRetail =
        {
            globalConstants.MODULE_ITEM_RETAIL,
            globalConstants.MODULE_PENERIMAAN_BARANG,
            globalConstants.MODULE_PENYESUAIAN_STOK,
            globalConstants.MODULE_TRANSAKSI_RETAIL
        };
        
        private int[] arrDrugs =
        {
            globalConstants.MODULE_ITEM_DRUGS,
            globalConstants.MODULE_PENERIMAAN_DRUGS,
            globalConstants.MODULE_PENYESUAIAN_DRUGS,
            globalConstants.MODULE_TRANSAKSI_DRUGS
        };

        public dataBrowse_ItemForm(int moduleID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();

            ReturnValue1 = "0";
            ReturnValue2 = "0";
        }

        private void dataBrowse_ItemForm_Load(object sender, EventArgs e)
        {
            int accessModuleID = MMConstants.TAMBAH_HAPUS_ITEM;
            if (arrDrugs.Contains(originModuleID))
            {
                accessModuleID = MMConstants.TAMBAH_HAPUS_ITEM_DRUGS;
            }

            displayAllAction();

            if (!gUtil.RS_userHasAccessTo(accessModuleID,
                MMConstants.HAK_NEW_DATA))
                setNewButtonEnable(false);
        }

        protected override void displayAllAction()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string paramName = "";

            REST_masterItem itemData = new REST_masterItem();

            paramName = getNamaTextBox();

            sqlCommand = "SELECT item_id, item_name, item_price, qty " +
                        "FROM master_item " +
                        "WHERE 1 = 1 ";

            if (paramName.Length > 0)
                sqlCommand += "AND item_name LIKE '%" + paramName + "%' ";

            if (!nonActiveIncluded())
                sqlCommand += "AND is_active = 'Y' ";

            if (originModuleID == globalConstants.MODULE_PENERIMAAN_BARANG ||
                originModuleID == globalConstants.MODULE_PENERIMAAN_DRUGS ||
                originModuleID == globalConstants.MODULE_PENYESUAIAN_STOK ||
                originModuleID == globalConstants.MODULE_PENYESUAIAN_DRUGS)
            {
                sqlCommand += "AND item_jasa = 'N' ";
            }

            if (arrRetail.Contains(originModuleID))
            {
                sqlCommand += "AND item_category_id = " + globalConstants.ITEM_RETAIL + " ";
            }
            else if (arrDrugs.Contains(originModuleID))
            {
                sqlCommand += "AND item_category_id = " + globalConstants.ITEM_DRUGS + " ";
            }

            sqlCommand += "ORDER BY item_name ASC";

            if (gRest.getMasterItemData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref itemData))
            {
                if (itemData.dataStatus.o_status == 1)
                {
                    dataGridView.DataSource = null;

                    dt.Columns.Add("ITEM_ID");
                    dt.Columns.Add("ITEM");
                    dt.Columns.Add("HARGA", typeof(double));
                    dt.Columns.Add("QTY", typeof(double));

                    for (int i = 0; i < itemData.dataList.Count; i++)
                    {
                        DataRow r = dt.Rows.Add();
                        r["ITEM_ID"] = itemData.dataList[i].item_id;
                        r["ITEM"] = itemData.dataList[i].item_name;
                        r["HARGA"] = itemData.dataList[i].item_price;
                        r["QTY"] = itemData.dataList[i].qty;
                    }

                    dataGridView.DataSource = dt;
                    dataGridView.Columns["ITEM_ID"].Visible = false;

                    dataGridView.Columns["HARGA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridView.Columns["HARGA"].DefaultCellStyle.Format = "N0";
                    dataGridView.Columns["HARGA"].DefaultCellStyle.FormatProvider = new CultureInfo("id-ID");

                    dataGridView.Columns["QTY"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridView.Columns["QTY"].DefaultCellStyle.Format = "N0";
                    dataGridView.Columns["QTY"].DefaultCellStyle.FormatProvider = new CultureInfo("id-ID");
                }
            }
        }

        protected override void dbGridDoubleClickAction(DataGridViewRow selectedRow)
        {
            int selectedID = Convert.ToInt32(selectedRow.Cells["ITEM_ID"].Value);

            switch (originModuleID)
            {
                case globalConstants.MODULE_DEFAULT:
                case globalConstants.MODULE_TRANSAKSI_RETAIL:
                case globalConstants.MODULE_TRANSAKSI_DRUGS:
                case globalConstants.MODULE_PENERIMAAN_BARANG:
                case globalConstants.MODULE_PENERIMAAN_DRUGS:
                    ReturnValue1 = selectedRow.Cells["ITEM_ID"].Value.ToString();
                    ReturnValue2 = selectedRow.Cells["ITEM"].Value.ToString();
                    this.Close();
                    break;

                case globalConstants.MODULE_PENYESUAIAN_STOK:
                case globalConstants.MODULE_PENYESUAIAN_DRUGS:
                    dataPenyesuaianStokForm adjustStok = new dataPenyesuaianStokForm(selectedID);
                    adjustStok.ShowDialog(this);
                    break;

                default:
                    dataItemDetailForm editForm = new dataItemDetailForm(selectedID, originModuleID);
                    editForm.ShowDialog(this);
                    break;
            }
        }

        protected override void newButtonAction()
        {
            dataItemDetailForm displayForm = new dataItemDetailForm(0, originModuleID);
            displayForm.ShowDialog(this);
        }
    }
}
