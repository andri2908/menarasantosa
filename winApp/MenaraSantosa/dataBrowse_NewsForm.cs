﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Linq;

namespace AlphaSoft
{
    public partial class dataBrowse_NewsForm : AlphaSoft.simpleBrowseForm
    {
        private int originModuleID = 0;
        private globalUserUtil gUtil;
        private globalRestAPI gRest;

        public dataBrowse_NewsForm(int moduleID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();

            ReturnValue1 = "0";
            ReturnValue2 = "0";
        }

        private void dataBrowse_NewsForm_Load(object sender, EventArgs e)
        {
            int accessModuleID = MMConstants.TAMBAH_HAPUS_NEWS;

            displayAllAction();

            if (!gUtil.RS_userHasAccessTo(accessModuleID,
                MMConstants.HAK_NEW_DATA))
                setNewButtonEnable(false);
        }

        protected override void displayAllAction()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string paramName = "";

            REST_news newsData = new REST_news();

            paramName = getNamaTextBox();

            sqlCommand = "SELECT news_id, news_type, subject, is_banner " +
                        "FROM news " +
                        "WHERE 1 = 1 ";

            if (paramName.Length > 0)
                sqlCommand += "AND subject LIKE '%" + paramName + "%' ";

            if (!nonActiveIncluded())
                sqlCommand += "AND is_active = 'Y' ";

            sqlCommand += "ORDER BY subject ASC";

            if (gRest.getNewsData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref newsData))
            {
                if (newsData.dataStatus.o_status == 1)
                {
                    dataGridView.DataSource = null;

                    dt.Columns.Add("news_id");
                    dt.Columns.Add("TIPE");
                    dt.Columns.Add("SUBJECT");
                    dt.Columns.Add("BANNER");

                    for (int i = 0; i < newsData.dataList.Count; i++)
                    {
                        DataRow r = dt.Rows.Add();
                        r["news_id"] = newsData.dataList[i].news_id;
                        r["TIPE"] = newsData.dataList[i].news_type;
                        r["SUBJECT"] = newsData.dataList[i].subject;
                        r["BANNER"] = newsData.dataList[i].is_banner;
                    }

                    dataGridView.DataSource = dt;
                    dataGridView.Columns["news_id"].Visible = false;
                }
            }
        }

        protected override void dbGridDoubleClickAction(DataGridViewRow selectedRow)
        {
            int selectedID = Convert.ToInt32(selectedRow.Cells["news_id"].Value);

            switch (originModuleID)
            {
                case globalConstants.MODULE_DEFAULT:
                    ReturnValue1 = selectedRow.Cells["news_id"].Value.ToString();
                    ReturnValue2 = selectedRow.Cells["SUBJECT"].Value.ToString();
                    this.Close();
                    break;

                default:
                    dataNewsForm editForm = new dataNewsForm(selectedID);
                    editForm.ShowDialog(this);
                    break;
            }
        }

        protected override void newButtonAction()
        {
            dataNewsForm displayForm = new dataNewsForm();
            displayForm.ShowDialog(this);
        }
    }
}
