﻿namespace AlphaSoft
{
    partial class dataBrowse_UserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // displayAllButton
            // 
            this.displayAllButton.BackColor = System.Drawing.Color.FloralWhite;
            this.displayAllButton.ForeColor = System.Drawing.Color.Black;
            this.displayAllButton.Location = new System.Drawing.Point(240, 115);
            // 
            // newButton
            // 
            this.newButton.BackColor = System.Drawing.Color.FloralWhite;
            this.newButton.ForeColor = System.Drawing.Color.Black;
            this.newButton.Location = new System.Drawing.Point(63, 115);
            // 
            // dataBrowse_UserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(513, 549);
            this.Name = "dataBrowse_UserForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.dataUserForm_FormClosed);
            this.Load += new System.EventHandler(this.dataUserForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}
