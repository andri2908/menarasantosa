ALTER TABLE `u1311857_sys_residential`.`user_login_data` 
CHANGE COLUMN `user_password` `user_password` VARCHAR(50) NULL DEFAULT '' ;

ALTER TABLE `u1311857_sys_residential`.`user_kavling` 
ADD COLUMN `user_qr_code` VARCHAR(50) NULL DEFAULT '' AFTER `remarks`;

ALTER TABLE `u1311857_sys_residential`.`news` 
ADD is_banner ENUM('Y','N') DEFAULT 'N' AFTER file_name;