<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Validator;
use Intervention\image\Facades\Image;
use Carbon\Carbon;

date_default_timezone_set('Asia/Jakarta');

class winAPP_Controller extends Controller
{
  
    public function testDate(){
        $current = Carbon::now();
        echo $current->format('Y-m-d H:i:s') . "\n";
        echo "<br>";
        $current->addHour(12);
        echo $current->format('Y-m-d H:i:s') . "\n";
    }

    public function checkToken($userID, $accessToken){
        try
        {
            $query = "  
            select  count(1) as num
            from    user_token ut
            where   ut.user_id = ".$userID." 
                    and ut.access_token = '". $accessToken ."'";

            $result = \DB::select(\DB::raw($query));

            return $result[0]->num;
        }
        catch(Exception $e) 
        { 
            return 0;
        }
    }

    public function saveLastLogin(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_loginType            = $request->get('p_loginType');
            $fieldToUpdate          = "last_login";     

            if ($p_loginType == 2)
                $fieldToUpdate = "last_logout";

            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $rows = \DB::table('user_login_data')
                ->where('user_id', $p_user_id)
                ->update([$fieldToUpdate => new \DateTime()]);
                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }
    
    public function getDataSingleValue(Request $request){
       
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
            'sqlQuery'              => 'required',
        ]);

        if($validation->fails())
        {
            $errors = $validation->errors();
            return $errors->toJson();
        }

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
    
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $query   = $request->get('sqlQuery');

                $result = \DB::select(\DB::raw($query));
    
                $result = response()->json([[
                            'data' => $result[0]->resultQuery, 
                            'o_status' => 1,
                            'o_message' => "success",        
                            ]], 200);       
                }
        } 
        catch (Exception $e) 
        {
            $result = response()->json([[
                'data' => 0,
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function getDataSingleValueNoParam(Request $request){
       
        $validation = Validator::make($request->all(),[ 
            'sqlQuery'              => 'required',
        ]);

        if($validation->fails())
        {
            $errors = $validation->errors();
            return $errors->toJson();
        }

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $query   = $request->get('sqlQuery');

            $result = \DB::select(\DB::raw($query));

            $result = response()->json([[
                        'data' => $result[0]->resultQuery, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);       
        } 
        catch (Exception $e) 
        {
            $result = response()->json([[
                'data' => 0,
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function getData(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
            'sqlQuery'              => 'required',
        ]);

        if($validation->fails())
        {
            $errors = $validation->errors();
            return $errors->toJson();
        }

        $result = response()->json(
            ['dataList' => "error",
            'dataStatus' => 
                ['data' => 0,
                'o_status'  => -2,
                'o_message' => "fail",]
            ], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $query                  = $request->get('sqlQuery');

            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $data = \DB::select(\DB::raw($query));

                $result = response()->json(
                    ['dataList' => $data,
                    'dataStatus' => 
                        ['data' => 0,
                        'o_status'  => 1,
                        'o_message' => "success",]
                    ], 200);
            }
        } 
        catch (Exception $e) 
        {
            $result = response()->json(
                ['dataList' => "error",
                'dataStatus' => 
                    ['data' => 0,
                    'o_status'  => -1,
                    'o_message' => $e->getMessage(),]
                ], 200);
        }

        return $result;    
    }

    public function getDataNoParam(Request $request){
        $validation = Validator::make($request->all(),[ 
            'sqlQuery'              => 'required',
        ]);

        if($validation->fails())
        {
            $errors = $validation->errors();
            return $errors->toJson();
        }

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $query                  = $request->get('sqlQuery');
            $data = \DB::select(\DB::raw($query));

            $result = response()->json(
                ['dataList' => $data,
                'dataStatus' => 
                    ['data' => 0,
                    'o_status'  => 1,
                    'o_message' => "success",]
                ], 200);
        } 
        catch (Exception $e) 
        {
            $result = response()->json([[
                'data' => 0,
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;    
    }

    public function getUserGroupAccess(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
            'p_group_user_id'      => 'required',
        ]);

        if($validation->fails())
        {
            $errors = $validation->errors();
            return $errors->toJson();
        }

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_group_user_id        = $request->get('p_group_user_id');

            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $query   = "select ms.module_id, ms.user_access_option 
                            from master_module_access ms, master_module ma
                            where ms.module_id = ma.module_id 
                            and ma.module_active = 1
                            and ms.group_id = ". $p_group_user_id;

                $data = \DB::select(\DB::raw($query));

                $result = response()->json(
                    ['dataList' => $data,
                    'dataStatus' => 
                        ['data' => 0,
                        'o_status'  => 1,
                        'o_message' => "success",]
                    ], 200);
            }
        } 
        catch (Exception $e) 
        {
            $result = response()->json([[
                'data' => 0,
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;    
    }
   
    public function changePassword(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
            'p_new_password'      => 'required',
        ]);

        if($validation->fails())
        {
            $errors = $validation->errors();
            return $errors->toJson();
        }

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        \DB::beginTransaction();

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_new_password         = $request->get('p_new_password');

            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $rows = \DB::table('user_login_data')
                ->where('user_id', $p_user_id)
                ->update([
                    'last_update_date' => new \DateTime(), 
                    'last_updated_by' => $p_user_id, 
                    'user_password' => $p_new_password,
                ]);
                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }
    }

    public function saveGroupUser(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));
                $queryData = $records[0];    

                if ($p_opType == 1) // INSERT
                {
                    $rows = \DB::table('master_group')
                            ->insertGetId([
                                'group_name' => $queryData->group_name,
                                'group_description' => $queryData->group_description,
                                'group_type' => $queryData->group_type,
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime()
                            ]);
                }
                else // UPDATE
                {
                    $rows = \DB::table('master_group')
                        ->where('group_id', $queryData->group_id)
                        ->update([
                            'group_name' => $queryData->group_name,
                            'group_description' => $queryData->group_description,
                            'group_type' => $queryData->group_type,
                            'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime()
                        ]);
                }

                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveUserData(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));
                $queryData = $records[0];    

                $recordsToken = json_decode($request->get('records_token'));
                $queryDataToken = $recordsToken[0];    

                if ($p_opType == 1) // INSERT
                {
                    $rows = \DB::table('user_login_data')
                            ->insertGetId([
                                'user_name' => $queryData->user_name,
                                'user_password' => $queryData->user_password,
                                'group_id' => $queryData->group_id,
                                'user_full_name' => $queryData->user_full_name,
                                'user_id_type' => $queryData->user_id_type,
                                'user_id_no' => $queryData->user_id_no,
                                'user_phone_1' => $queryData->user_phone_1,
                                'user_phone_2' => $queryData->user_phone_2,
                                'user_email_address' => $queryData->user_email_address,
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime()
                            ]);

                    $rowsToken = \DB::table('user_token')
                                ->insert([
                                'user_id' => $rows, //$queryDataToken->user_id,
                                'access_token' => $queryDataToken->access_token,
                            ]);
                }
                else // UPDATE
                {
                    $rows = \DB::table('user_login_data')
                        ->where('user_id', $queryData->user_id)
                        ->update([
                            'user_name' => $queryData->user_name,
                            'user_password' => $queryData->user_password,
                            'group_id' => $queryData->group_id,
                            'user_full_name' => $queryData->user_full_name,
                            'user_id_type' => $queryData->user_id_type,
                            'user_id_no' => $queryData->user_id_no,
                            'user_phone_1' => $queryData->user_phone_1,
                            'user_phone_2' => $queryData->user_phone_2,
                            'user_email_address' => $queryData->user_email_address,
                            'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime()
                        ]);

                    $rowsToken = \DB::table('user_token')
                        ->where('user_id', $queryDataToken->user_id)
                        ->update([
                            'access_token' => $queryDataToken->access_token,
                        ]);
                }

                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveModuleAccess(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_group_id        = $request->get('p_group_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));

                foreach ($records as $queryData) 
                {             
                    if ($queryData->uam_id > 0)
                    {
                        $rows = \DB::table('master_module_access')
                        ->where('id', $queryData->uam_id)
                        ->update([
                            'user_access_option' => $queryData->user_access_option,
                        ]);
                    }
                    else
                    {
                        $rows = \DB::table('master_module_access')
                            ->insert([
                            'group_id' => $p_group_id,
                            'module_id' => $queryData->module_id,
                            'user_access_option' => $queryData->user_access_option,
                        ]);
                    }
                }

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
                }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveSysConfig(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('sysConfigParam'));
                $sysConfigTrans = json_decode($request->get('sysConfigTransParam'));

                $sysConfig = $records[0];

                $rows = \DB::table('sys_config')
                    ->where('id', 1)
                    ->update([
                        'company_name' => $sysConfig->company_name,
                        'company_address' => $sysConfig->company_address,
                        'company_phone' => $sysConfig->company_phone,
                        'company_email' => $sysConfig->company_email,
                        'default_printer' => $sysConfig->default_printer,
                        'print_preview' => $sysConfig->print_preview,
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \DateTime(),
                    ]);

                foreach ($sysConfigTrans as $queryData) 
                {             
                    if ($queryData->id > 0)
                    {
                        $rowsTrans = \DB::table('sys_config_transaksi')
                        ->where('id', $queryData->id)
                        ->update([
                            'biaya_transaksi' => $queryData->biaya_transaksi,
                            'biaya_transaksi_percent' => $queryData->biaya_transaksi_percent,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime(),
                        ]);
                    }
                    else
                    {
                        $rowsTrans = \DB::table('sys_config_transaksi')
                            ->insert([
                                'payment_type' => $queryData->payment_type,
                                'biaya_transaksi' => $queryData->biaya_transaksi,
                                'biaya_transaksi_percent' => $queryData->biaya_transaksi_percent,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime(),
                        ]);
                    }
                }

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
                }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveMasterKompleks(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('kompleksParam'));
                $blokData = json_decode($request->get('blokParam'));

                $kompleksData = $records[0];

                if ($p_opType == 1) // INSERT
                {
                    $rows = \DB::table('master_kompleks')
                    ->insertGetId([
                        'kompleks_name' => $kompleksData->kompleks_name,
                        'kompleks_address' => $kompleksData->kompleks_address,
                        'biaya_ipl' => $kompleksData->biaya_ipl,
                        'durasi_pembayaran' => $kompleksData->durasi_pembayaran,
                        'is_active' => $kompleksData->is_active,
                        'created_by' => $p_user_id,
                        'creation_date' => new \DateTime()
                    ]);

                    $kompleksData->kompleks_id = $rows;
                }
                else // UPDATE
                {
                    $rows = \DB::table('master_kompleks')
                    ->where('kompleks_id', $kompleksData->kompleks_id)
                    ->update([
                        'kompleks_name' => $kompleksData->kompleks_name,
                        'kompleks_address' => $kompleksData->kompleks_address,
                        'biaya_ipl' => $kompleksData->biaya_ipl,
                        'durasi_pembayaran' => $kompleksData->durasi_pembayaran,
                        'is_active' => $kompleksData->is_active,
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \DateTime(),
                    ]);
                }

                foreach ($blokData as $queryData) 
                {             
                    if ($queryData->blok_id > 0)
                    {
                        $rowsTrans = \DB::table('master_blok')
                        ->where('blok_id', $queryData->blok_id)
                        ->update([
                            'blok_name' => $queryData->blok_name,
                            'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime(),
                        ]);
                    }
                    else
                    {
                        $rowsTrans = \DB::table('master_blok')
                            ->insert([
                                'kompleks_id' => $kompleksData->kompleks_id,
                                'blok_name' => $queryData->blok_name,
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime(),
                        ]);
                    }
                }

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
                }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveMasterKavling(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));
                $queryData = $records[0];    

                if ($p_opType == 1) // INSERT
                {
                    $rows = \DB::table('master_kavling')
                            ->insertGetId([
                                'kompleks_id' => $queryData->kompleks_id,
                                'blok_id' => $queryData->blok_id,
                                'house_no' => $queryData->house_no,
                                'biaya_ipl' => $queryData->biaya_ipl,
                                'durasi_pembayaran' => $queryData->durasi_pembayaran,
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime()
                            ]);
                }
                else // UPDATE
                {
                    $rows = \DB::table('master_kavling')
                        ->where('kavling_id', $queryData->kavling_id)
                        ->update([
                            'kompleks_id' => $queryData->kompleks_id,
                            'blok_id' => $queryData->blok_id,
                            'house_no' => $queryData->house_no,
                            'biaya_ipl' => $queryData->biaya_ipl,
                            'durasi_pembayaran' => $queryData->durasi_pembayaran,
                            'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime()
                        ]);
                }

                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveUserKavling(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }

        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -2,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));

                foreach ($records as $queryData) 
                {          
                    if ($queryData->id > 0)
                    {
                        $rows = \DB::table('user_kavling')
                        ->where('id', $queryData->id)
                        ->update([
                            'user_id' => $queryData->user_id,
                            'kavling_id' => $queryData->kavling_id,
                            'start_ipl' => $queryData->start_ipl,
                            'remarks' => $queryData->remarks,
                            'user_qr_code' => $queryData->user_qr_code,
                            'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \Datetime(),
                        ]);
                    }
                    else
                    {
                        $rows = \DB::table('user_kavling')
                        ->insert([
                            'user_id' => $queryData->user_id,
                            'kavling_id' => $queryData->kavling_id,
                            'start_ipl' => $queryData->start_ipl,
                            'remarks' => $queryData->remarks,
                            'user_qr_code' => $queryData->user_qr_code,
                            'is_active' => $queryData->is_active,
                            'created_by' => $p_user_id,
                            'creation_date' => new \DateTime()
                        ]);
                    }
                }

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveMasterItem(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));
                $queryData = $records[0];    

                if ($p_opType == 1) // INSERT
                {
                    $rows = \DB::table('master_item')
                            ->insertGetId([
                                'item_name' => $queryData->item_name,
                                'unit_id' => $queryData->unit_id,
                                'item_hpp' => $queryData->item_hpp,
                                'item_price' => $queryData->item_price,
                                'qty' => $queryData->qty,
                                'item_jasa' => $queryData->item_jasa,
                                'image_file' => $queryData->image_file,
                                'item_category_id' => $queryData->item_category_id,
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime()
                            ]);
                }
                else // UPDATE
                {
                    $rows = \DB::table('master_item')
                        ->where('item_id', $queryData->item_id)
                        ->update([
                            'item_name' => $queryData->item_name,
                            'unit_id' => $queryData->unit_id,
                            'item_hpp' => $queryData->item_hpp,
                            'item_price' => $queryData->item_price,
                            'qty' => $queryData->qty,
                            'item_jasa' => $queryData->item_jasa,
                            'image_file' => $queryData->image_file,
                            'item_category_id' => $queryData->item_category_id,
                        'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime()
                        ]);
                }

                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveMasterUnit(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));
                $queryData = $records[0];    

                if ($p_opType == 1) // INSERT
                {
                    $rows = \DB::table('master_unit')
                            ->insertGetId([
                                'unit_name' => $queryData->unit_name,                            
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime()
                            ]);
                }
                else // UPDATE
                {
                    $rows = \DB::table('master_unit')
                        ->where('unit_id', $queryData->unit_id)
                        ->update([
                            'unit_name' => $queryData->unit_name,       
                            'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime()
                        ]);
                }

                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveSummaryIPL(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('headerParam'));
                $detailData = json_decode($request->get('detailParam'));

                $headerData = $records[0];

                if ($p_opType == 1) // INSERT
                {
                    $rows = \DB::table('transaksi_ipl')
                    ->insertGetId([
                        'type_trans' => $headerData->type_trans,
                        'date_issued' => $headerData->date_issued,
                        'kavling_id' => $headerData->kavling_id,
                        'user_id' => $headerData->user_id,
                        'nominal' => $headerData->nominal,
                        'start_ipl' => $headerData->start_ipl,
                        'end_ipl' => $headerData->end_ipl,
                        'is_active' => $headerData->is_active,
                        'created_by' => $p_user_id,
                        'creation_date' => new \DateTime()
                    ]);

                    $headerData->id_trans = $rows;
                }
                else // UPDATE
                {
                    $rows = \DB::table('transaksi_ipl')
                    ->where('id_trans', $headerData->id_trans)
                    ->update([
                        'is_active' => $headerData->is_active,
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \DateTime(),
                    ]);
                }

                foreach ($detailData as $queryData) 
                {             
                    $rowsTrans = \DB::table('transaksi_ipl_detail')
                    ->insert([
                        'id_trans' => $headerData->id_trans,
                        'nominal' => $queryData->nominal,
                        'periode_pembayaran' => $queryData->periode_pembayaran,
                        'is_active' => $queryData->is_active,
                        'created_by' => $p_user_id,
                        'creation_date' => new \DateTime(),
                        ]);
                }

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
                }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function addQty($p_itemID, $p_qty){
        $query = "  
        update
        master_item
        set qty = qty + ".$p_qty."
        where item_id = ".$p_itemID;

        $rows = \DB::update($query);

        return $rows;
    }

    public function reduceQty($p_itemID, $p_qty){
        $query = "  
        update
        master_item
        set qty = qty - ".$p_qty."
        where item_id = ".$p_itemID;

        $rows = \DB::update($query);

        return $rows;
    }

    public function updateQtyRaw($p_itemID, $p_qty){
        $query = "  
        update
        master_item
        set qty = ".$p_qty."
        where item_id = ".$p_itemID;

        $rows = \DB::update($query);

        return $rows;
    }

    public function returnQtyBecauseOfVoid(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_idTrans              = $request->get('p_idTrans');
            $p_transType            = $request->get('p_transType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                if ($p_transType == 1) // TRANSAKSI RETAIL
                {
                    $query = "
                    SELECT td.item_id, td.item_qty 
                    FROM transaksi_retail_detail td, master_item mi 
                    WHERE td.item_id = mi.item_id 
                    AND td.is_active = 'Y' 
                    AND mi.item_jasa = 'N' 
                    AND td.id_trans = ". $p_idTrans;
    
                    $records = \DB::select(\DB::raw($query));
    
                    foreach ($records as $queryData) 
                    {             
                        $rows = $this->addQty($queryData->item_id, $queryData->item_qty);
                    }
                }
                else // PENERIMAAN DATA
                {
                    $query = "
                    SELECT td.item_id, td.item_qty 
                    FROM penerimaan_detail td, master_item mi 
                    WHERE td.item_id = mi.item_id 
                    AND td.is_active = 'Y' 
                    AND mi.item_jasa = 'N' 
                    AND td.penerimaan_id = ". $p_idTrans;

                    $records = \DB::select(\DB::raw($query));
    
                    foreach ($records as $queryData) 
                    {             
                        $rows = $this->reduceQty($queryData->item_id, $queryData->item_qty);
                    }
                }
                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function updateItemQty(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $p_itemID               = $request->get('p_itemID');
            $p_qty                  = $request->get('p_qty');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                

                $rows = $this->addQty($p_itemID, $p_qty);

                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveDataTransaksi(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('headerTransParam'));
                $detailData = json_decode($request->get('detailTransParam'));

                $headerData = $records[0];

                if ($p_opType == 1) // INSERT
                {
                    $expiredDT = Carbon::now();
                    $expiredDT->addHour(12);                             

                    $rows = \DB::table('transaksi_retail')
                    ->insertGetId([
                        'type_trans' => $headerData->type_trans,
                        'date_issued' => $headerData->date_issued,
                        'kavling_id' => $headerData->kavling_id,
                        'user_id' => $headerData->user_id,
                        'nominal' => $headerData->nominal,
                        'date_expired' => $expiredDT,
                        'is_active' => $headerData->is_active,
                        'created_by' => $p_user_id,
                        'creation_date' => new \DateTime()
                    ]);

                    $headerData->id_trans = $rows;
                }
                else // UPDATE
                {
                    $rows = \DB::table('transaksi_retail')
                    ->where('id_trans', $headerData->id_trans)
                    ->update([
                        'type_trans' => $headerData->type_trans,
                        'date_issued' => $headerData->date_issued,
                        'kavling_id' => $headerData->kavling_id,
                        'user_id' => $headerData->user_id,
                        'nominal' => $headerData->nominal,
                        'is_active' => $headerData->is_active,
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \DateTime(),
                    ]);
                }

                foreach ($detailData as $queryData) 
                {   
                    if ($queryData->is_active == 'Y')
                    {
                        if ($queryData->item_jasa == 'N')
                            $rowsTrans = $this->reduceQty($queryData->item_id, ($queryData->item_qty - $queryData->old_qty));

                        if ($queryData->id > 0)
                        {
                            $rowsTrans = \DB::table('transaksi_retail_detail')
                            ->where('id', $queryData->id)
                            ->update([
                                'id_trans' => $headerData->id_trans,
                                'item_id' => $queryData->item_id,
                                'item_qty' => $queryData->item_qty,
                                'item_hpp' => $queryData->item_hpp,
                                'item_price' => $queryData->item_price,
                                'subtotal' => $queryData->subtotal,
                                'is_active' => $queryData->is_active,
                                'last_updated_by' => $p_user_id,
                                'last_update_date' => new \DateTime(),
                                ]);
                        }
                        else
                        {
                            $rowsTrans = \DB::table('transaksi_retail_detail')
                            ->insert([
                                'id_trans' => $headerData->id_trans,
                                'item_id' => $queryData->item_id,
                                'item_qty' => $queryData->item_qty,
                                'item_hpp' => $queryData->item_hpp,
                                'item_price' => $queryData->item_price,
                                'subtotal' => $queryData->subtotal,
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime(),
                                ]);
                        }
                    }
                    else
                    {
                        if ($queryData->item_jasa == 'N')
                            $rowsTrans = $this->addQty($queryData->item_id, $queryData->old_qty);

                        $rowsTrans = \DB::table('transaksi_retail_detail')
                            ->where('id', $queryData->id)
                            ->update([
                                'is_active' => $queryData->is_active,
                                'last_updated_by' => $p_user_id,
                                'last_update_date' => new \DateTime(),
                                ]);
                    }          
                }

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
                }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function penyesuaianStok(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));

                $headerData = $records[0];

                $rowsTrans = $this->updateQtyRaw($headerData->item_id, $headerData->qty_baru);

                $rows = \DB::table('penyesuaian_stok')
                ->insertGetId([
                    'item_id' => $headerData->item_id,
                    'qty_awal' => $headerData->qty_awal,
                    'qty_baru' => $headerData->qty_baru,
                    'keterangan' => $headerData->keterangan,
                    'created_by' => $p_user_id,
                    'creation_date' => new \DateTime()
                ]);

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
                }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function savePenerimaan(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $p_opType               = $request->get('p_opType');
            $num                    = $this->checkToken($p_user_id, $p_access_token);
            
            if ($num > 0)
            {
                $json = $request->get('headerTransParam');
                $records = json_decode($json);
                $headerData = $records[0];

                $jsonDetail = $request->get('detailTransParam');
                $detailData = json_decode($jsonDetail);

                if ($p_opType == 1) // INSERT
                {
                    $rows = \DB::table('penerimaan_header')
                    ->insertGetId([
                        'penerimaan_datetime' => $headerData->penerimaan_datetime,
                        'type_trans' => $headerData->type_trans,
                        'penerimaan_total' => $headerData->penerimaan_total,
                        'is_active' => $headerData->is_active,
                        'created_by' => $p_user_id,
                        'creation_date' => new \DateTime()
                    ]);

                    $headerData->penerimaan_id = $rows;
                }
                else // UPDATE
                {
                    $rows = \DB::table('penerimaan_header')
                    ->where('penerimaan_id', $headerData->penerimaan_id)
                    ->update([
                        'penerimaan_datetime' => $headerData->penerimaan_datetime,
                        'type_trans' => $headerData->type_trans,
                        'penerimaan_total' => $headerData->penerimaan_total,
                        'is_active' => $headerData->is_active,
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \DateTime(),
                    ]);
                }

                foreach ($detailData as $queryData) 
                {   
                    if ($queryData->is_active == 'Y')
                    {
                        $rowsTrans = $this->addQty($queryData->item_id, ($queryData->item_qty - $queryData->old_qty));

                        if ($queryData->id > 0)
                        {
                            $rowsTrans = \DB::table('penerimaan_detail')
                            ->where('id', $queryData->id)
                            ->update([
                                'penerimaan_id' => $headerData->penerimaan_id,
                                'item_id' => $queryData->item_id,
                                'item_qty' => $queryData->item_qty,
                                'item_hpp' => $queryData->item_hpp,
                                'item_subtotal' => $queryData->item_subtotal,
                                'is_active' => $queryData->is_active,
                                'last_updated_by' => $p_user_id,
                                'last_update_date' => new \DateTime(),
                                ]);
                        }
                        else
                        {
                            $rowsTrans = \DB::table('penerimaan_detail')
                            ->insert([
                                'penerimaan_id' => $headerData->penerimaan_id,
                                'item_id' => $queryData->item_id,
                                'item_qty' => $queryData->item_qty,
                                'item_hpp' => $queryData->item_hpp,
                                'item_subtotal' => $queryData->item_subtotal,
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime(),
                                ]);
                        }
                    }
                    else
                    {
                        $rowsTrans = $this->reduceQty($queryData->item_id, $queryData->old_qty);

                        $rowsTrans = \DB::table('penerimaan_detail')
                            ->where('id', $queryData->id)
                            ->update([
                                'is_active' => $queryData->is_active,
                                'last_updated_by' => $p_user_id,
                                'last_update_date' => new \DateTime(),
                                ]);
                    }          
                }

                \DB::commit();

                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveCustCareDetail(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $json = $request->get('headerTransParam');
                $records = json_decode($json);

                if (json_last_error() == 0 && is_array($records))
                {
                    $headerData = $records[0];

                    $rows = \DB::table('cc_ticket')
                    ->where('ticket_id', $headerData->ticket_id)
                    ->update([
                        'status' => $headerData->status,
                        'status_date' => $headerData->status_date,
                        'status_by' => $headerData->status_by,
                        'mtc_detail' => $headerData->mtc_detail,
                        'mtc_cost' => $headerData->mtc_cost,
                        'last_updated_by' => $p_user_id,
                        'last_update_date' => new \DateTime()
                    ]);
                }  

                $jsonDetail = $request->get('detailTransParam');
                $detailData = json_decode($jsonDetail);
                $queryData = $detailData[0];    

                if ($queryData->history_id == 0) // INSERT
                {
                    $rows = \DB::table('cc_ticket_history')
                            ->insertGetId([
                                'ticket_id' => $queryData->ticket_id,
                                'sent_by' => $queryData->sent_by,
                                'send_date' => $queryData->send_date,
                                'description' => $queryData->description,
                                'is_mtc' => $queryData->is_mtc,
                                'mtc_cost' => $queryData->mtc_cost,
                                'is_active' => $queryData->is_active,
                                'created_by' => $p_user_id,
                                'creation_date' => new \DateTime()
                            ]);

                    $queryData->history_id = $rows;
                }
                else // UPDATE
                {
                    $rows = \DB::table('cc_ticket_history')
                        ->where('history_id', $queryData->history_id)
                        ->update([
                            'ticket_id' => $queryData->ticket_id,
                            'sent_by' => $queryData->sent_by,
                            'send_date' => $queryData->send_date,
                            'description' => $queryData->description,
                            'is_mtc' => $queryData->is_mtc,
                            'mtc_cost' => $queryData->mtc_cost,
                            'is_active' => $queryData->is_active,
                            'last_updated_by' => $p_user_id,
                            'last_update_date' => new \DateTime()
                        ]);
                }

                $jsonImg = $request->get('imgAttachmentParam');
                $imgDataList = json_decode($jsonImg);
                foreach ($imgDataList as $imgData) 
                {
                    if ($imgData->ftpFileName != "")
                    {
                        if ($imgData->attach_id == 0) // INSERT
                        {
                            $rows = \DB::table('cc_ticket_history_attach')
                                    ->insertGetId([
                                        'history_id' => $queryData->history_id,
                                        'ticket_id' => $queryData->ticket_id,
                                        'seqno' => $imgData->seqno,
                                        'attach_file' => $imgData->ftpFileName ,
                                        'is_active' => $imgData->is_active,
                                        'created_by' => $p_user_id,
                                        'creation_date' => new \DateTime()
                                    ]);
                        }
                        else // UPDATE
                        {
                            $rows = \DB::table('cc_ticket_history_attach')
                                ->where('attach_id', $imgData->attach_id)
                                ->update([
                                    'history_id' => $queryData->history_id,
                                    'ticket_id' => $queryData->ticket_id,
                                    'seqno' => $imgData->seqno,
                                    'attach_file' => $imgData->ftpFileName ,
                                    'is_active' => $imgData->is_active,
                                    'last_updated_by' => $p_user_id,
                                    'last_update_date' => new \DateTime()
                            ]);
                        } 
                    }   
                }

                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function updateCustCare(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $records = json_decode($request->get('records'));
                $queryData = $records[0];    

                $rows = \DB::table('cc_ticket')
                ->where('ticket_id', $queryData->ticket_id)
                ->update([
                    'status' => $queryData->status,
                    'status_date' => $queryData->status_date,
                    'status_by' => $queryData->status_by,
                    'is_closed' => $queryData->is_closed,
                    'is_active' => $queryData->is_active,
                    'last_updated_by' => $p_user_id,
                    'last_update_date' => new \DateTime()
                ]);

                \DB::commit();
      
                $result = response()->json([[
                        'data' => $rows, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function resetExpiredTrans(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
            'p_user_id'             => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $p_user_id              = $request->get('p_user_id');
            $num = $this->checkToken($p_user_id, $p_access_token);

            if ($num > 0)
            {
                $query = $request->get('sqlQuery');
                $data = \DB::select(\DB::raw($query));

                foreach ($data as $transData)
                {
                    $rows = \DB::table('transaksi_retail')
                    ->where('id_trans', $transData->id_trans)
                    ->update([
                        'is_active' => 'N',
                        'last_updated_by' => 0,
                        'last_update_date' => new \DateTime()
                    ]);
   
                    $queryDetail = "  
                    SELECT
                    td.item_id, td.item_qty
                    FROM transaksi_retail_detail td, master_item mi
                    where td.id_trans = ". $transData->id_trans . "
                    AND td.item_id = mi.item_id 
                    AND mi.item_jasa = 'N'";
                    
                    $itemData = \DB::select(\DB::raw($queryDetail));
                    foreach ($itemData as $transDetailData)
                    {
                        $detailProc = $this->addQty($transDetailData->item_id, $transDetailData->item_qty);
                    }
                } 

                \DB::commit();
      
                $result = response()->json([[
                        'data' => 1, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }
    
}