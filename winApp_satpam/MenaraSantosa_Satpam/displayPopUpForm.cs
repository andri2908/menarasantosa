﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class displayPopUpForm : Form
    {
        public displayPopUpForm(string userName, string timestamp, string accessID, string message)
        {
            InitializeComponent();

            labelTimeStamp.Text = timestamp;
            labelAccessID.Text = accessID;
            labelUserName.Text = userName;
            labelMessage.Text = message;
        }

        private void displayPopUpForm_Load(object sender, EventArgs e)
        {
            //timerClose.Start();
        }

        private void timerClose_Tick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
