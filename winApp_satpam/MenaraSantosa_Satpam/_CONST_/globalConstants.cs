﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaSoft
{
    class globalConstants
    {
        public const string TS_submit = "submit";
        public const string TS_closed = "closed";
        public const string TS_admin_response = "admin_response";
        public const string TS_user_response  = "user_response";
        public const string TS_request_approval = "request_approval";
        public const string TS_approved = "approved";
        public const string TS_rejected = "rejected";

        public const int ITEM_RETAIL = 1;
        public const int ITEM_DRUGS = 2;

        public const int GROUP_RESIDENT = 0;
        public const int GROUP_LANDLORD = 1;
        public const int GROUP_ADMIN = 2;

        public const int TRANS_TYPE_IPL = 1;
        public const int TRANS_TYPE_RETAIL = 2;
        public const int TRANS_TYPE_HANDYMAN = 3;
        public const int TRANS_TYPE_RETAIL_DRUGS = 4;

        public const int NEW_GROUP_USER = 1;
        public const int EDIT_GROUP_USER = 2;
        public const int NEW_USER = 3;
        public const int EDIT_USER = 4;
        public const int PENGATURAN_GROUP_ACCESS = 5;

        public const int NEW_KOMPLEKS = 11;
        public const int EDIT_KOMPLEKS = 12;
        public const int NEW_KAVLING = 13;
        public const int EDIT_KAVLING = 14;

        public const int NEW_UNIT = 15;
        public const int EDIT_UNIT = 16;

        public const int NEW_ITEM = 17;
        public const int EDIT_ITEM = 18;

        public const int ADD_KAVLING_USER = 15;

        public const int SUMMARY_IPL = 16;
        public const int USER_RFID = 17;

        public const int MODULE_USER = 1;
        public const int MODULE_HAK_AKSES = 2;

        public const int MODULE_ITEM_RETAIL = 6;
        public const int MODULE_ITEM_DRUGS = 7;

        public const int MODULE_TRANSAKSI_RETAIL = 11;
        public const int MODULE_PENYESUAIAN_STOK = 12;
        public const int MODULE_PENERIMAAN_BARANG = 13;

        public const int MODULE_TRANSAKSI_DRUGS = 14;
        public const int MODULE_PENYESUAIAN_DRUGS = 15;
        public const int MODULE_PENERIMAAN_DRUGS = 16;

        public const int MODULE_TICKET_REPAIR = 21;
        public const int MODULE_PENGERJAAN_REPAIR = 22;

        public const int MODULE_CETAK_TRANSAKSI_RETAIL = 23;
        public const int MODULE_CETAK_TRANSAKSI_DRUGS = 24;

        public const int VIEW_ONLY = 98;
        public const int MODULE_DEFAULT = 99;
        public const int MODULE_2ND_AUTH = 100;
    }
}
