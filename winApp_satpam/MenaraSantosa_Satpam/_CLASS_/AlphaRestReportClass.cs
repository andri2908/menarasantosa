﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaSoft
{
    public class REST_printoutTransaksi
    {
        public List<PRINTOUT_transaksi> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_reportSysConfig
    {
        public List<report_sysConfig> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class report_sysConfig
    {
        public string name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
    }



    public class PRINTOUT_transaksi
    {
        public string title { get; set; }
        public string user_full_name { get; set; }
        public string user_name { get; set; }
        public string nama_kavling { get; set; }
        public string user_phone_1 { get; set; }
        public string item_1 { get; set; }
        public double qty { get; set; }
        public double harga { get; set; }
        public double nominal { get; set; }
        public string item_2 { get; set; }
    }
    
}
