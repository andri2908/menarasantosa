﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaSoft
{
    public class genericReply
    {
        public object data { get; set; }
        public int o_status { get; set; }
        public string o_message { get; set; }
    }

    public class chiki_reply
    {
        public int o_status { get; set; }
        public string o_message { get; set; }
        public string o_history_id { get; set; }
    }

    class REST_groupUserAccess
    {
        public List<master_module_access> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_groupUser
    {
        public List<master_group> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_userLoginData
    {
        public List<user_login_data> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_userAccessData
    {
        public List<user_access> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_masterModule
    {
        public List<master_module> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_sysConfig
    {
        public List<sys_config> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_sysConfigTrans
    {
        public List<sys_config_trans> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_sysConfigSchedule
    {
        public List<sys_config_schedule> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_masterKompleks
    {
        public List<master_kompleks> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_masterBlok
    {
        public List<master_blok> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_masterKavling
    {
        public List<master_kavling> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_userKavling
    {
        public List<user_kavling> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_masterItem
    {
        public List<master_item> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_masterUnit
    {
        public List<master_unit> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_dataIPL
    {
        public List<data_ipl> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_dataTransaksi
    {
        public List<data_transaksi> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_transaksiHeader
    {
        public List<transaksi_header> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_transaksiDetail
    {
        public List<transaksi_detail> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_penerimaanHeader
    {
        public List<penerimaan_header> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_penerimaanDetail
    {
        public List<penerimaan_detail> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_custCare
    {
        public List<cc_ticket> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_custCareDetail
    {
        public List<cc_ticket_history> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_itemCategory
    {
        public List<item_category> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_expiredTrans
    {
        public List<expired_trans> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_imgAttachment
    {
        public List<imgAttachment> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_news
    {
        public List<news> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_updKavling
    {
        public List<upd_master_kavling> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_userKavlingRFID
    {
        public List<user_kavling_rfid> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_satpamAccess
    {
        public List<satpam_access_list> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_kompleksTerminal
    {
        public List<kompleks_terminal> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class REST_userAccessList
    {
        public List<user_access_list> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class news
    {
        public int news_id { get; set; }
        public string news_type { get; set; }
        public string subject { get; set; }
        public string description { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public string file_name { get; set; }
        public string is_banner { get; set; }
        public string is_active { get; set; }
        public string is_expired { get; set; }
    }

    public class ccAttach
    {
        public string raw_photo { get; set; }
        public int seqno { get; set; }
    }

    public class imgAttachment
    {
        public int seqno { get; set; }
        public int attach_id { get; set; }
        public string is_active { get; set; }
        public string fileName { get; set; }
        public string ftpFileName { get; set; }
        public bool ftpImage { get; set; }

        public imgAttachment()
        {
            seqno = 0;
            attach_id = 0;
            is_active = "Y";
            fileName = "";
            ftpFileName = "";
            ftpImage = false;
        }
    }

    public class expired_trans
    {
        public int id_trans { get; set; }
    }

    public class item_category
    {
        public int item_category_id { get; set; }
        public string item_category_name { get; set; }
    }

    public class cc_ticket_history
    {
        public int history_id { get; set; }
        public int ticket_id { get; set; }
        public int sent_by { get; set; }
        public string user_name { get; set; }
        public string user_full_name { get; set; }
        public DateTime send_date { get; set; }
        public string description { get; set; }
        public string is_mtc { get; set; }
        public double mtc_cost { get; set; }
  
        public string attach_file { get; set; }
        public int group_id { get; set; }

        public string is_active { get; set; }
        public int group_type { get; set; }

        public void resetValue()
        {
            history_id = 0;
            ticket_id = 0;
            sent_by = 0;
            user_name = "";
            user_full_name = "";
            send_date = DateTime.Now;
            description = "";
            is_active = "Y";
            is_mtc = "N";
            mtc_cost = 0;
        }
    }

    public class cc_status
    {
        public int ticket_id { get; set; }
        public string status { get; set; }
        public string is_closed { get; set; }
        public string is_active { get; set; }
        public DateTime status_date { get; set; }
        public int status_by { get; set; }
    }

    public class cc_ticket
    {
        public int ticket_id { get; set; }
        public string ticket_num { get; set; }
        public DateTime ticket_date { get; set; }
        public int request_by { get; set; }
        public string user_name { get; set; }
        public string user_full_name { get; set; }
        public int kavling_id { get; set; }
        public string blok_name { get; set; }
        public string house_no { get; set; }
        public int kompleks_id { get; set; }
        public string kompleks_name { get; set; }
        public string subject { get; set; }
        public int category_id { get; set; }
        public string category_name { get; set; }
        public string description { get; set; }
        public string status { get; set; }
        public string is_closed { get; set; }
        public string is_active { get; set; }
        public string is_mtc { get; set; }
        public string mtc_detail { get; set; }
        public double mtc_cost { get; set; }
        public DateTime status_date { get; set; }
        public int status_by { get; set; }
        public DateTime start_date { get; set; }
        public string vendor_name { get; set; }
    }

    public class penerimaan_header
    {
        public int penerimaan_id { get; set; }
        public DateTime penerimaan_datetime { get; set; }
        public int type_trans { get; set; }
        public double penerimaan_total { get; set; }
        public string is_active { get; set; }
    }

    public class penerimaan_detail
    {
        public int id { get; set; }
        public int penerimaan_id { get; set; }
        public int item_id { get; set; }
        public double item_qty { get; set; }
        public double item_hpp { get; set; }
        public double item_subtotal { get; set; }
        public string is_active { get; set; }
        public string item_name { get; set; }
        public string satuan { get; set; }
        public double old_qty { get; set; }
    }

    public class penyesuaian_stok
    {
        public int item_id { get; set; }
        public double qty_awal { get; set; }
        public double qty_baru { get; set; }
        public string keterangan { get; set; }
    }

    public class item_qty
    {
        public int item_id { get; set; }
        public double qty { get; set; }
        public double old_qty { get; set; }
    }

    public class transaksi_header
    {
        public int id_trans { get; set; }
        public int type_trans { get; set; }
        public DateTime date_issued { get; set; }
        public int kavling_id { get; set; }
        public int user_id { get; set; }
        public string payment_id { get; set; }
        public double nominal { get; set; }
        public int status_id { get; set; }
        public string user_name { get; set; }
        public string is_active { get; set; }
        public string kompleks { get; set; }
        public string nama_kavling { get; set; }
        public int kompleks_id { get; set; }
        public string deliver_status { get; set; }

    }

    public class transaksi_detail
    {
        public int id { get; set; }
        public int id_trans { get; set; }
        public int item_id { get; set; }
        public double item_qty { get; set; }
        public double item_hpp { get; set; }
        public double item_price { get; set; }
        public double subtotal { get; set; }
        public string is_active { get; set; }
        public string item_name { get; set; }
        public string satuan { get; set; }
        public string item_jasa { get; set; }
        public double old_qty { get; set; }
    }

    public class data_transaksi
    {
        public int id_trans { get; set; }
        public string tgl { get; set; }
        public string user_name { get; set; }
        public string kompleks_name { get; set; }
        public string blok { get; set; }
        public double nominal { get; set; }
    }

    public class trans_ipl_header
    {
        public int id_trans { get; set; }
        public int type_trans { get; set; }
        public DateTime date_issued { get; set; }
        public int kavling_id { get; set; }
        public int user_id { get; set; }
        public string payment_id { get; set; }
        public double nominal { get; set; }
        public int status_id { get; set; }
        public DateTime date_paid { get; set; }
        public DateTime start_ipl { get; set; }
        public DateTime end_ipl { get; set; }
        public string is_active { get; set; }

        public List<trans_ipl_detail> detail_data { get; set; }
    }

    public class trans_ipl_detail
    {
        public int id { get; set; }
        public int id_trans { get; set; }
        public double nominal { get; set; }
        public DateTime periode_pembayaran { get; set; }
        public string is_active { get; set; }
    }

    public class data_ipl
    {
        public int id_trans { get; set; }
        public DateTime date_issued { get; set; }
        public DateTime date_paid { get; set; }
        public DateTime start_ipl { get; set; }
        public DateTime end_ipl { get; set; }
        public int status_id { get; set; }
        public double nominal { get; set; }
        public int user_id { get; set; }
        public string user_name { get; set; }
    }

    public class master_unit
    {
        public int unit_id { get; set; }
        public string unit_name { get; set; }
        public string is_active { get; set; }
    }

    public class master_item
    {
        public int item_id { get; set; }
        public string item_name { get; set; }
        public int unit_id { get; set; }
        public string unit_name { get; set; }
        public double item_hpp { get; set; }
        public double item_price { get; set; }
        public double qty { get; set; }
        public string item_jasa { get; set; }
        public int item_category_id { get; set; }
        public string image_file { get; set; }
        public string is_active { get; set; }
    }

    public class user_kavling
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public int kavling_id { get; set; }
        public DateTime start_ipl { get; set; }
        public string remarks { get; set; }
        public string user_qr_code { get; set; }
        public string user_qr_enabled { get; set; }
        public string is_active { get; set; }
        public string user_full_name { get; set; }
        public string user_name { get; set; }
    }

    public class master_blok
    {
        public int blok_id { get; set; }
        public int kompleks_id { get; set; }
        public string blok_name { get; set; }
        public string is_active { get; set; }
    }

    public class master_kavling
    {
        public int kavling_id { get; set; }
        public int kompleks_id { get; set; }
        public int blok_id { get; set; }
        public string blok_name { get; set; }
        public string house_no { get; set; }
        public string kompleks_name { get; set; }
        public double biaya_ipl { get; set; }
        public int durasi_pembayaran { get; set; }
        public string status { get; set; }
        public string is_active { get; set; }
    }

    public class upd_master_kavling
    {
        public int kavling_id { get; set; }
        public string status { get; set; }
    }

    public class user_kavling_rfid
    {
        public int id { get; set; }
        public int user_kavling_id { get; set; }
        public string rfid_value { get; set; }
        public string rfid_enabled { get; set; }
        public string is_active { get; set; }
    }

    public class satpam_access_list
    {
        public int id { get; set; }
        public int kompleks_id { get; set; }
        public string user_full_name { get; set; }
        public string access_id { get; set; }
        public string access_type { get; set; }
        public string is_active { get; set; }
    }

    public class kompleks_terminal
    {
        public int id { get; set; }
        public int kompleks_id { get; set; }
        public string terminal_id { get; set; }
        public string terminal_type { get; set; }
        public string is_active { get; set; }
    }

    public class master_kompleks
    {
        public int kompleks_id { get; set; }
        public string kompleks_name { get; set; }
        public string kompleks_address { get; set; }
        public double biaya_ipl { get; set; }
        public int durasi_pembayaran { get; set; }
        public string img_kompleks { get; set; }
        public string is_active { get; set; }
    }

    public class sys_config_trans
    {
        public int id { get; set; }
        public int payment_type { get; set; }
        public string payment_type_name { get; set; }
        public double biaya_transaksi { get; set; }
        public double biaya_transaksi_percent { get; set; }
    }

    public class sys_config_schedule
    {
        public int id { get; set; }
        public string flag { get; set; }
        public DateTime start_time { get; set; }
        public DateTime end_time { get; set; }
    }

    public class sys_config
    {
        public string company_name { get; set; }
        public string company_address { get; set; }
        public string company_phone { get; set; }
        public string company_email { get; set; }
        public string default_printer { get; set; }
        public int auto_backup_flag { get; set; }
        public int print_preview { get; set; }
        public string message_template { get; set; }
    }

    public class master_module
    {
        public int module_id { get; set; }
        public string module_name { get; set; }
        public int module_features { get; set; }
        public int user_access_option { get; set; }
        public int opType { get; set; }
        public int uam_id { get; set; }
    }

    public class user_access
    {
        public int user_id { get; set; }
        public string user_name { get; set; }
        public string user_full_name { get; set; }
        public string access_token { get; set; }
    }

    public class user_token
    {
        public int user_id { get; set; }
        public string access_token { get; set; }
    }

    public class user_login_data
    {
        public int user_id { get; set; }
        public string user_name { get; set; }
        public string user_password { get; set; }
        public int group_id { get; set; }
        public string user_full_name { get; set; }
        public int user_id_type { get; set; }
        public string user_id_no { get; set; }
        public string user_phone_1 { get; set; }
        public string user_phone_2 { get; set; }
        public string user_email_address { get; set; }
        public string is_active { get; set; }
        public string group_name { get; set; }
    }

    public class master_group
    {
        public int group_id { get; set; }
        public string group_name { get; set; }
        public string group_description { get; set; }
        public int group_type { get; set; }
        public string is_active { get; set; }
    }

    class master_module_access
    {
        public int module_ID { get; set; }
        public int user_access_option { get; set; }
    }

    public class user_access_list
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public int kompleks_id { get; set; }
        public int kavling_id { get; set; }
        public string access_id { get; set; }
        public string access_type { get; set; }
        public int access_status { get; set; }
        public string message { get; set; }
        public string is_active { get; set; }
    }


    public class REST_satpamLog
    {
        public List<satpam_log> dataList { get; set; }
        public genericReply dataStatus { get; set; }
    }

    public class satpam_log
    {
        public int id { get; set; }
        public int access_status { get; set; }
        public string user_full_name { get; set; }
        public string access_id { get; set; }
        public DateTime timestamp { get; set; }
        public string terminal_id { get; set; }
        public string terminal_type { get; set; }
    }
    
    public class server_user_all
    {
        public string user_full_name { get; set; }
        public string terminal_id { get; set; }
        public string access_id { get; set; }
        public string access_type { get; set; }
        public int access_status { get; set; }
        public string message { get; set; }
    }

    public class server_userList
    {
        public int Status { get; set; }
        public string CardNo { get; set; }
        public string FullName { get; set; }
        public string UTC { get; set; }
        public string Message { get; set; }
        public int recordCount { get; set; }
    }
}
