﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlphaSoft
{   
    class sqlFields
    {
        public string fieldName { get; set; }

        public object fieldValue { get; set; }
    }

    class masterModule
    {
        public int moduleID { get; set; }

        public ToolStripMenuItem menuItem { get; set; }
    }

    public class gridColumns
    {
        public string gridName { get; set; }
        public string gridHeaderText { get; set; }
        public int gridColumnType { get; set; }
        public bool gridColumnVisible { get; set; }
        public bool gridColumnReadOnly { get; set; }
    }

    public class cbDataSource
    {
        public string valueMember { get; set; }
        public string displayMember { get; set; }
    }

    public class gridRowsValue
    {
        public List<object> gridValue;
    }
    
    public class userKavling
    {
        public string userName { get; set; }
        public DateTime startIPL { get; set; }
        public string remarkValue { get; set; }
        public string qrCodeValue { get; set; }
        public string qrFlagValue { get; set; }
    }
   
    public class productInput
    {
        public string productID { get; set; }
        public double qtyInput { get; set; }
        public int warnaID { get; set; }
    }

    public class cfFields
    {
        public int lineID { get; set; }
        public int cfModuleID { get; set; }
        public int cfMandatory { get; set; }
        public int cfID { get; set; }
        public string cfDesc { get; set; }
        public int cfType { get; set; }
        public string cfStrValue { get; set; }
        public double cfNumValue { get; set; }
        public string cfLookupValue { get; set; }

        public cfFields()
        {
            lineID = 0;
            cfModuleID = 0;
            cfMandatory = 0;
            cfID = 0;
            cfDesc = "";
            cfType = 0;
            cfStrValue = "";
            cfNumValue = 0;
            cfLookupValue = "";
        }
    }

    public class ccTicket
    {
        public int history_id { get; set; }
        public string component_name { get; set; }
        public string imgName { get; set; }
        public bool isAdmin { get; set; }
        public TextBox txtBox { get; set; }
        public PictureBox pbBox { get; set; }
    }

    public class kompleksIPL
    {
        public int kavling_id { get; set; }
        public string kavlingName { get; set; }
        public string generateStatus { get; set; }
    }

    public class user_rfid
    {
        public int id { get; set; }
        public string rfid_value { get; set; }
        public int rfid_enabled { get; set; }
        public string is_active { get; set; }
    }

    public class userKavlingRFID
    {
        public int kavling_id { get; set; }
        public int user_id { get; set; }
        public List<user_rfid> userRFID;
    }
}
