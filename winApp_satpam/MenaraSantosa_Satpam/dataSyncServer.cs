﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.IO;

namespace AlphaSoft
{
    public partial class dataSyncServer : basicHotkeysForm
    {
        private CultureInfo culture = new CultureInfo("id-ID");

        private globalRestAPI gRest;
        private globalUserUtil gUser;

        public dataSyncServer()
        {
            InitializeComponent();

            gUser = new globalUserUtil();
            gRest = new globalRestAPI();

            gRest.loadURL();

            getServerStatus();
        }

        private void getServerStatus()
        {
            if (gRest.getLiveServerStatus("037f6587a54504d3f512881c7ce30209"))
            {
                connStatusLabel.Text = "SERVER ONLINE";
                connStatusLabel.ForeColor = Color.Green;
            }
            else
            {
                connStatusLabel.Text = "SERVER OFFLINE";
                connStatusLabel.ForeColor = Color.Red;
            }
        }

        private void timerStatus_Tick(object sender, EventArgs e)
        {
            getServerStatus();
            refreshDevSyncStatus();
        }

        private void dataSyncServer_Load(object sender, EventArgs e)
        {
            int durationValue = 0;
            string sqlCommand = "";
            genericReply replyResult = new genericReply();

            sqlCommand = "SELECT server_pull_duration AS resultQuery " +
                                    "FROM sync_status " +
                                    "WHERE id = 1";
            try
            {
                if (gRest.getDataSingleValueNoParam(sqlCommand, ref replyResult))
                {
                    if (replyResult.o_status == 1)
                    {
                        durationValue = Convert.ToInt32(replyResult.data);
                    }
                }
            }
            catch (Exception ex)
            {
                durationValue = 0;
            }

            numericUpDown1.Value = durationValue;

            if (durationValue > 0)
            {
                timerPullServer.Stop();
                timerPullServer.Interval = durationValue * 60000;
                timerPullServer.Start();
            }
            else
            {
                timerPullServer.Stop();
                timerPullServer.Interval = 60 * 60000;
                timerPullServer.Start();
            }

            timerStatus.Start();
            timerPullServer.Start();

            refreshServerSyncStatus();
            refreshDevSyncStatus();

            durationTextBox.Enter += TextBox_Enter;
            durationTextBox.Leave += TextBox_Int32_Leave;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            int durationValue = 0;

            //if (durationTextBox.Text.Length <= 0 || 
            //    !int.TryParse(durationTextBox.Text, NumberStyles.Number, culture, out durationValue))
            if (numericUpDown1.Value <= 0)
            {
                MessageBox.Show("Input frekuensi salah");
            }
            else
            {
                durationValue = Convert.ToInt32(numericUpDown1.Value);
                if (gRest.savePullDuration("037f6587a54504d3f512881c7ce30209", durationValue) && durationValue > 0)
                {
                    timerPullServer.Stop();
                    timerPullServer.Interval = durationValue * 60000;
                    timerPullServer.Start();

                    MessageBox.Show("DONE");
                }
                else
                {
                    MessageBox.Show("Input frekuensi salah");
                }
            }
        }

        private void refreshServerSyncStatus()
        {
            string sqlCommand = "";
            genericReply replyResult = new genericReply();

            sqlCommand = "SELECT DATE_FORMAT(server_last_sync, '%d %M %Y %H:%i') AS resultQuery " +
                                    "FROM sync_status " +
                                    "WHERE id = 1";
            try
            {
                lastServerConnLabel.Text = "";
                if (gRest.getDataSingleValueNoParam(sqlCommand, ref replyResult))
                {
                    if (replyResult.o_status == 1)
                    {
                        lastServerConnLabel.Text = replyResult.data.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                lastServerConnLabel.Text = "-";
            }
        }

        private void refreshDevSyncStatus()
        {
            string sqlCommand = "";
            genericReply replyResult = new genericReply();

            sqlCommand = "SELECT DATE_FORMAT(device_last_sync, '%d %M %Y %H:%i') AS resultQuery " +
                                    "FROM sync_status " +
                                    "WHERE id = 1";
            try
            {
                lastDeviceConnLabel.Text = "";
                if (gRest.getDataSingleValueNoParam(sqlCommand, ref replyResult))
                {
                    if (replyResult.o_status == 1)
                    {
                        lastDeviceConnLabel.Text = replyResult.data.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                lastDeviceConnLabel.Text = "-";
            }
        }

        private void getServerData()
        {
            List<server_user_all> listUser = new List<server_user_all>();
            List<kompleks_terminal> listTerminal = new List<kompleks_terminal>();

            gRest.getLiveServerUserList("037f6587a54504d3f512881c7ce30209", ref listUser);
            gRest.getLiveKompleksTerminal("037f6587a54504d3f512881c7ce30209", ref listTerminal);

            gRest.truncateUserList("037f6587a54504d3f512881c7ce30209");
            gRest.saveUserList("037f6587a54504d3f512881c7ce30209", listUser);

            gRest.truncateTerminalData("037f6587a54504d3f512881c7ce30209");
            gRest.saveTerminalData("037f6587a54504d3f512881c7ce30209", listTerminal);

            refreshServerSyncStatus();
        }

        private void syncButton_Click(object sender, EventArgs e)
        {
            getServerData();
        }

        private void timerPullServer_Tick(object sender, EventArgs e)
        {
            getServerData();
        }
    }
}
