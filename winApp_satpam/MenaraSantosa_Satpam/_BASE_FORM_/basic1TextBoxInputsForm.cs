﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Globalization;

namespace AlphaSoft
{
    public partial class basic1TextBoxInputsForm : basicHotkeysForm
    {
        private globalUtilities gUtil = new globalUtilities();
        private CultureInfo culture = new CultureInfo("id-ID");
        private int moduleID = 0;

        public string returnValue = "";
        public bool valuePass = false;

        private void TextBox_Enter(object sender, EventArgs e)
        {
            TextBox txtBox;
            txtBox = (TextBox)sender;

            if (txtBox.Text.Length > 0)
            {
                double tempValue = 0;

                if (double.TryParse(txtBox.Text, NumberStyles.Number, culture, out tempValue))
                {
                    txtBox.Text = tempValue.ToString(culture);
                }
            }
        }

        private void TextBox_LEAVE(object sender, EventArgs e)
        {
            TextBox txtBox;
            txtBox = (TextBox)sender;

            if (txtBox.Text.Length > 0)
            {
                double tempValue = 0;

                if (double.TryParse(txtBox.Text, NumberStyles.Number, culture, out tempValue))
                {
                    txtBox.Text = tempValue.ToString("N2", culture);
                }
            }
        }


        public basic1TextBoxInputsForm(string labelValue = "INPUT", string inputValue = "", int maxLength = 200)
        {
            InitializeComponent();

            label2.Text = labelValue;
            namaTextBox.Text = inputValue;
            namaTextBox.MaxLength = maxLength;
        }

        public basic1TextBoxInputsForm(int moduleIDParam = globalConstants.MODULE_DEFAULT, 
            string labelValue = "INPUT", string inputValue = "", int maxLength = 200)
        {
            InitializeComponent();

            label2.Text = labelValue;
            namaTextBox.Text = inputValue;
            namaTextBox.MaxLength = maxLength;

            moduleID = moduleIDParam;

            namaTextBox.Enter += TextBox_Enter;
            namaTextBox.Leave += TextBox_LEAVE;
        }

        private void basic1TextBoxInputsForm_Load(object sender, EventArgs e)
        {
            gUtil.reArrangeTabOrder(this);
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            errorLabel.Text = "";

            if (moduleID == 0)
            {
                returnValue = namaTextBox.Text;
                this.Close();
            }
        }

        private void basic1TextBoxInputsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (namaTextBox.Text.Length <= 0)
            {
                if (DialogResult.Yes == MessageBox.Show("INPUT KOSONG ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    returnValue = "";
                else
                    e.Cancel = true;
            }
        }
    }
}
