﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Hotkeys;
using System.Globalization;
//using MySql.Data;
//using MySql.Data.MySqlClient;

namespace AlphaSoft
{
    public partial class basicHotkeysForm : Form
    {
        private DateTime localDate = DateTime.Now;
        private CultureInfo culture = new CultureInfo("id-ID");

        private Hotkeys.GlobalHotkey ghk_UP;
        private Hotkeys.GlobalHotkey ghk_DOWN;
        private Hotkeys.GlobalHotkey ghk_ESC;

        private bool navKeyRegistered = false;

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == Constants.WM_HOTKEY_MSG_ID)
            {
                Keys key = (Keys)(((int)m.LParam >> 16) & 0xFFFF);
                int modifier = (int)m.LParam & 0xFFFF;

                if (modifier == Constants.NOMOD)
                    captureAll(key);
            }

            base.WndProc(ref m);
        }

        protected virtual void captureAll(Keys key)
        {
            switch (key)
            {
                case Keys.Up:
                    SendKeys.Send("+{TAB}");
                    break;
                case Keys.Down:
                    SendKeys.Send("{TAB}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }
       
        protected virtual void registerGlobalHotkey()
        {
            ghk_UP = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.Up, this);
            ghk_UP.Register();

            ghk_DOWN = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.Down, this);
            ghk_DOWN.Register();

            ghk_ESC = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.Escape, this);
            ghk_ESC.Register();

            navKeyRegistered = true;
        }

        protected virtual void unregisterGlobalHotkey()
        {
            ghk_UP.Unregister();
            ghk_DOWN.Unregister();
            ghk_ESC.Unregister();

            navKeyRegistered = false;
        }

        public void genericControl_Enter(object sender, EventArgs e)
        {
            if (navKeyRegistered)
                unregisterGlobalHotkey();
        }

        public void genericControl_Leave(object sender, EventArgs e)
        {
            registerGlobalHotkey();
        }

        public basicHotkeysForm()
        {
            InitializeComponent();
        }

        //private void populateLocationCombo()
        //{
        //    Data_Access DS = new Data_Access();
        //    string sqlCommand = "";
        //    DataTable dt = new DataTable();
        //    MySqlDataReader rdr;

        //    //DS.sqlConnect();

        //    sqlCommand = "SELECT LOCATION_ID, LOCATION_NAME FROM MASTER_LOCATION WHERE LOCATION_ACTIVE = 1";
        //    using (rdr = DS.getData(sqlCommand))
        //    {
        //        if (rdr.HasRows)
        //            dt.Load(rdr);
        //    }
        //    rdr.Close();

        //    toolStripLocationCombo.ComboBox.DataSource = dt;
        //    toolStripLocationCombo.ComboBox.DisplayMember = "LOCATION_ID";
        //    toolStripLocationCombo.ComboBox.ValueMember = "LOCATION_ID";

        //    sqlCommand = "SELECT ";
        //}

        private void basicHotkeysForm_Load(object sender, EventArgs e)
        {
            errorLabel.Text = "";
            updateLabel();
            timerStatusLabel.Start();

            //populateLocationCombo();
        }

        private void updateLabel()
        {
            localDate = DateTime.Now;
            timeStampStatusLabel.Text = String.Format(culture, "{0:dddd, dd-MM-yyyy - HH:mm}", localDate);
        }

        private void timerStatusLabel_Tick(object sender, EventArgs e)
        {
            updateLabel();
        }

        private void basicHotkeysForm_Activated(object sender, EventArgs e)
        {
            registerGlobalHotkey();
        }

        private void basicHotkeysForm_Deactivate(object sender, EventArgs e)
        {
            unregisterGlobalHotkey();
        }

        public void TextBox_Enter(object sender, EventArgs e)
        {
            TextBox txtBox;
            txtBox = (TextBox)sender;

            if (txtBox.Text.Length > 0)
            {
                double tempValue = 0;

                if (double.TryParse(txtBox.Text, NumberStyles.Number, culture, out tempValue))
                {
                    txtBox.Text = tempValue.ToString(culture);
                }
            }
        }

        public void TextBox_Leave(object sender, EventArgs e)
        {
            TextBox txtBox;
            txtBox = (TextBox)sender;

            if (txtBox.Text.Length > 0)
            {
                double tempValue = 0;

                if (double.TryParse(txtBox.Text, NumberStyles.Number, culture, out tempValue))
                {
                    txtBox.Text = tempValue.ToString("N2", culture);
                }
            }
        }

        public void TextBox_Int32_Leave(object sender, EventArgs e)
        {
            TextBox txtBox;
            txtBox = (TextBox)sender;

            if (txtBox.Text.Length > 0)
            {
                double tempValue = 0;

                if (double.TryParse(txtBox.Text, NumberStyles.Number, culture, out tempValue))
                {
                    txtBox.Text = tempValue.ToString("N0", culture);
                }
            }
        }
    }
}
