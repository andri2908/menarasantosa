﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Hotkeys;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace AlphaSoft
{
    public partial class basicPersonDataForm : basicHotkeysForm
    {
        private Hotkeys.GlobalHotkey ghk_F5;
        private Hotkeys.GlobalHotkey ghk_F9;
        private globalUtilities gUtil = new globalUtilities();

        protected int selectedBankID = 0;
        protected int selectedBankRowIndex = -1;

        public basicPersonDataForm()
        {
            InitializeComponent();
        }

        protected virtual void setSaveButtonEnable(bool enableValue)
        {
            SaveButton.Enabled = enableValue;
        }

        protected virtual void setResetButtonEnable(bool enableValue)
        {
            ResetButton.Enabled = enableValue;
        }

        protected virtual void additionalProcessSave() { }

        private void basicPersonDataForm_Load(object sender, EventArgs e)
        {
            gUtil.reArrangeTabOrder(this);

            for (int i = 0;i<tabControl1.TabPages.Count;i++)
                gUtil.reArrangeTabOrder(tabControl1.TabPages[i], 1); 
        }

        private bool saveDataAction()
        {
            if (dataValidated())
            {
                return saveDataTransaction();
            }

            return false;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (saveDataAction())
            {
                additionalProcessSave();

                if (DialogResult.Yes == MessageBox.Show("SUCCESS, INPUT LAGI ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    ResetButton.PerformClick();
                else
                    this.Close();
            }
            else
                MessageBox.Show("FAIL");
        }

        protected override void captureAll(Keys key)
        {
            base.captureAll(key);

            switch (key)
            {
                case Keys.F5:
                    ResetButton.PerformClick();
                    break;

                case Keys.F9:
                    SaveButton.PerformClick();
                    break;
            }
        }

        protected override void registerGlobalHotkey()
        {
            base.registerGlobalHotkey();

            ghk_F5 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F5, this);
            ghk_F5.Register();

            ghk_F9 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F9, this);
            ghk_F9.Register();
        }

        protected override void unregisterGlobalHotkey()
        {
            ghk_F5.Unregister();
            ghk_F9.Unregister();
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            gUtil.ResetAllControls(this);
        }

        protected virtual bool saveDataTransaction()
        {
            return true;
        }

        protected virtual bool dataValidated()
        {
            if (namaTextBox.Text.Length <= 0)
            {
                errorLabel.Text = "INPUT NAMA KOSONG";
                return false;
            }

            if (alamat1TextBox.Text.Length <= 0)
            {
                errorLabel.Text = "ALAMAT KOSONG";
                return false;
            }

            if (teleponTextBox.Text.Length <= 0)
            {
                errorLabel.Text = "TELEPON KOSONG";
                return false;
            }

            errorLabel.Text = "";
            return true;
        }

        protected virtual void disableInput()
        {
            namaTextBox.Enabled = false;
            alamat1TextBox.Enabled = false;
            alamat2TextBox.Enabled = false;
            kotaTextBox.Enabled = false;
            teleponTextBox.Enabled = false;
            handphoneTextBox.Enabled = false;
            faxTextBox.Enabled = false;
            emailTextBox.Enabled = false;
            nonAktifCheckbox.Enabled = false;
            SaveButton.Enabled = false;
            ResetButton.Enabled = false;
        }
       
        private void searchButton_Click(object sender, EventArgs e)
        {
            /*dataBankForm searchBankForm = new dataBankForm(globalConstants.MODULE_DEFAULT);
            searchBankForm.ShowDialog(this);

            selectedBankID = Convert.ToInt32(searchBankForm.ReturnValue1);

            if (selectedBankID > 0)
            {
                bankTextBox.Text = searchBankForm.ReturnValue2;
                addButton.Enabled = true;
            }
            */
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            /*if (selectedBankRowIndex < 0)
            {
                dataGridViewBank.Rows.Add(0, selectedBankID, bankTextBox.Text, noRekTextBox.Text, namaRekTextBox.Text);
            }
            else
            {
                dataGridViewBank.Rows[selectedBankRowIndex].Cells["bankID"].Value = selectedBankID;
                dataGridViewBank.Rows[selectedBankRowIndex].Cells["bankName"].Value = bankTextBox.Text;
                dataGridViewBank.Rows[selectedBankRowIndex].Cells["bankAccount"].Value = noRekTextBox.Text;
                dataGridViewBank.Rows[selectedBankRowIndex].Cells["bankAccountName"].Value = namaRekTextBox.Text;

                addButton.Text = "ADD";
            }

            selectedBankID = 0;
            selectedBankRowIndex = -1;
            bankTextBox.Clear();
            noRekTextBox.Clear();
            namaRekTextBox.Clear();

            addButton.Enabled = false;
            */
        }

        private void dataGridViewBank_DoubleClick(object sender, EventArgs e)
        {
            /*if (dataGridViewBank.Rows.Count <= 0)
                return;

            int selectedrowindex = dataGridViewBank.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dataGridViewBank.Rows[selectedrowindex];

            selectedBankRowIndex = selectedrowindex;
            selectedBankID = Convert.ToInt32(selectedRow.Cells["bankID"].Value);
            bankTextBox.Text = selectedRow.Cells["bankName"].Value.ToString();
            noRekTextBox.Text = selectedRow.Cells["bankAccount"].Value.ToString();
            namaRekTextBox.Text = selectedRow.Cells["bankAccountName"].Value.ToString();

            addButton.Text = "UPDATE";
            addButton.Enabled = true;

            searchButton.Focus();
            */
        }

        private void dataGridViewBank_KeyDown(object sender, KeyEventArgs e)
        {
            /*if (dataGridViewBank.Rows.Count <= 0)
                return;

            int selectedrowindex = dataGridViewBank.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dataGridViewBank.Rows[selectedrowindex];

            if (e.KeyCode == Keys.Delete)
            {
                if (DialogResult.Yes == MessageBox.Show("HAPUS DATA REKENING?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    selectedRow.Cells["bankID"].Value = 0;
                    selectedRow.Visible = false;
                }
            }
            */
        }
    }
}
