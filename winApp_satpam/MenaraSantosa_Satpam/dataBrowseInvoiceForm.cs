﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace AlphaSoft
{
    public partial class dataBrowseInvoiceForm : AlphaSoft.browseRangeTextBoxForm
    {
        private Data_Access DS = new Data_Access();
        private globalUserUtil gUser;

        private int originModuleID = 0;

        public dataBrowseInvoiceForm(int moduleID = 0)
        {
            InitializeComponent();

            DS.sqlConnect();

            this.Text = "BROWSE INVOICE";

            gUser = new globalUserUtil(DS);

            originModuleID = moduleID;
        }

        protected override void displayAllAction()
        {
            MySqlDataReader rdr;
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string paramName = "";
            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now;
            dataGridView.DataSource = null;

            List<string> pList = new List<string>();
            List<object> pVal = new List<object>();

            paramName = MySqlHelper.EscapeString(getNamaTextBox());
            startDate = startDTPicker.Value;
            endDate = endDTPicker.Value;

            pList.Add("@dtStart");
            pList.Add("@dtEnd");

            pVal.Add(startDate.Date);
            pVal.Add(endDate.Date);

            sqlCommand = "SELECT " +
                                    "DI.INVOICE_ID AS 'NO INV', DATE_FORMAT(DI.INVOICE_DATETIME, '%d-%M-%Y') AS 'TGL INV', " +
                                    "DI.RPJ_ID AS 'NO RPJ', " +
                                    "MC.CONTACT_NAME AS CUSTOMER, MU.USER_NAME AS MARKETING " +
                                    "FROM DATA_INVOICE DI, " +
                                    "RPJ_HEADER RH " +
                                    "LEFT OUTER JOIN MASTER_CONTACT MC ON (RH.CONTACT_ID = MC.CONTACT_ID) " +
                                    "LEFT OUTER JOIN MASTER_USER MU ON (RH.MARKETING_ID = MU.ID) " +
                                    "WHERE DATE(DI.INVOICE_DATETIME) >= @dtStart " +
                                    "AND DATE(DI.INVOICE_DATETIME) <= @dtEnd " +
                                    "AND RH.RPJ_ID = DI.RPJ_ID";

            if (paramName.Length > 0)
            {
                sqlCommand = sqlCommand + " AND MC.CONTACT_NAME LIKE '%@paramName%' ";

                pList.Add("@paramName");
                pVal.Add(paramName);
            }

            if (!nonActiveCheckBox.Checked)
                sqlCommand = sqlCommand + " AND DI.INVOICE_STATUS = 1";

            sqlCommand = sqlCommand + " ORDER BY DI.INVOICE_DATETIME ASC";

            dataGridView.DataSource = null;
            using (rdr = DS.getDataWithParam(sqlCommand, pList, pVal))
            {
                if (rdr.HasRows)
                {
                    dt.Load(rdr);
                    dataGridView.DataSource = dt;
                }
            }
            rdr.Close();
        }

        private void dataBrowseInvoiceForm_Load(object sender, EventArgs e)
        {
            displayAllAction();

            if (!gUser.userHasAccessTo(MMConstants.TAMBAH_INVOICE,
                MMConstants.HAK_AKSES))
                setNewButtonEnable(false);
        }

        protected override void newButtonAction()
        {
        }

        private void printKartuKuning(string invIDparam)
        {
            string sqlCommand = "";
            List<sqlFields> listSql = new List<sqlFields>();

            int kartuKuningFlag = 0;
            sqlCommand = "SELECT KARTU_KUNING FROM DATA_INVOICE WHERE INVOICE_ID = '" + invIDparam + "'";
            kartuKuningFlag = Convert.ToInt32(DS.getDataSingleValue(sqlCommand));

            if (kartuKuningFlag == 1)
            {
                if (DialogResult.No == MessageBox.Show("KARTU KUNING SUDAH PERNAH DIPRINT, LANJUTKAN ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    return;
            }

            sqlFields sqlElement = new sqlFields();
            sqlElement.fieldName = "INVOICE_ID";
            sqlElement.fieldValue = invIDparam;

            listSql.Add(sqlElement);

            RPT_containerForm displayReportForm = new RPT_containerForm(DS, globalReportConstants.PRINTOUT_KARTU_KUNING,
                sqlCommand, listSql);
            displayReportForm.ShowDialog(this);

            #region UPDATE FLAG KARTU KUNING
            MySqlException inEx = null;

            DS.beginTransaction();
            try
            {
                sqlCommand = "UPDATE DATA_INVOICE SET KARTU_KUNING = 1 WHERE INVOICE_ID = '" + invIDparam + "'";
                if (!DS.executeNonQueryCommand(sqlCommand, ref inEx))
                    throw inEx;

                DS.commit();
            }
            catch (Exception ex) { }
            #endregion
        }

        protected override void dbGridDoubleClickAction(DataGridViewRow selectedRow)
        {
            string selectedID = selectedRow.Cells["NO INV"].Value.ToString();

            switch (originModuleID)
            {
                case globalConstants.MODULE_DEFAULT:
                    ReturnValue1 = selectedID;
                    this.Close();
                    break;

                default:
                    break;
            }
        }
    }
}
