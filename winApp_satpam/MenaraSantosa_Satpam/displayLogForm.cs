﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

using DBGridExtension;

namespace AlphaSoft
{
    public partial class displayLogForm : Form
    {
        private globalRestAPI gRest;
        private globalUserUtil gUser;
        private int currentRecord = 0;
        private int lastID = 0;

        private CultureInfo culture = new CultureInfo("id-ID");

        public displayLogForm()
        {
            InitializeComponent();

            gUser = new globalUserUtil();
            gRest = new globalRestAPI();

            gRest.loadURL();

            latestGridView.DoubleBuffered(true);
            logGridView.DoubleBuffered(true);
        }

        private int getNumRecord()
        {
            int numRecord = 0;
            string sqlQuery = "";
            genericReply replyResult = new genericReply();

            sqlQuery = "SELECT COUNT(1) AS resultQuery  FROM satpam_log";

            try
            {
                if (gRest.getDataSingleValueNoParam(sqlQuery, ref replyResult))
                {
                    if (replyResult.o_status == 1)
                    {
                        numRecord = Convert.ToInt32(replyResult.data);
                    }
                }
            }
            catch (Exception ex) { }

            return numRecord;
        }

        private void loadSingleNewData()
        {
            string msgStatus = "";
            int statusAkses = 0;
            DataTable dt = new DataTable();
            string sqlCommand = "";
            REST_satpamLog satpamData = new REST_satpamLog();

            string startDate = String.Format(culture, "{0:yyyyMMdd}", DateTime.Now);

            sqlCommand = "SELECT * " +
                                    "FROM satpam_log " +
                                    "WHERE id > " + lastID + " ";

            if (lastID <= 0)
            {
                sqlCommand += "AND DATE_FORMAT(timestamp, '%Y%m%d') >= '" + startDate + "' ";
            }

            sqlCommand += "ORDER BY timestamp ASC LIMIT 1";
            if (gRest.getSatpamLog(sqlCommand, ref satpamData))
            {
                if (satpamData.dataStatus.o_status == 1)
                {
                    for (int i = 0; i < satpamData.dataList.Count; i++)
                    {
                        statusAkses = satpamData.dataList[i].access_status;
                        msgStatus = (statusAkses == 1 ? "Welcome...." : (statusAkses == 2 ? "Terlambat IPL...." : "Blocked...."));

                        displayPopUpForm displayForm = new displayPopUpForm(
                                 satpamData.dataList[i].user_full_name,
                                 gUser.getCustomStringFormatDate(satpamData.dataList[i].timestamp, false, true),
                                 satpamData.dataList[i].access_id,
                                 msgStatus
                                 );
                        displayForm.ShowDialog(this);
                    }
                }
            }
        }

        private void loadDataGrid()
        {
            genericReply replyResult = new genericReply();
            int numRecord = 0;
            string msgStatus = "";
            int statusAkses = 0;
            DataTable dt = new DataTable();
            string sqlCommand = "";
            REST_satpamLog satpamData = new REST_satpamLog();

            string startDate = String.Format(culture, "{0:yyyyMMdd}", DateTime.Now);

            sqlCommand = "SELECT COUNT(1) as resultQuery " +
                                    "FROM satpam_log " +
                                    "WHERE id > " + lastID + " ";

            if (lastID <= 0)
            {
                sqlCommand += "AND DATE_FORMAT(timestamp, '%Y%m%d') >= '" + startDate + "' ";
            }

            if (gRest.getDataSingleValueNoParam(sqlCommand, ref replyResult))
            {
                if (replyResult.o_status == 1)
                {
                    numRecord = Convert.ToInt32(replyResult.data);
                }
            }

            if (numRecord <= 0)
                return;

            sqlCommand = "SELECT sl.*, IFNULL(kt.terminal_type, 'TIDAK TERDAFTAR') as terminal_type " +
                                    "FROM satpam_log sl " +
                                    "LEFT OUTER JOIN kompleks_terminal kt ON (sl.terminal_id = kt.terminal_id) " +
                                    "WHERE sl.id > " + lastID + " ";

            if (lastID <= 0)
            {
                sqlCommand += "AND DATE_FORMAT(timestamp, '%Y%m%d') >= '" + startDate + "' ";
            }

            sqlCommand += "ORDER BY timestamp ASC";

            if (gRest.getSatpamLog(sqlCommand, ref satpamData))
            {
                if (satpamData.dataStatus.o_status == 1)
                {
                    if (satpamData.dataList.Count > 0)
                        lastID = satpamData.dataList[satpamData.dataList.Count-1].id;

                    for (int i = 0; i < satpamData.dataList.Count; i++)
                    {
                        statusAkses = satpamData.dataList[i].access_status;
                        msgStatus = (statusAkses == 1 ? "Welcome...." : (statusAkses == 2 ? "Terlambat IPL...." : "Blocked...."));

                        logGridView.Rows.Insert(0,
                            gUser.getCustomStringFormatDate(satpamData.dataList[i].timestamp, false, true),
                            satpamData.dataList[i].terminal_id,
                            satpamData.dataList[i].terminal_type,
                            satpamData.dataList[i].access_id,
                            satpamData.dataList[i].user_full_name,
                            msgStatus
                            );

                        if ((satpamData.dataList[i].user_full_name.ToUpper() == "NOT_IN_DATABASE") ||
                            msgStatus.ToUpper().IndexOf("WELCOME") < 0)
                        {
                            logGridView.Rows[0].DefaultCellStyle.BackColor = Color.Red;
                        }
                    }

                    if (satpamData.dataList.Count > 0)
                    {
                        if (latestGridView.Rows.Count > 0)
                        {
                            latestGridView.Rows[0].Cells[0].Value = gUser.getCustomStringFormatDate(satpamData.dataList[satpamData.dataList.Count - 1].timestamp, false, true);
                            latestGridView.Rows[0].Cells[1].Value = satpamData.dataList[satpamData.dataList.Count - 1].access_id;
                            latestGridView.Rows[0].Cells[2].Value = satpamData.dataList[satpamData.dataList.Count - 1].user_full_name;
                            latestGridView.Rows[0].Cells[3].Value = msgStatus;
                        }
                        else
                        {
                            latestGridView.Rows.Add(
                                gUser.getCustomStringFormatDate(satpamData.dataList[satpamData.dataList.Count - 1].timestamp, false, true),
                                satpamData.dataList[satpamData.dataList.Count - 1].access_id,
                                satpamData.dataList[satpamData.dataList.Count - 1].user_full_name,
                                msgStatus
                                );
                        }

                        if ((satpamData.dataList[satpamData.dataList.Count - 1].user_full_name.ToUpper() == "NOT_IN_DATABASE") ||
                               msgStatus.ToUpper().IndexOf("WELCOME") < 0)
                        {
                            latestGridView.Rows[0].DefaultCellStyle.BackColor = Color.Red;
                        }
                        else
                        {
                            latestGridView.Rows[0].DefaultCellStyle.BackColor = Color.White;
                        }
                    }

                    currentRecord = getNumRecord();
                }
            }

            if (latestGridView.Rows.Count > 4)
                latestGridView.Rows.RemoveAt(3);
        }

        private void displayLogForm_Load(object sender, EventArgs e)
        {
            loadDataGrid();

            timerRefreshLog.Start();
            timerRefreshAll.Start();
        }

        private void timerRefreshLog_Tick(object sender, EventArgs e)
        {
            int tempRecord = getNumRecord();

            if (tempRecord > currentRecord)
            {
                loadDataGrid();
            }
        }

        private void logGridView_DoubleClick(object sender, EventArgs e)
        {
            if (logGridView.Rows.Count > 0)
            {
                int selectedrowindex = logGridView.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = logGridView.Rows[selectedrowindex];

                displayPopUpForm displayForm = new displayPopUpForm(
                               selectedRow.Cells["userFullName"].Value.ToString(),
                               selectedRow.Cells["timestamp"].Value.ToString(), //gUser.getCustomStringFormatDate(satpamData.dataList[i].timestamp, false, true),
                               selectedRow.Cells["cardNo"].Value.ToString(), //satpamData.dataList[i].access_id,
                               selectedRow.Cells["message"].Value.ToString() //
                               );
                displayForm.Show();// Dialog(this);
            }
        }

        private void getLiveUserAccessList()
        {
            //List<server_userList> listUser = new List<server_userList>();

            //gRest.getLiveServerUserList("037f6587a54504d3f512881c7ce30209", ref listUser);
            //gRest.truncateUserList("037f6587a54504d3f512881c7ce30209");
            //gRest.saveUserList("037f6587a54504d3f512881c7ce30209", listUser);
        }

        private void timerRefreshAll_Tick(object sender, EventArgs e)
        {
            lastID = 0;
            logGridView.Rows.Clear();
            loadDataGrid();
        }
    }
}
