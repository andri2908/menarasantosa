﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//using Microsoft.SqlServer;

using MySql.Data;
using MySql.Data.MySqlClient;

using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace AlphaSoft
{
    class Data_Access
    {
        public const string userName = "SYS_POS_ADMIN";//"menarasa_alphasoft_adminSQL";
        public const string password = "pass123";//"k6XoysFHgOia";//
        public const string databaseName = "MENARA_SANTOSA";//"menarasa_sys_residential";//

        private static string ipServer = "";
        private string configFile = Application.StartupPath + "\\conf\\pos.cfg";

#if true
        // MYSQL
        private static MySqlConnection conn = new MySqlConnection();
        private static MySqlConnection transConnection;
        private MySqlTransaction myTrans;
        private MySqlCommand myTransCommand;
        private static MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();
#endif

#if false
        // SQL SERVER
        private static SqlConnection conn = new SqlConnection();
        private static SqlConnection transConnection;
        private SqlTransaction myTrans;
        private SqlCommand myTransCommand;
        private static SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
#endif

        public bool setIPServer()
        {
            string s = "";

            if (File.Exists(configFile))
            {
                using (StreamReader sr = File.OpenText(configFile))
                {
                    if ((s = sr.ReadLine()) != null)
                    {
                        ipServer = s;
                    }
                }
                return true;
            }

            return false;
        }

        public void setIPServer(string s)
        {
            ipServer = s;
        }

        public string getIPServer()
        {
            return ipServer;
        }

#if true // MySQL
        public bool sqlConnect()
        {
            if (conn.State.ToString() != "Open")
            {
                try
                {
                    conn.ConnectionString = builder.ConnectionString;
                    conn.Open();

                    return true;
                }
                catch (MySql.Data.MySqlClient.MySqlException ex)
                {
                    return false;
                }
            }

            return true;
        }

        public void sqlClose()
        {
            if (null != conn)
            {
                conn.Close();
            }
        }

        public void buildConnString()
        {
            builder.Server = ipServer;
            builder.UserID = userName;
            builder.Password = password;
            builder.Database = databaseName;
        }

        public bool firstSqlConnect()
        {
            if (conn.State.ToString() != "Open")
            {
                try
                {
                    if (setIPServer())
                    {
                        buildConnString();
                        conn.ConnectionString = builder.ConnectionString;
                        conn.Open();

                        return true;
                    }
                    else
                        return false;
                }
                catch (MySqlException ex)
                {
                    return false;
                }
            }

            return true;
        }

        public bool testConnection(ref MySqlException returnE)
        {
            try
            {
                buildConnString();
                conn.ConnectionString = builder.ConnectionString;
                conn.Open();

                return true;
            }
            catch (MySqlException ex)
            {
                returnE = ex;
                return false;
            }
        }

        public object getDataSingleValue(string sqlCommand)
        {
            MySqlCommand command;

            if (conn.State.ToString() != "Open")
                sqlConnect();

            command = new MySqlCommand(sqlCommand, conn);
            object result = command.ExecuteScalar();
            /*if (result == null)
            {
                result = "BLANK";
            }*/
            return result;
        }

        public object getDataSingleValueWithParam(string sqlCommand, List<string> paramList, List<object> paramValues)
        {
            MySqlCommand command;

            if (conn.State.ToString() != "Open")
                sqlConnect();

            command = new MySqlCommand(sqlCommand, conn);

            for (int i = 0; i < paramList.Count(); i++)
                command.Parameters.AddWithValue(paramList[i], paramValues[i]);

            object result = command.ExecuteScalar();

            return result;
        }

        public MySqlDataReader getData(string sqlCommand)
        {
            MySqlCommand cmd = null;
            MySqlDataReader rdr;

            if (conn.State.ToString() != "Open")
                sqlConnect();

            cmd = new MySqlCommand(sqlCommand, conn);

            rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            return rdr;
        }

        public MySqlDataReader getDataWithParam(string sqlCommand, List<string> paramList, List<object> paramValues)
        {
            MySqlCommand cmd = null;
            MySqlDataReader rdr;

            if (conn.State.ToString() != "Open")
                sqlConnect();

            cmd = new MySqlCommand(sqlCommand, conn);

            for (int i = 0; i < paramList.Count(); i++)
                cmd.Parameters.AddWithValue(paramList[i], paramValues[i]);

            rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            return rdr;
        }

        public void getDataStoredProcWithParam(string storedProcName, List<string> paramList, List<object> paramValues, 
            List<string> paramOutList, ref List<string> paramOutVal)
        {
            MySqlCommand cmd = null;
            int i = 0;

            if (conn.State.ToString() != "Open")
                sqlConnect();

            cmd = new MySqlCommand();

            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = storedProcName;

            for (i = 0; i < paramList.Count(); i++)
                cmd.Parameters.AddWithValue(paramList[i], paramValues[i]);

            cmd.Parameters.Add("@o_status", MySqlDbType.Int16, 3).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@o_message", MySqlDbType.VarChar, 200).Direction = ParameterDirection.Output;

            for (i = 0; i < paramOutList.Count(); i++)
                cmd.Parameters.Add(paramOutList[i], MySqlDbType.VarChar, 50).Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            paramOutVal.Add(cmd.Parameters["@o_status"].Value.ToString());
            paramOutVal.Add(cmd.Parameters["@o_message"].Value.ToString());

            for (i = 0; i < paramOutList.Count(); i++)
                paramOutVal.Add(cmd.Parameters[paramOutList[i]].Value.ToString());
        }

        public void beginTransaction()
        {
            transConnection = new MySqlConnection(builder.ConnectionString);

            transConnection.Open();

            myTransCommand = transConnection.CreateCommand();

            // Start a local transaction
            myTrans = transConnection.BeginTransaction();

            // Must assign both transaction object and connection
            // to Command object for a pending local transaction
            myTransCommand.Connection = transConnection;
            myTransCommand.Transaction = myTrans;
        }

        public bool executeNonQueryCommand(string sqlCommand, ref MySqlException returnEx)
        {
            bool retVal = true;
            int temp;

            try
            {
                myTransCommand.CommandText = sqlCommand;
                if (myTransCommand.Connection.State.ToString() != "Open")
                    myTransCommand.Connection.Open();

                temp = myTransCommand.ExecuteNonQuery();

                retVal = true;
            }
            catch (MySqlException ex)
            {
                retVal = false;
                returnEx = ex;
            }

            return retVal;
        }

        public bool executeNonQueryCommandWithParameters(string sqlCommand, ref MySqlException returnEx,
                                List<string> paramList, List<object> paramValues)
        {
            bool retVal = true;
            int temp;

            try
            {
                myTransCommand.CommandText = sqlCommand;

                for (int i = 0; i < paramList.Count(); i++)
                    myTransCommand.Parameters.AddWithValue(paramList[i], paramValues[i]);

                if (myTransCommand.Connection.State.ToString() != "Open")
                    myTransCommand.Connection.Open();

                temp = myTransCommand.ExecuteNonQuery();

                retVal = true;
            }
            catch (MySqlException ex)
            {
                retVal = false;
                returnEx = ex;
            }

            return retVal;
        }

        public void commit()
        {
            myTrans.Commit();
        }

        public Boolean writeXML(string sqlCommand, string filename, List<string> paramList, List<object> paramValues)
        {
            Boolean rslt = false;
            DataSet myData = new DataSet();
            MySqlCommand cmd;
            MySqlDataAdapter myAdapter;

            cmd = new MySqlCommand();
            myAdapter = new MySqlDataAdapter();

            if (conn.State.ToString() != "Open")
                sqlConnect();

            try
            {
                cmd.CommandText = sqlCommand;
                cmd.Connection = conn;

                for (int i = 0; i < paramList.Count(); i++)
                    cmd.Parameters.AddWithValue(paramList[i], paramValues[i]);

                myAdapter.SelectCommand = cmd;
                myAdapter.Fill(myData);

                if (filename.Equals(""))
                {
                    filename = "\\xml\\dataset.xml";
                }
                else
                {
                    filename = "\\xml\\" + filename;
                }

                string appPath = Directory.GetCurrentDirectory() + filename;
                myData.WriteXml(@appPath, XmlWriteMode.WriteSchema);

                rslt = true;
            }
            catch (MySqlException ex)
            {
                rslt = false;
            }
            return rslt;
        }

#endif

#if false //SQL SERVER 
        public bool testConnection(ref SqlException returnE)
        {
            try
            {
                buildConnString();
                conn.ConnectionString = builder.ConnectionString;
                conn.Open();

                return true;
            }
            catch (SqlException ex)
            {
                returnE = ex;
                return false;
            }
        }

        public void buildConnString()
        {
            // Build connection string
            builder.DataSource = ipServer;   
            builder.UserID = userName;
            builder.Password = password;
            builder.InitialCatalog = databaseName;// "master";
        }

        public bool firstSqlConnect()
        {
            if (conn.State.ToString() != "Open")
            {
                try
                {
                    if (setIPServer())
                    {
                        buildConnString();
                        conn.ConnectionString = builder.ConnectionString;
                        conn.Open();

                        return true;
                    }
                    else
                        return false;
                }
                catch (SqlException ex)
                {
                    return false;
                }
            }

            return true;
        }

        public object getDataSingleValue(string sqlCommand)
        {
            SqlCommand command;

            if (conn.State.ToString() != "Open")
                sqlConnect();

            command = new SqlCommand(sqlCommand, conn);
            object result = command.ExecuteScalar();

            return result;
        }

        public SqlDataReader getData(string sqlCommand)
        {
            SqlCommand cmd = null;
            SqlDataReader rdr;

            if (conn.State.ToString() != "Open")
                sqlConnect();

            cmd = new SqlCommand(sqlCommand, conn);

            rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            return rdr;
        }

        public void beginTransaction()
        {
            transConnection = new SqlConnection(builder.ConnectionString);
       
            transConnection.Open();

            myTransCommand = transConnection.CreateCommand();

            // Start a local transaction
            myTrans = transConnection.BeginTransaction();

            // Must assign both transaction object and connection
            // to Command object for a pending local transaction
            myTransCommand.Connection = transConnection;
            myTransCommand.Transaction = myTrans;
        }

        public bool executeNonQueryCommand(string sqlCommand, ref SqlException returnEx)
        {
            bool retVal = true;
            int temp;

            try
            {
                myTransCommand.CommandText = sqlCommand;
                if (myTransCommand.Connection.State.ToString() != "Open")
                    myTransCommand.Connection.Open();

                temp = myTransCommand.ExecuteNonQuery();

                retVal = true;
            }
            catch (SqlException ex)
            {
                retVal = false;
                returnEx = ex;
            }

            return retVal;
        }

        public bool executeNonQueryCommandWithParameters(string sqlCommand, ref SqlException returnEx,
                                                                    List<string> paramList, List<object> paramValues)
        {
            bool retVal = true;
            int temp;

            try
            {
                myTransCommand.CommandText = sqlCommand;

                for (int i = 0;i<paramList.Count();i++)
                    myTransCommand.Parameters.AddWithValue(paramList[i], paramValues[i]);

                if (myTransCommand.Connection.State.ToString() != "Open")
                    myTransCommand.Connection.Open();

                temp = myTransCommand.ExecuteNonQuery();

                retVal = true;
            }
            catch (SqlException ex)
            {
                retVal = false;
                returnEx = ex;
            }

            return retVal;
        }

        public void commit()
        {
            myTrans.Commit();
        }

        public Boolean writeXML(string sqlCommand, string filename, List<string> paramList, List<object> paramValues)
        {
            Boolean rslt = false;
            DataSet myData = new DataSet();
            SqlCommand cmd;
            SqlDataAdapter myAdapter;

            cmd = new SqlCommand();
            myAdapter = new SqlDataAdapter();

            if (conn.State.ToString() != "Open")
                sqlConnect();

            try
            {
                cmd.CommandText = sqlCommand;
                cmd.Connection = conn;

                for (int i = 0; i < paramList.Count(); i++)
                    cmd.Parameters.AddWithValue(paramList[i], paramValues[i]);

                myAdapter.SelectCommand = cmd;
                myAdapter.Fill(myData);

                if (filename.Equals(""))
                {
                    filename = "\\xml\\dataset.xml";
                }
                else
                {
                    filename = "\\xml\\" + filename;
                }

                string appPath = Directory.GetCurrentDirectory() + filename;
                myData.WriteXml(@appPath, XmlWriteMode.WriteSchema);

                rslt = true;
            }
            catch (SqlException ex)
            {
                rslt = false;
            }
            return rslt;
        }
#endif
    }
}
