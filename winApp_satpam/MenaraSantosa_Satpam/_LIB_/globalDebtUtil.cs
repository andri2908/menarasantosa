﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaSoft
{
    class globalDebtUtil
    {
        private Data_Access DS;

        public globalDebtUtil(Data_Access DSParam)
        {
            DS = DSParam;
        }

        public double getTotalSOLumpSumPayment (int status = 0, int contactID = 0, string skipReturID = "")
        {
            double paymentValue = 0;
            string sqlCommand = "";

            sqlCommand = "SELECT SUM(PAYMENT_NOMINAL) " +
                                    "FROM PAYMENT_DEBT PD, DEBT D, SALES_HEADER SH " +
                                    "WHERE PD.DEBT_ID = D.DEBT_ID AND D.INVOICE_ID = SH.INVOICE_NO AND D.DEBT_ACTIVE = 1 AND SH.INVOICE_ACTIVE = 1 ";

            if (status == 1)
                sqlCommand = sqlCommand + " AND PD.PAYMENT_STATUS = 1";
            else
                sqlCommand = sqlCommand + " AND PD.PAYMENT_STATUS > 0";

            if (contactID > 0)
                sqlCommand = sqlCommand + " AND SH.CONTACT_ID = " + contactID + " ";

            if (skipReturID != "")
                sqlCommand = sqlCommand + " AND PD.RETUR_ID <> '" + skipReturID + "'";

            try
            {
                paymentValue = Convert.ToDouble(DS.getDataSingleValue(sqlCommand));
            }
            catch (Exception ex) { paymentValue = 0; }

            return paymentValue;
        }

        public double getTotalPayment(int status = 0, string selectedID = "", string skipReturID = "")
        {
            double paymentValue = 0;
            string sqlCommand = "";

            sqlCommand = "SELECT SUM(PAYMENT_NOMINAL) FROM DEBT_PAYMENT WHERE 1 = 1 ";

            if (selectedID != "")
                sqlCommand = sqlCommand + " AND DEBT_ID = '" + selectedID + "'";

            if (status == 1)
                sqlCommand = sqlCommand + " AND PAYMENT_STATUS = 1";
            else
                sqlCommand = sqlCommand + " AND PAYMENT_STATUS > 0";

            if (skipReturID != "")
                sqlCommand = sqlCommand + " AND RETUR_ID <> '" + skipReturID + "'";

            try
            {
                paymentValue = Convert.ToDouble(DS.getDataSingleValue(sqlCommand));
            }
            catch (Exception ex) { paymentValue = 0; }

            return paymentValue;
        }

        public double getTotalTerhutangSO(string selectedID = "", int contactID = 0)
        {
            string sqlCommand = "";
            double totalTerhutang = 0;
            sqlCommand = "SELECT SUM(SH.INVOICE_TOTAL - SH.INVOICE_DISC_FINAL)  " +
                                    "FROM SALES_HEADER SH, DEBT D  " +
                                    "WHERE SH.INVOICE_NO = D.INVOICE_ID AND SH.INVOICE_ACTIVE = 1";// AND D.DEBT_PAID = 0 ";

            if (selectedID != "")
                sqlCommand = sqlCommand + " AND SH.INVOICE_NO = '" + selectedID + "'";

            if (contactID > 0)
                sqlCommand = sqlCommand + " AND SH.CONTACT_ID = " + contactID;

            totalTerhutang = Convert.ToDouble(DS.getDataSingleValue(sqlCommand));

            return totalTerhutang;
        }

        public List<invoiceList> getDebtList(int selectedContactID, string selectedReturID)
        {
            DataTable dt = new DataTable();
            MySqlDataReader rdr;
            List<invoiceList> debtList = new List<invoiceList>();
            invoiceList debtElement;

            string sqlPayment = "SELECT DEBT_ID, SUM(PAYMENT_NOMINAL) AS TOTAL_PAYMENT FROM PAYMENT_DEBT WHERE RETUR_ID <> '" + selectedReturID + "' GROUP BY DEBT_ID";

            string sqlDebt = "SELECT D.DEBT_ID, D.INVOICE_ID, D.DEBT_TOTAL, IFNULL(PD.TOTAL_PAYMENT, 0) AS TOTAL_PAYMENT " +
                                    "FROM DEBT D " +
                                    "LEFT OUTER JOIN (" +sqlPayment+ ") PD ON (D.DEBT_ID = PD.DEBT_ID), " +
                                    "SALES_HEADER SH " +
                                    "WHERE D.INVOICE_ID = SH.INVOICE_NO AND SH.CONTACT_ID = " + selectedContactID + " " +
                                    "AND SH.INVOICE_ACTIVE = 1 AND D.DEBT_ACTIVE = 1";

            using (rdr = DS.getData(sqlDebt))
            {
                if (rdr.HasRows)
                    dt.Load(rdr);
            }
            rdr.Close();

            foreach(DataRow row in dt.Rows)
            {
                debtElement = new invoiceList();
                debtElement.debtID = row["DEBT_ID"].ToString();
                debtElement.debtNominal = Convert.ToDouble(row["DEBT_TOTAL"]) - Convert.ToDouble(row["TOTAL_PAYMENT"]);
                debtElement.invoiceID = row["INVOICE_ID"].ToString();

                if (debtElement.debtNominal > 0)
                    debtList.Add(debtElement);
            }

            return debtList;
        }
    }
}
