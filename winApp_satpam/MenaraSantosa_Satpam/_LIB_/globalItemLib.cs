﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace AlphaSoft
{
    class globalItemLib
    {
        globalRestAPI gRest;
        globalUserUtil gUser;

        public globalItemLib()
        {
            gRest = new globalRestAPI();
            gUser = new globalUserUtil();
        }

        public bool qtyItemEnough(string itemID, double itemQty, double itemOldQty)
        {
            bool result = false;
            double currQty = 0;
            string sqlCommand = "";

            genericReply replyResult = new genericReply();

            sqlCommand = "SELECT qty AS resultQuery " +
                                    "FROM master_item " + 
                                    "WHERE item_id = " + itemID;

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
            {
                currQty = Convert.ToDouble(replyResult.data);

                if (currQty >= (itemQty - itemOldQty))
                    result = true;
            }

            return result;
        }

    }
}
