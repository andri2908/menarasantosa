﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RestSharp;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace AlphaSoft
{
    class globalRestReport
    {
        private globalRestAPI gRest = new globalRestAPI();
        private globalUserUtil gUser = new globalUserUtil();

        public bool generatePrintOutTransaksiIPL(string sqlCommand, ref REST_printoutTransaksi transData)
        {
            REST_printoutTransaksi printoutTrans = new REST_printoutTransaksi();

            var client = new RestClient(globalRestAPI.SERVER_URL + globalRestAPI.FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", gUser.getUserToken());
            request.AddParameter("p_user_id", gUser.getUserID());
            request.AddParameter("sqlQuery", sqlCommand);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_printoutTransaksi>(response.Content);

            transData = (REST_printoutTransaksi)jsonResponse;

            if (transData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

    }
}
