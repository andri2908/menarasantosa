﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrystalDecisions.ReportSource;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportAppServer;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms;

namespace AlphaSoft
{
    class globalPrinterUtility : globalUtilities
    {
        public const int POS_RECEIPT_PAPER = 0;
        public const int HALF_KUARTO_PAPER = 1;
        public const int KUARTO_PAPER = 2;

        public const string QUARTER_KUARTO_PAPER_SIZE = "QUARTER_KUARTO";
        public const string HALF_KUARTO_PAPER_SIZE = "HALF_KUARTO";
        public const string KARTU_STOK_SIZE = "KARTU_STOK";
        public const string LETTER_PAPER_SIZE = "Letter";

        public PrinterSettings ps = new PrinterSettings();
        private PrintDocument printdoc = new PrintDocument();
        private string appPath = Application.StartupPath + "\\conf";

        public globalPrinterUtility()
        {
        }

        public int getReportPaperSize(string paperSizeName = LETTER_PAPER_SIZE)
        {
            int i = 0;
            PrintDocument doctoprint = new PrintDocument();
            int rawKind = 0;
            for (i = 0; i <= doctoprint.PrinterSettings.PaperSizes.Count - 1; i++)
            {
                if (doctoprint.PrinterSettings.PaperSizes[i].PaperName == paperSizeName)
                {
                    rawKind = Convert.ToInt32(doctoprint.PrinterSettings.PaperSizes[i].GetType().GetField("kind", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes[i]));
                    break;
                }
            }

            return rawKind;
        }

        public int getListOfPrinter(ref List<string> printerName)
        {
            int numOfPrinter = 0;

            String namaprinter = "";
            System.Drawing.Printing.PrintDocument doctoprint = new System.Drawing.Printing.PrintDocument();
            int i = 0;
            foreach (var item in PrinterSettings.InstalledPrinters)
            {
                namaprinter = item.ToString();
                printerName.Add(namaprinter);
                i++;
            }

            numOfPrinter = i;

            return numOfPrinter;
        }

        public string getConfigPrinterName(int printerType = 1)
        {
            string printerName = "";
        
            string posPath = appPath + "\\posPath.cfg";
            string kuartoPath = appPath + "\\kuartoPath.cfg";
            string result = "";

            if (printerType == 1) // POS RECEIPT PRINTER
            {
                if (File.Exists(posPath))
                    result = File.ReadAllText(posPath);
            }
            else // KUARTO RECEIPT PRINTER
            {
                if (File.Exists(kuartoPath))
                    result = File.ReadAllText(kuartoPath);
            }

            printerName = result;

            return printerName;
        }

    }
}
