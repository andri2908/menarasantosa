﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace AlphaSoft
{
    class globalImageLib
    {
        public const int IMG_PRODUCTS = 0;
        public const int IMG_CS = 1;
        public const int IMG_NEWS = 2;
        public const int IMG_KOMPLEKS = 3;

        public const string ftp_server = "ftp.menarasantosagroup.com";
        public const string ftp_username = "alphasoft@menarasantosagroup.com";
        public const string ftp_password = "k6XoysFHgOia";

        public const string ftp_img_products = "ftp://" + ftp_server + "/apps/public/sys_contents/img_products/";
        public const string ftp_img_cs = "ftp://" + ftp_server + "/apps/public/sys_contents/img_cs/";
        public const string ftp_img_news = "ftp://" + ftp_server + "/apps/public/sys_contents/img_news/";
        public const string ftp_img_kompleks = "ftp://" + ftp_server + "/apps/public/sys_contents/img_kompleks/";

        public const string server_img_products = "apps.menarasantosagroup.com/public/sys_contents/img_products/";
        public const string server_img_cs = "apps.menarasantosagroup.com/public/sys_contents/img_cs/";
        public const string server_img_news = "apps.menarasantosagroup.com/public/sys_contents/img_news/";
        public const string server_img_kompleks = "apps.menarasantosagroup.com/public/sys_contents/img_kompleks/";

        public string localImgDirectory = Application.StartupPath + "\\img\\";

        public const int def_img_height = 500;
        public const int def_img_width = 500;

        public bool uploadToFTP(string fileName, int imgType = IMG_PRODUCTS)
        {
            bool result = false;
            string From = @localImgDirectory + fileName;
            string To = "";

            switch (imgType)
            {
                case IMG_PRODUCTS:
                    To = ftp_img_products + fileName;
                    break;

                case IMG_CS:
                    To = ftp_img_cs + fileName;
                    break;

                case IMG_NEWS:
                    To = ftp_img_news + fileName;
                    break;

                case IMG_KOMPLEKS:
                    To = ftp_img_kompleks + fileName;
                    break;
            }

            try
            {
                using (WebClient client = new WebClient())
                {
                    client.Credentials = new NetworkCredential(ftp_username, ftp_password);
                    client.UploadFileTaskAsync(To, WebRequestMethods.Ftp.UploadFile, From);
                }

                result = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return result;
        }

        public byte[] GetImgByte(string fileName, int imgType = IMG_PRODUCTS)
        {
            string ftpDir = "";

            switch (imgType)
            {
                case IMG_PRODUCTS:
                    ftpDir = ftp_img_products + fileName;
                    break;

                case IMG_CS:
                    ftpDir = ftp_img_cs + fileName;
                    break;

                case IMG_NEWS:
                    ftpDir = ftp_img_news + fileName;
                    break;

                case IMG_KOMPLEKS:
                    ftpDir = ftp_img_kompleks + fileName;
                    break;
            }

            WebClient ftpClient = new WebClient();
            ftpClient.Credentials = new NetworkCredential(ftp_username, ftp_password);

            byte[] imageByte = ftpClient.DownloadData(ftpDir);
            return imageByte;
        }

        public Bitmap ByteToImage(string fileName, int imgType = IMG_PRODUCTS)
        {
            MemoryStream mStream = new MemoryStream();
            byte[] pData = GetImgByte(fileName, imgType);

            mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
            Bitmap bm = new Bitmap(mStream, false);
            mStream.Dispose();
            return bm;
        }

        public bool downloadFromFTP(string fileName)
        {
            bool result = false;

            string localDir = @localImgDirectory + fileName;
            string ftpDir = ftp_img_products + fileName;

            try
            {
                using (WebClient client = new WebClient())
                {
                    client.Credentials = new NetworkCredential(ftp_username, ftp_password);
                    client.DownloadFile(ftpDir, localDir);
                }

                result = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return result;
        }

        public void clearLocalImgDir(string fileName)
        {
            try
            {
                if (fileName.Length > 0)
                {
                    string localDir = @localImgDirectory + fileName;
                    File.Delete(localDir);
                }
            }
            catch (Exception ex) { }
        }

        public void ResizeImage(string fileName, string outputFile, 
            int maxHeight = def_img_height, int maxWidth = def_img_width)
        {
            System.Drawing.Image img = System.Drawing.Image.FromFile(fileName);

            int imgHeight = img.Height;
            int imgWidth = img.Width;

            int deltaSize = 0;
            if (imgHeight > imgWidth)
            {
                if (imgHeight > maxHeight)
                {
                    deltaSize = imgHeight - maxHeight;
                    imgHeight = maxHeight;
                    imgWidth -= deltaSize;

                    if (imgWidth <= 0)
                        imgWidth = def_img_width;
                }
            }
            else
            {
                if (imgWidth > maxWidth)
                {
                    deltaSize = imgWidth - maxWidth;
                    imgWidth = maxWidth;
                    imgHeight -= deltaSize;

                    if (imgHeight <= 0)
                        imgHeight = def_img_height;
                }
            }

            using (var srcImage = System.Drawing.Image.FromFile(fileName))
            {
                var newWidth = imgWidth;
                var newHeight = imgHeight;

                using (var newImage = new Bitmap(newWidth, newHeight))
                using (var graphics = Graphics.FromImage(newImage))
                {
                    graphics.SmoothingMode = SmoothingMode.AntiAlias;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    graphics.DrawImage(srcImage, new Rectangle(0, 0, newWidth, newHeight));
                    newImage.Save(localImgDirectory + outputFile, ImageFormat.Jpeg);
                }
            }
        }

        public Bitmap resizeBitmap(Bitmap bmp, int width = def_img_width, int height = def_img_height)
        {
            Bitmap result = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(result))
            {
                g.DrawImage(bmp, 0, 0, width, height);
            }

            return result;
        }
    }
}
