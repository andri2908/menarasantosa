﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Globalization;
using System.Data.SqlClient;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace AlphaSoft
{
    class globalMessageUtil
    {
        private Data_Access DS;
        private CultureInfo culture = new CultureInfo("id-ID");
        private globalDBUtil gDB;
        private globalUserUtil gUser;
        private globalUtilities gUtil;
        
        public const string MSG_LOG_CHANGES = "ADA PERUBAHAN PADA LOG";

        private string[] msgList =
        {
            "MODULE_ID",
            "REF_ID",
            "MESSAGE_CONTENT",
            "MSG_DATETIME",
            "USER_ID",
            "ID"
        };

        public globalMessageUtil(Data_Access DSParam = null)
        {
            if (null != DSParam)
                DS = DSParam;
            else
                DS = new Data_Access();

            gDB = new globalDBUtil(DS);
            gUser = new globalUserUtil(DS);
            gUtil = new globalUtilities();
        }

        private void pullEvent()
        {
        }

        public void pullMessage()
        {
            pullEvent();
        }

        public void setMessageToRead(int msgID, int readValue = 1, bool readAll = false)
        {
            string sqlCommand = "";
            MySqlException inEx = null;

            DS.beginTransaction();

            try
            {
                sqlCommand = "UPDATE MASTER_MESSAGE SET IS_READ = 1 ";

                if (!readAll)
                    sqlCommand += " WHERE ID = '" + msgID + "'";

                if (!DS.executeNonQueryCommand(sqlCommand, ref inEx))
                    throw inEx;

                DS.commit();
            }
            catch (Exception ex) { }
        }

        public bool saveToMessageTable(MySqlException inEXParam, int moduleIDAccess, int moduleID, 
            string refID, string keyWord, string messageToWrite, string idx, DateTime msgDT, int skipUserID = 0)
        {
            string sqlCommand = "";
            DateTime dtTime = DateTime.Now;
            MySqlDataReader rdr;
            DataTable dt = new DataTable();
            int i = 0;
            int userID = 0;

            List<string> pList = new List<string>();
            List<object> pVal = new List<object>();

            sqlCommand = "SELECT MU.ID " +
                                    "FROM MASTER_USER MU, " +
                                    "MASTER_MODULE_ACCESS MMA " +
                                    "WHERE MMA.MODULE_ID = " + moduleIDAccess + " " +
                                    "AND MMA.USER_ACCESS_OPTION > 0 " +
                                    "AND MMA.GROUP_ID = MU.GROUP_ID " +
                                    "AND MU.USER_ACTIVE = 1 ";

            if (skipUserID > 0)
            {
                sqlCommand += "AND MU.ID <> " + skipUserID;
            }

            using (rdr = DS.getData(sqlCommand))
            {
                if (rdr.HasRows)
                {
                    dt.Load(rdr);
                }
            }
            rdr.Close();

            i = 0;

            foreach (DataRow r in dt.Rows)
            {
                userID = Convert.ToInt32(r["ID"]);
                sqlCommand = "SELECT COUNT(1) FROM MASTER_MESSAGE " +
                                        "WHERE MESSAGE_CONTENT LIKE '%" + keyWord + "%' " +
                                        "AND MODULE_ID = " + moduleID + " " +
                                        "AND USER_ID = " + userID + " " +
                                        "AND IS_READ = 0";

                if (refID.Length > 0)
                    sqlCommand += "AND REF_ID = '" + refID + "' ";

                if (Convert.ToInt32(DS.getDataSingleValue(sqlCommand)) > 0)
                    return false;

                pList.Clear();
                gDB.addParamList(pList, msgList, "_msgTable_" + idx + "_" + i);

                pVal.Clear();
                pVal.Add(moduleID);
                pVal.Add(refID);
                pVal.Add(messageToWrite);
                pVal.Add(msgDT);
                pVal.Add(userID);
                pVal.Add(0);

                sqlCommand = gDB.constructMasterQuery("MASTER_MESSAGE", msgList, pList);

                if (!DS.executeNonQueryCommandWithParameters(sqlCommand, ref inEXParam, pList, pVal))
                    throw inEXParam;

                i++;
            }

            return true;
        }

        public void saveToMessageTable(MySqlException inEXParam, int moduleIDAccess, int moduleID, string refID, 
            string keyWord, string messageToWrite, int userID, DateTime msgDT,
            bool ignoreDuplicate = false, bool ignoreAccess = false, string idx = "")
        {
            string sqlCommand = "";
            DateTime dtTime = DateTime.Now;
            MySqlDataReader rdr;
            DataTable dt = new DataTable();
            int i = 0;

            List<string> pList = new List<string>();
            List<object> pVal = new List<object>();

            //if (!ignoreAccess && 
            //    !gUser.userHasAccessTo(moduleIDAccess, MMConstants.HAK_AKSES, gUser.getUserGroupID()))
            //    return;

            if (!ignoreDuplicate)
            {
                sqlCommand = "SELECT COUNT(1) FROM MASTER_MESSAGE " +
                                           "WHERE MESSAGE_CONTENT LIKE '%" + keyWord + "%' " +
                                           "AND MODULE_ID = " + moduleID + " " +
                                           "AND USER_ID = " + userID + " " +
                                           "AND IS_READ = 0 ";

                if (refID.Length > 0)
                    sqlCommand += "AND REF_ID = '" + refID + "' ";

                if (Convert.ToInt32(DS.getDataSingleValue(sqlCommand)) > 0)
                    return;
            }

            pList.Clear();
            gDB.addParamList(pList, msgList, idx);

            pVal.Clear();
            pVal.Add(moduleID);
            pVal.Add(refID);
            pVal.Add(messageToWrite);
            pVal.Add(msgDT);
            pVal.Add(userID);
            pVal.Add(0);

            sqlCommand = gDB.constructMasterQuery("MASTER_MESSAGE", msgList, pList);

            if (!DS.executeNonQueryCommandWithParameters(sqlCommand, ref inEXParam, pList, pVal))
                throw inEXParam;    
        }
    }
}
