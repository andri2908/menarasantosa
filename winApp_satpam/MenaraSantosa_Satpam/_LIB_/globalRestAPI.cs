﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;

using RestSharp;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Windows.Forms;
using System.Globalization;

namespace AlphaSoft
{
    class globalRestAPI
    {
        private CultureInfo culture = new CultureInfo("id-ID");

        private const int LAST_LOGIN = 1;
        private const int LAST_LOGOUT = 2;

        public const string DEF_SERVER_URL = "http://192.168.1.151"; // SERVER SATPAM
        public const string DEF_LIVE_SERVER_URL = "http://apps.menarasantosa.com/public";

        //public const string SERVER_URL = "http://192.168.1.151"; // SERVER SATPAM
        //public const string LIVE_SERVER_URL = "http://apps.menarasantosa.com/public";

        public static string SERVER_URL;// = "http://localhost:8002";
        public static string LIVE_SERVER_URL;// = "http://localhost:8001";

        public const string FUNC_GETDATA = "/getData";

        private const string FUNC_GETDATASINGLEVALUE = "/getDataSingleValue";
        private const string FUNC_GETDATASINGLEVALUENOPARAM = "/getDataSingleValueNoParam";
        private const string FUNC_GETDATANOPARAM = "/getDataNoParam";

        private const string FUNC_USER_LOGIN = "/user_login";
        private const string FUNC_SAVE_LAST_LOGIN = "/saveLastLogin";
        private const string FUNC_GET_USERGROUPACCESS = "/getUserGroupAccess";
        private const string FUNC_CHANGE_PASSWORD = "/changePassword";
        private const string FUNC_SAVE_GROUP_USER = "/saveGroupUser";
        private const string FUNC_SAVE_USER_LOGIN_DATA = "/saveUserData";
        private const string FUNC_SAVE_MODULE_ACCESS = "/saveModuleAccess";
        private const string FUNC_SAVE_SYS_CONFIG = "/saveSysConfig";
        private const string FUNC_SAVE_MASTER_KOMPLEKS = "/saveMasterKompleks";
        private const string FUNC_SAVE_MASTER_KAVLING = "/saveMasterKavling";
        private const string FUNC_SAVE_USER_KAVLING = "/saveUserKavling";
        private const string FUNC_SAVE_MASTER_ITEM = "/saveMasterItem";
        private const string FUNC_SAVE_MASTER_UNIT = "/saveMasterUnit";
        private const string FUNC_SAVE_SUMMARY_IPL = "/saveSummaryIPL";
        private const string FUNC_RETURN_QTY_VOID = "/returnQtyBecauseOfVoid";
        private const string FUNC_SAVE_TRANSAKSI = "/saveDataTransaksi";
        private const string FUNC_PENYESUAIAN_STOK = "/penyesuaianStok";
        private const string FUNC_SAVE_PENERIMAAN = "/savePenerimaan";
        private const string FUNC_SAVE_CUSTCARE_DETAIL = "/saveCustCareDetail";
        private const string FUNC_UPDATE_CUSTCARE = "/updateCustCare";
        private const string FUNC_RESET_TRANS = "/resetExpiredTrans";
        private const string FUNC_SAVE_NEWS = "/saveNews";
        private const string FUNC_CC_DETAIL = "/cc_reply";
        private const string FUNC_CC_WORK = "/cc_work";
        private const string FUNC_CC_CLOSE = "/cc_close";
        private const string FUNC_CC_CONFIRMATION = "/cc_confirmation";
        private const string FUNC_CLEAR_TAGIHAN = "/clearTagihanIPL";
        private const string FUNC_SAVE_USER_KAVLING_RFID = "/saveUserKavlingRFID";
        private const string FUNC_SAVE_USER_ACCESS_LIST = "/saveUserAccessList";
        private const string FUNC_SAVE_SATPAM_ACCESS_LIST = "/saveSatpamAkses";
        private const string FUNC_SAVE_KOMPLEKS_TERMINAL = "/saveKompleksTerminal";

        private const string FUNC_GET_LIVE_USER_LIST = "/getAllUserList";
        private const string FUNC_GET_LIVE_KOMPLEKS_TERMINAL = "/getKompleksTerminalData";
        private const string FUNC_GET_LIVE_SERVER_STATUS = "/getStatusConn";
        private const string FUNC_LIVE_SERVER_TRUNCATE_TABLE = "/truncateUserList";
        private const string FUNC_LIVE_SERVER_TRUNCATE_TERMINAL = "/truncateTerminalData";
        private const string FUNC_LIVE_SERVER_SAVE_USER_LIST = "/saveUserList";
        private const string FUNC_LIVE_SERVER_SAVE_TERMINAL_LIST = "/saveTerminalData";
        private const string FUNC_LIVE_SERVER_SAVE_SYNC_DATE = "/saveServerSyncDate";
        private const string FUNC_LIVE_SERVER_SAVE_DURATION_PULL = "/saveFrekuensiSync";

        private JsonSerializer jsonConvert = new JsonSerializer();

        public class urlValue
        {
            public string url_type { get; set; }
            public string url_value { get; set; }
        };

        List<urlValue> urlList = new List<urlValue>();

        public void loadURL()
        {
            string path = "";
            string configFile = Application.StartupPath + "\\conf\\url.cfg";
            urlValue urlElement;

            urlList.Clear();
            if (File.Exists(configFile))
            {
                string[] urlStr;
                using (StreamReader sr = File.OpenText(configFile))
                {
                    while ((path = sr.ReadLine()) != null)
                    {
                        urlStr = path.Split('=');
                        urlElement = new urlValue();
                        urlElement.url_type = urlStr[0];
                        urlElement.url_value = urlStr[1];

                        urlList.Add(urlElement);
                    }
                }
            }

            urlElement = urlList.Find(x => x.url_type == "SERVER_URL");
            if (null == urlElement)
            {
                SERVER_URL = DEF_SERVER_URL;
            }
            else
            {
                SERVER_URL = urlElement.url_value;
            }

            urlElement = urlList.Find(x => x.url_type == "LIVE_SERVER_URL");
            if (null == urlElement)
            {
                LIVE_SERVER_URL = DEF_LIVE_SERVER_URL;
            }
            else
            {
                LIVE_SERVER_URL = urlElement.url_value;
            }
        }

        public bool checkDataExist(int userID, string accessToken, string sqlComm, ref genericReply replyResult)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATASINGLEVALUE);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlComm);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
                return false;

            replyResult = (genericReply)jsonResponse[0];

            if (replyResult.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getDataSingleValue(int userID, string accessToken, string sqlComm, ref genericReply replyResult)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATASINGLEVALUE);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlComm);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
                return false;

            replyResult = (genericReply)jsonResponse[0];

            if (replyResult.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getDataSingleValueNoParam(int userID, string accessToken, string sqlComm, ref genericReply replyResult)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATASINGLEVALUENOPARAM);
            var request = new RestRequest(Method.POST);

            request.AddParameter("sqlQuery", sqlComm);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
                return false;

            replyResult = (genericReply)jsonResponse[0];

            if (replyResult.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getUserTokenData(string userName, string userPassword, ref REST_userAccessData userTokenData)
        {
            #region CHECK USER CORRECT
            genericReply replyResult = new genericReply();
            string sqlQuery = "SELECT COUNT(1) AS resultQuery " +
                                        "FROM user_login_data ul " +
                                        "WHERE UPPER(ul.user_name) = '" + userName.ToUpper() + "' " +
                                        "AND ul.user_password = '" + userPassword + "' " +
                                        "AND ul.is_active = 'Y'";

            var client = new RestClient(SERVER_URL + FUNC_GETDATASINGLEVALUENOPARAM);
            var request = new RestRequest(Method.POST);

            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
                return false;

            replyResult = (genericReply)jsonResponse[0];

            if (null == replyResult || 
                replyResult.o_status == -1 ||
                Convert.ToInt32(replyResult.data) <= 0)
            {
                return false;
            }
            #endregion

            #region USER TOKEN
            sqlQuery = "SELECT ul.user_id, ul.user_name, ul.user_full_name, ut.access_token " +
                                        "FROM user_login_data ul, user_token ut " +
                                        "WHERE ul.user_id = ut.user_id " +
                                        "AND UPPER(ul.user_name) = '" + userName.ToUpper() + "' " +
                                        "AND ul.user_password = '" + userPassword + "' " +
                                        "AND ul.is_active = 'Y'";

            client = new RestClient(SERVER_URL + FUNC_GETDATANOPARAM);
            request = new RestRequest(Method.POST);

            request.AddParameter("sqlQuery", sqlQuery);

            response = client.Execute(request);

            restResult = response.Content;
            var jsonResponseToken = JsonConvert.DeserializeObject<REST_userAccessData>(response.Content);

            if (null == jsonResponseToken)
                return false;

            userTokenData = (REST_userAccessData)jsonResponseToken;

            if (null != userTokenData && 
                userTokenData.dataStatus.o_status == 1)
                return true;
            else
                return false;
            #endregion
        }

        public bool saveUserLastLogin(int userID, string accessToken)
        {
            string errMsg = "";

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_LAST_LOGIN);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_loginType", LAST_LOGIN);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>) jsonResponse;

            if (replyResult[0].o_status == 1)
                return true;
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveUserLastLogout(int userID, string accessToken)
        {
            string errMsg = "";

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_LAST_LOGIN);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_loginType", LAST_LOGOUT);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
                return true;
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool getUserGroupID(int userID, string accessToken, ref genericReply replyResult)
        {
            string sqlComm = "SELECT IFNULL(group_id, 0) AS resultQuery " +
                                        "FROM user_login_data " +
                                        "WHERE user_id = " + userID;

            var client = new RestClient(SERVER_URL + FUNC_GETDATASINGLEVALUE);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlComm);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            replyResult = (genericReply)jsonResponse[0];

            if (replyResult.o_status == 1)
                return true;
            else
                return false;
        }

        public int userHasAccessTo(int userID, string accessToken, string sqlComm)
        {
            genericReply replyResult = new genericReply();

            var client = new RestClient(SERVER_URL + FUNC_GETDATASINGLEVALUE);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlComm);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                return 0;
            }

            replyResult = (genericReply)jsonResponse[0];

            if (replyResult.o_status == 1)
                return Convert.ToInt32(replyResult.data);
            else
                return 0;
        }

        public bool checkUserPassword(int userID, string accessToken, string userPassword)
        {
            genericReply replyResult = new genericReply();

            string sqlComm = "SELECT COUNT(1) AS resultQuery " +
                                        "FROM user_login_data " +
                                        "WHERE user_id = " + userID + " " +
                                        "AND user_password = '" + userPassword + "'";

            var client = new RestClient(SERVER_URL + FUNC_GETDATASINGLEVALUE);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlComm);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            replyResult = (genericReply)jsonResponse[0];

            if (replyResult.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getUserGroupAccess(int userID, string accessToken, int groupID, ref REST_groupUserAccess mmAccess)
        {
            var client = new RestClient(SERVER_URL + FUNC_GET_USERGROUPACCESS);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_group_user_id", groupID);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_groupUserAccess>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            mmAccess = (REST_groupUserAccess)jsonResponse;

            if (mmAccess.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getMasterGroupData(int userID, string accessToken, string sqlQuery, ref REST_groupUser groupUserData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_groupUser>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            groupUserData = (REST_groupUser)jsonResponse;

            if (groupUserData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getMasterModuleData(int userID, string accessToken, string sqlQuery, ref REST_masterModule mmData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_masterModule>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            mmData = (REST_masterModule)jsonResponse;

            if (mmData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getUserKavlingData(int userID, string accessToken, string sqlQuery, ref REST_userKavling userKavData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_userKavling>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            userKavData = (REST_userKavling)jsonResponse;

            if (userKavData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getSysConfigData(int userID, string accessToken, string sqlQuery, ref REST_sysConfig sysConfigData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_sysConfig>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            sysConfigData = (REST_sysConfig)jsonResponse;

            if (sysConfigData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getSysConfigTransData(int userID, string accessToken, string sqlQuery, ref REST_sysConfigTrans sysConfigData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_sysConfigTrans>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            sysConfigData = (REST_sysConfigTrans)jsonResponse;

            if (sysConfigData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getSysConfigSchedule(int userID, string accessToken, string sqlQuery, ref REST_sysConfigSchedule sysConfigData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_sysConfigSchedule>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            sysConfigData = (REST_sysConfigSchedule)jsonResponse;

            if (sysConfigData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getUserLoginData(int userID, string accessToken, string sqlQuery, ref REST_userLoginData userLoginData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_userLoginData>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            userLoginData = (REST_userLoginData)jsonResponse;

            if (userLoginData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getMasterKompleksData(int userID, string accessToken, string sqlQuery, ref REST_masterKompleks kompleksData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_masterKompleks>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            kompleksData = (REST_masterKompleks)jsonResponse;

            if (kompleksData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getMasterBlokData(int userID, string accessToken, string sqlQuery, ref REST_masterBlok blokData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_masterBlok>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            blokData = (REST_masterBlok)jsonResponse;

            if (blokData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getMasterKavlingData(int userID, string accessToken, string sqlQuery, ref REST_masterKavling kavlingData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_masterKavling>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            kavlingData = (REST_masterKavling)jsonResponse;

            if (kavlingData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getMasterItemData(int userID, string accessToken, string sqlQuery, ref REST_masterItem itemData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_masterItem>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            itemData = (REST_masterItem)jsonResponse;

            if (itemData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getMasterUnitData(int userID, string accessToken, string sqlQuery, ref REST_masterUnit unitData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_masterUnit>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            unitData = (REST_masterUnit)jsonResponse;

            if (unitData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getIPLData(int userID, string accessToken, string sqlQuery, ref REST_dataIPL iplData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_dataIPL>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            iplData = (REST_dataIPL)jsonResponse;

            if (iplData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getTransData(int userID, string accessToken, string sqlQuery, ref REST_dataTransaksi dataTrans)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_dataTransaksi>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            dataTrans = (REST_dataTransaksi)jsonResponse;

            if (dataTrans.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getTransHeaderData(int userID, string accessToken, string sqlQuery, ref REST_transaksiHeader dataTrans)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_transaksiHeader>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            dataTrans = (REST_transaksiHeader)jsonResponse;

            if (dataTrans.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getTransDetailData(int userID, string accessToken, string sqlQuery, ref REST_transaksiDetail dataTrans)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_transaksiDetail>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            dataTrans = (REST_transaksiDetail)jsonResponse;

            if (dataTrans.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getPenerimaanHeaderData(int userID, string accessToken, string sqlQuery, ref REST_penerimaanHeader dataTrans)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_penerimaanHeader>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            dataTrans = (REST_penerimaanHeader)jsonResponse;

            if (dataTrans.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getPenerimaanDetailData(int userID, string accessToken, string sqlQuery, ref REST_penerimaanDetail dataTrans)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_penerimaanDetail>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            dataTrans = (REST_penerimaanDetail)jsonResponse;

            if (dataTrans.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getCustCareData(int userID, string accessToken, string sqlQuery, ref REST_custCare dataTrans)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_custCare>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            dataTrans = (REST_custCare)jsonResponse;

            if (dataTrans.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getCustCareDetailData(int userID, string accessToken, string sqlQuery, ref REST_custCareDetail dataTrans)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_custCareDetail>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            dataTrans = (REST_custCareDetail)jsonResponse;

            if (dataTrans.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getItemCategoryData(int userID, string accessToken, string sqlQuery, ref REST_itemCategory itemCat)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_itemCategory>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            itemCat = (REST_itemCategory)jsonResponse;

            if (itemCat.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getCSAttachment(int userID, string accessToken, string sqlQuery, ref REST_imgAttachment imgAttach)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_imgAttachment>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            imgAttach = (REST_imgAttachment)jsonResponse;

            if (imgAttach.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getNewsData(int userID, string accessToken, string sqlQuery, ref REST_news newsData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_news>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            newsData = (REST_news)jsonResponse;

            if (newsData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getUserKavlingRFIDData(int userID, string accessToken, string sqlQuery, ref REST_userKavlingRFID rfidData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_userKavlingRFID>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            rfidData = (REST_userKavlingRFID)jsonResponse;

            if (rfidData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getSatpamAccessData(int userID, string accessToken, string sqlQuery, ref REST_satpamAccess satpamData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_satpamAccess>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            satpamData = (REST_satpamAccess)jsonResponse;

            if (satpamData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getKompleksTerminalData(int userID, string accessToken, string sqlQuery, ref REST_kompleksTerminal terminalData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_kompleksTerminal>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            terminalData = (REST_kompleksTerminal)jsonResponse;

            if (terminalData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getUserAccessListData(int userID, string accessToken, string sqlQuery, ref REST_userAccessList userData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_userAccessList>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            userData = (REST_userAccessList)jsonResponse;

            if (userData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool checkForExpiredTrans(int userID, string accessToken, string sqlQuery)
        {
            var client = new RestClient(SERVER_URL + FUNC_RESET_TRANS);
            var request = new RestRequest(Method.POST);
            List<genericReply> replyResult = new List<genericReply>();

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public void changePassword(int userID, string accessToken, string newPass)
        {
            var client = new RestClient(SERVER_URL + FUNC_CHANGE_PASSWORD);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_new_password", newPass);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
        } 

        public bool saveGroupUser(int userID, string accessToken, List<master_group> groupUserData, int opType, 
            out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_GROUP_USER);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(groupUserData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_opType", opType);
            request.AddParameter("records", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveHakAkses(int userID, string accessToken, int groupID, List<master_module> mmData,
                    out string errMsg)
        {
            errMsg = "";

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_MODULE_ACCESS);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(mmData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_group_id", groupID);
            request.AddParameter("records", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveUserLoginData(int userID, string accessToken, List<user_login_data> userData, List<user_token> userToken, 
            int opType, out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_USER_LOGIN_DATA);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(userData);
            var jsonToken = JsonConvert.SerializeObject(userToken);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_opType", opType);

            request.AddParameter("records", json);
            request.AddParameter("records_token", jsonToken);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveSysConfigData(int userID, string accessToken, List<sys_config> sysConfig, List<sys_config_trans> sysConfigTrans, List<sys_config_schedule> sysConfigSchedule,
            out string errMsg)
        {
            errMsg = "";

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_SYS_CONFIG);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(sysConfig);
            var jsonTrans = JsonConvert.SerializeObject(sysConfigTrans);
            var jsonSchedule = JsonConvert.SerializeObject(sysConfigSchedule);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);

            request.AddParameter("sysConfigParam", json);
            request.AddParameter("sysConfigTransParam", jsonTrans);
            request.AddParameter("sysConfigScheduleParam", jsonSchedule);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveMasterKompleks(int userID, string accessToken, List<master_kompleks> kompleksData, List<master_blok> blokData,
            int opType, out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_MASTER_KOMPLEKS);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(kompleksData);
            var jsonBlok = JsonConvert.SerializeObject(blokData);

            //string json = JsonConvert.SerializeObject(kompleksData);
            //string jsonBlok = JsonConvert.SerializeObject(blokData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_opType", opType);

            request.AddParameter("kompleksParam", json);
            request.AddParameter("blokParam", jsonBlok);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            //var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveUserKavling(int userID, string accessToken, List<user_kavling> kavlingData, List<upd_master_kavling> updKavling,
            out string errMsg)
        {
            errMsg = "";
            
            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_USER_KAVLING);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(kavlingData);
            var json2 = JsonConvert.SerializeObject(updKavling);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("records", json);
            request.AddParameter("updKavling", json2);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveMasterKavling(int userID, string accessToken, List<master_kavling> kavlingData, int opType,
            out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_MASTER_KAVLING);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(kavlingData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_opType", opType);
            request.AddParameter("records", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveMasterItem(int userID, string accessToken, List<master_item> itemData, int opType,
         out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_MASTER_ITEM);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(itemData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_opType", opType);
            request.AddParameter("records", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveMasterUnit(int userID, string accessToken, List<master_unit> unitData, int opType,
         out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_MASTER_UNIT);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(unitData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_opType", opType);
            request.AddParameter("records", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveDataIPL(int userID, string accessToken, List<trans_ipl_header> headerData, //List<trans_ipl_detail> detailData,
            int opType, out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_SUMMARY_IPL);
            var request = new RestRequest(Method.POST);
            var jsonHead = JsonConvert.SerializeObject(headerData);
            //var jsonDet = JsonConvert.SerializeObject(detailData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_opType", opType);

            request.AddParameter("headerParam", jsonHead);
            //request.AddParameter("detailParam", jsonDet);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public void returnQtyVoid(int userID, string accessToken, int idTrans, int transType)
        {
            var client = new RestClient(SERVER_URL + FUNC_RETURN_QTY_VOID);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_idTrans", idTrans);
            request.AddParameter("p_transType", transType);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
        }
    
        public bool saveDataTransaksi(int userID, string accessToken, List<transaksi_header> headerData, List<transaksi_detail> detailData,
            int opType, out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_TRANSAKSI);
            var request = new RestRequest(Method.POST);
            var jsonHead = JsonConvert.SerializeObject(headerData);
            var jsonDet = JsonConvert.SerializeObject(detailData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_opType", opType);

            request.AddParameter("headerTransParam", jsonHead);
            request.AddParameter("detailTransParam", jsonDet);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool savePenyesuaian(int userID, string accessToken, List<penyesuaian_stok> stokData, 
         out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_PENYESUAIAN_STOK);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(stokData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("records", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveDataPenerimaan(int userID, string accessToken, List<penerimaan_header> listHeader, List<penerimaan_detail> detailData,
          int opType, out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_PENERIMAAN);
            var request = new RestRequest(Method.POST);
            var jsonHead = JsonConvert.SerializeObject(listHeader);
            var jsonDet = JsonConvert.SerializeObject(detailData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_opType", opType);

            request.AddParameter("headerTransParam", jsonHead);
            request.AddParameter("detailTransParam", jsonDet);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveCustCareDetail(int userID, string accessToken, List<cc_ticket_history> listDetail, List<cc_ticket> listHeader, 
            List<imgAttachment> imgList, out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_CUSTCARE_DETAIL);
            var request = new RestRequest(Method.POST);
            var jsonHead = JsonConvert.SerializeObject(listHeader);
            var jsonDet = JsonConvert.SerializeObject(listDetail);
            var jsonImage = JsonConvert.SerializeObject(imgList);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);

            request.AddParameter("headerTransParam", jsonHead);
            request.AddParameter("detailTransParam", jsonDet);
            request.AddParameter("imgAttachmentParam", jsonImage);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveCCDetail(int userID, string accessToken, 
            int ticketID, string description, string isMTC, double mtcCost, string isConfirmation, string replyType,
            ccAttach[] listAttach, out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            //List<genericReply> replyResult = new List<genericReply>();
            List<chiki_reply> replyResult = new List<chiki_reply>();

            //replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_CC_DETAIL);
            var request = new RestRequest(Method.POST);

            var jSonAttach = JsonConvert.SerializeObject(listAttach);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_ticket_id", ticketID);
            request.AddParameter("p_description", description);
            request.AddParameter("p_is_mtc", isMTC);
            request.AddParameter("p_mtc_cost", mtcCost);
            request.AddParameter("p_is_confirmation", isConfirmation);
            request.AddParameter("p_reply_type", "admin");
            request.AddParameter("p_attach", jSonAttach);

            IRestResponse response = client.Execute(request);
            var restResult = response.Content;

            var jsonResponse = JsonConvert.DeserializeObject<List<chiki_reply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<chiki_reply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].o_history_id);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveCCWorkDetail(int userID, string accessToken,
            int ticketID, string startDate, string vendorName, out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<chiki_reply> replyResult = new List<chiki_reply>();

            var client = new RestClient(SERVER_URL + FUNC_CC_WORK);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_ticket_id", ticketID);
            request.AddParameter("p_start_date", startDate);
            request.AddParameter("p_vendor_name", vendorName);

            IRestResponse response = client.Execute(request);
            var restResult = response.Content;

            var jsonResponse = JsonConvert.DeserializeObject<List<chiki_reply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<chiki_reply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].o_history_id);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool closeCCTicket(int userID, string accessToken,
            int ticketID, out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<chiki_reply> replyResult = new List<chiki_reply>();

            var client = new RestClient(SERVER_URL + FUNC_CC_CLOSE);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_ticket_id", ticketID);

            IRestResponse response = client.Execute(request);
            var restResult = response.Content;

            var jsonResponse = JsonConvert.DeserializeObject<List<chiki_reply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<chiki_reply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].o_history_id);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool cc_confirmation(int userID, string accessToken,
            int ticketID, out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<chiki_reply> replyResult = new List<chiki_reply>();

            var client = new RestClient(SERVER_URL + FUNC_CC_CLOSE);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_ticket_id", ticketID);

            IRestResponse response = client.Execute(request);
            var restResult = response.Content;

            var jsonResponse = JsonConvert.DeserializeObject<List<chiki_reply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<chiki_reply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].o_history_id);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool updateCustCare(int userID, string accessToken, List<cc_status> listDetail,
           out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_UPDATE_CUSTCARE);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(listDetail);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("records", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveNews(int userID, string accessToken, List<news> newsData, int opType,
         out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_NEWS);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(newsData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_opType", opType);
            request.AddParameter("records", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool clearTagihanIPL(int userID, string accessToken, int userKavlingID, int kavlingID,
           out string errMsg)
        {
            errMsg = "";

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_CLEAR_TAGIHAN);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_user_kavling_id", userKavlingID);
            request.AddParameter("p_kavling_id", kavlingID);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveDataUserKavlingRFID(int userID, string accessToken, List<user_kavling_rfid> listHeader,
          out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_USER_KAVLING_RFID);
            var request = new RestRequest(Method.POST);
            var jsonHead = JsonConvert.SerializeObject(listHeader);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("records", jsonHead);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveDataKompleksTerminal(int userID, string accessToken, List<kompleks_terminal> listHeader,
          out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_KOMPLEKS_TERMINAL);
            var request = new RestRequest(Method.POST);
            var jsonHead = JsonConvert.SerializeObject(listHeader);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("records", jsonHead);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveDataAksesSatpam(int userID, string accessToken, List<satpam_access_list> listHeader,
              out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_SATPAM_ACCESS_LIST);
            var request = new RestRequest(Method.POST);
            var jsonHead = JsonConvert.SerializeObject(listHeader);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("records", jsonHead);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveDataUserAccessList(int userID, string accessToken, List<user_access_list> listHeader,
          out string errMsg)
        {
            errMsg = "";

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_USER_ACCESS_LIST);
            var request = new RestRequest(Method.POST);
            var jsonHead = JsonConvert.SerializeObject(listHeader);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("records", jsonHead);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool getSatpamLog(string sqlQuery, ref REST_satpamLog satpamData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATANOPARAM);
            var request = new RestRequest(Method.POST);

            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_satpamLog>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            satpamData = (REST_satpamLog)jsonResponse;

            if (satpamData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getDataSingleValueNoParam(string sqlComm, ref genericReply replyResult)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATASINGLEVALUENOPARAM);
            var request = new RestRequest(Method.POST);

            request.AddParameter("sqlQuery", sqlComm);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
                return false;

            replyResult = (genericReply)jsonResponse[0];

            if (replyResult.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getLiveServerUserList(string accessToken, ref List<server_user_all> listHeader)
        {
            var client = new RestClient(LIVE_SERVER_URL + FUNC_GET_LIVE_USER_LIST);
            var request = new RestRequest(Method.GET);

            request.AddParameter("token", accessToken);

            IRestResponse response = client.Execute(request);

            try
            {
                var restResult = response.Content;
                var jsonResponse = JsonConvert.DeserializeObject<List<server_user_all>>(response.Content);

                if (null == jsonResponse)
                {
                    return false;
                }

                listHeader = jsonResponse;
            }
            catch (Exception ex)
            {
                string startDate = String.Format(culture, "{0:yyyyMMddhhmmss}", DateTime.Now);
                string path = Application.StartupPath + "\\err\\ERR_getLiveServerUserList_" + startDate + ".txt";

                File.WriteAllText(path, response.Content);

                return false;
            }

            return true;
        }

        public bool getLiveKompleksTerminal(string accessToken, ref List<kompleks_terminal> listHeader)
        {
            var client = new RestClient(LIVE_SERVER_URL + FUNC_GET_LIVE_KOMPLEKS_TERMINAL);
            var request = new RestRequest(Method.GET);

            request.AddParameter("token", accessToken);

            IRestResponse response = client.Execute(request);

            try
            {
                var restResult = response.Content;
                var jsonResponse = JsonConvert.DeserializeObject<List<kompleks_terminal>>(response.Content);

                if (null == jsonResponse)
                {
                    return false;
                }

                listHeader = jsonResponse;
            }
            catch (Exception ex)
            {
                string startDate = String.Format(culture, "{0:yyyyMMddhhmmss}", DateTime.Now);
                string path = Application.StartupPath + "\\err\\ERR_getLiveKompleksTerminal_" + startDate + ".txt";

                File.WriteAllText(path, response.Content);

                return false;
            }

            return true;
        }

        public bool getLiveServerStatus(string accessToken)
        {
            var client = new RestClient(LIVE_SERVER_URL + FUNC_GET_LIVE_SERVER_STATUS);
            var request = new RestRequest(Method.GET);

            request.AddParameter("token", accessToken);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;

            if (null == restResult)
            {
                return false;
            }

            int result = 0;
            try
            {
                result = Convert.ToInt32(restResult);
            }
            catch (Exception ex) { }

            if (result == 0)
            {
                return false;
            }

            return true;
        }


        public bool truncateTerminalData(string accessToken)
        {
            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_LIVE_SERVER_TRUNCATE_TERMINAL);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public bool truncateUserList(string accessToken)
        {
            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_LIVE_SERVER_TRUNCATE_TABLE);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool saveUserList(string accessToken, List<server_user_all> userData)
        {
            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_LIVE_SERVER_SAVE_USER_LIST);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(userData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("server_userList", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool saveTerminalData(string accessToken, List<kompleks_terminal> terminalData)
        {
            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_LIVE_SERVER_SAVE_TERMINAL_LIST);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(terminalData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("list_terminal", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool savePullDuration(string accessToken, int durationValue)
        {
            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_LIVE_SERVER_SAVE_DURATION_PULL);
            var request = new RestRequest(Method.POST);

            request.AddParameter("token", accessToken);
            request.AddParameter("durationValue", durationValue);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            //var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == restResult)
            {
                return false;
            }

            //replyResult = (List<genericReply>)jsonResponse;

            if (Convert.ToInt32(restResult) == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
