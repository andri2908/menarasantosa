﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data;
using MySql.Data.MySqlClient;
using System.Globalization;
using System.Reflection;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;

namespace AlphaSoft
{
    class globalPOSPrinter
    {
        private string sqlCommandToPrint;
        private string identifierParameter = "";
        private string identifierParameter2 = "";
        private string selectedPeriode = "";
        private string selectedPersonnelID = "";

        private int identifierUserIDParam = 0;
        private DateTime identifierDateTimeParam = DateTime.Now;

        private string startDate = "";
        private string endDate = "";

        private Data_Access DS = new Data_Access();

        private globalUtilities gUtil = new globalUtilities();
        private globalPrinterUtility gPrinter = new globalPrinterUtility();
        private CultureInfo culture = new CultureInfo("id-ID");
        private System.Drawing.Printing.PrintDocument printDocument1 = new System.Drawing.Printing.PrintDocument();
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1 = new PrintPreviewDialog();

        private System.Drawing.Printing.PrintDocument printDocument2 = new System.Drawing.Printing.PrintDocument();
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog2 = new PrintPreviewDialog();

        private System.Windows.Forms.SaveFileDialog saveFileDialog1 = new SaveFileDialog();

        int paperLength = 500;

        private void setSingleSQLContent()
        {
            sqlCommandToPrint = "SELECT MU.USER_NAME, " +
                                            "DATE_FORMAT(CL.DATE_LOGIN, '%d-%m-%y %h:%i') AS LOGIN, DATE_FORMAT(CL.DATE_LOGOUT, '%d-%m-%y %h:%i') AS LOGOUT, " +
                                            "CL.AMOUNT_START, CL.AMOUNT_END, " +
                                            "CL.TOTAL_CASH_TRANSACTION AS TOTAL_CASH_TRANSACTION, " +
                                            "CL.TOTAL_NON_CASH_TRANSACTION AS TOTAL_NON_CASH_TRANSACTION, " +
                                            "CL.TOTAL_OTHER_TRANSACTION AS TOTAL_OTHER_TRANSACTION " +
                                            "FROM MASTER_USER MU, CASHIER_LOG CL " +
                                            "WHERE CL.USER_ID = MU.ID " +
                                            "AND CL.USER_ID = " + identifierParameter + " " +
                                            "AND CL.ID = " + identifierParameter2;
        }

        private void setSQLContent()
        {
            string dateStr = String.Format(culture, "{0:yyyyMMdd}", identifierDateTimeParam);
            string dateStrDisplay = gUtil.getCustomStringFormatDate(identifierDateTimeParam);

            //sqlCommandToPrint = "SELECT MU.USER_NAME, " +
            //                                "DATE_FORMAT(CL.DATE_LOGIN, '%d-%m-%y %h:%i') AS LOGIN, DATE_FORMAT(CL.DATE_LOGOUT, '%d-%m-%y %h:%i') AS LOGOUT, " +
            //                                "CL.AMOUNT_START, CL.AMOUNT_END, " +
            //                                "SUM(CL.TOTAL_CASH_TRANSACTION) AS TOTAL_CASH_TRANSACTION, " +
            //                                "SUM(CL.TOTAL_NON_CASH_TRANSACTION) AS TOTAL_NON_CASH_TRANSACTION, " +
            //                                "SUM(CL.TOTAL_OTHER_TRANSACTION) AS TOTAL_OTHER_TRANSACTION " +
            //                                "FROM MASTER_USER MU, CASHIER_LOG CL " +
            //                                "WHERE CL.USER_ID = MU.ID " +
            //                                "AND CL.USER_ID = " + identifierUserIDParam +
            //                                "AND DATE_FORMAT(DATE_LOGIN, '%Y%m%d') >= '" + dateStr + "' " +
            //                                "AND DATE_FORMAT(DATE_LOGOUT, '%Y%m%d') >= '" + dateStr + "' ";

            string sqlCashierLog = "SELECT CL.USER_ID, " +
                                            "SUM(CL.TOTAL_CASH_TRANSACTION) AS TOTAL_CASH_TRANSACTION, " +
                                            "SUM(CL.TOTAL_NON_CASH_TRANSACTION) AS TOTAL_NON_CASH_TRANSACTION, " +
                                            "SUM(CL.TOTAL_OTHER_TRANSACTION) AS TOTAL_OTHER_TRANSACTION " +
                                            "FROM CASHIER_LOG CL " +
                                            "WHERE CL.USER_ID = " + identifierUserIDParam + " " +
                                            "AND DATE_FORMAT(CL.DATE_LOGIN, '%Y%m%d') = '" + dateStr + "' " +
                                            "AND DATE_FORMAT(CL.DATE_LOGOUT, '%Y%m%d') = '" + dateStr + "' " +
                                            "GROUP BY CL.USER_ID";

            string sqlStartAmount = "SELECT CL.USER_ID, CL.AMOUNT_START " +
                                              "FROM CASHIER_LOG CL " +
                                              "WHERE CL.USER_ID = " + identifierUserIDParam + " " +
                                              "AND DATE_FORMAT(CL.DATE_LOGIN, '%Y%m%d') = '" + dateStr + "' " +
                                              "AND DATE_FORMAT(CL.DATE_LOGOUT, '%Y%m%d') = '" + dateStr + "' " +
                                              "ORDER BY CL.DATE_LOGIN ASC LIMIT 1";

            string sqlEndAmount = "SELECT CL.USER_ID, CL.AMOUNT_END " +
                                              "FROM CASHIER_LOG CL " +
                                              "WHERE CL.USER_ID = " + identifierUserIDParam + " " +
                                              "AND DATE_FORMAT(CL.DATE_LOGIN, '%Y%m%d') = '" + dateStr + "' " +
                                              "AND DATE_FORMAT(CL.DATE_LOGOUT, '%Y%m%d') = '" + dateStr + "' " +
                                              "ORDER BY CL.DATE_LOGOUT DESC LIMIT 1";

            sqlCommandToPrint = "SELECT MU.USER_NAME, '" + dateStrDisplay + "' AS DATETRANS, " +
                                            "CL1.AMOUNT_START, CL2.AMOUNT_END, " +
                                            "CL3.TOTAL_CASH_TRANSACTION, CL3.TOTAL_NON_CASH_TRANSACTION, CL3.TOTAL_OTHER_TRANSACTION " +
                                            "FROM MASTER_USER MU " +
                                            "LEFT OUTER JOIN (" + sqlStartAmount + ") CL1 ON (CL1.USER_ID = MU.ID) " +
                                            "LEFT OUTER JOIN (" + sqlEndAmount + ") CL2 ON (CL2.USER_ID = MU.ID) " +
                                            "LEFT OUTER JOIN (" + sqlCashierLog + ") CL3 ON (CL3.USER_ID = MU.ID) " +
                                            "WHERE MU.ID = " + identifierUserIDParam;
        }

        public void printPOSPaper(string identifierValue, string identifierValue2)
        {
            identifierParameter = identifierValue;
            identifierParameter2 = identifierValue2;

            //selectedPeriode = getPeriodeValue(identifierValue);
            //selectedPersonnelID = identifierValue2;

            //setSQLContent();
            setSingleSQLContent();

            doPrintReport();
        }

        public void printPOSPaperCL(int userIDParam, DateTime dateTimeParam)
        {
            identifierUserIDParam = userIDParam;
            identifierDateTimeParam = dateTimeParam;

            //selectedPeriode = getPeriodeValue(identifierValue);
            //selectedPersonnelID = identifierValue2;

            setSQLContent();

            doPrintReport();
        }

        private int calculatePaperLength()
        {
            int result = 500;
            int Offset = 5;
            Font font = new Font("Courier New", 9);
            int rowheight = (int)Math.Ceiling(font.GetHeight());

            int add_offset = rowheight;

            Offset = Offset + add_offset;

            Offset = Offset + add_offset;

            Offset = Offset + add_offset;

            Offset = Offset + add_offset;

            Offset = Offset + add_offset;

            Offset = Offset + add_offset;

            Offset = Offset + add_offset;

            Offset = Offset + add_offset;

            Offset = Offset + add_offset;

                    Offset = Offset + add_offset;

                    Offset = Offset + add_offset;

                    Offset = Offset + add_offset;

            Offset = Offset + add_offset;
            Offset = Offset + add_offset;
            Offset = Offset + add_offset;

            result = Offset + add_offset + add_offset + add_offset + add_offset;

            return result;
        }

        private void printPos_Page(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            String ucapan = "";
            MySqlDataReader rdr;
            string nm, almt, tlpn, email;

            double totalIncome = 0;
            double totalPotongan = 0;
            double grandTotal = 0;
            
            //event printing
            Graphics graphics = e.Graphics;
            int startX = 0;
            int startY = 0;
            int colxwidth = 85; //old 75
            int totrowwidth = 255; //old 250

            int padValue = 18;
            int fontSize = 8;

            Font font = new Font("Courier New", 9);
            int rowheight = (int)Math.Ceiling(font.GetHeight());

            int add_offset = rowheight;
            int Offset = 5;
            int offset_plus = 3;
            String underLine = "-----------------------------------";  //32 character
            string sqlCommand = "";
            
            //set allignemnt
            StringFormat sf = new StringFormat();
            sf.LineAlignment = StringAlignment.Center;
            sf.Alignment = StringAlignment.Center;

            //set whole printing area
            System.Drawing.RectangleF rect = new System.Drawing.RectangleF(startX, startY + Offset, totrowwidth, rowheight);
            //set right print area
            System.Drawing.RectangleF rectright = new System.Drawing.RectangleF(totrowwidth - colxwidth - startX, startY + Offset, colxwidth, rowheight);
            //set middle print area
            System.Drawing.RectangleF rectcenter = new System.Drawing.RectangleF((startX + totrowwidth), startY + Offset, colxwidth, rowheight);
            ///string dateFrom = String.Format(culture, "{0:yyyyMMdd}", DateTime.Now);

            Offset = Offset + add_offset;
            rect.Y = startY + Offset;
            graphics.DrawString(underLine, new Font("Courier New", fontSize),
                     new SolidBrush(Color.Black), rect, sf);
            
            Offset = Offset + add_offset;
            rect.Y = startY + Offset;
            gUtil.loadinfotoko(2, out nm, out almt, out tlpn, out email);

            graphics.DrawString(nm, new Font("Courier New", fontSize),
                                new SolidBrush(Color.Black), rect, sf);

            Offset = Offset + add_offset;
            rect.Y = startY + Offset;
            graphics.DrawString(underLine, new Font("Courier New", fontSize),
                     new SolidBrush(Color.Black), rect, sf);

            sf.LineAlignment = StringAlignment.Near;
            sf.Alignment = StringAlignment.Near;

            totalIncome = 0;
            using (rdr = DS.getData(sqlCommandToPrint))
            {
                if (rdr.HasRows)
                {
                    rdr.Read();

                    Offset = Offset + add_offset;
                    rect.Y = startY + Offset;
                    rect.X = startX + 10;
                    ucapan = "Nama User ";
                    ucapan = ucapan.PadRight(10, ' ') + rdr.GetString("USER_NAME");

                    graphics.DrawString(ucapan, new Font("Courier New", fontSize),
                                        new SolidBrush(Color.Black), rect, sf);

                    //Offset = Offset + add_offset;
                    //rect.Y = startY + Offset;
                    //rect.X = startX + 10;
                    //ucapan = "Tgl ";
                    //ucapan = ucapan.PadRight(10, ' ') + rdr.GetString("DATETRANS");

                    //graphics.DrawString(ucapan, new Font("Courier New", fontSize),
                    //                    new SolidBrush(Color.Black), rect, sf);

                    Offset = Offset + add_offset;
                    rect.Y = startY + Offset;
                    rect.X = startX + 10;
                    ucapan = "Login ";
                    ucapan = ucapan.PadRight(10, ' ') + rdr.GetString("LOGIN");

                    graphics.DrawString(ucapan, new Font("Courier New", fontSize),
                                        new SolidBrush(Color.Black), rect, sf);

                    Offset = Offset + add_offset;
                    rect.Y = startY + Offset;
                    rect.X = startX + 10;
                    ucapan = "LogOut ";
                    ucapan = ucapan.PadRight(10, ' ') + rdr.GetString("LOGOUT");

                    graphics.DrawString(ucapan, new Font("Courier New", fontSize),
                                        new SolidBrush(Color.Black), rect, sf);

                    Offset = Offset + add_offset;
                    rect.Y = startY + Offset;
                    graphics.DrawString(underLine, new Font("Courier New", fontSize),
                             new SolidBrush(Color.Black), rect, sf);


                    Offset = Offset + add_offset;
                    rect.Y = startY + Offset;
                    rect.X = startX + 10;
                    ucapan = "Kas Awal ";
                    ucapan = ucapan.PadRight(10, ' ') + rdr.GetDouble("AMOUNT_START").ToString("C", culture).PadLeft(padValue, ' ');

                    graphics.DrawString(ucapan, new Font("Courier New", fontSize),
                                        new SolidBrush(Color.Black), rect, sf);

                    Offset = Offset + add_offset;
                    rect.Y = startY + Offset;
                    rect.X = startX + 10;
                    ucapan = "Kas Masuk ";
                    ucapan = ucapan.PadRight(10, ' ') + rdr.GetDouble("TOTAL_CASH_TRANSACTION").ToString("C", culture).PadLeft(padValue, ' ');

                    graphics.DrawString(ucapan, new Font("Courier New", fontSize),
                                        new SolidBrush(Color.Black), rect, sf);

                    Offset = Offset + add_offset;
                    rect.Y = startY + Offset;
                    rect.X = startX + 10;
                    ucapan = "Kas Lain ";
                    ucapan = ucapan.PadRight(10, ' ') + rdr.GetDouble("TOTAL_OTHER_TRANSACTION").ToString("C", culture).PadLeft(padValue, ' ');

                    graphics.DrawString(ucapan, new Font("Courier New", fontSize),
                                        new SolidBrush(Color.Black), rect, sf);

                    Offset = Offset + add_offset;
                    rect.Y = startY + Offset;
                    graphics.DrawString(underLine, new Font("Courier New", fontSize),
                             new SolidBrush(Color.Black), rect, sf);

                    Offset = Offset + add_offset;
                    rect.Y = startY + Offset;
                    rect.X = startX + 10;
                    ucapan = "Total ";
                    ucapan = ucapan.PadRight(10, ' ') + (rdr.GetDouble("AMOUNT_START") + rdr.GetDouble("TOTAL_CASH_TRANSACTION") + rdr.GetDouble("TOTAL_OTHER_TRANSACTION")).ToString("C", culture).PadLeft(padValue, ' ');

                    graphics.DrawString(ucapan, new Font("Courier New", fontSize),
                                        new SolidBrush(Color.Black), rect, sf);

                    Offset = Offset + add_offset;
                    rect.Y = startY + Offset;
                    rect.X = startX + 10;
                    ucapan = "Kas Akhir ";
                    ucapan = ucapan.PadRight(10, ' ') + rdr.GetDouble("AMOUNT_END").ToString("C", culture).PadLeft(padValue, ' ');

                    graphics.DrawString(ucapan, new Font("Courier New", fontSize),
                                        new SolidBrush(Color.Black), rect, sf);

                    Offset = Offset + add_offset;
                    rect.Y = startY + Offset;
                    graphics.DrawString(underLine, new Font("Courier New", fontSize),
                             new SolidBrush(Color.Black), rect, sf);

                    Offset = Offset + add_offset;
                    rect.Y = startY + Offset;
                    rect.X = startX + 10;
                    ucapan = "Selisih ";
                    ucapan = ucapan.PadRight(10, ' ') + Math.Abs((rdr.GetDouble("AMOUNT_END") - (rdr.GetDouble("AMOUNT_START") + rdr.GetDouble("TOTAL_CASH_TRANSACTION") + rdr.GetDouble("TOTAL_OTHER_TRANSACTION")))).ToString("C", culture).PadLeft(padValue, ' ');

                    graphics.DrawString(ucapan, new Font("Courier New", fontSize),
                                        new SolidBrush(Color.Black), rect, sf);

                    Offset = Offset + add_offset;
                    rect.Y = startY + Offset;
                    graphics.DrawString(underLine, new Font("Courier New", fontSize),
                             new SolidBrush(Color.Black), rect, sf);

                    Offset = Offset + add_offset;
                    rect.Y = startY + Offset;
                    rect.X = startX + 10;
                    ucapan = "Non Tunai ";
                    ucapan = ucapan.PadRight(10, ' ') + rdr.GetDouble("TOTAL_NON_CASH_TRANSACTION").ToString("C", culture).PadLeft(padValue, ' ');

                    graphics.DrawString(ucapan, new Font("Courier New", fontSize),
                                        new SolidBrush(Color.Black), rect, sf);

                    Offset = Offset + add_offset;
                    rect.Y = startY + Offset;
                    graphics.DrawString(underLine, new Font("Courier New", fontSize),
                             new SolidBrush(Color.Black), rect, sf);
                }
            }
            rdr.Close();

            sf.LineAlignment = StringAlignment.Center;
            sf.Alignment = StringAlignment.Center;

        }
     
        private void doPrintReport()
        {
            printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printPos_Page);
            printPreviewDialog1.Document = printDocument1;

            paperLength = calculatePaperLength();

            PaperSize psize = new PaperSize("Custom", 255, paperLength);
            printDocument1.DefaultPageSettings.PaperSize = psize;
            DialogResult result;
            printPreviewDialog1.Width = 512;
            printPreviewDialog1.Height = 768;

            result = printPreviewDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                printDocument1.Print();
            }
        }
     }
}
