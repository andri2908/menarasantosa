<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Validator;
use Intervention\image\Facades\Image;
use Carbon\Carbon;

date_default_timezone_set('Asia/Jakarta');

class returnStatus_Update{
    public $Status;
    public $Message;
}

class emp{
    public $Status;
    public $CardNo;
    public $FullName;
    public $UTC;
    public $Message;
    public $recordCount;
}            


class returnStatus{
    public $Status;
}

class winAPP_Controller extends Controller
{  
    public function testDate(){
        $current = Carbon::now();
        echo $current->format('Y-m-d H:i:s') . "\n";
        echo "<br>";
        $current->addHour(12);
        echo $current->format('Y-m-d H:i:s') . "\n";
    }

    public function checkToken($userID, $accessToken){
        try
        {
            $query = "  
            select  count(1) as num
            from    user_token ut
            where   ut.user_id = ".$userID." 
                    and ut.access_token = '". $accessToken ."'";

            $result = \DB::select(\DB::raw($query));

            return $result[0]->num;
        }
        catch(Exception $e) 
        { 
            return 0;
        }
    }

    public function checkExternalToken($accessToken){
     
        $tokenDefault = "037f6587a54504d3f512881c7ce30209"; // menarasantosa

        try
        {
            if ($accessToken == $tokenDefault)
                return 1;
            else
                return 0;
        }
        catch(Exception $e) 
        { 
            return 0;
        }
    }   

    public function getDataSingleValueNoParam(Request $request){
       
        $validation = Validator::make($request->all(),[ 
            'sqlQuery'              => 'required',
        ]);

        if($validation->fails())
        {
            $errors = $validation->errors();
            return $errors->toJson();
        }

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $query   = $request->get('sqlQuery');

            $result = \DB::select(\DB::raw($query));

            $result = response()->json([[
                        'data' => $result[0]->resultQuery, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);       
        } 
        catch (Exception $e) 
        {
            $result = response()->json([[
                'data' => 0,
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }
	
	public function getDataNoParam(Request $request){
        $validation = Validator::make($request->all(),[ 
            'sqlQuery'              => 'required',
        ]);

        if($validation->fails())
        {
            $errors = $validation->errors();
            return $errors->toJson();
        }

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $query = $request->get('sqlQuery');
            $data = \DB::select(\DB::raw($query));

            $result = response()->json(
                ['dataList' => $data,
                'dataStatus' => 
                    ['data' => 0,
                    'o_status'  => 1,
                    'o_message' => "success",]
                ], 200);
        } 
        catch (Exception $e) 
        {
            $result = response()->json([[
                'data' => 0,
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;    
    }

    public function displaySatpam(Request $request){
        $errResponse = new returnStatus();
        $errResponse->Status = 0;

        $validation = Validator::make($request->all(),[ 
            'token'        => 'required',
        ]);

        if($validation->fails()){
            $result = json_encode($errResponse);
            return $result;
        }

        $token = $request->get('token');
        $accessStatus = $request->get('Status');
        $userFullName =  $request->get('FullName');
        $accessID = $request->get('CardNo');
        $timeutc = $request->get('UTC');

        date_default_timezone_set('Asia/Jakarta');
        $time1 = strtotime($timeutc.' UTC');
        $timeStamp = date("Y-m-d H:i:s", $time1);
        
        $tokenValid = $this->checkExternalToken($token);
        if ($tokenValid == 1)
        {
            try 
            {    
                $rows = \DB::table('satpam_log')
                ->insertGetId([
                    'access_status' => $accessStatus,
                    'user_full_name' => $userFullName,
                    'access_id' => $accessID,
                    'timestamp' => $timeStamp ,
                    'creation_date' => new \DateTime()
                ]);   

                \DB::commit();   

                $errResponse->Status = 1;
            }
            catch (Exception $e) 
            {
            }    
        }   

        $result = json_encode($errResponse);
        return $result;
    }

    public function truncateUserList(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token = $request->get('p_access_token');
            $num = $this->checkExternalToken($p_access_token);

            if ($num > 0)
            {
                \DB::table('user_access_temp_all')->truncate();
                \DB::commit();
    
                $result = response()->json([[
                        'data' => 1, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function saveUserList(Request $request){
        $validation = Validator::make($request->all(),[ 
            'p_access_token'        => 'required',
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return $errors->toJson();
        }
        
        \DB::beginTransaction();

        $result = response()->json([[
            'data' => 0, 
            'o_status'  => -1,
            'o_message' => "Fail",
            ]], 200);

        try 
        {
            $p_access_token         = $request->get('p_access_token');
            $num = $this->checkExternalToken($p_access_token);

            if ($num > 0)
            {
                $userData = $request->get('server_userList');
                $server_userList = json_decode($userData);

                // SAVE DETAIL
                foreach ($server_userList as $queryData) {
                    $rows = \DB::table('user_access_temp_all')
                    ->insertGetId([
                        'user_full_name' => $queryData->user_full_name,
                        'terminal_id' => $queryData->terminal_id,
                        'access_id' => $queryData->access_id,
                        'access_type' => $queryData->access_type,
                        'access_status' => $queryData->access_status,
                        'message' => $queryData->message,
                    ]);
                }
    
                $rows = \DB::table('sync_status')
                ->where('id', 1)
                ->update([
                    'server_last_sync' => new \DateTime(),
                ]);

                \DB::commit();
    
                $result = response()->json([[
                        'data' => 1, 
                        'o_status' => 1,
                        'o_message' => "success",        
                        ]], 200);            
            }
        }
        catch (Exception $e) 
        {
            \DB::rollback();
            $result = response()->json([[
                'data' => 0, 
                'o_status'  => -1,
                'o_message' => $e->getMessage(),
            ]], 200);
        }

        return $result;
    }

    public function fillInTempTable($terminalID, $accessType){
        $result = 1;    
        try
        {
            $query = "
            select * 
            from user_access_temp_all 
            where terminal_id = '".$terminalID."'
            and access_type = '".$accessType."'
            ";

            $data = \DB::select(\DB::raw($query));

            foreach ($data as $queryData) {       
                $rows = \DB::table('user_access_temp')
                ->insertGetId([
                    'user_full_name' => $queryData->user_full_name,
                    'access_id' => $queryData->access_id,
                    'access_type' => $accessType,
                    'access_status' => $queryData->access_status,
                    'message' => $queryData->message
                ]);         
            }

            \DB::commit();   
        }
        catch(Exception $e)
        {
            $result = 0;
        }

        return $result;
    }

    public function dropCreateTempTable(){
        $result = 1;
        try{
            $queryDrop = "DROP TABLE IF EXISTS `user_access_temp`";    
            $rows = \DB::update($queryDrop);   
    
            $queryTable ="CREATE TABLE user_access_temp (
                id bigint(10) unsigned NOT NULL AUTO_INCREMENT,
                user_full_name varchar(100) DEFAULT '',
                access_id varchar(100) DEFAULT '',
                access_type enum('QR','RFID') DEFAULT 'QR',
                access_status tinyint(3) DEFAULT '1',
                message varchar(100) DEFAULT 'Welcome...',
                PRIMARY KEY (id)
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1"; 
        
            $rows = \DB::update($queryTable);                
            }
        catch (Exception $e)
        {
            $result = 0;
        }

        return $result;
    }

    public function getUserList(Request $request){
        $validation = Validator::make($request->all(),[ 
            'token'        => 'required',
        ]);

        if($validation->fails()){
            $errResponse = new emp();	
            $errResponse->Status = 0;
            $errResponse->CardNo = null;
            $errResponse->FullName = "Unknown";
            $errResponse->UTC = gmdate("d-m-Y H:i:s");
            $errResponse->Message = "Failed [vl]!";
            $result = json_encode($errResponse);

            return $result;
        }

        $token = $request->get('token');
        $accessType = $request->get('accessType');
        $terminalID = $request->get('terminalID');
        $seqNo =  $request->get('seqNo');

        $maxRecord = 5;//20;

        $tokenValid = $this->checkExternalToken($token);
        if ($tokenValid == 1)
        {   
            if ($seqNo == 1)
            {
                $this->dropCreateTempTable();              
                $this->fillInTempTable($terminalID, $accessType);    
                
                // SAVE KE SYNC DATE
                $rows = \DB::table('sync_status')
                ->where('id', 1)
                ->update([
                    'device_last_sync' => new \DateTime(),
                ]);
            }

            try 
            {    
                $responseResult = array();

                $query = "select * from user_access_temp";

                $data = \DB::select(\DB::raw($query));
                $recordCount = count($data);
             
                $i = 0;
                foreach ($data as $queryData) {                
                    if ($i >= (($seqNo-1) * $maxRecord))
                    {
                        $response = new emp();          
                        $response->Status = intval($queryData->access_status);    
                        $response->CardNo = $queryData->access_id;    
                        $response->FullName = $queryData->user_full_name;    
                        $response->UTC = gmdate("d-m-Y H:i:s");    
                        $response->Message =  $queryData->message;    
                        $response->recordCount = $recordCount;
                        $responseResult[] = $response;        
                    }

                    $i+=1;
                    if ($i >= ($seqNo * $maxRecord))
                    {
                        break;
                    }
                }
                
                \DB::commit();

                $result = json_encode($responseResult);
            }
            catch (Exception $e) 
            {
                $errResponse = new emp();	
                $errResponse->Status = 0;
                $errResponse->CardNo = null;
                $errResponse->FullName = "Unknown";
                $errResponse->UTC = gmdate("d-m-Y H:i:s");
                $errResponse->Message = "Failed [tc]!";
                $response->recordCount = 0;
                $result = json_encode($errResponse);
            }    
        }   
        else
        {
            $errResponse = new emp();	
            $errResponse->Status = 0;
            $errResponse->CardNo = null;
            $errResponse->FullName = "Unknown";
            $errResponse->UTC = gmdate("d-m-Y H:i:s");
            $errResponse->Message = "Failed [tk]!";
            $response->recordCount = 0;
            $result = json_encode($errResponse);
        } 

        return $result;
    }

    public function saveServerSyncDate(Request $request){        
        $errResponse = new returnStatus();
        $errResponse->Status = 0;

        $validation = Validator::make($request->all(),[ 
            'token'        => 'required',
        ]);

        if($validation->fails()){
            $result = json_encode($errResponse);
            return $result;
        }

        $token = $request->get('token');
        $tokenValid = $this->checkExternalToken($token);
        if ($tokenValid == 1)
        {
            try 
            {    
                   // SAVE KE SYNC DATE
                $rows = \DB::table('sync_status')
                   ->where('id', 1)
                   ->update([
                       'server_last_sync' => new \DateTime(),
                ]);

                \DB::commit();   

                $errResponse->Status = 1;
            }
            catch (Exception $e) 
            {
            }    
        }   

        $result = json_encode($errResponse);
        return $result;
    }

    public function saveFrekuensiSync(Request $request){        
        $errResponse = new returnStatus();
        $errResponse->Status = 0;

        $validation = Validator::make($request->all(),[ 
            'token'        => 'required',
        ]);

        if($validation->fails()){
            $result = 0;
            return $result;
        }

        $token = $request->get('token');
        $durationValue = $request->get('durationValue');

        $tokenValid = $this->checkExternalToken($token);
        if ($tokenValid == 1)
        {
            try 
            {    
                   // SAVE KE SYNC DATE
                $rows = \DB::table('sync_status')
                   ->where('id', 1)
                   ->update([
                       'server_pull_duration' => $durationValue,
                ]);

                \DB::commit();   

                $result = 1;
            }
            catch (Exception $e) 
            {
            }    
        }   

        return $result;
    }
}