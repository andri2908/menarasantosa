<?php
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*=======================  MidTrans ================================*/
/*$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->post("/coba", "MidtransController@getSnapToken");
    $router->post("/coba/charge", "MidtransController@getSnapToken");
});
*/

/*=======================  CONFIGURATION ================================*/

/*=======================  GLOBAL ================================*/

/*=======================  MAIN ================================*/

/*=======================  winApp ================================*/
$router->post('/getDataSingleValueNoParam', 'winAPP_Controller@getDataSingleValueNoParam');
$router->post('/getDataNoParam', 'winAPP_Controller@getDataNoParam');

$router->post('/truncateUserList', 'winAPP_Controller@truncateUserList');
$router->post('/saveUserList', 'winAPP_Controller@saveUserList');
$router->post('/saveServerSyncDate', 'winAPP_Controller@saveServerSyncDate');
$router->post('/saveFrekuensiSync', 'winAPP_Controller@saveFrekuensiSync');

$router->get('/displaySatpam', 'winAPP_Controller@displaySatpam');
$router->get('/getUserList', 'winAPP_Controller@getUserList');
/*=======================  LOV ================================*/



