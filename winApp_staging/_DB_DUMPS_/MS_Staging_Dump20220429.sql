CREATE DATABASE  IF NOT EXISTS `u1311857_sys_staging` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `u1311857_sys_staging`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: u1311857_sys_staging
-- ------------------------------------------------------
-- Server version	5.7.34-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `data_flow_control`
--

DROP TABLE IF EXISTS `data_flow_control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_flow_control` (
  `task_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `task_name` varchar(45) DEFAULT '',
  `task_parent` bigint(10) DEFAULT '0',
  `task_duration` double DEFAULT '0',
  `department_id` bigint(10) DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_update_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_flow_control`
--

LOCK TABLES `data_flow_control` WRITE;
/*!40000 ALTER TABLE `data_flow_control` DISABLE KEYS */;
/*!40000 ALTER TABLE `data_flow_control` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_department`
--

DROP TABLE IF EXISTS `master_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_department` (
  `department_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `department_name` varchar(45) DEFAULT '',
  `department_description` varchar(200) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_department`
--

LOCK TABLES `master_department` WRITE;
/*!40000 ALTER TABLE `master_department` DISABLE KEYS */;
INSERT INTO `master_department` VALUES (1,'ADMIN KONSUMEN','BAGIAN KONSUMEN','Y',1,'2022-04-27 21:48:28',1,'2022-04-27 21:52:42'),(2,'ADMIN LEGAL','BAGIAN LEGAL','Y',1,'2022-04-27 21:48:38',1,'2022-04-27 21:52:48'),(3,'ADMIN TEKNIS','BAGIAN TEKNIS','Y',1,'2022-04-27 21:48:46',1,'2022-04-27 21:52:55');
/*!40000 ALTER TABLE `master_department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_group`
--

DROP TABLE IF EXISTS `master_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_group` (
  `group_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL DEFAULT '',
  `group_description` varchar(50) NOT NULL DEFAULT '',
  `group_type` tinyint(3) DEFAULT '0',
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_group`
--

LOCK TABLES `master_group` WRITE;
/*!40000 ALTER TABLE `master_group` DISABLE KEYS */;
INSERT INTO `master_group` VALUES (1,'ADMIN GROUP','ADMIN',2,'Y',1,'2021-08-20 22:22:59',1,'2022-04-27 20:20:09'),(2,'GROUP PENJUALAN','',0,'Y',1,'2022-04-27 20:17:13',NULL,NULL);
/*!40000 ALTER TABLE `master_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_kavling`
--

DROP TABLE IF EXISTS `master_kavling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_kavling` (
  `kavling_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `kompleks_id` bigint(10) DEFAULT '0',
  `blok_id` bigint(10) DEFAULT '0',
  `house_no` varchar(45) DEFAULT '',
  `biaya_ipl` double DEFAULT '0',
  `durasi_pembayaran` int(10) DEFAULT '0',
  `status` enum('Dihuni sendiri','Disewakan','Kosong') DEFAULT 'Kosong',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`kavling_id`),
  KEY `secondary` (`kompleks_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_kavling`
--

LOCK TABLES `master_kavling` WRITE;
/*!40000 ALTER TABLE `master_kavling` DISABLE KEYS */;
INSERT INTO `master_kavling` VALUES (1,1,1,'F0100',250000,2,'Kosong','Y',1,'2021-07-07 23:26:38',1,'2022-02-09 12:40:21'),(2,1,1,'20',20000,3,'Kosong','Y',1,'2021-07-07 23:36:52',1,'2022-02-09 12:40:21'),(3,2,3,'55',1500000,12,'Kosong','Y',1,'2021-08-03 23:48:49',1,'2022-02-09 15:07:27'),(4,2,3,'A-1',5000,20,'Kosong','Y',1,'2021-08-24 07:23:51',1,'2022-02-09 14:33:45'),(5,1,1,'132',20000,3,'Kosong','Y',1,'2021-10-22 00:07:17',1,'2022-02-09 12:40:21'),(6,1,5,'122',1250000,12,'Dihuni sendiri','Y',1,'2021-10-23 14:53:04',1,'2022-02-09 12:40:21'),(7,3,12,'1',150000,1,'Disewakan','Y',1,'2021-10-23 14:57:45',1,'2021-12-30 16:10:44'),(8,3,15,'30',400000,3,'Kosong','Y',1,'2021-10-23 16:11:07',1,'2021-12-30 16:10:44'),(9,4,19,'23',450000,12,'Kosong','Y',1,'2021-10-27 16:20:24',1,'2021-10-27 16:45:40'),(10,4,20,'75',300000,6,'Kosong','Y',1,'2021-10-27 16:34:05',1,'2021-12-30 16:03:17'),(11,3,16,'50',350000,3,'Kosong','Y',1,'2021-10-27 16:47:16',1,'2021-12-30 16:10:44'),(12,3,14,'9',150000,3,'Disewakan','Y',1,'2021-12-30 15:19:49',1,'2021-12-30 16:10:44'),(13,2,21,'25',250000,6,'Dihuni sendiri','Y',1,'2021-12-30 15:52:21',1,'2022-02-09 14:33:45'),(14,2,3,'100',250000,6,'Kosong','Y',1,'2022-01-19 17:01:27',1,'2022-02-09 14:33:45'),(15,2,26,'06',250000,6,'Kosong','Y',1,'2022-02-08 13:28:31',1,'2022-02-09 14:33:45'),(16,2,26,'50',1350000,12,'Kosong','Y',1,'2022-02-08 18:07:05',1,'2022-02-09 14:52:00'),(17,1,1,'11+15',866667,12,'Kosong','Y',1,'2022-02-09 11:30:40',1,'2022-02-09 12:40:21');
/*!40000 ALTER TABLE `master_kavling` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_kompleks`
--

DROP TABLE IF EXISTS `master_kompleks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_kompleks` (
  `kompleks_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `kompleks_name` varchar(50) DEFAULT '',
  `kompleks_address` varchar(200) DEFAULT '',
  `biaya_ipl` double DEFAULT '0',
  `durasi_pembayaran` int(10) DEFAULT '0',
  `img_kompleks` varchar(100) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) unsigned DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`kompleks_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_kompleks`
--

LOCK TABLES `master_kompleks` WRITE;
/*!40000 ALTER TABLE `master_kompleks` DISABLE KEYS */;
INSERT INTO `master_kompleks` VALUES (1,'PERMATA VILLAGE','JL. PERMATA RAYA UTARA - BLULUKAN - COLOMADU - KARANGANYAR',866667,3,'a8468e62fabc32ab7f118e19924dc989.jpg','Y',1,'2021-07-07 01:02:58',1,'2022-02-09 11:29:55'),(2,'PERMATA PARKVIEW','KERTONATAN, KARTASURA',250000,6,'f5f45c2fafa4ab4adee9b1140a35e209.jpg','Y',1,'2021-08-03 23:00:22',1,'2022-02-08 13:28:06'),(3,'MENARA ONE','SOLO',150000,1,'3bd7872fc312d384a5070f5259fe6e0d.jpg','Y',1,'2021-10-23 14:57:18',1,'2021-12-30 16:26:22'),(4,'PERMATA BOTANICAL','DUSUN 1, SINGOPURAN, KARTASURA, SUKOHARJO',2500,6,'2bab44e3d0366f727c3b7235a2bbe4c7.jpg','Y',1,'2021-10-27 16:17:09',1,'2021-12-30 16:38:15');
/*!40000 ALTER TABLE `master_kompleks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_module`
--

DROP TABLE IF EXISTS `master_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_module` (
  `module_id` smallint(6) NOT NULL,
  `module_name` varchar(100) DEFAULT '',
  `module_features` tinyint(3) unsigned DEFAULT '0',
  `module_active` tinyint(3) unsigned DEFAULT '1',
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_module`
--

LOCK TABLES `master_module` WRITE;
/*!40000 ALTER TABLE `master_module` DISABLE KEYS */;
INSERT INTO `master_module` VALUES (1,'View Jobs',1,1),(2,'View Reports',1,1),(3,'Preferences',1,1),(4,'Pengaturan Master User',2,1),(5,'Pengaturan User Group',1,1),(6,'Pengaturan Master Group User',2,1),(7,'Pengaturan Group Akses',1,1),(8,'Pengaturan Departmemen',1,1),(9,'Pengaturan Task Flow',1,1);
/*!40000 ALTER TABLE `master_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_module_access`
--

DROP TABLE IF EXISTS `master_module_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_module_access` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` tinyint(3) unsigned NOT NULL,
  `module_id` smallint(6) NOT NULL,
  `user_access_option` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_module_access`
--

LOCK TABLES `master_module_access` WRITE;
/*!40000 ALTER TABLE `master_module_access` DISABLE KEYS */;
INSERT INTO `master_module_access` VALUES (1,1,1,1),(2,1,2,1),(3,1,3,1),(4,1,4,7),(5,1,5,1),(6,1,6,7),(7,1,7,1),(8,2,1,1),(9,2,2,0),(10,2,3,0),(11,2,4,0),(12,2,5,0),(13,2,6,0),(14,2,7,0);
/*!40000 ALTER TABLE `master_module_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_task`
--

DROP TABLE IF EXISTS `master_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_task` (
  `task_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `task_name` varchar(50) DEFAULT '',
  `task_parent` bigint(10) DEFAULT '0',
  `task_duration` double DEFAULT '0',
  `department_id` bigint(10) DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_update_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_task`
--

LOCK TABLES `master_task` WRITE;
/*!40000 ALTER TABLE `master_task` DISABLE KEYS */;
INSERT INTO `master_task` VALUES (1,'POTONG KUE',0,10,1,'Y',1,'2022-04-27 22:52:51',0,NULL),(2,'TIUP LILIN',1,10,1,'Y',1,'2022-04-27 23:03:00',0,NULL);
/*!40000 ALTER TABLE `master_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_config`
--

DROP TABLE IF EXISTS `sys_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_config` (
  `ID` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(50) DEFAULT '',
  `company_address` varchar(100) DEFAULT '',
  `company_phone` varchar(20) DEFAULT '',
  `company_email` varchar(50) DEFAULT '',
  `default_printer` varchar(200) DEFAULT '',
  `auto_backup_flag` tinyint(3) unsigned DEFAULT '0',
  `auto_backup_dir` varchar(200) DEFAULT '',
  `print_preview` tinyint(3) unsigned DEFAULT '0',
  `message_template` varchar(3000) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_config`
--

LOCK TABLES `sys_config` WRITE;
/*!40000 ALTER TABLE `sys_config` DISABLE KEYS */;
INSERT INTO `sys_config` VALUES (1,'PT. MENARA SANTOSA','JL. RONGGOWARSITO NO.73, KEPRABON','(0271) 643100','menara@menarasantosa.com','Microsoft Print to PDF',0,'',0,'Ini adalah test template message','Y',0,NULL,1,'2022-02-08 19:51:44');
/*!40000 ALTER TABLE `sys_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_department`
--

DROP TABLE IF EXISTS `user_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_department` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) DEFAULT '0',
  `department_id` tinyint(3) DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_department`
--

LOCK TABLES `user_department` WRITE;
/*!40000 ALTER TABLE `user_department` DISABLE KEYS */;
INSERT INTO `user_department` VALUES (1,2,1,'N',1,'2022-04-29 15:16:57',1,'2022-04-29 15:17:13'),(2,2,2,'N',1,'2022-04-29 15:16:57',1,'2022-04-29 15:17:13'),(3,2,3,'Y',1,'2022-04-29 15:16:57',1,'2022-04-29 15:17:13'),(4,1,1,'N',1,'2022-04-29 15:17:03',0,NULL),(5,1,2,'Y',1,'2022-04-29 15:17:03',0,NULL),(6,1,3,'N',1,'2022-04-29 15:17:03',0,NULL);
/*!40000 ALTER TABLE `user_department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_group` (
  `ID` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) DEFAULT '0',
  `group_id` tinyint(3) DEFAULT '0',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_group`
--

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
INSERT INTO `user_group` VALUES (1,1,1,'N',1,'2022-04-27 19:58:30',1,'2022-04-29 15:11:15'),(2,1,2,'Y',1,'2022-04-29 15:11:15',0,NULL);
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_login_data`
--

DROP TABLE IF EXISTS `user_login_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_login_data` (
  `user_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(15) DEFAULT '',
  `user_password` varchar(50) DEFAULT '',
  `group_id` tinyint(3) unsigned DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `user_full_name` varchar(100) DEFAULT '',
  `user_id_type` tinyint(3) DEFAULT '0',
  `user_id_no` varchar(100) DEFAULT '',
  `user_phone_1` varchar(45) DEFAULT '',
  `user_phone_2` varchar(45) DEFAULT '',
  `user_email_address` varchar(200) DEFAULT '',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `created_by` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` int(10) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `SECONDARY` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_login_data`
--

LOCK TABLES `user_login_data` WRITE;
/*!40000 ALTER TABLE `user_login_data` DISABLE KEYS */;
INSERT INTO `user_login_data` VALUES (1,'ADMIN','21232f297a57a5a743894a0e4a801fc3',0,'2022-04-29 15:16:52','2022-04-29 15:17:28','ADMIN1234567',1,'123456','081558775652','0111','ARISTON@ALPHASOFT.COM','Y',NULL,NULL,1,'2022-04-27 19:59:33'),(2,'ANDRI','6bd3108684ccc9dfd40b126877f850b0',0,NULL,NULL,'ANDRI',1,'','012345','','1111','Y',1,'2022-04-26 23:02:03',1,'2022-04-27 19:59:40');
/*!40000 ALTER TABLE `user_login_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_token`
--

DROP TABLE IF EXISTS `user_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_token` (
  `user_id` bigint(10) unsigned NOT NULL DEFAULT '0',
  `access_token` varchar(300) DEFAULT NULL,
  `fcm_token` varchar(300) DEFAULT NULL,
  `gcm_token` varchar(300) DEFAULT NULL,
  `ios_token` varchar(300) DEFAULT NULL,
  `web_token` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_token`
--

LOCK TABLES `user_token` WRITE;
/*!40000 ALTER TABLE `user_token` DISABLE KEYS */;
INSERT INTO `user_token` VALUES (1,'OQP0UY8ZUX','cwbSQIGlQqyS2Knm20z4tl:APA91bE4u2AAU24aeJHDE19oDRzLoMqMazT0MOwKXT_MrwxP5r4w3fLzC1rtaNSLCfGj3CVaNfkvcN-mkN_yI546EhMkXEb4mUC2C_reaOI8H8uOHXOxEcW-mLR0avFZSm7QtbGu_5D2','1','1','1'),(2,'e570c8164c6931e92703bea42f6574e7',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user_token` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-29 23:38:08
