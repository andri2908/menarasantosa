﻿namespace AlphaSoft
{
    partial class dataSistemAplikasi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.AppModeBox = new System.Windows.Forms.GroupBox();
            this.printPreviewCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.kuartoPrinter = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.emailTextbox = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.teleponTextbox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.alamatTextbox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.namaTextbox = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.paymentGridView = new System.Windows.Forms.DataGridView();
            this.lineID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paymentType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paymentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.biayaRP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.biayaPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.msgTemplate = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.startSun = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBoxSunday = new System.Windows.Forms.CheckBox();
            this.endSat = new System.Windows.Forms.DateTimePicker();
            this.checkBoxMonday = new System.Windows.Forms.CheckBox();
            this.startSat = new System.Windows.Forms.DateTimePicker();
            this.checkBoxTuesday = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBoxWednesday = new System.Windows.Forms.CheckBox();
            this.endFri = new System.Windows.Forms.DateTimePicker();
            this.checkBoxThursday = new System.Windows.Forms.CheckBox();
            this.startFri = new System.Windows.Forms.DateTimePicker();
            this.checkBoxFriday = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBoxSaturday = new System.Windows.Forms.CheckBox();
            this.endThur = new System.Windows.Forms.DateTimePicker();
            this.endSun = new System.Windows.Forms.DateTimePicker();
            this.startThur = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.startMon = new System.Windows.Forms.DateTimePicker();
            this.endWed = new System.Windows.Forms.DateTimePicker();
            this.endMon = new System.Windows.Forms.DateTimePicker();
            this.startWed = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.startTue = new System.Windows.Forms.DateTimePicker();
            this.endTue = new System.Windows.Forms.DateTimePicker();
            this.saveButton = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.dokumenFileServerDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.checkAutoLogin = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.AppModeBox.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.paymentGridView)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(605, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(7, 37);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(590, 402);
            this.tabControl1.TabIndex = 26;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.AppModeBox);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 27);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(582, 371);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Data Perusahaan";
            // 
            // AppModeBox
            // 
            this.AppModeBox.BackColor = System.Drawing.Color.Gainsboro;
            this.AppModeBox.Controls.Add(this.checkAutoLogin);
            this.AppModeBox.Controls.Add(this.printPreviewCheckBox);
            this.AppModeBox.Controls.Add(this.groupBox6);
            this.AppModeBox.Controls.Add(this.groupBox4);
            this.AppModeBox.Controls.Add(this.groupBox3);
            this.AppModeBox.Controls.Add(this.groupBox2);
            this.AppModeBox.Controls.Add(this.groupBox1);
            this.AppModeBox.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AppModeBox.Location = new System.Drawing.Point(7, 6);
            this.AppModeBox.Name = "AppModeBox";
            this.AppModeBox.Size = new System.Drawing.Size(569, 361);
            this.AppModeBox.TabIndex = 24;
            this.AppModeBox.TabStop = false;
            this.AppModeBox.Text = "Pengaturan Aplikasi";
            // 
            // printPreviewCheckBox
            // 
            this.printPreviewCheckBox.AutoSize = true;
            this.printPreviewCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.printPreviewCheckBox.Location = new System.Drawing.Point(17, 329);
            this.printPreviewCheckBox.Name = "printPreviewCheckBox";
            this.printPreviewCheckBox.Size = new System.Drawing.Size(170, 20);
            this.printPreviewCheckBox.TabIndex = 25;
            this.printPreviewCheckBox.Text = "Enable Print Preview";
            this.printPreviewCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.kuartoPrinter);
            this.groupBox6.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(9, 269);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(314, 54);
            this.groupBox6.TabIndex = 51;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Kuarto Printer";
            // 
            // kuartoPrinter
            // 
            this.kuartoPrinter.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.kuartoPrinter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.kuartoPrinter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.kuartoPrinter.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kuartoPrinter.FormattingEnabled = true;
            this.kuartoPrinter.Location = new System.Drawing.Point(8, 21);
            this.kuartoPrinter.Name = "kuartoPrinter";
            this.kuartoPrinter.Size = new System.Drawing.Size(298, 26);
            this.kuartoPrinter.TabIndex = 48;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.emailTextbox);
            this.groupBox4.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(9, 209);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(546, 54);
            this.groupBox4.TabIndex = 29;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "EMail ";
            // 
            // emailTextbox
            // 
            this.emailTextbox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailTextbox.Location = new System.Drawing.Point(6, 19);
            this.emailTextbox.Name = "emailTextbox";
            this.emailTextbox.Size = new System.Drawing.Size(532, 27);
            this.emailTextbox.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.teleponTextbox);
            this.groupBox3.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(9, 149);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(546, 54);
            this.groupBox3.TabIndex = 28;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Telepon";
            // 
            // teleponTextbox
            // 
            this.teleponTextbox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teleponTextbox.Location = new System.Drawing.Point(6, 21);
            this.teleponTextbox.MaxLength = 20;
            this.teleponTextbox.Name = "teleponTextbox";
            this.teleponTextbox.Size = new System.Drawing.Size(532, 27);
            this.teleponTextbox.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.alamatTextbox);
            this.groupBox2.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(9, 89);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(546, 54);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Alamat Perusahaan";
            // 
            // alamatTextbox
            // 
            this.alamatTextbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.alamatTextbox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alamatTextbox.Location = new System.Drawing.Point(6, 19);
            this.alamatTextbox.Name = "alamatTextbox";
            this.alamatTextbox.Size = new System.Drawing.Size(532, 27);
            this.alamatTextbox.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.namaTextbox);
            this.groupBox1.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(9, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(546, 54);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Nama Perusahaan";
            // 
            // namaTextbox
            // 
            this.namaTextbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.namaTextbox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.namaTextbox.Location = new System.Drawing.Point(6, 19);
            this.namaTextbox.Name = "namaTextbox";
            this.namaTextbox.Size = new System.Drawing.Size(532, 27);
            this.namaTextbox.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage2.Controls.Add(this.paymentGridView);
            this.tabPage2.Location = new System.Drawing.Point(4, 27);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(582, 371);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Transaksi";
            // 
            // paymentGridView
            // 
            this.paymentGridView.AllowUserToAddRows = false;
            this.paymentGridView.AllowUserToDeleteRows = false;
            this.paymentGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.paymentGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.paymentGridView.BackgroundColor = System.Drawing.Color.FloralWhite;
            this.paymentGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.paymentGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.lineID,
            this.paymentType,
            this.paymentName,
            this.biayaRP,
            this.biayaPercent});
            this.paymentGridView.Location = new System.Drawing.Point(17, 23);
            this.paymentGridView.MultiSelect = false;
            this.paymentGridView.Name = "paymentGridView";
            this.paymentGridView.RowHeadersVisible = false;
            this.paymentGridView.Size = new System.Drawing.Size(550, 326);
            this.paymentGridView.TabIndex = 65;
            this.paymentGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.paymentGridView_CellFormatting);
            this.paymentGridView.CurrentCellDirtyStateChanged += new System.EventHandler(this.paymentGridView_CurrentCellDirtyStateChanged);
            // 
            // lineID
            // 
            this.lineID.HeaderText = "lineID";
            this.lineID.Name = "lineID";
            this.lineID.Visible = false;
            this.lineID.Width = 56;
            // 
            // paymentType
            // 
            this.paymentType.HeaderText = "paymentType";
            this.paymentType.Name = "paymentType";
            this.paymentType.Visible = false;
            this.paymentType.Width = 113;
            // 
            // paymentName
            // 
            this.paymentName.HeaderText = "Tipe Pembayaran";
            this.paymentName.Name = "paymentName";
            this.paymentName.Width = 149;
            // 
            // biayaRP
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.biayaRP.DefaultCellStyle = dataGridViewCellStyle1;
            this.biayaRP.HeaderText = "Biaya (Rp)";
            this.biayaRP.Name = "biayaRP";
            this.biayaRP.Width = 103;
            // 
            // biayaPercent
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.biayaPercent.DefaultCellStyle = dataGridViewCellStyle2;
            this.biayaPercent.HeaderText = "Biaya (%)";
            this.biayaPercent.Name = "biayaPercent";
            this.biayaPercent.Width = 96;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.msgTemplate);
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Location = new System.Drawing.Point(4, 27);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(582, 371);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Jam Kerja";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 221);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 18);
            this.label7.TabIndex = 54;
            this.label7.Text = "Pesan";
            // 
            // msgTemplate
            // 
            this.msgTemplate.Location = new System.Drawing.Point(17, 241);
            this.msgTemplate.MaxLength = 3000;
            this.msgTemplate.Multiline = true;
            this.msgTemplate.Name = "msgTemplate";
            this.msgTemplate.Size = new System.Drawing.Size(544, 113);
            this.msgTemplate.TabIndex = 54;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.startSun);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.checkBoxSunday);
            this.groupBox5.Controls.Add(this.endSat);
            this.groupBox5.Controls.Add(this.checkBoxMonday);
            this.groupBox5.Controls.Add(this.startSat);
            this.groupBox5.Controls.Add(this.checkBoxTuesday);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.checkBoxWednesday);
            this.groupBox5.Controls.Add(this.endFri);
            this.groupBox5.Controls.Add(this.checkBoxThursday);
            this.groupBox5.Controls.Add(this.startFri);
            this.groupBox5.Controls.Add(this.checkBoxFriday);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.checkBoxSaturday);
            this.groupBox5.Controls.Add(this.endThur);
            this.groupBox5.Controls.Add(this.endSun);
            this.groupBox5.Controls.Add(this.startThur);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.startMon);
            this.groupBox5.Controls.Add(this.endWed);
            this.groupBox5.Controls.Add(this.endMon);
            this.groupBox5.Controls.Add(this.startWed);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.startTue);
            this.groupBox5.Controls.Add(this.endTue);
            this.groupBox5.Location = new System.Drawing.Point(17, 15);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(544, 207);
            this.groupBox5.TabIndex = 54;
            this.groupBox5.TabStop = false;
            // 
            // startSun
            // 
            this.startSun.CustomFormat = "HH:mm";
            this.startSun.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startSun.Location = new System.Drawing.Point(96, 0);
            this.startSun.Name = "startSun";
            this.startSun.Size = new System.Drawing.Size(92, 24);
            this.startSun.TabIndex = 33;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(194, 184);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(14, 18);
            this.label8.TabIndex = 53;
            this.label8.Text = "-";
            // 
            // checkBoxSunday
            // 
            this.checkBoxSunday.AutoSize = true;
            this.checkBoxSunday.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxSunday.Location = new System.Drawing.Point(12, 4);
            this.checkBoxSunday.Name = "checkBoxSunday";
            this.checkBoxSunday.Size = new System.Drawing.Size(77, 20);
            this.checkBoxSunday.TabIndex = 26;
            this.checkBoxSunday.Text = "Minggu";
            this.checkBoxSunday.UseVisualStyleBackColor = true;
            // 
            // endSat
            // 
            this.endSat.CustomFormat = "HH:mm";
            this.endSat.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endSat.Location = new System.Drawing.Point(218, 180);
            this.endSat.Name = "endSat";
            this.endSat.Size = new System.Drawing.Size(92, 24);
            this.endSat.TabIndex = 52;
            // 
            // checkBoxMonday
            // 
            this.checkBoxMonday.AutoSize = true;
            this.checkBoxMonday.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxMonday.Location = new System.Drawing.Point(12, 34);
            this.checkBoxMonday.Name = "checkBoxMonday";
            this.checkBoxMonday.Size = new System.Drawing.Size(66, 20);
            this.checkBoxMonday.TabIndex = 27;
            this.checkBoxMonday.Text = "Senin";
            this.checkBoxMonday.UseVisualStyleBackColor = true;
            // 
            // startSat
            // 
            this.startSat.CustomFormat = "HH:mm";
            this.startSat.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startSat.Location = new System.Drawing.Point(96, 180);
            this.startSat.Name = "startSat";
            this.startSat.Size = new System.Drawing.Size(92, 24);
            this.startSat.TabIndex = 51;
            // 
            // checkBoxTuesday
            // 
            this.checkBoxTuesday.AutoSize = true;
            this.checkBoxTuesday.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxTuesday.Location = new System.Drawing.Point(12, 64);
            this.checkBoxTuesday.Name = "checkBoxTuesday";
            this.checkBoxTuesday.Size = new System.Drawing.Size(76, 20);
            this.checkBoxTuesday.TabIndex = 28;
            this.checkBoxTuesday.Text = "Selasa";
            this.checkBoxTuesday.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(194, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 18);
            this.label5.TabIndex = 50;
            this.label5.Text = "-";
            // 
            // checkBoxWednesday
            // 
            this.checkBoxWednesday.AutoSize = true;
            this.checkBoxWednesday.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxWednesday.Location = new System.Drawing.Point(12, 94);
            this.checkBoxWednesday.Name = "checkBoxWednesday";
            this.checkBoxWednesday.Size = new System.Drawing.Size(64, 20);
            this.checkBoxWednesday.TabIndex = 29;
            this.checkBoxWednesday.Text = "Rabu";
            this.checkBoxWednesday.UseVisualStyleBackColor = true;
            // 
            // endFri
            // 
            this.endFri.CustomFormat = "HH:mm";
            this.endFri.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endFri.Location = new System.Drawing.Point(218, 150);
            this.endFri.Name = "endFri";
            this.endFri.Size = new System.Drawing.Size(92, 24);
            this.endFri.TabIndex = 49;
            // 
            // checkBoxThursday
            // 
            this.checkBoxThursday.AutoSize = true;
            this.checkBoxThursday.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxThursday.Location = new System.Drawing.Point(12, 124);
            this.checkBoxThursday.Name = "checkBoxThursday";
            this.checkBoxThursday.Size = new System.Drawing.Size(69, 20);
            this.checkBoxThursday.TabIndex = 30;
            this.checkBoxThursday.Text = "Kamis";
            this.checkBoxThursday.UseVisualStyleBackColor = true;
            // 
            // startFri
            // 
            this.startFri.CustomFormat = "HH:mm";
            this.startFri.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startFri.Location = new System.Drawing.Point(96, 150);
            this.startFri.Name = "startFri";
            this.startFri.Size = new System.Drawing.Size(92, 24);
            this.startFri.TabIndex = 48;
            // 
            // checkBoxFriday
            // 
            this.checkBoxFriday.AutoSize = true;
            this.checkBoxFriday.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxFriday.Location = new System.Drawing.Point(12, 152);
            this.checkBoxFriday.Name = "checkBoxFriday";
            this.checkBoxFriday.Size = new System.Drawing.Size(68, 20);
            this.checkBoxFriday.TabIndex = 31;
            this.checkBoxFriday.Text = "Jumat";
            this.checkBoxFriday.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(194, 124);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 18);
            this.label6.TabIndex = 47;
            this.label6.Text = "-";
            // 
            // checkBoxSaturday
            // 
            this.checkBoxSaturday.AutoSize = true;
            this.checkBoxSaturday.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxSaturday.Location = new System.Drawing.Point(12, 182);
            this.checkBoxSaturday.Name = "checkBoxSaturday";
            this.checkBoxSaturday.Size = new System.Drawing.Size(67, 20);
            this.checkBoxSaturday.TabIndex = 32;
            this.checkBoxSaturday.Text = "Sabtu";
            this.checkBoxSaturday.UseVisualStyleBackColor = true;
            // 
            // endThur
            // 
            this.endThur.CustomFormat = "HH:mm";
            this.endThur.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endThur.Location = new System.Drawing.Point(218, 120);
            this.endThur.Name = "endThur";
            this.endThur.Size = new System.Drawing.Size(92, 24);
            this.endThur.TabIndex = 46;
            // 
            // endSun
            // 
            this.endSun.CustomFormat = "HH:mm";
            this.endSun.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endSun.Location = new System.Drawing.Point(218, 0);
            this.endSun.Name = "endSun";
            this.endSun.Size = new System.Drawing.Size(92, 24);
            this.endSun.TabIndex = 34;
            // 
            // startThur
            // 
            this.startThur.CustomFormat = "HH:mm";
            this.startThur.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startThur.Location = new System.Drawing.Point(96, 120);
            this.startThur.Name = "startThur";
            this.startThur.Size = new System.Drawing.Size(92, 24);
            this.startThur.TabIndex = 45;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(194, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 18);
            this.label1.TabIndex = 35;
            this.label1.Text = "-";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(194, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 18);
            this.label3.TabIndex = 44;
            this.label3.Text = "-";
            // 
            // startMon
            // 
            this.startMon.CustomFormat = "HH:mm";
            this.startMon.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startMon.Location = new System.Drawing.Point(96, 30);
            this.startMon.Name = "startMon";
            this.startMon.Size = new System.Drawing.Size(92, 24);
            this.startMon.TabIndex = 36;
            // 
            // endWed
            // 
            this.endWed.CustomFormat = "HH:mm";
            this.endWed.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endWed.Location = new System.Drawing.Point(218, 90);
            this.endWed.Name = "endWed";
            this.endWed.Size = new System.Drawing.Size(92, 24);
            this.endWed.TabIndex = 43;
            // 
            // endMon
            // 
            this.endMon.CustomFormat = "HH:mm";
            this.endMon.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endMon.Location = new System.Drawing.Point(218, 30);
            this.endMon.Name = "endMon";
            this.endMon.Size = new System.Drawing.Size(92, 24);
            this.endMon.TabIndex = 37;
            // 
            // startWed
            // 
            this.startWed.CustomFormat = "HH:mm";
            this.startWed.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startWed.Location = new System.Drawing.Point(96, 90);
            this.startWed.Name = "startWed";
            this.startWed.Size = new System.Drawing.Size(92, 24);
            this.startWed.TabIndex = 42;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(194, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 18);
            this.label2.TabIndex = 38;
            this.label2.Text = "-";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(194, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 18);
            this.label4.TabIndex = 41;
            this.label4.Text = "-";
            // 
            // startTue
            // 
            this.startTue.CustomFormat = "HH:mm";
            this.startTue.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startTue.Location = new System.Drawing.Point(96, 60);
            this.startTue.Name = "startTue";
            this.startTue.Size = new System.Drawing.Size(92, 24);
            this.startTue.TabIndex = 39;
            // 
            // endTue
            // 
            this.endTue.CustomFormat = "HH:mm";
            this.endTue.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endTue.Location = new System.Drawing.Point(218, 60);
            this.endTue.Name = "endTue";
            this.endTue.Size = new System.Drawing.Size(92, 24);
            this.endTue.TabIndex = 40;
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.Color.FloralWhite;
            this.saveButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.ForeColor = System.Drawing.Color.Black;
            this.saveButton.Location = new System.Drawing.Point(207, 454);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(195, 54);
            this.saveButton.TabIndex = 27;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // checkAutoLogin
            // 
            this.checkAutoLogin.AutoSize = true;
            this.checkAutoLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkAutoLogin.Location = new System.Drawing.Point(377, 294);
            this.checkAutoLogin.Name = "checkAutoLogin";
            this.checkAutoLogin.Size = new System.Drawing.Size(100, 20);
            this.checkAutoLogin.TabIndex = 52;
            this.checkAutoLogin.Text = "Auto Login";
            this.checkAutoLogin.UseVisualStyleBackColor = true;
            // 
            // dataSistemAplikasi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(605, 547);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "dataSistemAplikasi";
            this.Text = "SISTEM APLIKASI";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.pengaturanSistemAplikasi_FormClosed);
            this.Load += new System.EventHandler(this.pengaturanSistemAplikasi_Load);
            this.Controls.SetChildIndex(this.tabControl1, 0);
            this.Controls.SetChildIndex(this.saveButton, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.AppModeBox.ResumeLayout(false);
            this.AppModeBox.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.paymentGridView)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox AppModeBox;
        private System.Windows.Forms.CheckBox printPreviewCheckBox;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox kuartoPrinter;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox emailTextbox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox teleponTextbox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox alamatTextbox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox namaTextbox;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.FolderBrowserDialog dokumenFileServerDialog;
        private System.Windows.Forms.TabPage tabPage2;
        protected System.Windows.Forms.DataGridView paymentGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn lineID;
        private System.Windows.Forms.DataGridViewTextBoxColumn paymentType;
        private System.Windows.Forms.DataGridViewTextBoxColumn paymentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn biayaRP;
        private System.Windows.Forms.DataGridViewTextBoxColumn biayaPercent;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.CheckBox checkBoxSaturday;
        private System.Windows.Forms.CheckBox checkBoxFriday;
        private System.Windows.Forms.CheckBox checkBoxThursday;
        private System.Windows.Forms.CheckBox checkBoxWednesday;
        private System.Windows.Forms.CheckBox checkBoxTuesday;
        private System.Windows.Forms.CheckBox checkBoxMonday;
        private System.Windows.Forms.CheckBox checkBoxSunday;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DateTimePicker startSun;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker endSat;
        private System.Windows.Forms.DateTimePicker startSat;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker endFri;
        private System.Windows.Forms.DateTimePicker startFri;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker endThur;
        private System.Windows.Forms.DateTimePicker endSun;
        private System.Windows.Forms.DateTimePicker startThur;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker startMon;
        private System.Windows.Forms.DateTimePicker endWed;
        private System.Windows.Forms.DateTimePicker endMon;
        private System.Windows.Forms.DateTimePicker startWed;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker startTue;
        private System.Windows.Forms.DateTimePicker endTue;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox msgTemplate;
        private System.Windows.Forms.CheckBox checkAutoLogin;
    }
}
