﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Globalization;

namespace AlphaSoft
{
    public partial class dataSalesForm : AlphaSoft.basic2TextBoxInputsForm
    {
        private int originModuleID = 0;
        private int selectedSalesID = 0;

        private globalUserUtil gUser = new globalUserUtil();
        private globalRestAPI gRest = new globalRestAPI();
        private CultureInfo culture = new CultureInfo("id-ID");

        public REST_sales salesData = new REST_sales();

        public int returnSalesIDValue = 0;

        public dataSalesForm(int salesID = 0)
        {
            InitializeComponent();

            this.Text = "DATA SALES";

            selectedSalesID = salesID;

            if (selectedSalesID > 0)
            {
                originModuleID = globalConstants.EDIT_SALES;

                //if (!gUser.RS_userHasAccessTo(MMConstants.TAMBAH_HAPUS_GROUP_USER,
                //MMConstants.HAK_UPDATE_DATA))
                //{
                //    setSaveButtonEnable(false);
                //    setResetButtonEnable(false);
                //}
            }
            else
                originModuleID = globalConstants.NEW_SALES;
        }

        private void dataSalesForm_Load(object sender, EventArgs e)
        {
            if (selectedSalesID > 0)
            {
                loadSalesDataInformation();
            }

            namaTextBox.Focus();
            gUser.reArrangeTabOrder(this);
        }

        private void dataSalesForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            returnSalesIDValue = selectedSalesID;
        }

        private void loadSalesDataInformation()
        {
            DataTable dt = new DataTable();

            string sqlCommand = "SELECT * FROM master_sales WHERE sales_id = " + selectedSalesID;

            gRest.getMasterSalesData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref salesData);

            if (salesData.dataStatus.o_status == 1)
            {
                namaTextBox.Text = salesData.dataList[0].sales_name;
                deskripsiTextBox.Text = salesData.dataList[0].sales_description;
                nonAktifCheckbox.Checked = (salesData.dataList[0].is_active == "Y" ? false : true);
            }
        }

        protected override bool saveDataTransaction()
        {
            bool result = false;
            int opType = 0;
            string errMsg = "";

            List<master_sales> listSalesData = new List<master_sales>();
            master_sales salesData = new master_sales();

            try
            {
                salesData.sales_name = namaTextBox.Text;
                salesData.sales_description = deskripsiTextBox.Text;
                salesData.is_active = (nonAktifCheckbox.Checked ? "N" : "Y");

                switch (originModuleID)
                {
                    case globalConstants.NEW_SALES:
                        opType = 1;
                        salesData.sales_id = 0;
                        break;

                    case globalConstants.EDIT_SALES:
                        opType = 2;
                        salesData.sales_id = selectedSalesID;
                        break;
                }

                listSalesData.Add(salesData);

                int newID = 0;
                if (!gRest.saveSales(gUser.getUserID(), gUser.getUserToken(), listSalesData, opType, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                selectedSalesID = newID;

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        protected override bool dataValidated()
        {
            errorLabel.Text = "";

            bool result = false;

            result = base.dataValidated();

            if (!result)
                return false;

            return true;
        }
    }
}
