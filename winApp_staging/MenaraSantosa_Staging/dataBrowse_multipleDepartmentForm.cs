﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class dataBrowse_multipleDepartmentForm : AlphaSoft.basicHotkeysForm
    {
        private globalRestAPI gRest = new globalRestAPI();
        private globalUserUtil gUser = new globalUserUtil();

        REST_department deptData = new REST_department();
        adminForm parentForm;

        public List<int> deptList = new List<int>();

        public dataBrowse_multipleDepartmentForm(adminForm pForm)
        {
            InitializeComponent();

            parentForm = pForm;
            deptList.Clear();
        }

        private void loadUserDeptData()
        {
            string sqlCommand = "";
            bool selected = false;

            sqlCommand = "SELECT md.department_id, md.department_name " +
                                    "FROM master_department md " +
                                    "WHERE md.is_active = 'Y' " +
                                    "ORDER BY md.department_name ASC";

            if (gRest.getDepartmentData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref deptData))
            {
                deptGrid.Rows.Clear();

                for (int i = 0; i < deptData.dataList.Count; i++)
                {
                    selected = false;

                    if (parentForm.deptIDExist(deptData.dataList[i].department_id))
                        selected = true;

                    deptGrid.Rows.Add(
                        (selected ? 1 : 0),
                        deptData.dataList[i].department_id,
                        selected,
                        deptData.dataList[i].department_name
                        );


                    if (selected)
                    {
                        deptGrid.Rows[deptGrid.Rows.Count - 1].DefaultCellStyle.BackColor = Color.Gray;
                        deptGrid.Rows[deptGrid.Rows.Count - 1].ReadOnly = true;
                    }
                }
            }
        }

        private void dataBrowse_multipleDepartmentForm_Load(object sender, EventArgs e)
        {
            loadUserDeptData();
        }

        private void displayButton_Click(object sender, EventArgs e)
        {
            DataGridViewRow sRow;
            for (int i = 0;i<deptGrid.Rows.Count;i++)
            {
                sRow = deptGrid.Rows[i];

                if (sRow.Cells["isExist"].Value.ToString() == "0" && Convert.ToBoolean(sRow.Cells["flag"].Value))
                {
                    deptList.Add(Convert.ToInt32(sRow.Cells["departmentID"].Value));
                    parentForm.addDepartmentIDToList(Convert.ToInt32(sRow.Cells["departmentID"].Value));
                }
            }

            this.Close();
        }

        private void nonAktifCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0;i<deptGrid.Rows.Count;i++)
            {
                deptGrid.Rows[i].Cells["flag"].Value = nonAktifCheckbox.Checked;
            }
        }
    }
}
