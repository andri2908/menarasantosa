﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class dataBrowse_KompleksForm : AlphaSoft.simpleBrowseForm
    {
        private int originModuleID = 0;
        private int selectedUserID = 0;

        private globalUserUtil gUtil;
        private globalRestAPI gRest;

        public double ret_biayaIPL = 0;
        public int ret_durasiBayar = 0;

        public dataBrowse_KompleksForm(int moduleID = 0, int userID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;
            selectedUserID = userID;

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();

            ReturnValue1 = "0";
            ReturnValue2 = "0";
        }

        private void dataBrowse_kompleksForm_Load(object sender, EventArgs e)
        {
            switch (originModuleID)
            {
                //case globalConstants.MODULE_TRANSAKSI_RETAIL:
                //    nonActiveCheckBox.Visible = false;
                //    break;
            }

            displayAllAction();

            //if (!gUtil.RS_userHasAccessTo(MMConstants.TAMBAH_HAPUS_KOMPLEKS,
            //    MMConstants.HAK_NEW_DATA))
            //    setNewButtonEnable(false);
        }

        private void displayDefault()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string paramName = "";
            REST_masterKompleks kompleksData = new REST_masterKompleks();

            paramName = getNamaTextBox();

            sqlCommand = "SELECT kompleks_id, kompleks_name, kompleks_address, is_active, biaya_ipl, durasi_pembayaran " +
                                    "FROM master_kompleks " +
                                    "WHERE 1 = 1 ";

            if (paramName.Length > 0)
                sqlCommand = sqlCommand + "AND kompleks_name LIKE '%" + paramName + "%' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY kompleks_name ASC";

            if (gRest.getMasterKompleksData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref kompleksData))
            {
                if (kompleksData.dataStatus.o_status == 1)
                {
                    dataGridView.DataSource = null;

                    dt.Columns.Add("KOMPLEKS_ID");
                    dt.Columns.Add("NAMA KOMPLEKS");
                    dt.Columns.Add("ALAMAT");
                    dt.Columns.Add("biayaIPL");
                    dt.Columns.Add("durasiBayar");

                    for (int i = 0; i < kompleksData.dataList.Count; i++)
                    {
                        DataRow r = dt.Rows.Add();
                        r["KOMPLEKS_ID"] = kompleksData.dataList[i].kompleks_id;
                        r["NAMA KOMPLEKS"] = kompleksData.dataList[i].kompleks_name;
                        r["ALAMAT"] = kompleksData.dataList[i].kompleks_address;
                        r["biayaIPL"] = kompleksData.dataList[i].biaya_ipl;
                        r["durasiBayar"] = kompleksData.dataList[i].durasi_pembayaran;
                    }

                    dataGridView.DataSource = dt;
                    dataGridView.Columns["KOMPLEKS_ID"].Visible = false;
                    dataGridView.Columns["biayaIPL"].Visible = false;
                    dataGridView.Columns["durasiBayar"].Visible = false;
                }
            }
        }

        private void displayKompleksRetail()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string sqlKavling = "";
            string paramName = "";
            REST_masterKompleks kompleksData = new REST_masterKompleks();

            dataGridView.DataSource = null;

            paramName = getNamaTextBox();

            sqlKavling = "SELECT mk.kompleks_id FROM master_kavling mk, user_kavling uk " +
                                "WHERE uk.user_id = " + selectedUserID + " " +
                                "AND uk.kavling_id = mk.kavling_id " +
                                "AND uk.is_active = 'Y' " +
                                "GROUP BY kompleks_id";

            sqlCommand = "SELECT kompleks_id, kompleks_name, kompleks_address, is_active, biaya_ipl, durasi_pembayaran " +
                                    "FROM master_kompleks " +
                                    "WHERE kompleks_id IN (" + sqlKavling + ") " +
                                    "AND is_active = 'Y' ";

            if (paramName.Length > 0)
                sqlCommand = sqlCommand + "AND kompleks_name LIKE '%" + paramName + "%' ";

            sqlCommand = sqlCommand + "ORDER BY kompleks_name ASC";

            if (gRest.getMasterKompleksData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref kompleksData))
            {
                if (kompleksData.dataStatus.o_status == 1)
                {
                    dataGridView.DataSource = null;

                    dt.Columns.Add("KOMPLEKS_ID");
                    dt.Columns.Add("NAMA KOMPLEKS");
                    dt.Columns.Add("ALAMAT");
                    dt.Columns.Add("biayaIPL");
                    dt.Columns.Add("durasiBayar");

                    for (int i = 0; i < kompleksData.dataList.Count; i++)
                    {
                        DataRow r = dt.Rows.Add();
                        r["KOMPLEKS_ID"] = kompleksData.dataList[i].kompleks_id;
                        r["NAMA KOMPLEKS"] = kompleksData.dataList[i].kompleks_name;
                        r["ALAMAT"] = kompleksData.dataList[i].kompleks_address;
                        r["biayaIPL"] = kompleksData.dataList[i].biaya_ipl;
                        r["durasiBayar"] = kompleksData.dataList[i].durasi_pembayaran;
                    }

                    dataGridView.DataSource = dt;
                    dataGridView.Columns["KOMPLEKS_ID"].Visible = false;
                    dataGridView.Columns["biayaIPL"].Visible = false;
                    dataGridView.Columns["durasiBayar"].Visible = false;
                }
            }
        }

        protected override void displayAllAction()
        {
            switch(originModuleID)
            {
                //case globalConstants.MODULE_TRANSAKSI_RETAIL:
                //    displayKompleksRetail();
                //    break;

                default:
                    displayDefault();
                    break;
            }
        }

        protected override void dbGridDoubleClickAction(DataGridViewRow selectedRow)
        {
            int selectedID = Convert.ToInt32(selectedRow.Cells["KOMPLEKS_ID"].Value);

            switch (originModuleID)
            {
                case globalConstants.MODULE_DEFAULT:
                //case globalConstants.MODULE_TRANSAKSI_RETAIL:
                    ReturnValue1 = selectedRow.Cells["KOMPLEKS_ID"].Value.ToString();
                    ReturnValue2 = selectedRow.Cells["NAMA KOMPLEKS"].Value.ToString();
                    ret_biayaIPL = Convert.ToDouble(selectedRow.Cells["biayaIPL"].Value);
                    ret_durasiBayar = Convert.ToInt32(selectedRow.Cells["durasiBayar"].Value);

                    this.Close();
                    break;

                default:
                    break;
            }
        }

        protected override void newButtonAction()
        {
        }

        private void dataBrowse_kompleksForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
