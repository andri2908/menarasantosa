﻿namespace AlphaSoft
{
    partial class dataDepartmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.TabIndex = 3;
            // 
            // ResetButton
            // 
            this.ResetButton.TabIndex = 10;
            // 
            // SaveButton
            // 
            this.SaveButton.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.TabIndex = 4;
            // 
            // nonAktifCheckbox
            // 
            this.nonAktifCheckbox.TabIndex = 8;
            // 
            // namaTextBox
            // 
            this.namaTextBox.TabIndex = 6;
            // 
            // deskripsiTextBox
            // 
            this.deskripsiTextBox.TabIndex = 7;
            // 
            // panel1
            // 
            this.panel1.TabIndex = 0;
            // 
            // errorLabel
            // 
            this.errorLabel.TabIndex = 1;
            // 
            // dataDepartmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(558, 265);
            this.Name = "dataDepartmentForm";
            this.Text = "DATA DEPARTEMEN";
            this.Load += new System.EventHandler(this.dataDepartmentForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
