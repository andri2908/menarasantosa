﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using Hotkeys;

namespace AlphaSoft
{
    public partial class loginForm : Form
    {
        private globalUserUtil gUtil;
        private globalRestAPI gRest;

        private const int MENU_JOBS = 0;
        private const int MENU_EDIT_JOBS = 1;
        private const int MENU_REPORTS = 2;
        private const int MENU_PREFERENCES = 3;

        private Hotkeys.GlobalHotkey ghk_UP;
        private Hotkeys.GlobalHotkey ghk_DOWN;
        private Hotkeys.GlobalHotkey ghk_ESC;
        private Hotkeys.GlobalHotkey ghk_F9;

        private bool navKeyRegistered = false;

        private int originModuleID = 0;
        private bool isAutoLogin = false;
        private int autoLoginUserID = 0;
        private int defaultScreenAutoLogin = 0;

        public loginForm(int moduleID = 0)
        {
            crReportContainer dummyReport = new crReportContainer();
            dummyReport.ShowDialog();

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();

            InitializeComponent();

            originModuleID = moduleID;
        }

        private void captureAll(Keys key)
        {
            switch (key)
            {
                case Keys.Up:
                    SendKeys.Send("+{TAB}");
                    break;

                case Keys.Down:
                    SendKeys.Send("{TAB}");
                    break;

                case Keys.F9:
                    loginButton.PerformClick();
                    break;

                case Keys.Escape:
                    Application.Exit();
                    break;
            }
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == Constants.WM_HOTKEY_MSG_ID)
            {
                Keys key = (Keys)(((int)m.LParam >> 16) & 0xFFFF);
                int modifier = (int)m.LParam & 0xFFFF;

                if (modifier == Constants.NOMOD)
                    captureAll(key);
            }

            base.WndProc(ref m);
        }

        private void registerGlobalHotkey()
        {
            ghk_UP = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.Up, this);
            ghk_UP.Register();

            ghk_DOWN = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.Down, this);
            ghk_DOWN.Register();

            ghk_ESC = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.Escape, this);
            ghk_ESC.Register();

            ghk_F9 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F9, this);
            ghk_F9.Register();

            navKeyRegistered = true;
        }

        private void unregisterGlobalHotkey()
        {
            ghk_UP.Unregister();
            ghk_DOWN.Unregister();
            ghk_ESC.Unregister();
            ghk_F9.Unregister();

            navKeyRegistered = false;
        }

        private void genericControl_Enter(object sender, EventArgs e)
        {
            if (navKeyRegistered)
                unregisterGlobalHotkey();
        }

        private void genericControl_Leave(object sender, EventArgs e)
        {
            registerGlobalHotkey();
        }

        private bool checkUserNamePassword(int moduleID = 0, bool isAutoLogin = false, int userAutoLogin = 0)
        {
            if (isAutoLogin)
            {
                gUtil.RS_doUserAutoLogin(userAutoLogin);
                return true;
            }
            else
                return gUtil.RS_doUserLogin(userNameTextBox.Text, passwordTextBox.Text);
        }

        private bool checkTextBox()
        {
            bool retVal = false;
            string userName;
            string password;

            if (isAutoLogin)
                return true;

            userName = userNameTextBox.Text;
            password = passwordTextBox.Text;

            if (userName !="" & password != "")
            {
                retVal = true;
            }

            return retVal;
        }

        private void displayScreen()
        {
            int comboIndex = 0;
            comboIndex = comboAkses.SelectedIndex;

            switch(comboIndex)
            {
                case MENU_JOBS:
                    if (gUtil.RS_userHasAccessTo(MMConstants.VIEW_JOBS, MMConstants.HAK_AKSES, gUtil.getUserID()))
                    {
                        adminForm displayAdmin = new adminForm();
                        displayAdmin.ShowDialog(this);
                    }
                    else
                    {
                        MessageBox.Show("User tidak punya akses");
                    }
                    break;

                case MENU_EDIT_JOBS:
                    if (gUtil.RS_userHasAccessTo(MMConstants.UPDATE_JOB_HEADER, MMConstants.HAK_AKSES, gUtil.getUserID()))
                    {
                        dataJobHeaderListForm displayHeader = new dataJobHeaderListForm();
                        displayHeader.ShowDialog(this);
                    }
                    else
                    {
                        MessageBox.Show("User tidak punya akses");
                    }
                    break;

                case MENU_REPORTS:
                    if (gUtil.RS_userHasAccessTo(MMConstants.VIEW_REPORTS, MMConstants.HAK_AKSES, gUtil.getUserID()))
                    {
                        dataFilterReportJobs displayReport = new dataFilterReportJobs();
                        displayReport.ShowDialog(this);
                    }
                    else
                    {
                        MessageBox.Show("User tidak punya akses");
                    }
                    break;

                case MENU_PREFERENCES:
                    if (gUtil.RS_userHasAccessTo(MMConstants.PREFERENCES, MMConstants.HAK_AKSES, gUtil.getUserID()))
                    {
                        dataConfigForm displayConfig = new dataConfigForm();
                        displayConfig.ShowDialog(this);
                    }
                    else
                    {
                        MessageBox.Show("User tidak punya akses");
                    }
                    break;
            }
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (checkTextBox())
            {
                if (checkUserNamePassword(0, isAutoLogin, autoLoginUserID))
                {
                    this.Hide();

                    gUtil.RS_setUserGroupID();
                    gUtil.RS_saveUserLastLogin();

                    displayScreen();

                    userNameTextBox.Clear();
                    passwordTextBox.Clear();
                    comboAkses.SelectedIndex = 0;

                    isAutoLogin = false;
                    userNameTextBox.Enabled = true;
                    passwordTextBox.Enabled = true;

                    gUtil.RS_saveUserLastLogout();
                    this.Show();

                    userNameTextBox.Focus();
                }
                else
                {
                    errorLabel.Text = "Login gagal";
                }
            }
            else
            {
                errorLabel.Text = "Input salah";
            }
        }

        private bool getAutoLogin()
        {
            string appPath = Application.StartupPath + "\\conf";
            string configPath = appPath + "\\autoLogin.cfg";
            string result = "0";
            string[] autosetting;

            if (File.Exists(configPath))
                result = File.ReadAllText(configPath);

            if (result.Length > 0)
            {
                autosetting = result.Split('#');
                if (autosetting[0] == "1")
                {
                    autoLoginUserID = Convert.ToInt32(autosetting[1]);
                    defaultScreenAutoLogin = Convert.ToInt32(autosetting[2]);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            //genericReply replyResult = new genericReply();

            //if (gRest.getAutoLoginDataNoParam(ref replyResult))
            //{
            //    if (replyResult.o_status == 1)
            //    {
            //        if (replyResult.data.ToString() == "Y")
            //            return true;
            //    }
            //}

            //return false;
        }

        private void getAutoLoginUserID()
        {
            genericReply replyResult = new genericReply();

            if (gRest.getUserAutoLoginDataNoParam(ref replyResult))
            {
                if (replyResult.o_status == 1)
                {
                    autoLoginUserID = Convert.ToInt32(replyResult.data);
                }
            }
        }

        private void loginForm_Load(object sender, EventArgs e)
        {
            appVersionLabel.Text = "v" + Application.ProductVersion;

            gUtil.reArrangeTabOrder(this);

            if (originModuleID == globalConstants.MODULE_2ND_AUTH)
            {
                loginButton.Text = "Verify";
            }
            else
            {
                // checkAutoLogin
                isAutoLogin = getAutoLogin();
                if (isAutoLogin)
                {
                    //getAutoLoginUserID();

                    userNameTextBox.Enabled = false;
                    passwordTextBox.Enabled = false;

                    if (defaultScreenAutoLogin > 0)
                    {
                        comboAkses.SelectedIndex = defaultScreenAutoLogin - 1;
                        loginButton.PerformClick();
                    }
                }
            }

            comboAkses.SelectedIndex = 0;
        }

        private void passwordTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 13)
            {
                loginButton.PerformClick();
            }
        }

        private void loginForm_Activated(object sender, EventArgs e)
        {
            errorLabel.Text = "";
            registerGlobalHotkey();
        }

        private void loginForm_Deactivate(object sender, EventArgs e)
        {
            unregisterGlobalHotkey();
        }

        private void exitAppsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void exitAppsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboAkses_KeyDown(object sender, KeyEventArgs e)
        {
            if (Convert.ToInt32(e.KeyCode) == 13)
            {
                loginButton.PerformClick();
            }
        }
    }
}
