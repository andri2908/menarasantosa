﻿namespace AlphaSoft
{
    partial class dataConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dataConfigForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nonActiveCheckBox = new System.Windows.Forms.CheckBox();
            this.configTabControl = new System.Windows.Forms.TabControl();
            this.tabUser = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonUser = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.userDataGrid = new System.Windows.Forms.DataGridView();
            this.userID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.editButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.groupButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.departmentButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tabGroup = new System.Windows.Forms.TabPage();
            this.groupDataGrid = new System.Windows.Forms.DataGridView();
            this.groupID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupEditButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.groupAksesButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.buttonGroupUser = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tabDepartment = new System.Windows.Forms.TabPage();
            this.departmentDataGrid = new System.Windows.Forms.DataGridView();
            this.departmentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departmentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deptDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departmentEditButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.buttonDepartment = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tabFlow = new System.Windows.Forms.TabPage();
            this.taskDataGrid = new System.Windows.Forms.DataGridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.buttonFlow = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tabCustomer = new System.Windows.Forms.TabPage();
            this.custGridView = new System.Windows.Forms.DataGridView();
            this.custID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.custName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.custDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.editCust = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel6 = new System.Windows.Forms.Panel();
            this.buttonCustomer = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tabSales = new System.Windows.Forms.TabPage();
            this.salesGridView = new System.Windows.Forms.DataGridView();
            this.salesID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salesDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.editSales = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel7 = new System.Windows.Forms.Panel();
            this.buttonSales = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.tabKompleks = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.buttonKompleks = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.kompleksGridView = new System.Windows.Forms.DataGridView();
            this.kompleksID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kompleksName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kompleksAddres = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.editKompleks = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tabKavling = new System.Windows.Forms.TabPage();
            this.buttonAll = new System.Windows.Forms.Button();
            this.searchKompleks = new System.Windows.Forms.Button();
            this.kompleksTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.buttonKavling = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.kavlingGridView = new System.Windows.Forms.DataGridView();
            this.kavlingID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kavlingKompleksName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kavlingName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.editKavling = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tabSystem = new System.Windows.Forms.TabPage();
            this.AppModeBox = new System.Windows.Forms.GroupBox();
            this.checkBoxAutoLogin = new System.Windows.Forms.CheckBox();
            this.comboAkses = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.kuartoPrinter = new System.Windows.Forms.ComboBox();
            this.printPreviewCheckBox = new System.Windows.Forms.CheckBox();
            this.buttonExit = new System.Windows.Forms.Button();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.exitPreferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taskID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skenarioName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noUrut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deptName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taskName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taskParent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taskDuration = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taskEditButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.configTabControl.SuspendLayout();
            this.tabUser.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userDataGrid)).BeginInit();
            this.tabGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupDataGrid)).BeginInit();
            this.panel3.SuspendLayout();
            this.tabDepartment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.departmentDataGrid)).BeginInit();
            this.panel5.SuspendLayout();
            this.tabFlow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.taskDataGrid)).BeginInit();
            this.panel4.SuspendLayout();
            this.tabCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.custGridView)).BeginInit();
            this.panel6.SuspendLayout();
            this.tabSales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.salesGridView)).BeginInit();
            this.panel7.SuspendLayout();
            this.tabKompleks.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kompleksGridView)).BeginInit();
            this.tabKavling.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kavlingGridView)).BeginInit();
            this.tabSystem.SuspendLayout();
            this.AppModeBox.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonExit);
            this.panel1.Size = new System.Drawing.Size(1051, 29);
            this.panel1.Controls.SetChildIndex(this.errorLabel, 0);
            this.panel1.Controls.SetChildIndex(this.buttonExit, 0);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox1.Controls.Add(this.nonActiveCheckBox);
            this.groupBox1.Controls.Add(this.configTabControl);
            this.groupBox1.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(12, 48);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1027, 539);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            // 
            // nonActiveCheckBox
            // 
            this.nonActiveCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.nonActiveCheckBox.AutoSize = true;
            this.nonActiveCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nonActiveCheckBox.Location = new System.Drawing.Point(807, 518);
            this.nonActiveCheckBox.Name = "nonActiveCheckBox";
            this.nonActiveCheckBox.Size = new System.Drawing.Size(203, 20);
            this.nonActiveCheckBox.TabIndex = 93;
            this.nonActiveCheckBox.Text = "Tampilkan Data Non Aktif";
            this.nonActiveCheckBox.UseVisualStyleBackColor = true;
            this.nonActiveCheckBox.CheckedChanged += new System.EventHandler(this.nonActiveCheckBox_CheckedChanged);
            // 
            // configTabControl
            // 
            this.configTabControl.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.configTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.configTabControl.Controls.Add(this.tabUser);
            this.configTabControl.Controls.Add(this.tabGroup);
            this.configTabControl.Controls.Add(this.tabDepartment);
            this.configTabControl.Controls.Add(this.tabFlow);
            this.configTabControl.Controls.Add(this.tabCustomer);
            this.configTabControl.Controls.Add(this.tabSales);
            this.configTabControl.Controls.Add(this.tabKompleks);
            this.configTabControl.Controls.Add(this.tabKavling);
            this.configTabControl.Controls.Add(this.tabSystem);
            this.configTabControl.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.configTabControl.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configTabControl.ItemSize = new System.Drawing.Size(50, 200);
            this.configTabControl.Location = new System.Drawing.Point(18, 27);
            this.configTabControl.Multiline = true;
            this.configTabControl.Name = "configTabControl";
            this.configTabControl.SelectedIndex = 0;
            this.configTabControl.Size = new System.Drawing.Size(992, 485);
            this.configTabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.configTabControl.TabIndex = 0;
            // 
            // tabUser
            // 
            this.tabUser.BackColor = System.Drawing.Color.DarkGray;
            this.tabUser.Controls.Add(this.panel2);
            this.tabUser.Controls.Add(this.userDataGrid);
            this.tabUser.Location = new System.Drawing.Point(204, 4);
            this.tabUser.Name = "tabUser";
            this.tabUser.Size = new System.Drawing.Size(784, 477);
            this.tabUser.TabIndex = 2;
            this.tabUser.Text = "Data User";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.buttonUser);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(21, 10);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(751, 30);
            this.panel2.TabIndex = 104;
            // 
            // buttonUser
            // 
            this.buttonUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUser.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonUser.BackgroundImage")));
            this.buttonUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonUser.Location = new System.Drawing.Point(725, 3);
            this.buttonUser.Name = "buttonUser";
            this.buttonUser.Size = new System.Drawing.Size(23, 23);
            this.buttonUser.TabIndex = 104;
            this.buttonUser.UseVisualStyleBackColor = true;
            this.buttonUser.Click += new System.EventHandler(this.buttonUser_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(3, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 18);
            this.label2.TabIndex = 103;
            this.label2.Text = "Data User";
            // 
            // userDataGrid
            // 
            this.userDataGrid.AllowUserToAddRows = false;
            this.userDataGrid.AllowUserToDeleteRows = false;
            this.userDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.userDataGrid.BackgroundColor = System.Drawing.Color.FloralWhite;
            this.userDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.userDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.userID,
            this.userName,
            this.editButton,
            this.groupButton,
            this.departmentButton});
            this.userDataGrid.Location = new System.Drawing.Point(21, 46);
            this.userDataGrid.MultiSelect = false;
            this.userDataGrid.Name = "userDataGrid";
            this.userDataGrid.RowHeadersVisible = false;
            this.userDataGrid.RowTemplate.Height = 30;
            this.userDataGrid.Size = new System.Drawing.Size(751, 420);
            this.userDataGrid.TabIndex = 65;
            this.userDataGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.userDataGrid_CellContentClick);
            // 
            // userID
            // 
            this.userID.HeaderText = "id";
            this.userID.Name = "userID";
            this.userID.Visible = false;
            this.userID.Width = 29;
            // 
            // userName
            // 
            this.userName.HeaderText = "Nama User";
            this.userName.MaxInputLength = 50;
            this.userName.Name = "userName";
            this.userName.ReadOnly = true;
            this.userName.Width = 130;
            // 
            // editButton
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.editButton.DefaultCellStyle = dataGridViewCellStyle1;
            this.editButton.HeaderText = "";
            this.editButton.Name = "editButton";
            this.editButton.Width = 5;
            // 
            // groupButton
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.groupButton.DefaultCellStyle = dataGridViewCellStyle2;
            this.groupButton.HeaderText = "";
            this.groupButton.Name = "groupButton";
            this.groupButton.Width = 5;
            // 
            // departmentButton
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.departmentButton.DefaultCellStyle = dataGridViewCellStyle3;
            this.departmentButton.HeaderText = "";
            this.departmentButton.Name = "departmentButton";
            this.departmentButton.Width = 5;
            // 
            // tabGroup
            // 
            this.tabGroup.BackColor = System.Drawing.Color.DarkGray;
            this.tabGroup.Controls.Add(this.groupDataGrid);
            this.tabGroup.Controls.Add(this.panel3);
            this.tabGroup.Location = new System.Drawing.Point(204, 4);
            this.tabGroup.Name = "tabGroup";
            this.tabGroup.Size = new System.Drawing.Size(784, 477);
            this.tabGroup.TabIndex = 3;
            this.tabGroup.Text = "Data Group";
            // 
            // groupDataGrid
            // 
            this.groupDataGrid.AllowUserToAddRows = false;
            this.groupDataGrid.AllowUserToDeleteRows = false;
            this.groupDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.groupDataGrid.BackgroundColor = System.Drawing.Color.FloralWhite;
            this.groupDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.groupDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.groupID,
            this.groupName,
            this.groupDescription,
            this.groupEditButton,
            this.groupAksesButton});
            this.groupDataGrid.Location = new System.Drawing.Point(21, 46);
            this.groupDataGrid.MultiSelect = false;
            this.groupDataGrid.Name = "groupDataGrid";
            this.groupDataGrid.RowHeadersVisible = false;
            this.groupDataGrid.RowTemplate.Height = 30;
            this.groupDataGrid.Size = new System.Drawing.Size(751, 420);
            this.groupDataGrid.TabIndex = 106;
            this.groupDataGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.groupDataGrid_CellContentClick);
            // 
            // groupID
            // 
            this.groupID.HeaderText = "groupID";
            this.groupID.Name = "groupID";
            this.groupID.Visible = false;
            this.groupID.Width = 87;
            // 
            // groupName
            // 
            this.groupName.HeaderText = "Nama Group";
            this.groupName.MaxInputLength = 50;
            this.groupName.Name = "groupName";
            this.groupName.ReadOnly = true;
            this.groupName.Width = 131;
            // 
            // groupDescription
            // 
            this.groupDescription.HeaderText = "groupDescription";
            this.groupDescription.Name = "groupDescription";
            this.groupDescription.Visible = false;
            this.groupDescription.Width = 183;
            // 
            // groupEditButton
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.groupEditButton.DefaultCellStyle = dataGridViewCellStyle4;
            this.groupEditButton.HeaderText = "";
            this.groupEditButton.Name = "groupEditButton";
            this.groupEditButton.Width = 5;
            // 
            // groupAksesButton
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.groupAksesButton.DefaultCellStyle = dataGridViewCellStyle5;
            this.groupAksesButton.HeaderText = "";
            this.groupAksesButton.Name = "groupAksesButton";
            this.groupAksesButton.Width = 5;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.Gainsboro;
            this.panel3.Controls.Add(this.buttonGroupUser);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(21, 10);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(751, 30);
            this.panel3.TabIndex = 105;
            // 
            // buttonGroupUser
            // 
            this.buttonGroupUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGroupUser.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonGroupUser.BackgroundImage")));
            this.buttonGroupUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonGroupUser.Location = new System.Drawing.Point(725, 3);
            this.buttonGroupUser.Name = "buttonGroupUser";
            this.buttonGroupUser.Size = new System.Drawing.Size(23, 23);
            this.buttonGroupUser.TabIndex = 104;
            this.buttonGroupUser.UseVisualStyleBackColor = true;
            this.buttonGroupUser.Click += new System.EventHandler(this.buttonGroupUser_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(155, 18);
            this.label1.TabIndex = 103;
            this.label1.Text = "Data Group User";
            // 
            // tabDepartment
            // 
            this.tabDepartment.BackColor = System.Drawing.Color.DarkGray;
            this.tabDepartment.Controls.Add(this.departmentDataGrid);
            this.tabDepartment.Controls.Add(this.panel5);
            this.tabDepartment.Location = new System.Drawing.Point(204, 4);
            this.tabDepartment.Name = "tabDepartment";
            this.tabDepartment.Padding = new System.Windows.Forms.Padding(3);
            this.tabDepartment.Size = new System.Drawing.Size(784, 477);
            this.tabDepartment.TabIndex = 0;
            this.tabDepartment.Text = "Data Departemen";
            // 
            // departmentDataGrid
            // 
            this.departmentDataGrid.AllowUserToAddRows = false;
            this.departmentDataGrid.AllowUserToDeleteRows = false;
            this.departmentDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.departmentDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.departmentDataGrid.BackgroundColor = System.Drawing.Color.FloralWhite;
            this.departmentDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.departmentDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.departmentID,
            this.departmentName,
            this.deptDescription,
            this.departmentEditButton});
            this.departmentDataGrid.Location = new System.Drawing.Point(21, 46);
            this.departmentDataGrid.MultiSelect = false;
            this.departmentDataGrid.Name = "departmentDataGrid";
            this.departmentDataGrid.RowHeadersVisible = false;
            this.departmentDataGrid.RowTemplate.Height = 30;
            this.departmentDataGrid.Size = new System.Drawing.Size(751, 420);
            this.departmentDataGrid.TabIndex = 110;
            this.departmentDataGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.departmentDataGrid_CellContentClick);
            // 
            // departmentID
            // 
            this.departmentID.HeaderText = "departmentID";
            this.departmentID.Name = "departmentID";
            this.departmentID.Visible = false;
            this.departmentID.Width = 139;
            // 
            // departmentName
            // 
            this.departmentName.HeaderText = "Nama Departemen";
            this.departmentName.Name = "departmentName";
            this.departmentName.ReadOnly = true;
            this.departmentName.Width = 181;
            // 
            // deptDescription
            // 
            this.deptDescription.HeaderText = "Deskripsi Departemen";
            this.deptDescription.Name = "deptDescription";
            this.deptDescription.ReadOnly = true;
            this.deptDescription.Width = 206;
            // 
            // departmentEditButton
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.departmentEditButton.DefaultCellStyle = dataGridViewCellStyle6;
            this.departmentEditButton.HeaderText = "";
            this.departmentEditButton.Name = "departmentEditButton";
            this.departmentEditButton.Width = 5;
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.BackColor = System.Drawing.Color.Gainsboro;
            this.panel5.Controls.Add(this.buttonDepartment);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Location = new System.Drawing.Point(21, 10);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(751, 30);
            this.panel5.TabIndex = 109;
            // 
            // buttonDepartment
            // 
            this.buttonDepartment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDepartment.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonDepartment.BackgroundImage")));
            this.buttonDepartment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonDepartment.Location = new System.Drawing.Point(725, 3);
            this.buttonDepartment.Name = "buttonDepartment";
            this.buttonDepartment.Size = new System.Drawing.Size(23, 23);
            this.buttonDepartment.TabIndex = 104;
            this.buttonDepartment.UseVisualStyleBackColor = true;
            this.buttonDepartment.Click += new System.EventHandler(this.buttonDepartment_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(3, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(165, 18);
            this.label4.TabIndex = 103;
            this.label4.Text = "Data Departemen";
            // 
            // tabFlow
            // 
            this.tabFlow.BackColor = System.Drawing.Color.DarkGray;
            this.tabFlow.Controls.Add(this.taskDataGrid);
            this.tabFlow.Controls.Add(this.panel4);
            this.tabFlow.Location = new System.Drawing.Point(204, 4);
            this.tabFlow.Name = "tabFlow";
            this.tabFlow.Padding = new System.Windows.Forms.Padding(3);
            this.tabFlow.Size = new System.Drawing.Size(784, 477);
            this.tabFlow.TabIndex = 1;
            this.tabFlow.Text = "Data Tugas";
            // 
            // taskDataGrid
            // 
            this.taskDataGrid.AllowUserToAddRows = false;
            this.taskDataGrid.AllowUserToDeleteRows = false;
            this.taskDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.taskDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.taskDataGrid.BackgroundColor = System.Drawing.Color.FloralWhite;
            this.taskDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.taskDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.taskID,
            this.skenarioName,
            this.noUrut,
            this.deptName,
            this.taskName,
            this.taskParent,
            this.taskDuration,
            this.taskEditButton});
            this.taskDataGrid.Location = new System.Drawing.Point(21, 46);
            this.taskDataGrid.MultiSelect = false;
            this.taskDataGrid.Name = "taskDataGrid";
            this.taskDataGrid.RowHeadersVisible = false;
            this.taskDataGrid.RowTemplate.Height = 30;
            this.taskDataGrid.Size = new System.Drawing.Size(751, 420);
            this.taskDataGrid.TabIndex = 108;
            this.taskDataGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.taskDataGrid_CellContentClick);
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.Gainsboro;
            this.panel4.Controls.Add(this.buttonFlow);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Location = new System.Drawing.Point(21, 10);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(751, 30);
            this.panel4.TabIndex = 107;
            // 
            // buttonFlow
            // 
            this.buttonFlow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonFlow.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonFlow.BackgroundImage")));
            this.buttonFlow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonFlow.Location = new System.Drawing.Point(725, 3);
            this.buttonFlow.Name = "buttonFlow";
            this.buttonFlow.Size = new System.Drawing.Size(23, 23);
            this.buttonFlow.TabIndex = 104;
            this.buttonFlow.UseVisualStyleBackColor = true;
            this.buttonFlow.Click += new System.EventHandler(this.buttonFlow_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(3, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 18);
            this.label3.TabIndex = 103;
            this.label3.Text = "Arus Tugas";
            // 
            // tabCustomer
            // 
            this.tabCustomer.BackColor = System.Drawing.Color.DarkGray;
            this.tabCustomer.Controls.Add(this.custGridView);
            this.tabCustomer.Controls.Add(this.panel6);
            this.tabCustomer.Location = new System.Drawing.Point(204, 4);
            this.tabCustomer.Name = "tabCustomer";
            this.tabCustomer.Size = new System.Drawing.Size(784, 477);
            this.tabCustomer.TabIndex = 4;
            this.tabCustomer.Text = "Data Customer";
            // 
            // custGridView
            // 
            this.custGridView.AllowUserToAddRows = false;
            this.custGridView.AllowUserToDeleteRows = false;
            this.custGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.custGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.custGridView.BackgroundColor = System.Drawing.Color.FloralWhite;
            this.custGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.custGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.custID,
            this.custName,
            this.custDescription,
            this.editCust});
            this.custGridView.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.custGridView.Location = new System.Drawing.Point(21, 46);
            this.custGridView.MultiSelect = false;
            this.custGridView.Name = "custGridView";
            this.custGridView.RowHeadersVisible = false;
            this.custGridView.RowTemplate.Height = 30;
            this.custGridView.Size = new System.Drawing.Size(751, 420);
            this.custGridView.TabIndex = 111;
            this.custGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.custGridView_CellContentClick);
            // 
            // custID
            // 
            this.custID.HeaderText = "custID";
            this.custID.Name = "custID";
            this.custID.Visible = false;
            this.custID.Width = 71;
            // 
            // custName
            // 
            this.custName.HeaderText = "Nama Customer";
            this.custName.Name = "custName";
            this.custName.ReadOnly = true;
            this.custName.Width = 159;
            // 
            // custDescription
            // 
            this.custDescription.HeaderText = "Deskripsi";
            this.custDescription.Name = "custDescription";
            this.custDescription.ReadOnly = true;
            this.custDescription.Width = 112;
            // 
            // editCust
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.editCust.DefaultCellStyle = dataGridViewCellStyle9;
            this.editCust.HeaderText = "";
            this.editCust.Name = "editCust";
            this.editCust.Width = 5;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.BackColor = System.Drawing.Color.Gainsboro;
            this.panel6.Controls.Add(this.buttonCustomer);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Location = new System.Drawing.Point(21, 10);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(751, 30);
            this.panel6.TabIndex = 110;
            // 
            // buttonCustomer
            // 
            this.buttonCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCustomer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonCustomer.BackgroundImage")));
            this.buttonCustomer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonCustomer.Location = new System.Drawing.Point(725, 3);
            this.buttonCustomer.Name = "buttonCustomer";
            this.buttonCustomer.Size = new System.Drawing.Size(23, 23);
            this.buttonCustomer.TabIndex = 104;
            this.buttonCustomer.UseVisualStyleBackColor = true;
            this.buttonCustomer.Click += new System.EventHandler(this.buttonCustomer_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(3, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 18);
            this.label5.TabIndex = 103;
            this.label5.Text = "Data Customer";
            // 
            // tabSales
            // 
            this.tabSales.BackColor = System.Drawing.Color.DarkGray;
            this.tabSales.Controls.Add(this.salesGridView);
            this.tabSales.Controls.Add(this.panel7);
            this.tabSales.Location = new System.Drawing.Point(204, 4);
            this.tabSales.Name = "tabSales";
            this.tabSales.Size = new System.Drawing.Size(784, 477);
            this.tabSales.TabIndex = 5;
            this.tabSales.Text = "Data Sales";
            // 
            // salesGridView
            // 
            this.salesGridView.AllowUserToAddRows = false;
            this.salesGridView.AllowUserToDeleteRows = false;
            this.salesGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.salesGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.salesGridView.BackgroundColor = System.Drawing.Color.FloralWhite;
            this.salesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.salesGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.salesID,
            this.salesName,
            this.salesDesc,
            this.editSales});
            this.salesGridView.Location = new System.Drawing.Point(21, 46);
            this.salesGridView.MultiSelect = false;
            this.salesGridView.Name = "salesGridView";
            this.salesGridView.RowHeadersVisible = false;
            this.salesGridView.RowTemplate.Height = 30;
            this.salesGridView.Size = new System.Drawing.Size(751, 420);
            this.salesGridView.TabIndex = 112;
            this.salesGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.salesGridView_CellContentClick);
            // 
            // salesID
            // 
            this.salesID.HeaderText = "salesID";
            this.salesID.Name = "salesID";
            this.salesID.Visible = false;
            this.salesID.Width = 79;
            // 
            // salesName
            // 
            this.salesName.HeaderText = "Nama Sales";
            this.salesName.Name = "salesName";
            this.salesName.ReadOnly = true;
            this.salesName.Width = 123;
            // 
            // salesDesc
            // 
            this.salesDesc.HeaderText = "Deskripsi";
            this.salesDesc.Name = "salesDesc";
            this.salesDesc.ReadOnly = true;
            this.salesDesc.Width = 112;
            // 
            // editSales
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.editSales.DefaultCellStyle = dataGridViewCellStyle10;
            this.editSales.HeaderText = "";
            this.editSales.Name = "editSales";
            this.editSales.Width = 5;
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.BackColor = System.Drawing.Color.Gainsboro;
            this.panel7.Controls.Add(this.buttonSales);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Location = new System.Drawing.Point(21, 10);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(751, 30);
            this.panel7.TabIndex = 111;
            // 
            // buttonSales
            // 
            this.buttonSales.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSales.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonSales.BackgroundImage")));
            this.buttonSales.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonSales.Location = new System.Drawing.Point(725, 3);
            this.buttonSales.Name = "buttonSales";
            this.buttonSales.Size = new System.Drawing.Size(23, 23);
            this.buttonSales.TabIndex = 104;
            this.buttonSales.UseVisualStyleBackColor = true;
            this.buttonSales.Click += new System.EventHandler(this.buttonSales_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(3, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 18);
            this.label6.TabIndex = 103;
            this.label6.Text = "Data Sales";
            // 
            // tabKompleks
            // 
            this.tabKompleks.BackColor = System.Drawing.Color.DarkGray;
            this.tabKompleks.Controls.Add(this.panel8);
            this.tabKompleks.Controls.Add(this.kompleksGridView);
            this.tabKompleks.Location = new System.Drawing.Point(204, 4);
            this.tabKompleks.Name = "tabKompleks";
            this.tabKompleks.Padding = new System.Windows.Forms.Padding(3);
            this.tabKompleks.Size = new System.Drawing.Size(784, 477);
            this.tabKompleks.TabIndex = 6;
            this.tabKompleks.Text = "Data Kompleks";
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel8.BackColor = System.Drawing.Color.Gainsboro;
            this.panel8.Controls.Add(this.buttonKompleks);
            this.panel8.Controls.Add(this.label7);
            this.panel8.Location = new System.Drawing.Point(17, 10);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(751, 30);
            this.panel8.TabIndex = 106;
            // 
            // buttonKompleks
            // 
            this.buttonKompleks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonKompleks.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonKompleks.BackgroundImage")));
            this.buttonKompleks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonKompleks.Location = new System.Drawing.Point(725, 3);
            this.buttonKompleks.Name = "buttonKompleks";
            this.buttonKompleks.Size = new System.Drawing.Size(23, 23);
            this.buttonKompleks.TabIndex = 104;
            this.buttonKompleks.UseVisualStyleBackColor = true;
            this.buttonKompleks.Click += new System.EventHandler(this.buttonKompleks_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(3, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(139, 18);
            this.label7.TabIndex = 103;
            this.label7.Text = "Data Kompleks";
            // 
            // kompleksGridView
            // 
            this.kompleksGridView.AllowUserToAddRows = false;
            this.kompleksGridView.AllowUserToDeleteRows = false;
            this.kompleksGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kompleksGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.kompleksGridView.BackgroundColor = System.Drawing.Color.FloralWhite;
            this.kompleksGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.kompleksGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.kompleksID,
            this.kompleksName,
            this.kompleksAddres,
            this.editKompleks});
            this.kompleksGridView.Location = new System.Drawing.Point(17, 46);
            this.kompleksGridView.MultiSelect = false;
            this.kompleksGridView.Name = "kompleksGridView";
            this.kompleksGridView.RowHeadersVisible = false;
            this.kompleksGridView.RowTemplate.Height = 30;
            this.kompleksGridView.Size = new System.Drawing.Size(751, 420);
            this.kompleksGridView.TabIndex = 105;
            this.kompleksGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.kompleksGridView_CellContentClick);
            // 
            // kompleksID
            // 
            this.kompleksID.HeaderText = "kompleksID";
            this.kompleksID.Name = "kompleksID";
            this.kompleksID.Visible = false;
            this.kompleksID.Width = 117;
            // 
            // kompleksName
            // 
            this.kompleksName.HeaderText = "Nama Kompleks";
            this.kompleksName.MaxInputLength = 50;
            this.kompleksName.Name = "kompleksName";
            this.kompleksName.ReadOnly = true;
            this.kompleksName.Width = 158;
            // 
            // kompleksAddres
            // 
            this.kompleksAddres.HeaderText = "Alamat";
            this.kompleksAddres.Name = "kompleksAddres";
            this.kompleksAddres.ReadOnly = true;
            this.kompleksAddres.Width = 94;
            // 
            // editKompleks
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.editKompleks.DefaultCellStyle = dataGridViewCellStyle11;
            this.editKompleks.HeaderText = "";
            this.editKompleks.Name = "editKompleks";
            this.editKompleks.Width = 5;
            // 
            // tabKavling
            // 
            this.tabKavling.BackColor = System.Drawing.Color.DarkGray;
            this.tabKavling.Controls.Add(this.buttonAll);
            this.tabKavling.Controls.Add(this.searchKompleks);
            this.tabKavling.Controls.Add(this.kompleksTextBox);
            this.tabKavling.Controls.Add(this.label9);
            this.tabKavling.Controls.Add(this.panel9);
            this.tabKavling.Controls.Add(this.kavlingGridView);
            this.tabKavling.Location = new System.Drawing.Point(204, 4);
            this.tabKavling.Name = "tabKavling";
            this.tabKavling.Padding = new System.Windows.Forms.Padding(3);
            this.tabKavling.Size = new System.Drawing.Size(784, 477);
            this.tabKavling.TabIndex = 7;
            this.tabKavling.Text = "Data Kavling";
            // 
            // buttonAll
            // 
            this.buttonAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonAll.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAll.Location = new System.Drawing.Point(542, 44);
            this.buttonAll.Name = "buttonAll";
            this.buttonAll.Size = new System.Drawing.Size(111, 30);
            this.buttonAll.TabIndex = 112;
            this.buttonAll.Text = "Tampil Semua";
            this.buttonAll.UseVisualStyleBackColor = true;
            this.buttonAll.Click += new System.EventHandler(this.buttonAll_Click);
            // 
            // searchKompleks
            // 
            this.searchKompleks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchKompleks.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchKompleks.Location = new System.Drawing.Point(459, 44);
            this.searchKompleks.Name = "searchKompleks";
            this.searchKompleks.Size = new System.Drawing.Size(77, 30);
            this.searchKompleks.TabIndex = 111;
            this.searchKompleks.Text = "Cari";
            this.searchKompleks.UseVisualStyleBackColor = true;
            this.searchKompleks.Click += new System.EventHandler(this.searchKompleks_Click_1);
            // 
            // kompleksTextBox
            // 
            this.kompleksTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kompleksTextBox.Location = new System.Drawing.Point(117, 46);
            this.kompleksTextBox.MaxLength = 50;
            this.kompleksTextBox.Name = "kompleksTextBox";
            this.kompleksTextBox.ReadOnly = true;
            this.kompleksTextBox.Size = new System.Drawing.Size(336, 27);
            this.kompleksTextBox.TabIndex = 109;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(19, 49);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 18);
            this.label9.TabIndex = 110;
            this.label9.Text = "Kompleks";
            // 
            // panel9
            // 
            this.panel9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel9.BackColor = System.Drawing.Color.Gainsboro;
            this.panel9.Controls.Add(this.buttonKavling);
            this.panel9.Controls.Add(this.label8);
            this.panel9.Location = new System.Drawing.Point(17, 10);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(751, 30);
            this.panel9.TabIndex = 108;
            // 
            // buttonKavling
            // 
            this.buttonKavling.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonKavling.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonKavling.BackgroundImage")));
            this.buttonKavling.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonKavling.Location = new System.Drawing.Point(725, 3);
            this.buttonKavling.Name = "buttonKavling";
            this.buttonKavling.Size = new System.Drawing.Size(23, 23);
            this.buttonKavling.TabIndex = 104;
            this.buttonKavling.UseVisualStyleBackColor = true;
            this.buttonKavling.Click += new System.EventHandler(this.buttonKavling_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(3, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(118, 18);
            this.label8.TabIndex = 103;
            this.label8.Text = "Data Kavling";
            // 
            // kavlingGridView
            // 
            this.kavlingGridView.AllowUserToAddRows = false;
            this.kavlingGridView.AllowUserToDeleteRows = false;
            this.kavlingGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kavlingGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.kavlingGridView.BackgroundColor = System.Drawing.Color.FloralWhite;
            this.kavlingGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.kavlingGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.kavlingID,
            this.kavlingKompleksName,
            this.kavlingName,
            this.editKavling});
            this.kavlingGridView.Location = new System.Drawing.Point(17, 79);
            this.kavlingGridView.MultiSelect = false;
            this.kavlingGridView.Name = "kavlingGridView";
            this.kavlingGridView.RowHeadersVisible = false;
            this.kavlingGridView.RowTemplate.Height = 30;
            this.kavlingGridView.Size = new System.Drawing.Size(751, 387);
            this.kavlingGridView.TabIndex = 107;
            this.kavlingGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.kavlingGridView_CellContentClick);
            // 
            // kavlingID
            // 
            this.kavlingID.HeaderText = "kavlingID";
            this.kavlingID.Name = "kavlingID";
            this.kavlingID.Visible = false;
            this.kavlingID.Width = 96;
            // 
            // kavlingKompleksName
            // 
            this.kavlingKompleksName.HeaderText = "Nama Kompleks";
            this.kavlingKompleksName.MaxInputLength = 50;
            this.kavlingKompleksName.Name = "kavlingKompleksName";
            this.kavlingKompleksName.ReadOnly = true;
            this.kavlingKompleksName.Width = 158;
            // 
            // kavlingName
            // 
            this.kavlingName.HeaderText = "Kavling";
            this.kavlingName.Name = "kavlingName";
            this.kavlingName.ReadOnly = true;
            this.kavlingName.Width = 96;
            // 
            // editKavling
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.editKavling.DefaultCellStyle = dataGridViewCellStyle12;
            this.editKavling.HeaderText = "";
            this.editKavling.Name = "editKavling";
            this.editKavling.Width = 5;
            // 
            // tabSystem
            // 
            this.tabSystem.BackColor = System.Drawing.Color.DarkGray;
            this.tabSystem.Controls.Add(this.AppModeBox);
            this.tabSystem.Location = new System.Drawing.Point(204, 4);
            this.tabSystem.Name = "tabSystem";
            this.tabSystem.Padding = new System.Windows.Forms.Padding(3);
            this.tabSystem.Size = new System.Drawing.Size(784, 477);
            this.tabSystem.TabIndex = 8;
            this.tabSystem.Text = "Data Sistem";
            // 
            // AppModeBox
            // 
            this.AppModeBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AppModeBox.BackColor = System.Drawing.Color.Gainsboro;
            this.AppModeBox.Controls.Add(this.checkBoxAutoLogin);
            this.AppModeBox.Controls.Add(this.comboAkses);
            this.AppModeBox.Controls.Add(this.label10);
            this.AppModeBox.Controls.Add(this.saveButton);
            this.AppModeBox.Controls.Add(this.label12);
            this.AppModeBox.Controls.Add(this.kuartoPrinter);
            this.AppModeBox.Controls.Add(this.printPreviewCheckBox);
            this.AppModeBox.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AppModeBox.Location = new System.Drawing.Point(20, 15);
            this.AppModeBox.Name = "AppModeBox";
            this.AppModeBox.Size = new System.Drawing.Size(750, 443);
            this.AppModeBox.TabIndex = 25;
            this.AppModeBox.TabStop = false;
            // 
            // checkBoxAutoLogin
            // 
            this.checkBoxAutoLogin.AutoSize = true;
            this.checkBoxAutoLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxAutoLogin.Location = new System.Drawing.Point(540, 83);
            this.checkBoxAutoLogin.Name = "checkBoxAutoLogin";
            this.checkBoxAutoLogin.Size = new System.Drawing.Size(100, 20);
            this.checkBoxAutoLogin.TabIndex = 92;
            this.checkBoxAutoLogin.Text = "Auto Login";
            this.checkBoxAutoLogin.UseVisualStyleBackColor = true;
            this.checkBoxAutoLogin.CheckedChanged += new System.EventHandler(this.checkBoxAutoLogin_CheckedChanged);
            // 
            // comboAkses
            // 
            this.comboAkses.Enabled = false;
            this.comboAkses.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboAkses.FormattingEnabled = true;
            this.comboAkses.Items.AddRange(new object[] {
            "Layar Login",
            "Tampilkan Tugas",
            "Ubah Informasi Tugas",
            "Tampilkan Laporan",
            "Pengaturan Sistem"});
            this.comboAkses.Location = new System.Drawing.Point(165, 79);
            this.comboAkses.Name = "comboAkses";
            this.comboAkses.Size = new System.Drawing.Size(358, 26);
            this.comboAkses.TabIndex = 93;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Location = new System.Drawing.Point(32, 82);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(127, 18);
            this.label10.TabIndex = 94;
            this.label10.Text = "Default Layar";
            // 
            // saveButton
            // 
            this.saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveButton.BackColor = System.Drawing.Color.FloralWhite;
            this.saveButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.ForeColor = System.Drawing.Color.Black;
            this.saveButton.Location = new System.Drawing.Point(269, 368);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(195, 54);
            this.saveButton.TabIndex = 28;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label12.Location = new System.Drawing.Point(90, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 18);
            this.label12.TabIndex = 91;
            this.label12.Text = "Printer";
            // 
            // kuartoPrinter
            // 
            this.kuartoPrinter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.kuartoPrinter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.kuartoPrinter.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kuartoPrinter.FormattingEnabled = true;
            this.kuartoPrinter.Location = new System.Drawing.Point(165, 24);
            this.kuartoPrinter.Name = "kuartoPrinter";
            this.kuartoPrinter.Size = new System.Drawing.Size(357, 26);
            this.kuartoPrinter.TabIndex = 48;
            // 
            // printPreviewCheckBox
            // 
            this.printPreviewCheckBox.AutoSize = true;
            this.printPreviewCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.printPreviewCheckBox.Location = new System.Drawing.Point(540, 27);
            this.printPreviewCheckBox.Name = "printPreviewCheckBox";
            this.printPreviewCheckBox.Size = new System.Drawing.Size(117, 20);
            this.printPreviewCheckBox.TabIndex = 25;
            this.printPreviewCheckBox.Text = "Print Preview";
            this.printPreviewCheckBox.UseVisualStyleBackColor = true;
            // 
            // buttonExit
            // 
            this.buttonExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonExit.BackgroundImage")));
            this.buttonExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonExit.Location = new System.Drawing.Point(1016, 3);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(23, 23);
            this.buttonExit.TabIndex = 103;
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitPreferencesToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(68, 26);
            // 
            // exitPreferencesToolStripMenuItem
            // 
            this.exitPreferencesToolStripMenuItem.Name = "exitPreferencesToolStripMenuItem";
            this.exitPreferencesToolStripMenuItem.Size = new System.Drawing.Size(67, 22);
            // 
            // taskID
            // 
            this.taskID.HeaderText = "taskID";
            this.taskID.Name = "taskID";
            this.taskID.Visible = false;
            this.taskID.Width = 72;
            // 
            // skenarioName
            // 
            this.skenarioName.HeaderText = "Skenario";
            this.skenarioName.Name = "skenarioName";
            this.skenarioName.ReadOnly = true;
            this.skenarioName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.skenarioName.Width = 91;
            // 
            // noUrut
            // 
            this.noUrut.HeaderText = "No";
            this.noUrut.Name = "noUrut";
            this.noUrut.ReadOnly = true;
            this.noUrut.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.noUrut.Width = 38;
            // 
            // deptName
            // 
            this.deptName.HeaderText = "Nama Departemen";
            this.deptName.Name = "deptName";
            this.deptName.ReadOnly = true;
            this.deptName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.deptName.Visible = false;
            this.deptName.Width = 180;
            // 
            // taskName
            // 
            this.taskName.HeaderText = "Nama Tugas";
            this.taskName.Name = "taskName";
            this.taskName.ReadOnly = true;
            this.taskName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.taskName.Width = 112;
            // 
            // taskParent
            // 
            this.taskParent.HeaderText = "Induk Tugas";
            this.taskParent.Name = "taskParent";
            this.taskParent.ReadOnly = true;
            this.taskParent.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.taskParent.Visible = false;
            this.taskParent.Width = 112;
            // 
            // taskDuration
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.taskDuration.DefaultCellStyle = dataGridViewCellStyle7;
            this.taskDuration.HeaderText = "Durasi";
            this.taskDuration.Name = "taskDuration";
            this.taskDuration.ReadOnly = true;
            this.taskDuration.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.taskDuration.Width = 70;
            // 
            // taskEditButton
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.taskEditButton.DefaultCellStyle = dataGridViewCellStyle8;
            this.taskEditButton.HeaderText = "";
            this.taskEditButton.Name = "taskEditButton";
            this.taskEditButton.Width = 5;
            // 
            // dataConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1051, 628);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Name = "dataConfigForm";
            this.Text = "PREFERENCES";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.dataConfigForm_Load);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.configTabControl.ResumeLayout(false);
            this.tabUser.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userDataGrid)).EndInit();
            this.tabGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupDataGrid)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabDepartment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.departmentDataGrid)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tabFlow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.taskDataGrid)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tabCustomer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.custGridView)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.tabSales.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.salesGridView)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.tabKompleks.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kompleksGridView)).EndInit();
            this.tabKavling.ResumeLayout(false);
            this.tabKavling.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kavlingGridView)).EndInit();
            this.tabSystem.ResumeLayout(false);
            this.AppModeBox.ResumeLayout(false);
            this.AppModeBox.PerformLayout();
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabControl configTabControl;
        private System.Windows.Forms.TabPage tabDepartment;
        private System.Windows.Forms.TabPage tabFlow;
        private System.Windows.Forms.TabPage tabUser;
        private System.Windows.Forms.TabPage tabGroup;
        protected System.Windows.Forms.DataGridView userDataGrid;
        protected System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem exitPreferencesToolStripMenuItem;
        private System.Windows.Forms.Button buttonUser;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button buttonGroupUser;
        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.DataGridView groupDataGrid;
        protected System.Windows.Forms.DataGridView taskDataGrid;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button buttonFlow;
        protected System.Windows.Forms.Label label3;
        protected System.Windows.Forms.DataGridView departmentDataGrid;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button buttonDepartment;
        protected System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn departmentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn departmentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn deptDescription;
        private System.Windows.Forms.DataGridViewButtonColumn departmentEditButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn groupID;
        private System.Windows.Forms.DataGridViewTextBoxColumn groupName;
        private System.Windows.Forms.DataGridViewTextBoxColumn groupDescription;
        private System.Windows.Forms.DataGridViewButtonColumn groupEditButton;
        private System.Windows.Forms.DataGridViewButtonColumn groupAksesButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn userID;
        private System.Windows.Forms.DataGridViewTextBoxColumn userName;
        private System.Windows.Forms.DataGridViewButtonColumn editButton;
        private System.Windows.Forms.DataGridViewButtonColumn groupButton;
        private System.Windows.Forms.DataGridViewButtonColumn departmentButton;
        private System.Windows.Forms.TabPage tabCustomer;
        private System.Windows.Forms.TabPage tabSales;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button buttonCustomer;
        protected System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button buttonSales;
        protected System.Windows.Forms.Label label6;
        protected System.Windows.Forms.DataGridView custGridView;
        protected System.Windows.Forms.DataGridView salesGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn custID;
        private System.Windows.Forms.DataGridViewTextBoxColumn custName;
        private System.Windows.Forms.DataGridViewTextBoxColumn custDescription;
        private System.Windows.Forms.DataGridViewButtonColumn editCust;
        private System.Windows.Forms.DataGridViewTextBoxColumn salesID;
        private System.Windows.Forms.DataGridViewTextBoxColumn salesName;
        private System.Windows.Forms.DataGridViewTextBoxColumn salesDesc;
        private System.Windows.Forms.DataGridViewButtonColumn editSales;
        private System.Windows.Forms.TabPage tabKompleks;
        private System.Windows.Forms.TabPage tabKavling;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button buttonKompleks;
        protected System.Windows.Forms.Label label7;
        protected System.Windows.Forms.DataGridView kompleksGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn kompleksID;
        private System.Windows.Forms.DataGridViewTextBoxColumn kompleksName;
        private System.Windows.Forms.DataGridViewTextBoxColumn kompleksAddres;
        private System.Windows.Forms.DataGridViewButtonColumn editKompleks;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button buttonKavling;
        protected System.Windows.Forms.Label label8;
        protected System.Windows.Forms.DataGridView kavlingGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn kavlingID;
        private System.Windows.Forms.DataGridViewTextBoxColumn kavlingKompleksName;
        private System.Windows.Forms.DataGridViewTextBoxColumn kavlingName;
        private System.Windows.Forms.DataGridViewButtonColumn editKavling;
        private System.Windows.Forms.TabPage tabSystem;
        private System.Windows.Forms.GroupBox AppModeBox;
        private System.Windows.Forms.ComboBox kuartoPrinter;
        private System.Windows.Forms.CheckBox printPreviewCheckBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button searchKompleks;
        private System.Windows.Forms.TextBox kompleksTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button buttonAll;
        private System.Windows.Forms.CheckBox checkBoxAutoLogin;
        private System.Windows.Forms.CheckBox nonActiveCheckBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboAkses;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskID;
        private System.Windows.Forms.DataGridViewTextBoxColumn skenarioName;
        private System.Windows.Forms.DataGridViewTextBoxColumn noUrut;
        private System.Windows.Forms.DataGridViewTextBoxColumn deptName;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskName;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskParent;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskDuration;
        private System.Windows.Forms.DataGridViewButtonColumn taskEditButton;
    }
}
