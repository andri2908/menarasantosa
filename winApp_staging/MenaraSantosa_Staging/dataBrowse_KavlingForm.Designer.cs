﻿namespace AlphaSoft
{
    partial class dataBrowse_KavlingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Size = new System.Drawing.Size(92, 18);
            this.label1.Text = "Kompleks";
            // 
            // namaTextBox
            // 
            this.namaTextBox.Location = new System.Drawing.Point(107, 27);
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(52, 18);
            this.groupBox1.Size = new System.Drawing.Size(403, 91);
            // 
            // displayAllButton
            // 
            this.displayAllButton.Location = new System.Drawing.Point(52, 115);
            this.displayAllButton.Size = new System.Drawing.Size(403, 37);
            // 
            // nonActiveCheckBox
            // 
            this.nonActiveCheckBox.Location = new System.Drawing.Point(107, 60);
            // 
            // newButton
            // 
            this.newButton.Visible = false;
            // 
            // dataBrowse_KavlingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(513, 549);
            this.Name = "dataBrowse_KavlingForm";
            this.Text = "DATA KAVLING";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.dataBrowse_KavlingForm_FormClosed);
            this.Load += new System.EventHandler(this.dataBrowse_KavlingForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}
