﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Globalization;

namespace AlphaSoft
{
    public partial class dataJobsForm : AlphaSoft.basicHotkeysForm
    {
        private int originModuleID = 0;

        private CultureInfo culture = new CultureInfo("id-ID");

        private globalUserUtil gUser = new globalUserUtil();
        private globalRestAPI gRest = new globalRestAPI();
        private globalFTPLib gFtp = new globalFTPLib();

        List<cbDataSource> cbType = new List<cbDataSource>();

        private int jobsID = 0;
        private int deptID = 0;
        private int taskID = 0;
        private int kompleksID = 0;
        private int kavlingID = 0;
        private int customerID = 0;
        private int salesID = 0;
        private string denahFilename = "";
        private string selectedFileName = "";
        private string fileNameOnly = "";
        private int startTaskID = 0;
        private int startTaskUrutan = 0;

        public dataJobsForm(int departmentID, string departmentName, int jobsIDParam = 0, 
            int moduleID = globalConstants.NEW_JOBS)
        {
            InitializeComponent();

            this.Text = "DATA JOBS [" + departmentName + "]";

            deptID = departmentID;
            jobsID = jobsIDParam;

            if (jobsID > 0)
            {
                if (moduleID == globalConstants.NEW_JOBS)
                    originModuleID = globalConstants.EDIT_JOBS;
                else
                    originModuleID = moduleID;
            }
            else
                originModuleID = globalConstants.NEW_JOBS;
        }

        private void fillInComboBayar()
        {
            cbDataSource cbElement;

            cbType.Clear();

            cbElement = new cbDataSource();
            cbElement.displayMember = "CASH KERAS";
            cbElement.valueMember = globalConstants.PAYMENT_CASH_KERAS.ToString();
            cbType.Add(cbElement);

            cbElement = new cbDataSource();
            cbElement.displayMember = "CASH TERMIN";
            cbElement.valueMember = globalConstants.PAYMENT_CASH_TERMIN.ToString(); ;
            cbType.Add(cbElement);

            cbElement = new cbDataSource();
            cbElement.displayMember = "KPR";
            cbElement.valueMember = globalConstants.PAYMENT_KPR.ToString(); ;
            cbType.Add(cbElement);

            comboBayar.DataSource = cbType;
            comboBayar.DisplayMember = "displayMember";
            comboBayar.ValueMember = "valueMember";

            comboBayar.SelectedValue = globalConstants.PAYMENT_CASH_KERAS.ToString();
            comboBayar.SelectedIndex = 0;
        }

        private void loadDataJobs()
        {
            REST_jobHeader jobData = new REST_jobHeader();

            string sqlCommand = "SELECT jh.*, IFNULL(mm2.task_name , '-') AS task_name, " +
                                            "IFNULL(mm.skenario_name , '-') AS skenario_name, " +
                                            "IFNULL(mm2.urutan , 0) AS urutan, " +
                                            "IFNULL(mk.kompleks_name, '-') as kompleks_name, " +
                                            "IFNULL(ms.sales_name, '-') as sales_name, " +
                                            "IFNULL(mc.customer_name, '-') as customer_name, " +
                                            "CONCAT(IFNULL(mb.blok_name, ''), '-', IFNULL(mv.house_no, '')) as kavling_name " +
                                            "FROM data_jobs_header jh " +
                                            "LEFT OUTER JOIN master_task mm ON (jh.task_id = mm.task_id) " +
                                            "LEFT OUTER JOIN master_task mm2 ON (jh.start_task_id = mm2.task_id) " +
                                            "LEFT OUTER JOIN master_kompleks mk ON (jh.kompleks_id = mk.kompleks_id) " +
                                            "LEFT OUTER JOIN master_sales ms ON (jh.sales_id = ms.sales_id) " +
                                            "LEFT OUTER JOIN master_customer mc ON (jh.customer_id = mc.customer_id) " +
                                            "LEFT OUTER JOIN master_kavling mv ON (jh.kavling_id = mv.kavling_id) " +
                                            "LEFT OUTER JOIN master_blok mb ON (mv.blok_id = mb.blok_id) " +
                                            "WHERE jh.job_id = " + jobsID;

            if (gRest.getJobsHeader(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref jobData))
            {
                if (jobData.dataStatus.o_status == 1)
                {
                    taskID = jobData.dataList[0].task_id;
                    skenarioTextBox.Text = jobData.dataList[0].skenario_name;
                    startProsesTextBox.Text = jobData.dataList[0].task_name;

                    jobsDescription.Text = jobData.dataList[0].jobs_description;

                    kompleksID = jobData.dataList[0].kompleks_id;
                    kompleksTextBox.Text = jobData.dataList[0].kompleks_name;

                    kavlingID = jobData.dataList[0].kavling_id;
                    kavlingTextBox.Text = jobData.dataList[0].kavling_name;

                    salesID = jobData.dataList[0].sales_id;
                    salesTextBox.Text = jobData.dataList[0].sales_name;

                    customerID = jobData.dataList[0].customer_id;
                    customerTextBox.Text = jobData.dataList[0].customer_name;

                    ltTextBox.Text = jobData.dataList[0].luas_tanah.ToString("N2", culture);
                    lbTextBox.Text = jobData.dataList[0].luas_bangunan.ToString("N2", culture);

                    comboBayar.SelectedValue = jobData.dataList[0].sistem_pembayaran.ToString();

                    ppnCheckBox.Checked = (jobData.dataList[0].is_ppn == "Y" ? true : false);

                    deadlineDTPicker.Value = jobData.dataList[0].deadline_bast;
                    realisasiDTPicker.Value = jobData.dataList[0].realisasi_bast;

                    denahTextBox.Text = jobData.dataList[0].denah_filename;
                    denahFilename = jobData.dataList[0].denah_filename;

                    if (denahFilename.Length > 0)
                        buttonPreview.Enabled = true;

                    promoTextBox.Text = jobData.dataList[0].promo_value;

                    nonAktifCheckbox.Checked = (jobData.dataList[0].is_active == "N" ? true : false);

                    startTaskID = jobData.dataList[0].start_task_id;
                    startTaskUrutan = jobData.dataList[0].urutan;
                }
            }
        }

        private void dataJobsForm_Load(object sender, EventArgs e)
        {
            fillInComboBayar();

            if (originModuleID == globalConstants.EDIT_JOBS || 
                originModuleID == globalConstants.VIEW_JOBS)
            {
                loadDataJobs();
                searchTask.Enabled = false;
                buttonSearchProses.Enabled = false;

                if (kompleksID > 0)
                    searchKavling.Enabled = true;
            }
            else
            {
                nonAktifCheckbox.Visible = false;
            }

            if (originModuleID == globalConstants.VIEW_JOBS)
            {
                gUser.disableAllControls(this);

                if (denahFilename.Length > 0)
                    buttonPreview.Enabled = true;
            }

            ltTextBox.Enter += TextBox_Enter;
            ltTextBox.Leave += TextBox_Leave;

            lbTextBox.Enter += TextBox_Enter;
            lbTextBox.Leave += TextBox_Leave;
        }

        private void searchKompleks_Click(object sender, EventArgs e)
        {
            dataBrowse_KompleksForm displayForm = new dataBrowse_KompleksForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                kompleksID = Convert.ToInt32(displayForm.ReturnValue1);
                kompleksTextBox.Text = displayForm.ReturnValue2;

                searchKavling.Enabled = true;

                kavlingID = 0;
                kavlingTextBox.Text = "";
            }
        }

        private void searchKavling_Click(object sender, EventArgs e)
        {
            dataBrowse_KavlingForm displayForm = new dataBrowse_KavlingForm(globalConstants.MODULE_DEFAULT, kompleksID);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                kavlingID = Convert.ToInt32(displayForm.ReturnValue1);
                kavlingTextBox.Text = displayForm.ReturnValue2;
            }
        }

        private void searchCustomer_Click(object sender, EventArgs e)
        {
            dataBrowse_CustomerForm displayForm = new dataBrowse_CustomerForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                customerID = Convert.ToInt32(displayForm.ReturnValue1);
                customerTextBox.Text = displayForm.ReturnValue2;
            }
        }

        private void searchSales_Click(object sender, EventArgs e)
        {
            dataBrowse_SalesForm displayForm = new dataBrowse_SalesForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                salesID = Convert.ToInt32(displayForm.ReturnValue1);
                salesTextBox.Text = displayForm.ReturnValue2;
            }
        }

        private void searchTask_Click(object sender, EventArgs e)
        {
            dataBrowse_TaskForm displayForm = new dataBrowse_TaskForm(globalConstants.MODULE_JOBS, deptID);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                taskID = Convert.ToInt32(displayForm.ReturnValue1);
                skenarioTextBox.Text = displayForm.skenarioName;
                taskTextBox.Text = displayForm.ReturnValue2;
                startProsesTextBox.Text = displayForm.ReturnValue2;
                startTaskID = taskID;
            }
        }

        public int getNumOfActiveChild()
        {
            int numOfChild = 0;
            string sqlCommand = "";

            sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                        "FROM data_jobs_detail WHERE job_id = " + jobsID + " " +
                                        "AND (is_active = 'Y' OR task_status = 'PENDING') " +
                                        "AND task_id <> " + taskID;

            genericReply replyResult = new genericReply();
            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
            {
                if (replyResult.o_status == 1)
                {
                    numOfChild = Convert.ToInt32(replyResult.data);
                }
            }

            return numOfChild;
        }

        private bool dataValid()
        {
            errorLabel.Text = "";

            if (taskID == 0)
            {
                errorLabel.Text = "Task belum dipilih";
                return false;
            }

            if (kompleksID == 0)
            {
                errorLabel.Text = "Kompleks belum dipilih";
                return false;
            }

            if (kavlingID == 0)
            {
                errorLabel.Text = "Kavling belum dipilih";
                return false;
            }

            double tempValue = 0;
            if (ltTextBox.Text.Length <= 0 || !double.TryParse(ltTextBox.Text, NumberStyles.Number, culture, out tempValue))
            {
                errorLabel.Text = "Input LT salah";
                return false;
            }

            if (lbTextBox.Text.Length <= 0 || !double.TryParse(lbTextBox.Text, NumberStyles.Number, culture, out tempValue))
            {
                errorLabel.Text = "Input LB salah";
                return false;
            }

            if (nonAktifCheckbox.Checked)
            {
                if (getNumOfActiveChild() > 0)
                {
                    errorLabel.Text = "Ada tugas aktif, tidak bisa diset non aktif";
                    return false;
                }
            }

            if (denahTextBox.Text.Length > 0)
            {
                if (denahTextBox.Text.IndexOf(' ') >=0)
                {
                    errorLabel.Text = "Nama file tidak boleh ada spasi";
                    return false;
                }
            }

            return true;
        }

        private void doUploadFile(string jobsID)
        {
            string erMsg = "";
            int resultVal;

            if (!gFtp.checkFTPDirectoryExist(jobsID.ToString()))
            {
                gFtp.createFTPDirectory(jobsID.ToString());
            }

            if (gFtp.uploadToFTP(jobsID, fileNameOnly, selectedFileName))
            {
                resultVal = gFtp.checkFTPFileExist(jobsID, fileNameOnly, out erMsg);

                if (resultVal != globalFTPLib.FILE_FOUND)
                {
                    throw new Exception(erMsg);
                }
            }
            else
            {
                throw new Exception("");
            }
        }
        
        private bool saveTrans()
        {
            bool result = false;
            int opType = 0;

            double luasBangunan = 0;
            double luasTanah = 0;

            List<jobs_header> listHeader = new List<jobs_header>();
            jobs_header headerData;

            List<jobs_detail> listDetail = new List<jobs_detail>();
            jobs_detail detailData;

            genericReply replyResult = new genericReply();

            try
            {
                double.TryParse(lbTextBox.Text, NumberStyles.Number, culture, out luasBangunan);
                double.TryParse(ltTextBox.Text, NumberStyles.Number, culture, out luasTanah);

                #region HEADER
                headerData = new jobs_header();
                switch (originModuleID)
                {
                    case globalConstants.NEW_JOBS:
                        opType = 1;
                        headerData.job_id = 0;
                        break;

                    case globalConstants.EDIT_JOBS:
                        opType = 2;
                        headerData.job_id = jobsID;
                        break;
                }

                headerData.jobs_datetime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                headerData.department_id = deptID;
                headerData.jobs_description = jobsDescription.Text;
                headerData.kompleks_id = kompleksID;
                headerData.kavling_id = kavlingID;
                headerData.sales_id = salesID;
                headerData.task_id = taskID;
                headerData.customer_id = customerID;
                headerData.luas_tanah = luasTanah;
                headerData.luas_bangunan = luasBangunan;
                headerData.sistem_pembayaran = Convert.ToInt32(comboBayar.SelectedValue);
                headerData.is_ppn = (ppnCheckBox.Checked ? "Y" : "N");
                headerData.deadline_bast = new DateTime(deadlineDTPicker.Value.Year, deadlineDTPicker.Value.Month, deadlineDTPicker.Value.Day);
                headerData.realisasi_bast = new DateTime(realisasiDTPicker.Value.Year, realisasiDTPicker.Value.Month, realisasiDTPicker.Value.Day);
                headerData.denah_filename = denahTextBox.Text;
                headerData.promo_value = promoTextBox.Text;
                headerData.is_active = (nonAktifCheckbox.Checked ? "N" : "Y");
                headerData.start_task_id = startTaskID;

                listHeader.Add(headerData);
                #endregion

                #region DETAIL
                if (originModuleID == globalConstants.NEW_JOBS)
                {
                    if (startTaskID == taskID)
                    {
                        detailData = new jobs_detail();
                        detailData.id = 0;
                        detailData.job_id = jobsID;
                        detailData.task_id = taskID;
                        detailData.task_start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                        detailData.notes = "";
                        detailData.task_status = "PENDING";
                        detailData.reject_count = 0;
                        detailData.is_active = "Y";

                        listDetail.Add(detailData);
                    }
                    else
                    {
                        detailData = new jobs_detail();
                        detailData.id = 0;
                        detailData.job_id = jobsID;
                        detailData.task_id = taskID;
                        detailData.task_start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                        detailData.notes = "SKIP PROSES";
                        detailData.task_status = "FINISH";
                        detailData.reject_count = 0;
                        detailData.is_active = "Y";

                        listDetail.Add(detailData);

                        REST_task taskData = new REST_task();
                        string sqlCommand = "SELECT mt.* " +
                                            "FROM master_task mt " +
                                            "WHERE mt.origin_parent_task_id = " + taskID + " " +
                                            "ORDER BY mt.urutan ASC";
                        
                        if (gRest.getTaskData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref taskData))
                        {
                            if (taskData.dataStatus.o_status == 1)
                            {
                                for (int i = 0;i<taskData.dataList.Count && taskData.dataList[i].urutan <= startTaskUrutan;i++)
                                {
                                    detailData = new jobs_detail();
                                    detailData.id = 0;
                                    detailData.job_id = jobsID;
                                    detailData.task_id = taskData.dataList[i].task_id;
                                    detailData.task_start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                                    detailData.notes = (taskData.dataList[i].urutan < startTaskUrutan ? "SKIP PROSES" : "");
                                    detailData.task_status = (taskData.dataList[i].urutan < startTaskUrutan ? "FINISH" : "PENDING"); 
                                    detailData.reject_count = 0;
                                    detailData.is_active = "Y";

                                    listDetail.Add(detailData);
                                }
                            }
                        }
                    }
                }
                #endregion

                int newID = 0;
                string errMsg;
                if (!gRest.saveDataJobs(gUser.getUserID(), gUser.getUserToken(), listHeader, listDetail, opType, 
                    out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                if (originModuleID == globalConstants.NEW_JOBS)
                {
                    jobsID = newID;
                }

                #region UPLOAD FTP
                if (selectedFileName.Length > 0) // ADA FILE BARU
                    doUploadFile(jobsID.ToString());
                #endregion

                result = true;
            }
            catch (Exception ex) { errorLabel.Text = ex.Message; }
         
            return result;
        }

        private bool saveData()
        {
            if (dataValid())
                return saveTrans();

            return false;
        }


        private void saveButton_Click(object sender, EventArgs e)
        {
            if (saveData())
            {
                MessageBox.Show("SUCCESS");
                this.Close();
            }
            else
            {
                MessageBox.Show("FAIL");
            }
        }

        private void buttonPreview_Click(object sender, EventArgs e)
        {
            string erMsg = "";
            int resultVal = 0;
            string fullFilePath = "";

            resultVal = gFtp.checkFTPFileExist(jobsID.ToString(), denahFilename, out erMsg);

            if (resultVal == globalFTPLib.FILE_FOUND)
            {
                if (!gFtp.previewFromFTP(jobsID.ToString(), denahFilename, out fullFilePath))
                {
                    MessageBox.Show("Preview gagal");
                }
                else
                {
                    gFtp.openFileUsingDefaultAssoc(fullFilePath);
                }
            }
            else
            {
                MessageBox.Show(erMsg);
            }
        }

        private void searchDenah_Click(object sender, EventArgs e)
        {
            string[] arrFileName;

            openPDFDialog.DefaultExt = "*.pdf";

            if (DialogResult.OK == openPDFDialog.ShowDialog())
            {
                selectedFileName = openPDFDialog.FileName;
                arrFileName = selectedFileName.Split('\\');

                fileNameOnly = arrFileName[arrFileName.Length - 1];

                denahTextBox.Text = fileNameOnly;
            }
            else
            {
                selectedFileName = "";
                fileNameOnly = "";
            }
        }

        private void buttonSearchProses_Click(object sender, EventArgs e)
        {
            dataBrowse_TaskForm displayForm = new dataBrowse_TaskForm(globalConstants.SELECT_START_PROSES, taskID);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                startTaskID = Convert.ToInt32(displayForm.ReturnValue1);
                startProsesTextBox.Text = displayForm.ReturnValue2;
                startTaskUrutan = displayForm.urutanTask;
            }
        }
    }
}
