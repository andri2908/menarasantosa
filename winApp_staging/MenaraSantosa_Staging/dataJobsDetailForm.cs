﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Globalization;

namespace AlphaSoft
{
    public partial class dataJobsDetailForm : AlphaSoft.basicHotkeysForm
    {
        private globalUserUtil gUser = new globalUserUtil();
        
        private globalRestAPI gRest = new globalRestAPI();

        private int jobID = 0;
        private int jobDetailID = 0;
        private int jobDetailTaskID = 0;
        private int deptID = 0;
        private string deptName = "";

        private List<child_task> listTask = new List<child_task>();

        public dataJobsDetailForm(int jobDetailIDParam)
        {
            InitializeComponent();

            jobDetailID = jobDetailIDParam;
        }

        private int getDepartmentID()
        {
            int deptIDParam = 0;

            string sqlCommand = "SELECT department_id as resultQuery FROM data_jobs_header WHERE job_id = " + jobID;

            genericReply replyResult = new genericReply();
            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
            {
                if (replyResult.o_status == 1)
                {
                    deptIDParam = Convert.ToInt32(replyResult.data);
                }
            }

            return deptIDParam;
        }

        private string getDepartmentName()
        {
            string deptNameParam = "";

            string sqlCommand = "SELECT department_name as resultQuery FROM master_department WHERE department_id = " + deptID;

            genericReply replyResult = new genericReply();
            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
            {
                if (replyResult.o_status == 1)
                {
                    deptNameParam = replyResult.data.ToString();
                }
            }

            return deptNameParam;
        }

        private void loadJobNotes()
        {
            REST_jobNotes jobData = new REST_jobNotes();

            string sqlCommand = "SELECT jd.*, IFNULL(ul.user_name , '-') AS user_name " +
                                            "FROM data_jobs_detail_notes jd " +
                                            "LEFT OUTER JOIN user_login_data ul ON (jd.last_updated_by = ul.user_id) " +
                                            "WHERE jd.job_detail_id = " + jobDetailID + " " +
                                            "ORDER BY jd.last_update_date DESC";

            if (gRest.getJobsNotes(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref jobData))
            {
                if (jobData.dataStatus.o_status == 1)
                {
                    notesGridView.Rows.Clear();
                    for (int i = 0;i<jobData.dataList.Count;i++)
                    {
                        notesGridView.Rows.Add(
                            gUser.getCustomStringFormatDate(jobData.dataList[i].last_update_date),
                            jobData.dataList[i].user_name,
                            jobData.dataList[i].notes_value
                            );
                    }
                }
            }
        }

        private void loadJobDetailInfo()
        {
            REST_jobDetail jobData = new REST_jobDetail();

            string sqlCommand = "SELECT jd.*, IFNULL(mm.task_name , '-') AS task_name, IFNULL(mm.task_duration, 0) as task_duration " +
                                            "FROM data_jobs_detail jd " +
                                            "LEFT OUTER JOIN master_task mm ON (jd.task_id = mm.task_id) " +
                                            "WHERE jd.id = " + jobDetailID;

            if (gRest.getJobsDetail(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref jobData))
            {
                if (jobData.dataStatus.o_status == 1)
                {
                    jobID = jobData.dataList[0].job_id;
                    jobDetailTaskID = jobData.dataList[0].task_id;
                    taskNameTextBox.Text = jobData.dataList[0].task_name;
                    taskDateDTPicker.Value = jobData.dataList[0].task_start;

                    TimeSpan ts = DateTime.Now - jobData.dataList[0].task_start;
                    labelDurasi.Text = ts.Days.ToString() + " hari";

                    if (ts.Days > jobData.dataList[0].task_duration)
                        labelDurasi.ForeColor = Color.Red;

                    loadJobNotes();
                }
            }
        }

        private int getParentJobDetailID()
        {
            int parentJobID = 0;

            string sqlCommand = 
                "SELECT dd.id as resultQuery " +
                "FROM data_jobs_detail dd, master_task mt " +
                "WHERE dd.task_id = mt.task_parent " +
                "AND dd.job_id = " + jobID + " " +
                "AND mt.task_id = " + jobDetailTaskID + " " +
                "AND mt.department_id = " + deptID;

            genericReply replyResult = new genericReply();
            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
            {
                if (replyResult.o_status == 1)
                {
                    parentJobID = Convert.ToInt32(replyResult.data);
                }
            }

            return parentJobID;
        }

        private int getParentID()
        {
            int parentJobID = 0;

            string sqlCommand =
                "SELECT task_parent as resultQuery " +
                "FROM master_task " +
                "WHERE task_id = " + jobDetailTaskID;// + " " +
                //"AND department_id = " + deptID;

            genericReply replyResult = new genericReply();
            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
            {
                if (replyResult.o_status == 1)
                {
                    parentJobID = Convert.ToInt32(replyResult.data);
                }
            }

            return parentJobID;
        }

        private void getChildTaskList()
        {
            REST_childTaskList restChildTask = new REST_childTaskList();
            child_task childTask;

            string sqlCommand =
                "SELECT mt.task_id, IFNULL(jd.id, 0) AS id, IFNULL(jd.task_status, 'PENDING') as task_status " +
                "FROM master_task mt " +
                "LEFT OUTER JOIN data_jobs_detail jd ON (mt.task_id = jd.task_id AND jd.job_id = " + jobID + ") " +
                "WHERE mt.task_parent = " + jobDetailTaskID + " " +
                //"AND mt.department_id = " + deptID + " " +
                "AND mt.is_active = 'Y'";

            if (gRest.getChildTaskList(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref restChildTask))
            {
                if (restChildTask.dataStatus.o_status == 1)
                {
                    listTask.Clear();

                    for (int i = 0;i< restChildTask.dataList.Count;i++)
                    {
                        childTask = new child_task();
                        childTask.id = restChildTask.dataList[i].id;
                        childTask.task_id = restChildTask.dataList[i].task_id;
                        childTask.task_status = restChildTask.dataList[i].task_status;

                        if (childTask.task_status != "FINISH")
                            listTask.Add(childTask);
                    }
                }
            }
        }

        private void dataJobsDetailForm_Load(object sender, EventArgs e)
        {
            loadJobDetailInfo();

            deptID = getDepartmentID();
            deptName = getDepartmentName();

            int parentID = getParentID();
            if (parentID == 0)
                rejectButton.Enabled = false;
        }

        private void buttonJobInfo_Click(object sender, EventArgs e)
        {
            dataJobsForm displayForm;
            displayForm = new dataJobsForm(deptID, deptName, jobID, globalConstants.VIEW_JOBS);
                
            displayForm.ShowDialog(this);
        }

        private bool doReject()
        {
            bool result = false;

            List<jobs_detail> listDetail = new List<jobs_detail>();
            jobs_detail detailData;

            try
            {
                // UPDATE CURRENT JOB DETAIL
                detailData = new jobs_detail();
                detailData.id = jobDetailID;
                detailData.is_active = "Y";
                detailData.notes = notesTextBox.Text;
                detailData.task_status = "REJECTED";
                listDetail.Add(detailData);

                // GET PARENT DETAILS           
                detailData = new jobs_detail();
                detailData.id = getParentJobDetailID();
                detailData.is_active = "Y";
                detailData.task_status = "PENDING";
                listDetail.Add(detailData);

                string errMsg = "";
                int newID = 0;

                if (!gRest.saveDataJobsDetail(gUser.getUserID(), gUser.getUserToken(), listDetail, jobID,
                    out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private void rejectButton_Click(object sender, EventArgs e)
        {
            errorLabel.Text = "";
            if (DialogResult.Yes == MessageBox.Show("Tolak Tugas ?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {
                if (doReject())
                {
                    MessageBox.Show("SUCCESS");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("FAIL");
                }
            }
        }

        private bool doFinish()
        {
            bool result = false;

            List<jobs_detail> listDetail = new List<jobs_detail>();
            jobs_detail detailData;

            try
            {
                getChildTaskList();

                // UPDATE CURRENT JOB DETAIL
                detailData = new jobs_detail();
                detailData.id = jobDetailID;
                detailData.is_active = "Y";
                detailData.notes = notesTextBox.Text;
                detailData.task_status = "FINISH";
                listDetail.Add(detailData);

                // CREATE / UPDATE CHILD TASK
                for (int i = 0;i<listTask.Count;i++)
                {
                    detailData = new jobs_detail();
                    detailData.id = listTask[i].id;
                    detailData.job_id = jobID;
                    detailData.task_id = listTask[i].task_id;
                    detailData.task_status = "PENDING";
                    detailData.notes = "";
                    detailData.is_active = "Y";

                    listDetail.Add(detailData);
                }

                string errMsg = "";
                int newID = 0;

                if (!gRest.saveDataJobsDetail(gUser.getUserID(), gUser.getUserToken(), listDetail, jobID,
                    out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                if (!gRest.checkJobFinished(gUser.getUserID(), gUser.getUserToken(), jobID,
                    out errMsg))
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private void finishButton_Click(object sender, EventArgs e)
        {
            errorLabel.Text = "";
            if (DialogResult.Yes == MessageBox.Show("Tugas Selesai ?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {
                if (doFinish())
                {
                    MessageBox.Show("SUCCESS");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("FAIL");
                }
            }
        }

        private bool doUpdate()
        {
            bool result = false;

            try
            {
                string errMsg = "";
                int newID = 0;

                if (!gRest.saveDataJobsNotes(gUser.getUserID(), gUser.getUserToken(), jobDetailID, notesTextBox.Text,
                    out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            errorLabel.Text = "";

            if (DialogResult.Yes == MessageBox.Show("Simpan Catatan ?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {
                if (notesTextBox.Text.Length <= 0)
                {
                    if (DialogResult.No == MessageBox.Show("Catatan Kosong, lanjutkan ?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                        return;
                }

                if (doUpdate())
                {
                    MessageBox.Show("SUCCESS");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("FAIL");
                }
            }
        }
    }
}
