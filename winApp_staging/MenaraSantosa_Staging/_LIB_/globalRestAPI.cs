﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;

using RestSharp;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AlphaSoft
{
    class globalRestAPI
    {
        private const int LAST_LOGIN = 1;
        private const int LAST_LOGOUT = 2;

        //public const string SERVER_URL = "http://staging.menarasantosa.com/public";
        public const string SERVER_URL = "http://localhost:8002";
        public const string FUNC_GETDATA = "/getData";

        private const string FUNC_GETDATASINGLEVALUE = "/getDataSingleValue";
        private const string FUNC_GETDATASINGLEVALUENOPARAM = "/getDataSingleValueNoParam";
        private const string FUNC_GETDATANOPARAM = "/getDataNoParam";

        private const string FUNC_USER_LOGIN = "/user_login";
        private const string FUNC_SAVE_LAST_LOGIN = "/saveLastLogin";
        private const string FUNC_GET_USERGROUPACCESS = "/getUserGroupAccess";
        private const string FUNC_CHANGE_PASSWORD = "/changePassword";
        private const string FUNC_SAVE_GROUP_USER = "/saveGroupUser";
        private const string FUNC_SAVE_USER_LOGIN_DATA = "/saveUserData";
        private const string FUNC_SAVE_MODULE_ACCESS = "/saveModuleAccess";
        private const string FUNC_SAVE_USER_ACCESS_LIST = "";
        private const string FUNC_SAVE_SYS_CONFIG = "";

        private const string FUNC_SAVE_USER_GROUP_DATA = "/saveUserGroupData";
        private const string FUNC_SAVE_USER_DEPT_DATA = "/saveUserDepartmentData";
        private const string FUNC_SAVE_DEPARTMENT = "/saveDepartment";
        private const string FUNC_SAVE_TASK = "/saveTask";
        private const string FUNC_SAVE_SALES = "/saveSales";
        private const string FUNC_SAVE_CUSTOMER = "/saveCustomer";
        private const string FUNC_SAVE_JOBS = "/saveJobs";
        private const string FUNC_SAVE_JOBS_DETAIL = "/saveJobsDetail";
        private const string FUNC_SAVE_JOBS_NOTES = "/saveJobsNote";

        private const string FUNC_SAVE_MASTER_KOMPLEKS = "/saveMasterKompleks";
        private const string FUNC_SAVE_MASTER_KAVLING = "/saveMasterKavling";
        private const string FUNC_CHECK_JOB_FINISH = "/checkJobsFinished";
        private const string FUNC_SAVE_AUTOLOGIN = "/saveAutoLogin";


        private JsonSerializer jsonConvert = new JsonSerializer();

        public bool checkDataExist(int userID, string accessToken, string sqlComm, ref genericReply replyResult)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATASINGLEVALUE);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlComm);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
                return false;

            replyResult = (genericReply)jsonResponse[0];

            if (replyResult.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getDataSingleValue(int userID, string accessToken, string sqlComm, ref genericReply replyResult)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATASINGLEVALUE);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlComm);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
                return false;

            replyResult = (genericReply)jsonResponse[0];

            if (replyResult.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getUserTokenData(string userName, string userPassword, ref REST_userAccessData userTokenData)
        {
            #region CHECK USER CORRECT
            genericReply replyResult = new genericReply();
            string sqlQuery = "SELECT COUNT(1) AS resultQuery " +
                                        "FROM user_login_data ul " +
                                        "WHERE UPPER(ul.user_name) = '" + userName.ToUpper() + "' " +
                                        "AND ul.user_password = '" + userPassword + "' " +
                                        "AND ul.is_active = 'Y'";

            var client = new RestClient(SERVER_URL + FUNC_GETDATASINGLEVALUENOPARAM);
            var request = new RestRequest(Method.POST);

            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
                return false;

            replyResult = (genericReply)jsonResponse[0];

            if (null == replyResult || 
                replyResult.o_status == -1 ||
                Convert.ToInt32(replyResult.data) <= 0)
            {
                return false;
            }
            #endregion

            #region USER TOKEN
            sqlQuery = "SELECT ul.user_id, ul.user_name, ul.user_full_name, ut.access_token " +
                                        "FROM user_login_data ul, user_token ut " +
                                        "WHERE ul.user_id = ut.user_id " +
                                        "AND UPPER(ul.user_name) = '" + userName.ToUpper() + "' " +
                                        "AND ul.user_password = '" + userPassword + "' " +
                                        "AND ul.is_active = 'Y'";

            client = new RestClient(SERVER_URL + FUNC_GETDATANOPARAM);
            request = new RestRequest(Method.POST);

            request.AddParameter("sqlQuery", sqlQuery);

            response = client.Execute(request);

            restResult = response.Content;
            var jsonResponseToken = JsonConvert.DeserializeObject<REST_userAccessData>(response.Content);

            if (null == jsonResponseToken)
                return false;

            userTokenData = (REST_userAccessData)jsonResponseToken;

            if (null != userTokenData && 
                userTokenData.dataStatus.o_status == 1)
                return true;
            else
                return false;
            #endregion
        }

        public bool getUserTokenDataByUserID(int userID, ref REST_userAccessData userTokenData)
        {
            genericReply replyResult = new genericReply();
            string sqlQuery = "";
           
            #region USER TOKEN
            sqlQuery = "SELECT ul.user_id, ul.user_name, ul.user_full_name, ut.access_token " +
                                        "FROM user_login_data ul, user_token ut " +
                                        "WHERE ul.user_id = ut.user_id " +
                                        "AND ul.user_id = " + userID;

            var client = new RestClient(SERVER_URL + FUNC_GETDATANOPARAM);
            var request = new RestRequest(Method.POST);

            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponseToken = JsonConvert.DeserializeObject<REST_userAccessData>(response.Content);

            if (null == jsonResponseToken)
                return false;

            userTokenData = (REST_userAccessData)jsonResponseToken;

            if (null != userTokenData &&
                userTokenData.dataStatus.o_status == 1)
                return true;
            else
                return false;
            #endregion
        }

        public bool saveUserLastLogin(int userID, string accessToken)
        {
            string errMsg = "";

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_LAST_LOGIN);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_loginType", LAST_LOGIN);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>) jsonResponse;

            if (replyResult[0].o_status == 1)
                return true;
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveUserLastLogout(int userID, string accessToken)
        {
            string errMsg = "";

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_LAST_LOGIN);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_loginType", LAST_LOGOUT);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
                return true;
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool getUserGroupID(int userID, string accessToken, ref genericReply replyResult)
        {
            string sqlComm = "SELECT IFNULL(group_id, 0) AS resultQuery " +
                                        "FROM user_login_data " +
                                        "WHERE user_id = " + userID;

            var client = new RestClient(SERVER_URL + FUNC_GETDATASINGLEVALUE);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlComm);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            replyResult = (genericReply)jsonResponse[0];

            if (replyResult.o_status == 1)
                return true;
            else
                return false;
        }

        public int userHasAccessTo(int userID, string accessToken, string sqlComm)
        {
            genericReply replyResult = new genericReply();

            var client = new RestClient(SERVER_URL + FUNC_GETDATASINGLEVALUE);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlComm);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                return 0;
            }

            replyResult = (genericReply)jsonResponse[0];

            if (replyResult.o_status == 1)
                return Convert.ToInt32(replyResult.data);
            else
                return 0;
        }

        public bool checkUserPassword(int userID, string accessToken, string userPassword)
        {
            genericReply replyResult = new genericReply();

            string sqlComm = "SELECT COUNT(1) AS resultQuery " +
                                        "FROM user_login_data " +
                                        "WHERE user_id = " + userID + " " +
                                        "AND user_password = '" + userPassword + "'";

            var client = new RestClient(SERVER_URL + FUNC_GETDATASINGLEVALUE);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlComm);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            replyResult = (genericReply)jsonResponse[0];

            if (replyResult.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getUserGroupAccess(int userID, string accessToken, int groupID, ref REST_groupUserAccess mmAccess)
        {
            var client = new RestClient(SERVER_URL + FUNC_GET_USERGROUPACCESS);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_group_user_id", groupID);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_groupUserAccess>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            mmAccess = (REST_groupUserAccess)jsonResponse;

            if (mmAccess.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getMasterGroupData(int userID, string accessToken, string sqlQuery, ref REST_groupUser groupUserData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_groupUser>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            groupUserData = (REST_groupUser)jsonResponse;

            if (groupUserData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getMasterSalesData(int userID, string accessToken, string sqlQuery, ref REST_sales salesData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_sales>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            salesData = (REST_sales)jsonResponse;

            if (salesData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getMasterCustomerData(int userID, string accessToken, string sqlQuery, ref REST_customer custData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_customer>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            custData = (REST_customer)jsonResponse;

            if (custData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getDepartmentData(int userID, string accessToken, string sqlQuery, ref REST_department deptData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_department>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            deptData = (REST_department)jsonResponse;

            if (deptData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getTaskData(int userID, string accessToken, string sqlQuery, ref REST_task taskData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_task>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            taskData = (REST_task)jsonResponse;

            if (taskData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getMasterModuleData(int userID, string accessToken, string sqlQuery, ref REST_masterModule mmData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_masterModule>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            mmData = (REST_masterModule)jsonResponse;

            if (mmData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getSysConfigData(int userID, string accessToken, string sqlQuery, ref REST_sysConfig sysConfigData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_sysConfig>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            sysConfigData = (REST_sysConfig)jsonResponse;

            if (sysConfigData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getSysConfigTransData(int userID, string accessToken, string sqlQuery, ref REST_sysConfigTrans sysConfigData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_sysConfigTrans>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            sysConfigData = (REST_sysConfigTrans)jsonResponse;

            if (sysConfigData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getSysConfigSchedule(int userID, string accessToken, string sqlQuery, ref REST_sysConfigSchedule sysConfigData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_sysConfigSchedule>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            sysConfigData = (REST_sysConfigSchedule)jsonResponse;

            if (sysConfigData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getUserLoginData(int userID, string accessToken, string sqlQuery, ref REST_userLoginData userLoginData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_userLoginData>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            userLoginData = (REST_userLoginData)jsonResponse;

            if (userLoginData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getUserGroupData(int userID, string accessToken, string sqlQuery, ref REST_userGroupData userGroupData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_userGroupData>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            userGroupData = (REST_userGroupData)jsonResponse;

            if (userGroupData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getUserDepartmentData(int userID, string accessToken, string sqlQuery, ref REST_userDepartmentData userDeptData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_userDepartmentData>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            userDeptData = (REST_userDepartmentData)jsonResponse;

            if (userDeptData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getMasterKompleksData(int userID, string accessToken, string sqlQuery, ref REST_masterKompleks kompleksData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_masterKompleks>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            kompleksData = (REST_masterKompleks)jsonResponse;

            if (kompleksData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getMasterKavlingData(int userID, string accessToken, string sqlQuery, ref REST_masterKavling kavlingData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_masterKavling>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            kavlingData = (REST_masterKavling)jsonResponse;

            if (kavlingData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getJobsList(int userID, string accessToken, string sqlQuery, ref REST_displayJobs jobsList)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_displayJobs>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            jobsList = (REST_displayJobs)jsonResponse;

            if (jobsList.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getMasterItemData(int userID, string accessToken, string sqlQuery, ref REST_masterItem itemData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_masterItem>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            itemData = (REST_masterItem)jsonResponse;

            if (itemData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getUserAccessListData(int userID, string accessToken, string sqlQuery, ref REST_userAccessList userData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_userAccessList>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            userData = (REST_userAccessList)jsonResponse;

            if (userData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public void changePassword(int userID, string accessToken, string newPass)
        {
            var client = new RestClient(SERVER_URL + FUNC_CHANGE_PASSWORD);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_new_password", newPass);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
        } 

        public bool saveGroupUser(int userID, string accessToken, List<master_group> groupUserData, int opType, 
            out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_GROUP_USER);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(groupUserData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_opType", opType);
            request.AddParameter("records", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveCustomer(int custID, string accessToken, List<master_customer> custData, int opType,
            out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_CUSTOMER);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(custData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", custID);
            request.AddParameter("p_opType", opType);
            request.AddParameter("records", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveSales(int custID, string accessToken, List<master_sales> salesData, int opType,
            out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_SALES);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(salesData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", custID);
            request.AddParameter("p_opType", opType);
            request.AddParameter("records", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveDepartment(int userID, string accessToken, List<master_department> departmentData, int opType,
            out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_DEPARTMENT);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(departmentData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_opType", opType);
            request.AddParameter("records", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveTask(int userID, string accessToken, List<master_task> taskData, int opType,
            out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_TASK);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(taskData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_opType", opType);
            request.AddParameter("records", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveHakAkses(int userID, string accessToken, int groupID, List<master_module> mmData,
                    out string errMsg)
        {
            errMsg = "";

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_MODULE_ACCESS);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(mmData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_group_id", groupID);
            request.AddParameter("records", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveUserGroup(int userID, string accessToken, int userAccessID, List<user_group> userGroupData,
                    out string errMsg)
        {
            errMsg = "";

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_USER_GROUP_DATA);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(userGroupData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_user_access_id", userAccessID);
            request.AddParameter("records", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveUserDepartment(int userID, string accessToken, int userAccessID, List<user_department> userDeptData,
                    out string errMsg)
        {
            errMsg = "";

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_USER_DEPT_DATA);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(userDeptData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_user_access_id", userAccessID);
            request.AddParameter("records", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveUserLoginData(int userID, string accessToken, List<user_login_data> userData, List<user_token> userToken, 
            int opType, out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_USER_LOGIN_DATA);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(userData);
            var jsonToken = JsonConvert.SerializeObject(userToken);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_opType", opType);

            request.AddParameter("records", json);
            request.AddParameter("records_token", jsonToken);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveSysConfigData(int userID, string accessToken, List<sys_config> sysConfig, List<sys_config_trans> sysConfigTrans, List<sys_config_schedule> sysConfigSchedule,
            out string errMsg)
        {
            errMsg = "";

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_SYS_CONFIG);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(sysConfig);
            var jsonTrans = JsonConvert.SerializeObject(sysConfigTrans);
            var jsonSchedule = JsonConvert.SerializeObject(sysConfigSchedule);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);

            request.AddParameter("sysConfigParam", json);
            request.AddParameter("sysConfigTransParam", jsonTrans);
            request.AddParameter("sysConfigScheduleParam", jsonSchedule);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveDataUserAccessList(int userID, string accessToken, List<user_access_list> listHeader,
          out string errMsg)
        {
            errMsg = "";

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_USER_ACCESS_LIST);
            var request = new RestRequest(Method.POST);
            var jsonHead = JsonConvert.SerializeObject(listHeader);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("records", jsonHead);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveDataJobs(int userID, string accessToken, 
            List<jobs_header> headerData, List<jobs_detail> detailData,
            int opType, out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_JOBS);
            var request = new RestRequest(Method.POST);
            var jsonHead = JsonConvert.SerializeObject(headerData);
            var jsonDet = JsonConvert.SerializeObject(detailData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_opType", opType);

            request.AddParameter("headerTransParam", jsonHead);
            request.AddParameter("detailTransParam", jsonDet);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveDataJobsDetail(int userID, string accessToken,
            List<jobs_detail> detailData,
            int jobID, out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_JOBS_DETAIL);
            var request = new RestRequest(Method.POST);
            var jsonDet = JsonConvert.SerializeObject(detailData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_jobID", jobID);
            request.AddParameter("detailTransParam", jsonDet);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool checkJobFinished(int userID, string accessToken,
            int jobID, out string errMsg)
        {
            errMsg = "";

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_CHECK_JOB_FINISH);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_jobID", jobID);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveDataJobsNotes(int userID, string accessToken,
            int jobDetailID, string notesValue,
            out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_JOBS_NOTES);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_jobDetailID", jobDetailID);
            request.AddParameter("p_notesValue", notesValue);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool getJobsHeader(int userID, string accessToken, string sqlQuery, ref REST_jobHeader jobsList)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_jobHeader>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            jobsList = (REST_jobHeader)jsonResponse;

            if (jobsList.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getJobsDetail(int userID, string accessToken, string sqlQuery, ref REST_jobDetail jobsList)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_jobDetail>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            jobsList = (REST_jobDetail)jsonResponse;

            if (jobsList.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getJobsNotes(int userID, string accessToken, string sqlQuery, ref REST_jobNotes jobsList)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_jobNotes>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            jobsList = (REST_jobNotes)jsonResponse;

            if (jobsList.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getChildTaskList(int userID, string accessToken, string sqlQuery, ref REST_childTaskList jobsList)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_childTaskList>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            jobsList = (REST_childTaskList)jsonResponse;

            if (jobsList.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getMasterBlokData(int userID, string accessToken, string sqlQuery, ref REST_masterBlok blokData)
        {
            var client = new RestClient(SERVER_URL + FUNC_GETDATA);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlQuery);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<REST_masterBlok>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            blokData = (REST_masterBlok)jsonResponse;

            if (blokData.dataStatus.o_status == 1)
                return true;
            else
                return false;
        }

        public bool saveMasterKompleks(int userID, string accessToken, List<master_kompleks> kompleksData, List<master_blok> blokData,
            int opType, out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_MASTER_KOMPLEKS);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(kompleksData);
            var jsonBlok = JsonConvert.SerializeObject(blokData);

            //string json = JsonConvert.SerializeObject(kompleksData);
            //string jsonBlok = JsonConvert.SerializeObject(blokData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_opType", opType);

            request.AddParameter("kompleksParam", json);
            request.AddParameter("blokParam", jsonBlok);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            //var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool saveMasterKavling(int userID, string accessToken, List<master_kavling> kavlingData, int opType,
            out string errMsg, out int newID)
        {
            errMsg = "";
            newID = 0;

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_MASTER_KAVLING);
            var request = new RestRequest(Method.POST);
            var json = JsonConvert.SerializeObject(kavlingData);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_opType", opType);
            request.AddParameter("records", json);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                newID = Convert.ToInt32(replyResult[0].data);
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

        public bool getAutoLoginData(int userID, string accessToken, ref genericReply replyResult)
        {
            string sqlComm = "SELECT auto_login AS resultQuery " +
                                        "FROM sys_config " +
                                        "WHERE id = 1";

            var client = new RestClient(SERVER_URL + FUNC_GETDATASINGLEVALUE);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("sqlQuery", sqlComm);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            replyResult = (genericReply)jsonResponse[0];

            if (replyResult.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getUserAutoLoginDataNoParam(ref genericReply replyResult)
        {
            string sqlComm = "SELECT auto_login_user_id AS resultQuery " +
                                        "FROM sys_config " +
                                        "WHERE id = 1";

            var client = new RestClient(SERVER_URL + FUNC_GETDATASINGLEVALUENOPARAM);
            var request = new RestRequest(Method.POST);

            request.AddParameter("sqlQuery", sqlComm);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            replyResult = (genericReply)jsonResponse[0];

            if (replyResult.o_status == 1)
                return true;
            else
                return false;
        }

        public bool getAutoLoginDataNoParam(ref genericReply replyResult)
        {
            string sqlComm = "SELECT auto_login AS resultQuery " +
                                        "FROM sys_config " +
                                        "WHERE id = 1";

            var client = new RestClient(SERVER_URL + FUNC_GETDATASINGLEVALUENOPARAM);
            var request = new RestRequest(Method.POST);

            request.AddParameter("sqlQuery", sqlComm);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                return false;
            }

            replyResult = (genericReply)jsonResponse[0];

            if (replyResult.o_status == 1)
                return true;
            else
                return false;
        }

        public bool saveAutoLogin(int userID, string accessToken, string autoLoginValue,
            out string errMsg)
        {
            errMsg = "";

            List<genericReply> replyResult = new List<genericReply>();

            replyResult.Clear();

            var client = new RestClient(SERVER_URL + FUNC_SAVE_AUTOLOGIN);
            var request = new RestRequest(Method.POST);

            request.AddParameter("p_access_token", accessToken);
            request.AddParameter("p_user_id", userID);
            request.AddParameter("p_autoLoginValue", autoLoginValue);

            IRestResponse response = client.Execute(request);

            var restResult = response.Content;
            var jsonResponse = JsonConvert.DeserializeObject<List<genericReply>>(response.Content);

            if (null == jsonResponse)
            {
                errMsg = "Error connection";
                return false;
            }

            replyResult = (List<genericReply>)jsonResponse;

            if (replyResult[0].o_status == 1)
            {
                return true;
            }
            else
            {
                errMsg = replyResult[0].o_message;
                return false;
            }
        }

    }
}
