﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlphaSoft
{
    public class globalFTPLib
    {
        public const int IMG_PRODUCTS = 0;
        public const int IMG_CS = 1;
        public const int IMG_NEWS = 2;
        public const int IMG_KOMPLEKS = 3;

        public const string ftp_server = "ftp.menarasantosa.com";
        public const string ftp_username = "alphasoft@menarasantosa.com";
        public const string ftp_password = "ulGk9K8zmAWO";

        public const string ftp_img_products = "ftp://" + ftp_server + "/apps/public/sys_contents/img_products/";
        public const string ftp_img_cs = "ftp://" + ftp_server + "/apps/public/sys_contents/img_cs/";
        public const string ftp_img_news = "ftp://" + ftp_server + "/apps/public/sys_contents/img_news/";
        public const string ftp_img_kompleks = "ftp://" + ftp_server + "/apps/public/sys_contents/img_kompleks/";

        public const string server_img_products = "apps.menarasantosa.com/public/sys_contents/img_products/";
        public const string server_img_cs = "apps.menarasantosa.com/public/sys_contents/img_cs/";
        public const string server_img_news = "apps.menarasantosa.com/public/sys_contents/img_news/";
        public const string server_img_kompleks = "apps.menarasantosa.com/public/sys_contents/img_kompleks/";

        public const string ftp_doc_staging = "ftp://" + ftp_server + "/staging/public/sys_contents/staging/";
        public const string server_doc_staging = "staging.menarasantosa.com/public/sys_contents/staging/";

        public string localDirectory = Application.StartupPath + "\\ftpFile\\";

        public const int def_img_height = 500;
        public const int def_img_width = 500;

        public string FTP_FILE_KEYWORD = "PSS";

        public const int FILE_FOUND = 1;
        public const int FILE_NOT_FOUND = 0;
        public const int ACCESS_FAILED = 2;

        public bool deleteFromFTP(string dirName, string fileName)
        {
            bool result = false;

            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftp_doc_staging + dirName + "/" + fileName);// "ftp://" + ftp_server + "/" + dirName + "/" + fileName);

                request.Credentials = new NetworkCredential(ftp_username, ftp_password);
                request.Method = WebRequestMethods.Ftp.DeleteFile;
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                response.Close();

                if (response.StatusDescription.ToUpper().IndexOf("DELETED") >= 0)
                    result = true;
                else
                    result = false;
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        public bool listFileFTP(List<string> dirList)
        {
            List<string> tempList = new List<string>();
            try
            {
                dirList.Clear();

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://" + ftp_server + "/");
                request.Method = WebRequestMethods.Ftp.ListDirectory;

                request.Credentials = new NetworkCredential(ftp_username, ftp_password);
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                string names = reader.ReadToEnd();

                reader.Close();
                response.Close();

                tempList = names.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();

                for (int i = 0;i<tempList.Count;i++)
                {
                    if (tempList[i].Contains(FTP_FILE_KEYWORD))
                        dirList.Add(tempList[i]);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool uploadToFTP(string dirName, string fileName, string filePath)
        {
            bool result = false;
            string From = filePath;
            string To = "";
            byte[] retVal;

            To = ftp_doc_staging + dirName + "/" + fileName;// "ftp://" + ftp_server + "/" + dirName + "/" + fileName;

            try
            {
                using (WebClient client = new WebClient())
                {
                    client.Credentials = new NetworkCredential(ftp_username, ftp_password);
                    retVal = client.UploadFile(To, WebRequestMethods.Ftp.UploadFile, From);
                    //client.UploadFileTaskAsync(To, WebRequestMethods.Ftp.UploadFile, From);
                }

                result = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return result;
        }

        public bool createFTPDirectory(string dirName)
        {
            bool result = false;

            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftp_doc_staging + dirName);

                request.Credentials = new NetworkCredential(ftp_username, ftp_password);
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                response.Close();

                if (response.StatusDescription.ToUpper().IndexOf("CREATED") >= 0)
                    result = true;
                else
                    result = false;
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        public bool checkFTPDirectoryExist(string dirName)
        {
            bool result = false;

            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftp_doc_staging + dirName);//"ftp://" + ftp_server + "/" + dirName);

                request.Credentials = new NetworkCredential(ftp_username, ftp_password);
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                response.Close();

                if (response.StatusDescription.ToUpper().IndexOf("MATCH") >= 0)
                    result = true;
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public int checkFTPFileExist(string dirName, string fileName, out string errMsg)
        {
            int result = ACCESS_FAILED;
            string ftpDir = ftp_doc_staging + dirName + "/"; //"ftp://" + ftp_server + "/" + dirName + "/";
            List<string> tempList = new List<string>();
            errMsg = "";

            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpDir);
                request.Method = WebRequestMethods.Ftp.ListDirectory;

                request.Credentials = new NetworkCredential(ftp_username, ftp_password);
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                string names = reader.ReadToEnd();

                reader.Close();
                response.Close();

                tempList = names.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();

                if (tempList.Contains(fileName))
                {
                    result = FILE_FOUND;
                }   
                else
                {
                    result = FILE_NOT_FOUND;
                    errMsg = "File tidak ditemukan";
                }                
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
            }

            return result;
        }

        public bool downloadFromFTP(string dirName, string fileName, string destDir)
        {
            bool result = false;

            string localDir = destDir;// @localDirectory + fileName;
            string ftpDir = "ftp://" + ftp_server + "/" + dirName + "/" + fileName;

            try
            {
                using (WebClient client = new WebClient())
                {
                    client.Credentials = new NetworkCredential(ftp_username, ftp_password);
                    client.DownloadFile(ftpDir, localDir);
                }

                result = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return result;
        }

        public bool previewFromFTP(string dirName, string fileName, out string fullFilePath)
        {
            bool result = false;

            string localDir = @localDirectory + fileName;
            string ftpDir = ftp_doc_staging + dirName + "/" + fileName;//"ftp://" + ftp_server + "/" + dirName + "/" + fileName;
            fullFilePath = "";

            try
            {
                if (!Directory.Exists(localDirectory))
                {
                    Directory.CreateDirectory(localDirectory);
                }

                using (WebClient client = new WebClient())
                {
                    client.Credentials = new NetworkCredential(ftp_username, ftp_password);
                    client.DownloadFile(ftpDir, localDir);
                }

                fullFilePath = localDir;
                result = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return result;
        }

        public void openFileUsingDefaultAssoc(string path)
        {
            Process fileopener = new Process();

            fileopener.StartInfo.FileName = "explorer";
            fileopener.StartInfo.Arguments = "\"" + path + "\"";
            fileopener.Start();
        }

        public void clearTempFolder()
        {
            string localDir = @localDirectory;

            if (Directory.Exists(localDir))
            {
                Directory.Delete(localDir, true);
            }
            
            Directory.CreateDirectory(localDir);
        }
    }
}
