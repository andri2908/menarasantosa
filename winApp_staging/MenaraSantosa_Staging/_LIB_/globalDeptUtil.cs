﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaSoft
{
    class globalDeptUtil
    {
        private globalRestAPI gRest = new globalRestAPI();
        private globalUserUtil gUser = new globalUserUtil();
        REST_department deptData;

        public string getDepartmentName(int departmentID)
        {
            string deptName = "";
            string sqlCommand = "";

            sqlCommand = "SELECT * " +
                                    "FROM master_department " +
                                    "WHERE department_id = " + departmentID;

            if (gRest.getDepartmentData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref deptData))
            {
                deptName = deptData.dataList[0].department_name;
            }

            return deptName;
        }

        public int getNumOfDept()
        {
            int numOfDept = 0;
            string sqlCommand = "";

            sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                        "FROM user_department WHERE user_id = " + gUser.getUserID() + " " +
                                        "AND is_active = 'Y'";

            genericReply replyResult = new genericReply();
            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
            {
                if (replyResult.o_status == 1)
                {
                    numOfDept = Convert.ToInt32(replyResult.data);
                }
            }

            return numOfDept;
        }

        public int getDeptID()
        {
            int numOfDept = 0;
            string sqlCommand = "";

            sqlCommand = "SELECT department_id AS resultQuery " +
                                        "FROM user_department WHERE user_id = " + gUser.getUserID() + " " +
                                        "AND is_active = 'Y'";

            genericReply replyResult = new genericReply();
            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
            {
                if (replyResult.o_status == 1)
                {
                    numOfDept = Convert.ToInt32(replyResult.data);
                }
            }

            return numOfDept;
        }

    }
}
