﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer;
using System.Configuration;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;
using MySql.Data.MySqlClient;

namespace AlphaSoft
{
    class globalDBUtil : globalUtilities
    {
        private Data_Access DS;

        public globalDBUtil(Data_Access DSParam)
        {
            DS = DSParam;
        }

        public string getAutoGenerateID(string tableName, string prefix, string valueDelimiter, string fieldName, 
            string whereCondition = "", int minLength = 0)
        {
            string newID = "";
            DateTime localDate = DateTime.Now;
            string maxID = "0";
            double maxIDValue = 0;
            string idPrefix;
            string sqlCommand = "";

            idPrefix = prefix + valueDelimiter;

            if (idPrefix.Length > 0)
                sqlCommand = "SELECT IFNULL(MAX(CONVERT(SUBSTRING(" + fieldName + ", INSTR(" + fieldName + ", '" + idPrefix + "')+" + idPrefix.Length + "), UNSIGNED INTEGER)),'0') AS " + fieldName + " FROM " + tableName + " WHERE " + fieldName + " LIKE '" + idPrefix + "%'";
            else
                sqlCommand = "SELECT IFNULL(MAX(" + fieldName + ") , 0) AS " + fieldName + " FROM " + tableName + " WHERE 1 = 1 ";

            sqlCommand = sqlCommand + " " + whereCondition;

            try
            {
                maxID = DS.getDataSingleValue(sqlCommand).ToString();
                maxIDValue = Convert.ToInt32(maxID);
            }
            catch (Exception ex)
            {
                maxID = "0";
            }
           
            if (maxIDValue > 0)
            {
                maxIDValue += 1;
                maxID = maxIDValue.ToString();
            }
            else
            {
                maxID = "1";
            }

            if (minLength > 0)
            {
                maxID = maxID.PadLeft(minLength, '0');
            }

            newID = prefix + valueDelimiter + maxID;

            return newID;
        }

        public bool autobackupEnabled()
        {
            bool result = false;
            string sqlCommand = "SELECT IFNULL(AUTO_BACKUP_FLAG, 0) FROM SYS_CONFIG WHERE ID = 2";

            if (Convert.ToInt32(DS.getDataSingleValue(sqlCommand)) > 0)
                result = true;

            return result;
        }

        public bool restoreDatabase(string fileName, out string errMessage)
        {
            bool result = true;

            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            string ipServer;

            try
            {
                killAllConnections();

                ipServer = DS.getIPServer();

                proc.StartInfo.FileName = "CMD.exe";
                proc.StartInfo.Arguments = "/C " + "mysql -h " + ipServer + " -u" + Data_Access.userName + " -p" + Data_Access.password + " " + Data_Access.databaseName + " < \"" + fileName + "\"";
                proc.EnableRaisingEvents = true;
                proc.Start();
                errMessage = "";
            }
            catch (Exception ex)
            {
                result = false;
                errMessage = ex.Message;
            }

            return result;
        }

        public bool backupDatabase(string fileName, out string errMessage)
        {
            bool result = true;

            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            string ipServer;

            try
            {
                ipServer = DS.getIPServer();
                proc.StartInfo.FileName = "CMD.exe";
                proc.StartInfo.Arguments = "/C " + "mysqldump -h " + ipServer + " -u" + Data_Access.userName + " -p" + Data_Access.password + " " + Data_Access.databaseName + " > \"" + fileName + "\"";
                proc.EnableRaisingEvents = true;
                proc.Start();

                errMessage = "";
            }
            catch (Exception ex)
            {
                result = false;
                errMessage = ex.Message;
            }

            return result;
        }

        public int getLocationID()
        {
            int result = 0;
            string sqlCommand = "SELECT IFNULL(LOCATION_ID, 0) FROM SYS_CONFIG WHERE ID = 2";

            try
            {
                result = getLocalLocationID();

                if (result <= 0)
                    result = Convert.ToInt32(DS.getDataSingleValue(sqlCommand));
            }
            catch (Exception ex) { }

            return result;
        }

        public DataRow convertDGViewRowToDRow(DataGridViewRow selectedRow)
        {
            DataTable dt = new DataTable();
            DataRow myRow = null;
            int i = 0;
            for (i = 0; i < selectedRow.Cells.Count; i++)
            {
                dt.Columns.Add(selectedRow.Cells[i].OwningColumn.Name);
            }
            myRow = dt.NewRow();

            for (i = 0; i < selectedRow.Cells.Count; i++)
            {
                myRow[i] = selectedRow.Cells[i].Value;
            }

            return myRow;
        }

        public bool getPrintPreview()
        {
            bool printPreviewResult = false;

            //if (Convert.ToDouble(DS.getDataSingleValue("SELECT PRINT_PREVIEW FROM SYS_CONFIG WHERE ID = 2")) == 1)
            //    printPreviewResult = true;

            return printPreviewResult;
        }

        public void addParamList(List<string> paramList, string[] paramNames, string sufix = "")
        {
            for (int i = 0; i < paramNames.Count(); i++)
            {
                paramList.Add(paramNames[i] + sufix);
            }
        }

        public string constructMasterQuery(string tableName, string[] fieldNames, 
            List<string> paramList, int mode = 0, int numWhereIdx = 0) // 0 = insert, 1 = update, 2 = delete
        {
            int i = 0;
            string result = "";
            string fieldSQL = "";
            string valuesSQL = "";
            string whereSQL = "";

            switch (mode)
            {
                case 0: // INSERT 
                        for (i = 0; i < (fieldNames.Count() - numWhereIdx); i++)
                        {
                            fieldSQL = fieldSQL + fieldNames[i];
                            valuesSQL = valuesSQL + "@" + paramList[i];

                            if (i < fieldNames.Count() - 1)
                            {
                                fieldSQL += ", ";
                                valuesSQL += ", ";
                            }
                        }

                        result = "INSERT INTO " + tableName + " (" + fieldSQL + ") VALUES (" + valuesSQL + ")";
                    break;

                case 1: // UPDATE
                        for (i = 0; i < (fieldNames.Count() - numWhereIdx); i++)
                        {
                            fieldSQL = fieldSQL + fieldNames[i] + " = @" + paramList[i];

                            if (i < fieldNames.Count() - (numWhereIdx + 1))
                            {
                                fieldSQL += ", ";
                            }
                        }

                        while (i < fieldNames.Count())
                        {
                            whereSQL += fieldNames[i] + " = @" + paramList[i];

                            if (i < fieldNames.Count() - 1)
                                whereSQL += " AND ";

                            i++;
                        }

                        result = "UPDATE " + tableName + " SET " + fieldSQL + " WHERE " + whereSQL;
                    break;

                case 2: // DELETE
                    for (i = (fieldNames.Count() - numWhereIdx); i < fieldNames.Count(); i++)
                    {
                        whereSQL += fieldNames[i] + " = @" + paramList[i];

                        if (i < fieldNames.Count() - 1)
                            whereSQL += " AND ";

                        i++;
                    }

                        result = "DELETE FROM " + tableName + " WHERE " + whereSQL;
                    break;
            }

            return result;
        }

        public void killAllConnections()
        {
            MySqlDataReader rdr;
            MySqlException intEx = null;
            DataTable dt = new DataTable();
            string sqlCommand = "";

            sqlCommand = "SHOW PROCESSLIST";

            using (rdr = DS.getData(sqlCommand))
            {
                if (rdr.HasRows)
                    dt.Load(rdr);
            }
            rdr.Close();

            DS.beginTransaction();

            try
            {
                foreach (DataRow r in dt.Rows)
                {
                    if (r["db"].ToString().ToUpper() == Data_Access.databaseName)
                    {
                        if (r["Command"].ToString().ToUpper() == "SLEEP")
                        {
                            sqlCommand = "kill " + r["id"].ToString();

                            if (!DS.executeNonQueryCommand(sqlCommand, ref intEx))
                                throw intEx;
                        }
                    }
                }

                DS.commit();
            }
            catch (Exception ex) { }
        }

        public bool dataExist(string tableName, string fieldName, string fieldValue)
        {
            bool result = false;
            string sqlCommand = "SELECT COUNT(1) FROM " + tableName + " WHERE " + fieldName + " = '" + fieldValue + "'";

            if (DS.getDataSingleValue(sqlCommand).ToString() != "0")
                result = true;

            return result;
        }

        public bool dataExist(string tableName, string whereValue = "")
        {
            bool result = false;
            string sqlCommand = "SELECT COUNT(1) FROM " + tableName + " WHERE 1 = 1 " + whereValue;

            if (DS.getDataSingleValue(sqlCommand).ToString() != "0")
                result = true;

            return result;
        }
    }
}
