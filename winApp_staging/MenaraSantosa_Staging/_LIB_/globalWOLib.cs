﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using System.Data;

namespace AlphaSoft
{
    class globalWOLib
    {
        private Data_Access DS;
        private globalProdukLib gProd;

        public globalWOLib(Data_Access DSParam = null)
        {
            if (null == DSParam)
                DS = new Data_Access();
            else
                DS = DSParam;

            gProd = new globalProdukLib(DS);
        }

        public bool qtyWOEnough(string woIDParam, string produksiIDParam, int faseParam,
            string productID, double qtyReq, double oldQty)
        {
            return true;
            //if (faseParam != globalConstants.FASE_REPACKING)
            //{
            //    return qtyWOInputEnough(woIDParam, produksiIDParam, faseParam, productID, qtyReq, oldQty);
            //}
            //else
            //{
            //    return qtyWORepackingEnough(woIDParam, produksiIDParam, faseParam, productID, qtyReq, oldQty);
            //}
        }

        public bool qtyPenerimaanWOValid(string prWOParam, string woIDParam, string produksiIDParam, int faseParam,
            string productID, int warnaID, double qtyReq)
        {
            bool result = true;
            string sqlWO = "";
            string sqlPenerimaanWO = "";

            double qtyWO = 0;
            double qtyPRWO = 0;

            return result;
        }

  
        public void checkTotalPenerimaanWO(string produksiIDParam, int faseParam)
        {
            string sqlCommand = "";
            string sqlProduct = "";
            string sqlWO = "";
            string sqlPenerimaan = "";
            MySqlDataReader rdr;
            DataTable dt = new DataTable();
            MySqlException inEx = null;

            sqlProduct = "SELECT PRODUCT_ID FROM MASTER_PRODUCT WHERE PRODUCT_ACTIVE = 1";

            sqlWO = "SELECT WH.CONTACT_ID, WD.WARNA_ID, SUM(WD.PRODUCT_QTY) AS QTY_WO " +
                                    "FROM WO_HEADER WH, WO_DETAIL WD " +
                                    "WHERE WH.WO_ACTIVE = 1 " +
                                    "AND WD.WO_ID = WH.WO_ID " +
                                    "AND WD.PRODUCT_ID NOT IN (" + sqlProduct + ") " +
                                    "AND WH.PRODUKSI_ID = '" + produksiIDParam + "' " +
                                    "AND WH.FASE_AKTIF = " + faseParam + " " +
                                    "GROUP BY WH.CONTACT_ID, WD.WARNA_ID"; 

            sqlPenerimaan = "SELECT WOH.CONTACT_ID, WD.WARNA_ID, SUM(WD.PRODUCT_QTY) AS QTY_PR_WO " +
                                    "FROM PENERIMAAN_WO_HEADER WH, PENERIMAAN_WO_DETAIL WD, " +
                                    "WO_HEADER WOH " +
                                    "WHERE WH.PENERIMAAN_ACTIVE = 1 " +
                                    "AND WD.PENERIMAAN_ID = WH.PENERIMAAN_ID " +
                                    "AND WD.PRODUCT_ID NOT IN (" + sqlProduct + ") " +
                                    "AND WH.PRODUKSI_ID = '" + produksiIDParam + "' " +
                                    "AND WH.FASE_AKTIF = " + faseParam + " " +
                                    "AND WH.WO_ID = WOH.WO_ID " +
                                    "AND WOH.WO_ACTIVE = 1 " +
                                    "GROUP BY WOH.CONTACT_ID, WD.WARNA_ID";

            sqlCommand = "SELECT WO.CONTACT_ID, WO.WARNA_ID, WO.QTY_WO, IFNULL(PR.QTY_PR_WO, 0) AS QTY_PR " +
                                    "FROM (" + sqlWO + ") WO " +
                                    "LEFT OUTER JOIN (" + sqlPenerimaan + ") PR ON (WO.CONTACT_ID = PR.CONTACT_ID AND WO.WARNA_ID = PR.WARNA_ID) ";

            using (rdr = DS.getData(sqlCommand))
            {
                if (rdr.HasRows)
                    dt.Load(rdr);
            }
            rdr.Close();

            int contactID = 0;
            int warnaID = 0;
            double qtyWO = 0;
            double qtyPR = 0;
            string prID = "";
            double lossQty = 0;

            foreach(DataRow r in dt.Rows)
            {
                contactID = Convert.ToInt32(r["CONTACT_ID"]);
                warnaID = Convert.ToInt32(r["WARNA_ID"]);
                qtyWO = Convert.ToDouble(r["QTY_WO"]);
                qtyPR = Convert.ToDouble(r["QTY_PR"]);

                if (qtyPR < qtyWO)
                {
                    lossQty = Math.Round(qtyWO - qtyPR, 2);

                    sqlPenerimaan = "SELECT MAX(PENERIMAAN_DATETIME) FROM PENERIMAAN_WO_HEADER PH, " +
                                                "WO_HEADER WH " +
                                                "WHERE WH.PRODUKSI_ID = '" + produksiIDParam + "' " +
                                                "AND WH.FASE_AKTIF = " + faseParam + " " +
                                                "AND WH.CONTACT_ID = " + contactID + " " +
                                                "AND PH.WO_ID = WH.WO_ID";

                    sqlCommand = "SELECT PH.PENERIMAAN_ID " +
                                            "FROM PENERIMAAN_WO_HEADER PH, " +
                                            "WO_HEADER WH " +
                                            "WHERE WH.PRODUKSI_ID = '" + produksiIDParam + "' " +
                                            "AND WH.FASE_AKTIF = " + faseParam + " " +
                                            "AND WH.CONTACT_ID =  " + contactID + " " +
                                            "AND PH.WO_ID = WH.WO_ID " +
                                            "AND PH.PENERIMAAN_DATETIME = (" + sqlPenerimaan +")";
                    prID = DS.getDataSingleValue(sqlCommand).ToString();

                    sqlCommand = "UPDATE PENERIMAAN_WO_HEADER SET IS_LOSS_PR = 1 WHERE PENERIMAAN_ID = '" + prID + "'";
                    if (!DS.executeNonQueryCommand(sqlCommand, ref inEx))
                        throw inEx;

                    sqlCommand = "UPDATE PENERIMAAN_WO_DETAIL " +
                                            "SET LOSS_QTY = " + lossQty + " " +
                                            "WHERE PENERIMAAN_ID = '" + prID + "' " +
                                            "AND WARNA_ID = " + warnaID + " " +
                                            "AND PRODUCT_ID NOT IN (" + sqlProduct + ")";
                    if (!DS.executeNonQueryCommand(sqlCommand, ref inEx))
                        throw inEx;
                }
            }
        }
    }
}
