﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace AlphaSoft
{
    class globalFULib
    {
        private Data_Access DS;
        private globalDBUtil gDB;

        public string[] folUpHeader =
        {
              "CONTACT_ID",
              "MARKETING_ID",
              "REFERAL_ID",
              "FOLLOWUP_START_DATE",
              "REMARK",
              "FOLLOWUP_STATUS",
              "FINAL_STATUS_ID",
              "FOLLOWUP_ID",
        };

        public string[] folUpDetail =
        {
            "FOLLOWUP_ID",
            "ACTION_ID",
            "RESULT_ID",
            "REMARK",
            "RESULT",
            "ACTION_DATETIME",
            "ACTION_REMINDER",
            "ACTION_STATUS",
            "USER_ID",
            "ID"
        };

        public globalFULib(Data_Access DSParam = null)
        {
            if (null != DSParam)
                DS = DSParam;
            else
                DS = new Data_Access();

            gDB = new globalDBUtil(DS);
        }

        public void createFUCard(int customerID, int marketingID, MySqlException inEx)
        {
            string sqlCommand = "";

            List<string> pList = new List<string>();
            List<object> pVal = new List<object>();

            pList.Clear();
            pVal.Clear();

            gDB.addParamList(pList, folUpHeader, "_FU_");

            pVal.Add(customerID);
            pVal.Add(marketingID);
            pVal.Add(0);
            pVal.Add(DateTime.Now);
            pVal.Add("");
            pVal.Add(globalConstants.FU_ONGOING);
            pVal.Add(globalConstants.STATUS_AKTIF);
            pVal.Add(0);

            sqlCommand = gDB.constructMasterQuery("FOLLOWUP_HEADER", folUpHeader, pList);

            if (!DS.executeNonQueryCommandWithParameters(sqlCommand, ref inEx, pList, pVal))
                throw inEx;
        }
    }
}
