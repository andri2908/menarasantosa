﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.Globalization;
using System.IO;
using System.Data.SqlClient;

namespace AlphaSoft
{
    class globalUserUtil : globalUtilities
    {
        Data_Access DS;
        private static int userID = 0;
        private static int userGroupID = 0;

        private static string RS_userName = "";
        private static string userToken = "";

        private globalRestAPI gs = new globalRestAPI();

        private CultureInfo culture = new CultureInfo("id-ID");

        public globalUserUtil(Data_Access DSParam)
        {
        }

        public globalUserUtil()
        {

        }

        private REST_userAccessData userLoginData = new REST_userAccessData();

        public int getUserID()
        {
            return userID;
        }

        public string getUserToken()
        {
            return userToken;
        }

        public string getUserName()
        {
            return RS_userName;
        }

        public int getUserGroupID()
        {
            return userGroupID;
        }

        #region LARAVEL

        public bool RS_loadInfoSysConfig(out string namaComp, out string alamatComp, out string teleponComp, out string emailComp)
        {
            DataTable dt = new DataTable();
            REST_sysConfig restConfig = new REST_sysConfig();

            string sqlComm = "SELECT IFNULL(company_name,'') AS 'company_name', " +
                                        "IFNULL(company_address,'') AS 'company_address', " +
                                        "IFNULL(company_phone,'') AS 'company_phone', " +
                                        "IFNULL(company_email,'') AS 'company_email' " +
                                        "FROM sys_config LIMIT 1";

            namaComp = "";
            alamatComp = "";
            teleponComp = "";
            emailComp = "";
            bool rslt = false;

            if (gs.getSysConfigData(userID, userToken, sqlComm, ref restConfig))
            {
                if (restConfig.dataStatus.o_status == 1)
                {
                    rslt = true;
                    namaComp = restConfig.dataList[0].company_name;
                    alamatComp = restConfig.dataList[0].company_address;
                    teleponComp = restConfig.dataList[0].company_phone;
                    emailComp = restConfig.dataList[0].company_email;
                }
            }
            return rslt;
        }

        public int RS_getUserGroupType(int paramUserID)
        {
            string sqlCommand = "";
            int groupType = 0;

            genericReply replyResult = new genericReply();

            sqlCommand = "SELECT mg.group_type AS resultQuery " +
                                    "FROM master_group mg, user_login_data ul " +
                                    "WHERE ul.group_id = mg.group_id " +
                                    "AND ul.user_id = " + paramUserID;

            if (gs.getDataSingleValue(userID, userToken, sqlCommand, ref replyResult))
            {
                if (replyResult.o_status == 1)
                {
                    groupType = Convert.ToInt32(replyResult.data);
                }
            }

            return groupType;
        }

        public int RS_getUserGroupTypeByGroup(int groupID)
        {
            string sqlCommand = "";
            int groupType = 0;

            genericReply replyResult = new genericReply();

            sqlCommand = "SELECT mg.group_type AS resultQuery " +
                                    "FROM master_group mg " +
                                    "WHERE mg.group_id = " + groupID;

            if (gs.getDataSingleValue(userID, userToken, sqlCommand, ref replyResult))
            {
                if (replyResult.o_status == 1)
                {
                    groupType = Convert.ToInt32(replyResult.data);
                }
            }

            return groupType;
        }

        public bool RS_validateUser(string passwordParam)
        {
            string passwordMD5 = getMD5Value(passwordParam);

            return gs.checkUserPassword(userID, userToken, passwordMD5);
        }

        public bool RS_doUserLogin(string userName, string userPassword)
        {
            bool result = false;
            string passwordMD5 = getMD5Value(userPassword);

            if (gs.getUserTokenData(userName, passwordMD5, ref userLoginData))
            {
                result = true;
                if (userLoginData.dataStatus.o_status == 1)
                {
                    userID = userLoginData.dataList[0].user_id;
                    RS_userName = userLoginData.dataList[0].user_name;
                    userToken = userLoginData.dataList[0].access_token;
                }
            }

            return result;
        }

        public bool RS_doUserAutoLogin(int userIDParam)
        {
            bool result = false;
            if (gs.getUserTokenDataByUserID(userIDParam, ref userLoginData))
            {
                result = true;
                if (userLoginData.dataStatus.o_status == 1)
                {
                    userID = userIDParam;// userLoginData.dataList[0].user_id;
                    RS_userName = userLoginData.dataList[0].user_name;
                    userToken = userLoginData.dataList[0].access_token;
                }
            }

            return result;
        }

        public bool RS_saveUserLastLogin()
        {
            return gs.saveUserLastLogin(userID, userToken);
        }

        public bool RS_saveUserLastLogout()
        {
            return gs.saveUserLastLogout(userID, userToken);
        }

        public void RS_setUserGroupID()
        {
            genericReply replyResult = new genericReply();

            gs.getUserGroupID(userID, userToken, ref replyResult);
            userGroupID = Convert.ToInt32(replyResult.data);
        }

        public void RS_getGroupUserAccess(ref REST_groupUserAccess mmAccess)
        {
            gs.getUserGroupAccess(userID, userToken, userGroupID, ref mmAccess);
        }

        public void RS_changePassword(string newPass)
        {
            gs.changePassword(userID, userToken, newPass);
        }

        public bool RS_userHasAccessTo(int moduleID, int accessType = 1, int userIDParam = 0)//int groupIDParam = 0)
        {
            bool result = false;
            int accessValue = 0;
            string sqlCommand = "";

            sqlCommand = "SELECT MAX(IFNULL(user_access_option, 0)) AS resultQuery " +
                                   "FROM master_module_access ma, user_group ug " +
                                   "WHERE ma.module_id = " + moduleID + " " +
                                   "AND ug.group_id = ma.group_id " +
                                   "AND ug.user_id = " + userIDParam;

            //sqlCommand = "SELECT IFNULL(user_access_option, 0) AS resultQuery " +
            //                        "FROM master_module_access " +
            //                        "WHERE group_id = " + (groupIDParam == 0 ? userGroupID : groupIDParam) + " AND module_id = " + moduleID;

            accessValue = gs.userHasAccessTo(userID, userToken, sqlCommand);

            if (accessType > 1)
            {
                accessValue = accessValue / accessType;
                accessValue = accessValue % accessType;
            }
            else
            {
                if (accessValue > 0)
                    accessValue = 1;
            }

            if (accessValue == 1)
                result = true;

            return result;
        }
        #endregion
    }
}
