﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;
using System.Windows.Forms;
using System.Data;
using System.Globalization;

namespace AlphaSoft
{
    class globalIPL
    {
        private CultureInfo culture = new CultureInfo("id-ID");
        private globalUserUtil gUser;
        private globalRestAPI gRest;

        public globalIPL()
        {
            gUser = new globalUserUtil();
            gRest = new globalRestAPI();
        }

        public bool userAssignedExist(int kavlingID)
        {
            bool result = false;
            string sqlCommand = "";

            genericReply replyResult = new genericReply();

            sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                    "FROM user_kavling " +
                                    "WHERE kavling_id = " + kavlingID + " " +
                                    "AND is_active = 'Y'";

            if (gRest.checkDataExist(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    result = true;
            }

            return result;
        }

        public DateTime getStartIPL(int kavlingID, int userID = 0)
        {
            DateTime startIPL = new DateTime(3000, 1, 1);
            string sqlCommand = "";
            genericReply replyResult = new genericReply();

            sqlCommand = "SELECT start_ipl AS resultQuery " +
                                    "FROM user_kavling " +
                                    "WHERE kavling_id = " + kavlingID + " " +
                                    "AND is_active = 'Y' ";

            if (userID > 0)
                sqlCommand += "AND user_id = " + userID;

            try
            {
                if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
                {
                    if (replyResult.o_status == 1)
                    {
                        startIPL = Convert.ToDateTime(replyResult.data);
                    }
                }
            }
            catch (Exception ex) { }

            return startIPL;
        }

        public bool outstandingIPLExist(int kavlingID)
        {
            string sqlCommand = "";
            DateTime startIPL = getStartIPL(kavlingID);
            genericReply replyResult = new genericReply();

            string dateFrom = String.Format(culture, "{0:yyyyMM}", DateTime.Now);

            // IPL BELUM DIMULAI
            if (startIPL > DateTime.Now.Date)
                return false;

            // ADA TAGIHAN IPL DENGAN RANGE > DARI TGL HARI INI
            sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                    "FROM transaksi_ipl ih " +
                                    "WHERE is_active = 'Y' " +
                                    "AND ih.kavling_id = " + kavlingID + " " +
                                    "AND DATE_FORMAT(ih.end_ipl, '%Y%m') >= '" + dateFrom + "'";

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    return false;
            }

            return true;
        }

        public int getDurasiIPL(int kavlingID)
        {
            int result = 0;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT durasi_pembayaran AS resultQuery " +
                                "FROM master_kavling " +
                                "WHERE kavling_id = " + kavlingID;

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    result = Convert.ToInt32(replyResult.data);
            }           

            return result;
        }

        public double getNilaiIPL(int kavlingID)
        {
            double result = 0;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT biaya_ipl AS resultQuery " + 
                                "FROM master_kavling " +
                                "WHERE kavling_id = " + kavlingID;

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    result = Convert.ToDouble(replyResult.data);
            }

            return result;
        }

        public int getUserKavling(int kavlingID)
        {
            int result = 0;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT user_id AS resultQuery " +
                                "FROM user_kavling " +
                                "WHERE kavling_id = " + kavlingID + " " +
                                "AND is_active = 'Y'";

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    result = Convert.ToInt32(replyResult.data);
            }

            return result;
        }

        public DateTime getLastIPLPayment(int kavlingID)
        {
            DateTime dtResult = new DateTime(3000, 1, 1);
            string sqlCommand = "";
            genericReply replyResult = new genericReply();

            sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                    "FROM transaksi_ipl " +
                                    "WHERE is_active = 'Y' " +
                                    "AND kavling_id = " + kavlingID + " ";

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                {
                    sqlCommand = "SELECT end_ipl AS resultQuery  " +
                                            "FROM transaksi_ipl " +
                                            "WHERE is_active = 'Y' " +
                                            "AND kavling_id = " + kavlingID + " " +
                                            "ORDER BY end_ipl DESC " +
                                            "LIMIT 1";

                    if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
                    {
                        dtResult = Convert.ToDateTime(replyResult.data);
                    }
                }
                else
                {
                    dtResult = getStartIPL(kavlingID);
                }
            }

            return dtResult;
        }

        public bool generateOutstandingIPL(int kavlingID, int userID, out string errMsg)
        {
            bool result = false;
            errMsg = "";

            double nilaiIPL = 0;
            int durasiBayar = 0;
            DateTime dtStartTagihan = DateTime.Now;
            int month, year; 

            List<trans_ipl_header> listHeader = new List<trans_ipl_header>();
            trans_ipl_header headerData;

            List<trans_ipl_detail> listDetail = new List<trans_ipl_detail>();
            trans_ipl_detail detailData;

            genericReply replyResult = new genericReply();

            try
            {
                dtStartTagihan = getLastIPLPayment(kavlingID);
                if (dtStartTagihan.Year >= 3000)
                    throw new Exception("error getting start IPL");

                durasiBayar = getDurasiIPL(kavlingID);
                if (durasiBayar <= 0)
                    throw new Exception("error getting durasi bayar IPL");

                nilaiIPL = getNilaiIPL(kavlingID);
                if (nilaiIPL <= 0)
                    throw new Exception("error getting nilai IPL");

                while (dtStartTagihan.Date <= DateTime.Now.Date)
                {
                    #region HEADER DATA
                    headerData = new trans_ipl_header();

                    headerData.id_trans = 0;
                    headerData.type_trans = globalConstants.TRANS_TYPE_IPL;
                    headerData.date_issued = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    headerData.kavling_id = kavlingID;
                    headerData.user_id = userID;
                    headerData.nominal = nilaiIPL * durasiBayar;
                    headerData.start_ipl = new DateTime(dtStartTagihan.Year, dtStartTagihan.Month, 1);

                    month = dtStartTagihan.Date.AddMonths(durasiBayar - 1).Month;
                    year = dtStartTagihan.Date.AddMonths(durasiBayar - 1).Year;

                    headerData.end_ipl = new DateTime(year, month, DateTime.DaysInMonth(year, month));//;
                    headerData.is_active = "Y";
                    headerData.detail_data = new List<trans_ipl_detail>();
                    #endregion

                    #region DETAIL
                    for (int i = 0; i < durasiBayar; i++)
                    {
                        detailData = new trans_ipl_detail();

                        detailData.id = 0;
                        detailData.id_trans = 0;
                        detailData.nominal = nilaiIPL;
                        detailData.periode_pembayaran = dtStartTagihan.Date.AddMonths(i);
                        detailData.is_active = "Y";

                        headerData.detail_data.Add(detailData);
                    }
                    #endregion

                    listHeader.Add(headerData);
                    dtStartTagihan = dtStartTagihan.AddMonths(durasiBayar);
                }

                int newID = 0;
                if (!gRest.saveDataIPL(gUser.getUserID(), gUser.getUserToken(), listHeader, 1, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
            }

            return result;
        }

        public bool generateOutstandingIPLKompleks(int kompleksID, ProgressBar progressBar, List<kompleksIPL> kompleksStatus, 
            out string errMsg)
        {
            bool result = false;
            string sqlCommand = "";
            errMsg = "";

            double nilaiIPL = 0;
            int durasiBayar = 0;
            DateTime dtStartTagihan = DateTime.Now;
            int month, year;
            int kavlingID = 0;
            int userID = 0;
            kompleksIPL kompleksIPLData;

            #region GET LIST OF KAVLING
            REST_masterKavling kavlingData = new REST_masterKavling();

            sqlCommand = "SELECT mk.kavling_id, ml.kompleks_name, " +
                                    "IFNULL(mb.blok_name, '') AS blok_name, mk.house_no AS house_no " +
                                    "FROM master_kavling mk " +
                                    "LEFT OUTER JOIN master_kompleks ml ON (mk.kompleks_id = ml.kompleks_id) " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id) " +
                                    "WHERE mk.kompleks_id = " + kompleksID + " " +
                                    "AND mk.is_active = 'Y' ";

            if (gRest.getMasterKavlingData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref kavlingData))
            {
                if (kavlingData.dataStatus.o_status == 1)
                {
                }
                else
                {
                    errMsg = "Tidak ada informasi kavling";
                    return false;
                }
            }
            #endregion

            List<trans_ipl_header> listHeader = new List<trans_ipl_header>();
            trans_ipl_header headerData;

            trans_ipl_detail detailData;

            genericReply replyResult = new genericReply();

            try
            {
                progressBar.Value = 0;
                progressBar.Maximum = kavlingData.dataList.Count;

                for (int i = 0; i < kavlingData.dataList.Count; i++)
                {
                    Application.DoEvents();

                    listHeader.Clear();

                    kavlingID = kavlingData.dataList[i].kavling_id;

                    dtStartTagihan = getLastIPLPayment(kavlingID);
                    if (dtStartTagihan.Year >= 3000)
                    {
                        //throw new Exception("error getting start IPL");
                        continue;
                    }

                    durasiBayar = getDurasiIPL(kavlingID);
                    if (durasiBayar <= 0)
                    {
                        //throw new Exception("error getting durasi bayar IPL");
                        continue;
                    }

                    nilaiIPL = getNilaiIPL(kavlingID);
                    if (nilaiIPL <= 0)
                    {
                        //throw new Exception("error getting nilai IPL");
                        continue;
                    }

                    userID = getUserKavling(kavlingID);

                    while (dtStartTagihan.Date <= DateTime.Now.Date)
                    {
                        #region HEADER DATA
                        headerData = new trans_ipl_header();

                        headerData.id_trans = 0;
                        headerData.type_trans = globalConstants.TRANS_TYPE_IPL;
                        headerData.date_issued = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                        headerData.kavling_id = kavlingID;
                        headerData.user_id = userID;
                        headerData.nominal = nilaiIPL * durasiBayar;
                        headerData.start_ipl = new DateTime(dtStartTagihan.Year, dtStartTagihan.Month, 1);

                        month = dtStartTagihan.Date.AddMonths(durasiBayar - 1).Month;
                        year = dtStartTagihan.Date.AddMonths(durasiBayar - 1).Year;

                        headerData.end_ipl = new DateTime(year, month, DateTime.DaysInMonth(year, month));//;
                        headerData.is_active = "Y";
                        headerData.detail_data = new List<trans_ipl_detail>();
                        #endregion

                        #region DETAIL
                        for (int j = 0; j < durasiBayar; j++)
                        {
                            detailData = new trans_ipl_detail();

                            detailData.id = 0;
                            detailData.id_trans = 0;
                            detailData.nominal = nilaiIPL;
                            detailData.periode_pembayaran = dtStartTagihan.Date.AddMonths(j);
                            detailData.is_active = "Y";

                            headerData.detail_data.Add(detailData);
                        }
                        #endregion

                        listHeader.Add(headerData);
                        dtStartTagihan = dtStartTagihan.AddMonths(durasiBayar);
                    }

                    int newID = 0;
                    if (listHeader.Count > 0)
                    {
                        if (!gRest.saveDataIPL(gUser.getUserID(), gUser.getUserToken(), listHeader, 1, out errMsg, out newID))
                        {
                            //throw new Exception(errMsg);
                        }
                    }
                    else
                    {
                        errMsg = "Tidak ada tagihan";
                    }

                    kompleksIPLData = new kompleksIPL();
                    kompleksIPLData.generateStatus = (errMsg == "" ? "Success" : "Fail - " + errMsg);
                    kompleksIPLData.kavling_id = kavlingID;
                    kompleksIPLData.kavlingName = kavlingData.dataList[i].blok_name + " - " + kavlingData.dataList[i].house_no;
                    kompleksStatus.Add(kompleksIPLData);

                    progressBar.Value += 1;
                }

                progressBar.Value = progressBar.Maximum;

                result = true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
            }

            return result;
        }

        public bool deleteIPL(int idTrans, out string errMsg)
        {
            errMsg = "";
            bool result = false;

            List<trans_ipl_header> listHeader = new List<trans_ipl_header>();
            trans_ipl_header headerData;

            List<trans_ipl_detail> listDetail = new List<trans_ipl_detail>();

            try
            {
                headerData = new trans_ipl_header();

                headerData.id_trans = idTrans;
                headerData.is_active = "N";

                listHeader.Add(headerData);

                int newID = 0;
                if (!gRest.saveDataIPL(gUser.getUserID(), gUser.getUserToken(), listHeader, 2, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch(Exception ex)
            {
                errMsg = ex.Message;
            }

            return result;
        }

        public int getNumKavling(int userID)
        {
            int result = 0;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT COUNT(1) AS resultQuery " +
                                "FROM user_kavling " +
                                "WHERE user_id = " + userID + " " +
                                "AND is_active = 'Y'";

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    result = Convert.ToInt32(replyResult.data);
            }

            return result;
        }

        public bool isOutstandingTagihanExist(int userID, int kavlingID = 0)
        {
            bool result = false;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT count(1) AS resultQuery " +
                                "FROM transaksi_ipl " +
                                "WHERE user_id = " + userID + " " +
                                "AND status_id <> 2 " +
                                "AND is_active = 'Y'";

            if (kavlingID > 0)
            {
                sqlComm += "AND kavling_id = " + kavlingID;
            }

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    result = true;
            }

            return result;
        }

        public bool isLateOutstandingTagihanExist(int userID, int kavlingID = 0)
        {
            bool result = false;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT count(1) AS resultQuery " +
                                "FROM transaksi_ipl " +
                                "WHERE user_id = " + userID + " " +
                                "AND status_id <> 2 " +
                                "AND is_active = 'Y' " +
                                "AND start_ipl <= NOW() ";

            if (kavlingID > 0)
            {
                sqlComm += "AND kavling_id = " + kavlingID;
            }

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    result = true;
            }

            return result;
        }

        public bool rfidExist(string rfidValue, int userRFID)
        {
            bool result = false;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT count(1) AS resultQuery " +
                                "FROM user_kavling_rfid " +
                                "WHERE rfid_value = '" + rfidValue + "' " +
                                "AND id <> " + userRFID;

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    result = true;
            }

            return result;
        }

        public bool terminalExist(string terminalValue, int lineID)
        {
            bool result = false;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT count(1) AS resultQuery " +
                                "FROM kompleks_terminal " +
                                "WHERE UPPER(terminal_id) = '" + terminalValue.ToUpper() + "' " +
                                "AND id <> " + lineID;

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    result = true;
            }

            return result;
        }

        public bool accessSatpamExist(string accessIDValue, int aksesID, int kompleksID, List<string> deletedID)
        {
            bool result = false;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT count(1) AS resultQuery " +
                                "FROM satpam_access_list " +
                                "WHERE UPPER(access_id) = '" + accessIDValue.ToUpper() + "' " +
                                "AND id <> " + aksesID + " " +
                                "AND kompleks_id = " + kompleksID + " " +
                                "AND is_active = 'Y'";

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                    result = true;
            }

            if (deletedID.Contains(accessIDValue.ToUpper()))
                result = false;

            return result;
        }

        public bool isQREnabledStatus(int userID, int kavlingID)
        {
            bool result = false;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT user_qr_enabled AS resultQuery " +
                                "FROM user_kavling " +
                                "WHERE user_id = " + userID + " " +
                                "AND kavling_id = " + kavlingID;

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (replyResult.data.ToString() == "Y")
                    result = true;
            }

            return result;
        }

        public bool isRFIDEnabledStatus(int userID, int kavlingID, string rfidValue)
        {
            bool result = false;
            string sqlComm = "";
            genericReply replyResult = new genericReply();

            sqlComm = "SELECT rfid_enabled AS resultQuery " +
                                "FROM user_kavling uk, user_kavling_rfid ur " +
                                "WHERE uk.user_id = " + userID + " " +
                                "AND uk.kavling_id = " + kavlingID + " " +
                                "AND ur.user_kavling_id = uk.id " +
                                "AND ur.rfid_value = '" + rfidValue + "'";

            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlComm, ref replyResult))
            {
                if (replyResult.data.ToString() == "Y")
                    result = true;
            }

            return result;
        }
    }
}
