﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace AlphaSoft
{
    class globalMenuUtil
    {
        public void menuDropDownOpened(ToolStripMenuItem menuItem)
        {
            menuItem.ForeColor = Color.Black;
        }

        public void menuDropDownClosed(ToolStripMenuItem menuItem)
        {
            menuItem.ForeColor = Color.Black;
        }

        public void changeBGImage(Form parentForm, OpenFileDialog BGImageFileDialog, string imgPath)
        {
            string fileName = "";
            BGImageFileDialog.FileName = "";
            BGImageFileDialog.Filter = "(*.jpg)|*.jpg|(*.bmp)|*.bmp";
            if (BGImageFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    fileName = BGImageFileDialog.FileName;

                    if (BGImageFileDialog.FileName != imgPath) 
                    {
                        if (null != parentForm.BackgroundImage)
                        {
                            parentForm.BackgroundImage.Dispose();
                        }

                        System.IO.File.Delete(imgPath);// appPath + "\\" + BG_IMAGE);
                        System.IO.File.Copy(BGImageFileDialog.FileName, imgPath);// appPath + "\\" + BG_IMAGE);

                        parentForm.BackgroundImage = Image.FromFile(imgPath);// BG_IMAGE);
                        parentForm.BackgroundImageLayout = ImageLayout.Stretch;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

    }
}
