﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace AlphaSoft
{
    class globalTimerUtil
    {
        private CultureInfo culture = new CultureInfo("id-ID");

        private globalUserUtil gUser;
        private globalRestAPI gRest;
        
        public globalTimerUtil()
        {
            gUser = new globalUserUtil();
            gRest = new globalRestAPI();
            gIPL = new globalIPL();
        }

        public void checkForExpiredTrans()
        {
            string sqlCommand = "";
            REST_expiredTrans listTrans = new REST_expiredTrans();

            try
            {
                sqlCommand = "SELECT ID_TRANS " +
                                        "FROM TRANSAKSI_RETAIL " +
                                        "WHERE IS_ACTIVE = 'Y' " +
                                        "AND STATUS_ID not in (1, 2) " +
                                        "AND DATE_EXPIRED < NOW()";
                gRest.checkForExpiredTrans(gUser.getUserID(), gUser.getUserToken(), sqlCommand);
            }
            catch (Exception ex) { }

        }

        public void checkUserAccessStatus()
        {
            string sqlCommand = "";
            REST_userAccessList userList = new REST_userAccessList();

            List<user_access_list> listUserAccessList = new List<user_access_list>();
            user_access_list userAccessList;

            int accessStatus = 0;
            string accessMessage = "";

            try
            {
                sqlCommand = "SELECT ul.* " +
                                        "FROM user_access_list ul, master_kompleks mk, user_login_data uld " +
                                        "WHERE ul.access_status = 2 " +
                                        "AND ul.kompleks_id = mk.kompleks_id " +
                                        "AND ul.user_id = uld.user_id " +
                                        "AND uld.is_active = 'Y' " +
                                        "AND ul.is_active = 'Y' " +
                                        "AND mk.is_active = 'Y' ";

                if (gRest.getUserAccessListData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref userList))
                {
                    if (userList.dataStatus.o_status == 1)
                    {
                        for (int i = 0;i<userList.dataList.Count;i++)
                        {
                            accessStatus = 0;
                            if (!gIPL.isLateOutstandingTagihanExist(userList.dataList[i].user_id, userList.dataList[i].kavling_id))
                            {
                                accessStatus = 1;
                                accessMessage = "Welcome....";
                                if (userList.dataList[i].access_type == "QR")
                                {
                                    if (!gIPL.isQREnabledStatus(userList.dataList[i].user_id, userList.dataList[i].kavling_id))
                                    {
                                        accessStatus = 3;
                                        accessMessage = "QR Blocked....";
                                    }
                                }
                                else
                                {
                                    if (!gIPL.isRFIDEnabledStatus(userList.dataList[i].user_id, userList.dataList[i].kavling_id, userList.dataList[i].access_id))
                                    {
                                        accessStatus = 3;
                                        accessMessage = "RFID Blocked....";
                                    }
                                }
                            }

                            if (accessStatus != 0)
                            {
                                userAccessList = new user_access_list();
                                userAccessList.id = userList.dataList[i].id;
                                userAccessList.user_id = userList.dataList[i].user_id;
                                userAccessList.kompleks_id = userList.dataList[i].kompleks_id;
                                userAccessList.kavling_id = userList.dataList[i].kavling_id;
                                userAccessList.access_id = userList.dataList[i].access_id;
                                userAccessList.access_type = userList.dataList[i].access_type;
                                userAccessList.access_status = accessStatus;
                                userAccessList.message = accessMessage;
                                userAccessList.is_active = userList.dataList[i].is_active;

                                listUserAccessList.Add(userAccessList);
                            }
                        }

                        if (listUserAccessList.Count > 0)
                        {
                            string errMsg = "";
                            if (!gRest.saveDataUserAccessList(gUser.getUserID(), gUser.getUserToken(), listUserAccessList, out errMsg))
                            {
                                throw new Exception(errMsg);
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { }

        }
    }
}
