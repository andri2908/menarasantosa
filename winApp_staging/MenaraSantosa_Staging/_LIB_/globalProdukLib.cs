﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data;
using System.Data.SqlClient;

namespace AlphaSoft
{
    class globalProdukLib
    {
        private Data_Access DS;
        private globalDBUtil gDB;

        private string[] dataProduk =
        {
            "PRODUCT_NAME",
            "PRODUCT_DESCRIPTION",
            "PRODUCT_ACTIVE",
            "PRODUCT_UNIT_ID",
            "PRODUCT_QTY",
            "PRODUCT_TYPE",
            "WARNA_ID",
            "MODEL_ID",
            "MERK_ID",
            "UKURAN_ID",
            "PRODUCT_ID"
        };

        private string[] productWarna =
        {
            "PRODUCT_ID",
            "WARNA_ID"
        };

        public string[] sizeName =
        {
            "NONE",
            "S",
            "M",
            "L",
            "XL",
            "XXL"
        };

        public string[] faseProdukID =
        {
            "PTG",
            "MTK",
            "JHT",
            "QC",
            "RPK"
        };

        public string[] faseProdukName =
        {
            "POTONG",
            "MESTAK",
            "JAHIT",
            "BARANG JADI",
            "PACK"
        };

        public string[] faseName =
        {
            "PTG",//"POTONG",
            "MESTAK",
            "JAHIT",
            "QC",
            "REPACKING"
        };

        public const int lusinSet = 10;

        public const int UNIT_SET = 1;
        public const int UNIT_PCS = 2;
        public const int UNIT_KG = 3;

        public globalProdukLib(Data_Access DSParam = null)
        {
            if (null != DSParam)
                DS = DSParam;
            else
                DS = new Data_Access();

            gDB = new globalDBUtil(DS);
        }

        public int getProductType(string productID)
        {
            string sqlCommand = "";
            int productType = 0;// globalConstants.PRODUK_KAIN;

            try
            {
                sqlCommand = "SELECT PRODUCT_TYPE FROM MASTER_PRODUCT WHERE PRODUCT_ID = '" + productID + "'";
                productType = Convert.ToInt32(DS.getDataSingleValue(sqlCommand));
            }
            catch (Exception ex) { }

            return productType;
        }

        public int getProductUnit(string productID)
        {
            string sqlCommand = "";
            int productUnit = globalConstants.UNIT_PCS;

            try
            {
                sqlCommand = "SELECT PRODUCT_UNIT_ID FROM MASTER_PRODUCT WHERE PRODUCT_ID = '" + productID + "'";
                productUnit = Convert.ToInt32(DS.getDataSingleValue(sqlCommand));
            }
            catch (Exception ex) { }

            return productUnit;
        }

        public bool qtyIsEnough(string productID, double productQty, double prevQty)
        {
            bool result = true;
            string sqlCommand = "";
            double qtyTable = 0;

            sqlCommand = "SELECT PRODUCT_QTY FROM MASTER_PRODUCT WHERE PRODUCT_ID = '" + productID + "'";
            qtyTable = Convert.ToDouble(DS.getDataSingleValue(sqlCommand));

            if (qtyTable + prevQty < productQty)
                result = false;

            return result;
        }

        public void updateProductQty(string productID, double productQty, double prevQty, MySqlException inEx)
        {
            string sqlCommand = "";
            double qtyToReduce = productQty - prevQty;

            sqlCommand = "UPDATE MASTER_PRODUCT " +
                                    "SET PRODUCT_QTY = PRODUCT_QTY - " + qtyToReduce + " " +
                                    "WHERE PRODUCT_ID = '" + productID + "'";

            if (!DS.executeNonQueryCommand(sqlCommand, ref inEx))
                throw inEx;
        }

        public void addProductQty(string productID, double productQty, double prevQty, MySqlException inEx)
        {
            string sqlCommand = "";
            double qtyToAdd = productQty - prevQty;

            sqlCommand = "UPDATE MASTER_PRODUCT " +
                                    "SET PRODUCT_QTY = PRODUCT_QTY + " + qtyToAdd + " " +
                                    "WHERE PRODUCT_ID = '" + productID + "'";

            if (!DS.executeNonQueryCommand(sqlCommand, ref inEx))
                throw inEx;
        }

        public double getProductHPP(string productID)
        {
            double hppValue = 0;

            try
            {
                hppValue = Convert.ToDouble(DS.getDataSingleValue("SELECT PRODUCT_HPP FROM MASTER_PRODUCT WHERE PRODUCT_ID = '" + productID + "'"));
            }
            catch (Exception ex) { }

            return hppValue;
        }

        public bool productRepackExist(out string productID, int ukuran, int model, int merkID, string warna)
        {
            bool result = false;
            productID = "";
            string sqlCommand = "";
            string sqlWarna = "";
            int countWarna = 0;
            string[] warnaID = warna.Split(',');
            countWarna = warnaID.Length;
            MySqlDataReader rdr;
            DataTable dt = new DataTable();

            sqlWarna = "SELECT PRODUCT_ID, COUNT(1) AS NUM " +
                                "FROM MASTER_PRODUCT_WARNA " +
                                "GROUP BY PRODUCT_ID";

            sqlCommand = "SELECT MW.PRODUCT_ID " +
                                    "FROM MASTER_PRODUCT MP, MASTER_PRODUCT_WARNA MW, " +
                                    "(" + sqlWarna + ") TAB " +
                                    "WHERE MW.PRODUCT_ID = TAB.PRODUCT_ID " +
                                    "AND MW.PRODUCT_ID = MP.PRODUCT_ID " +
                                    "AND MW.WARNA_ID IN (" + warna + ") " +
                                    "AND TAB.NUM = " + countWarna + " " +
                                    "AND MP.MERK_ID = " + merkID + " " +
                                    "AND MP.MODEL_ID = " + model + " " +
                                    "AND MP.UKURAN_ID = " + ukuran + " " +
                                    "GROUP BY MW.PRODUCT_ID";

            using (rdr = DS.getData(sqlCommand))
            {
                if (rdr.HasRows)
                    dt.Load(rdr);
            }
            rdr.Close();

            foreach (DataRow r in dt.Rows)
            {
                result = true;
                productID = r["PRODUCT_ID"].ToString();
            }

            return result;
        }

        public bool recordExist(string tableName, string fieldName, string fieldValue, string idField, out string retVal)
        {
            bool result = false;
            retVal = "0";
            string sqlCommand = "SELECT COUNT(1) FROM " + tableName + " WHERE " + fieldName + " = '" + fieldValue + "'";

            if (Convert.ToInt32(DS.getDataSingleValue(sqlCommand)) > 0)
            {
                result = true;
                sqlCommand = "SELECT " + idField + " FROM " + tableName + " WHERE " + fieldName + " = '" + fieldValue + "'";

                retVal = DS.getDataSingleValue(sqlCommand).ToString();
            }

            return result;
        }

        public bool isProduksiRepackBS(string produksiID)
        {
            bool result = false;
            string sqlCommand = "";

            sqlCommand = "SELECT IS_REPACK_BS FROM PRODUKSI_HEADER WHERE PRODUKSI_ID = '" + produksiID + "'";
            if (DS.getDataSingleValue(sqlCommand).ToString() == "1")
                result = true;

            return result;
        }

        public void generateNewProduct(string prID, double qty, int lineID, string productName, 
            int warnaID, int unitID, 
            int runningNo, out int newRunningNo)
        {
            string sqlCommand = "";
            MySqlDataReader rdr;
            DataTable dt = new DataTable();

            List<string> pList = new List<string>();
            List<object> pVal = new List<object>();
            MySqlException inEx = null;
            string newProductID = "";

            newRunningNo = runningNo;

            #region MASTER PRODUCT
            pList.Clear();
            pVal.Clear();

            gDB.addParamList(pList, dataProduk, "_mentah_"+ lineID);

            newProductID = "PRD-" + runningNo;
            sqlCommand = gDB.constructMasterQuery("MASTER_PRODUCT", dataProduk, pList);

            pVal.Add(productName);
            pVal.Add("PENERIMAAN " + prID);
            pVal.Add(1);
            pVal.Add(unitID);
            pVal.Add(qty);
            pVal.Add(0);// globalConstants.PRODUK_BAHAN_MENTAH);
            pVal.Add(warnaID);
            pVal.Add(0);
            pVal.Add(0);
            pVal.Add(0);
            pVal.Add(newProductID);

            if (!DS.executeNonQueryCommandWithParameters(sqlCommand, ref inEx, pList, pVal))
                throw inEx;
            #endregion

            newRunningNo = runningNo + 1;
        }

        public void updateAvgProduct(string productID, double newQty, double newPrice, MySqlException inEx)
        {
            MySqlDataReader rdr;
            DataTable dt = new DataTable();

            double avgPrice = 0;
            string sqlCommand = "";
            double currentPrice = 0;
            double currentQty = 0;

            sqlCommand = "SELECT PRODUCT_QTY, PRODUCT_HPP " +
                                    "FROM MASTER_PRODUCT " +
                                    "WHERE PRODUCT_ID = '" + productID + "'";

            using (rdr = DS.getData(sqlCommand))
            {
                if (rdr.HasRows)
                    dt.Load(rdr);
            }
            rdr.Close();

            foreach(DataRow r in dt.Rows)
            {
                currentQty = Convert.ToDouble(r["PRODUCT_QTY"]);
                currentPrice = Convert.ToDouble(r["PRODUCT_HPP"]);
            }

            avgPrice = Math.Round(
                ((currentPrice * currentQty) + (newPrice * newQty)) / (currentQty + newQty)
                , 2);

            sqlCommand = "UPDATE MASTER_PRODUCT " +
                                    "SET PRODUCT_HPP = " + avgPrice + " " +
                                    "WHERE PRODUCT_ID = '" + productID + "'";

            if (!DS.executeNonQueryCommand(sqlCommand, ref inEx))
                throw inEx;
        }
    }
}
