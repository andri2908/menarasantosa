﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.Globalization;
using System.IO;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace AlphaSoft
{
    class LIB_dataGridUtil
    {
        private Data_Access DS;
        private CultureInfo culture = new CultureInfo("id-ID");

        public LIB_dataGridUtil(Data_Access DSParam)
        {
            DS = DSParam;
        }

        public void addDataToDataGrid(DataGridView dGrid, List<string> valuesToAdd, 
                                                        int primaryIndex = 0, string primaryValue = "",
                                                        bool allowDuplicate = false)
        {
            int rowCount = 0;
            DataGridViewRow rows;
            string valueToCheck;
            bool rowFound = false;

            if (dGrid.AllowUserToAddRows)
                rowCount = dGrid.Rows.Count + 1;
            else
                rowCount = dGrid.Rows.Count;

            for (int i = 0;i<rowCount && !rowFound;i++)
            {
                rows = dGrid.Rows[i];

                if (null != rows.Cells[primaryIndex].Value)
                    valueToCheck = rows.Cells[primaryIndex].Value.ToString();
                else
                    continue;

                if (primaryValue== valueToCheck)
                    rowFound = true;
            }

            if (rowFound)
            {

            }
            else
            {

            }
        }
    }
}
