﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class dataUserDepartmentForm : AlphaSoft.basicHotkeysForm
    {
        private globalRestAPI gRest = new globalRestAPI();
        private globalUserUtil gUser = new globalUserUtil();

        REST_userDepartmentData userDeptData = new REST_userDepartmentData();

        private int userID = 0;

        public dataUserDepartmentForm(int userIDParam)
        {
            InitializeComponent();

            userID = userIDParam;
        }

        private void loadUserDeptData()
        {
            string sqlCommand = "";

            sqlCommand = "SELECT md.department_id, md.department_name, IFNULL(ud.id, 0) AS id, IFNULL(ud.is_active, 'N') as is_active " +
                                    "FROM master_department md " +
                                    "LEFT OUTER JOIN user_department ud ON (ud.department_id = md.department_id AND ud.user_id = " + userID + ") " +
                                    "WHERE md.is_active = 'Y' " +
                                    "ORDER BY md.department_name ASC";

            if (gRest.getUserDepartmentData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref userDeptData))
            {
                userDeptGrid.Rows.Clear();

                for (int i = 0; i < userDeptData.dataList.Count; i++)
                {
                    userDeptGrid.Rows.Add(
                        userDeptData.dataList[i].id,
                        userDeptData.dataList[i].department_id,
                        (userDeptData.dataList[i].is_active == "N" ? false : true),
                        userDeptData.dataList[i].department_name
                        );
                }
            }
        }

        private void dataUserDepartmentForm_Load(object sender, EventArgs e)
        {
            loadUserDeptData();
        }

        private bool saveDataTransaction()
        {
            bool result = false;
            int deptId = 0;
            string deptName = "";
            string isSelected = "Y";
            int lineID = 0;
            string errMsg = "";

            List<user_department> listUserDeptData = new List<user_department>();
            user_department userDeptData;

            try
            {
                for (int i = 0; i < userDeptGrid.Rows.Count; i++)
                {
                    lineID = Convert.ToInt32(userDeptGrid.Rows[i].Cells["id"].Value);
                    deptId = Convert.ToInt32(userDeptGrid.Rows[i].Cells["departmentID"].Value);
                    deptName = userDeptGrid.Rows[i].Cells["departmentName"].Value.ToString();
                    isSelected = (Convert.ToBoolean(userDeptGrid.Rows[i].Cells["flag"].Value) ? "Y" : "N");

                    userDeptData = new user_department();
                    userDeptData.id = lineID;
                    userDeptData.department_id = deptId;
                    userDeptData.department_name = deptName;
                    userDeptData.is_active = isSelected;

                    listUserDeptData.Add(userDeptData);
                }

                if (!gRest.saveUserDepartment(gUser.getUserID(), gUser.getUserToken(), userID, listUserDeptData, out errMsg))
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (saveDataTransaction())
            {
                errorLabel.Text = "";
                MessageBox.Show("Success");
                this.Close();
            }
            else
                MessageBox.Show("Fail");
        }

        private void nonAktifCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < userDeptGrid.Rows.Count; i++)
            {
                userDeptGrid.Rows[i].Cells["flag"].Value = nonAktifCheckbox.Checked;
            }
        }
    }
}
