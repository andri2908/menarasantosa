﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaSoft
{
    class globalConstants
    {
        public const string TS_submit = "submit";
        public const string TS_closed = "closed";
        public const string TS_admin_response = "admin_response";
        public const string TS_user_response  = "user_response";
        public const string TS_request_approval = "request_approval";
        public const string TS_approved = "approved";
        public const string TS_rejected = "rejected";

        public const int PAYMENT_CASH_KERAS = 0;
        public const int PAYMENT_CASH_TERMIN = 1;
        public const int PAYMENT_KPR = 2;

        public const int GROUP_RESIDENT = 0;
        public const int GROUP_LANDLORD = 1;
        public const int GROUP_ADMIN = 2;

        public const int NEW_GROUP_USER = 1;
        public const int EDIT_GROUP_USER = 2;
        public const int NEW_USER = 3;
        public const int EDIT_USER = 4;
        public const int PENGATURAN_GROUP_ACCESS = 5;

        public const int NEW_TASK = 6;
        public const int EDIT_TASK = 7;

        public const int NEW_DEPARTMENT = 8;
        public const int EDIT_DEPARTMENT = 9;

        public const int NEW_KOMPLEKS = 11;
        public const int EDIT_KOMPLEKS = 12;
        public const int NEW_KAVLING = 13;
        public const int EDIT_KAVLING = 14;

        public const int NEW_UNIT = 15;
        public const int EDIT_UNIT = 16;

        public const int NEW_ITEM = 17;
        public const int EDIT_ITEM = 18;

        public const int ADD_KAVLING_USER = 15;

        public const int NEW_CUSTOMER = 18;
        public const int EDIT_CUSTOMER = 19;

        public const int NEW_SALES = 20;
        public const int EDIT_SALES = 21;

        public const int NEW_JOBS = 22;
        public const int EDIT_JOBS = 23;
        public const int VIEW_JOBS = 24;

        public const int EDIT_JOB_HEADER = 25;

        public const int MODULE_USER = 1;
        public const int MODULE_HAK_AKSES = 2;

        public const int MODULE_JOBS = 3;

        public const int SELECT_START_PROSES = 4;

        public const int VIEW_ONLY = 98;
        public const int MODULE_DEFAULT = 99;
        public const int MODULE_2ND_AUTH = 100;
    }
}
