﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaSoft
{
    class logConstants
    {
        public const int LOG_TYPE_DEFAULT = 0;
        public const int LOG_CREATE_CUSTOMER = 1;
        public const int LOG_ASSIGN_CUSTOMER = 2;
        public const int LOG_REASSIGN_CUSTOMER = 3;
        public const int LOG_RELEASE_CUSTOMER = 4;
        public const int LOG_SCRAP_CUSTOMER = 5;
        public const int LOG_SET_CUSTOMER_TO_BUYER = 6;
        public const int LOG_SET_CUSTOMER_NON_AKTIF = 3;
    }
}
