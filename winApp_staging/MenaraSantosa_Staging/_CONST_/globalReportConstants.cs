﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaSoft
{
    class globalReportConstants
    {
        public const int _START_PRINTOUT_ = 300;

        #region PRINTOUT CONST
        public const int PRINTOUT_IPL = 301;
        public const int PRINTOUT_TRANSAKSI_RETAIL = 302;
        public const int PRINTOUT_TRANSAKSI_PENERIMAAN = 303;
        public const int PRINTOUT_PENGIRIMAN_TRANSAKSI = 304;
        #endregion

        public const int _END_PRINTOUT_ = 400;


        public const int START_REPORT = 501;

        #region REPORT CONST
        public const int REPORT_JOBS = 502;
        #endregion

        public const int END_REPORT = 600;


        public const string transaksiXML = "transaksi.xml";
        public const string reportJobsXML = "jobs.xml";
    }
}
