﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaSoft
{
    class MMConstants
    {
        public const int HAK_AKSES = 1;
        public const int HAK_NEW_DATA = 2;
        public const int HAK_UPDATE_DATA = 4;

        public const int VIEW_JOBS = 1;
        public const int VIEW_REPORTS = 2;
        public const int PREFERENCES = 3;

        public const int PENGATURAN_USER = 4;
        public const int PENGATURAN_USER_GROUP = 5;
        public const int PENGATURAN_MASTER_GROUP = 6;
        public const int PENGATURAN_GROUP_AKSES = 7;
        public const int PENGATURAN_DEPARTEMEN = 8;
        public const int PENGATURAN_TASK_FLOW = 9;
        public const int PENGATURAN_CUSTOMER = 10;
        public const int PENGATURAN_SALES = 11;
        public const int PENGATURAN_KOMPLEKS = 12;
        public const int PENGATURAN_KAVLING = 13;
        public const int PENGATURAN_SISTEM = 14;
        public const int PENGATURAN_JOB = 15;
        public const int UPDATE_JOB_HEADER = 16;

        //public const int MANAJEMEN_SISTEM = 1;
        //public const int TAMBAH_HAPUS_GROUP_USER = 2;
        //public const int TAMBAH_HAPUS_USER = 3;
        //public const int PENGATURAN_AKSES_MODUL = 4;
        //public const int BACKUP_RESTORE_DATABASE = 5;
        //public const int PENGATURAN_SISTEM_APLIKASI = 6;

        //public const int TAMBAH_HAPUS_KOMPLEKS = 21;
        //public const int TAMBAH_HAPUS_KAVLING = 22;

        //public const int TAMBAH_HAPUS_ITEM = 23;
        //public const int TAMBAH_HAPUS_UNIT = 24;
        //public const int TAMBAH_HAPUS_PEMILIK_KAVLING = 25;
        //public const int TAMBAH_HAPUS_ITEM_DRUGS = 26;
        //public const int TAMBAH_HAPUS_NEWS = 27;

        //public const int RINGKASAN_IPL = 41;
        //public const int TAMBAH_HAPUS_RETAIL = 42;
        //public const int PENYESUAIAN_STOK = 43;
        //public const int PENERIMAAN_BARANG = 44;

        //public const int TAMBAH_HAPUS_DRUGS = 45;
        //public const int PENYESUAIAN_DRUGS = 46;
        //public const int PENERIMAAN_DRUGS = 47;

        public const int SECOND_AUTH = 999;
    }
}
