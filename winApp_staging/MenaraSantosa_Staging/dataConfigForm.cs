﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.IO;

using DBGridExtension;
using Hotkeys;

namespace AlphaSoft
{
    public partial class dataConfigForm : AlphaSoft.basicHotkeysForm
    {
        private CultureInfo culture = new CultureInfo("id-ID");

        private globalRestAPI gRest = new globalRestAPI();
        private globalUserUtil gUtil = new globalUserUtil();
        private globalPrinterUtility gPrinter = new globalPrinterUtility();

        private REST_userLoginData userLoginData = new REST_userLoginData();
        private REST_groupUser groupUserData = new REST_groupUser();
        private REST_department deptData = new REST_department();
        private REST_customer custData = new REST_customer();
        private REST_sales salesData = new REST_sales();
        private REST_task taskData = new REST_task();

        private List<string> printerList = new List<string>();

        private Hotkeys.GlobalHotkey ghk_F4;

        private void createNewData()
        {
            int idxTab = configTabControl.SelectedIndex;
            string tabName = configTabControl.TabPages[idxTab].Name;
            
            switch(tabName)
            {
                case "tabUser":
                    newUser_Click(this, EventArgs.Empty);
                    break;

                case "tabGroup":
                    newGroupUser_Click(this, EventArgs.Empty);
                    break;

                case "tabDepartment":
                    newDepartment_Click(this, EventArgs.Empty);
                    break;

                case "tabFlow":
                    newTask_Click(this, EventArgs.Empty);
                    break;

                case "tabCustomer":
                    newCustomer_Click(this, EventArgs.Empty);
                    break;

                case "tabSales":
                    newSales_Click(this, EventArgs.Empty);
                    break;

                case "tabKompleks":
                    newKompleks_Click(this, EventArgs.Empty);
                    break;

                case "tabKavling":
                    newKavling_Click(this, EventArgs.Empty);
                    break;
            }

        }

        protected override void captureAll(Keys key)
        {
            base.captureAll(key);

            switch (key)
            {
                case Keys.F4:
                    createNewData();
                    break;
            }
        }

        protected override void registerGlobalHotkey()
        {
            base.registerGlobalHotkey();

            ghk_F4 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F4, this);
            ghk_F4.Register();
        }

        protected override void unregisterGlobalHotkey()
        {
            base.unregisterGlobalHotkey();

            ghk_F4.Unregister();
        }

        public dataConfigForm()
        {
            InitializeComponent();
        }

        private void tabControl1_DrawItem(Object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush _textBrush = new SolidBrush(e.ForeColor);

            // Get the item from the collection.
            TabPage _tabPage = configTabControl.TabPages[e.Index];

            // Get the real bounds for the tab rectangle.
            Rectangle _tabBounds = configTabControl.GetTabRect(e.Index);

            if (e.State == DrawItemState.Selected)
            {
                // Draw a different background color, and don't paint a focus rectangle.
                //_textBrush = new SolidBrush(e.ForeColor);
                g.FillRectangle(Brushes.SteelBlue, e.Bounds);
            }
            else
            {
                //_textBrush = new System.Drawing.SolidBrush(e.ForeColor);
                e.DrawBackground();
            }

            // Use our own font.
            Font _tabFont = new Font("Verdana", 14.0f, FontStyle.Bold, GraphicsUnit.Pixel);

            // Draw string. Center the text.
            StringFormat _stringFlags = new StringFormat();
            _stringFlags.Alignment = StringAlignment.Center;
            _stringFlags.LineAlignment = StringAlignment.Center;

            g.DrawString(_tabPage.Text, _tabFont, _textBrush, _tabBounds, new StringFormat(_stringFlags));
        }

        private void loadUserData()
        {
            string sqlCommand = "";
            string whereUser = "";

            if (!nonActiveCheckBox.Checked)
                whereUser = "AND mu.is_active = 'Y' ";

            sqlCommand = "SELECT mu.* " +
                                    "FROM user_login_data mu " +
                                    "WHERE 1 =1 " + 
                                    whereUser +
                                    "ORDER BY mu.user_name ASC";

            if (gRest.getUserLoginData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref userLoginData))
            {
                userDataGrid.Rows.Clear();

                for (int i = 0; i < userLoginData.dataList.Count; i++)
                {
                    userDataGrid.Rows.Add(
                        userLoginData.dataList[i].user_id,
                        userLoginData.dataList[i].user_name,
                        "EDIT",
                        "GROUP",
                        "DEPARTEMEN"
                        );
                }
            }
        }

        private void loadGroupUserData()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";

            sqlCommand = "SELECT * " +
                                    "FROM master_group " +
                                    "WHERE 1 = 1 ";

            if (!nonActiveCheckBox.Checked)
                sqlCommand = sqlCommand + "AND is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY group_name ASC";

            if (gRest.getMasterGroupData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref groupUserData))
            {
                groupDataGrid.Rows.Clear();

                for (int i = 0; i < groupUserData.dataList.Count; i++)
                {
                    groupDataGrid.Rows.Add(
                        groupUserData.dataList[i].group_id,
                        groupUserData.dataList[i].group_name,
                        groupUserData.dataList[i].group_description,
                        "EDIT",
                        "AKSES"
                        );
                }
            }
        }

        private void loadDepartmentData()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";

            sqlCommand = "SELECT * " +
                                    "FROM master_department " +
                                    "WHERE 1 = 1 ";

            if (!nonActiveCheckBox.Checked)
                sqlCommand = sqlCommand + "AND is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY department_name ASC";

            if (gRest.getDepartmentData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref deptData))
            {
                departmentDataGrid.Rows.Clear();

                for (int i = 0; i < deptData.dataList.Count; i++)
                {
                    departmentDataGrid.Rows.Add(
                        deptData.dataList[i].department_id,
                        deptData.dataList[i].department_name,
                        deptData.dataList[i].department_description,
                        "EDIT"
                        );
                }
            }
        }

        private void loadTaskData()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";

            sqlCommand = "SELECT mt.*, IFNULL(mp.task_name, '-') as task_parent_name, IFNULL(md.department_name, '-') as department_name " +
                                    "FROM master_task mt " +
                                    "LEFT OUTER JOIN master_task mp ON (mt.task_parent = mp.task_id) " +
                                    "LEFT OUTER JOIN master_department md ON (mt.department_id = md.department_id) " +
                                    "WHERE 1 = 1 ";

            if (!nonActiveCheckBox.Checked)
                sqlCommand = sqlCommand + "AND mt.is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY mt.skenario_name, mt.urutan, mt.task_name ASC";// LPAD(mt.task_parent, 10, '0') ASC, mt.task_name ASC";

            if (gRest.getTaskData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref taskData))
            {
                taskDataGrid.Rows.Clear();

                for (int i = 0; i < taskData.dataList.Count; i++)
                {
                    taskDataGrid.Rows.Add(
                        taskData.dataList[i].task_id,
                        (taskData.dataList[i].origin_parent_task_id == 0 ? taskData.dataList[i].skenario_name : ""),
                        taskData.dataList[i].urutan,
                        taskData.dataList[i].department_name,
                        taskData.dataList[i].task_name,
                        taskData.dataList[i].task_parent_name,
                        taskData.dataList[i].task_duration.ToString("N0", culture) + " hari",
                        "EDIT"
                        );
                }
            }
        }

        private void loadCustomerData()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";

            sqlCommand = "SELECT * " +
                                    "FROM master_customer " +
                                    "WHERE 1 = 1 ";

            if (!nonActiveCheckBox.Checked)
                sqlCommand = sqlCommand + "AND is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY customer_name ASC";

            if (gRest.getMasterCustomerData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref custData))
            {
                custGridView.Rows.Clear();

                for (int i = 0; i < custData.dataList.Count; i++)
                {
                    custGridView.Rows.Add(
                        custData.dataList[i].customer_id,
                        custData.dataList[i].customer_name,
                        custData.dataList[i].customer_description,
                        "EDIT"
                        );
                }
            }
        }

        private void loadSalesData()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";

            sqlCommand = "SELECT * " +
                                    "FROM master_sales " +
                                    "WHERE 1 = 1 ";

            if (!nonActiveCheckBox.Checked)
                sqlCommand = sqlCommand + "AND is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY sales_name ASC";

            if (gRest.getMasterSalesData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref salesData))
            {
                salesGridView.Rows.Clear();

                for (int i = 0; i < salesData.dataList.Count; i++)
                {
                    salesGridView.Rows.Add(
                        salesData.dataList[i].sales_id,
                        salesData.dataList[i].sales_name,
                        salesData.dataList[i].sales_description,
                        "EDIT"
                        );
                }
            }
        }

        private void loadKompleksData()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            REST_masterKompleks kompleksData = new REST_masterKompleks();

            sqlCommand = "SELECT kompleks_id, kompleks_name, kompleks_address " +
                                    "FROM master_kompleks " +
                                    "WHERE 1 = 1 ";

            if (!nonActiveCheckBox.Checked)
                sqlCommand = sqlCommand + "AND is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY kompleks_name ASC";

            if (gRest.getMasterKompleksData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref kompleksData))
            {
                if (kompleksData.dataStatus.o_status == 1)
                {
                    kompleksGridView.Rows.Clear();

                    for (int i = 0; i < kompleksData.dataList.Count; i++)
                    {
                        kompleksGridView.Rows.Add(
                            kompleksData.dataList[i].kompleks_id,
                            kompleksData.dataList[i].kompleks_name,
                            kompleksData.dataList[i].kompleks_address,
                            "EDIT"
                            );
                    }
                }
            }
        }

        private void loadKavlingData(int kompleksID = 0)
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";

            REST_masterKavling kavlingData = new REST_masterKavling();

            sqlCommand = "SELECT mk.kavling_id, ml.kompleks_name, " +
                                    "mb.blok_name, mk.house_no " +
                                    "FROM master_kavling mk " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id), " +
                                    "master_kompleks ml " +
                                    "WHERE mk.kompleks_id = ml.kompleks_id ";

            if (kompleksID > 0)
            {
                sqlCommand = sqlCommand + "AND ml.kompleks_id = " + kompleksID + " ";
            }

            if (!nonActiveCheckBox.Checked)
                sqlCommand = sqlCommand + "AND mk.is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY ml.kompleks_name ASC, mb.blok_name ASC, mk.house_no ASC";

            if (gRest.getMasterKavlingData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref kavlingData))
            {
                if (kavlingData.dataStatus.o_status == 1)
                {
                    kavlingGridView.Rows.Clear();

                    for (int i = 0; i < kavlingData.dataList.Count; i++)
                    {
                        kavlingGridView.Rows.Add(
                            kavlingData.dataList[i].kavling_id,
                            kavlingData.dataList[i].kompleks_name,
                             kavlingData.dataList[i].blok_name + " [" + kavlingData.dataList[i].house_no + "]",
                             "EDIT"
                            );
                    }
                }
            }
        }

        private void fillInPrinterCombo()
        {
            gPrinter.getListOfPrinter(ref printerList);

            kuartoPrinter.Items.Clear();

            for (int i = 0; i < printerList.Count; i++)
            {
                kuartoPrinter.Items.Add(printerList[i]);
            }

            if (printerList.Count > 0)
                kuartoPrinter.SelectedIndex = 0;
        }

        private void getAutoLoginValue()
        {
            //genericReply replyResult = new genericReply();

            //if (gRest.getAutoLoginData(gUtil.getUserID(), gUtil.getUserToken(), ref replyResult))
            //{
            //    if (replyResult.o_status == 1)
            //    {
            //        if (replyResult.data.ToString() == "Y")
            //            checkBoxAutoLogin.Checked = true;
            //    }
            //}

            string appPath = Application.StartupPath + "\\conf";
            string configPath = appPath + "\\autoLogin.cfg";
            string result = "0";
            string[] autosetting;

            if (File.Exists(configPath))
                result = File.ReadAllText(configPath);

            if (result.Length > 0)
            {
                autosetting = result.Split('#');
                if (autosetting[0] == "1")
                {
                    checkBoxAutoLogin.Checked = true;
                    comboAkses.SelectedIndex = Convert.ToInt32(autosetting[2]);
                }
            }
        }

        private void loadSysConfig()
        {
            int comboIndex = 0;
            string printerName = gPrinter.getConfigPrinterName(2);

            comboIndex = kuartoPrinter.FindString(printerName);
            if (comboIndex >= 0)
                kuartoPrinter.SelectedIndex = comboIndex;
            else
                kuartoPrinter.SelectedIndex = 0;

            printPreviewCheckBox.Checked = gPrinter.getPrintPreview();

            getAutoLoginValue();
        }

        private void applyModuleAccess()
        {
            // tab user
            if (!gUtil.RS_userHasAccessTo(MMConstants.PENGATURAN_USER, MMConstants.HAK_UPDATE_DATA, gUtil.getUserID()))
            {
                userDataGrid.Columns["editButton"].Visible = false;
                userDataGrid.Columns["groupButton"].Visible = false;
                userDataGrid.Columns["departmentButton"].Visible = false;
            }
        }

        private void dataConfigForm_Load(object sender, EventArgs e)
        {
            configTabControl.DrawItem += new DrawItemEventHandler(tabControl1_DrawItem);

            loadUserData();
            loadGroupUserData();
            loadDepartmentData();
            loadTaskData();
            loadCustomerData();
            loadSalesData();
            loadKompleksData();
            loadKavlingData();

            fillInPrinterCombo();

            loadSysConfig();

            userDataGrid.DoubleBuffered(true);
            groupDataGrid.DoubleBuffered(true);
            taskDataGrid.DoubleBuffered(true);
            departmentDataGrid.DoubleBuffered(true);
            custGridView.DoubleBuffered(true);
            salesGridView.DoubleBuffered(true);
            kompleksGridView.DoubleBuffered(true);
            kavlingGridView.DoubleBuffered(true);

            if (!gUtil.RS_userHasAccessTo(MMConstants.PENGATURAN_SISTEM, MMConstants.HAK_AKSES, gUtil.getUserID()))
            {
                kuartoPrinter.Enabled = false;
                printPreviewCheckBox.Enabled = false;
                saveButton.Enabled = false;
            }

            applyModuleAccess();

            comboAkses.SelectedIndex = 0;
        }

        private void userEditButton(int userID)
        {
            dataUserDetailForm displayForm = new dataUserDetailForm(userID);
            displayForm.ShowDialog(this);

            loadUserData();
        }

        private void userGroupButton(int userID)
        {
            dataUserGroupForm displayForm = new dataUserGroupForm(userID);
            displayForm.ShowDialog(this);
        }

        private void userDepartmentButton(int userID)
        {
            dataUserDepartmentForm displayForm = new dataUserDepartmentForm(userID);
            displayForm.ShowDialog(this);
        }

        private void userDataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            string columnName = "";
     
            DataGridViewRow sRow;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                sRow = userDataGrid.Rows[e.RowIndex];

                columnName = sRow.Cells[e.ColumnIndex].OwningColumn.Name;
                int userID = Convert.ToInt32(sRow.Cells["userID"].Value);

                switch (columnName)
                {
                    case "editButton":
                        userEditButton(userID);
                        break;

                    case "groupButton":
                        userGroupButton(userID);
                        break;

                    case "departmentButton":
                        userDepartmentButton(userID);
                        break;
                }
            }
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem exitPref = new ToolStripMenuItem();

            contextMenuStrip.Items.Clear();

            exitPref.Text = "Exit Preferences";
            exitPref.Font = new Font("Verdana", 9, FontStyle.Bold);

            contextMenuStrip.Items.Add(exitPref);
            contextMenuStrip.Show(buttonExit, new Point(0, buttonExit.Height));

            exitPref.Click += exitPref_Click;
        }

        private void exitPref_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonUser_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem userMenu = new ToolStripMenuItem();

            contextMenuStrip.Items.Clear();

            if (gUtil.RS_userHasAccessTo(MMConstants.PENGATURAN_USER, MMConstants.HAK_NEW_DATA, gUtil.getUserID()))
            {
                userMenu.Text = "User Baru";
                userMenu.Font = new Font("Verdana", 9, FontStyle.Bold);

                contextMenuStrip.Items.Add(userMenu);
                contextMenuStrip.Show(buttonUser, new Point(0, buttonUser.Height));

                userMenu.Click += newUser_Click;
            }
        }

        private void newUser_Click(object sender, EventArgs e)
        {
            dataUserDetailForm displayForm = new dataUserDetailForm();
            displayForm.ShowDialog(this);

            loadUserData();
        }

        private void buttonGroupUser_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem userMenu = new ToolStripMenuItem();

            contextMenuStrip.Items.Clear();

            if (gUtil.RS_userHasAccessTo(MMConstants.PENGATURAN_MASTER_GROUP, MMConstants.HAK_NEW_DATA, gUtil.getUserID()))
            {
                userMenu.Text = "Group User Baru";
                userMenu.Font = new Font("Verdana", 9, FontStyle.Bold);

                contextMenuStrip.Items.Add(userMenu);
                contextMenuStrip.Show(buttonGroupUser, new Point(0, buttonGroupUser.Height));

                userMenu.Click += newGroupUser_Click;
            }
        }

        private void newGroupUser_Click(object sender, EventArgs e)
        {
            dataGroupUserDetailForm displayForm = new dataGroupUserDetailForm();
            displayForm.ShowDialog(this);

            loadGroupUserData();
        }

        private void newTask_Click(object sender, EventArgs e)
        {
            dataTaskForm displayForm = new dataTaskForm();
            displayForm.ShowDialog(this);

            loadTaskData();
        }

        private void buttonFlow_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = new ToolStripMenuItem();

            contextMenuStrip.Items.Clear();

            if (gUtil.RS_userHasAccessTo(MMConstants.PENGATURAN_TASK_FLOW, MMConstants.HAK_NEW_DATA, gUtil.getUserID()))
            {
                menuItem.Text = "Tugas Baru";
                menuItem.Font = new Font("Verdana", 9, FontStyle.Bold);

                contextMenuStrip.Items.Add(menuItem);
                contextMenuStrip.Show(buttonFlow, new Point(0, buttonFlow.Height));

                menuItem.Click += newTask_Click;
            }
        }

        private void newDepartment_Click(object sender, EventArgs e)
        {
            dataDepartmentForm displayForm = new dataDepartmentForm();
            displayForm.ShowDialog(this);

            loadDepartmentData();
        }

        private void buttonDepartment_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = new ToolStripMenuItem();

            contextMenuStrip.Items.Clear();

            if (gUtil.RS_userHasAccessTo(MMConstants.PENGATURAN_DEPARTEMEN, MMConstants.HAK_NEW_DATA, gUtil.getUserID()))
            {
                menuItem.Text = "Departemen Baru";
                menuItem.Font = new Font("Verdana", 9, FontStyle.Bold);

                contextMenuStrip.Items.Add(menuItem);
                contextMenuStrip.Show(buttonDepartment, new Point(0, buttonDepartment.Height));

                menuItem.Click += newDepartment_Click;
            }
        }

        private void groupUserEditButton(int groupID)
        {
            dataGroupUserDetailForm displayForm = new dataGroupUserDetailForm(groupID);
            displayForm.ShowDialog(this);

            loadGroupUserData();
        }

        private void groupUserAksesButton(int groupID, string groupName, string groupDesc)
        {
            dataPengaturanHakAkses displayHakAksesForm = new dataPengaturanHakAkses(groupID, groupName, groupDesc);
            displayHakAksesForm.ShowDialog(this);
        }

        private void groupDataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            string columnName = "";

            DataGridViewRow sRow;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                sRow = groupDataGrid.Rows[e.RowIndex];

                columnName = sRow.Cells[e.ColumnIndex].OwningColumn.Name;
                int groupID = Convert.ToInt32(sRow.Cells["groupID"].Value);
                string groupName = sRow.Cells["groupName"].Value.ToString();
                string groupDesc= sRow.Cells["groupDescription"].Value.ToString();

                switch (columnName)
                {
                    case "groupEditButton":
                        groupUserEditButton(groupID);
                        break;

                    case "groupAksesButton":
                        groupUserAksesButton(groupID, groupName, groupDesc);
                        break;
                }
            }
        }

        private void doDepartmentEditButton(int deptID)
        {
            dataDepartmentForm displayForm = new dataDepartmentForm(deptID);
            displayForm.ShowDialog(this);

            loadDepartmentData();
        }

        private void departmentDataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            string columnName = "";

            DataGridViewRow sRow;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                sRow = departmentDataGrid.Rows[e.RowIndex];

                columnName = sRow.Cells[e.ColumnIndex].OwningColumn.Name;
                int deptID = Convert.ToInt32(sRow.Cells["departmentID"].Value);
                
                switch (columnName)
                {
                    case "departmentEditButton":
                        doDepartmentEditButton(deptID);
                        break;
                }
            }
        }

        private void doTaskEditButton(int taskID)
        {
            dataTaskForm displayForm = new dataTaskForm(taskID);
            displayForm.ShowDialog(this);

            loadTaskData();
        }

        private void taskDataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            string columnName = "";

            DataGridViewRow sRow;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                sRow = taskDataGrid.Rows[e.RowIndex];

                columnName = sRow.Cells[e.ColumnIndex].OwningColumn.Name;
                int taskID = Convert.ToInt32(sRow.Cells["taskID"].Value);

                switch (columnName)
                {
                    case "taskEditButton":
                        doTaskEditButton(taskID);
                        break;
                }
            }
        }

        private void newCustomer_Click(object sender, EventArgs e)
        {
            dataCustomerForm displayForm = new dataCustomerForm();
            displayForm.ShowDialog(this);

            loadCustomerData();
        }

        private void buttonCustomer_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = new ToolStripMenuItem();

            contextMenuStrip.Items.Clear();

            if (gUtil.RS_userHasAccessTo(MMConstants.PENGATURAN_CUSTOMER, MMConstants.HAK_NEW_DATA, gUtil.getUserID()))
            {
                menuItem.Text = "Customer Baru";
                menuItem.Font = new Font("Verdana", 9, FontStyle.Bold);

                contextMenuStrip.Items.Add(menuItem);
                contextMenuStrip.Show(buttonCustomer, new Point(0, buttonCustomer.Height));

                menuItem.Click += newCustomer_Click;
            }
        }

        private void newSales_Click(object sender, EventArgs e)
        {
            dataSalesForm displayForm = new dataSalesForm();
            displayForm.ShowDialog(this);

            loadSalesData();
        }

        private void buttonSales_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = new ToolStripMenuItem();

            contextMenuStrip.Items.Clear();

            if (gUtil.RS_userHasAccessTo(MMConstants.PENGATURAN_SALES, MMConstants.HAK_NEW_DATA, gUtil.getUserID()))
            {
                menuItem.Text = "Sales Baru";
                menuItem.Font = new Font("Verdana", 9, FontStyle.Bold);

                contextMenuStrip.Items.Add(menuItem);
                contextMenuStrip.Show(buttonSales, new Point(0, buttonSales.Height));

                menuItem.Click += newSales_Click;
            }
        }

        private void newKompleks_Click(object sender, EventArgs e)
        {
            dataKompleksDetailForm displayForm = new dataKompleksDetailForm();
            displayForm.ShowDialog(this);

            loadKompleksData();
        }

        private void buttonKompleks_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = new ToolStripMenuItem();

            contextMenuStrip.Items.Clear();

            if (gUtil.RS_userHasAccessTo(MMConstants.PENGATURAN_KOMPLEKS, MMConstants.HAK_NEW_DATA, gUtil.getUserID()))
            {
                menuItem.Text = "Kompleks Baru";
                menuItem.Font = new Font("Verdana", 9, FontStyle.Bold);

                contextMenuStrip.Items.Add(menuItem);
                contextMenuStrip.Show(buttonKompleks, new Point(0, buttonKompleks.Height));

                menuItem.Click += newKompleks_Click;
            }
        }
        
        private void doCustEditButton(int custID)
        {
            dataCustomerForm displayForm = new dataCustomerForm(custID);
            displayForm.ShowDialog(this);

            loadCustomerData();
        }

        private void custGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            string columnName = "";

            DataGridViewRow sRow;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                sRow = custGridView.Rows[e.RowIndex];

                columnName = sRow.Cells[e.ColumnIndex].OwningColumn.Name;
                int custID = Convert.ToInt32(sRow.Cells["custID"].Value);

                switch (columnName)
                {
                    case "editCust":
                        doCustEditButton(custID);
                        break;
                }
            }
        }

        private void doSalesEditButton(int salesID)
        {
            dataSalesForm displayForm = new dataSalesForm(salesID);
            displayForm.ShowDialog(this);

            loadSalesData();
        }

        private void salesGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            string columnName = "";

            DataGridViewRow sRow;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                sRow = salesGridView.Rows[e.RowIndex];

                columnName = sRow.Cells[e.ColumnIndex].OwningColumn.Name;
                int salesID = Convert.ToInt32(sRow.Cells["salesID"].Value);

                switch (columnName)
                {
                    case "editSales":
                        doSalesEditButton(salesID);
                        break;
                }
            }
        }

        private void doKompleksEditButton(int idParam)
        {
            dataKompleksDetailForm displayForm = new dataKompleksDetailForm(idParam);
            displayForm.ShowDialog(this);

            loadKompleksData();
        }

        private void kompleksGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            string columnName = "";

            DataGridViewRow sRow;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                sRow = kompleksGridView.Rows[e.RowIndex];

                columnName = sRow.Cells[e.ColumnIndex].OwningColumn.Name;
                int selectedID = Convert.ToInt32(sRow.Cells["kompleksID"].Value);

                switch (columnName)
                {
                    case "editKompleks":
                        doKompleksEditButton(selectedID);
                        break;
                }
            }
        }

        private void newKavling_Click(object sender, EventArgs e)
        {
            dataKavlingDetailForm displayForm = new dataKavlingDetailForm();
            displayForm.ShowDialog(this);

            loadKavlingData();
        }

        private void buttonKavling_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = new ToolStripMenuItem();

            contextMenuStrip.Items.Clear();

            if (gUtil.RS_userHasAccessTo(MMConstants.PENGATURAN_KAVLING, MMConstants.HAK_NEW_DATA, gUtil.getUserID()))
            {
                menuItem.Text = "Kavling Baru";
                menuItem.Font = new Font("Verdana", 9, FontStyle.Bold);

                contextMenuStrip.Items.Add(menuItem);
                contextMenuStrip.Show(buttonKavling, new Point(0, buttonKavling.Height));

                menuItem.Click += newKavling_Click;
            }
        }

        private void doKavlingEditButton(int idParam)
        {
            dataKavlingDetailForm displayForm = new dataKavlingDetailForm(idParam);
            displayForm.ShowDialog(this);

            loadKavlingData();
        }

        private void kavlingGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            string columnName = "";

            DataGridViewRow sRow;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                sRow = kavlingGridView.Rows[e.RowIndex];

                columnName = sRow.Cells[e.ColumnIndex].OwningColumn.Name;
                int selectedID = Convert.ToInt32(sRow.Cells["kavlingID"].Value);

                switch (columnName)
                {
                    case "editKavling":
                        doKavlingEditButton(selectedID);
                        break;
                }
            }
        }

        private string saveConfigSetting()
        {
            //bool result = false;
            //string errMsg = "";

            string autoLogin = "";

            if (checkBoxAutoLogin.Checked)
            {
                autoLogin = "1#" + gUtil.getUserID() + "#" + comboAkses.SelectedIndex;
            }
            else
            {
                autoLogin = "0#0#0";
            }

            //try
            //{
            //    if (!gRest.saveAutoLogin(gUtil.getUserID(), gUtil.getUserToken(), (checkBoxAutoLogin.Checked ? "Y" : "N"), out errMsg))
            //    {
            //        throw new Exception(errMsg);
            //    }

            //    result = true;
            //}
            //catch (Exception ex)
            //{
            //    errorLabel.Text = ex.Message;
            //}

            return autoLogin;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                gUtil.writeFile(gUtil.appPath + "\\checkPreview.cfg", printPreviewCheckBox.Checked ? "1" : "0");
                gUtil.writeFile(gUtil.appPath + "\\kuartoPath.cfg", kuartoPrinter.Text);

                //saveConfigSetting();
                gUtil.writeFile(gUtil.appPath + "\\autoLogin.cfg", saveConfigSetting());

                MessageBox.Show("Done");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void searchKompleks_Click_1(object sender, EventArgs e)
        {
            dataBrowse_KompleksForm displayForm = new dataBrowse_KompleksForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if(displayForm.ReturnValue1 != "0")
            {
                kompleksTextBox.Text = displayForm.ReturnValue2;

                loadKavlingData(Convert.ToInt32(displayForm.ReturnValue1));
            }
        }

        private void buttonAll_Click(object sender, EventArgs e)
        {
            loadKavlingData();
        }

        private void nonActiveCheckBox_CheckedChanged(object sender, EventArgs e)
        {

            loadUserData();
            loadGroupUserData();
            loadDepartmentData();
            loadTaskData();
            loadCustomerData();
            loadSalesData();
            loadKompleksData();
            loadKavlingData();
        }

        private void checkBoxAutoLogin_CheckedChanged(object sender, EventArgs e)
        {
            comboAkses.Enabled = checkBoxAutoLogin.Checked;
        }
    }
}
