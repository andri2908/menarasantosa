﻿namespace AlphaSoft
{
    partial class dataUserDepartmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SaveButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nonAktifCheckbox = new System.Windows.Forms.CheckBox();
            this.userDeptGrid = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departmentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flag = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.departmentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userDeptGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(643, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // SaveButton
            // 
            this.SaveButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.SaveButton.BackColor = System.Drawing.Color.FloralWhite;
            this.SaveButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveButton.ForeColor = System.Drawing.Color.Black;
            this.SaveButton.Location = new System.Drawing.Point(253, 432);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(115, 37);
            this.SaveButton.TabIndex = 56;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = false;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox1.Controls.Add(this.nonAktifCheckbox);
            this.groupBox1.Controls.Add(this.userDeptGrid);
            this.groupBox1.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(12, 43);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(619, 369);
            this.groupBox1.TabIndex = 55;
            this.groupBox1.TabStop = false;
            // 
            // nonAktifCheckbox
            // 
            this.nonAktifCheckbox.AutoSize = true;
            this.nonAktifCheckbox.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nonAktifCheckbox.ForeColor = System.Drawing.Color.Black;
            this.nonAktifCheckbox.Location = new System.Drawing.Point(10, 6);
            this.nonAktifCheckbox.Name = "nonAktifCheckbox";
            this.nonAktifCheckbox.Size = new System.Drawing.Size(113, 20);
            this.nonAktifCheckbox.TabIndex = 67;
            this.nonAktifCheckbox.Text = "check semua";
            this.nonAktifCheckbox.UseVisualStyleBackColor = true;
            this.nonAktifCheckbox.CheckedChanged += new System.EventHandler(this.nonAktifCheckbox_CheckedChanged);
            // 
            // userDeptGrid
            // 
            this.userDeptGrid.AllowUserToAddRows = false;
            this.userDeptGrid.AllowUserToDeleteRows = false;
            this.userDeptGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userDeptGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.userDeptGrid.BackgroundColor = System.Drawing.Color.FloralWhite;
            this.userDeptGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.userDeptGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.departmentID,
            this.flag,
            this.departmentName});
            this.userDeptGrid.Location = new System.Drawing.Point(6, 27);
            this.userDeptGrid.MultiSelect = false;
            this.userDeptGrid.Name = "userDeptGrid";
            this.userDeptGrid.RowHeadersVisible = false;
            this.userDeptGrid.Size = new System.Drawing.Size(607, 336);
            this.userDeptGrid.TabIndex = 66;
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.Visible = false;
            this.id.Width = 33;
            // 
            // departmentID
            // 
            this.departmentID.HeaderText = "departmentID";
            this.departmentID.Name = "departmentID";
            this.departmentID.Visible = false;
            this.departmentID.Width = 149;
            // 
            // flag
            // 
            this.flag.HeaderText = "";
            this.flag.Name = "flag";
            this.flag.Width = 5;
            // 
            // departmentName
            // 
            this.departmentName.HeaderText = "Nama Departemen";
            this.departmentName.MaxInputLength = 50;
            this.departmentName.Name = "departmentName";
            this.departmentName.ReadOnly = true;
            this.departmentName.Width = 191;
            // 
            // dataUserDepartmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(643, 513);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.groupBox1);
            this.Name = "dataUserDepartmentForm";
            this.Text = "DEPARTEMEN USER ";
            this.Load += new System.EventHandler(this.dataUserDepartmentForm_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.SaveButton, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userDeptGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.Button SaveButton;
        protected System.Windows.Forms.GroupBox groupBox1;
        protected System.Windows.Forms.CheckBox nonAktifCheckbox;
        protected System.Windows.Forms.DataGridView userDeptGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn departmentID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn departmentName;
    }
}
