﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Globalization;
using System.IO;

namespace AlphaSoft
{
    public partial class backupAndRestoreForm : basicHotkeysForm
    {
        private globalDBUtil gUtil;
        private Data_Access DS = new Data_Access();
        private CultureInfo culture = new CultureInfo("id-ID");
        
        public backupAndRestoreForm()
        {
            InitializeComponent();

            DS.sqlConnect();
            gUtil = new globalDBUtil(DS);
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            string fileName = "";
            string subStrExtension = "";

            openFileDialog1.Filter = "Bak File (.bak)|*.bak";
            openFileDialog1.FileName = "";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    fileName = openFileDialog1.FileName;

                    subStrExtension = fileName.Substring(fileName.Length - 4);

                    if (subStrExtension != ".bak")
                        throw new Exception("FILE SALAH");

                    fileNameTextbox.Text = fileName;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void restoreButton_Click(object sender, EventArgs e)
        {
            string errMessage = "";
            if (fileNameTextbox.Text != "")
            {
                Application.DoEvents();

                if (gUtil.restoreDatabase(fileNameTextbox.Text, out errMessage))
                {
                    MessageBox.Show("SUCCESS");
                    errorLabel.Text = "";
                }
                else
                {
                    errorLabel.Text = errMessage;
                }
            }
            else
            {
                errorLabel.Text = "Filename is blank." + Environment.NewLine + "Please find the appropriate file!";
            }
        }

        private void backupButton_Click(object sender, EventArgs e)
        {
            string errMessage = "";
            string localDate = "";
            string fileName = "";
            localDate = String.Format(culture, "{0:ddMMyyyy}", DateTime.Now);
            fileName = "BACKUP_" + localDate + ".bak";

            saveFileDialog1.FileName = fileName;
            saveFileDialog1.AddExtension = true;
            saveFileDialog1.DefaultExt = "bak";
            saveFileDialog1.Filter = "Bak File (.bak)|*.bak";

            if (DialogResult.OK == saveFileDialog1.ShowDialog())
                if (gUtil.allTrim(saveFileDialog1.FileName).Length > 0)
                {
                    Application.DoEvents();

                    if (gUtil.backupDatabase(saveFileDialog1.FileName, out errMessage))
                    {
                        MessageBox.Show("SUCCESS");
                        errorLabel.Text = "";
                    }
                    else
                    {
                        errorLabel.Text = errMessage;
                    }
                }

        }

        private void backupAndRestoreForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            DS.sqlClose();
        }
    }
}
