﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class dataBrowse_KavlingForm : AlphaSoft.simpleBrowseForm
    {
        private int originModuleID = 0;
        private globalUserUtil gUtil;
        private globalRestAPI gRest;

        private int selectedKompleksID = 0;
        private int selectedUserID = 0;

        public dataBrowse_KavlingForm(int moduleID = 0, int kompleksID = 0, int userID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;
            selectedKompleksID = kompleksID;
            selectedUserID = userID;

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();
        }

        private void dataBrowse_KavlingForm_Load(object sender, EventArgs e)
        {
            //switch (originModuleID)
            //{
            //    //case globalConstants.MODULE_TRANSAKSI_RETAIL:
            //    //case globalConstants.SUMMARY_IPL:
            //    //    nonActiveCheckBox.Visible = false;
            //    //    break;
            //}

            displayAllAction();

            //if (!gUtil.RS_userHasAccessTo(MMConstants.TAMBAH_HAPUS_KAVLING,
            //    MMConstants.HAK_NEW_DATA))
            //    setNewButtonEnable(false);
        }

        private void displayDefault()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string paramName = "";

            paramName = getNamaTextBox();

            REST_masterKavling kavlingData = new REST_masterKavling();

            sqlCommand = "SELECT mk.kavling_id, ml.kompleks_name, " +
                                    "IFNULL(mb.blok_name, '') AS blok_name, mk.house_no AS house_no " +
                                    "FROM master_kavling mk " +
                                    "LEFT OUTER JOIN master_kompleks ml ON (mk.kompleks_id = ml.kompleks_id) " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id) " +
                                    "WHERE 1 = 1 ";

            if (paramName.Length > 0)
                sqlCommand = sqlCommand + "AND kompleks_name LIKE '%" + paramName + "%' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND mk.is_active = 'Y' ";

            if (selectedKompleksID > 0)
                sqlCommand += "AND mk.kompleks_id = " + selectedKompleksID + " ";

            sqlCommand = sqlCommand + "ORDER BY kompleks_name ASC";

            if (gRest.getMasterKavlingData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref kavlingData))
            {
                if (kavlingData.dataStatus.o_status == 1)
                {
                    dataGridView.DataSource = null;

                    dt.Columns.Add("KAVLING_ID");
                    dt.Columns.Add("NAMA KOMPLEKS");
                    dt.Columns.Add("BLOK");

                    for (int i = 0; i < kavlingData.dataList.Count; i++)
                    {
                        DataRow r = dt.Rows.Add();
                        r["KAVLING_ID"] = kavlingData.dataList[i].kavling_id;
                        r["NAMA KOMPLEKS"] = kavlingData.dataList[i].kompleks_name;
                        r["BLOK"] = kavlingData.dataList[i].blok_name + "-" + kavlingData.dataList[i].house_no;
                    }

                    dataGridView.DataSource = dt;
                    dataGridView.Columns["KAVLING_ID"].Visible = false;
                }
            }
        }

        private void displayKavlingUser()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string sqlKavling = "";
            string paramName = "";

            REST_masterKavling kavlingData = new REST_masterKavling();

            paramName = getNamaTextBox();

            sqlKavling = "SELECT mk.kavling_id FROM master_kavling mk, user_kavling uk " +
                                "WHERE uk.user_id = " + selectedUserID + " " +
                                "AND uk.kavling_id = mk.kavling_id " +
                                "AND uk.is_active = 'Y'";

            sqlCommand = "SELECT mk.kavling_id, ml.kompleks_name, " +
                                    "IFNULL(mb.blok_name, '') AS blok_name, mk.house_no AS house_no " +
                                    "FROM master_kavling mk " +
                                    "LEFT OUTER JOIN master_kompleks ml ON (mk.kompleks_id = ml.kompleks_id) " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id) " +
                                    "WHERE mk.kavling_id IN (" + sqlKavling + ") " +
                                    "AND mk.is_active = 'Y' ";

            if (paramName.Length > 0)
                sqlCommand = sqlCommand + "AND kompleks_name LIKE '%" + paramName + "%' ";

            sqlCommand = sqlCommand + "ORDER BY kompleks_name ASC";

            if (gRest.getMasterKavlingData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref kavlingData))
            {
                if (kavlingData.dataStatus.o_status == 1)
                {
                    dataGridView.DataSource = null;

                    dt.Columns.Add("KAVLING_ID");
                    dt.Columns.Add("NAMA KOMPLEKS");
                    dt.Columns.Add("BLOK");

                    for (int i = 0; i < kavlingData.dataList.Count; i++)
                    {
                        DataRow r = dt.Rows.Add();
                        r["KAVLING_ID"] = kavlingData.dataList[i].kavling_id;
                        r["NAMA KOMPLEKS"] = kavlingData.dataList[i].kompleks_name;
                        r["BLOK"] = kavlingData.dataList[i].blok_name + "-" + kavlingData.dataList[i].house_no;
                    }

                    dataGridView.DataSource = dt;
                    dataGridView.Columns["KAVLING_ID"].Visible = false;
                }
            }
        }

        protected override void displayAllAction()
        {
            switch(originModuleID)
            {
                //case globalConstants.MODULE_TRANSAKSI_RETAIL:
                //    displayKavlingUser();
                //    break;

                //case globalConstants.SUMMARY_IPL:
                default:
                    displayDefault();
                    break;
            }

        }

        protected override void dbGridDoubleClickAction(DataGridViewRow selectedRow)
        {
            int selectedID = Convert.ToInt32(selectedRow.Cells["KAVLING_ID"].Value);

            switch (originModuleID)
            {
                //case globalConstants.SUMMARY_IPL:
                case globalConstants.MODULE_DEFAULT:
                //case globalConstants.MODULE_TRANSAKSI_RETAIL:
                    ReturnValue1 = selectedRow.Cells["KAVLING_ID"].Value.ToString();
                    ReturnValue2 = selectedRow.Cells["BLOK"].Value.ToString();

                    this.Close();
                    break;

                default:
                    break;
            }
        }

        protected override void newButtonAction()
        {
        }

        private void dataBrowse_KavlingForm_FormClosed(object sender, FormClosedEventArgs e)
        {
        }
    }
}
