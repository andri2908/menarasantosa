﻿namespace AlphaSoft
{
    partial class dataBrowse_multipleDepartmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.displayButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nonAktifCheckbox = new System.Windows.Forms.CheckBox();
            this.deptGrid = new System.Windows.Forms.DataGridView();
            this.isExist = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departmentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flag = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.departmentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deptGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(643, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // displayButton
            // 
            this.displayButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.displayButton.BackColor = System.Drawing.Color.FloralWhite;
            this.displayButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayButton.ForeColor = System.Drawing.Color.Black;
            this.displayButton.Location = new System.Drawing.Point(253, 432);
            this.displayButton.Name = "displayButton";
            this.displayButton.Size = new System.Drawing.Size(115, 37);
            this.displayButton.TabIndex = 58;
            this.displayButton.Text = "Tampilkan";
            this.displayButton.UseVisualStyleBackColor = false;
            this.displayButton.Click += new System.EventHandler(this.displayButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox1.Controls.Add(this.nonAktifCheckbox);
            this.groupBox1.Controls.Add(this.deptGrid);
            this.groupBox1.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(12, 43);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(619, 369);
            this.groupBox1.TabIndex = 57;
            this.groupBox1.TabStop = false;
            // 
            // nonAktifCheckbox
            // 
            this.nonAktifCheckbox.AutoSize = true;
            this.nonAktifCheckbox.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nonAktifCheckbox.ForeColor = System.Drawing.Color.Black;
            this.nonAktifCheckbox.Location = new System.Drawing.Point(10, 6);
            this.nonAktifCheckbox.Name = "nonAktifCheckbox";
            this.nonAktifCheckbox.Size = new System.Drawing.Size(113, 20);
            this.nonAktifCheckbox.TabIndex = 67;
            this.nonAktifCheckbox.Text = "check semua";
            this.nonAktifCheckbox.UseVisualStyleBackColor = true;
            this.nonAktifCheckbox.CheckedChanged += new System.EventHandler(this.nonAktifCheckbox_CheckedChanged);
            // 
            // deptGrid
            // 
            this.deptGrid.AllowUserToAddRows = false;
            this.deptGrid.AllowUserToDeleteRows = false;
            this.deptGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.deptGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.deptGrid.BackgroundColor = System.Drawing.Color.FloralWhite;
            this.deptGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.deptGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.isExist,
            this.departmentID,
            this.flag,
            this.departmentName});
            this.deptGrid.Location = new System.Drawing.Point(6, 27);
            this.deptGrid.MultiSelect = false;
            this.deptGrid.Name = "deptGrid";
            this.deptGrid.RowHeadersVisible = false;
            this.deptGrid.Size = new System.Drawing.Size(607, 336);
            this.deptGrid.TabIndex = 66;
            // 
            // isExist
            // 
            this.isExist.HeaderText = "isExist";
            this.isExist.Name = "isExist";
            this.isExist.Visible = false;
            this.isExist.Width = 78;
            // 
            // departmentID
            // 
            this.departmentID.HeaderText = "departmentID";
            this.departmentID.Name = "departmentID";
            this.departmentID.Visible = false;
            this.departmentID.Width = 149;
            // 
            // flag
            // 
            this.flag.HeaderText = "";
            this.flag.Name = "flag";
            this.flag.Width = 5;
            // 
            // departmentName
            // 
            this.departmentName.HeaderText = "Nama Departemen";
            this.departmentName.MaxInputLength = 50;
            this.departmentName.Name = "departmentName";
            this.departmentName.ReadOnly = true;
            this.departmentName.Width = 191;
            // 
            // dataBrowse_multipleDepartmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(643, 513);
            this.Controls.Add(this.displayButton);
            this.Controls.Add(this.groupBox1);
            this.Name = "dataBrowse_multipleDepartmentForm";
            this.Load += new System.EventHandler(this.dataBrowse_multipleDepartmentForm_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.displayButton, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deptGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.Button displayButton;
        protected System.Windows.Forms.GroupBox groupBox1;
        protected System.Windows.Forms.CheckBox nonAktifCheckbox;
        protected System.Windows.Forms.DataGridView deptGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn isExist;
        private System.Windows.Forms.DataGridViewTextBoxColumn departmentID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn departmentName;
    }
}
