﻿namespace AlphaSoft
{
    partial class dataJobHeaderListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxSelesai = new System.Windows.Forms.CheckBox();
            this.checkBoxDitolak = new System.Windows.Forms.CheckBox();
            this.checkBoxPending = new System.Windows.Forms.CheckBox();
            this.jobGridView = new System.Windows.Forms.DataGridView();
            this.jobID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departmentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.no = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skenarioName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departemen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taskStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jobName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.custName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.jobGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(1067, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FloralWhite;
            this.label1.Location = new System.Drawing.Point(12, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 18);
            this.label1.TabIndex = 115;
            this.label1.Text = "Status";
            // 
            // checkBoxSelesai
            // 
            this.checkBoxSelesai.AutoSize = true;
            this.checkBoxSelesai.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxSelesai.ForeColor = System.Drawing.Color.FloralWhite;
            this.checkBoxSelesai.Location = new System.Drawing.Point(338, 43);
            this.checkBoxSelesai.Name = "checkBoxSelesai";
            this.checkBoxSelesai.Size = new System.Drawing.Size(88, 22);
            this.checkBoxSelesai.TabIndex = 114;
            this.checkBoxSelesai.Text = "Selesai";
            this.checkBoxSelesai.UseVisualStyleBackColor = true;
            this.checkBoxSelesai.CheckedChanged += new System.EventHandler(this.checkBoxSelesai_CheckedChanged);
            // 
            // checkBoxDitolak
            // 
            this.checkBoxDitolak.AutoSize = true;
            this.checkBoxDitolak.Checked = true;
            this.checkBoxDitolak.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDitolak.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxDitolak.ForeColor = System.Drawing.Color.FloralWhite;
            this.checkBoxDitolak.Location = new System.Drawing.Point(220, 43);
            this.checkBoxDitolak.Name = "checkBoxDitolak";
            this.checkBoxDitolak.Size = new System.Drawing.Size(87, 22);
            this.checkBoxDitolak.TabIndex = 113;
            this.checkBoxDitolak.Text = "Ditolak";
            this.checkBoxDitolak.UseVisualStyleBackColor = true;
            this.checkBoxDitolak.CheckedChanged += new System.EventHandler(this.checkBoxDitolak_CheckedChanged);
            // 
            // checkBoxPending
            // 
            this.checkBoxPending.AutoSize = true;
            this.checkBoxPending.Checked = true;
            this.checkBoxPending.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPending.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxPending.ForeColor = System.Drawing.Color.FloralWhite;
            this.checkBoxPending.Location = new System.Drawing.Point(96, 43);
            this.checkBoxPending.Name = "checkBoxPending";
            this.checkBoxPending.Size = new System.Drawing.Size(103, 22);
            this.checkBoxPending.TabIndex = 112;
            this.checkBoxPending.Text = "Diproses";
            this.checkBoxPending.UseVisualStyleBackColor = true;
            this.checkBoxPending.CheckedChanged += new System.EventHandler(this.checkBoxPending_CheckedChanged);
            // 
            // jobGridView
            // 
            this.jobGridView.AllowUserToAddRows = false;
            this.jobGridView.AllowUserToDeleteRows = false;
            this.jobGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.jobGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.jobGridView.BackgroundColor = System.Drawing.Color.FloralWhite;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.jobGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.jobGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.jobGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.jobID,
            this.departmentID,
            this.no,
            this.skenarioName,
            this.departemen,
            this.taskStatus,
            this.jobName,
            this.salesName,
            this.custName});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.jobGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.jobGridView.Location = new System.Drawing.Point(12, 71);
            this.jobGridView.MultiSelect = false;
            this.jobGridView.Name = "jobGridView";
            this.jobGridView.ReadOnly = true;
            this.jobGridView.RowHeadersVisible = false;
            this.jobGridView.RowTemplate.Height = 30;
            this.jobGridView.Size = new System.Drawing.Size(1043, 445);
            this.jobGridView.TabIndex = 111;
            this.jobGridView.DoubleClick += new System.EventHandler(this.jobGridView_DoubleClick);
            // 
            // jobID
            // 
            this.jobID.HeaderText = "jobID";
            this.jobID.Name = "jobID";
            this.jobID.ReadOnly = true;
            this.jobID.Visible = false;
            this.jobID.Width = 63;
            // 
            // departmentID
            // 
            this.departmentID.HeaderText = "departmentID";
            this.departmentID.Name = "departmentID";
            this.departmentID.ReadOnly = true;
            this.departmentID.Visible = false;
            this.departmentID.Width = 139;
            // 
            // no
            // 
            this.no.HeaderText = "No";
            this.no.Name = "no";
            this.no.ReadOnly = true;
            this.no.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.no.Width = 38;
            // 
            // skenarioName
            // 
            this.skenarioName.HeaderText = "Skenario";
            this.skenarioName.Name = "skenarioName";
            this.skenarioName.ReadOnly = true;
            this.skenarioName.Width = 110;
            // 
            // departemen
            // 
            this.departemen.HeaderText = "Departemen";
            this.departemen.Name = "departemen";
            this.departemen.ReadOnly = true;
            this.departemen.Width = 143;
            // 
            // taskStatus
            // 
            this.taskStatus.HeaderText = "Status";
            this.taskStatus.Name = "taskStatus";
            this.taskStatus.ReadOnly = true;
            this.taskStatus.Width = 89;
            // 
            // jobName
            // 
            this.jobName.HeaderText = "Tugas";
            this.jobName.Name = "jobName";
            this.jobName.ReadOnly = true;
            this.jobName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.jobName.Width = 68;
            // 
            // salesName
            // 
            this.salesName.HeaderText = "Sales";
            this.salesName.Name = "salesName";
            this.salesName.ReadOnly = true;
            this.salesName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.salesName.Width = 60;
            // 
            // custName
            // 
            this.custName.HeaderText = "Customer";
            this.custName.Name = "custName";
            this.custName.ReadOnly = true;
            this.custName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.custName.Width = 99;
            // 
            // dataJobHeaderListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1067, 551);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBoxSelesai);
            this.Controls.Add(this.checkBoxDitolak);
            this.Controls.Add(this.checkBoxPending);
            this.Controls.Add(this.jobGridView);
            this.Name = "dataJobHeaderListForm";
            this.Text = "DATA TUGAS";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.dataJobHeaderListForm_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.jobGridView, 0);
            this.Controls.SetChildIndex(this.checkBoxPending, 0);
            this.Controls.SetChildIndex(this.checkBoxDitolak, 0);
            this.Controls.SetChildIndex(this.checkBoxSelesai, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.jobGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBoxSelesai;
        private System.Windows.Forms.CheckBox checkBoxDitolak;
        private System.Windows.Forms.CheckBox checkBoxPending;
        protected System.Windows.Forms.DataGridView jobGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn jobID;
        private System.Windows.Forms.DataGridViewTextBoxColumn departmentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn no;
        private System.Windows.Forms.DataGridViewTextBoxColumn skenarioName;
        private System.Windows.Forms.DataGridViewTextBoxColumn departemen;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn jobName;
        private System.Windows.Forms.DataGridViewTextBoxColumn salesName;
        private System.Windows.Forms.DataGridViewTextBoxColumn custName;
    }
}
