﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class dataBrowse_CustomerForm : AlphaSoft.simpleBrowseForm
    {
        private int originModuleID = 0;
        private globalUserUtil gUtil;
        private globalRestAPI gRest;

        public REST_customer custData = new REST_customer();

        public dataBrowse_CustomerForm(int moduleID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();

            this.Text = "BROWSE CUSTOMER";
        }

        private void dataBrowse_CustomerForm_Load(object sender, EventArgs e)
        {
            displayAllAction();
        }

        protected override void displayAllAction()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string paramName = "";

            dataGridView.DataSource = null;

            paramName = getNamaTextBox();

            sqlCommand = "SELECT * " +
                                    "FROM master_customer " +
                                    "WHERE 1 = 1 ";

            if (paramName.Length > 0)
                sqlCommand = sqlCommand + "AND customer_name LIKE '%" + paramName + "%' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY customer_name ASC";

            if (gRest.getMasterCustomerData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref custData))
            {
                dt.Columns.Add("CUSTOMER_ID");
                dt.Columns.Add("NAMA");
                dt.Columns.Add("DESKRIPSI");

                for (int i = 0; i < custData.dataList.Count; i++)
                {
                    DataRow r = dt.Rows.Add();
                    r["CUSTOMER_ID"] = custData.dataList[i].customer_id;
                    r["NAMA"] = custData.dataList[i].customer_name;
                    r["DESKRIPSI"] = custData.dataList[i].customer_description;
                }

                dataGridView.DataSource = dt;
                dataGridView.Columns["CUSTOMER_ID"].Visible = false;
            }
        }

        protected override void dbGridDoubleClickAction(DataGridViewRow selectedRow)
        {
            int selectedID = Convert.ToInt32(selectedRow.Cells["CUSTOMER_ID"].Value);

            switch (originModuleID)
            {
                case globalConstants.MODULE_DEFAULT:
                    ReturnValue1 = selectedRow.Cells["CUSTOMER_ID"].Value.ToString();
                    ReturnValue2 = selectedRow.Cells["NAMA"].Value.ToString();

                    this.Close();
                    break;

                default:
                    dataCustomerForm editCustomerForm = new dataCustomerForm(selectedID);
                    editCustomerForm.ShowDialog(this);
                    break;
            }
        }

        protected override void newButtonAction()
        {
            dataCustomerForm displayForm = new dataCustomerForm();
            displayForm.ShowDialog(this);
        }
    }
}
