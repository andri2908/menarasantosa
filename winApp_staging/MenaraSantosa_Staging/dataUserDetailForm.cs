﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using Hotkeys;

namespace AlphaSoft
{
    public partial class dataUserDetailForm : basicHotkeysForm
    {
        private int originModuleID = 0;
        private int selectedUserID = 0;
        private string resultToken = "";

        private CultureInfo culture = new CultureInfo("id-ID");

        private globalUserUtil gUtil;
        private globalRestAPI gRest;

        public REST_userLoginData userLoginData = new REST_userLoginData();

        private Hotkeys.GlobalHotkey ghk_F5;
        private Hotkeys.GlobalHotkey ghk_F9;

        public dataUserDetailForm(int userID = 0)
        {
            InitializeComponent();
            selectedUserID = userID;

            if (selectedUserID > 0)
                originModuleID = globalConstants.EDIT_USER;
            else
                originModuleID = globalConstants.NEW_USER;

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();
        }

        protected override void captureAll(Keys key)
        {
            base.captureAll(key);

            switch (key)
            {
                case Keys.F5:
                    resetbutton.PerformClick();
                    break;

                case Keys.F9:
                    saveButton.PerformClick();
                    break;
            }
        }

        protected override void registerGlobalHotkey()
        {
            base.registerGlobalHotkey();

            ghk_F5 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F5, this);
            ghk_F5.Register();

            ghk_F9 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F9, this);
            ghk_F9.Register();
        }

        protected override void unregisterGlobalHotkey()
        {
            base.unregisterGlobalHotkey();

            ghk_F5.Unregister();
            ghk_F9.Unregister();
        }

        private void loadUserDataInformation()
        {
            DataTable dt = new DataTable();

            string sqlCommand = "SELECT ul.* " +
                                            "FROM user_login_data ul " +
                                            "WHERE ul.user_id = " + selectedUserID + " ";

            gRest.getUserLoginData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref userLoginData);

            if (userLoginData.dataStatus.o_status == 1)
            {
                userNameTextBox.Text = userLoginData.dataList[0].user_name;
                userFullNameTextBox.Text = userLoginData.dataList[0].user_full_name;
                nonAktifCheckbox.Checked = (userLoginData.dataList[0].is_active == "Y" ? false : true);
                userPhoneTextBox.Text = userLoginData.dataList[0].user_phone_1;
           
                if (selectedUserID > 1)
                    nonAktifCheckbox.Enabled = true;

                radioKTP.Checked = (userLoginData.dataList[0].user_id_type == 1 ? true : false);
                radioSIM.Checked = (userLoginData.dataList[0].user_id_type == 2 ? true : false);
                noIdTextBox.Text = userLoginData.dataList[0].user_id_no;
                userPhone2TextBox.Text = userLoginData.dataList[0].user_phone_2;
                userEmailAddress.Text = userLoginData.dataList[0].user_email_address;

                #region USER TOKEN
                sqlCommand = "SELECT access_token AS resultQuery " +
                                        "FROM user_token WHERE user_id = " + selectedUserID + " ";

                genericReply replyResult = new genericReply();
                if (gRest.getDataSingleValue(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref replyResult))
                {
                    if (replyResult.o_status == 1)
                    {
                        resultToken = replyResult.data.ToString();
                    }
                }
                #endregion
            }
        }

        private bool dataValidated()
        {
            genericReply replyResult = new genericReply();
            string sqlCommand = "";

            errorLabel.Text = "";

            if (nonAktifCheckbox.Checked)
            {
                if (DialogResult.Yes == MessageBox.Show("Non aktifkan user ?", "WARNING", 
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    return true;
                }
                else
                    return false;
            }

            if (userNameTextBox.Text.Trim().Equals(""))
            {
                errorLabel.Text = "User name tidak kosong";
                return false;
            }

            if (!gUtil.matchRegEx(userNameTextBox.Text, globalUtilities.REGEX_ALPHANUMERIC_ONLY))
            {
                errorLabel.Text = "Username harus alphanumeric (tanpa spasi dan tanda baca)";
                return false;
            }

            sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                            "FROM user_login_data WHERE user_name = '" + userNameTextBox.Text + "' " +
                                            "AND user_id <> " + selectedUserID + " " +
                                            "AND is_Active = 'Y'";

            replyResult = new genericReply();
            if (gRest.checkDataExist(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref replyResult))
            {
                if (Convert.ToInt32(replyResult.data) > 0)
                {
                    errorLabel.Text = "Username sudah ada";
                    return false;
                }
            }

            if (userFullNameTextBox.Text.Trim().Equals(""))
            {
                errorLabel.Text = "User full name kosong";
                return false;
            }

            if (passwordTextBox.Text.Trim().Equals(""))
            {
                errorLabel.Text = "Password kosong";
                return false;
            }

            if (!gUtil.matchRegEx(passwordTextBox.Text, globalUtilities.REGEX_ALPHANUMERIC_ONLY))
            {
                errorLabel.Text = "Password harus alphanumeric";
                return false;
            }

            //if (selectedGroupID == 0)
            //{
            //    errorLabel.Text = "User harus di dalam group";
            //    return false;
            //}

            if (!passwordTextBox.Text.Equals(password2TextBox.Text))
            {
                errorLabel.Text = "Input password dan ulang password tidak sama";
                return false;
            }

            if (userPhoneTextBox.Text.Length <= 0)
            {
                errorLabel.Text = "Telepon 1 harus diisi";
                return false;
            }

            TextBox[] txtBox = { userPhoneTextBox, userPhone2TextBox};
            for (int i = 0;i<txtBox.Length;i++)
            {
                if (txtBox[i].Text.Length > 0)
                {
                    if (txtBox[i].Text[0] != '0')
                    {
                        errorLabel.Text = "Telepon harus diawali angka 0";
                        return false;
                    }
                }
            }

            if (userEmailAddress.Text.Length <= 0)
            {
                errorLabel.Text = "Email harus diisi";
                return false;
            }

            errorLabel.Text = "";
            return true;
        }

        private bool saveDataTransaction()
        {
            bool result = false;
            int opType = 0;
            string errMsg = "";
            string sqlCommand = "";
            string inputName = String.Format(culture, "{0:yyyyMMddhhmmss}", DateTime.Now);

            List<user_login_data> userLoginData = new List<user_login_data>();
            user_login_data userData = new user_login_data();

            List<user_token> userTokenData = new List<user_token>();
            user_token userToken = new user_token();

            try
            {
                userData.user_name = userNameTextBox.Text;
                userData.user_password = gUtil.getMD5Value(passwordTextBox.Text);
                userData.group_id = 0;// selectedGroupID;
                userData.user_full_name = userFullNameTextBox.Text;
                userData.user_id_type = (radioKTP.Checked ? 1 : 2);
                userData.user_id_no = noIdTextBox.Text;
                userData.user_phone_1 = userPhoneTextBox.Text;
                userData.user_phone_2 = userPhone2TextBox.Text;
                userData.user_email_address = userEmailAddress.Text;
                userData.is_active = (nonAktifCheckbox.Checked ? "N" : "Y");
                userData.group_name = "";

                switch (originModuleID)
                {
                    case globalConstants.NEW_USER:
                        opType = 1;
                        userData.user_id = 0;

                        userToken.user_id = 0;
                        while (true)
                        {
                            inputName += gUtil.allTrim(userNameTextBox.Text);
                            resultToken = gUtil.getMD5Value(inputName);//gUtil.getRandomString(10);

                            sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                                    "FROM user_login_data ul, user_token ut WHERE ut.access_token = '" + resultToken + "' " +
                                                    "AND ut.user_id = ul.user_id " +
                                                    "AND ul.is_active = 'Y'";

                            genericReply replyResult = new genericReply();
                            if (gRest.checkDataExist(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref replyResult))
                            {
                                if (Convert.ToInt32(replyResult.data) <= 0)
                                {
                                    userToken.access_token = resultToken;
                                    break;
                                }
                            }
                        }

                        userTokenData.Add(userToken);
                        break;

                    case globalConstants.EDIT_USER:
                        opType = 2;
                        userData.user_id = selectedUserID;

                        userToken.user_id = selectedUserID;
                        userToken.access_token = resultToken;
                        userTokenData.Add(userToken);
                        break;
                }

                userLoginData.Add(userData);

                int newID = 0;
                if (!gRest.saveUserLoginData(gUtil.getUserID(), gUtil.getUserToken(), userLoginData, userTokenData,
                    opType, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private bool saveData()
        {
            bool result = false;
            if (dataValidated())
            {
                //  Allow main UI thread to properly display please wait form.
                Application.DoEvents();
                result = saveDataTransaction();

                return result;
            }

            return result;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (saveData())
            {
                MessageBox.Show("Success");
                this.Close();
            }
        }

        private void resetbutton_Click(object sender, EventArgs e)
        {
            selectedUserID = 0;
            gUtil.ResetAllControls(this);
            errorLabel.Text = "";

            userNameTextBox.Focus();
        }

        private void dataUserDetailForm_Load(object sender, EventArgs e)
        {
            Button[] arrButton = new Button[2];

            errorLabel.Text = "";

            arrButton[0] = saveButton;
            arrButton[1] = resetbutton;
            gUtil.reArrangeButtonPosition(arrButton, arrButton[0].Top, this.Width);
            
            gUtil.reArrangeTabOrder(this);

            if (originModuleID == globalConstants.EDIT_USER)
            {
                loadUserDataInformation();

                //if (!gUtil.RS_userHasAccessTo(MMConstants.TAMBAH_HAPUS_USER,
                //        MMConstants.HAK_UPDATE_DATA))
                //{
                //    saveButton.Enabled = false;
                //    resetbutton.Enabled = false;
                //}

                userNameTextBox.Enabled = false;
                password2TextBox.Select();
            }
            else
                userNameTextBox.Select();
        }

        private void dataUserDetailForm_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void showPassCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (showPassCheckBox.Checked)
            {
                passwordTextBox.PasswordChar = '\0';
                password2TextBox.PasswordChar = '\0';
            }
            else
            {
                passwordTextBox.PasswordChar = '*';
                password2TextBox.PasswordChar = '*';
            }
        }
    }
}
