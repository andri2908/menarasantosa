﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class progressBarForm : Form
    {
        public progressBarForm()
        {
            InitializeComponent();
        }

        public progressBarForm(int maxValue)
        {
            InitializeComponent();
            progressBar1.Maximum = maxValue;
        }

        public void setMaxValue(int valueParam)
        {
            progressBar1.Maximum = valueParam;
        }

        public void setProgressValue(int valueParam)
        {
            progressBar1.Value = valueParam;
        }

        private void smallPleaseWait_Activated(object sender, EventArgs e)
        {
        }

        private void smallPleaseWait_Deactivate(object sender, EventArgs e)
        {
        }
    }
}
