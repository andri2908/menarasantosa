﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class remarkEntryForm : Form
    {
        public string retValue = "";
        bool mandatoryInput = false;

        public remarkEntryForm(string labelValue, int maxLength, bool mandatoryInputParam = false)
        {
            InitializeComponent();

            label2.Text = labelValue;
            namaTextBox.MaxLength = maxLength;
            mandatoryInput = mandatoryInputParam;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (namaTextBox.Text.Length <= 0 
                && mandatoryInput)
            {
                MessageBox.Show(label2.Text + " TIDAK BOLEH KOSONG");
            }
            else
            {
                retValue = namaTextBox.Text;
                this.Close();
            }
        }
    }
}
