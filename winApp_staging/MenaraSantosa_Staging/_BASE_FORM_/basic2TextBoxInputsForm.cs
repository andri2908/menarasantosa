﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Hotkeys;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace AlphaSoft
{
    public partial class basic2TextBoxInputsForm : basicHotkeysForm
    {
        private Hotkeys.GlobalHotkey ghk_F5;
        private Hotkeys.GlobalHotkey ghk_F9;
        private globalUtilities gUtil = new globalUtilities();
        protected bool partialupdate;

        public basic2TextBoxInputsForm()
        {
            InitializeComponent();

            partialupdate = false;
        }

        protected virtual void setVisibility(Button buttonToSet, bool visibilityValue)
        {
            buttonToSet.Visible = visibilityValue;
        }

        protected virtual void setSaveButtonEnable(bool enableValue)
        {
            SaveButton.Enabled = enableValue;
        }

        protected virtual void setResetButtonEnable(bool enableValue)
        {
            ResetButton.Enabled = enableValue;
        }

        private void basic2TextBoxInputsForm_Load(object sender, EventArgs e)
        {
            gUtil.reArrangeTabOrder(this);
        }

        private bool saveDataAction()
        {
            if (dataValidated())
            {
                return saveDataTransaction();
            }

            return false;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (saveDataAction())
            {
                if (partialupdate == false)
                {
                    if (DialogResult.Yes == MessageBox.Show("Success, input lagi ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                        ResetButton.PerformClick();
                    else
                        this.Close();
                }
                else
                {
                    if (DialogResult.Yes == MessageBox.Show("Success, tutup layar ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                        this.Close();
                    else
                        partialupdate = false;
                }
            }
            else
                MessageBox.Show("FAIL");
        }

        protected override void captureAll(Keys key)
        {
            base.captureAll(key);

            switch (key)
            {
                case Keys.F5:
                    ResetButton.PerformClick();
                    break;

                case Keys.F9:
                    SaveButton.PerformClick();
                    break;
            }
        }

        protected override void registerGlobalHotkey()
        {
            base.registerGlobalHotkey();

            ghk_F5 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F5, this);
            ghk_F5.Register();

            ghk_F9 = new Hotkeys.GlobalHotkey(Constants.NOMOD, Keys.F9, this);
            ghk_F9.Register();
        }

        protected override void unregisterGlobalHotkey()
        {
            ghk_F5.Unregister();
            ghk_F9.Unregister();
        }

        protected virtual void resetProcess()
        {
            gUtil.ResetAllControls(this);

            namaTextBox.Select();
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            resetProcess();
        }

        protected virtual bool saveDataTransaction()
        {
            return true;
        }

        protected virtual bool dataValidated()
        {
            if (namaTextBox.Text.Length <= 0)
            {
                errorLabel.Text = "Input nama kosong";
                return false;
            }

            //if (deskripsiTextBox.Text.Length <= 0)
            //{
            //    errorLabel.Text = "DESKRIPSI KOSONG";
            //    return false;
            //}

            errorLabel.Text = "";
            return true;
        }


    }
}
