﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DBGridExtension;

namespace AlphaSoft
{
    public partial class dataDepartmentJobs : AlphaSoft.basicHotkeysForm
    {
        private adminForm parentForm;

        private int departmentID = 0;
        private string departmentName = "";

        private int currPointer = 0;
        private int maxRows = 10;
        private int totalRows = 0;
        DataTable dtJobs;

        private int parentModule = 0;

        private globalDeptUtil gDept = new globalDeptUtil();
        private globalUserUtil gUser = new globalUserUtil();
        private globalRestAPI gRest = new globalRestAPI();

        private int colorBlink = 1;

        private void doFormClosedCheckForm()
        {
            int countForm = 0;

            for (int i = 0; i < Application.OpenForms.Count; i++)
            {
                if (Application.OpenForms[i].GetType() == typeof(dataDepartmentJobs))
                {
                    countForm++;
                }
            }

            if (countForm <= 0)
                parentForm.Close();
        }

        private void doCheckCloseForm()
        {
            int countForm = 0;

            for (int i = 0; i < Application.OpenForms.Count; i++)
            {
                if (Application.OpenForms[i].GetType() == typeof(dataDepartmentJobs))
                {
                    countForm++;
                }
            }

            if (countForm > 1)
                this.Close();
            else
            {
                if (countForm > 0)
                    if (DialogResult.Yes == MessageBox.Show("Tutup layar, dan kembali ke layar login ?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                        parentForm.Close();
            }
        }

        public dataDepartmentJobs(adminForm pForm, int deptID)
        {
            InitializeComponent();

            parentForm = pForm;
            departmentID = deptID;

            parentModule = parentForm.moduleID;
            doRegisterKey = false;
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem exitScr = new ToolStripMenuItem();
            ToolStripMenuItem exitApp = new ToolStripMenuItem();
            ToolStripMenuItem newScr = new ToolStripMenuItem();
            ToolStripMenuItem newJobs = new ToolStripMenuItem();

            contextMenuStrip.Items.Clear();

            if (gUser.RS_userHasAccessTo(MMConstants.VIEW_JOBS, MMConstants.HAK_AKSES, gUser.getUserID()))
            {
                newScr.Text = "New Screen";
                newScr.Font = new Font("Verdana", 9, FontStyle.Bold);
                contextMenuStrip.Items.Add(newScr);
                newScr.Click += newScr_Click;
            }

            if (gUser.RS_userHasAccessTo(MMConstants.PENGATURAN_JOB, MMConstants.HAK_NEW_DATA, gUser.getUserID()))
            {
                newJobs.Text = "New Jobs";
                newJobs.Font = new Font("Verdana", 9, FontStyle.Bold);
                contextMenuStrip.Items.Add(newJobs);
                newJobs.Click += newJobs_Click;
            }

            exitScr.Text = "Exit Screen";
            exitScr.Font = new Font("Verdana", 9, FontStyle.Bold);
            contextMenuStrip.Items.Add(exitScr);
            exitScr.Click += exitScreen_Click;

            exitApp.Text = "Exit Apps";
            exitApp.Font = new Font("Verdana", 9, FontStyle.Bold);
            contextMenuStrip.Items.Add(exitApp);
            exitApp.Click += exitPref_Click;

            contextMenuStrip.Show(buttonExit, new Point(0, buttonExit.Height));
        }

        private void exitPref_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Tutup semua layar, dan kembali ke layar login ?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                parentForm.Close();
        }

        private void exitScreen_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Tutup layar?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {
                doCheckCloseForm();
                parentForm.removeDeptIDfromList(departmentID);
                parentForm.LayoutMdi(System.Windows.Forms.MdiLayout.TileHorizontal);
            }
        }

        private void newScr_Click(object sender, EventArgs e)
        {
            doDisplayDeptList();
        }

        private void newJobs_Click(object sender, EventArgs e)
        {
            dataJobsForm displayForm = new dataJobsForm(departmentID, departmentName);
            displayForm.ShowDialog(this);

            if (null != this)
            {
                parentForm.refreshAllDisplay();
                //loadDepartmentJobs();
            }
        }

        private void populateJobsList()
        {
            string sqlCommand = "";
            dtJobs = new DataTable();

            string statusValue = "";

            if (checkBoxPending.Checked)
                statusValue += ", 'PENDING'";

            if (checkBoxDitolak.Checked)
                statusValue += ", 'REJECTED'";

            if (checkBoxSelesai.Checked)
                statusValue += ", 'FINISH'";

            REST_displayJobs jobsList = new REST_displayJobs();

            sqlCommand = "SELECT jd.task_status, jd.job_id, jd.id, mt.department_id, mt.task_name, mt.task_duration, jd.task_start, ml.kompleks_name, mb.blok_name, mk.house_no, ms.sales_name, mc.customer_name " +
                                     "FROM data_jobs_detail jd " +
                                     "LEFT OUTER JOIN master_task mt ON (jd.task_id = mt.task_id), " +
                                     "data_jobs_header jh " +
                                     "LEFT OUTER JOIN master_kompleks ml ON (ml.kompleks_id = jh.kompleks_id) " +
                                     "LEFT OUTER JOIN master_kavling mk ON (mk.kavling_id = jh.kavling_id) " +
                                     "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id) " +
                                     "LEFT OUTER JOIN master_sales ms ON (jh.sales_id = ms.sales_id) " +
                                     "LEFT OUTER JOIN master_customer mc ON (jh.customer_id = mc.customer_id) " +
                                     "WHERE mt.department_id = " + departmentID + " " +
                                     "AND jh.is_active = 'Y' " +
                                     "AND jd.task_status in ('TEMP'" + statusValue + ") " + // add default value supaya tidak error query-nya jika tidak ada status dipilih
                                     "AND jd.job_id = jh.job_id " +
                                     "ORDER BY jd.last_update_date DESC";

            if (gRest.getJobsList(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref jobsList))
            {
                dtJobs.Columns.Add("job_id");
                dtJobs.Columns.Add("id");
                dtJobs.Columns.Add("task_name");
                dtJobs.Columns.Add("task_start");
                dtJobs.Columns.Add("kompleks_name");
                dtJobs.Columns.Add("blok_name");
                dtJobs.Columns.Add("house_no");
                dtJobs.Columns.Add("sales_name");
                dtJobs.Columns.Add("customer_name");
                dtJobs.Columns.Add("task_duration");
                dtJobs.Columns.Add("task_status");

                for (int i = 0; i < jobsList.dataList.Count; i++)
                {
                    DataRow r = dtJobs.Rows.Add();
                    r["job_id"] = jobsList.dataList[i].job_id;
                    r["id"] = jobsList.dataList[i].id;
                    r["task_name"] = jobsList.dataList[i].task_name;
                    r["task_start"] = jobsList.dataList[i].task_start;
                    r["kompleks_name"] = jobsList.dataList[i].kompleks_name;
                    r["blok_name"] = jobsList.dataList[i].blok_name;
                    r["house_no"] = jobsList.dataList[i].house_no;
                    r["sales_name"] = jobsList.dataList[i].sales_name;
                    r["customer_name"] = jobsList.dataList[i].customer_name;
                    r["task_duration"] = jobsList.dataList[i].task_duration;
                    r["task_status"] = jobsList.dataList[i].task_status;
                }

                currPointer = 0;
                totalRows = jobsList.dataList.Count;
                jobCountLabel.Text = "Jobs : " + totalRows;
            }
        }

        private void displayJobsList()
        {
            int idx = 1;
            DataRow r;

            double currTaskDur = 0;
            double taskDur = 0;
            DateTime taskStart = DateTime.Now;
            TimeSpan ts;

            jobGridView.Rows.Clear();

            int pageNo = (currPointer / maxRows) + 1;
            int totalPage = totalRows / maxRows;

            totalPage = (totalPage <= 0 ? 1 : totalPage);

            labelPage.Text = "Page " + pageNo.ToString().PadLeft(2, '0') + " from " + totalPage.ToString().PadLeft(2, '0');

            for (int i = currPointer;i<currPointer + maxRows && i<totalRows;i++)
            {
                r = dtJobs.Rows[i];

                taskDur = Convert.ToInt32(r["task_duration"]);
                taskStart = Convert.ToDateTime(r["task_start"]);
                ts = DateTime.Now - taskStart;
                currTaskDur = ts.Days;

                jobGridView.Rows.Add(
                    r["job_id"].ToString(),
                    r["id"].ToString(),
                    idx++.ToString().PadLeft(2, '0'),
                    (r["task_status"].ToString() == "PENDING" ? "Diproses" : (r["task_status"].ToString() == "REJECTED" ? "Ditolak" : "Selesai")),
                    "[" + r["kompleks_name"].ToString() + " - " + r["blok_name"].ToString() + " " + r["house_no"] + "] " + r["task_name"].ToString(),
                    r["sales_name"].ToString(),
                    r["customer_name"].ToString(),
                    currTaskDur.ToString() + " hari",//r["id"].ToString(),
                    0
                    );

                if (currTaskDur > taskDur && taskDur > 0)
                {
                    jobGridView.Rows[jobGridView.Rows.Count - 1].Cells["waktu"].Style.ForeColor = Color.Red;
                    jobGridView.Rows[jobGridView.Rows.Count - 1].Cells["isBlink"].Value = 1;
                }
            }
        }

        public void loadDepartmentJobs()
        {
            populateJobsList();

            displayJobsList();  
        }

        private bool doDisplayDeptList()
        {
            bool result = true;

            int deptCount = 1;

            deptCount = gDept.getNumOfDept();

            if (deptCount > 1)
            {
                dataBrowse_multipleDepartmentForm displayDept = new dataBrowse_multipleDepartmentForm(parentForm);
                displayDept.ShowDialog(this);

                if (displayDept.deptList.Count > 0)
                {
                    for (int i = 0; i < displayDept.deptList.Count; i++)
                    {
                        dataDepartmentJobs displayMultiJobs = new dataDepartmentJobs(parentForm, displayDept.deptList[i]);
                        displayMultiJobs.MdiParent = parentForm;
                        displayMultiJobs.Show();
                    }

                    parentForm.LayoutMdi(System.Windows.Forms.MdiLayout.TileHorizontal);
                }
            }

            return result;
        }
       
        private void dataDepartmentJobs_Load(object sender, EventArgs e)
        {
            departmentName = gDept.getDepartmentName(departmentID);
            departmentLabel.Text = departmentName;

            loadDepartmentJobs();

            timerScroll.Interval = 15000;
            timerScroll.Start();

            timerBlink.Interval = 500;
            timerBlink.Start();

            jobGridView.DoubleBuffered(true);
        }

        private void jobGridView_DoubleClick(object sender, EventArgs e)
        {
            if (jobGridView.Rows.Count <= 0)
                return;

            int rIndex = jobGridView.SelectedCells[0].RowIndex;
            DataGridViewRow sRow = jobGridView.Rows[rIndex];

            if (gUser.RS_userHasAccessTo(MMConstants.PENGATURAN_JOB, MMConstants.HAK_UPDATE_DATA, gUser.getUserID()))
            {
                dataJobsDetailForm displayForm = new dataJobsDetailForm(Convert.ToInt32(sRow.Cells["id"].Value));
                displayForm.ShowDialog(this);

                loadDepartmentJobs();

                parentForm.refreshAllDisplay();
            }
        }

        private void timerScroll_Tick(object sender, EventArgs e)
        {
            currPointer+= maxRows;

            if (currPointer >= totalRows)
                currPointer = 0;

            displayJobsList();
        }

        private void timerBlink_Tick(object sender, EventArgs e)
        {
            DataGridViewRow sRow;
            colorBlink = colorBlink * -1;

            for (int i = 0;i<jobGridView.Rows.Count;i++)
            {
                sRow = jobGridView.Rows[i];

                if (sRow.Cells["isBlink"].Value.ToString() == "1" && sRow.Cells["taskStatus"].Value.ToString() != "Selesai")
                {
                    if (colorBlink < 0)
                        sRow.DefaultCellStyle.BackColor = Color.LightSalmon;
                    else
                        sRow.DefaultCellStyle.BackColor = Color.FloralWhite;
                }
            }
        }

        private void dataDepartmentJobs_FormClosed(object sender, FormClosedEventArgs e)
        {
            //doFormClosedCheckForm();

            //if (null != parentForm)
            //{
            //    parentForm.removeDeptIDfromList(departmentID);
            //    parentForm.LayoutMdi(System.Windows.Forms.MdiLayout.TileHorizontal);
            //}
        }

        private void checkBoxPending_CheckedChanged(object sender, EventArgs e)
        {
            loadDepartmentJobs();
        }

        private void checkBoxDitolak_CheckedChanged(object sender, EventArgs e)
        {
            loadDepartmentJobs();
        }

        private void checkBoxSelesai_CheckedChanged(object sender, EventArgs e)
        {
            loadDepartmentJobs();
        }
    }
}
