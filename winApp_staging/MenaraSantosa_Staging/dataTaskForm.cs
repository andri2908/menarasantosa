﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace AlphaSoft
{
    public partial class dataTaskForm : AlphaSoft.basicHotkeysForm
    {
        private globalRestAPI gRest = new globalRestAPI();
        private globalUserUtil gUser = new globalUserUtil();

        private int taskID = 0;
        private int deptID = 0;
        private int taskParentID = 0;
        private int originParentTaskID = 0;
        private int moduleID = globalConstants.NEW_TASK;
        private CultureInfo culture = new CultureInfo("id-ID");

        private REST_task taskData = new REST_task();


        public dataTaskForm(int taskIDParam = 0)
        {
            InitializeComponent();
            taskID = taskIDParam;

            gUser = new globalUserUtil();
            gRest = new globalRestAPI();

            if (taskID > 0)
            {
                moduleID = globalConstants.EDIT_TASK;

                //if (!gUser.RS_userHasAccessTo(MMConstants.TAMBAH_HAPUS_GROUP_USER,
                //MMConstants.HAK_UPDATE_DATA))
                //{
                //    setSaveButtonEnable(false);
                //    setResetButtonEnable(false);
                //}
            }
            else
            {
                nonAktifCheckbox.Enabled = false;
                moduleID = globalConstants.NEW_TASK;
            }
        }

        private void loadDataTask()
        {
            DataTable dt = new DataTable();

            string sqlCommand = "SELECT mt.*, IFNULL(mm.task_name , '-') AS task_parent_name, IFNULL(md.department_name, '-') as department_name " +
                                            "FROM master_task mt "+
                                            "LEFT OUTER JOIN master_task mm ON (mt.task_parent = mm.task_id) " +
                                            "LEFT OUTER JOIN master_department md ON (mt.department_id = md.department_id) " +
                                            "WHERE mt.task_id = " + taskID;

            if (gRest.getTaskData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref taskData))
            {
                if (taskData.dataStatus.o_status == 1)
                {
                    taskNameTextBox.Text = taskData.dataList[0].task_name;
                    durasiTextBox.Text = taskData.dataList[0].task_duration.ToString("N0", culture);

                    taskParentID = taskData.dataList[0].task_parent;
                    taskParentTextBox.Text = taskData.dataList[0].task_parent_name;
                    
                    deptID = taskData.dataList[0].department_id;
                    departmentTextBox.Text = taskData.dataList[0].department_name;

                    nonAktifCheckbox.Checked = (taskData.dataList[0].is_active == "Y" ? false : true);

                    skenarioTextBox.Text = taskData.dataList[0].skenario_name;
                    urutanTextBox.Text = taskData.dataList[0].urutan.ToString();
                    originParentTaskID = taskData.dataList[0].origin_parent_task_id;

                    if (taskParentID > 0)
                        skenarioTextBox.ReadOnly = true;
                }
            }
        }

        private void loadInfoParent()
        {
            genericReply replyResult = new genericReply();
            string sqlCommand = "";

            sqlCommand = "SELECT skenario_name as resultQuery FROM master_task WHERE task_id = " + taskParentID;
            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
            {
                if (replyResult.o_status == 1)
                {
                    skenarioTextBox.Text = replyResult.data.ToString();
                }
            }

            sqlCommand = "SELECT urutan as resultQuery FROM master_task WHERE task_id = " + taskParentID;
            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
            {
                if (replyResult.o_status == 1)
                {
                    urutanTextBox.Text = (Convert.ToInt32(replyResult.data) + 1).ToString();
                }
            }

            sqlCommand = "SELECT origin_parent_task_id as resultQuery FROM master_task WHERE task_id = " + taskParentID;
            if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
            {
                if (replyResult.o_status == 1)
                {
                    originParentTaskID = Convert.ToInt32(replyResult.data);
                    if (originParentTaskID == 0)
                        originParentTaskID = taskParentID;
                }
            }

            skenarioTextBox.ReadOnly = true;
        }

        private void dataTaskForm_Load(object sender, EventArgs e)
        {
            if (taskID > 0)
            {
                moduleID = globalConstants.EDIT_TASK;
                loadDataTask();
            }

            durasiTextBox.Enter += TextBox_Enter;
            durasiTextBox.Leave += TextBox_Int32_Leave;

            urutanTextBox.Enter += TextBox_Enter;
            urutanTextBox.Leave += TextBox_Int32_Leave;
        }

        private void searchDeptButton_Click(object sender, EventArgs e)
        {
            dataBrowse_DepartmentForm displayForm = new dataBrowse_DepartmentForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                deptID = Convert.ToInt32(displayForm.ReturnValue1);
                departmentTextBox.Text = displayForm.ReturnValue2;
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            dataBrowse_TaskForm displayForm = new dataBrowse_TaskForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                taskParentID = Convert.ToInt32(displayForm.ReturnValue1);
                taskParentTextBox.Text = displayForm.ReturnValue2;

                loadInfoParent();
            }
        }

        private void clearParent_Click(object sender, EventArgs e)
        {
            taskParentID = 0;
            taskParentTextBox.Clear();
        }

        private void clearDept_Click(object sender, EventArgs e)
        {
            deptID = 0;
            departmentTextBox.Clear();
        }

        private bool saveDataTransaction()
        {
            bool result = false;
            int opType = 0;
            string errMsg = "";
            double durasi = 0;
            int urutanValue = 0;

            List<master_task> listTaskData = new List<master_task>();
            master_task taskData = new master_task();

            try
            {
                double.TryParse(durasiTextBox.Text, NumberStyles.Number, culture, out durasi);
                int.TryParse(urutanTextBox.Text, NumberStyles.Number, culture, out urutanValue);

                taskData.task_name = taskNameTextBox.Text;
                taskData.task_parent = taskParentID;
                taskData.task_duration = durasi;
                taskData.department_id = deptID;
                taskData.is_active = (nonAktifCheckbox.Checked ? "N" : "Y");
                taskData.skenario_name = skenarioTextBox.Text;
                taskData.urutan = urutanValue;
                taskData.origin_parent_task_id = originParentTaskID;

                switch (moduleID)
                {
                    case globalConstants.NEW_TASK:
                        opType = 1;
                        taskData.task_id = 0;
                        break;

                    case globalConstants.EDIT_TASK:
                        opType = 2;
                        taskData.task_id = taskID;
                        break;
                }

                listTaskData.Add(taskData);

                int newID = 0;
                if (!gRest.saveTask(gUser.getUserID(), gUser.getUserToken(), listTaskData, opType, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                taskID = newID;

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private bool dataValidated()
        {
            errorLabel.Text = "";
            string sqlCommand = "";
            genericReply replyResult;

            if (taskParentID == 0)
            {
                if (skenarioTextBox.Text.Length <= 0)
                {
                    errorLabel.Text = "Nama Skenario kosong";
                    return false;
                }

                sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                                "FROM master_task " +
                                                "WHERE task_parent = 0 " +
                                                "AND UPPER(REPLACE(skenario_name, ' ', '')) = '" + gUser.allTrim(skenarioTextBox.Text).ToUpper() + "' " +
                                                "AND is_active = 'Y' " +
                                                "AND task_id <> " + taskID;
                replyResult = new genericReply();
                if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
                {
                    if (replyResult.o_status == 1)
                    {
                        if (Convert.ToInt32(replyResult.data) > 0)
                        {
                            errorLabel.Text = "Nama Skenario sudah ada";
                            return false;
                        }
                    }
                }
            }

            if (taskNameTextBox.Text.Length <= 0)
            {
                errorLabel.Text = "Task Name kosong";
                return false;
            }

            double durasi = 0;
            if (durasiTextBox.Text.Length <= 0 ||
                !double.TryParse(durasiTextBox.Text, NumberStyles.Number, culture, out durasi))
            {
                errorLabel.Text = "Durasi salah";
                return false;
            }

            int urutanValue = 0;
            if (urutanTextBox.Text.Length <= 0 ||
                !int.TryParse(urutanTextBox.Text, NumberStyles.Number, culture, out urutanValue))
            {
                errorLabel.Text = "Urutan salah";
                return false;
            }

            if (nonAktifCheckbox.Checked)
            {
                sqlCommand = "SELECT COUNT(1) AS resultQuery " +
                                                "FROM master_task " +
                                                "WHERE task_parent = " + taskID + " " +
                                                "AND is_active = 'Y'";
                replyResult = new genericReply();
                if (gRest.getDataSingleValue(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref replyResult))
                {
                    if (replyResult.o_status == 1)
                    {
                        if (Convert.ToInt32(replyResult.data) > 0)
                        {
                            errorLabel.Text = "Ada anak tugas aktif";
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        private bool saveData()
        {
            if (dataValidated())
                return saveDataTransaction();

            return false;
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (saveData())
            {
                MessageBox.Show("SUCCESS");
                this.Close();
            }
            else
            {
                MessageBox.Show("FAIL");
            }
        }
    }
}
