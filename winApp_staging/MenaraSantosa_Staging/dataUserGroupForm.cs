﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class dataUserGroupForm : AlphaSoft.basicHotkeysForm
    {
        private globalRestAPI gRest = new globalRestAPI();
        private globalUserUtil gUser = new globalUserUtil();

        REST_userGroupData userGroupData = new REST_userGroupData();

        private int userID = 0;

        public dataUserGroupForm(int userIDParam)
        {
            InitializeComponent();

            userID = userIDParam;
        }

        private void loadUserData()
        {
            string sqlCommand = "";

            sqlCommand = "SELECT mg.group_id, mg.group_name, IFNULL(ug.id, 0) AS id, IFNULL(ug.is_active, 'N') as is_active " +
                                    "FROM master_group mg " +
                                    "LEFT OUTER JOIN user_group ug ON (ug.group_id = mg.group_id AND ug.user_id = " + userID + ") " +
                                    "WHERE mg.is_active = 'Y' " +
                                    "ORDER BY mg.group_name ASC";

            if (gRest.getUserGroupData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref userGroupData))
            {
                aksesDataGrid.Rows.Clear();

                for (int i = 0; i < userGroupData.dataList.Count; i++)
                {
                    aksesDataGrid.Rows.Add(
                        userGroupData.dataList[i].id,
                        userGroupData.dataList[i].group_id,
                        (userGroupData.dataList[i].is_active == "N" ? false : true),
                        userGroupData.dataList[i].group_name
                        );
                }
            }
        }

        private void dataGroupAksesForm_Load(object sender, EventArgs e)
        {
            loadUserData();
        }

        private bool saveDataTransaction()
        {
            bool result = false;
            int groupId = 0;
            string groupName = "";
            string isSelected = "Y";
            int lineID = 0;
            string errMsg = "";

            List<user_group> listUserGroupData = new List<user_group>();
            user_group userGroupData;

            try
            {
                for (int i = 0; i < aksesDataGrid.Rows.Count; i++)
                {
                    lineID = Convert.ToInt32(aksesDataGrid.Rows[i].Cells["id"].Value);
                    groupId = Convert.ToInt32(aksesDataGrid.Rows[i].Cells["groupID"].Value);
                    groupName = aksesDataGrid.Rows[i].Cells["groupName"].Value.ToString();
                    isSelected = (Convert.ToBoolean(aksesDataGrid.Rows[i].Cells["flag"].Value) ? "Y" : "N");

                    userGroupData = new user_group();
                    userGroupData.id = lineID;
                    userGroupData.group_id = groupId;
                    userGroupData.group_name = groupName;
                    userGroupData.is_active = isSelected;

                    listUserGroupData.Add(userGroupData);
                }

                if (!gRest.saveUserGroup(gUser.getUserID(), gUser.getUserToken(), userID, listUserGroupData, out errMsg))
                {
                    throw new Exception(errMsg);
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (saveDataTransaction())
            {
                errorLabel.Text = "";
                MessageBox.Show("Success");
                this.Close();
            }
            else
                MessageBox.Show("Fail");
        }

        private void nonAktifCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0;i< aksesDataGrid.Rows.Count;i++)
            {
                aksesDataGrid.Rows[i].Cells["flag"].Value = nonAktifCheckbox.Checked;
            }
        }
    }
}
