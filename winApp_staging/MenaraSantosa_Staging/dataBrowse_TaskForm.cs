﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace AlphaSoft
{
    public partial class dataBrowse_TaskForm : AlphaSoft.simpleBrowseForm
    {
        private int originModuleID = 0;
        private globalUserUtil gUtil;
        private globalRestAPI gRest;
        private int paramID = 0;
        private CultureInfo culture = new CultureInfo("id-ID");

        REST_task taskData = new REST_task();

        public string skenarioName = "";
        public int urutanTask = 0;

        public dataBrowse_TaskForm(int moduleID = 0, int paramIDValue = 0)
        {
            InitializeComponent();
            originModuleID = moduleID;

            paramID = paramIDValue;

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();

            ReturnValue1 = "0";
            ReturnValue2 = "0";
        }

        private void dataBrowse_TaskForm_Load(object sender, EventArgs e)
        {
            displayAllAction();
        }

        private void displayDefault()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string paramName = "";

            dataGridView.DataSource = null;

            paramName = getNamaTextBox();

            sqlCommand = "SELECT mt.*, IFNULL(mp.task_name, '-') as task_parent_name, " +
                                    "IFNULL(md.department_name, '-') as department_name " +
                                    "FROM master_task mt " +
                                    "LEFT OUTER JOIN master_task mp ON (mt.task_parent = mp.task_id) " +
                                    "LEFT OUTER JOIN master_department md ON (mt.department_id = md.department_id) " +
                                    "WHERE 1= 1 ";

            if (paramName.Length > 0)
                sqlCommand = sqlCommand + "AND mt.task_name LIKE '%" + paramName + "%' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND mt.is_active = 'Y' ";

            if (originModuleID == globalConstants.MODULE_JOBS)
            {
                sqlCommand += "AND mt.department_id = " + paramID + " ";
                sqlCommand += "AND mt.task_parent = 0 ";
            }

            sqlCommand = sqlCommand + "ORDER BY mt.task_name ASC";

            if (gRest.getTaskData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref taskData))
            {
                dt.Columns.Add("TASK_ID");

                if (originModuleID == globalConstants.MODULE_JOBS)
                    dt.Columns.Add("SKENARIO");

                dt.Columns.Add("NAMA TUGAS");
                dt.Columns.Add("INDUK TUGAS");
                dt.Columns.Add("DURASI TUGAS");
                dt.Columns.Add("DEPARTEMEN");

                for (int i = 0; i < taskData.dataList.Count; i++)
                {
                    DataRow r = dt.Rows.Add();
                    r["TASK_ID"] = taskData.dataList[i].task_id;

                    if (originModuleID == globalConstants.MODULE_JOBS)
                        r["SKENARIO"] = taskData.dataList[i].skenario_name;

                    r["NAMA TUGAS"] = taskData.dataList[i].task_name;
                    r["INDUK TUGAS"] = taskData.dataList[i].task_parent_name;
                    r["DURASI TUGAS"] = taskData.dataList[i].task_duration.ToString("N0", culture);
                    r["DEPARTEMEN"] = taskData.dataList[i].department_name;
                }

                dataGridView.DataSource = dt;
                dataGridView.Columns["TASK_ID"].Visible = false;

                if (originModuleID == globalConstants.MODULE_JOBS)
                {
                    dataGridView.Columns["NAMA TUGAS"].Visible = false;
                    dataGridView.Columns["INDUK TUGAS"].Visible = false;
                    dataGridView.Columns["DURASI TUGAS"].Visible = false;
                    dataGridView.Columns["DEPARTEMEN"].Visible = false;
                }
            }
        }

        private void displaySkenarioSteps()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string paramName = "";

            dataGridView.DataSource = null;

            paramName = getNamaTextBox();

            sqlCommand =
                                    "SELECT mt.*, '-' as task_parent_name, " +
                                    "IFNULL(md.department_name, '-') as department_name " +
                                    "FROM master_task mt " +
                                    "LEFT OUTER JOIN master_department md ON (mt.department_id = md.department_id) " +
                                    "WHERE mt.task_id = " + paramID + " " +
                                    "UNION ALL " +
                                    "SELECT mt.*, IFNULL(mp.task_name, '-') as task_parent_name, " +
                                    "IFNULL(md.department_name, '-') as department_name " +
                                    "FROM master_task mt " +
                                    "LEFT OUTER JOIN master_task mp ON (mt.task_parent = mp.task_id) " +
                                    "LEFT OUTER JOIN master_department md ON (mt.department_id = md.department_id) " +
                                    "WHERE mt.origin_parent_task_id = " + paramID + " "; 

            if (paramName.Length > 0)
                sqlCommand = sqlCommand + "AND mt.task_name LIKE '%" + paramName + "%' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND mt.is_active = 'Y' ";

            sqlCommand = "SELECT * FROM (" + sqlCommand + ") tab " +
                                    "ORDER BY tab.task_name ASC";
            //sqlCommand = sqlCommand + "ORDER BY mt.task_name ASC";

            if (gRest.getTaskData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref taskData))
            {
                dt.Columns.Add("TASK_ID");
                dt.Columns.Add("NO");
                dt.Columns.Add("NAMA TUGAS");
                dt.Columns.Add("INDUK TUGAS");
                dt.Columns.Add("DURASI TUGAS");
                dt.Columns.Add("DEPARTEMEN");

                for (int i = 0; i < taskData.dataList.Count; i++)
                {
                    DataRow r = dt.Rows.Add();
                    r["TASK_ID"] = taskData.dataList[i].task_id;
                    r["NO"] = taskData.dataList[i].urutan;
                    r["NAMA TUGAS"] = taskData.dataList[i].task_name;
                    r["INDUK TUGAS"] = taskData.dataList[i].task_parent_name;
                    r["DURASI TUGAS"] = taskData.dataList[i].task_duration.ToString("N0", culture);
                    r["DEPARTEMEN"] = taskData.dataList[i].department_name;
                }

                dataGridView.DataSource = dt;
                dataGridView.Columns["TASK_ID"].Visible = false;
            }
        }

        protected override void displayAllAction()
        {
            switch (originModuleID)
            {
                case globalConstants.SELECT_START_PROSES:
                    displaySkenarioSteps();
                    break;

                default:
                    displayDefault();
                    break;
            }
        }

        protected override void dbGridDoubleClickAction(DataGridViewRow selectedRow)
        {
            int selectedID = Convert.ToInt32(selectedRow.Cells["TASK_ID"].Value);

            switch (originModuleID)
            {
                case globalConstants.MODULE_DEFAULT:
                case globalConstants.MODULE_JOBS:
                case globalConstants.SELECT_START_PROSES:
                    ReturnValue1 = selectedID.ToString();
                    ReturnValue2 = selectedRow.Cells["NAMA TUGAS"].Value.ToString();

                    if (originModuleID == globalConstants.MODULE_JOBS)
                    {
                        skenarioName = selectedRow.Cells["SKENARIO"].Value.ToString();
                    }
                    else if (originModuleID == globalConstants.SELECT_START_PROSES)
                    {
                        urutanTask = Convert.ToInt32(selectedRow.Cells["NO"].Value);
                    }
                    this.Close();
                    break;

                default:
                    break;
            }
        }
    }
}
