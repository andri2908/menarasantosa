﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class dataBrowse_DepartmentForm : AlphaSoft.simpleBrowseForm
    {
        private int originModuleID = 0;
        private globalUserUtil gUtil;
        private globalRestAPI gRest;
        private int paramID = 0;
     
        
        REST_department deptData = new REST_department();

        public dataBrowse_DepartmentForm(int moduleID = 0, int paramIDValue = 0)
        {
            InitializeComponent();
            originModuleID = moduleID;

            paramID = paramIDValue;

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();

            ReturnValue1 = "0";
            ReturnValue2 = "0";
        }

        
        private void dataBrowse_DepartmentForm_Load(object sender, EventArgs e)
        {
            displayAllAction();

            //if (!gUtil.RS_userHasAccessTo(MMConstants.TAMBAH_HAPUS_USER,
            //    MMConstants.HAK_NEW_DATA))
            //    setNewButtonEnable(false);
        }

        private void displayDefault()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string paramName = "";

            dataGridView.DataSource = null;

            paramName = getNamaTextBox();

            sqlCommand = "SELECT md.* " +
                                    "FROM master_department md " +
                                    "WHERE 1= 1 ";

            if (paramName.Length > 0)
                sqlCommand = sqlCommand + "AND md.department_name LIKE '%" + paramName + "%' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND md.is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY md.department_name ASC";

            if (gRest.getDepartmentData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref deptData))
            {
                dt.Columns.Add("DEPARTMENT_ID");
                dt.Columns.Add("NAMA DEPARTEMEN");
                dt.Columns.Add("DESKRIPSI DEPARTEMEN");

                for (int i = 0; i < deptData.dataList.Count; i++)
                {
                    DataRow r = dt.Rows.Add();
                    r["DEPARTMENT_ID"] = deptData.dataList[i].department_id;
                    r["NAMA DEPARTEMEN"] = deptData.dataList[i].department_name;
                    r["DESKRIPSI DEPARTEMEN"] = deptData.dataList[i].department_description;
                }

                dataGridView.DataSource = dt;
                dataGridView.Columns["DEPARTMENT_ID"].Visible = false;
            }
        }

        private void displayUserDept()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string paramName = "";

            dataGridView.DataSource = null;

            paramName = getNamaTextBox();

            sqlCommand = "SELECT md.* " +
                                    "FROM master_department md, user_department ud " +
                                    "WHERE ud.department_id = md.department_id " +
                                    "AND ud.user_id = " + paramID + " " +
                                    "AND ud.is_active = 'Y' ";

            if (paramName.Length > 0)
                sqlCommand = sqlCommand + "AND md.department_name LIKE '%" + paramName + "%' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND md.is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY md.department_name ASC";

            if (gRest.getDepartmentData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref deptData))
            {
                dt.Columns.Add("DEPARTMENT_ID");
                dt.Columns.Add("NAMA DEPARTEMEN");
                dt.Columns.Add("DESKRIPSI DEPARTEMEN");

                for (int i = 0; i < deptData.dataList.Count; i++)
                {
                    DataRow r = dt.Rows.Add();
                    r["DEPARTMENT_ID"] = deptData.dataList[i].department_id;
                    r["NAMA DEPARTEMEN"] = deptData.dataList[i].department_name;
                    r["DESKRIPSI DEPARTEMEN"] = deptData.dataList[i].department_description;
                }

                dataGridView.DataSource = dt;
                dataGridView.Columns["DEPARTMENT_ID"].Visible = false;
            }
        }

        protected override void displayAllAction()
        {
            switch (originModuleID)
            {
                case globalConstants.MODULE_JOBS:
                    displayUserDept();
                    break;

                default:
                    displayDefault();
                    break;
            }
        }

        protected override void dbGridDoubleClickAction(DataGridViewRow selectedRow)
        {
            int selectedID = Convert.ToInt32(selectedRow.Cells["DEPARTMENT_ID"].Value);

            switch (originModuleID)
            {
                case globalConstants.MODULE_JOBS:          
                case globalConstants.MODULE_DEFAULT:
                    ReturnValue1 = selectedID.ToString();
                    ReturnValue2 = selectedRow.Cells["NAMA DEPARTEMEN"].Value.ToString();

                    this.Close();
                    break;

                default:
                    break;
            }
        }

        private void dataBrowse_DepartmentForm_Activated(object sender, EventArgs e)
        {
        }
    }
}
