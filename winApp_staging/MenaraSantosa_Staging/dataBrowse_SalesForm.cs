﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class dataBrowse_SalesForm : AlphaSoft.simpleBrowseForm
    {
        private int originModuleID = 0;
        private globalUserUtil gUtil;
        private globalRestAPI gRest;

        public REST_sales salesData = new REST_sales();

        public dataBrowse_SalesForm(int moduleID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;

            gUtil = new globalUserUtil();
            gRest = new globalRestAPI();

            this.Text = "BROWSE SALES";
        }

        private void dataBrowse_SalesForm_Load(object sender, EventArgs e)
        {
            displayAllAction();
        }

        protected override void displayAllAction()
        {
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string paramName = "";

            dataGridView.DataSource = null;

            paramName = getNamaTextBox();

            sqlCommand = "SELECT * " +
                                    "FROM master_sales " +
                                    "WHERE 1 = 1 ";

            if (paramName.Length > 0)
                sqlCommand = sqlCommand + "AND sales_name LIKE '%" + paramName + "%' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND is_active = 'Y' ";

            sqlCommand = sqlCommand + "ORDER BY sales_name ASC";

            if (gRest.getMasterSalesData(gUtil.getUserID(), gUtil.getUserToken(), sqlCommand, ref salesData))
            {
                dt.Columns.Add("SALES_ID");
                dt.Columns.Add("NAMA");
                dt.Columns.Add("DESKRIPSI");

                for (int i = 0; i < salesData.dataList.Count; i++)
                {
                    DataRow r = dt.Rows.Add();
                    r["SALES_ID"] = salesData.dataList[i].sales_id;
                    r["NAMA"] = salesData.dataList[i].sales_name;
                    r["DESKRIPSI"] = salesData.dataList[i].sales_description;
                }

                dataGridView.DataSource = dt;
                dataGridView.Columns["SALES_ID"].Visible = false;
            }
        }

        protected override void dbGridDoubleClickAction(DataGridViewRow selectedRow)
        {
            int selectedID = Convert.ToInt32(selectedRow.Cells["SALES_ID"].Value);

            switch (originModuleID)
            {
                case globalConstants.MODULE_DEFAULT:
                    ReturnValue1 = selectedRow.Cells["SALES_ID"].Value.ToString();
                    ReturnValue2 = selectedRow.Cells["NAMA"].Value.ToString();

                    this.Close();
                    break;

                default:
                    dataSalesForm editForm = new dataSalesForm(selectedID);
                    editForm.ShowDialog(this);
                    break;
            }
        }

        protected override void newButtonAction()
        {
            dataSalesForm displayForm = new dataSalesForm();
            displayForm.ShowDialog(this);
        }
    }
}
