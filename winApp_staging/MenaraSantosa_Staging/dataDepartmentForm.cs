﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace AlphaSoft
{
    public partial class dataDepartmentForm : basic2TextBoxInputsForm
    {
        private int originModuleID = 0;
        private int deptID = 0;

        private globalUserUtil gUser = new globalUserUtil();
        private globalRestAPI gRest = new globalRestAPI();
        private CultureInfo culture = new CultureInfo("id-ID");

        List<cbDataSource> cbType = new List<cbDataSource>();

        public REST_department departmentData = new REST_department();

        public int returnGroupUserIDValue = 0;

        public dataDepartmentForm(int deptIDParam = 0)
        {
            InitializeComponent();

            if (deptIDParam > 0)
            {
                deptID = deptIDParam;
                originModuleID = globalConstants.EDIT_DEPARTMENT;
            }
            else
            {
                originModuleID = globalConstants.NEW_DEPARTMENT;
            }
        }

        private void loadDeptDataInformation()
        {
            DataTable dt = new DataTable();

            string sqlCommand = "SELECT * FROM master_department WHERE department_id = " + deptID;

            if (gRest.getDepartmentData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref departmentData))
            {
                if (departmentData.dataStatus.o_status == 1)
                {
                    namaTextBox.Text = departmentData.dataList[0].department_name;
                    deskripsiTextBox.Text = departmentData.dataList[0].department_description;
                    nonAktifCheckbox.Checked = (departmentData.dataList[0].is_active == "Y" ? false : true);
                }
            }
        }

        private void dataDepartmentForm_Load(object sender, EventArgs e)
        {
            if (deptID > 0)
            {
                loadDeptDataInformation();
            }

            namaTextBox.Focus();
            gUser.reArrangeTabOrder(this);
        }

        protected override bool saveDataTransaction()
        {
            bool result = false;
            int opType = 0;
            string errMsg = "";

            List<master_department> listDeptData = new List<master_department>();
            master_department deptData = new master_department();

            try
            {
                deptData.department_name= namaTextBox.Text;
                deptData.department_description = deskripsiTextBox.Text;
                deptData.is_active = (nonAktifCheckbox.Checked ? "N" : "Y");

                switch (originModuleID)
                {
                    case globalConstants.NEW_DEPARTMENT:
                        opType = 1;
                        deptData.department_id = 0;
                        break;

                    case globalConstants.EDIT_DEPARTMENT:
                        opType = 2;
                        deptData.department_id = deptID;
                        break;
                }

                listDeptData.Add(deptData);

                int newID = 0;
                if (!gRest.saveDepartment(gUser.getUserID(), gUser.getUserToken(), listDeptData, opType, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                deptID = newID;

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        protected override bool dataValidated()
        {
            errorLabel.Text = "";

            bool result = false;

            result = base.dataValidated();

            if (!result)
                return false;

            return true;
        }
    }
}
