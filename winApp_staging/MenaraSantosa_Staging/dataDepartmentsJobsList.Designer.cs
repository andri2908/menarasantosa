﻿namespace AlphaSoft
{
    partial class dataDepartmentJobs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dataDepartmentJobs));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonExit = new System.Windows.Forms.Button();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.exitPreferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jobCountLabel = new System.Windows.Forms.Label();
            this.departmentLabel = new System.Windows.Forms.Label();
            this.jobGridView = new System.Windows.Forms.DataGridView();
            this.jobID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.no = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taskStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jobName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.custName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.waktu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isBlink = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timerScroll = new System.Windows.Forms.Timer(this.components);
            this.labelPage = new System.Windows.Forms.Label();
            this.timerBlink = new System.Windows.Forms.Timer(this.components);
            this.checkBoxPending = new System.Windows.Forms.CheckBox();
            this.checkBoxDitolak = new System.Windows.Forms.CheckBox();
            this.checkBoxSelesai = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.jobGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.departmentLabel);
            this.panel1.Controls.Add(this.jobCountLabel);
            this.panel1.Controls.Add(this.buttonExit);
            this.panel1.Size = new System.Drawing.Size(1061, 29);
            this.panel1.Controls.SetChildIndex(this.errorLabel, 0);
            this.panel1.Controls.SetChildIndex(this.buttonExit, 0);
            this.panel1.Controls.SetChildIndex(this.jobCountLabel, 0);
            this.panel1.Controls.SetChildIndex(this.departmentLabel, 0);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // buttonExit
            // 
            this.buttonExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonExit.BackgroundImage")));
            this.buttonExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonExit.Location = new System.Drawing.Point(1035, 3);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(23, 23);
            this.buttonExit.TabIndex = 104;
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitPreferencesToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(68, 26);
            // 
            // exitPreferencesToolStripMenuItem
            // 
            this.exitPreferencesToolStripMenuItem.Name = "exitPreferencesToolStripMenuItem";
            this.exitPreferencesToolStripMenuItem.Size = new System.Drawing.Size(67, 22);
            // 
            // jobCountLabel
            // 
            this.jobCountLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.jobCountLabel.AutoSize = true;
            this.jobCountLabel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobCountLabel.Location = new System.Drawing.Point(891, 7);
            this.jobCountLabel.Name = "jobCountLabel";
            this.jobCountLabel.Size = new System.Drawing.Size(138, 16);
            this.jobCountLabel.TabIndex = 105;
            this.jobCountLabel.Text = "Jumlah Tugas : xx";
            // 
            // departmentLabel
            // 
            this.departmentLabel.AutoSize = true;
            this.departmentLabel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.departmentLabel.Location = new System.Drawing.Point(9, 5);
            this.departmentLabel.Name = "departmentLabel";
            this.departmentLabel.Size = new System.Drawing.Size(159, 16);
            this.departmentLabel.TabIndex = 106;
            this.departmentLabel.Text = "[DEPARTMENT NAME]";
            // 
            // jobGridView
            // 
            this.jobGridView.AllowUserToAddRows = false;
            this.jobGridView.AllowUserToDeleteRows = false;
            this.jobGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.jobGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.jobGridView.BackgroundColor = System.Drawing.Color.FloralWhite;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.jobGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.jobGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.jobGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.jobID,
            this.id,
            this.no,
            this.taskStatus,
            this.jobName,
            this.salesName,
            this.custName,
            this.waktu,
            this.isBlink});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.jobGridView.DefaultCellStyle = dataGridViewCellStyle3;
            this.jobGridView.Location = new System.Drawing.Point(12, 63);
            this.jobGridView.MultiSelect = false;
            this.jobGridView.Name = "jobGridView";
            this.jobGridView.ReadOnly = true;
            this.jobGridView.RowHeadersVisible = false;
            this.jobGridView.RowTemplate.Height = 30;
            this.jobGridView.Size = new System.Drawing.Size(1037, 465);
            this.jobGridView.TabIndex = 66;
            this.jobGridView.DoubleClick += new System.EventHandler(this.jobGridView_DoubleClick);
            // 
            // jobID
            // 
            this.jobID.HeaderText = "jobID";
            this.jobID.Name = "jobID";
            this.jobID.ReadOnly = true;
            this.jobID.Visible = false;
            this.jobID.Width = 63;
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            this.id.Width = 29;
            // 
            // no
            // 
            this.no.HeaderText = "No";
            this.no.Name = "no";
            this.no.ReadOnly = true;
            this.no.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.no.Width = 38;
            // 
            // taskStatus
            // 
            this.taskStatus.HeaderText = "Status";
            this.taskStatus.Name = "taskStatus";
            this.taskStatus.ReadOnly = true;
            this.taskStatus.Width = 89;
            // 
            // jobName
            // 
            this.jobName.HeaderText = "Tugas";
            this.jobName.Name = "jobName";
            this.jobName.ReadOnly = true;
            this.jobName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.jobName.Width = 68;
            // 
            // salesName
            // 
            this.salesName.HeaderText = "Sales";
            this.salesName.Name = "salesName";
            this.salesName.ReadOnly = true;
            this.salesName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.salesName.Width = 60;
            // 
            // custName
            // 
            this.custName.HeaderText = "Customer";
            this.custName.Name = "custName";
            this.custName.ReadOnly = true;
            this.custName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.custName.Width = 99;
            // 
            // waktu
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.waktu.DefaultCellStyle = dataGridViewCellStyle2;
            this.waktu.HeaderText = "Waktu";
            this.waktu.Name = "waktu";
            this.waktu.ReadOnly = true;
            this.waktu.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.waktu.Width = 71;
            // 
            // isBlink
            // 
            this.isBlink.HeaderText = "isBlink";
            this.isBlink.Name = "isBlink";
            this.isBlink.ReadOnly = true;
            this.isBlink.Visible = false;
            this.isBlink.Width = 87;
            // 
            // timerScroll
            // 
            this.timerScroll.Interval = 1000;
            this.timerScroll.Tick += new System.EventHandler(this.timerScroll_Tick);
            // 
            // labelPage
            // 
            this.labelPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPage.AutoSize = true;
            this.labelPage.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPage.ForeColor = System.Drawing.Color.FloralWhite;
            this.labelPage.Location = new System.Drawing.Point(897, 531);
            this.labelPage.Name = "labelPage";
            this.labelPage.Size = new System.Drawing.Size(132, 16);
            this.labelPage.TabIndex = 106;
            this.labelPage.Text = "Hal xxx from xxx";
            // 
            // timerBlink
            // 
            this.timerBlink.Interval = 1000;
            this.timerBlink.Tick += new System.EventHandler(this.timerBlink_Tick);
            // 
            // checkBoxPending
            // 
            this.checkBoxPending.AutoSize = true;
            this.checkBoxPending.Checked = true;
            this.checkBoxPending.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPending.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxPending.ForeColor = System.Drawing.Color.FloralWhite;
            this.checkBoxPending.Location = new System.Drawing.Point(96, 35);
            this.checkBoxPending.Name = "checkBoxPending";
            this.checkBoxPending.Size = new System.Drawing.Size(103, 22);
            this.checkBoxPending.TabIndex = 107;
            this.checkBoxPending.Text = "Diproses";
            this.checkBoxPending.UseVisualStyleBackColor = true;
            this.checkBoxPending.CheckedChanged += new System.EventHandler(this.checkBoxPending_CheckedChanged);
            // 
            // checkBoxDitolak
            // 
            this.checkBoxDitolak.AutoSize = true;
            this.checkBoxDitolak.Checked = true;
            this.checkBoxDitolak.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDitolak.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxDitolak.ForeColor = System.Drawing.Color.FloralWhite;
            this.checkBoxDitolak.Location = new System.Drawing.Point(220, 35);
            this.checkBoxDitolak.Name = "checkBoxDitolak";
            this.checkBoxDitolak.Size = new System.Drawing.Size(87, 22);
            this.checkBoxDitolak.TabIndex = 108;
            this.checkBoxDitolak.Text = "Ditolak";
            this.checkBoxDitolak.UseVisualStyleBackColor = true;
            this.checkBoxDitolak.CheckedChanged += new System.EventHandler(this.checkBoxDitolak_CheckedChanged);
            // 
            // checkBoxSelesai
            // 
            this.checkBoxSelesai.AutoSize = true;
            this.checkBoxSelesai.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxSelesai.ForeColor = System.Drawing.Color.FloralWhite;
            this.checkBoxSelesai.Location = new System.Drawing.Point(338, 35);
            this.checkBoxSelesai.Name = "checkBoxSelesai";
            this.checkBoxSelesai.Size = new System.Drawing.Size(88, 22);
            this.checkBoxSelesai.TabIndex = 109;
            this.checkBoxSelesai.Text = "Selesai";
            this.checkBoxSelesai.UseVisualStyleBackColor = true;
            this.checkBoxSelesai.CheckedChanged += new System.EventHandler(this.checkBoxSelesai_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FloralWhite;
            this.label1.Location = new System.Drawing.Point(12, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 18);
            this.label1.TabIndex = 110;
            this.label1.Text = "Status";
            // 
            // dataDepartmentJobs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1061, 575);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBoxSelesai);
            this.Controls.Add(this.checkBoxDitolak);
            this.Controls.Add(this.checkBoxPending);
            this.Controls.Add(this.labelPage);
            this.Controls.Add(this.jobGridView);
            this.Name = "dataDepartmentJobs";
            this.Text = "DAFTAR TUGAS";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.dataDepartmentJobs_FormClosed);
            this.Load += new System.EventHandler(this.dataDepartmentJobs_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.jobGridView, 0);
            this.Controls.SetChildIndex(this.labelPage, 0);
            this.Controls.SetChildIndex(this.checkBoxPending, 0);
            this.Controls.SetChildIndex(this.checkBoxDitolak, 0);
            this.Controls.SetChildIndex(this.checkBoxSelesai, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.contextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.jobGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem exitPreferencesToolStripMenuItem;
        private System.Windows.Forms.Label jobCountLabel;
        private System.Windows.Forms.Label departmentLabel;
        protected System.Windows.Forms.DataGridView jobGridView;
        private System.Windows.Forms.Timer timerScroll;
        private System.Windows.Forms.Label labelPage;
        private System.Windows.Forms.Timer timerBlink;
        private System.Windows.Forms.CheckBox checkBoxPending;
        private System.Windows.Forms.CheckBox checkBoxDitolak;
        private System.Windows.Forms.CheckBox checkBoxSelesai;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn jobID;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn no;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn jobName;
        private System.Windows.Forms.DataGridViewTextBoxColumn salesName;
        private System.Windows.Forms.DataGridViewTextBoxColumn custName;
        private System.Windows.Forms.DataGridViewTextBoxColumn waktu;
        private System.Windows.Forms.DataGridViewTextBoxColumn isBlink;
    }
}
