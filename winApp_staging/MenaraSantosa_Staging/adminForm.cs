﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class adminForm : Form
    {
        private globalDeptUtil gDept = new globalDeptUtil();
        private List<int> departmentID = new List<int>();
        public int moduleID = 0;
        public int deptID = 0;

        public adminForm(int modParam = 0)
        {
            InitializeComponent();
            departmentID.Clear();

            moduleID = modParam;
        }

        public void addDepartmentIDToList(int deptID)
        {
            departmentID.Add(deptID);
        }

        public void removeDeptIDfromList(int deptID)
        {
            departmentID.Remove(deptID);
        }

        public bool deptIDExist(int deptID)
        {
            bool result = false;

            int idx = departmentID.IndexOf(deptID);

            if (idx >= 0)
                result = true;

            return result;
        }

        public void refreshAllDisplay()
        {
            for (int i = 0;i<this.MdiChildren.Length;i++)
            {
                dataDepartmentJobs mdiForm = (dataDepartmentJobs)this.MdiChildren[i];
                mdiForm.loadDepartmentJobs();
            }
        }

        private void doDisplayDeptList()
        {
            int deptCount = 1;

            deptCount = gDept.getNumOfDept();

            if (deptCount <= 0)
            {
                MessageBox.Show("User tidak di assign ke departemen apapun", "WARNING");
                this.Close();
            }
            else if (deptCount == 1) // KONDISI PASTI TIDAK ADA FORM YG TERBUKA
            {
                deptID = gDept.getDeptID();
                dataDepartmentJobs displayJobs = new dataDepartmentJobs(this, deptID);
                displayJobs.MdiParent = this;
                displayJobs.Show();
            }
            else
            {
                dataBrowse_multipleDepartmentForm displayDept = new dataBrowse_multipleDepartmentForm(this);
                displayDept.ShowDialog(this);

                if (displayDept.deptList.Count > 0)
                {
                    for (int i = 0;i<displayDept.deptList.Count;i++)
                    {
                        dataDepartmentJobs displayMultiJobs = new dataDepartmentJobs(this, displayDept.deptList[i]);
                        displayMultiJobs.MdiParent = this;
                        displayMultiJobs.Show();
                    }
                }
                else
                {
                    this.Close();
                }
            }
        }

        private void adminForm_Load(object sender, EventArgs e)
        {
            doDisplayDeptList();
         
            this.LayoutMdi(System.Windows.Forms.MdiLayout.TileHorizontal);
        }
    }
}
