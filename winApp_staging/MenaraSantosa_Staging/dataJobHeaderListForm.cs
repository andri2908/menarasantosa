﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class dataJobHeaderListForm : AlphaSoft.basicHotkeysForm
    {
        private globalRestAPI gRest = new globalRestAPI();
        private globalUserUtil gUser = new globalUserUtil();

        public dataJobHeaderListForm()
        {
            InitializeComponent();
        }

        private void loadJobHeaderList()
        {
            string sqlCommand = "";
            REST_displayJobs jobsList = new REST_displayJobs();

            string statusValue = "";

            if (checkBoxPending.Checked)
                statusValue += ", 'PENDING'";

            if (checkBoxDitolak.Checked)
                statusValue += ", 'REJECTED'";

            if (checkBoxSelesai.Checked)
                statusValue += ", 'FINISH'";

            string sqlStatusChild = "SELECT mt.origin_parent_task_id, COUNT(1) as numTask " +
                                            "FROM data_jobs_detail jd, master_task mt " +
                                            "WHERE jd.task_id = mt.task_id " +
                                            "AND jd.task_status <> 'FINISH' " +
                                            "GROUP by mt.origin_parent_task_id";

            sqlCommand = "SELECT IF(jd.task_status = 'FINISH', IF(IFNULL(child.numTask, 0) <= 0, 'FINISH', 'PENDING'), jd.task_status) as task_status, jd.job_id, jd.id, mt.skenario_name, mt.task_name, mt.task_duration, jd.task_start, " + //jd.task_status, " +
                                     "ml.kompleks_name, mb.blok_name, mk.house_no, ms.sales_name, mc.customer_name, " +
                                     "md.department_name, jh.department_id " +
                                     "FROM data_jobs_detail jd, " +
                                     "master_task mt, " +
                                     "data_jobs_header jh " +
                                     "LEFT OUTER JOIN master_kompleks ml ON (ml.kompleks_id = jh.kompleks_id) " +
                                     "LEFT OUTER JOIN master_kavling mk ON (mk.kavling_id = jh.kavling_id) " +
                                     "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id) " +
                                     "LEFT OUTER JOIN master_sales ms ON (jh.sales_id = ms.sales_id) " +
                                     "LEFT OUTER JOIN master_customer mc ON (jh.customer_id = mc.customer_id) " +
                                     "LEFT OUTER JOIN master_department md ON (jh.department_id = md.department_id) " +
                                     "LEFT OUTER JOIN (" + sqlStatusChild  + ") child ON (jh.task_id = child.origin_parent_task_id) " +
                                     "WHERE jh.is_active = 'Y' " +
                                     "AND jd.job_id = jh.job_id " +
                                     "AND jd.task_id = mt.task_id " +
                                    //"AND jd.task_status in ('TEMP'" + statusValue + ") " + // add default value supaya tidak error query-nya jika tidak ada status dipilih
                                    "AND IF(jd.task_status = 'FINISH', IF(IFNULL(child.numTask, 0) <= 0, 'FINISH', 'PENDING'), jd.task_status) in ('TEMP'" + statusValue + ") " + 
                                    "AND mt.task_parent = 0 " +
                                     "ORDER BY jh.last_update_date DESC";

            if (gRest.getJobsList(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref jobsList))
            {
                jobGridView.Rows.Clear();
                for (int i = 0; i < jobsList.dataList.Count; i++)
                {
                    jobGridView.Rows.Add(
                         jobsList.dataList[i].job_id,
                         jobsList.dataList[i].department_id,
                         (i +1).ToString().PadLeft(2, '0'),
                         jobsList.dataList[i].skenario_name,
                         jobsList.dataList[i].department_name,
                         (jobsList.dataList[i].task_status == "PENDING" ? "Diproses" : (jobsList.dataList[i].task_status == "REJECTED" ? "Ditolak" : "Selesai")),
                         //(jobsList.dataList[i].task_status == "PENDING" ? "Diproses" : (jobsList.dataList[i].task_status == "REJECTED" ? "Ditolak" : (jobsList.dataList[i].numTask > 0 ? "Diproses" : "Selesai"))),
                         "[" + jobsList.dataList[i].kompleks_name + " - " + jobsList.dataList[i].blok_name + " " + jobsList.dataList[i].house_no + "] " + jobsList.dataList[i].task_name,
                         jobsList.dataList[i].sales_name,
                         jobsList.dataList[i].customer_name
                    );
                }
            }
        }

        private void dataJobHeaderListForm_Load(object sender, EventArgs e)
        {
            loadJobHeaderList();
        }

        private void jobGridView_DoubleClick(object sender, EventArgs e)
        {
            if (jobGridView.Rows.Count <= 0)
                return;

            int rIndex = jobGridView.SelectedCells[0].RowIndex;
            DataGridViewRow sRow = jobGridView.Rows[rIndex];

            int deptID = Convert.ToInt32(sRow.Cells["departmentID"].Value);
            string departmentName = sRow.Cells["departemen"].Value.ToString();
            int jobID = Convert.ToInt32(sRow.Cells["jobID"].Value);

            dataJobsForm displayForm = new dataJobsForm(deptID, departmentName, jobID);
            displayForm.ShowDialog(this);

            loadJobHeaderList();
        }

        private void checkBoxPending_CheckedChanged(object sender, EventArgs e)
        {
            loadJobHeaderList();
        }

        private void checkBoxDitolak_CheckedChanged(object sender, EventArgs e)
        {
            loadJobHeaderList();
        }

        private void checkBoxSelesai_CheckedChanged(object sender, EventArgs e)
        {
            loadJobHeaderList();
        }
    }
}
