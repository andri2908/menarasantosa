﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using MySql.Data;
using MySql.Data.MySqlClient;

namespace AlphaSoft
{
    public partial class dataPMForm : AlphaSoft.simpleBrowseForm
    {
        private Data_Access DS = new Data_Access();
        private int originModuleID = 0;
        private globalUserUtil gUser;

        public dataPMForm(int moduleID = 0)
        {
            InitializeComponent();
            originModuleID = moduleID;

            this.Text = "BROWSE DATA PAYMENT METHOD";
            DS.sqlConnect();

            gUser = new globalUserUtil(DS);

            ReturnValue1 = "0";
            ReturnValue2 = "0";
        }

        protected override void displayAllAction()
        {
            MySqlDataReader rdr;
            DataTable dt = new DataTable();
            string sqlCommand = "";
            string paramName = "";

            dataGridView.DataSource = null;

            paramName = MySqlHelper.EscapeString(getNamaTextBox());

            sqlCommand = "SELECT MP.PM_ID, MP.PM_NAME AS 'NAMA' " +
                                    "FROM MASTER_PAYMENT_METHOD MP " +
                                    "WHERE 1 = 1 ";

            if (paramName.Length > 0)
                sqlCommand = sqlCommand + "AND MP.PM_NAME LIKE '%" + paramName + "%' ";

            if (!nonActiveIncluded())
                sqlCommand = sqlCommand + "AND MP.PM_ACTIVE = 1 ";

            sqlCommand = sqlCommand + "ORDER BY MP.PM_NAME ASC";

            using (rdr = DS.getData(sqlCommand))
            {
                if (rdr.HasRows)
                {
                    dt.Load(rdr);
                    dataGridView.DataSource = dt;

                    dataGridView.Columns["PM_ID"].Visible = false;
                }
            }
        }

        private void dataPMForm_Load(object sender, EventArgs e)
        {
            displayAllAction();

            if (!gUser.userHasAccessTo(MMConstants.PENGATURAN_PAYMENT_METHOD,
                MMConstants.HAK_NEW_DATA))
                setNewButtonEnable(false);
        }

        protected override void newButtonAction()
        {
            dataPMDetailForm displayForm = new dataPMDetailForm(globalConstants.NEW_PM);
            displayForm.ShowDialog(this);
        }

        protected override void dbGridDoubleClickAction(DataGridViewRow selectedRow)
        {
            int selectedID = Convert.ToInt32(selectedRow.Cells["PM_ID"].Value);

            switch (originModuleID)
            {
                case globalConstants.MODULE_DEFAULT:
                    ReturnValue1 = selectedRow.Cells["PM_ID"].Value.ToString();
                    ReturnValue2 = selectedRow.Cells["NAMA"].Value.ToString();

                    this.Close();
                    break;

                default:
                    dataPMDetailForm editForm = new dataPMDetailForm(globalConstants.EDIT_PM, selectedID);
                    editForm.ShowDialog(this);
                    break;
            }
        }
    }
}
