﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Globalization;

namespace AlphaSoft
{
    public partial class dataCustomerForm : AlphaSoft.basic2TextBoxInputsForm
    {
        private int originModuleID = 0;
        private int selectedCustID = 0;

        private globalUserUtil gUser = new globalUserUtil();
        private globalRestAPI gRest = new globalRestAPI();
        private CultureInfo culture = new CultureInfo("id-ID");

        public REST_customer custData = new REST_customer();

        public int returnCustIDValue = 0;

        public dataCustomerForm(int custID = 0)
        {
            InitializeComponent();

            this.Text = "DATA CUSTOMER";

            selectedCustID = custID;

            if (selectedCustID > 0)
            {
                originModuleID = globalConstants.EDIT_CUSTOMER;

                //if (!gUser.RS_userHasAccessTo(MMConstants.TAMBAH_HAPUS_GROUP_USER,
                //MMConstants.HAK_UPDATE_DATA))
                //{
                //    setSaveButtonEnable(false);
                //    setResetButtonEnable(false);
                //}
            }
            else
                originModuleID = globalConstants.NEW_CUSTOMER;
        }

        private void dataCustomerForm_Load(object sender, EventArgs e)
        {
            if (selectedCustID > 0)
            {
                loadCustDataInformation();
            }

            namaTextBox.Focus();
            gUser.reArrangeTabOrder(this);
        }

        private void dataCustomerForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            returnCustIDValue = selectedCustID;
        }

        private void loadCustDataInformation()
        {
            DataTable dt = new DataTable();

            string sqlCommand = "SELECT * FROM master_customer WHERE customer_id = " + selectedCustID;

            gRest.getMasterCustomerData(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref custData);

            if (custData.dataStatus.o_status == 1)
            {
                namaTextBox.Text = custData.dataList[0].customer_name;
                deskripsiTextBox.Text = custData.dataList[0].customer_description;
                nonAktifCheckbox.Checked = (custData.dataList[0].is_active == "Y" ? false : true);
            }
        }

        protected override bool saveDataTransaction()
        {
            bool result = false;
            int opType = 0;
            string errMsg = "";

            List<master_customer> listCustData = new List<master_customer>();
            master_customer custData = new master_customer();

            try
            {
                custData.customer_name= namaTextBox.Text;
                custData.customer_description= deskripsiTextBox.Text;
                custData.is_active = (nonAktifCheckbox.Checked ? "N" : "Y");

                switch (originModuleID)
                {
                    case globalConstants.NEW_CUSTOMER:
                        opType = 1;
                        custData.customer_id = 0;
                        break;

                    case globalConstants.EDIT_CUSTOMER:
                        opType = 2;
                        custData.customer_id = selectedCustID;
                        break;
                }

                listCustData.Add(custData);

                int newID = 0;
                if (!gRest.saveCustomer(gUser.getUserID(), gUser.getUserToken(), listCustData, opType, out errMsg, out newID))
                {
                    throw new Exception(errMsg);
                }

                selectedCustID = newID;

                result = true;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
            }

            return result;
        }

        protected override bool dataValidated()
        {
            errorLabel.Text = "";

            bool result = false;

            result = base.dataValidated();

            if (!result)
                return false;

            return true;
        }

        protected override void resetProcess()
        {
            base.resetProcess();

            originModuleID = globalConstants.NEW_CUSTOMER;
            selectedCustID = 0;
        }

    }
}
