﻿namespace AlphaSoft
{
    partial class dataJobsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.startProsesTextBox = new System.Windows.Forms.TextBox();
            this.buttonSearchProses = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.skenarioTextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.buttonPreview = new System.Windows.Forms.Button();
            this.jobsDescription = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.searchTask = new System.Windows.Forms.Button();
            this.taskTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.promoTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.realisasiDTPicker = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.deadlineDTPicker = new System.Windows.Forms.DateTimePicker();
            this.ppnCheckBox = new System.Windows.Forms.CheckBox();
            this.comboBayar = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lbTextBox = new System.Windows.Forms.TextBox();
            this.searchCustomer = new System.Windows.Forms.Button();
            this.customerTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.searchSales = new System.Windows.Forms.Button();
            this.salesTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.searchKavling = new System.Windows.Forms.Button();
            this.kavlingTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.nonAktifCheckbox = new System.Windows.Forms.CheckBox();
            this.searchDenah = new System.Windows.Forms.Button();
            this.denahTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.searchKompleks = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.kompleksTextBox = new System.Windows.Forms.TextBox();
            this.ltTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.openPDFDialog = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(821, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox1.Controls.Add(this.startProsesTextBox);
            this.groupBox1.Controls.Add(this.buttonSearchProses);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.skenarioTextBox);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.buttonPreview);
            this.groupBox1.Controls.Add(this.jobsDescription);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.searchTask);
            this.groupBox1.Controls.Add(this.taskTextBox);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.promoTextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.realisasiDTPicker);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.deadlineDTPicker);
            this.groupBox1.Controls.Add(this.ppnCheckBox);
            this.groupBox1.Controls.Add(this.comboBayar);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lbTextBox);
            this.groupBox1.Controls.Add(this.searchCustomer);
            this.groupBox1.Controls.Add(this.customerTextBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.searchSales);
            this.groupBox1.Controls.Add(this.salesTextBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.searchKavling);
            this.groupBox1.Controls.Add(this.kavlingTextBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.nonAktifCheckbox);
            this.groupBox1.Controls.Add(this.searchDenah);
            this.groupBox1.Controls.Add(this.denahTextBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.searchKompleks);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.kompleksTextBox);
            this.groupBox1.Controls.Add(this.ltTextBox);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(20, 46);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(778, 632);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            // 
            // startProsesTextBox
            // 
            this.startProsesTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startProsesTextBox.Location = new System.Drawing.Point(135, 116);
            this.startProsesTextBox.MaxLength = 50;
            this.startProsesTextBox.Name = "startProsesTextBox";
            this.startProsesTextBox.ReadOnly = true;
            this.startProsesTextBox.Size = new System.Drawing.Size(539, 27);
            this.startProsesTextBox.TabIndex = 99;
            // 
            // buttonSearchProses
            // 
            this.buttonSearchProses.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonSearchProses.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSearchProses.Location = new System.Drawing.Point(682, 114);
            this.buttonSearchProses.Name = "buttonSearchProses";
            this.buttonSearchProses.Size = new System.Drawing.Size(77, 30);
            this.buttonSearchProses.TabIndex = 98;
            this.buttonSearchProses.Text = "Cari";
            this.buttonSearchProses.UseVisualStyleBackColor = true;
            this.buttonSearchProses.Click += new System.EventHandler(this.buttonSearchProses_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label15.Location = new System.Drawing.Point(17, 119);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(111, 18);
            this.label15.TabIndex = 97;
            this.label15.Text = "Start Tugas";
            // 
            // skenarioTextBox
            // 
            this.skenarioTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.skenarioTextBox.Location = new System.Drawing.Point(135, 18);
            this.skenarioTextBox.MaxLength = 50;
            this.skenarioTextBox.Name = "skenarioTextBox";
            this.skenarioTextBox.ReadOnly = true;
            this.skenarioTextBox.Size = new System.Drawing.Size(539, 27);
            this.skenarioTextBox.TabIndex = 95;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label14.Location = new System.Drawing.Point(43, 21);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 18);
            this.label14.TabIndex = 96;
            this.label14.Text = "Skenario";
            // 
            // buttonPreview
            // 
            this.buttonPreview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonPreview.Enabled = false;
            this.buttonPreview.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPreview.Location = new System.Drawing.Point(584, 427);
            this.buttonPreview.Name = "buttonPreview";
            this.buttonPreview.Size = new System.Drawing.Size(77, 30);
            this.buttonPreview.TabIndex = 94;
            this.buttonPreview.Text = "Preview";
            this.buttonPreview.UseVisualStyleBackColor = true;
            this.buttonPreview.Click += new System.EventHandler(this.buttonPreview_Click);
            // 
            // jobsDescription
            // 
            this.jobsDescription.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobsDescription.Location = new System.Drawing.Point(135, 52);
            this.jobsDescription.MaxLength = 200;
            this.jobsDescription.Multiline = true;
            this.jobsDescription.Name = "jobsDescription";
            this.jobsDescription.ReadOnly = true;
            this.jobsDescription.Size = new System.Drawing.Size(539, 58);
            this.jobsDescription.TabIndex = 92;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label13.Location = new System.Drawing.Point(41, 55);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(87, 18);
            this.label13.TabIndex = 93;
            this.label13.Text = "Deskripsi";
            // 
            // searchTask
            // 
            this.searchTask.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchTask.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchTask.Location = new System.Drawing.Point(682, 16);
            this.searchTask.Name = "searchTask";
            this.searchTask.Size = new System.Drawing.Size(77, 30);
            this.searchTask.TabIndex = 91;
            this.searchTask.Text = "Cari";
            this.searchTask.UseVisualStyleBackColor = true;
            this.searchTask.Click += new System.EventHandler(this.searchTask_Click);
            // 
            // taskTextBox
            // 
            this.taskTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taskTextBox.Location = new System.Drawing.Point(135, 52);
            this.taskTextBox.MaxLength = 50;
            this.taskTextBox.Name = "taskTextBox";
            this.taskTextBox.ReadOnly = true;
            this.taskTextBox.Size = new System.Drawing.Size(539, 27);
            this.taskTextBox.TabIndex = 89;
            this.taskTextBox.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label12.Location = new System.Drawing.Point(66, 55);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 18);
            this.label12.TabIndex = 90;
            this.label12.Text = "Tugas";
            this.label12.Visible = false;
            // 
            // promoTextBox
            // 
            this.promoTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.promoTextBox.Location = new System.Drawing.Point(159, 463);
            this.promoTextBox.MaxLength = 200;
            this.promoTextBox.Multiline = true;
            this.promoTextBox.Name = "promoTextBox";
            this.promoTextBox.Size = new System.Drawing.Size(515, 113);
            this.promoTextBox.TabIndex = 88;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(82, 465);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 18);
            this.label3.TabIndex = 87;
            this.label3.Text = "Promo";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label11.Location = new System.Drawing.Point(361, 401);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(136, 18);
            this.label11.TabIndex = 86;
            this.label11.Text = "Realisasi BAST";
            // 
            // realisasiDTPicker
            // 
            this.realisasiDTPicker.CustomFormat = "dd MMM yyyy";
            this.realisasiDTPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.realisasiDTPicker.Location = new System.Drawing.Point(503, 393);
            this.realisasiDTPicker.Name = "realisasiDTPicker";
            this.realisasiDTPicker.Size = new System.Drawing.Size(171, 28);
            this.realisasiDTPicker.TabIndex = 85;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Location = new System.Drawing.Point(14, 401);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(136, 18);
            this.label10.TabIndex = 84;
            this.label10.Text = "Deadline BAST";
            // 
            // deadlineDTPicker
            // 
            this.deadlineDTPicker.CustomFormat = "dd MMM yyyy";
            this.deadlineDTPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.deadlineDTPicker.Location = new System.Drawing.Point(161, 393);
            this.deadlineDTPicker.Name = "deadlineDTPicker";
            this.deadlineDTPicker.Size = new System.Drawing.Size(171, 28);
            this.deadlineDTPicker.TabIndex = 83;
            // 
            // ppnCheckBox
            // 
            this.ppnCheckBox.AutoSize = true;
            this.ppnCheckBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ppnCheckBox.ForeColor = System.Drawing.Color.Black;
            this.ppnCheckBox.Location = new System.Drawing.Point(161, 365);
            this.ppnCheckBox.Name = "ppnCheckBox";
            this.ppnCheckBox.Size = new System.Drawing.Size(59, 22);
            this.ppnCheckBox.TabIndex = 82;
            this.ppnCheckBox.Text = "PPN";
            this.ppnCheckBox.UseVisualStyleBackColor = true;
            // 
            // comboBayar
            // 
            this.comboBayar.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBayar.FormattingEnabled = true;
            this.comboBayar.Items.AddRange(new object[] {
            "View Jobs",
            "View Reports",
            "Preferences"});
            this.comboBayar.Location = new System.Drawing.Point(161, 332);
            this.comboBayar.Name = "comboBayar";
            this.comboBayar.Size = new System.Drawing.Size(358, 26);
            this.comboBayar.TabIndex = 81;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(260, 303);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(19, 18);
            this.label8.TabIndex = 80;
            this.label8.Text = "/";
            // 
            // lbTextBox
            // 
            this.lbTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.lbTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTextBox.Location = new System.Drawing.Point(285, 300);
            this.lbTextBox.MaxLength = 50;
            this.lbTextBox.Name = "lbTextBox";
            this.lbTextBox.Size = new System.Drawing.Size(93, 27);
            this.lbTextBox.TabIndex = 79;
            this.lbTextBox.Text = "0";
            this.lbTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // searchCustomer
            // 
            this.searchCustomer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchCustomer.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchCustomer.Location = new System.Drawing.Point(477, 251);
            this.searchCustomer.Name = "searchCustomer";
            this.searchCustomer.Size = new System.Drawing.Size(77, 30);
            this.searchCustomer.TabIndex = 78;
            this.searchCustomer.Text = "Cari";
            this.searchCustomer.UseVisualStyleBackColor = true;
            this.searchCustomer.Click += new System.EventHandler(this.searchCustomer_Click);
            // 
            // customerTextBox
            // 
            this.customerTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerTextBox.Location = new System.Drawing.Point(135, 253);
            this.customerTextBox.MaxLength = 50;
            this.customerTextBox.Name = "customerTextBox";
            this.customerTextBox.ReadOnly = true;
            this.customerTextBox.Size = new System.Drawing.Size(336, 27);
            this.customerTextBox.TabIndex = 76;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Location = new System.Drawing.Point(35, 256);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 18);
            this.label7.TabIndex = 77;
            this.label7.Text = "Customer";
            // 
            // searchSales
            // 
            this.searchSales.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchSales.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchSales.Location = new System.Drawing.Point(477, 218);
            this.searchSales.Name = "searchSales";
            this.searchSales.Size = new System.Drawing.Size(77, 30);
            this.searchSales.TabIndex = 75;
            this.searchSales.Text = "Cari";
            this.searchSales.UseVisualStyleBackColor = true;
            this.searchSales.Click += new System.EventHandler(this.searchSales_Click);
            // 
            // salesTextBox
            // 
            this.salesTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salesTextBox.Location = new System.Drawing.Point(135, 220);
            this.salesTextBox.MaxLength = 50;
            this.salesTextBox.Name = "salesTextBox";
            this.salesTextBox.ReadOnly = true;
            this.salesTextBox.Size = new System.Drawing.Size(336, 27);
            this.salesTextBox.TabIndex = 73;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(74, 223);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 18);
            this.label6.TabIndex = 74;
            this.label6.Text = "Sales";
            // 
            // searchKavling
            // 
            this.searchKavling.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchKavling.Enabled = false;
            this.searchKavling.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchKavling.Location = new System.Drawing.Point(477, 185);
            this.searchKavling.Name = "searchKavling";
            this.searchKavling.Size = new System.Drawing.Size(77, 30);
            this.searchKavling.TabIndex = 72;
            this.searchKavling.Text = "Cari";
            this.searchKavling.UseVisualStyleBackColor = true;
            this.searchKavling.Click += new System.EventHandler(this.searchKavling_Click);
            // 
            // kavlingTextBox
            // 
            this.kavlingTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kavlingTextBox.Location = new System.Drawing.Point(135, 187);
            this.kavlingTextBox.MaxLength = 50;
            this.kavlingTextBox.Name = "kavlingTextBox";
            this.kavlingTextBox.ReadOnly = true;
            this.kavlingTextBox.Size = new System.Drawing.Size(336, 27);
            this.kavlingTextBox.TabIndex = 70;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(57, 190);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 18);
            this.label5.TabIndex = 71;
            this.label5.Text = "Kavling";
            // 
            // nonAktifCheckbox
            // 
            this.nonAktifCheckbox.AutoSize = true;
            this.nonAktifCheckbox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nonAktifCheckbox.ForeColor = System.Drawing.Color.Black;
            this.nonAktifCheckbox.Location = new System.Drawing.Point(156, 582);
            this.nonAktifCheckbox.Name = "nonAktifCheckbox";
            this.nonAktifCheckbox.Size = new System.Drawing.Size(103, 22);
            this.nonAktifCheckbox.TabIndex = 69;
            this.nonAktifCheckbox.Text = "Non Aktif";
            this.nonAktifCheckbox.UseVisualStyleBackColor = true;
            // 
            // searchDenah
            // 
            this.searchDenah.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchDenah.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchDenah.Location = new System.Drawing.Point(501, 427);
            this.searchDenah.Name = "searchDenah";
            this.searchDenah.Size = new System.Drawing.Size(77, 30);
            this.searchDenah.TabIndex = 66;
            this.searchDenah.Text = "Cari";
            this.searchDenah.UseVisualStyleBackColor = true;
            this.searchDenah.Click += new System.EventHandler(this.searchDenah_Click);
            // 
            // denahTextBox
            // 
            this.denahTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.denahTextBox.Location = new System.Drawing.Point(159, 429);
            this.denahTextBox.MaxLength = 15;
            this.denahTextBox.Name = "denahTextBox";
            this.denahTextBox.ReadOnly = true;
            this.denahTextBox.Size = new System.Drawing.Size(336, 27);
            this.denahTextBox.TabIndex = 64;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(83, 432);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 18);
            this.label4.TabIndex = 65;
            this.label4.Text = "Denah";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(30, 335);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 18);
            this.label1.TabIndex = 61;
            this.label1.Text = "Pembayaran";
            // 
            // searchKompleks
            // 
            this.searchKompleks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchKompleks.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchKompleks.Location = new System.Drawing.Point(477, 152);
            this.searchKompleks.Name = "searchKompleks";
            this.searchKompleks.Size = new System.Drawing.Size(77, 30);
            this.searchKompleks.TabIndex = 59;
            this.searchKompleks.Text = "Cari";
            this.searchKompleks.UseVisualStyleBackColor = true;
            this.searchKompleks.Click += new System.EventHandler(this.searchKompleks_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(77, 303);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 18);
            this.label2.TabIndex = 55;
            this.label2.Text = "LT / LB";
            // 
            // kompleksTextBox
            // 
            this.kompleksTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kompleksTextBox.Location = new System.Drawing.Point(135, 154);
            this.kompleksTextBox.MaxLength = 50;
            this.kompleksTextBox.Name = "kompleksTextBox";
            this.kompleksTextBox.ReadOnly = true;
            this.kompleksTextBox.Size = new System.Drawing.Size(336, 27);
            this.kompleksTextBox.TabIndex = 57;
            // 
            // ltTextBox
            // 
            this.ltTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ltTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ltTextBox.Location = new System.Drawing.Point(161, 300);
            this.ltTextBox.MaxLength = 50;
            this.ltTextBox.Name = "ltTextBox";
            this.ltTextBox.Size = new System.Drawing.Size(93, 27);
            this.ltTextBox.TabIndex = 56;
            this.ltTextBox.Text = "0";
            this.ltTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(36, 157);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 18);
            this.label9.TabIndex = 58;
            this.label9.Text = "Kompleks";
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.Color.FloralWhite;
            this.saveButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.ForeColor = System.Drawing.Color.Black;
            this.saveButton.Location = new System.Drawing.Point(346, 695);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(130, 37);
            this.saveButton.TabIndex = 67;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // openPDFDialog
            // 
            this.openPDFDialog.FileName = "*.pdf";
            // 
            // dataJobsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(821, 771);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.groupBox1);
            this.Name = "dataJobsForm";
            this.Text = "DATA TUGAS";
            this.Load += new System.EventHandler(this.dataJobsForm_Load);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.saveButton, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button searchDenah;
        private System.Windows.Forms.TextBox denahTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button searchKompleks;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox kompleksTextBox;
        private System.Windows.Forms.TextBox ltTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button searchCustomer;
        private System.Windows.Forms.TextBox customerTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button searchSales;
        private System.Windows.Forms.TextBox salesTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button searchKavling;
        private System.Windows.Forms.TextBox kavlingTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox lbTextBox;
        private System.Windows.Forms.ComboBox comboBayar;
        protected System.Windows.Forms.CheckBox ppnCheckBox;
        private System.Windows.Forms.Label label10;
        protected System.Windows.Forms.DateTimePicker deadlineDTPicker;
        private System.Windows.Forms.Label label11;
        protected System.Windows.Forms.DateTimePicker realisasiDTPicker;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox promoTextBox;
        protected System.Windows.Forms.CheckBox nonAktifCheckbox;
        private System.Windows.Forms.Button searchTask;
        private System.Windows.Forms.TextBox taskTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox jobsDescription;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button buttonPreview;
        private System.Windows.Forms.OpenFileDialog openPDFDialog;
        private System.Windows.Forms.TextBox skenarioTextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox startProsesTextBox;
        private System.Windows.Forms.Button buttonSearchProses;
    }
}
