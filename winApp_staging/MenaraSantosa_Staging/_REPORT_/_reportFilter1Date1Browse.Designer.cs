﻿namespace AlphaSoft
{
    partial class _reportFilter1Date
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_reportFilter1Date));
            this.dateDTPicker = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.displayButton = new System.Windows.Forms.Button();
            this.browseButton = new System.Windows.Forms.Button();
            this.filterValueTextBox = new System.Windows.Forms.TextBox();
            this.labelFilter = new System.Windows.Forms.Label();
            this.showAllCheckBox = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(528, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // dateDTPicker
            // 
            this.dateDTPicker.CustomFormat = "dd MMM yyyy";
            this.dateDTPicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateDTPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateDTPicker.Location = new System.Drawing.Point(45, 69);
            this.dateDTPicker.Name = "dateDTPicker";
            this.dateDTPicker.Size = new System.Drawing.Size(171, 26);
            this.dateDTPicker.TabIndex = 50;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(42, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 18);
            this.label2.TabIndex = 49;
            this.label2.Text = "TGL";
            // 
            // displayButton
            // 
            this.displayButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayButton.Location = new System.Drawing.Point(94, 167);
            this.displayButton.Name = "displayButton";
            this.displayButton.Size = new System.Drawing.Size(156, 58);
            this.displayButton.TabIndex = 48;
            this.displayButton.Text = "DISPLAY";
            this.displayButton.UseVisualStyleBackColor = true;
            this.displayButton.Click += new System.EventHandler(this.displayButton_Click);
            // 
            // browseButton
            // 
            this.browseButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("browseButton.BackgroundImage")));
            this.browseButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.browseButton.Location = new System.Drawing.Point(476, 122);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(25, 27);
            this.browseButton.TabIndex = 88;
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // filterValueTextBox
            // 
            this.filterValueTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filterValueTextBox.Location = new System.Drawing.Point(45, 122);
            this.filterValueTextBox.Name = "filterValueTextBox";
            this.filterValueTextBox.ReadOnly = true;
            this.filterValueTextBox.Size = new System.Drawing.Size(425, 27);
            this.filterValueTextBox.TabIndex = 87;
            // 
            // labelFilter
            // 
            this.labelFilter.AutoSize = true;
            this.labelFilter.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFilter.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelFilter.Location = new System.Drawing.Point(42, 101);
            this.labelFilter.Name = "labelFilter";
            this.labelFilter.Size = new System.Drawing.Size(77, 18);
            this.labelFilter.TabIndex = 86;
            this.labelFilter.Text = "FILTER ";
            // 
            // showAllCheckBox
            // 
            this.showAllCheckBox.AutoSize = true;
            this.showAllCheckBox.Location = new System.Drawing.Point(405, 102);
            this.showAllCheckBox.Name = "showAllCheckBox";
            this.showAllCheckBox.Size = new System.Drawing.Size(65, 17);
            this.showAllCheckBox.TabIndex = 89;
            this.showAllCheckBox.Text = "show All";
            this.showAllCheckBox.UseVisualStyleBackColor = true;
            this.showAllCheckBox.CheckedChanged += new System.EventHandler(this.showAllCheckBox_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(289, 167);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(146, 58);
            this.button1.TabIndex = 90;
            this.button1.Text = "CETAK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // _reportFilter1Date
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(528, 266);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.showAllCheckBox);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.filterValueTextBox);
            this.Controls.Add(this.labelFilter);
            this.Controls.Add(this.dateDTPicker);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.displayButton);
            this.MaximizeBox = false;
            this.Name = "_reportFilter1Date";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this._reportFilter1Date_FormClosed);
            this.Load += new System.EventHandler(this._report1FilterDate_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.displayButton, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.dateDTPicker, 0);
            this.Controls.SetChildIndex(this.labelFilter, 0);
            this.Controls.SetChildIndex(this.filterValueTextBox, 0);
            this.Controls.SetChildIndex(this.browseButton, 0);
            this.Controls.SetChildIndex(this.showAllCheckBox, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.DateTimePicker dateDTPicker;
        protected System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button displayButton;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.TextBox filterValueTextBox;
        private System.Windows.Forms.Label labelFilter;
        private System.Windows.Forms.CheckBox showAllCheckBox;
        private System.Windows.Forms.Button button1;
    }
}
