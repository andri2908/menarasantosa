﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlphaSoft
{
    public partial class dataFilterReportJobs : AlphaSoft.basicHotkeysForm
    {
        private int kompleksID = 0;
        private int kavlingID = 0;
        private int customerID = 0;
        private int salesID = 0;

        public dataFilterReportJobs()
        {
            InitializeComponent();
        }

        private void searchKompleks_Click(object sender, EventArgs e)
        {
            dataBrowse_KompleksForm displayForm = new dataBrowse_KompleksForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                kompleksID = Convert.ToInt32(displayForm.ReturnValue1);
                kompleksTextBox.Text = displayForm.ReturnValue2;

                searchKavling.Enabled = true;
            }
        }

        private void searchKavling_Click(object sender, EventArgs e)
        {
            dataBrowse_KavlingForm displayForm = new dataBrowse_KavlingForm(globalConstants.MODULE_DEFAULT, kompleksID);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                kavlingID = Convert.ToInt32(displayForm.ReturnValue1);
                kavlingTextBox.Text = displayForm.ReturnValue2;
            }
        }

        private void searchSales_Click(object sender, EventArgs e)
        {
            dataBrowse_SalesForm displayForm = new dataBrowse_SalesForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                salesID = Convert.ToInt32(displayForm.ReturnValue1);
                salesTextBox.Text = displayForm.ReturnValue2;
            }
        }

        private void searchCustomer_Click(object sender, EventArgs e)
        {
            dataBrowse_CustomerForm displayForm = new dataBrowse_CustomerForm(globalConstants.MODULE_DEFAULT);
            displayForm.ShowDialog(this);

            if (displayForm.ReturnValue1 != "0")
            {
                customerID = Convert.ToInt32(displayForm.ReturnValue1);
                customerTextBox.Text = displayForm.ReturnValue2;
            }
        }

        private void generateButton_Click(object sender, EventArgs e)
        {
            List<sqlFields> listSql = new List<sqlFields>();
            sqlFields sqlElement;

            sqlElement = new sqlFields();
            sqlElement.fieldName = "TIPE_REPORT";
            sqlElement.fieldValue = comboAkses.SelectedIndex;
            listSql.Add(sqlElement);

            if (checkBoxRange.Checked)
            {
                sqlElement = new sqlFields();
                sqlElement.fieldName = "START_DATE";
                sqlElement.fieldValue = startDTPicker.Value.Date;
                listSql.Add(sqlElement);

                sqlElement = new sqlFields();
                sqlElement.fieldName = "END_DATE";
                sqlElement.fieldValue = endDTPicker.Value.Date;
                listSql.Add(sqlElement);
            }

            if (checkBoxKompleks.Checked)
            {
                sqlElement = new sqlFields();
                sqlElement.fieldName = "KOMPLEKS_ID";
                sqlElement.fieldValue = kompleksID;
                listSql.Add(sqlElement);
            }

            if (checkBoxKavling.Checked)
            {
                sqlElement = new sqlFields();
                sqlElement.fieldName = "KAVLING_ID";
                sqlElement.fieldValue = kavlingID;
                listSql.Add(sqlElement);
            }

            if (checkBoxSales.Checked)
            {
                sqlElement = new sqlFields();
                sqlElement.fieldName = "SALES_ID";
                sqlElement.fieldValue = salesID;
                listSql.Add(sqlElement);
            }

            if (checkBoxKompleks.Checked)
            {
                sqlElement = new sqlFields();
                sqlElement.fieldName = "CUST_ID";
                sqlElement.fieldValue = customerID;
                listSql.Add(sqlElement);
            }

            RPT_containerForm displayReport = new RPT_containerForm(globalReportConstants.REPORT_JOBS, listSql);
            displayReport.ShowDialog(this);
        }

        private void dataFilterReportJobs_Load(object sender, EventArgs e)
        {
            comboAkses.SelectedIndex = 0;
        }
    }
}
