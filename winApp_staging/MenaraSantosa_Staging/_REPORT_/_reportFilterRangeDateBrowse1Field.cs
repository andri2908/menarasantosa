﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Globalization;
using System.IO;

namespace AlphaSoft
{
    public partial class _reportFilterRangeDateBrowse1Field : AlphaSoft.basicHotkeysForm
    {
        private Data_Access DS = new Data_Access();

        private int originModuleID = 0;
        private CultureInfo culture = new CultureInfo("id-ID");
        private string selectedValue = "0";

        private globalDBUtil gDB;
        private globalUserUtil gUser;

        private List<string> pList = new List<string>();
        private List<object> pVal = new List<object>();

        public _reportFilterRangeDateBrowse1Field(int moduleID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;

            DS.sqlConnect();

            gDB = new globalDBUtil(DS);
            gUser = new globalUserUtil(DS);
        }

        private void _reportFilterRangeDateBrowse1Field_Load(object sender, EventArgs e)
        {
            displayButton.PerformClick();
        }

        private void displayButton_Click(object sender, EventArgs e)
        {
            int paramID = Convert.ToInt32(selectedValue);

            pList.Clear();
            pList.Add("@refID");

            pVal.Clear();
            pVal.Add(selectedValue);
        }

        private void dbGridView_DoubleClick(object sender, EventArgs e)
        {
            List<sqlFields> listSql = new List<sqlFields>();
            sqlFields sqlElement;

            if (dbGridView.Rows.Count <= 0)
                return;

            int rowIndex = dbGridView.SelectedCells[0].RowIndex;
            DataGridViewRow sRow = dbGridView.Rows[rowIndex];

            switch (originModuleID)
            {
                default:
                    break;
            }
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            switch (originModuleID)
            {
            }
        }

        private void _reportFilterRangeDateBrowse1Field_FormClosed(object sender, FormClosedEventArgs e)
        {
            DS.sqlClose();
        }

        private void dbGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
