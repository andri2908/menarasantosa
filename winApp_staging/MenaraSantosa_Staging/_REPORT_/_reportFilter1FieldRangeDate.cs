﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using MySql.Data;
using MySql.Data.MySqlClient;

namespace AlphaSoft
{
    public partial class _reportFilter1FieldRangeDate : basicHotkeysForm
    {
        private int originModuleID = 0;
        private string selectedValue = "0";

        public _reportFilter1FieldRangeDate(int moduleID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;
        }
      
        private void browseButton_Click(object sender, EventArgs e)
        {
            switch (originModuleID)
            {
            }
        }

        private void displayButton_Click(object sender, EventArgs e)
        {
            string sqlCommand = "";

            List<sqlFields> listSql = new List<sqlFields>();
            sqlFields sqlElement;

            switch (originModuleID)
            {

                default:
                    break;
            }

            sqlElement = new sqlFields();
            sqlElement.fieldName = "START_DATE";
            sqlElement.fieldValue = startDTPicker.Value.Date;
            listSql.Add(sqlElement);

            sqlElement = new sqlFields();
            sqlElement.fieldName = "END_DATE";
            sqlElement.fieldValue = endDTPicker.Value.Date;
            listSql.Add(sqlElement);

            RPT_containerForm displayReportForm = new RPT_containerForm(null, originModuleID, sqlCommand, listSql);
            displayReportForm.ShowDialog(this);
        }

        private void _reportFilter1FieldRangeDate_Load(object sender, EventArgs e)
        {
            switch (originModuleID)
            {
            }
        }
    }
}
