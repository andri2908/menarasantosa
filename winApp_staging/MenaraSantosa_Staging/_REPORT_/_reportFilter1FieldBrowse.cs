﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Globalization;

namespace AlphaSoft
{
    public partial class _reportFilter1FieldBrowse : AlphaSoft.basicHotkeysForm
    {
        private int originModuleID = 0;
        private CultureInfo culture = new CultureInfo("id-ID");
        private string selectedID = "";

        public _reportFilter1FieldBrowse(int moduleID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            string retVal1 = "";
            string retVal2 = "";

            selectedID = retVal1;
            filterValueTextBox.Text = retVal2;
        }

        private void displayButton_Click(object sender, EventArgs e)
        {
            string sqlCommand = "";

            List<sqlFields> listSql = new List<sqlFields>();
            sqlFields sqlElement;

            switch (originModuleID)
            {
                default:
                    break;
            }

            RPT_containerForm displayReportForm = new RPT_containerForm(null, originModuleID, sqlCommand, listSql);
            displayReportForm.ShowDialog(this);
        }
    }
}
