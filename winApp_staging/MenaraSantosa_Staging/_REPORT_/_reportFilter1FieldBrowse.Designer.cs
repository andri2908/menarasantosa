﻿namespace AlphaSoft
{
    partial class _reportFilter1FieldBrowse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_reportFilter1FieldBrowse));
            this.showAllCheckBox = new System.Windows.Forms.CheckBox();
            this.browseButton = new System.Windows.Forms.Button();
            this.filterValueTextBox = new System.Windows.Forms.TextBox();
            this.labelFilter = new System.Windows.Forms.Label();
            this.displayButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(528, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // showAllCheckBox
            // 
            this.showAllCheckBox.AutoSize = true;
            this.showAllCheckBox.Location = new System.Drawing.Point(391, 98);
            this.showAllCheckBox.Name = "showAllCheckBox";
            this.showAllCheckBox.Size = new System.Drawing.Size(65, 17);
            this.showAllCheckBox.TabIndex = 95;
            this.showAllCheckBox.Text = "show All";
            this.showAllCheckBox.UseVisualStyleBackColor = true;
            // 
            // browseButton
            // 
            this.browseButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("browseButton.BackgroundImage")));
            this.browseButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.browseButton.Location = new System.Drawing.Point(462, 65);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(25, 27);
            this.browseButton.TabIndex = 94;
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // filterValueTextBox
            // 
            this.filterValueTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filterValueTextBox.Location = new System.Drawing.Point(31, 65);
            this.filterValueTextBox.Name = "filterValueTextBox";
            this.filterValueTextBox.ReadOnly = true;
            this.filterValueTextBox.Size = new System.Drawing.Size(425, 27);
            this.filterValueTextBox.TabIndex = 93;
            // 
            // labelFilter
            // 
            this.labelFilter.AutoSize = true;
            this.labelFilter.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFilter.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelFilter.Location = new System.Drawing.Point(28, 44);
            this.labelFilter.Name = "labelFilter";
            this.labelFilter.Size = new System.Drawing.Size(77, 18);
            this.labelFilter.TabIndex = 92;
            this.labelFilter.Text = "FILTER ";
            // 
            // displayButton
            // 
            this.displayButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayButton.Location = new System.Drawing.Point(166, 115);
            this.displayButton.Name = "displayButton";
            this.displayButton.Size = new System.Drawing.Size(156, 58);
            this.displayButton.TabIndex = 91;
            this.displayButton.Text = "DISPLAY";
            this.displayButton.UseVisualStyleBackColor = true;
            this.displayButton.Click += new System.EventHandler(this.displayButton_Click);
            // 
            // _reportFilter1FieldBrowse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(528, 221);
            this.Controls.Add(this.showAllCheckBox);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.filterValueTextBox);
            this.Controls.Add(this.labelFilter);
            this.Controls.Add(this.displayButton);
            this.Name = "_reportFilter1FieldBrowse";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.displayButton, 0);
            this.Controls.SetChildIndex(this.labelFilter, 0);
            this.Controls.SetChildIndex(this.filterValueTextBox, 0);
            this.Controls.SetChildIndex(this.browseButton, 0);
            this.Controls.SetChildIndex(this.showAllCheckBox, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckBox showAllCheckBox;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.TextBox filterValueTextBox;
        private System.Windows.Forms.Label labelFilter;
        private System.Windows.Forms.Button displayButton;
    }
}
