﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using System.Globalization;
using CrystalDecisions.ReportSource;
using CrystalDecisions.CrystalReports.Engine;

using AlphaSoft._REPORT_;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace AlphaSoft
{
    partial class RPT_containerForm : Form
    {
        private Data_Access DS;
        private globalUserUtil gUser;
        private globalDBUtil gDB;
        private globalRestReport gRest = new globalRestReport();
        private globalRestAPI gRestAPI = new globalRestAPI();
        private CultureInfo culture = new CultureInfo("id-ID");

        private List<sqlFields> sqlFieldsValue;
        private int originModuleID = 0;
        private string xmlReportName = "";
        private string defaultSqlCommand = "";
        private string dateTo, dateFrom;
        private string report_month = "";
        private string appPath = "";

        private int[] arrQuarterKuarto =
        {
       
        };

        private int[] arrHalfKuarto =
        {       
        };

        private int[] arrKartuStok =
        {
        };

        private int[] arrNoWriteXML =
        {
        };

        private int[] arrLangsungPrint =
        {
        };

        private int[] arrCheckPreview =
        {
            globalReportConstants.REPORT_JOBS
        };

        private int[] arrSkipCompleteHeader =
        {
            globalReportConstants.PRINTOUT_IPL,
            globalReportConstants.PRINTOUT_TRANSAKSI_RETAIL,
            globalReportConstants.PRINTOUT_TRANSAKSI_PENERIMAAN,
            globalReportConstants.PRINTOUT_PENGIRIMAN_TRANSAKSI,
            globalReportConstants.REPORT_JOBS
        };

        private int[] arrNoHeader =
        {
            globalReportConstants.PRINTOUT_IPL,
            globalReportConstants.PRINTOUT_TRANSAKSI_RETAIL,
            globalReportConstants.PRINTOUT_TRANSAKSI_PENERIMAAN,
            globalReportConstants.PRINTOUT_PENGIRIMAN_TRANSAKSI,
            globalReportConstants.REPORT_JOBS
        };

        private void RS_writeXML(string sqlCommand, string xmlReportName)
        {

        }

        private void generateXML_printoutIPL(out string sqlCommand)
        {
            string IDParam = "";
            sqlFields sqlFieldsElement;
       
            REST_printoutTransaksi restTrans = new REST_printoutTransaksi();

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "ID_TRANS");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    IDParam = sqlFieldsElement.fieldValue.ToString();
                }
            }

            sqlCommand = "SELECT 'PEMBAYARAN IPL' AS title, " +
                                    "CONCAT(IFNULL(mo.kompleks_name, ''), ' ', IFNULL(mb.blok_name, ''), '-', IFNULL(mk.house_no, '')) AS nama_kavling, " +
                                    "CONCAT('PEMBAYARAN IPL ', IFNULL(mo.kompleks_name, ''), ' ', IFNULL(mb.blok_name, ''), '-', IFNULL(mk.house_no, '')) AS item_1, " +
                                    "CONCAT(DATE_FORMAT(h.start_ipl, '%M %Y'), ' - ', DATE_FORMAT(h.end_ipl, '%M %Y'), IF(h.status_id = 1, ' (LUNAS)', ' (BELUM LUNAS)')) AS ITEM_2, " +
                                    "h.nominal AS harga, 1 AS qty, " +
                                    "h.nominal, ul.user_name, ul.user_full_name, ul.user_phone_1 " +
                                    "FROM transaksi_ipl h " +
                                    "LEFT OUTER JOIN user_login_data ul ON (h.user_id = ul.user_id) " +
                                    "LEFT OUTER JOIN master_kavling mk ON (h.kavling_id= mk.kavling_id) " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id) " +
                                    "LEFT OUTER JOIN master_kompleks mo ON (mk.kompleks_id = mo.kompleks_id) " +
                                    "WHERE h.is_active = 'Y' ";

            if (IDParam != "")
            {
                sqlCommand += "AND h.id_trans = " + IDParam;
            }

            if (gRest.generatePrintOutTransaksiIPL(sqlCommand, ref restTrans))
            {
                DataTable dt = new DataTable("Table");

                dt.Columns.Add("title");
                dt.Columns.Add("user_full_name");
                dt.Columns.Add("user_name");
                dt.Columns.Add("nama_kavling");
                dt.Columns.Add("user_phone_1");
                dt.Columns.Add("item_1");
                dt.Columns.Add("item_2");
                dt.Columns.Add("qty", typeof(double));
                dt.Columns.Add("harga", typeof(double));
                dt.Columns.Add("nominal", typeof(double));

                for (int i = 0; i < restTrans.dataList.Count; i++)
                {
                    DataRow r = dt.Rows.Add();

                    r["title"] = restTrans.dataList[i].title;
                    r["user_full_name"] = restTrans.dataList[i].user_full_name;
                    r["user_name"] = restTrans.dataList[i].user_name;
                    r["nama_kavling"] = restTrans.dataList[i].nama_kavling;
                    r["user_phone_1"] = restTrans.dataList[i].user_phone_1;
                    r["item_1"] = restTrans.dataList[i].item_1;
                    r["item_2"] = restTrans.dataList[i].item_2;
                    r["qty"] = restTrans.dataList[i].qty;
                    r["harga"] = restTrans.dataList[i].harga;
                    r["nominal"] = restTrans.dataList[i].nominal;
                }

                string appPath = Directory.GetCurrentDirectory() + "\\xml\\" + xmlReportName;

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                using (StreamWriter fs = new StreamWriter(appPath)) // XML File Path
                {
                    ds.WriteXml(fs, XmlWriteMode.WriteSchema);
                }
            }
        }

        private void generateXML_printoutTransaksiRetail(out string sqlCommand)
        {
            string IDParam = "";
            sqlFields sqlFieldsElement;
            REST_printoutTransaksi restTrans = new REST_printoutTransaksi();

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "ID_TRANS");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    IDParam = sqlFieldsElement.fieldValue.ToString();
                }
            }

            sqlCommand = "SELECT 'TRANSAKSI RETAIL' AS title, " +
                                    "CONCAT(IFNULL(mo.kompleks_name, ''), ' ', IFNULL(mb.blok_name, ''), '-', IFNULL(mk.house_no, '')) AS nama_kavling, " +
                                    "CONCAT(mi.item_name, ' (', mu.unit_name, ')') AS item_1, " +
                                    "'' AS item_2, " +
                                    "d.item_price AS harga, d.item_qty AS qty, " +
                                    "d.subtotal AS nominal, ul.user_name, ul.user_full_name, ul.user_phone_1 " +
                                    "FROM transaksi_retail h " +
                                    "LEFT OUTER JOIN user_login_data ul ON (h.user_id = ul.user_id) " +
                                    "LEFT OUTER JOIN master_kavling mk ON (h.kavling_id = mk.kavling_id) " +
                                    "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id) " +
                                    "LEFT OUTER JOIN master_kompleks mo ON (mk.kompleks_id = mo.kompleks_id), " +
                                    "transaksi_retail_detail d " +
                                    "LEFT OUTER JOIN master_item mi ON (d.item_id = mi.item_id) " +
                                    "LEFT OUTER JOIN master_unit mu ON (mi.unit_id = mu.unit_id) " +
                                    "WHERE d.id_trans = h.id_trans " +
                                    "AND h.is_active = 'Y' AND d.is_active = 'Y' ";

            if (IDParam != "")
            {
                sqlCommand += "AND h.id_trans = " + IDParam;
            }

            if (gRest.generatePrintOutTransaksiIPL(sqlCommand, ref restTrans))
            {
                DataTable dt = new DataTable("Table");

                dt.Columns.Add("title");
                dt.Columns.Add("user_full_name");
                dt.Columns.Add("user_name");
                dt.Columns.Add("nama_kavling");
                dt.Columns.Add("user_phone_1");
                dt.Columns.Add("item_1");
                dt.Columns.Add("item_2");
                dt.Columns.Add("qty", typeof(double));
                dt.Columns.Add("harga", typeof(double));
                dt.Columns.Add("nominal", typeof(double));

                for (int i = 0; i < restTrans.dataList.Count; i++)
                {
                    DataRow r = dt.Rows.Add();

                    r["title"] = restTrans.dataList[i].title;
                    r["user_full_name"] = restTrans.dataList[i].user_full_name;
                    r["user_name"] = restTrans.dataList[i].user_name;
                    r["nama_kavling"] = restTrans.dataList[i].nama_kavling;
                    r["user_phone_1"] = restTrans.dataList[i].user_phone_1;
                    r["item_1"] = restTrans.dataList[i].item_1;
                    r["item_2"] = restTrans.dataList[i].item_2;
                    r["qty"] = restTrans.dataList[i].qty;
                    r["harga"] = restTrans.dataList[i].harga;
                    r["nominal"] = restTrans.dataList[i].nominal;
                }

                string appPath = Directory.GetCurrentDirectory() + "\\xml\\" + xmlReportName;

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                using (StreamWriter fs = new StreamWriter(appPath)) // XML File Path
                {
                    ds.WriteXml(fs, XmlWriteMode.WriteSchema);
                }
            }
        }

        private void generateXML_printoutTransaksiPenerimaan(out string sqlCommand)
        {
            string IDParam = "";
            sqlFields sqlFieldsElement;
            REST_printoutTransaksi restTrans = new REST_printoutTransaksi();
            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "PENERIMAAN_ID");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                if (sqlFieldsElement.fieldValue.ToString() != "0")
                {
                    IDParam = sqlFieldsElement.fieldValue.ToString();
                }
            }

            sqlCommand = "SELECT 'TRANSAKSI PENERIMAAN' AS title, " +
                                    "h.penerimaan_datetime, " +
                                    "CONCAT(mi.item_name, ' (', mu.unit_name, ')') AS item_1, " +
                                    "'' AS item_2, " +
                                    "d.item_hpp AS harga, d.item_qty AS qty, " +
                                    "d.item_subtotal AS nominal " +
                                    "FROM penerimaan_header h, " +
                                    "penerimaan_detail d " +
                                    "LEFT OUTER JOIN master_item mi ON (d.item_id = mi.item_id) " +
                                    "LEFT OUTER JOIN master_unit mu ON (mi.unit_id = mu.unit_id) " +
                                    "WHERE d.penerimaan_id = h.penerimaan_id " +
                                    "AND h.is_active = 'Y' AND d.is_active = 'Y' ";

            if (IDParam != "")
            {
                sqlCommand += "AND h.penerimaan_id = " + IDParam;
            }

            if (gRest.generatePrintOutTransaksiIPL(sqlCommand, ref restTrans))
            {
                DataTable dt = new DataTable("Table");

                dt.Columns.Add("title");
                dt.Columns.Add("user_full_name");
                dt.Columns.Add("user_name");
                dt.Columns.Add("nama_kavling");
                dt.Columns.Add("user_phone_1");
                dt.Columns.Add("item_1");
                dt.Columns.Add("item_2");
                dt.Columns.Add("qty", typeof(double));
                dt.Columns.Add("harga", typeof(double));
                dt.Columns.Add("nominal", typeof(double));

                for (int i = 0; i < restTrans.dataList.Count; i++)
                {
                    DataRow r = dt.Rows.Add();

                    r["title"] = restTrans.dataList[i].title;
                    r["user_full_name"] = restTrans.dataList[i].user_full_name;
                    r["user_name"] = restTrans.dataList[i].user_name;
                    r["nama_kavling"] = restTrans.dataList[i].nama_kavling;
                    r["user_phone_1"] = restTrans.dataList[i].user_phone_1;
                    r["item_1"] = restTrans.dataList[i].item_1;
                    r["item_2"] = restTrans.dataList[i].item_2;
                    r["qty"] = restTrans.dataList[i].qty;
                    r["harga"] = restTrans.dataList[i].harga;
                    r["nominal"] = restTrans.dataList[i].nominal;
                }

                string appPath = Directory.GetCurrentDirectory() + "\\xml\\" + xmlReportName;

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                using (StreamWriter fs = new StreamWriter(appPath)) // XML File Path
                {
                    ds.WriteXml(fs, XmlWriteMode.WriteSchema);
                }
            }
        }

        private void generatePRINTOUTXml()
        {
            string sqlCommand = "";

            List<string> paramList = new List<string>();
            List<object> paramValues = new List<object>();

            switch (originModuleID)
            {
                case globalReportConstants.PRINTOUT_IPL:
                    xmlReportName = globalReportConstants.transaksiXML;
                    generateXML_printoutIPL(out sqlCommand);
                    break;

                case globalReportConstants.PRINTOUT_TRANSAKSI_RETAIL:
                case globalReportConstants.PRINTOUT_PENGIRIMAN_TRANSAKSI:
                    xmlReportName = globalReportConstants.transaksiXML;
                    generateXML_printoutTransaksiRetail(out sqlCommand);
                    break;

                case globalReportConstants.PRINTOUT_TRANSAKSI_PENERIMAAN:
                    xmlReportName = globalReportConstants.transaksiXML;
                    generateXML_printoutTransaksiPenerimaan(out sqlCommand);
                    break;

                default:
                    sqlCommand = defaultSqlCommand;
                    break;
            }
        }

        private void generateXML_reportJobs(out string sqlCommand)
        {
            sqlCommand = "";
            string reportTitle = "";
            string reportTitle2 = "";
            sqlFields sqlFieldsElement;

            int tipeReport = 0;
            int kompleksID = 0;
            int kavlingID = 0;
            int custID = 0;
            int salesID = 0;

            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now;

            string dtStartValue = "";
            string dtEndValue = "";

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "START_DATE");
            if (null != sqlFieldsElement)
            {
                startDate = Convert.ToDateTime(sqlFieldsElement.fieldValue).Date;
                dtStartValue = String.Format(culture, "{0:dd/MM/yyyy}", startDate);
            }

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "END_DATE");
            if (null != sqlFieldsElement)
            {
                endDate = Convert.ToDateTime(sqlFieldsElement.fieldValue).Date;
                dtEndValue = String.Format(culture, "{0:dd/MM/yyyy}", endDate);
            }

            REST_displayJobs jobsList = new REST_displayJobs();

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "TIPE_REPORT");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                tipeReport = Convert.ToInt32(sqlFieldsElement.fieldValue);

                if (tipeReport == 0)
                    reportTitle = "LAPORAN JOBS SELESAI";
                else
                    reportTitle = "LAPORAN JOBS BELUM SELESAI";
            }

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "KOMPLEKS_ID");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                kompleksID = Convert.ToInt32(sqlFieldsElement.fieldValue);
            }

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "KAVLING_ID");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                kavlingID = Convert.ToInt32(sqlFieldsElement.fieldValue);
            }

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "SALES_ID");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                salesID = Convert.ToInt32(sqlFieldsElement.fieldValue);
            }

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "CUST_ID");
            if (null != sqlFieldsElement && sqlFieldsElement.fieldValue.ToString().Length > 0)
            {
                custID = Convert.ToInt32(sqlFieldsElement.fieldValue);
            }

            sqlCommand = "SELECT jd.job_id, jd.id, mt.task_name, mt.task_duration, jd.task_start, jd.last_update_date, " +
                                    "ml.kompleks_name, mb.blok_name, mk.house_no, ms.sales_name, mc.customer_name, " +
                                    "jh.department_id, md.department_name, jd.task_status " +
                                     "FROM data_jobs_detail jd " +
                                     "LEFT OUTER JOIN master_task mt ON (jd.task_id = mt.task_id), " +
                                     "data_jobs_header jh " +
                                     "LEFT OUTER JOIN master_kompleks ml ON (ml.kompleks_id = jh.kompleks_id) " +
                                     "LEFT OUTER JOIN master_kavling mk ON (mk.kavling_id = jh.kavling_id) " +
                                     "LEFT OUTER JOIN master_blok mb ON (mk.blok_id = mb.blok_id) " +
                                     "LEFT OUTER JOIN master_sales ms ON (jh.sales_id = ms.sales_id) " +
                                     "LEFT OUTER JOIN master_customer mc ON (jh.customer_id = mc.customer_id) " +
                                     "LEFT OUTER JOIN master_department md ON (jh.department_id = md.department_id) " +
                                     "WHERE jh.is_active = 'Y' " +
                                     "AND jd.job_id = jh.job_id ";

            if (tipeReport == 0)
            {
                sqlCommand += "AND jd.task_status = 'FINISH' ";
            }
            else if (tipeReport == 1)
            {
                sqlCommand += "AND jd.task_status <> 'FINISH' ";
            }

            if (kompleksID != 0)
            {
                sqlCommand += "AND jh.kompleks_id = " + kompleksID + " ";
            }

            if (kavlingID != 0)
            {
                sqlCommand += "AND jh.kavling_id = " + kavlingID + " ";
            }

            if (salesID != 0)
            {
                sqlCommand += "AND jh.sales_id = " + salesID + " ";
            }

            if (custID != 0)
            {
                sqlCommand += "AND jh.customer_id = " + custID + " ";
            }

            if (dtStartValue.Length > 0 && dtEndValue.Length > 0)
            {
                reportTitle2 = dtStartValue + "-" + dtEndValue;

                dtStartValue = String.Format(culture, "{0:yyyyMMdd}", startDate);
                dtEndValue = String.Format(culture, "{0:yyyyMMdd}", endDate);

                sqlCommand += "AND DATE_FORMAT(jh.task_start, '%Y%M%d') >= '" + dtStartValue + "' " +
                                         "AND DATE_FORMAT(jh.task_start, '%Y%M%d') <= '" + dtEndValue + "' ";
            }

            DataTable dtJobs = new DataTable("Table");
            int timeElapsedValue = 0;

            if (gRestAPI.getJobsList(gUser.getUserID(), gUser.getUserToken(), sqlCommand, ref jobsList))
            {
                dtJobs.Columns.Add("title");
                dtJobs.Columns.Add("title2");
                dtJobs.Columns.Add("job_id");
                dtJobs.Columns.Add("id");
                dtJobs.Columns.Add("task_name");
                dtJobs.Columns.Add("task_start");
                dtJobs.Columns.Add("kompleks_name");
                dtJobs.Columns.Add("blok_name");
                dtJobs.Columns.Add("house_no");
                dtJobs.Columns.Add("sales_name");
                dtJobs.Columns.Add("customer_name");
                dtJobs.Columns.Add("task_duration");
                dtJobs.Columns.Add("department_id");
                dtJobs.Columns.Add("department_name");
                dtJobs.Columns.Add("time_elapsed");
                dtJobs.Columns.Add("task_status");
                
                for (int i = 0; i < jobsList.dataList.Count; i++)
                {
                    if (tipeReport == 0)
                        timeElapsedValue = (jobsList.dataList[i].last_update_date - jobsList.dataList[i].task_start).Days;
                    else
                        timeElapsedValue = (DateTime.Now - jobsList.dataList[i].task_start).Days;

                    DataRow r = dtJobs.Rows.Add();
                    r["title"] = reportTitle;
                    r["title2"] = reportTitle2;
                    r["job_id"] = jobsList.dataList[i].job_id;
                    r["id"] = jobsList.dataList[i].id;
                    r["task_name"] = jobsList.dataList[i].task_name;
                    r["task_start"] = gUser.getCustomStringFormatDate(jobsList.dataList[i].task_start, true);
                    r["kompleks_name"] = jobsList.dataList[i].kompleks_name;
                    r["blok_name"] = jobsList.dataList[i].blok_name;
                    r["house_no"] = jobsList.dataList[i].house_no;
                    r["sales_name"] = jobsList.dataList[i].sales_name;
                    r["customer_name"] = jobsList.dataList[i].customer_name;
                    r["task_duration"] = jobsList.dataList[i].task_duration;
                    r["department_id"] = jobsList.dataList[i].department_id;
                    r["department_name"] = jobsList.dataList[i].department_name;
                    r["time_elapsed"] = timeElapsedValue + " days";
                    r["task_status"] = jobsList.dataList[i].task_status;
                }

                string appPath = Directory.GetCurrentDirectory() + "\\xml\\" + xmlReportName;

                DataSet ds = new DataSet();
                ds.Tables.Add(dtJobs);
                using (StreamWriter fs = new StreamWriter(appPath)) // XML File Path
                {
                    ds.WriteXml(fs, XmlWriteMode.WriteSchema);
                }
            }
        }

        private void generateREPORTXml()
        {
            string sqlCommand = "";
            string sqlSubCommand = "";

            List<string> paramList = new List<string>();
            List<object> paramValues = new List<object>();
            sqlFields sqlFieldsElement;

            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now;

            string dtStartValue = "";
            string dtEndValue = "";

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "START_DATE");
            if (null != sqlFieldsElement)
                startDate = Convert.ToDateTime(sqlFieldsElement.fieldValue).Date;

            sqlFieldsElement = sqlFieldsValue.Find(x => x.fieldName == "END_DATE");
            if (null != sqlFieldsElement)
                endDate = Convert.ToDateTime(sqlFieldsElement.fieldValue).Date;

            dtStartValue = String.Format(culture, "{0:dd/MM/yyyy}", startDate);
            dtEndValue = String.Format(culture, "{0:dd/MM/yyyy}", endDate);

            paramList.Clear();
            paramValues.Clear();

            switch (originModuleID)
            {
                case globalReportConstants.REPORT_JOBS:
                    xmlReportName = globalReportConstants.reportJobsXML;
                    generateXML_reportJobs(out sqlCommand);
                    break;

                default:
                    sqlCommand = defaultSqlCommand;
                    break;
            }

            //DS.writeXML(sqlCommand, xmlReportName, paramList, paramValues);
        }

        private void generateXMLFile()
        {
            if (originModuleID > globalReportConstants._START_PRINTOUT_
                && originModuleID < globalReportConstants._END_PRINTOUT_)
            {
                generatePRINTOUTXml();
            }
            else if (originModuleID > globalReportConstants.START_REPORT
               && originModuleID < globalReportConstants.END_REPORT)
            {
                generateREPORTXml();
            }
        }

        public RPT_containerForm(Data_Access DSParam, int moduleID, 
            string sqlCommand = "", List<sqlFields> sqlFieldsParam = null)
        {
            InitializeComponent();

            originModuleID = moduleID;

            if (null != DSParam)
                DS = DSParam;
            else
                DS = new Data_Access();

            gUser = new globalUserUtil(DS);
            gDB = new globalDBUtil(DS);

            defaultSqlCommand = sqlCommand;
            sqlFieldsValue = sqlFieldsParam;

            appPath = Directory.GetCurrentDirectory() + "\\xml\\";

            if (!Directory.Exists(appPath))
                Directory.CreateDirectory(appPath);

            generateXMLFile();
        }

        public RPT_containerForm(int moduleID, List<sqlFields> sqlFieldsParam = null)
        {
            InitializeComponent();

            originModuleID = moduleID;
            gUser = new globalUserUtil();

            defaultSqlCommand = "";
            sqlFieldsValue = sqlFieldsParam;

            appPath = Directory.GetCurrentDirectory() + "\\xml\\";

            if (!Directory.Exists(appPath))
                Directory.CreateDirectory(appPath);

            generateXMLFile();
        }

        public void setDateReport(string dateToF, string dateFromF)
        {
            dateTo = dateToF;
            dateFrom = dateFromF;
        }

        private void RPT_containerForm_Load(object sender, EventArgs e)
        {
            DataSet dsTempReport = new DataSet();

            TextObject txtReportHeader1, txtReportHeader2, txtReportHeader3, txtColumnHeader1, txtTitle;
            TextObject fieldHeader1, fieldHeader2, fieldHeader3, fieldHeader4;

            try
            {
                string appPath = "";

                // if (originModuleID != globalReportConstants.PRINTOUT_KARTU_STOK)
                {
                    appPath = Directory.GetCurrentDirectory() + "\\xml\\" + xmlReportName;
                    dsTempReport.ReadXml(@appPath);
                }

                //ReportClass rptXMLReport = null;
                var rptXMLReport = (dynamic)null;

                switch (originModuleID)
                {
                    case globalReportConstants.PRINTOUT_IPL:
                    case globalReportConstants.PRINTOUT_TRANSAKSI_RETAIL:
                        rptXMLReport = new PRINTOUT_trans();
                        break;

                    case globalReportConstants.PRINTOUT_PENGIRIMAN_TRANSAKSI:
                        rptXMLReport = new PRINTOUT_transDelivery();
                        break;

                    case globalReportConstants.PRINTOUT_TRANSAKSI_PENERIMAAN:
                        rptXMLReport = new PRINTOUT_penerimaan();
                        break;

                    case globalReportConstants.REPORT_JOBS:
                        rptXMLReport = new REPORT_jobs();
                        break;

                    default:
                        rptXMLReport = new dummyReport();
                        break;
                }

                // if (originModuleID != globalReportConstants.PRINTOUT_KARTU_STOK)
                rptXMLReport.Database.Tables[0].SetDataSource(dsTempReport.Tables[0]);

                #region SUB REPORT 
          
                #endregion

                #region GENERAL HEADER INFORMATION
                String nama, alamat, telepon, email;
                if (!gUser.RS_loadInfoSysConfig(out nama, out alamat, out telepon, out email))
                {
                    nama = "NAMA DEFAULT";
                    alamat = "ALAMAT DEFAULT";
                    telepon = "0271-XXXXXXX";
                    email = "A@B.COM";
                }

                if (!arrSkipCompleteHeader.Contains(originModuleID))
                {
                    txtReportHeader1 = rptXMLReport.ReportDefinition.ReportObjects["namaCompLabel"] as TextObject;
                    txtReportHeader2 = rptXMLReport.ReportDefinition.ReportObjects["infoCompLabel"] as TextObject;

                    txtReportHeader1.Text = nama;
                    txtReportHeader2.Text = alamat + Environment.NewLine + telepon + Environment.NewLine + email;
                }
                else if (!arrNoHeader.Contains(originModuleID))
                {
                    txtReportHeader1 = rptXMLReport.ReportDefinition.ReportObjects["namaCompLabel"] as TextObject;
                    txtReportHeader1.Text = nama;
                }
                #endregion

                globalPrinterUtility gPrinter = new globalPrinterUtility();
                rptXMLReport.PrintOptions.PrinterName = gPrinter.getConfigPrinterName(2);

                if (arrQuarterKuarto.Contains(originModuleID))
                {
                    rptXMLReport.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)gPrinter.getReportPaperSize(globalPrinterUtility.QUARTER_KUARTO_PAPER_SIZE);
                }
                else if (arrHalfKuarto.Contains(originModuleID))
                {
                    rptXMLReport.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)gPrinter.getReportPaperSize(globalPrinterUtility.HALF_KUARTO_PAPER_SIZE);
                }
                else
                {
                    rptXMLReport.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)gPrinter.getReportPaperSize(globalPrinterUtility.LETTER_PAPER_SIZE);
                }

                rptXMLReport.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;

                if (arrLangsungPrint.Contains(originModuleID))
                {
                    rptXMLReport.PrintToPrinter(1, false, 0, 0);
                    this.Close();
                }
                else if (arrCheckPreview.Contains(originModuleID))
                {
                    if (gPrinter.getPrintPreview())
                    {
                        crViewer.ReportSource = rptXMLReport;
                        crViewer.Refresh();
                    }
                    else
                    {
                        rptXMLReport.PrintToPrinter(1, false, 0, 0);
                        this.Close();
                    }
                }
                else
                {
                    this.WindowState = FormWindowState.Maximized;
                    crViewer.ReportSource = rptXMLReport;
                    crViewer.Refresh();
                }
            }
            catch (Exception ex) { }
        }

        public void setReportMonth(string monthName)
        {
            report_month = monthName;
        }
    }
}

