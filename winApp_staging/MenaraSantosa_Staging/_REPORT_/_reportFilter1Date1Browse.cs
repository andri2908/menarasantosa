﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Globalization;
using System.IO;

namespace AlphaSoft
{
    public partial class _reportFilter1Date : AlphaSoft.basicHotkeysForm
    {
        private Data_Access DS;

        private int originModuleID = 0;
        private CultureInfo culture = new CultureInfo("id-ID");
        private string selectedValue = "0";

        private globalDBUtil gDB;

        public _reportFilter1Date(int moduleID = 0)
        {
            InitializeComponent();

            originModuleID = moduleID;
        }

        private void _report1FilterDate_Load(object sender, EventArgs e)
        {
        }

        private void displayReport(int directPrint = 0)
        {
            string sqlCommand = "";

            List<sqlFields> listSql = new List<sqlFields>();
            sqlFields sqlElement;

            switch (originModuleID)
            {

                default:
                    break;
            }

        }

        private void displayButton_Click(object sender, EventArgs e)
        {
            displayReport();  
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
       
        }

        private void showAllCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            browseButton.Enabled = !showAllCheckBox.Checked;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            displayReport(1);
        }

        private void _reportFilter1Date_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (null != DS)
                DS.sqlClose();
        }
    }
    
}
