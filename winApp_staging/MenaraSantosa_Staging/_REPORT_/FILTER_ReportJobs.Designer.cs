﻿namespace AlphaSoft
{
    partial class dataFilterReportJobs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBoxCust = new System.Windows.Forms.CheckBox();
            this.checkBoxSales = new System.Windows.Forms.CheckBox();
            this.checkBoxKavling = new System.Windows.Forms.CheckBox();
            this.checkBoxKompleks = new System.Windows.Forms.CheckBox();
            this.checkBoxRange = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboAkses = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.endDTPicker = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.startDTPicker = new System.Windows.Forms.DateTimePicker();
            this.searchCustomer = new System.Windows.Forms.Button();
            this.customerTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.searchSales = new System.Windows.Forms.Button();
            this.salesTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.searchKavling = new System.Windows.Forms.Button();
            this.kavlingTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.searchKompleks = new System.Windows.Forms.Button();
            this.kompleksTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.generateButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox1.Controls.Add(this.checkBoxCust);
            this.groupBox1.Controls.Add(this.checkBoxSales);
            this.groupBox1.Controls.Add(this.checkBoxKavling);
            this.groupBox1.Controls.Add(this.checkBoxKompleks);
            this.groupBox1.Controls.Add(this.checkBoxRange);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboAkses);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.endDTPicker);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.startDTPicker);
            this.groupBox1.Controls.Add(this.searchCustomer);
            this.groupBox1.Controls.Add(this.customerTextBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.searchSales);
            this.groupBox1.Controls.Add(this.salesTextBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.searchKavling);
            this.groupBox1.Controls.Add(this.kavlingTextBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.searchKompleks);
            this.groupBox1.Controls.Add(this.kompleksTextBox);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(7, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(621, 252);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            // 
            // checkBoxCust
            // 
            this.checkBoxCust.AutoSize = true;
            this.checkBoxCust.Location = new System.Drawing.Point(136, 194);
            this.checkBoxCust.Name = "checkBoxCust";
            this.checkBoxCust.Size = new System.Drawing.Size(15, 14);
            this.checkBoxCust.TabIndex = 93;
            this.checkBoxCust.UseVisualStyleBackColor = true;
            // 
            // checkBoxSales
            // 
            this.checkBoxSales.AutoSize = true;
            this.checkBoxSales.Location = new System.Drawing.Point(136, 161);
            this.checkBoxSales.Name = "checkBoxSales";
            this.checkBoxSales.Size = new System.Drawing.Size(15, 14);
            this.checkBoxSales.TabIndex = 92;
            this.checkBoxSales.UseVisualStyleBackColor = true;
            // 
            // checkBoxKavling
            // 
            this.checkBoxKavling.AutoSize = true;
            this.checkBoxKavling.Location = new System.Drawing.Point(136, 128);
            this.checkBoxKavling.Name = "checkBoxKavling";
            this.checkBoxKavling.Size = new System.Drawing.Size(15, 14);
            this.checkBoxKavling.TabIndex = 91;
            this.checkBoxKavling.UseVisualStyleBackColor = true;
            // 
            // checkBoxKompleks
            // 
            this.checkBoxKompleks.AutoSize = true;
            this.checkBoxKompleks.Location = new System.Drawing.Point(136, 95);
            this.checkBoxKompleks.Name = "checkBoxKompleks";
            this.checkBoxKompleks.Size = new System.Drawing.Size(15, 14);
            this.checkBoxKompleks.TabIndex = 90;
            this.checkBoxKompleks.UseVisualStyleBackColor = true;
            // 
            // checkBoxRange
            // 
            this.checkBoxRange.AutoSize = true;
            this.checkBoxRange.Location = new System.Drawing.Point(136, 62);
            this.checkBoxRange.Name = "checkBoxRange";
            this.checkBoxRange.Size = new System.Drawing.Size(15, 14);
            this.checkBoxRange.TabIndex = 89;
            this.checkBoxRange.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(84, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 18);
            this.label1.TabIndex = 88;
            this.label1.Text = "Tipe";
            // 
            // comboAkses
            // 
            this.comboAkses.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboAkses.FormattingEnabled = true;
            this.comboAkses.Items.AddRange(new object[] {
            "Laporan Tugas Selesai",
            "Laporan Tugas Belum Selesai"});
            this.comboAkses.Location = new System.Drawing.Point(136, 22);
            this.comboAkses.Name = "comboAkses";
            this.comboAkses.Size = new System.Drawing.Size(377, 26);
            this.comboAkses.TabIndex = 87;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label11.Location = new System.Drawing.Point(332, 62);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 18);
            this.label11.TabIndex = 86;
            this.label11.Text = "-";
            // 
            // endDTPicker
            // 
            this.endDTPicker.CustomFormat = "dd MMM yyyy";
            this.endDTPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endDTPicker.Location = new System.Drawing.Point(354, 54);
            this.endDTPicker.Name = "endDTPicker";
            this.endDTPicker.Size = new System.Drawing.Size(171, 28);
            this.endDTPicker.TabIndex = 85;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Location = new System.Drawing.Point(36, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 18);
            this.label10.TabIndex = 84;
            this.label10.Text = "Tgl Tugas";
            // 
            // startDTPicker
            // 
            this.startDTPicker.CustomFormat = "dd MMM yyyy";
            this.startDTPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startDTPicker.Location = new System.Drawing.Point(155, 54);
            this.startDTPicker.Name = "startDTPicker";
            this.startDTPicker.Size = new System.Drawing.Size(171, 28);
            this.startDTPicker.TabIndex = 83;
            // 
            // searchCustomer
            // 
            this.searchCustomer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchCustomer.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchCustomer.Location = new System.Drawing.Point(497, 187);
            this.searchCustomer.Name = "searchCustomer";
            this.searchCustomer.Size = new System.Drawing.Size(77, 30);
            this.searchCustomer.TabIndex = 78;
            this.searchCustomer.Text = "Cari";
            this.searchCustomer.UseVisualStyleBackColor = true;
            this.searchCustomer.Click += new System.EventHandler(this.searchCustomer_Click);
            // 
            // customerTextBox
            // 
            this.customerTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerTextBox.Location = new System.Drawing.Point(155, 189);
            this.customerTextBox.MaxLength = 50;
            this.customerTextBox.Name = "customerTextBox";
            this.customerTextBox.ReadOnly = true;
            this.customerTextBox.Size = new System.Drawing.Size(336, 27);
            this.customerTextBox.TabIndex = 76;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Location = new System.Drawing.Point(37, 192);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 18);
            this.label7.TabIndex = 77;
            this.label7.Text = "Customer";
            // 
            // searchSales
            // 
            this.searchSales.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchSales.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchSales.Location = new System.Drawing.Point(497, 154);
            this.searchSales.Name = "searchSales";
            this.searchSales.Size = new System.Drawing.Size(77, 30);
            this.searchSales.TabIndex = 75;
            this.searchSales.Text = "Cari";
            this.searchSales.UseVisualStyleBackColor = true;
            this.searchSales.Click += new System.EventHandler(this.searchSales_Click);
            // 
            // salesTextBox
            // 
            this.salesTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salesTextBox.Location = new System.Drawing.Point(155, 156);
            this.salesTextBox.MaxLength = 50;
            this.salesTextBox.Name = "salesTextBox";
            this.salesTextBox.ReadOnly = true;
            this.salesTextBox.Size = new System.Drawing.Size(336, 27);
            this.salesTextBox.TabIndex = 73;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(76, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 18);
            this.label6.TabIndex = 74;
            this.label6.Text = "Sales";
            // 
            // searchKavling
            // 
            this.searchKavling.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchKavling.Enabled = false;
            this.searchKavling.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchKavling.Location = new System.Drawing.Point(497, 121);
            this.searchKavling.Name = "searchKavling";
            this.searchKavling.Size = new System.Drawing.Size(77, 30);
            this.searchKavling.TabIndex = 72;
            this.searchKavling.Text = "Cari";
            this.searchKavling.UseVisualStyleBackColor = true;
            this.searchKavling.Click += new System.EventHandler(this.searchKavling_Click);
            // 
            // kavlingTextBox
            // 
            this.kavlingTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kavlingTextBox.Location = new System.Drawing.Point(155, 123);
            this.kavlingTextBox.MaxLength = 50;
            this.kavlingTextBox.Name = "kavlingTextBox";
            this.kavlingTextBox.ReadOnly = true;
            this.kavlingTextBox.Size = new System.Drawing.Size(336, 27);
            this.kavlingTextBox.TabIndex = 70;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(59, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 18);
            this.label5.TabIndex = 71;
            this.label5.Text = "Kavling";
            // 
            // searchKompleks
            // 
            this.searchKompleks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchKompleks.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchKompleks.Location = new System.Drawing.Point(497, 88);
            this.searchKompleks.Name = "searchKompleks";
            this.searchKompleks.Size = new System.Drawing.Size(77, 30);
            this.searchKompleks.TabIndex = 59;
            this.searchKompleks.Text = "Cari";
            this.searchKompleks.UseVisualStyleBackColor = true;
            this.searchKompleks.Click += new System.EventHandler(this.searchKompleks_Click);
            // 
            // kompleksTextBox
            // 
            this.kompleksTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kompleksTextBox.Location = new System.Drawing.Point(155, 90);
            this.kompleksTextBox.MaxLength = 50;
            this.kompleksTextBox.Name = "kompleksTextBox";
            this.kompleksTextBox.ReadOnly = true;
            this.kompleksTextBox.Size = new System.Drawing.Size(336, 27);
            this.kompleksTextBox.TabIndex = 57;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(38, 93);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 18);
            this.label9.TabIndex = 58;
            this.label9.Text = "Kompleks";
            // 
            // generateButton
            // 
            this.generateButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.generateButton.BackColor = System.Drawing.Color.FloralWhite;
            this.generateButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.generateButton.ForeColor = System.Drawing.Color.Black;
            this.generateButton.Location = new System.Drawing.Point(210, 301);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(215, 37);
            this.generateButton.TabIndex = 30;
            this.generateButton.Text = "Tampilkan";
            this.generateButton.UseVisualStyleBackColor = false;
            this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // dataFilterReportJobs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(635, 374);
            this.Controls.Add(this.generateButton);
            this.Controls.Add(this.groupBox1);
            this.Name = "dataFilterReportJobs";
            this.Text = "REPORTS VIEWER";
            this.Load += new System.EventHandler(this.dataFilterReportJobs_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.generateButton, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label11;
        protected System.Windows.Forms.DateTimePicker endDTPicker;
        private System.Windows.Forms.Label label10;
        protected System.Windows.Forms.DateTimePicker startDTPicker;
        private System.Windows.Forms.Button searchCustomer;
        private System.Windows.Forms.TextBox customerTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button searchSales;
        private System.Windows.Forms.TextBox salesTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button searchKavling;
        private System.Windows.Forms.TextBox kavlingTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button searchKompleks;
        private System.Windows.Forms.TextBox kompleksTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboAkses;
        private System.Windows.Forms.CheckBox checkBoxCust;
        private System.Windows.Forms.CheckBox checkBoxSales;
        private System.Windows.Forms.CheckBox checkBoxKavling;
        private System.Windows.Forms.CheckBox checkBoxKompleks;
        private System.Windows.Forms.CheckBox checkBoxRange;
        private System.Windows.Forms.Button generateButton;
    }
}
