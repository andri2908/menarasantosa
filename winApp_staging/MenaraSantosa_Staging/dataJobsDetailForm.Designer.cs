﻿namespace AlphaSoft
{
    partial class dataJobsDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.taskDateDTPicker = new System.Windows.Forms.DateTimePicker();
            this.buttonJobInfo = new System.Windows.Forms.Button();
            this.notesTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.labelDurasi = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.taskNameTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.finishButton = new System.Windows.Forms.Button();
            this.rejectButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.notesGridView = new System.Windows.Forms.DataGridView();
            this.dateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.msgContent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.notesGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(819, 29);
            // 
            // errorLabel
            // 
            this.errorLabel.Size = new System.Drawing.Size(0, 18);
            this.errorLabel.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox1.Controls.Add(this.notesGridView);
            this.groupBox1.Controls.Add(this.taskDateDTPicker);
            this.groupBox1.Controls.Add(this.buttonJobInfo);
            this.groupBox1.Controls.Add(this.notesTextBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.labelDurasi);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.taskNameTextBox);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(12, 46);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(795, 451);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            // 
            // taskDateDTPicker
            // 
            this.taskDateDTPicker.CustomFormat = "dd MMM yyyy";
            this.taskDateDTPicker.Enabled = false;
            this.taskDateDTPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.taskDateDTPicker.Location = new System.Drawing.Point(137, 49);
            this.taskDateDTPicker.Name = "taskDateDTPicker";
            this.taskDateDTPicker.Size = new System.Drawing.Size(171, 28);
            this.taskDateDTPicker.TabIndex = 84;
            // 
            // buttonJobInfo
            // 
            this.buttonJobInfo.BackColor = System.Drawing.Color.FloralWhite;
            this.buttonJobInfo.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonJobInfo.ForeColor = System.Drawing.Color.Black;
            this.buttonJobInfo.Location = new System.Drawing.Point(667, 14);
            this.buttonJobInfo.Name = "buttonJobInfo";
            this.buttonJobInfo.Size = new System.Drawing.Size(110, 61);
            this.buttonJobInfo.TabIndex = 68;
            this.buttonJobInfo.Text = "Info Tugas";
            this.buttonJobInfo.UseVisualStyleBackColor = false;
            this.buttonJobInfo.Click += new System.EventHandler(this.buttonJobInfo_Click);
            // 
            // notesTextBox
            // 
            this.notesTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.notesTextBox.Location = new System.Drawing.Point(137, 118);
            this.notesTextBox.MaxLength = 200;
            this.notesTextBox.Multiline = true;
            this.notesTextBox.Name = "notesTextBox";
            this.notesTextBox.Size = new System.Drawing.Size(640, 71);
            this.notesTextBox.TabIndex = 64;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(53, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 18);
            this.label4.TabIndex = 65;
            this.label4.Text = "Catatan";
            // 
            // labelDurasi
            // 
            this.labelDurasi.AutoSize = true;
            this.labelDurasi.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurasi.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelDurasi.Location = new System.Drawing.Point(137, 86);
            this.labelDurasi.Name = "labelDurasi";
            this.labelDurasi.Size = new System.Drawing.Size(93, 18);
            this.labelDurasi.TabIndex = 63;
            this.labelDurasi.Text = "1000 Hari";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(13, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 18);
            this.label1.TabIndex = 61;
            this.label1.Text = "Berlangsung";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(13, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 18);
            this.label2.TabIndex = 55;
            this.label2.Text = "Nama Tugas";
            // 
            // taskNameTextBox
            // 
            this.taskNameTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.taskNameTextBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taskNameTextBox.Location = new System.Drawing.Point(137, 16);
            this.taskNameTextBox.MaxLength = 50;
            this.taskNameTextBox.Name = "taskNameTextBox";
            this.taskNameTextBox.ReadOnly = true;
            this.taskNameTextBox.Size = new System.Drawing.Size(524, 27);
            this.taskNameTextBox.TabIndex = 56;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(52, 57);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 18);
            this.label9.TabIndex = 58;
            this.label9.Text = "Tanggal";
            // 
            // finishButton
            // 
            this.finishButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.finishButton.BackColor = System.Drawing.Color.FloralWhite;
            this.finishButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.finishButton.ForeColor = System.Drawing.Color.Black;
            this.finishButton.Location = new System.Drawing.Point(354, 515);
            this.finishButton.Name = "finishButton";
            this.finishButton.Size = new System.Drawing.Size(120, 37);
            this.finishButton.TabIndex = 68;
            this.finishButton.Text = "Selesai";
            this.finishButton.UseVisualStyleBackColor = false;
            this.finishButton.Click += new System.EventHandler(this.finishButton_Click);
            // 
            // rejectButton
            // 
            this.rejectButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rejectButton.BackColor = System.Drawing.Color.FloralWhite;
            this.rejectButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rejectButton.ForeColor = System.Drawing.Color.Black;
            this.rejectButton.Location = new System.Drawing.Point(169, 515);
            this.rejectButton.Name = "rejectButton";
            this.rejectButton.Size = new System.Drawing.Size(130, 37);
            this.rejectButton.TabIndex = 67;
            this.rejectButton.Text = "Tolak";
            this.rejectButton.UseVisualStyleBackColor = false;
            this.rejectButton.Click += new System.EventHandler(this.rejectButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveButton.BackColor = System.Drawing.Color.FloralWhite;
            this.saveButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.ForeColor = System.Drawing.Color.Black;
            this.saveButton.Location = new System.Drawing.Point(529, 515);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(120, 37);
            this.saveButton.TabIndex = 69;
            this.saveButton.Text = "Simpan";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // notesGridView
            // 
            this.notesGridView.AllowUserToAddRows = false;
            this.notesGridView.AllowUserToDeleteRows = false;
            this.notesGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.notesGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.notesGridView.BackgroundColor = System.Drawing.Color.FloralWhite;
            this.notesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.notesGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dateTime,
            this.userName,
            this.msgContent});
            this.notesGridView.Location = new System.Drawing.Point(137, 195);
            this.notesGridView.MultiSelect = false;
            this.notesGridView.Name = "notesGridView";
            this.notesGridView.ReadOnly = true;
            this.notesGridView.RowHeadersVisible = false;
            this.notesGridView.Size = new System.Drawing.Size(640, 250);
            this.notesGridView.TabIndex = 85;
            // 
            // dateTime
            // 
            this.dateTime.HeaderText = "Tgl";
            this.dateTime.Name = "dateTime";
            this.dateTime.ReadOnly = true;
            this.dateTime.Width = 64;
            // 
            // userName
            // 
            this.userName.HeaderText = "User";
            this.userName.Name = "userName";
            this.userName.ReadOnly = true;
            this.userName.Width = 77;
            // 
            // msgContent
            // 
            this.msgContent.HeaderText = "Catatan";
            this.msgContent.Name = "msgContent";
            this.msgContent.ReadOnly = true;
            this.msgContent.Width = 107;
            // 
            // dataJobsDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(819, 593);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.finishButton);
            this.Controls.Add(this.rejectButton);
            this.Controls.Add(this.groupBox1);
            this.Name = "dataJobsDetailForm";
            this.Text = "DATA TUGAS";
            this.Load += new System.EventHandler(this.dataJobsDetailForm_Load);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.rejectButton, 0);
            this.Controls.SetChildIndex(this.finishButton, 0);
            this.Controls.SetChildIndex(this.saveButton, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.notesGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox notesTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelDurasi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox taskNameTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button finishButton;
        private System.Windows.Forms.Button rejectButton;
        private System.Windows.Forms.Button buttonJobInfo;
        protected System.Windows.Forms.DateTimePicker taskDateDTPicker;
        private System.Windows.Forms.Button saveButton;
        protected System.Windows.Forms.DataGridView notesGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn userName;
        private System.Windows.Forms.DataGridViewTextBoxColumn msgContent;
    }
}
